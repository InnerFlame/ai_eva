import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { SharedModule }                  from "../shared";
import { AccessComponent }               from "./access.component";
import { PagerService }                  from "../shared/services";

const phrasesRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path      : 'access',
        component : AccessComponent
    },
    {
        path      : 'access/:page',
        component : AccessComponent
    },
]);

@NgModule({
    imports: [
        phrasesRouting,
        SharedModule
    ],
    declarations: [
        AccessComponent
    ],
    providers: [
        PagerService
    ]
})

export class AccessModule {}