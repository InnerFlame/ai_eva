import { Component }        from '@angular/core';
import { Title }            from "@angular/platform-browser";
import { HttpService }      from "../shared/services/http.service";
import { OnInit }           from '@angular/core';
import {
    NlpClassifiersModel,
    NlpClassifiersClassesModel,
    NlpClassifiersClassPhrasesModel,
    PhraseEntitiesModel,
    NlpClassifiersRouting
} from "../shared/models/nlp.classifiers.model";

@Component({
    selector    : 'nlpClassifiers',
    templateUrl : './nlp.classifiers.html',
})

export class NlpClassifiersComponent implements OnInit
{
    readonly classifierTypeML     : string = 'ml_classifier';
    readonly classifierTypeRootML : string = 'root_ml_classifier';

    readonly maxTrainClassifierAttemps  : number = 4;
    readonly trainClassifierRequstDelay : number = 5000;

    private trainAttempts: number = 1;

    public errorText           : string  = '';
    public successText         : string  = '';
    public modalErrorText      : string  = '';
    public modalSuccessText    : string  = '';
    public displayLoadingBlock : boolean = true;
    public displayTrainBlock   : boolean = false;
    public disableListGroups   : boolean = false;
    public displayModalLoader  : boolean = false;

    public availableClassifiers            : any;
    public availableClassifiersModels      : any;
    public availableModels                 : any;
    public originalClassifiersModels       : any;
    public availableClassifierClasses      : any;
    public availableClassifierClassPhrases : any;
    public availablePhraseEntities         : any;

    public classifierModel            : NlpClassifiersModel;
    public classifierClassModel       : NlpClassifiersClassesModel;
    public classifierClassPhraseModel : NlpClassifiersClassPhrasesModel;
    public phraseEntitiesModel        : PhraseEntitiesModel = new PhraseEntitiesModel();

    public activeClassifiersType : string = this.classifierTypeML;
    public checkedClassPhrases   : any = [];

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param titleService
     */
    constructor(private httpService: HttpService, private titleService: Title)
    {
        this.titleService.setTitle( 'NLP classifiers' );
    }

    /**
     * Init actions
     */
    ngOnInit(): void
    {
        this.initClassifiersTree();
    }
    
    /**
     * Tabs switching
     * @param {string} type
     */
    public switchClassifiersType(type: string): void
    {
        this.activeClassifiersType = type;
        this.displayLoadingBlock   = true;
        this.disableListGroups     = true;
        this.initClassifiersTree();
    }

    /**
     * Filter availableModels list by classifier name
     */
    public filterClassifiersAvailableModels(): void
    {
        this.availableClassifiersModels = this.availableClassifiersModels
            .filter(x => x.split('_')[0] == this.classifierModel.text);
    }

    /**
     * Classifiers list group item click
     * @param classifier
     */
    public changeClassifier(classifier: any): void
    {
        this.classifierModel = new NlpClassifiersModel(
            this.activeClassifiersType,
            classifier.classifier_id,
            classifier.model,
            classifier.text,
        );
        this.setAvailableClassifiersModels();
        this.unCheckAllPhrases();
        this.availableClassifiersModels = this.originalClassifiersModels;
        this.getClassifierClassesById(classifier.classifier_id);
    }

    /**
     * Classes list group item click
     * @param classifierClass
     */
    public changeClassifierClass(classifierClass: any): void
    {
        this.classifierClassModel = new NlpClassifiersClassesModel(
            classifierClass.classifier_class_id,
            classifierClass.classifier_id,
            classifierClass.name,
            classifierClass.trigger_threshold
        );
        this.unCheckAllPhrases();
        this.getClassPhrasesById(classifierClass.classifier_class_id);
    }

    /**
     * Phrases list group item click
     * @param classPhrase
     */
    public changeClassifierClassPhrase(classPhrase: any): void
    {
        this.classifierClassPhraseModel = new NlpClassifiersClassPhrasesModel(
            classPhrase.learning_phrase_id,
            classPhrase.classifier_class_id,
            classPhrase.text
        );

        this.phraseEntitiesModel.learning_phrase_id = classPhrase.learning_phrase_id;
        this.phraseEntitiesModel.classifier_class_id = classPhrase.classifier_class_id;
        this.phraseEntitiesModel.value = '';
        this.phraseEntitiesModel.entity = '';
        this.modalErrorText = '';
        this.modalSuccessText = '';
    }

    /**
     * Sets entity model data
     * @param entityData
     */
    public setPhraseEntityModel(entityData: any): void
    {
        this.phraseEntitiesModel = new PhraseEntitiesModel(
            this.classifierClassPhraseModel.learning_phrase_id,
            entityData.learning_phrase_entity_id,
            this.classifierClassModel.classifier_class_id,
            entityData.start,
            entityData.end,
            entityData.entity,
            entityData.value
        );
    }

    /**
     * Check all phrases and store their ids into checkedClassPhrases array
     */
    public checkAllPhrases(): void
    {
        for (let i = 0; i < this.availableClassifierClassPhrases.length; i++) {
            this.checkedClassPhrases.push(this.availableClassifierClassPhrases[i].learning_phrase_id);
        }
    }

    /**
     * Reset checkedClassPhrases array
     */
    public unCheckAllPhrases(): void
    {
        this.checkedClassPhrases = [];
    }

    /**
     * Create new classifier and update availableList
     */
    public createClassifier(): void
    {
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierCreateUrl(), this.classifierModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data[0]).length || data[0].err) {
                this.errorText = 'Error while creating classifier!';
                this.disableListGroups = false;
                return;
            }

            this.resetClassifierModel();
            this.availableClassifiers.push(data[0].item.item);
            this.disableListGroups = false;
            this.successText = 'Classifier was created successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Update selected classifier
     */
    public updateClassifier(): void
    {
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierUpdateUrl(), this.classifierModel).then(data => {
            if (!Object.keys(data).length || !data.result || data.result[0].err) {
                this.errorText = 'Error while updating classifier!';
                this.disableListGroups = false;
                return;
            }

            for (let i = 0; i < this.availableClassifiers.length; i++) {
                if (this.availableClassifiers[i].classifier_id == this.classifierModel.classifier_id) {
                    this.availableClassifiers[i].text = this.classifierModel.text;
                }
            }

            this.resetClassifierClassModel();
            this.disableListGroups = false;
            this.successText = 'Classifier was updated successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Delete classifier and update availableList
     */
    public deleteClassifier(): void
    {
        if (!confirm('Do you sure you want to delete "' + this.classifierModel.text + '" classifier?')) {
            return;
        }

        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierDeleteUrl(), this.classifierModel).then(data => {
            if (!Object.keys(data).length) {
                this.errorText = 'Error while creating classifier!';
                this.disableListGroups = false;
                return;
            }

            this.filterClassifierListAfterDelete();
            this.resetClassifierModel();
            this.disableListGroups = false;
            this.successText = 'Classifier was deleted successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Train classifier
     */
    public trainClassifier(): void
    {
        this.displayTrainBlock = true;

        this.httpService.post(NlpClassifiersRouting.getClassifierTrainUrl(), this.classifierModel).then(data => {
            if (!Object.keys(data).length) {
                this.errorText = 'Error while train classifier, maybe another classifier train in proccess!';
                this.displayTrainBlock = false;
                return;
            }
        }, err => {
            this.errorText = 'Error on server!';
            this.displayTrainBlock = false;
        });

        this.getAvailableModels();
    }

    /**
     * Create classifier class
     */
    public createClass(): void
    {
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierClassCreateUrl(), this.classifierClassModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data[0]).length || data[0].err) {
                this.errorText = 'Error while creating class!';
                this.disableListGroups = false;
                return;
            }

            this.resetClassifierClassModel();
            this.availableClassifierClasses.push(data[0].item.item);
            this.disableListGroups = false;
            this.successText = 'Class was created successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Update classifier class
     */
    public updateClass(): void
    {
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierClassUpdateUrl(), this.classifierClassModel).then(data => {
            if (!Object.keys(data).length || !data.result || data.result[0].err) {
                this.errorText = 'Error while updating class!';
                this.disableListGroups = false;
                return;
            }

            for (let i = 0; i < this.availableClassifierClasses.length; i++) {
                if (this.availableClassifierClasses[i].classifier_class_id == this.classifierClassModel.classifier_class_id) {
                    this.availableClassifierClasses[i].name = this.classifierClassModel.name;
                }
            }

            this.disableListGroups = false;
            this.successText = 'Class was updated successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Delete classifier class
     */
    public deleteClass(): void
    {
        if (!confirm('Do you sure you want to delete "' + this.classifierClassModel.name + '" class?')) {
            return;
        }

        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierClassDeleteUrl(), this.classifierClassModel).then(data => {
            if (!Object.keys(data).length) {
                this.errorText = 'Error while creating classifier!';
                this.disableListGroups = false;
                return;
            }

            this.filterClassifierClassesListAfterDelete();
            this.resetClassifierClassModel();
            this.disableListGroups = false;
            this.successText = 'Classifier was deleted successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Create class phrase
     */
    public createPhrase():void
    {
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierClassPhraseCreateUrl(), this.classifierClassPhraseModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data[0]).length || data[0].err) {
                this.errorText = 'Error while creating phrase!';
                this.disableListGroups = false;
                return;
            }

            this.resetClassifierClassPhraseModel();
            this.addPhraseToAvailablePhrasesAfterCreate(data);

            this.disableListGroups = false;
            this.successText = 'Phrase was created successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Update class phrase
     */
    public updatePhrase():void
    {
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierClassPhraseUpdateUrl(), this.classifierClassPhraseModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data.result).length || data.result[0].err) {
                this.errorText = 'Error while updating phrase!';
                this.disableListGroups = false;
                return;
            }

            this.updateAvailablePhrasesItemAfterUpdate(data);
            this.disableListGroups = false;
            this.successText = 'Phrase was updated successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Delete class  phrase
     */
    public deletePhrase():void
    {
        let unifyPhrasesData = Array.from(new Set(this.checkedClassPhrases));

        if (unifyPhrasesData.length > 1) {
            this.deleteMultiplePhrases(unifyPhrasesData);
            return;
        }

        if (!confirm('Do you sure you want to delete "' + this.classifierClassPhraseModel.text + '" phrase?')) {
            return;
        }

        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getClassifierClassPhraseDeleteUrl(), this.classifierClassPhraseModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data.result).length || data.result[0].err) {
                this.errorText = 'Error while deleting phrase!';
                this.disableListGroups = false;
                return;
            }

            this.filterClassifierClassPhrasesListAfterDelete();
            this.resetClassifierClassPhraseModel();
            this.disableListGroups = false;
            this.successText = 'Phrase was deleted successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Delete multiple phrases by ids
     * @param {Array<any>} phraseIds
     */
    private deleteMultiplePhrases(phraseIds: Array<any>): void
    {
        if (!confirm('Do you sure you want to delete selected phrases?')) {
            return;
        }
        this.disableListGroups = true;

        this.httpService.post(NlpClassifiersRouting.getClassifierClassPhraseDeleteMultipleUrl(), {phraseIds}).then(data => {
            if (!Object.keys(data).length) {
                this.errorText = 'Error while deleting phrases!';
                this.disableListGroups = false;
                return;
            }

            this.filterClassifierClassPhrasesListAfterMultiDelete(phraseIds);
            this.resetClassifierClassPhraseModel();
            this.disableListGroups = false;
            this.successText = 'Phrases were deleted successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });

    }

    /**
     * Create phrase entity
     */
    public createPhraseEntity(): void
    {
        this.displayModalLoader = true;
        this.setStartAndEndValuesForEntity();
        this.httpService.post(NlpClassifiersRouting.getPhraseEntityCreateUrl(), this.phraseEntitiesModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data[0]).length || data[0].err) {
                this.modalErrorText = 'Error while creating entity!';
                this.displayModalLoader = false;
                return;
            }

            if (!data[0].item || !data[0].item.item) {
                this.modalErrorText = 'Invalid create response from create action!';
                this.displayModalLoader = false;
                return;
            }

            let createdEntity = data[0].item.item;
            if (!this.availablePhraseEntities[this.classifierClassPhraseModel.learning_phrase_id]) {
                let phraseId = this.classifierClassPhraseModel.learning_phrase_id;
                this.availablePhraseEntities[phraseId] = [];
            }

            this.availablePhraseEntities[this.classifierClassPhraseModel.learning_phrase_id].push(createdEntity);
            this.phraseEntitiesModel.learning_phrase_entity_id = createdEntity.learning_phrase_entity_id;

            this.displayModalLoader = false;
            this.modalSuccessText = 'Entity was created successful!';
        }, err => {
            this.modalErrorText = 'Error on server!';
            this.displayModalLoader = false;
        });
    }

    /**
     * Update phrase entity
     */
    public updatePhraseEntity(): void
    {
        this.displayModalLoader = true;
        this.setStartAndEndValuesForEntity();
        this.httpService.post(NlpClassifiersRouting.getPhraseEntityUpdateUrl(), this.phraseEntitiesModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data['result']).length || !Object.keys(data['result'][0]).length || data['result'][0].err) {
                this.modalErrorText = 'Error while updating entity!';
                this.displayModalLoader = false;
                return;
            }

            if (!data['result'][0].item) {
                this.modalErrorText = 'Invalid create response from create action!';
                this.displayModalLoader = false;
                return;
            }

           this.updateAvailableEntitiesItemAfterUpdate(data['result'][0].item);

            this.displayModalLoader = false;
            this.modalSuccessText = 'Entity was updated successful!';
        }, err => {
            this.modalErrorText = 'Error on server!';
            this.displayModalLoader = false;
        });

    }

    /**
     * Delete phrase entity
     */
    public deletePhraseEntity(): void
    {
        if (!confirm('Do you really want to delete entity "' + this.phraseEntitiesModel.entity + '" ?')) {
            return;
        }

        this.displayModalLoader = true;
        this.httpService.post(NlpClassifiersRouting.getPhraseEntityDeleteUrl(), this.phraseEntitiesModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data['result']).length || !Object.keys(data['result'][0]).length || data['result'][0].err) {
                this.modalErrorText = 'Error while deleting entity!';
                this.displayModalLoader = false;
                return;
            }

            if (!data['result'][0].item) {
                this.modalErrorText = 'Invalid create response from create action!';
                this.displayModalLoader = false;
                return;
            }

            this.availablePhraseEntities[this.classifierClassPhraseModel.learning_phrase_id] = this.availablePhraseEntities[this.classifierClassPhraseModel.learning_phrase_id]
                .filter(x => x.learning_phrase_entity_id != this.phraseEntitiesModel.learning_phrase_entity_id);

            this.displayModalLoader = false;
            this.modalSuccessText = 'Entity was deleted successful!';
        }, err => {
            this.modalErrorText = 'Error on server!';
            this.displayModalLoader = false;
        });
    }

    /**
     * Crete entities for all class phrases
     */
    public createMultiPhraseEntity(): void
    {
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getPhraseEntityCreateMultiUrl(), this.phraseEntitiesModel).then(data => {
            if (!Object.keys(data).length || !data.status) {
                this.modalErrorText = 'Error while creating entities from phrases!';
                this.disableListGroups = false;
                return;
            }

            this.getClassPhrasesById(this.classifierClassModel.classifier_class_id);
            this.successText = 'Entities were created successfully!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Update entities for all class phrases
     */
    public updateMultiPhraseEntity(): void
    {
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getPhraseEntityUpdateMultiUrl(), this.phraseEntitiesModel).then(data => {
            if (!Object.keys(data).length || !data.status) {
                this.modalErrorText = 'Error while updating entities from phrases!';
                this.disableListGroups = false;
                return;
            }

            this.getClassPhrasesById(this.classifierClassModel.classifier_class_id);
            this.successText = 'Entities were updated successfully!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Delete all same entities from class phrases
     */
    public deleteMultiPhraseEntity(): void
    {
        if (!confirm('Do you really want to delete entity "' + this.phraseEntitiesModel.entity + '" from all phrases?')) {
            return;
        }
        this.disableListGroups = true;
        this.httpService.post(NlpClassifiersRouting.getPhraseEntityDeleteMultiUrl(), this.phraseEntitiesModel).then(data => {
            if (!Object.keys(data).length || !data.status) {
                this.modalErrorText = 'Error while deleting entities from phrases!';
                this.disableListGroups = false;
                return;
            }

            this.getClassPhrasesById(this.classifierClassModel.classifier_class_id);
            this.successText = 'Entities were deleted successfully!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    /**
     * Calculare initial/updated start and end values and also set relation ids if its empty
     */
    private setStartAndEndValuesForEntity()
    {
        if (!this.phraseEntitiesModel.learning_phrase_id) {
            this.phraseEntitiesModel.learning_phrase_id = this.classifierClassPhraseModel.learning_phrase_id;
        }

        if (!this.phraseEntitiesModel.classifier_class_id) {
            this.phraseEntitiesModel.classifier_class_id = this.classifierClassModel.classifier_class_id;
        }

        this.phraseEntitiesModel.start = this.classifierClassPhraseModel.text.indexOf(this.phraseEntitiesModel.value);
        this.phraseEntitiesModel.end   = (this.phraseEntitiesModel.start + this.phraseEntitiesModel.value.length);
    }

    /**
     * Update avaialble entities for phrase
     * @param updatedItem
     */
    private updateAvailableEntitiesItemAfterUpdate(updatedItem: any)
    {
        for (let i = 0; i < this.availablePhraseEntities[this.phraseEntitiesModel.learning_phrase_id].length; i++) {
            let availableItem = this.availablePhraseEntities[this.phraseEntitiesModel.learning_phrase_id][i];
            if (availableItem.learning_phrase_entity_id == updatedItem.learning_phrase_entity_id) {
                availableItem.value = updatedItem.value;
                availableItem.entity = updatedItem.entity;
                availableItem.start = updatedItem.start;
                availableItem.end = updatedItem.end;
            }
        }
    }

    /**
     * Get classes for classifier by classifier id
     * @param {number} classifierId
     */
    public getClassifierClassesById(classifierId: number): void
    {
        this.disableListGroups = true;

        this.httpService.get(NlpClassifiersRouting.getClassifierClassesUrl(),
            {'classifierId': classifierId})
            .then(data => {
                if (!Object.keys(data).length) {
                    this.disableListGroups = false;
                    this.resetClassesWithPhrases();
                    return;
                }

                if (!data[0] || !data[0].classifier_class_id) {
                    this.errorText = 'Invalid response for classes!';
                    this.disableListGroups = false;
                    this.resetClassesWithPhrases();
                    return;
                }

                this.classifierClassModel = new NlpClassifiersClassesModel(
                    data[0].classifier_class_id,
                    data[0].classifier_id,
                    data[0].name,
                    data[0].trigger_threshold
                );

                this.getClassPhrasesById(data[0].classifier_class_id);
                this.availableClassifierClasses = data;
                this.disableListGroups = false;
            });
    }

    /**
     * Get class phrases by id
     * @param {number} classId
     */
    public getClassPhrasesById(classId: number): void
    {
        this.disableListGroups = true;

        this.httpService.get(NlpClassifiersRouting.getClassifierClassPhrasesUrl(),
            {'classId': classId})
            .then(data => {
                if (!Object.keys(data['phrasesData']).length) {
                    this.disableListGroups = false;
                    this.resetPhrases();
                    return;
                }

                if (!data['phrasesData'][0] || !data['phrasesData'][0].learning_phrase_id) {
                    this.errorText = 'Invalid response for phrases!';
                    this.disableListGroups = false;
                    this.resetPhrases();
                    return;
                }

                if (data['entitiesData']) {
                    this.availablePhraseEntities = data['entitiesData'];
                }

                this.classifierClassPhraseModel = new NlpClassifiersClassPhrasesModel(
                    data['phrasesData'][0].learning_phrase_id,
                    data['phrasesData'][0].classifier_class_id,
                    data['phrasesData'][0].text
                );

                this.availableClassifierClassPhrases = data['phrasesData'];
                this.disableListGroups = false;
            });
    }

    /**
     * Marked entity value for phrase
     *
     * @param {number} phraseId
     * @param {string} phraseText
     * @returns {string}
     */
    public getMarkedPhraseText(phraseId: number, phraseText: string): string
    {
        if (!this.availablePhraseEntities[phraseId]) {
            return phraseText;
        }

        let markedText = '';

        for (let i = 0; i < this.availablePhraseEntities[phraseId].length; i++) {
            let entityData = this.availablePhraseEntities[phraseId][i];

            if (phraseText.indexOf(entityData.value) !== -1) {
                markedText = phraseText.replace(new RegExp('<[^>]+>|('+ entityData.value +')', 'g'), function (p1, p2) {
                    return ((p2==undefined)||p2=='') ? p1 : '<span class="marked">'+p1+'</span>';
                });

                phraseText = markedText;
            }
        }

        return phraseText;
    }

    /**
     * Get all classifiers data, classes data and phrases
     */
    private initClassifiersTree(): void
    {
        this.httpService.get(NlpClassifiersRouting.getClassifiersUrl(),
            {'type': this.activeClassifiersType})
            .then(data => {
                if (!data || !data['classifiers']) {
                    this.errorText = 'No classifiers data!';
                    this.displayLoadingBlock = false;
                    this.resetClassifiersTree();
                    return;
                }

                this.processClassifiersData(data['classifiers']);

                if (data && data['availableModels']) {
                    this.availableModels = data['availableModels'];
                    this.setAvailableClassifiersModels();
                }

                this.displayLoadingBlock = false;
                this.disableListGroups = false;
            }, err => {
                this.errorText = 'Error on server!';
                this.disableListGroups = false;
                this.displayLoadingBlock = false;
            });
    }

    /**
     * Set model set to classifier.
     */
    private setAvailableClassifiersModels(): void
    {
        this.originalClassifiersModels = this.availableClassifiersModels =
            this.availableModels.available_projects[this.classifierModel.text] ?
            this.availableModels.available_projects[this.classifierModel.text].available_models : [];
    }

    /**
     * Update all data when classifier item changed
     * @param classifiersData
     */
    private processClassifiersData(classifiersData: any): void
    {
        this.availableClassifiers = classifiersData;

        if (!classifiersData[0] || !classifiersData[0].classifier_id) {
            return;
        }

        this.classifierModel = new NlpClassifiersModel(
            this.activeClassifiersType,
            classifiersData[0].classifier_id,
            classifiersData[0].model,
            classifiersData[0].text,
        );
        this.getClassifierClassesById(classifiersData[0].classifier_id);
    }

    /**
     * Get available models list
     */
    private getAvailableModels()
    {
        this.httpService.post(NlpClassifiersRouting.getClassifierAvailableModelsUrl(), this.classifierModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data['models']).length) {
                this.errorText = 'Invalid response after train classifier!';
                this.displayTrainBlock = false;
                return;
            }

            if (data.trainings_processes && (this.trainAttempts <= this.maxTrainClassifierAttemps)) {
                this.repeatModelsRequest();
                return;
            }

            this.availableClassifiersModels = data['models'];
            this.successText = 'Classifier was trained successful!';
            this.displayTrainBlock = false;
        }, err => {
            this.errorText = 'Error on server!';
            this.displayTrainBlock = false;
        });
    }

    /**
     * Repeat get models request with 5 sec delay
     */
    private repeatModelsRequest()
    {
        this.trainAttempts++;

        setTimeout(e => {
            this.getAvailableModels()
        }, this.trainClassifierRequstDelay)
    }

    /**
     * Update avaialble phrases list after phrase(s) was/were created
     * @param responseData
     */
    private addPhraseToAvailablePhrasesAfterCreate(responseData: any): void
    {
        if (Object.keys(responseData).length > 1) {
            for (let i=0; i < Object.keys(responseData).length; i++) {
                this.availableClassifierClassPhrases.push(responseData[i][0].item.item);
            }
        } else {
            this.availableClassifierClassPhrases.push(responseData[0].item.item);
        }
    }

    /**
     * Update item value in availableList after successful update
     * @param requestData
     */
    private updateAvailablePhrasesItemAfterUpdate(requestData: any): void
    {
        for (let i=0; i < Object.keys(this.availableClassifierClassPhrases).length; i++)
        {
            let updatedPhraseId  : number = requestData.result[0].item.learning_phrase_id;
            let updatedPhraseText: string = requestData.result[0].item.text;

            if (this.availableClassifierClassPhrases[i].learning_phrase_id == updatedPhraseId) {
                this.availableClassifierClassPhrases[i].text = updatedPhraseText;
            }
        }
    }

    /**
     * Filter availableClassifiersList when classifier was deleted
     */
    private filterClassifierListAfterDelete(): void
    {
        this.availableClassifiers = this.availableClassifiers
            .filter(x => x.classifier_id != this.classifierModel.classifier_id);
    }

    /**
     * Filter availableClassifiersList when classifier was deleted
     */
    private filterClassifierClassesListAfterDelete(): void
    {
        this.availableClassifierClasses = this.availableClassifierClasses
            .filter(x => x.classifier_class_id != this.classifierClassModel.classifier_class_id);
    }

    /**
     * Filter availableClassifierClassPhrases when phrase was deleted
     */
    private filterClassifierClassPhrasesListAfterDelete(): void
    {
        this.availableClassifierClassPhrases = this.availableClassifierClassPhrases
            .filter(x => x.learning_phrase_id != this.classifierClassPhraseModel.learning_phrase_id);
    }

    /**
     * Filter phrases after multi delete
     * @param phraseIds
     */
    private filterClassifierClassPhrasesListAfterMultiDelete(phraseIds: any)
    {
        let filteredPhrases: any = [];

        for (let i = 0; i < this.availableClassifierClassPhrases.length; i++ ) {
            if (phraseIds.indexOf(this.availableClassifierClassPhrases[i].learning_phrase_id) === -1) {
                filteredPhrases.push(this.availableClassifierClassPhrases[i]);
            }
        }

        this.availableClassifierClassPhrases = filteredPhrases;
    }

    /**
     * Reset only classifier model
     */
    private resetClassifierModel(): void
    {
        this.classifierModel = new NlpClassifiersModel(this.activeClassifiersType);
        this.unCheckAllPhrases();
    }

    /**
     * Reset only classifier class model
     */
    private resetClassifierClassModel(): void
    {
        this.availableClassifierClassPhrases = [];
        this.classifierClassModel            = new NlpClassifiersClassesModel(0, this.classifierModel.classifier_id);
        this.resetClassifierClassPhraseModel();
        this.unCheckAllPhrases();
    }

    /**
     * Reset only classifier class model
     */
    private resetClassifierClassPhraseModel(): void
    {
        if (this.classifierClassModel.classifier_class_id) {
            this.classifierClassPhraseModel = new NlpClassifiersClassPhrasesModel(0, this.classifierClassModel.classifier_class_id, '');
        } else {
            this.classifierClassPhraseModel = new NlpClassifiersClassPhrasesModel();
        }
    }

    /**
     * Reset all classifiers with classes and availableHolders
     */
    private resetClassifiersTree(): void
    {
        this.availableClassifiers            = [];
        this.availableClassifierClasses      = [];
        this.availableClassifierClassPhrases = [];
        this.classifierModel                 = new NlpClassifiersModel(this.activeClassifiersType);
        this.classifierClassModel            = new NlpClassifiersClassesModel(0, this.classifierModel.classifier_id);
        this.classifierClassPhraseModel      = new NlpClassifiersClassPhrasesModel();
        this.unCheckAllPhrases();
    }

    /**
     * Reset all classes with phrases and availableHolders
     */
    private resetClassesWithPhrases():void
    {
        this.availableClassifierClasses      = [];
        this.availableClassifierClassPhrases = [];
        this.classifierClassModel            = new NlpClassifiersClassesModel(0, this.classifierModel.classifier_id);
        this.classifierClassPhraseModel      = new NlpClassifiersClassPhrasesModel();
        this.unCheckAllPhrases();
    }

    /**
     * Reset all phrases and availableHolder
     */
    private resetPhrases(): void
    {
        this.availableClassifierClassPhrases = [];
        if (this.classifierClassModel.classifier_class_id) {
            this.classifierClassPhraseModel = new NlpClassifiersClassPhrasesModel(0, this.classifierClassModel.classifier_class_id, '');
        } else {
            this.classifierClassPhraseModel = new NlpClassifiersClassPhrasesModel();
        }
    }
}
