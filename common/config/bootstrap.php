<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

$envConfig = @include_once(dirname(dirname(__DIR__)) . '/common/config/set-environment.php');
if (is_array($envConfig)) {
    foreach ($envConfig as $envVarname => $envValue) {
        getenv($envVarname, true) !== false or putenv($envVarname . '=' . $envValue);
    }
}
unset($envConfig, $envVarname, $envValue);
