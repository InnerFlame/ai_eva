<?php

namespace frontend\assets;

class SearchAsset extends BasicAsset
{
    public $js = [
        'js/search.js',
    ];
}
