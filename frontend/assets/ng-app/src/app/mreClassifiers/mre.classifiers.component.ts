import { Component }        from '@angular/core';
import { Title }            from "@angular/platform-browser";
import { HttpService }      from "../shared/services/http.service";
import { OnInit }           from '@angular/core';
import {
    MreClassifierModel,
    MreClassModel,
    MrePatternModel,
    MreClassifierRouting
} from "../shared/models/mre.classifiers.model";

@Component({
    selector    : 'mreClassifiers',
    templateUrl : './mre.classifiers.html',
})

export class MreClassifiersComponent implements OnInit
{
    public errorText: string = '';
    public successText: string = '';

    public displayLoadingBlock: boolean = true;

    public mreClassifiers: any;
    public mreClasses: any;
    public mrePatterns: any;

    public mreClassifierModel: MreClassifierModel;
    public mreClassModel: MreClassModel;
    public mrePatternModel: MrePatternModel;

    public mrePatternsChecked: any = [];

    public constructor(private httpService: HttpService, private titleService: Title)
    {
        this.titleService.setTitle('Multi regexp classifiers');
    }

    public ngOnInit(): void
    {
        this.initClassifiersTree();
    }

    /*
    |--------------------------------------------------------------------------
    | Classifiers
    |--------------------------------------------------------------------------
    */

    private initClassifiersTree(): void
    {
        this.httpService.get(MreClassifierRouting.actionGetClassifiers()).then(data => {
            if (!data || !data['classifiers'] || !data['classifiers'][0] || !data['classifiers'][0].classifier_id) {
                this.errorText = 'No classifiers data!';
                this.displayLoadingBlock = false;
                this.resetClassifiersTree();
                return;
            }

            this.mreClassifiers = data['classifiers'];

            this.mreClassifierModel = new MreClassifierModel();
            this.mreClassifierModel.load(this.mreClassifiers[0]);

            this.getClasses(this.mreClassifierModel);
        }, err => {
            this.errorText = 'Error on server!';
            this.displayLoadingBlock = false;
        });
    }

    private resetClassifiersTree(): void
    {
        this.mreClassifiers = [];
        this.mreClasses = [];
        this.mrePatterns = [];

        this.mreClassifierModel = new MreClassifierModel();
        this.mreClassModel = new MreClassModel();
        this.mrePatternModel = new MrePatternModel();
    }

    public createClassifier(): void
    {
        this.httpService.post(MreClassifierRouting.actionCreateClassifier(), this.mreClassifierModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data[0]).length || data[0].err) {
                this.errorText = 'Error while creating classifier!';
                return;
            }

            this.mreClassifierModel = new MreClassifierModel();
            this.mreClassifierModel.load(data[0].item.item);

            this.mreClassifiers.push(JSON.parse(JSON.stringify(this.mreClassifierModel)));

            this.resetClasses();

            this.successText = 'Classifier was created successful!';
        }, err => {
            this.errorText = 'Error on server!';
        });
    }

    public updateClassifier(): void
    {
        this.httpService.post(MreClassifierRouting.actionUpdateClassifier(), this.mreClassifierModel).then(data => {
            if (!Object.keys(data).length || !data.result || data.result[0].err) {
                this.errorText = 'Error while updating classifier!';
                return;
            }

            for (let i = 0; i < this.mreClassifiers.length; i++) {
                if (this.mreClassifiers[i].classifier_id == this.mreClassifierModel.classifier_id) {
                    this.mreClassifiers[i].text = this.mreClassifierModel.text;
                }
            }

            this.successText = 'Classifier was updated successful!';
        }, err => {
            this.errorText = 'Error on server!';
        });
    }

    public deleteClassifier(): void
    {
        if (!confirm('Do you sure you want to delete "' + this.mreClassifierModel.text + '" classifier?')) {
            return;
        }

        this.httpService.post(MreClassifierRouting.actionDeleteClassifier(), this.mreClassifierModel).then(data => {
            if (!Object.keys(data).length) {
                this.errorText = 'Error while deleting classifier!';
                return;
            }

            this.filterClassifierListAfterDelete();
            this.resetClassifierModel();
            this.resetClasses();

            this.successText = 'Classifier was deleted successful!';
        }, err => {
            this.errorText = 'Error on server!';
        });
    }

    public changeClassifier(classifier: any): void
    {
        this.mreClassifierModel = new MreClassifierModel(
            classifier.classifier_id,
            classifier.model,
            classifier.text,
        );

        this.unCheckAllPatterns();

        this.getClasses(this.mreClassifierModel);
    }

    /*
    |--------------------------------------------------------------------------
    | Classes
    |--------------------------------------------------------------------------
    */

    private getClasses(mreClassifierModel: MreClassifierModel): void
    {
        this.httpService.get(MreClassifierRouting.actionGetClasses(), {'classifierId': mreClassifierModel.classifier_id}).then(data => {
            if (!Object.keys(data).length) {
                this.resetClasses();
                return;
            }

            if (!data[0] || !data[0].classifier_class_id) {
                this.errorText = 'Invalid response!';
                this.resetClasses();
                return;
            }

            this.mreClassModel = new MreClassModel();
            this.mreClassModel.load(data[0]);

            this.mreClasses = data;

            this.getPatterns(this.mreClassModel);
        });

        this.displayLoadingBlock = false;
    }

    public createClass(): void
    {
        this.httpService.post(MreClassifierRouting.actionCreateClass(), this.mreClassModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data[0]).length || data[0].err) {
                this.errorText = 'Error while creating class!';
                return;
            }

            this.mreClassModel = new MreClassModel();
            this.mreClassModel.load(data[0].item.item);

            this.mreClasses.push(JSON.parse(JSON.stringify(this.mreClassModel)));

            this.resetPatterns();

            this.successText = 'Class was created successful!';
        }, err => {
            this.errorText = 'Error on server!';
        });
    }

    public updateClass(): void
    {
        this.httpService.post(MreClassifierRouting.actionUpdateClass(), this.mreClassModel).then(data => {
            if (!Object.keys(data).length || !data.result || data.result[0].err) {
                this.errorText = 'Error while updating class!';
                return;
            }

            for (let i = 0; i < this.mreClasses.length; i++) {
                if (this.mreClasses[i].classifier_class_id == this.mreClassModel.classifier_class_id) {
                    this.mreClasses[i].name = this.mreClassModel.name;
                }
            }

            this.successText = 'Class was updated successful!';
        }, err => {
            this.errorText = 'Error on server!';
        });
    }

    public deleteClass(): void
    {
        if (!confirm('Do you sure you want to delete "' + this.mreClassModel.name + '" class?')) {
            return;
        }

        this.httpService.post(MreClassifierRouting.actionDeleteClass(), this.mreClassModel).then(data => {
            if (!Object.keys(data).length) {
                this.errorText = 'Error while creating classifier!';
                return;
            }

            this.filterClassifierClassesListAfterDelete();
            this.resetPatterns();

            this.successText = 'Classifier was deleted successful!';
        }, err => {
            this.errorText = 'Error on server!';
        });
    }

    public changeClass(data: any): void
    {
        this.mreClassModel = new MreClassModel();
        this.mreClassModel.load(data);

        this.getPatterns(this.mreClassModel);
    }

    private resetClasses(): void
    {
        this.mreClasses = [];
        this.mreClassModel = new MreClassModel();
        this.mreClassModel.classifier_id = this.mreClassifierModel.classifier_id;

        this.resetPatterns();
    }

    /*
    |--------------------------------------------------------------------------
    | Patterns
    |--------------------------------------------------------------------------
    */

    private getPatterns(mreClassModel: MreClassModel)
    {
        this.httpService.get(MreClassifierRouting.actionGetPatterns(), {classId: mreClassModel.classifier_class_id}).then(data => {
            if (!Object.keys(data).length) {
                this.resetPatterns();
                return;
            }

            this.mrePatternModel = new MrePatternModel();
            this.mrePatternModel.load(data[0]);

            this.mrePatterns = data;
        });
    }

    public changePattern(data: any): void
    {
        this.mrePatternModel = new MrePatternModel();
        this.mrePatternModel.load(data);
    }

    public createPattern():void
    {
        if (!this.testPattern(false)) {
            return;
        }

        this.httpService.post(MreClassifierRouting.actionCreatePattern(), this.mrePatternModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data[0]).length || data[0].err) {
                this.errorText = 'Troubles with create pattern response!';
                return;
            }

            this.mrePatternModel = new MrePatternModel();
            this.mrePatternModel.load(data[0].item.item);

            this.mrePatterns.push(JSON.parse(JSON.stringify(this.mrePatternModel)));

            this.successText = 'Pattern was created successfully!';
        }, err => {
            this.errorText = 'Error on server while creating pattern!';
        });
    }

    public updatePattern(toggleActive = false):void
    {
        if (!this.testPattern(false)) {
            return;
        }

        if (this.mrePatternsChecked.length) {
            this.updateMultiPatterns(toggleActive);
            return;
        }

        this.mrePatternModel.classifier_id = this.mreClassifierModel.classifier_id;
        this.httpService.post(MreClassifierRouting.actionUpdatePattern(), this.mrePatternModel).then(data => {
            if (!data || !Object.keys(data). length || !data.result || !data.result.length || data.result[0].err) {
                this.errorText = 'Troubles with update pattern response!';
                return;
            }

            this.mrePatterns = this.mrePatterns.map(function (el) {
                if (el.classifier_pattern_id == this.patternId) {
                    el.text = this.newText;
                    el.active = this.newActive
                }

                return el;
            }, {patternId: this.mrePatternModel.classifier_pattern_id, newText: this.mrePatternModel.text, newActive: this.mrePatternModel.active});

            this.successText = 'Pattern was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating pattern!';
        });
    }

    public deletePattern():void
    {
        if (this.mrePatternsChecked.length) {
            if (!confirm("Do you really want to delete all checked patterns?")) {
                return;
            }
            this.deleteMultiPatterns();
            return;
        }

        if (!confirm("Do you really want to delete '" + this.mrePatternModel.text + "' pattern?")) {
            return;
        }

        this.httpService.post(MreClassifierRouting.actionDeletePattern(), this.mrePatternModel).then(data => {
            if (!data || !Object.keys(data). length || !data.result || !data.result.length || data.result[0].err) {
                this.errorText = 'Troubles with delete classifier response!';
                return;
            }

            this.mrePatterns = this.mrePatterns
                .filter(el => el.classifier_pattern_id != this.mrePatternModel.classifier_pattern_id);

            this.successText = 'Pattern was deleted successfully!';
        }, err => {
            this.errorText = 'Error on server while deleting pattern!';
        });
    }

    private deleteMultiPatterns(): void
    {
        this.httpService.post(MreClassifierRouting.actionDeleteMultiPatterns(), {ids: this.mrePatternsChecked}).then(data => {
            this.mrePatterns = this.mrePatterns
                .filter(el => this.mrePatternsChecked.indexOf(el.classifier_pattern_id) === -1);

            this.unCheckAllPatterns();

            this.successText = 'Checked patterns was deleted successfully!';
        }, err => {
            this.errorText = 'Error on server while deleting multi patterns!';
        });
    }

    private updateMultiPatterns(toggleActive): void
    {
        if (toggleActive) {
            this.mrePatternModel.test = null;
            this.mrePatternModel.text = null;
        }

        this.httpService.post(MreClassifierRouting.actionUpdateMultiPatterns(), {model: this.mrePatternModel, ids: this.mrePatternsChecked}).then(data => {
            this.mrePatterns = this.mrePatterns.map(function (el) {
                if (this.checkedPatternsArray.indexOf(el.classifier_pattern_id) !== -1) {
                    if (!toggleActive) {
                        el.text = this.model.text;
                        el.test = this.model.test;
                    }
                    el.active = this.model.active;
                }

                return el;
            }, {model: this.mrePatternModel, checkedPatternsArray: this.mrePatternsChecked});

            this.unCheckAllPatterns();
            this.successText = 'Checked patterns was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating multi patterns!';
        });
    }

    private resetPatterns(): void
    {
        this.mrePatterns = [];
        this.mrePatternModel = new MrePatternModel();
        this.mrePatternModel.classifier_id = this.mreClassModel.classifier_id;
        this.mrePatternModel.classifier_class_id = this.mreClassModel.classifier_class_id;
    }

    public deactivatePattern(): void
    {
        this.mrePatternModel.active = 0;
        this.updatePattern(true);
    }

    public activatePattern(): void
    {
        this.mrePatternModel.active = 1;
        this.updatePattern(true);
    }

    public checkAllPatterns(): void
    {
        for (let i = 0; i < this.mrePatterns.length; i++) {
            this.mrePatternsChecked.push(this.mrePatterns[i].classifier_pattern_id);
        }
    }

    public unCheckAllPatterns(): void
    {
        this.mrePatternsChecked = [];
    }

    public testPattern(successAlert: boolean = true): boolean
    {
        let testStatus = this.mrePatternModel.test.match(this.mrePatternModel.text);

        if (testStatus) {
            if (successAlert) {
                alert('Success. The regular expression and string entered is match.');
            }

            return true;
        }

        alert('Error. The regular expression and string entered do not match.');
        return false;
    }

    private filterClassifierListAfterDelete(): void
    {
        this.mreClassifiers = this.mreClassifiers
            .filter(x => x.classifier_id != this.mreClassifierModel.classifier_id);
    }

    private filterClassifierClassesListAfterDelete(): void
    {
        this.mreClasses = this.mreClasses
            .filter(x => x.classifier_class_id != this.mreClassModel.classifier_class_id);
    }

    private resetClassifierModel(): void
    {
        this.mreClassifierModel = new MreClassifierModel();
        this.unCheckAllPatterns();
    }
}
