import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { SubstitutesComponent }          from "./substitutes.component";
import { SharedModule }                  from "../shared";

const routing: ModuleWithProviders = RouterModule.forChild([
    {
        path      : 'substitutes',
        component : SubstitutesComponent
    }
]);

@NgModule({
    imports: [
        routing,
        SharedModule
    ],
    declarations: [
        SubstitutesComponent
    ]
})

export class SubstitutesModule {}