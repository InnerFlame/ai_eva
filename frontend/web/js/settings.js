function updateSettings()
{
    $.ajax({
        url: '/settings/settings',
        success: function (data) {
            //Update settings
            $('#settingsAjax').html(data);
            //Make settings sortable (by drag&drop)
            $("#expanded-table tbody").sortable({
                axis: 'y',
                items: 'tr:not(.expanded-table-header, .expanded-table-footer)',
                cancel: ".current",
                start: function () {},
                stop: function () {
                    var elements = [];
                    var i = 0
                    $('#expanded-table tbody tr:not(.expanded-table-header, .expanded-table-footer)').each(function () {
                        var elem = {};
                        elem.id = $(this).find('th span input').val();
                        elem.priority = i;
                        elements.push(elem);
                        i++;
                    });
                    $.ajax({
                        url: '/settings/update-settings-priority',
                        type: "POST",
                        data: 'params=' + JSON.stringify(elements),
                        success: function (data) {
                            if (data != 'true') {
                                alert('An error occurred while updating settings priority!');
                            }
                        }
                    });
                }
            });
        }
    });
};

$(document).ready(function () {
    updateSettings();

    $(document).on('click', '#expanded-table div[name="settingsControlProfile"] ul li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#expanded-table div[name="settingsControlProfile"] button input').val(val);
        $('#expanded-table div[name="settingsControlProfile"] button span:eq(0)').html(name);
    });
    $(document).on('click', '#expanded-table div[name="settingsTargetProfile"] ul li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#expanded-table div[name="settingsTargetProfile"] button input').val(val);
        $('#expanded-table div[name="settingsTargetProfile"] button span:eq(0)').html(name);
    });

    //Modal add new setting functionality
    $(document).on('click', '#addNewSettingModal div[id="addNewSettingControlProfile"] ul li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#addNewSettingModal div[id="addNewSettingControlProfile"] button input').val(val);
        $('#addNewSettingModal div[id="addNewSettingControlProfile"] button span:eq(0)').html(name);
    });
    $(document).on('click', '#addNewSettingModal div[id="addNewSettingTargetProfile"] ul li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#addNewSettingModal div[id="addNewSettingTargetProfile"] button input').val(val);
        $('#addNewSettingModal div[id="addNewSettingTargetProfile"] button span:eq(0)').html(name);
    });
    //Disable 'Add' button when setting name is empty
    if ($('#addNewSettingName').val() === '') {
        $('#addNewSettingButton').attr('disabled', true);
    } else {
        $('#addNewSettingButton').attr('disabled', false);
    }
    $(document).on('input', '#addNewSettingName', function () {
        if ($(this).val() === '') {
            $('#addNewSettingButton').attr('disabled', true);
        } else {
            $('#addNewSettingButton').attr('disabled', false);
        }
    });

    //Click 'Add new setting' button
    $(document).on('click', '#addNewSettingButton', function () {
        var settingName = $('#addNewSettingName').val();
        var controlProfile = $('#addNewSettingControlProfile span[name="name"]').html();
        var targetProfile = $('#addNewSettingTargetProfile span[name="name"]').html();
        $.ajax({
            url: '/settings/create-setting',
            type: "POST",
            data: 'name=' + settingName + '&controlProfile=' + controlProfile + '&targetProfile=' + targetProfile,
            success: function (data) {
                if (data == 'true') {
                    updateSettings();
                } else {
                    alert('An error occurred while creating the setting!');
                }
                $('#addNewSettingModal').modal('hide');
            }
        });
    });

    //Click 'Update setting' button
    $(document).on('click', 'button[name="updateSettingButton"]', function () {
        var settingId = $('#expanded-table input[name="settingsId"]').val();
        var settingName = $('#expanded-table input[name="settingsName"]').val();
        var projects = checkboxesToArray('settingsProjects');
        var controlProfile = $('#expanded-table div[name="settingsControlProfile"] button span:eq(0)').html();
        var targetProfile = $('#expanded-table div[name="settingsTargetProfile"] button span:eq(0)').html();
        var countries = checkboxesToArray('settingsCountries');
        var locales = checkboxesToArray('settingsLocales');
        var statuses = checkboxesToArray('settingsStatus');
        var sites = checkboxesToArray('settingsSite');
        var trafficSources = checkboxesToArray('settingsTrafficSource');
        var actionWays = checkboxesToArray('settingsActionWay');
        var trafficTypes = checkboxesToArray('settingsTrafficType');
        var platforms = checkboxesToArray('settingsPlatform');

        var fanClubs = checkboxesToArray('settingsFanClub');
        var onlines = checkboxesToArray('settingsOnline');

        var scenariosObjects = $('#expanded-table div[name="settingsScenario"] ul li.sortable');
        var arr = [];
        for (var i = 0; i < scenariosObjects.size(); i++) {
            var scenarioId = $(scenariosObjects[i]).find('input[type="hidden"]').val();
            var scenarioIsFixed = $(scenariosObjects[i]).find('input[type="checkbox"]').is(':checked');
            arr.push({'id': scenarioId, 'isFixed': scenarioIsFixed});
        };
        var scenarios = JSON.stringify(arr);

        var active = $('#expanded-table button[name="settingsActiveRadioButton"]').hasClass('active');

        $.ajax({
            url: '/settings/update-setting',
            type: "POST",
            data:
                'id=' + settingId +
                '&name=' + settingName +
                '&projects=' + projects +
                '&controlProfile=' + controlProfile +
                '&targetProfile=' + targetProfile +
                '&countries=' + countries +
                '&locales=' + locales +
                '&statuses=' + statuses +
                '&sites=' + sites +
                '&trafficSources=' + trafficSources +
                '&actionWays=' + actionWays +
                '&trafficTypes=' + trafficTypes +
                '&platforms=' + platforms +
                '&fanClubs=' + fanClubs +
                '&onlines=' + onlines +
                '&scenarios=' + scenarios +
                '&active=' + active,
            success: function (data) {
                if (data == 'true') {
                    updateSettings();
                } else {
                    alert('An error occurred while updating the setting!');
                }
            }
        });
    });

    //Click 'Cancel setting' button
    $(document).on('click', 'button[name="cancelSettingButton"]', function () {
        collapseExpandedItem();
    });

    //Click setting delete button
    $(document).on('click', 'button[name="deleteSettingButton"]', function () {
        var settingId = $('#expanded-table input[name="settingsId"]').val();
        var settingName = $('#expanded-table input[name="settingsName"]').val();
        $('#deleteSettingId').val(settingId);
        $('#deleteSettingName').html(settingName);
        $('#confirmDeleteSettingModal').modal('show');
    });

    //Click setting copy button
    $(document).on('click', 'button[name="copySettingButton"]', function () {
        var settingId = $('#expanded-table input[name="settingsId"]').val();
        $.ajax({
            url: '/settings/copy-setting',
            type: "POST",
            data: 'id=' + settingId,
            success: function (data) {
                if (data == 'true') {
                    updateSettings();
                } else {
                    alert('An error occurred while copying setting!');
                }
            }
        });
    });

    //Click confirm topic delete button
    $(document).on('click', '#confirmDeleteSettingButton', function () {
        var settingId = $('#deleteSettingId').val();
        $.ajax({
            url: '/settings/delete-setting',
            type: "POST",
            data: 'id=' + settingId,
            success: function (data) {
                if (data == 'true') {
                    updateSettings();
                } else {
                    alert('An error occurred while deleting the setting!');
                }
                $('#confirmDeleteSettingModal').modal('hide');
            }
        });
    });

    //Click add scenario to panel button
    $(document).on('click', 'input[name="addScenarioToPanel"]', function () {
        var scenarioId = $(this).closest('a').siblings('input').val();
        var scenarioName = $(this).val();
        var scenarioLi = '<li class="sortable"><a href="javascript:void(0);">' +
            '<span class="number"></span><span class="glyphicon glyphicon-trash deleteScenarioFromPanel"></span> ' +
            scenarioName +
            '<input type="hidden" value="' +
            scenarioId +
            '"><input type="checkbox" aria-label="..." checked="checked"></a></li>';
        $('#expanded-table div[name="settingsScenario"] ul li.divider').before(scenarioLi);
        recalculateScenariosIndexNumbers();
    });

    //Click delete scenario from panel button
    $(document).on('click', '#expanded-table div[name="settingsScenario"] ul span.deleteScenarioFromPanel', function () {
        $(this).closest('li').remove();
        recalculateScenariosIndexNumbers();
    });

    //Click "Check all"
    $(document).on('click', '#expanded-table ul li.checkAll', function () {
        var isChecked = ($(this).siblings('li').first().find('a input[type="checkbox"]:checked').length > 0);
        $(this).siblings('li:visible').find('a input[type="checkbox"]').prop('checked', !isChecked);
    });

    //Click "Check all scenarios"
    $(document).on('click', '#expanded-table ul li.checkAllScenarios', function () {
        var isChecked = ($(this).siblings('li').next().find('a input[type="checkbox"]:checked').length > 0);
        $(this).siblings('li:visible').find('a input[type="checkbox"]').prop('checked', !isChecked);
    });

    //Live search functionality
    $(document).on('keyup', 'input.livesearch', function () {
        var pattern = $(this).val().toLowerCase();
        $(this).siblings('li:not(.checkAll)').each(function (i) {
            var text = $(this).contents().eq(0).text().toLowerCase();
            if (text.indexOf(pattern) == -1 && !$(this).first().find('a input[type="checkbox"]:checked').length > 0) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });

    //Extended live search functionality
    $(document).on('keyup', 'input.extendedLivesearch', function () {
        var patternStr  = $(this).val().toLowerCase();
        var patterns    = patternStr.split(' ');

        $(this).siblings('li:not(.checkAll)').each(function (i) {
            var text        = $(this).contents().eq(0).text().toLowerCase();
            var show        = false;

            patterns.forEach(function(item) {
                if (
                    item == '' || (
                        item != '' &&
                        (text.indexOf(item) != -1)
                    )
                ) {
                    show = true;
                }
            });

            if (show) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    });

    //Extended buttons live search functionality
    $(document).on('keyup', 'input.extendedButtonsLivesearch', function () {
        var patternStr  = $(this).val().toLowerCase();
        var patterns    = patternStr.split(' ');

        $(this).siblings('li.livesearchButton').each(function (i) {
            var text        = $(this).find('input[name="addScenarioToPanel"]').val().toLowerCase();
            var show        = false;

            patterns.forEach(function(item) {
                if (item == '' || (item != '' && text.indexOf(item) != -1)) {
                    show = true;
                }
            });

            if (show) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    });

    // Prevent closing dropdown when click on it or 'Check all'
    $(document).on('click', '#expanded-table .dropdown-menu', function (e) {
        e.stopPropagation();
    });

    //Click 'Combine settings' button
    $(document).on('click', '#combineSettings', function () {
        $.ajax({
            url: '/settings/combine-settings',
            type: "GET",
            success: function (data) {
                if (data != 'true') {
                    alert('An error occurred while combined settings!');
                }
                $('#combineSettingsModal').modal('hide');
            }
        });
    });
});

function checkboxesToArray(divName)
{
    var objects = $('#expanded-table div[name="' + divName + '"] ul li a input[type="checkbox"]:checked').closest('li').find('input[type="hidden"]');
    var arr = [];
    for (var i = 0; i < objects.size(); i++) {
        arr.push($(objects[i]).val());
    };
    return arr;
};

function clickExpandedTableItemCallback(item)
{
    //Set id
    var settingsId = $(item).find('th span span').html();
    $('#expanded-table input[name="settingsId"]').val(settingsId);
    //Set name
    var settingsName = $(item).find('td:eq(0) span span').html();
    $('#expanded-table input[name="settingsName"]').val(settingsName);
    //Set projects
    $(item).find('td:eq(1) span input').each(function () {
        $('#expanded-table div[name="settingsProjects"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set control profile
    var controlProfileVal = $(item).find('td:eq(2) span input').val();
    var controlProfile = $('#expanded-table div[name="settingsControlProfile"] ul li input[value="' + controlProfileVal + '"]').siblings('a').html();
    $('#expanded-table div[name="settingsControlProfile"] button input').val(controlProfileVal);
    $('#expanded-table div[name="settingsControlProfile"] button span:eq(0)').html(controlProfile);
    //Set target profile
    var targetProfileVal = $(item).find('td:eq(3) span input').val();
    var targetProfile = $('#expanded-table div[name="settingsTargetProfile"] ul li input[value="' + targetProfileVal + '"]').siblings('a').html();
    $('#expanded-table div[name="settingsTargetProfile"] button input').val(targetProfileVal);
    $('#expanded-table div[name="settingsTargetProfile"] button span:eq(0)').html(targetProfile);
    //Set countries
    $(item).find('td:eq(4) span input').each(function () {
        $('#expanded-table div[name="settingsCountries"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set locales
    $(item).find('td:eq(5) span input').each(function () {
        $('#expanded-table div[name="settingsLocales"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set status
    $(item).find('td:eq(6) span input').each(function () {
        $('#expanded-table div[name="settingsStatus"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set site
    $(item).find('td:eq(7) span input').each(function () {
        $('#expanded-table div[name="settingsSite"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
        $('#expanded-table div[name="settingsSite"] ul li').first().after(
            $('#expanded-table div[name="settingsSite"] ul li input[value="' + $(this).val() + '"]').closest('li')
        );
    });
    //Set traffic source
    $(item).find('td:eq(8) input').each(function () {
        $('#expanded-table div[name="settingsTrafficSource"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set action way
    $(item).find('td:eq(9) span input').each(function () {
        $('#expanded-table div[name="settingsActionWay"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set traffic type
    $(item).find('td:eq(10) span input').each(function () {
        $('#expanded-table div[name="settingsTrafficType"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set platform
    $(item).find('td:eq(11) span input').each(function () {
        $('#expanded-table div[name="settingsPlatform"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set fan club
    $(item).find('td:eq(12) span input').each(function () {
        $('#expanded-table div[name="settingsFanClub"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set online
    $(item).find('td:eq(13) span input').each(function () {
        $('#expanded-table div[name="settingsOnline"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set scenario
    $(item).find('td:eq(14) span input[name="hidden_1"]').each(function () {
        //$('#expanded-table div[name="settingsScenario"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
        var scenarioId = $(this).val();
        var scenarioName = $(this).siblings('span').html();
        var scenarioIsFixed = $(this).siblings('input[name="hidden_2"]').val();
        var scenarioLi = '<li class="sortable"><a href="javascript:void(0);">' +
            '<span class="number"></span><span class="glyphicon glyphicon-trash deleteScenarioFromPanel"></span> ' +
            scenarioName +
            '<input type="hidden" value="' +
            scenarioId +
            '"><input type="checkbox" aria-label="..."';
        if (scenarioIsFixed == true) {//scenarioIsFixed is string and if we write if (scenarioIsFixed) it always be true
            scenarioLi += ' checked';
        }
        scenarioLi += '></a></li>';
        $('#expanded-table div[name="settingsScenario"] ul li.divider').before(scenarioLi);
    });
    //Make scenarios sortable
    $('#expanded-table div[name="settingsScenario"] ul').sortable({
        start: function () {},
        stop: function () {
            recalculateScenariosIndexNumbers();
        }
    });
    //Set active
    if ($(item).find('td:eq(15) span input').val() == "1") {
        $('#expanded-table button[name="settingsActiveRadioButton"]').click();
    }
    recalculateScenariosIndexNumbers();
}

// Recalculate and display index numbers of scenarios
function recalculateScenariosIndexNumbers()
{
    $('#expanded-table div[name="settingsScenario"] ul').find('li.sortable').each(function(i){
        $(this).find('span.number').text((i + 1) + '. ');
    });
}
