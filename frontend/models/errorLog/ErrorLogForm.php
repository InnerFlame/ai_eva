<?php
declare(strict_types=1);

namespace frontend\models\errorLog;

use frontend\models\BaseForm;
use yii\data\ActiveDataProvider;

class ErrorLogForm extends BaseForm
{
    public $provider;

    public function __construct()
    {
        $query = Log::find();
        $this->provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
    }

    public function clearLog()
    {
        Log::deleteAll();
    }

    public static function getCount()
    {
        return Log::find()->count();
    }
}
