import {ModuleWithProviders, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SharedModule} from "../shared";
import {TopicsComponent} from "./topics.component";
import {DragulaModule} from 'ng2-dragula';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import { TopicsUpdateModalComponent } from './topics.update.modal/topics.update.modal.component';

const routing: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'topics-ng',
        component: TopicsComponent
    }
]);

@NgModule({
    imports: [
        routing,
        SharedModule,
        DragulaModule,
        MultiselectDropdownModule
    ],
    declarations: [
        TopicsComponent,
        TopicsUpdateModalComponent
    ]
})

export class TopicsModule {}