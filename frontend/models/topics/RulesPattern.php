<?php
declare(strict_types=1);

namespace frontend\models\topics;

class RulesPattern
{
    private $patternId;
    private $ruleId;
    private $text;
    private $deleted;
    private $test;
    private $version;
    private $active;

    /**
     * RulesPattern constructor.
     * @param $patternId
     * @param $ruleId
     * @param $text
     * @param $deleted
     * @param $test
     * @param $version
     * @param $active
     */
    public function __construct(
        int $patternId,
        int $ruleId,
        string $text,
        bool $deleted,
        string $test,
        int $version,
        bool $active
    ) {
        $this->patternId = $patternId;
        $this->ruleId = $ruleId;
        $this->text = $text;
        $this->deleted = $deleted;
        $this->test = $test;
        $this->version = $version;
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getPatternId() : int
    {
        return $this->patternId;
    }

    /**
     * @param int $patternId
     */
    public function setPatternId(int $patternId)
    {
        $this->patternId = $patternId;
    }

    /**
     * @return int
     */
    public function getRuleId() : int
    {
        return $this->ruleId;
    }

    /**
     * @param int $ruleId
     */
    public function setRuleId(int $ruleId)
    {
        $this->ruleId = $ruleId;
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return string
     */
    public function getTest() : string
    {
        return $this->test;
    }

    /**
     * @param string $test
     */
    public function setTest(string $test)
    {
        $this->test = $test;
    }

    /**
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }
}
