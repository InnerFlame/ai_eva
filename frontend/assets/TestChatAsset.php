<?php

namespace frontend\assets;

class TestChatAsset extends BasicAsset
{
    public $css = [
        'css/testChat.css',
    ];
    public $js = [
        'js/testChat.js',
    ];
}
