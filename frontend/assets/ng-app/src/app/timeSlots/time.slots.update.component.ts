import { Component }              from '@angular/core';
import { Title }                  from "@angular/platform-browser";
import { HttpService }    from "../shared/services/http.service";
import { OnInit }                 from '@angular/core';
import { TimeSlotsUpdateModel }   from "../shared";
import { ActivatedRoute, Params } from "@angular/router";
import { IMyDpOptions }           from 'mydatepicker';

@Component({
    selector    : 'timeSlotsUpdate',
    templateUrl : './time.slots.update.html',
})

export class TimeSlotsUpdateComponent implements OnInit
{
    public timeSlotModel        : TimeSlotsUpdateModel;
    public errorText            : string  = '';
    public successText          : string  = '';
    public displayLoadingBlock  : boolean = true;

    public myDatePickerDateOptions: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
    };

    public timeMask = [/\d/, /\d/, ':', /\d/, /\d/, ':00'];

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param activatedRoute
     * @param titleService
     */
    constructor(private httpService: HttpService, private activatedRoute: ActivatedRoute, private titleService: Title)
    {
        this.titleService.setTitle( 'Update Timeslot' );
    }

    /**
     * On init script actions and events, here we bind data for update
     */
    ngOnInit(): void
    {
        this.activatedRoute.params.subscribe((params: Params) => {
            let timeslotId = params['id'];

            this.httpService.get(TimeSlotsUpdateModel.getTimeslotDataApiUrl(timeslotId)).then(data => {
                if (!data) {
                    this.errorText = 'Something goes wrong with response!';
                } else {
                    this.bindInitialData(data);
                    this.displayLoadingBlock = false;
                }
            });
        });
    }

    bindInitialData(data: any): void
    {
        let preparedDate1: any = '';
        let preparedDate2: any = '';

        if (data.date1 && data.date2) {
            preparedDate1 = data.date1.substring(0, 10).split('-');
            preparedDate2 = data.date2.substring(0, 10).split('-');

            preparedDate1 = {'date': {
                'year': parseInt(preparedDate1[0]), 'month': parseInt(preparedDate1[1]), 'day': parseInt(preparedDate1[2])}
            };
            preparedDate2 = {'date': {
                'year': parseInt(preparedDate2[0]), 'month': parseInt(preparedDate2[1]), 'day': parseInt(preparedDate2[2])}
            };
        }

        this.timeSlotModel = new TimeSlotsUpdateModel(
            data.timeslot_id,
            data.name,
            data.time1,
            data.time2,
            preparedDate1,
            preparedDate2
        );
    }

    /**
     * Submit form handler
     */
    public onSubmit(): void
    {
        this.errorText           = '';
        this.successText         = '';
        this.displayLoadingBlock = true;

        if (!this.isValidDateAndTimeInputs()) {
            this.displayLoadingBlock = false;
            return;
        }

        const body = { formData: JSON.stringify(this.timeSlotModel) };

        this.httpService.post(TimeSlotsUpdateModel.getTiemslotsUpdateApiUrl(), body)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.errorText = 'Something goes wrong wit test response or its empty...';
                } else {
                    this.successText = 'Timeslot was updated successfully!';
                }

                this.displayLoadingBlock = false;
            });
    }

    public isValidDateAndTimeInputs(): boolean
    {
        if (this.timeSlotModel.date1 != '' && this.timeSlotModel.date2 == '') {
            this.errorText = 'Date 1 is invalid or you don\'t specify Date 2';
            return false;
        }

        if (this.timeSlotModel.date2 != '' && this.timeSlotModel.date1 == '') {
            this.errorText = 'Date 1 is invalid or you don\'t specify Date 2';
            return false;
        }


        let filteredTime1 = this.timeSlotModel.time1.replace('_', '');
        let filteredTime2 = this.timeSlotModel.time2.replace('_', '');

        if ((filteredTime1.length < 8) || (filteredTime1 == '00:00:00')) {
            this.errorText = 'Time 1 is incorrect';
            return false;
        }

        if ((filteredTime2.length < 8) || (filteredTime2 == '00:00:00')) {
            this.errorText = 'Time 2 is incorrect';
            return false;
        }

        return true;
    }
}
