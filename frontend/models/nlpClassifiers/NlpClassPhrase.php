<?php
declare(strict_types=1);

namespace frontend\models\nlpClassifiers;

class NlpClassPhrase
{
    public $id;
    public $classId;
    public $text;
    public $entities = [];

    public function __construct(int $id, int $classId, string $text)
    {
        $this->id           = $id;
        $this->classId      = $classId;
        $this->text         = $text;
    }
}
