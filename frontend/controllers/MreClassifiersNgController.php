<?php

namespace frontend\controllers;

use frontend\controllers\traits\ClassifiersTrait;
use frontend\models\mreClassifiers\MreClassifiersNgForm;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class MreClassifiersNgController
 * @package frontend\controllers
 */
class MreClassifiersNgController extends BaseNgController
{
    use ClassifiersTrait;

    /**
     * @inheritdoc
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new MreClassifiersNgForm());
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('scenarios')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /*
    |--------------------------------------------------------------------------
    | Classifiers
    |--------------------------------------------------------------------------
    */

    public function actionGetClassifiers(): array
    {
        return [
            'classifiers'     => $this->getFormModel()->getClassifiers(),
            'availableModels' => $this->getClassifiersAvailableModels()
        ];
    }

    public function actionCreateClassifier(): array
    {
        $createdData = $this->getFormModel()->createClassifier($this->requestData);

        return $createdData;
    }

    public function actionUpdateClassifier(): array
    {
        $updateData = $this->getFormModel()->updateClassifier($this->requestData);

        return $updateData;
    }

    public function actionDeleteClassifier(): array
    {
        $deleteData = $this->getFormModel()->deleteClassifier($this->requestData);

        return $deleteData;
    }

    /*
    |--------------------------------------------------------------------------
    | Classes
    |--------------------------------------------------------------------------
    */

    public function actionGetClasses(string $classifierId): array
    {
        $classesData  = $this->getFormModel()->getClasses((int) $classifierId);

        return $classesData;
    }

    public function actionCreateClass(): array
    {
        $createData = $this->getFormModel()->createClass($this->requestData);

        return $createData;
    }

    public function actionUpdateClass(): array
    {
        $updateData = $this->getFormModel()->updateClass($this->requestData);

        return $updateData;
    }

    public function actionDeleteClass(): array
    {
        $deleteData = $this->getFormModel()->deleteClass($this->requestData);

        return $deleteData;
    }

    /*
    |--------------------------------------------------------------------------
    | Patterns
    |--------------------------------------------------------------------------
    */

    public function actionGetPatterns(string $classId): array
    {
        return $this->getFormModel()->getPatterns((int) $classId);
    }

    public function actionCreatePattern(): array
    {
        return $this->getFormModel()->createPattern($this->requestData);
    }

    public function actionUpdatePattern(): array
    {
        return $this->getFormModel()->updatePattern($this->requestData);
    }

    public function actionUpdateMultiPatterns(): array
    {
        return $this->getFormModel()->updateMultiPatterns($this->requestData);
    }

    public function actionDeletePattern(): array
    {
        return $this->getFormModel()->deletePattern($this->requestData);
    }

    public function actionDeleteMultiPatterns(): array
    {
        return $this->getFormModel()->deleteMultiPatterns($this->requestData);
    }

    protected function getFormModel(): MreClassifiersNgForm
    {
        return $this->formModel;
    }
}
