import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { TestPhrasesComponent }          from './test.phrases.component';
import { SharedModule }                  from "../shared";

const phrasesRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'test-phrases',
        component: TestPhrasesComponent
    }
]);

@NgModule({
    imports: [
        phrasesRouting,
        SharedModule
    ],
    declarations: [
        TestPhrasesComponent
    ]
})

export class TestPhrasesModule {}