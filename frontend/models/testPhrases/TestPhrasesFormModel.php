<?php

namespace frontend\models\testPhrases;

use frontend\models\BaseNgForm;

/**
 * Class TestPhrasesFormModel
 * @package frontend\models\testPhrases
 */
class TestPhrasesFormModel extends BaseNgForm
{
    /**
     * @return array
     */
    public function getClassifiersStatus(): array
    {
        return $this->getBrungildaApi()->getClassifiersStatus();
    }

    /**
     * @param $text
     * @param $model
     * @param $project
     * @return mixed
     */
    public function getResponseTestResults(string $text, string $model, string $project): array
    {
        $response = $this->getBrungildaApi()->testClassifierModel($model, $text, $project);

        return $response;
    }
}
