<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class Scenario extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_scenarios_table';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
