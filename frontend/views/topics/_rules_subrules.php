<?php
use common\widgets\BindableFormItems\BindableFormItems;

$subrulesItems = array();
foreach ($model->rule->getRules() as $subrule) {
    $item['images'] = [
        [
            'class' => 'glyphicon glyphicon-signal' . ($subrule->isActive() ? ' active' : ''),
            'name' => 'activeSubRulesIcon',
        ],
        [
            'class' => 'glyphicon glyphicon-off' . ($subrule->isEnabled() ? ' active' : ''),
            'name' => 'onSubRulesIcon',
        ],
        [
            'class' => 'glyphicon glyphicon-ok' . (($subrule->getExcludeTemplate() == 1) ? ' active' : ''),
            'name' => 'excSubRulesIcon',
        ],
    ];
    $item['text'] = $subrule->getName();
    $item['hiddens'] = [
        ['name' => 'subRuleId', 'value' => $subrule->getRuleId()],
        ['name' => 'parentId', 'value' => $subrule->getParentId()],
        ['name' => 'subRulePriority', 'value' => $subrule->getPriority()],
        ['name' => 'itemExecution', 'value' => ($subrule->getExecLimit()) == null ? 0 : $subrule->getExecLimit()],
    ];
    $subrulesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'subRulesBindableForm',
    'items' => $subrulesItems,
]);
