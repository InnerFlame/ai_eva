<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\nlpClassifiers\NlpClassifiersForm;
use yii\web\ForbiddenHttpException;

class NlpClassifiersController extends BaseController
{
    protected $model;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new NlpClassifiersForm();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('scenarios')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        return $this->render('index', ['model' => $this->model]);
    }

    public function actionClassifiers()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getClassifiers();
                return $this->renderAjax('_classifiers', ['model' => $this->model]);
            }
        );
    }

    public function actionRootClassifiers()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getRootClassifiers();
                return $this->renderAjax('_rootClassifiers', ['model' => $this->model]);
            }
        );
    }

    public function actionCreateClassifiers()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->createNlpClassifier($post['text'], $this->model::CLASSIFIERS_TYPES[0]);
            }
        );
    }

    public function actionCreateRootClassifiers()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->createNlpClassifier($post['text'], $this->model::CLASSIFIERS_TYPES[1]);
            }
        );
    }

    public function actionUpdateClassifiers()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                return $this->model->updateNlpClassifier((int) $post['classifierId'], $post['text'], $post['classifierModel']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateRootClassifiers()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                return $this->model->updateNlpClassifier((int) $post['classifierId'], $post['text'], $post['classifierModel']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeleteClassifiers()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteNlpClassifier((int) $post['classifierId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionClassifiersStatus()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $tmp    = [];
                $models = [];
                $result = $this->model->getNlpClassifiersStatus();
                foreach ($result['models'] as $model) {
                    $data = explode('_', $model);
                    $date = explode('-', $data[1]);
                    $tmp[$date[0]][] = $model;
                }
                krsort($tmp);

                foreach ($tmp as $date => $values) {
                    foreach ($values as $value) {
                        $models[] = $value;
                    }
                }

                $result['models'] = $models;

                return json_encode($result);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionTrainClassifier()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                return $this->model->trainNlpClassifier((int) $post['classifierId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionClassifierClasses()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getClasses((int) $post['classifierId']);
                return $this->renderAjax('_classifierClasses', ['model' => $this->model]);
            }
        );
    }

    public function actionRootClassifierClasses()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getClasses((int) $post['classifierId']);
                return $this->renderAjax('_rootClassifierClasses', ['model' => $this->model]);
            }
        );
    }

    public function actionCreateClassifierClasses()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->createNlpClassifierClass(
                    (int) $post['сlassifierId'],
                    $post['text'],
                    (int) $post['probability']
                );
            }
        );
    }

    public function actionUpdateClassifierClasses()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                return $this->model->updateNlpClassifierClass(
                    (int) $post['classifierClassId'],
                    $post['text'],
                    (int) $post['probability']
                );
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeleteClassifierClasses()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteNlpClassifierClass((int) $post['classifierClassId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionClassPhrases()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getPhrases((int) $post['classId']);

                $phrases  = $this->model->phrases;
                $entities = \Yii::$app->BrungildaApiCurl->getClassifierClassPhrasesEntities((int) $post['classId']);

                    foreach ($phrases as $key => $phrase) {
                        foreach ($entities as $entityData) {
                            if ($entityData['learning_phrase_id'] == $phrase->id) {
                            $phrase->entities[] = $entityData;
                        }
                    }
                }

                return $this->renderAjax('_classPhrases', ['model' => $this->model]);
            }
        );
    }

    public function actionRootClassPhrases()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getPhrases((int) $post['classId']);

                $phrases  = $this->model->phrases;
                $entities = \Yii::$app->BrungildaApiCurl->getClassifierClassPhrasesEntities((int) $post['classId']);

                foreach ($phrases as $key => $phrase) {
                    foreach ($entities as $entityData) {
                        if ($entityData['learning_phrase_id'] == $phrase->id) {
                            $phrase->entities[] = $entityData;
                        }
                    }
                }

                return $this->renderAjax('_rootClassPhrases', ['model' => $this->model]);
            }
        );
    }

    public function actionCreateClassPhrases()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->createNlpClassPhrase(
                    (int) $post['classId'],
                    $post['text']
                );
            }
        );
    }

    public function actionUpdateClassPhrases()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                return $this->model->updateNlpClassPhrase(
                    (int) $post['classPhraseId'],
                    $post['text']
                );
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeleteClassPhrases()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->deleteNlpClassPhrases($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionGetPhraseEntities()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $phraseId = $post['phraseId'];
                $res = $this->model->getEntityForPhrase((int) $phraseId);
                return json_encode($res);
            }
        );
    }

    public function actionAddPhraseEntity()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $data = [
                    'entity'                => $post['entity'],
                    'value'                 => $post['value'],
                    'start'                 => $post['start'],
                    'end'                   => $post['end'],
                    'classifier_class_id'   => $post['classId'],
                    'learning_phrase_id'    => $post['phraseId'],
                ];

                return $this->model->createEntityForPhrase($data);
            }
        );
    }

    public function actionMultiAddPhraseEntity()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $data    = [];
                $phrases = $this->model->getPhrasesIds((int) $post['classId']);

                foreach ($phrases as $id => $text) {
                    if(strpos($text, $post['value']) !== false) {
                        $data[] = [
                            'entity'                => $post['entity'],
                            'value'                 => $post['value'],
                            'start'                 => $post['start'],
                            'end'                   => $post['end'],
                            'classifier_class_id'   => $post['classId'],
                            'learning_phrase_id'    => $id
                        ];
                    }
                }

                return $this->model->createMultiEntityForPhrase($data);
            }
        );
    }

    public function actionUpdatePhraseEntity()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $data = [
                    'learning_phrase_entity_id' => $post['entityId'],
                    'entity'                    => $post['entity'],
                    'value'                     => $post['value'],
                    'start'                     => $post['start'],
                    'end'                       => $post['end'],
                    'classifier_class_id'       => $post['classId'],
                    'learning_phrase_id'        => $post['phraseId'],
                ];

                return $this->model->updateEntityForPhrase($data);
            }
        );
    }

    public function actionMultiUpdatePhraseEntity()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $data    = [];
                $phrases = $this->model->getPhrasesIds((int) $post['classId']);

                foreach ($phrases as $id => $text) {
                    if(strpos($text, $post['value']) !== false) {
                        $phraseEntities = $this->model->getEntityForPhrase($id);
                        foreach ($phraseEntities as $phraseEntity) {
                            if (($phraseEntity['value'] == $post['value'])) {
                                $data[] = [
                                    'learning_phrase_entity_id' => $phraseEntity['learning_phrase_entity_id'],
                                    'entity'                    => $post['entity'],
                                    'value'                     => $post['value'],
                                    'start'                     => $post['start'],
                                    'end'                       => $post['end'],
                                    'classifier_class_id'       => $post['classId'],
                                    'learning_phrase_id'        => $id
                                ];
                            }
                        }
                    }
                }

                $data[] = [
                    'learning_phrase_entity_id' => $post['entityId'],
                    'entity'                    => $post['entity'],
                    'value'                     => $post['value'],
                    'start'                     => $post['start'],
                    'end'                       => $post['end'],
                    'classifier_class_id'       => $post['classId'],
                    'learning_phrase_id'        => $post['phraseId'],
                ];

                return $this->model->updateMultiEntityForPhrase($data);
            }
        );
    }

    public function actionDeletePhraseEntity()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $entityId = $post['entityId'];
                return $this->model->deleteEntityForPhrase((int) $entityId);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionMultiDeletePhraseEntity()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $post = \Yii::$app->request->post();
                $classId     = (int) $post['classId'];
                $entityId    = (int) $post['entityId'];
                $entityValue = (string) $post['value'];

                $result = $this->model->deleteEntityForPhrase($entityId);
                $this->deleteAllEntitiesFromPhrasesByClassId($classId, $entityValue);

                return $result;
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    private function deleteAllEntitiesFromPhrasesByClassId($classId, $entityValue)
    {
        $result      = false;
        $phrases     = $this->model->getPhrasesIds($classId);

        foreach ($phrases as $id => $text) {
            if(strpos($text, $entityValue) !== false) {
                $phraseEntities = $this->model->getEntityForPhrase($id);
                foreach ($phraseEntities as $phraseEntity) {
                    if($phraseEntity['value'] == $entityValue) {
                        $result = $this->model->deleteEntityForPhrase((int) $phraseEntity['learning_phrase_entity_id']);
                    }
                }
            }
        }

        return $result;
    }
}
