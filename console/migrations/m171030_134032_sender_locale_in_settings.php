<?php

use yii\db\Migration;

class m171030_134032_sender_locale_in_settings extends Migration
{
    public function safeUp()
    {
        $this->createTable('settings_sender_locales', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'locale_name' => $this->string(3)->defaultValue('DEF'),
        ]);
        $this->addColumn('consolidatedSettings', 'sender_locale_name', $this->string(3)->defaultValue('DEF')->after('locale_name'));
        $this->execute('INSERT INTO settings_sender_locales (setting_id, locale_name) SELECT id, \'DEF\' FROM settings_table');
    }

    public function safeDown()
    {
        $this->dropTable('settings_sender_locales');
    }
}
