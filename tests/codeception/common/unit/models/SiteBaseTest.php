<?php
namespace tests\codeception\common\unit\models;

use Yii;
use tests\codeception\common\unit\TestCase;

class SiteBaseTest extends TestCase
{
    public $yaml;
    public $params;

    public function setUp()
    {
        parent::setUp();

        $this->params = array_merge(
            require(__DIR__ . '/../../../../../common/config/settings.php'),
            require(__DIR__ . '/../../../../../common/config/projectBySiteId.php')
        );

        $this->yaml = "- k:
    siteId: 7744c2c8769e4b45a325190205b66420 # lovefordate.com
  v: compliance sites

- k:
    siteId:
      - 63a81f205e24ea9c43b14454ec884429 # becoquin.com
      - bb1496f215dfa945152799e06bb81d0d # becoquin-app.com
      - 8a4fe96d3f91836cf75d32fc64947860 # becoquin.fr
      - 2b5035a6205bb474ed68015a084fd04c # benaughty.co.uk
      - 3c09509e33441cf21f9b1cc3a49413f2 # benaughty.com
      - ea3b54e41ec2d956e65ae68c2b2cde73 # mate4date.com
      - 44351df02be85047aefaede44322e44d # naughtythai.com
      - 3ed0b15e7dfb11e3ac4dd4bed9a9456d # hornyasia.com
      - 3ed0b1687dfb11e3ac4dd4bed9a9456d # sugarbbw.com
      - c425af0343ed96267736cc2f801cb07f # lovelegia.com
      - 5bd9e0af97e4e3652f7da0f2662577ec # 4lovemate.com
      - 9ebfe66349b85463d7dca5b1ccac482e # wuvmates.com
      - fd775361e720c8a1628c6c510c458721 # mycouplesearch.com
      - 07d0b35be92c83fe2bb330f0eb4369ec # winamate.com
      - a0087de96ea51b62237d26f0f9f254db # tallylove.com
      - 8e4d8cc295ccca5c193cd6210862e843 # love2wonder.com
      - db2944d18b08d062d5579d26813d4f52 # choosemehere.com
      - 2044ff77f4a2b0223552967a9d3db6da # 4everwithyou.com
      - 5d325027916e7283ede6d4c3efabb601 # withu4ever.com
      - b5129e7423c96de6624a643d4bd0f185 # dazzymeet.com
      - dd6d741767af3c490c2f23db2bc2553f # flirtysparks.com
      - 3e7aa89bf6f9ff4de756c17bf67a39e8 # flirtymagic.com
      - 4b4559eea7fd66e075bd2e7d0b4f678c # bemycrush.com
      - bebbe2368ed21d2a882f481182c79448 # localsseekdates.com
      - 6885350cc3e09ce55f4d9eb1d19fe5ce # easylovetime.com
      - 2fea93bba56142affb2bfa9878c376a4 # amour4ever.com
      - 31527e7682f1144990c32d6c97a0a3dc # loveisage.com
      - 22cfebc4ade430d82caa6e6522b36731 # bemyfate.com
      - e95d69b5c0ecfd37bfdd1b5e865f1fca # yniwota.com
      - b12db32dbf420d86813c68432897c6a3 # putongai.com
      - 4391ee59d98e59d56faa692341659a19 # benaughty.cz
      - 1cc1458917ece460e5b52ee7c493c06c # benaughty.dk
      - e7c09f76872331ae6256b00ed5c2e593 # benaughty.no
      - 3f37485e2dd02830b80ddfdc50c7f96f # benaughty.se
      - fa8013b144f11a143db1e6a60c879e70 # cheekylovers.com
      - b407ac145d90039eb71e39edf1f8d086 # clickandflirt-app.com
      - c75a229e34a4d484845c196f82b1df6c # clickandflirt.com
      - bd0bc6cc53479bbb60f106d1c1fa7da6 # contactosrapidos.com
      - 07b4b7d8dcf411e4ae56a6b599de9034 # desirdunsoir.com
      - 290c083fcec0b192469be40facddb7f3 # divierteteligando.com
      - d8c4b4f5fdcb0bd217ab8c9698c757c5 # ebonyflirt.com
      - b43bce24d28527adcacb9565f4f000b2 # flingpa1blunk.com
      - c47566362bf21304b8ec57e43804d5ca # flirt.com
      - d6f3b5a3338fe50deddd8fc584de40fe # flirt.no
      - 96ab27d69104d8000ebc6e7eec04965d # flirt-online-site.com
      - f2ab4e821ab65f0d95f457cc514b1ee3 # flirt.fr
      - 1e56618be3b87a23b0b6c952c9b4fe29 # flirtsenzalimiti.com
      - 374c280b6a359e6e5aae4f3c6c4dd906 # flirtspielchen.ch
      - 1ad28d5d489204ddbca7b21475d6b7be # flirtspielchen.de
      - fbc6681910f0abfb5f111f549a813692 # flirtydesires-app.com
      - 83d9e97e0922adac76d66ffe07c77011 # flirtydesires.com
      - 0be1fa017efd1cde9bae0a5623657029 # frechespiele.de
      - f069c917f0463731bdc5ddf947fc645d # gibsmir.at
      - f9b0b8a68096fa8fbdd81983563a3e70 # gibsmir.de
      - b032827d5443f19199cc5c82a5a2e09e # hellohotties-app.com
      - d3a76c15116d741a140ce093fbfec3e7 # hellohotties.com
      - 0dc5d5b8089350bdf03a69fa9dd1eae3 # hurtigflirt.com
      - 43fc03eaf48411e2baf44b331f04b47e # idhandleit.com
      - 7ccdbbe114315416055d9e67fee58db2 # incontrispudorati.com
      - 35bed5aadd0a11e4ae56a6b599de9034 # liaisontorride.com
      - 22ea7b91cbb7a8cc161ab47fa4555ec0 # livebenaughty.com
      - 8fd646677223c318508942feeca4500a # localsgowild.com
      - 225ccad058df4a43f2cda5b3c65b137b # iwantblacks.com
      - 8c53eaebb6061347996df351a07bc31b # lonestardating.com
      - bb4ffe8873ce5b6a2627b9b19d32bb58 # loveaholics.com
      - 397dd0168c01cd518301c086028374e1 # loveaholicslive.com
      - ba44311d2a23f71261181706ce8fefad # naughtydate-app.com
      - 4d5947a22f09eec399d938bb0e6f7671 # naughtydate.com
      - 09d83b58cd8fb96f930b25b8bf35ec55 # askme4date.com
      - 66f4346a7b279b7980681083c52202fd # onenightfriend-app.com
      - 4329bfee2dcc66c03e4002b9034b154b # onenightfriend.com
      - 4c28e085126e7b3ed7932ccb6f2be294 # pikaflirtti.com
      - 3b4b7c1edd0a11e4ae56a6b599de9034 # plaisirexpress.com
      - 9994ade32391a13eeece0ccfe5a50364 # prapaquerar.com
      - a6a80f2a50171b20ac5863b4c3c55645 # prapaquerar-app.com
      - 319ec8620d7c1b627b7ae14ed7135fe3 # voisinssolitaires-app.com
      - 23d96a5e9a2f860ee19b2af6725ad12e # quickflirt.com
      - ef74413fdd0811e4ae56a6b599de9034 # rdvtorride.com
      - 6de56237c4aa04e79b86ae2362d4fabc # realdateroulette.com
      - e4d46083c010c6e6895d2206151ba5b7 # russianturkishlove.com
      - 7f3377c8b0d17665d994172e52a43934 # seifrech.com
      - 2171d31ed0baa9382cfcf6265df04e10 # sejaatrevido.com
      - 67a04ad98e58aebd7c698fbbf9810d5a # senzapudore.it
      - c6b7ff5ba372da648656497734e16253 # setravieso.com
      - 375dab39a4531ed3b5a78f15a7e1f375 # setravieso.es
      - fd22961d3936611bd944c9413c810009 # setravieso.mx
      - a0c347044681a825d2ca7fa9220ff756 # snabbflirt.com
      - b9f374be9b26acc72dec1a4b087ead37 # citasconlatinas.com
      - bdb06941df76e50bdd060f216ff64c1b # spicydesires-app.com
      - 68f37b22bc1449ca317fd61f42319f43 # spicydesires.com
      - 66bfeb28cc2ab45ee2558c063250baa8 # tapsweet.com
      - 58c95263d1f09bf639341b112bf44c0d # iwantasian.com
      - 14913b7e1698a52d3976d043f2f7d8a5 # tatschmi.com
      - 69c25eb72d7a33714ca329c253c51a56 # together.com
      - f1878e86ded8e1fa7fc1de3151fa61d9 # together2night.com
      - 585882fc13e089d9e0dc52d3598ad2c2 # wildmeet-app.com
      - bd72e468f47e11e2baf44b331f04b47e # wildmeets.com
      - 98235292ea434ca39cf27660b6319bc7 # affairesecrete.com
      - c68e8571529014415c7a3d365b7c7507 # taetsidesprang.com
      - cd4a31f97f6e465d8022fde9b6e65321 # jhlive.com
      - 8c92d49ab6453d453b9451832024606d # naughtyflirt.com
      - b2463e76c4f2d323076fe1ed29f83e90 # wantmatures.com
      - 89be305211872106012ba70d733327fd # adopteunemature.com
      - ca3a604b8e1ea935f8ea2b756b5a72c4 # locals.dating
      - c911274061117fdd69df5442af536cca # gaystryst.com
      - 0a1a0bcef2f28a51f995e34171c14b72 # gaysgodating.com
      - 75e542c8e919480aacd038dbed48e9bb # jmec.fr
      - 5913b7fbe4d347f29803bf8b47cf1d2f # mytilene.fr
      - e66a44133374ec3713587cdf5e7df8d9 # lesbiedates.com
      - 4e3c546f1fc640908317cc47a1496778 # ulla.com
      - 0e351195264936449bcb8ee1d29255b2 # meetmilfy.com
      - 1616742006b24c6f2fdcc9e3c4297eee # flirtycougars.com
      - f6803a32d4303d12d41f89838715e183 # flirtymature.com
  v: TN

- k:
    siteId:
      - 3ed0b1d67dfb11e3ac4dd4bed9a9456d # 1nightstand.co.uk
      - 3ed0b3ca7dfb11e3ac4dd4bed9a9456d # 21sexturydating.com
      - 3ed0b5787dfb11e3ac4dd4bed9a9456d # 3somes.org
      - 3ed0b5647dfb11e3ac4dd4bed9a9456d # adult-friend-finder.org
      - 0242dc1b12eb3671ec9c6ff73ef49860 # affairdate.com
      - 22be992958ea871e03ffdee6421d8bc5 # affairdating.com
      - 3ed0b67c7dfb11e3ac4dd4bed9a9456d # allmybbw.com
      - 3ed0b1407dfb11e3ac4dd4bed9a9456d # amateursupforit.com
      - 3ed0b3207dfb11e3ac4dd4bed9a9456d # amissexy.com
      - c673a3f58dfd17290ac36a8bf010a750 # amantssexy.com
      - cde95acedb89c0f68bbcba3763bc1c29 # annoncessexy.fr
      - f75c7ca110b022e17a5794d5b7e54e04 # assets.upforitnetworks.com
      - 3ed0b3fc7dfb11e3ac4dd4bed9a9456d # bbwlocalsex.com
      - 3ed0b4067dfb11e3ac4dd4bed9a9456d # bbwsexdate.com
      - 3ed0b1227dfb11e3ac4dd4bed9a9456d # bbwsupforit.com
      - 3ed0b2447dfb11e3ac4dd4bed9a9456d # bbwtodate.com
      - 3ed0b2627dfb11e3ac4dd4bed9a9456d # bediscreet.com
      - 9385b9fcc779bf93a302f2e6d96b0911 # flirtdiscreet.com
      - 3ed0b3847dfb11e3ac4dd4bed9a9456d # bemyfuckbuddy.co.uk
      - 3ed0b5327dfb11e3ac4dd4bed9a9456d # bestdatingsites.me.uk
      - 1ab49b9de718002de3e60b682a7b4bd0 # bestsexmatch.com
      - 3c3dd5ab8c310b733f1b120cba935e62 # blackwink.com
      - 3ed0b4107dfb11e3ac4dd4bed9a9456d # bustymaturehookup.com
      - 3ed0b1b87dfb11e3ac4dd4bed9a9456d # cambiarpareja.com
      - 3ed0b3c07dfb11e3ac4dd4bed9a9456d # casual.divorceddate.com
      - 3ed0b5287dfb11e3ac4dd4bed9a9456d # chatroomsuk.co.uk
      - c98018e2f06bb89051b987af2d64ffc6 # cherchemilfs.com
      - 3ed0b4247dfb11e3ac4dd4bed9a9456d # chubbysexdate.com
      - 3ed0b1907dfb11e3ac4dd4bed9a9456d # cockmad.co.uk
      - 3ed0b3347dfb11e3ac4dd4bed9a9456d # contactos.freeones.es
      - 3ed0b0fa7dfb11e3ac4dd4bed9a9456d # contacts.anywhere.xxx
      - e8e315d1457e58b16c5e174fa262cab3 # copainssexy.com
      - 3ed0b4607dfb11e3ac4dd4bed9a9456d # cougarpourmoi.com
      - 4e348433271ff7f6155d325b0c7f1285 # cougaramoi.com
      - d69e782560e83318917169bf0d58707e # follaconcougars.com
      - bf61caffb6b64491fc7bb9605f968200 # panteroneonfire.com
      - 3ed0b0be7dfb11e3ac4dd4bed9a9456d # couplesmeet.com
      - e8541e31d7985a1ba1dfebefb11fd61f # dammisesso.com
      - 3ed0b3ac7dfb11e3ac4dd4bed9a9456d # dateburg.com
      - 3ed0b4387dfb11e3ac4dd4bed9a9456d # dateflux.com
      - 22b6992958ea871e03ffdee6421d8bc5 # datesguru.com
      - 3ed0b42e7dfb11e3ac4dd4bed9a9456d # datestation.com
      - 3ed0b6047dfb11e3ac4dd4bed9a9456d # datethelatina.com
      - 3ed0b5507dfb11e3ac4dd4bed9a9456d # dating-websites-uk.com
      - 3ed0b4f67dfb11e3ac4dd4bed9a9456d # dating.adult-finder.com
      - 3ed0b2d07dfb11e3ac4dd4bed9a9456d # dating.avn.com
      - 3ed0b4ec7dfb11e3ac4dd4bed9a9456d # dating.cyber-sex.com
      - 3ed0b1a47dfb11e3ac4dd4bed9a9456d # dating.flirt.com.au
      - 3ed0b30c7dfb11e3ac4dd4bed9a9456d # dating.pornbanana.com
      - 3ed0b2a87dfb11e3ac4dd4bed9a9456d # dating.private.com
      - 3ed0b5967dfb11e3ac4dd4bed9a9456d # double-sex.com
      - 3ed0b1ae7dfb11e3ac4dd4bed9a9456d # facebookdesexo.es
      - 3ed0b2c67dfb11e3ac4dd4bed9a9456d # fatflirt.com
      - 32faf07b1ec03f20aaee30eb62c5a78c # fattychaser.com
      - 3ed0b2bc7dfb11e3ac4dd4bed9a9456d # findbbwsex.com
      - 3ed0b21c7dfb11e3ac4dd4bed9a9456d # fkbook.net
      - 3ed0b0dc7dfb11e3ac4dd4bed9a9456d # flirtmatch.co.uk
      - 4a6d32e790b0217032a0d585d4d07493 # flirtymilfs.co.uk
      - a2619a464727e0b1c48d3195f7d7ea9a # flirtymilfs.com
      - 8d2858fa04da34d024a28cb9e8ddc5ae # forexlucks.com
      - 3ed0b51e7dfb11e3ac4dd4bed9a9456d # fortyandnaughty.com
      - 3ed0b58c7dfb11e3ac4dd4bed9a9456d # free-adultwebcam.org
      - 3ed0b56e7dfb11e3ac4dd4bed9a9456d # freeadultchatrooms.biz
      - 3ed0b5a07dfb11e3ac4dd4bed9a9456d # freesexdating.biz
      - eef88c78ab3e8efd345e411cc399f99d # freesexmatch.com
      - 3ed0b0f07dfb11e3ac4dd4bed9a9456d # friskyflirts.com
      - e54ed198be49e70999487bb041cc61c1 # fristemeg.no
      - 14cd52a6595042fb22b75905482d16d3 # fristmignu.dk
      - 3ed0b5aa7dfb11e3ac4dd4bed9a9456d # gay-chatrooms.net
      - 3ed0b6407dfb11e3ac4dd4bed9a9456d # getamilfaffair.com
      - 3ed0b2587dfb11e3ac4dd4bed9a9456d # getanaffair.com
      - 8f90283b6fffcd01f482b100828a60bd # trovaunamante.com
      - 56d5724acc93b63e3de1aea1222898f4 # buscarollos.com
      - 8c6670d7674a01be8a5139db7d2921bc # seekanaffair.com
      - af319309fed13c64184b7ccb070ddd0c # gibmirsex.com
      - cb9e5abc59ca06f29c298323f2b70b62 # hookupsfinder.com
      - 3ed0b3987dfb11e3ac4dd4bed9a9456d # hornyclub.co.uk
      - 7fb3c29be523ba3f92d532261fc4ace2 # hornycontacts.com
      - 2f3c0067ae63f4444c19f51bbbc725cc # myhornycontacts.com
      - 3ed0b0a07dfb11e3ac4dd4bed9a9456d # hornyonlinematch.com
      - 3ed0b4a67dfb11e3ac4dd4bed9a9456d # hornyplumps.com
      - be0308922ac6cdc145a996b05639a87e # hotsexmatch.com
      - 07b8fc75d0a2ed467c0b2ef97050f50b # iamnaughty.com
      - dca447b6744d12a3dae09b5cf5e3a19f # bestsite4dating.com
      - 33393a5f5cfbfe9f11e822539e86eefa # flirtylatinas.com
      - deda2945a8f3ea31f37b9f4a015782cb # lustylocals.com
      - 6296942bd892924c6ada2948512fcf9a # yolovers.com
      - 45fe55c6b9a1942551a9a56631a5f9a3 # steamylocals.com
      - 00a89e5ea715d06612e039410c5ec329 # igetnaughty.com
      - 3ed0b2da7dfb11e3ac4dd4bed9a9456d # iwantcasual.com
      - 3ed0b1ea7dfb11e3ac4dd4bed9a9456d # iwantu.com
      - ef2bb46f107b4bcccefcedf67ecebe83 # lystpadeg.com
      - e2534af8d053e529bf3d9a22abf31a82 # jagvillhadig.com
      - 1fffba60796b2004a36a5a5a2e878aff # vilhavedig.com
      - 3c38e4b9d4d4453e5a9786e45682bd8d # iwantubbw.com
      - 484ea00059eb655690494e32327c3852 # iwantucougar.com
      - 52158ddd6f12d8c3202e2cbe461bcbdd # iwantumilf.com
      - 04062bd38f529a8a5eb9ebc353c938ec # jenomsex.cz
      - 2db03b96e850a70eed04aa0aac32b68b # laymatures.co.uk
      - 3ed0b0827dfb11e3ac4dd4bed9a9456d # luludating.com
      - 3ed0b60e7dfb11e3ac4dd4bed9a9456d # lustmatesforfun.com
      - 3ed0b24e7dfb11e3ac4dd4bed9a9456d # maritalhookup.com
      - 3ed0b65e7dfb11e3ac4dd4bed9a9456d # match-milf.com
      - 06ce3e1f2f2c6b0e9d967e5020f6efd5 # maturesforfuck.com
      - 3ed0b1367dfb11e3ac4dd4bed9a9456d # matureupforit.com
      - 3ed0b2807dfb11e3ac4dd4bed9a9456d # maturexmatch.com
      - 3ed0b5dc7dfb11e3ac4dd4bed9a9456d # meetdivorced.com
      - 3ed0b4747dfb11e3ac4dd4bed9a9456d # milfparfaite.com
      - 3ed0b4ba7dfb11e3ac4dd4bed9a9456d # mybbwmatch.com
      - eee70c9429a528733c4ddda6d20b5418 # meetwild.com
      - 3ed0b14a7dfb11e3ac4dd4bed9a9456d # milfberry.com
      - cb26546d56463c83db6551812b8fcf9f # milfberries.com
      - f8be7ca20eb70b4be98e44474d1e0bec # momsgetnaughty.com
      - 3ed0b5147dfb11e3ac4dd4bed9a9456d # mysecretbbw.com
      - 3ed0b5007dfb11e3ac4dd4bed9a9456d # mysecretcougar.com
      - 3ed0b6d67dfb11e3ac4dd4bed9a9456d # mysecretdiary.com
      - 3ed0b4b07dfb11e3ac4dd4bed9a9456d # mysecretmilf.com
      - 0cc1d7fa91b64d3d9966835fbfcbdd91 # nastyhookups.com
      - 3ed0b06e7dfb11e3ac4dd4bed9a9456d # naughty123.com
      - 3ed0b6187dfb11e3ac4dd4bed9a9456d # naughtyandhotty.com
      - 3ed0b3b67dfb11e3ac4dd4bed9a9456d # naughtyfreeandsingle.com
      - 3ed0b38e7dfb11e3ac4dd4bed9a9456d # naughtyhookups.co.uk
      - 238404ae470349506beeb63da8c22bf6 # naughtysporty.com
      - 3ed0b2307dfb11e3ac4dd4bed9a9456d # newsfilter.org.uk
      - 7b8ed3e6b64174c3ad9e3f4d80cdeccb # noitedesexo.com
      - 3ed0b3e87dfb11e3ac4dd4bed9a9456d # olderwomenseekingsex.com
      - 3ed0b1187dfb11e3ac4dd4bed9a9456d # onlinesexmatch.com
      - 3de4e5d23ac4e4a144a9d2a83dfb1d5a # pakmenu.be
      - 3ed0b29e7dfb11e3ac4dd4bed9a9456d # passionmature.com
      - 3ed0b4887dfb11e3ac4dd4bed9a9456d # planetemature.com
      - 6228667425b1c22af455fd5e355fd8cf # piacerediscreto.com
      - 5921cf7d348f61797a4df3b5a9f58454 # playcougar.com
      - 3ed0b6fe7dfb11e3ac4dd4bed9a9456d # quierorollo.es
      - 0df91ba7a954626eb8e81f0b4f862a89 # rampetepiker.no
      - 3ed0b3167dfb11e3ac4dd4bed9a9456d # relacionesmaduras.com
      - 3ed0b3707dfb11e3ac4dd4bed9a9456d # rolloduro.es
      - 3ed0b6cc7dfb11e3ac4dd4bed9a9456d # saucysingles.com
      - 86e22a0e90a58d3609d16857ef4d0188 # mysaucysingles.com
      - 37fa167c120b454448f30ee4b4142ad1 # seksueltforhold.dk
      - 3dfde1d992c4ff0e4488b8a18dfb14f9 # seksverlangen.be
      - 3ed0b0d27dfb11e3ac4dd4bed9a9456d # sexcontactsclub.co.uk
      - 153a7f325bfed6549681016d6c9eec55 # sexefemmemure.com
      - a03c78177bb66f6793f27830557faddf # sexeronde.com
      - b3a0fa7c492b5b1b964d84a7795fa52b # sexigdate.se
      - 8baeb996a9dd09389833090156008534 # sexinfidele.com
      - b4887931a2c35378148ce4406ac1b2fe # sexlugar.com
      - 3d585ae74c02b21d098e904ad386eb0b # sexosemamor.com
      - 096e2bd269b31210952ba303c6ab782e # sexyblackpeople.com
      - 3ed0b01e7dfb11e3ac4dd4bed9a9456d # shagaholic.com
      - 3ed0b5c87dfb11e3ac4dd4bed9a9456d # sinfulmates.com
      - 3ed0b47e7dfb11e3ac4dd4bed9a9456d # soissecret.com
      - c7295ecafcf84074327128fb37622253 # myshagaholic.com
      - 6131a6ebca6e4a3565e813043e4646df # snuskflirt.se
      - 17c06041853eb6d2749614727733a886 # soifdetoi.com
      - 940ef8d7b2b4af0e438eb8a18dfb14f9 # soloavventure.it
      - 3ed0b1cc7dfb11e3ac4dd4bed9a9456d # sololigar.com
      - fda1790e9b207491438c29d5cdc5ded2 # sololigones.com
      - 5a8da184f50196225426bd1e9cbd10b1 # sportyhottie.com
      - 3ed0b6687dfb11e3ac4dd4bed9a9456d # titbracebbw.com
      - 3ed0b2947dfb11e3ac4dd4bed9a9456d # town.cougarloversdating.com
      - b519b83f1a7c6b088f856ac22f3af17e # treffegirls.com
      - 5182a73fae46381e91a7b032f0d42105 # tuhmameille.com
      - 3ed0b0aa7dfb11e3ac4dd4bed9a9456d # ultimatesexcontacts.co.uk
      - 3ed0b2267dfb11e3ac4dd4bed9a9456d # undercoverlovers.com
      - 693cdfa9f44120452ac66e47fbee717f # up4hookup.com
      - 3ed0b0c87dfb11e3ac4dd4bed9a9456d # upforanything.co.uk
      - 3ed0b6b87dfb11e3ac4dd4bed9a9456d # upforit.com
      - 879007a9e1cc39257d262cb60282da1a # upforit-app.com
      - 04633cdeee23900a2cba6d3e490d0295 # upforafling.com
      - 85143acc55888de4dd3bd04a7e4e4a72 # upforitnetworks.com
      - 3ed0b6227dfb11e3ac4dd4bed9a9456d # upwithmilf.com
      - 8604147027e83a377b889e0f2e4f1d45 # vittubuddie.com
      - 051b5c33e2bfe578f3604dece9c23f78 # voisinssolitaires.com
      - 82eaa355e8ecee933179507329567a6d # volendoti.com
      - 3ed0b2f87dfb11e3ac4dd4bed9a9456d # vraiflirt.com
      - c008539a6aba2970c600b4a4fb03b136 # wantubad.com
      - 369c2d3d7a1d2fbe48645bed52b12d4e # wivesgowild.com
      - 00d202ad587104caa587dd65c1f57307 # woohoodates.com
      - 402c16bfc7897d858765dd2ccd6ffbe6 # zhavenoci.cz
      - 4d90f017495690ddf2448318e97b51a2 # figlarnerandki.com
      - 2ba2b7b02a73f00b667b7d1948805ed4 # lystpasex.com
      - 8f90283b6fffcd01f482b100828a60bd # trovaunamante.com
      - 56d5724acc93b63e3de1aea1222898f4 # buscarollos.com
      - df741670c3421747e437c772ec897ce6 # buddygays.com
      - 28c0c162ee4f6fbc8f7735f1ac4ccc3a # lesbiemates.com
      - 5258a5b211ccb2f94095e70d7e57200e # wantubad-app.com
      - 051eac2677d2779b6c311375a9cca890 # iamnaughty-app.com
      - 0394d72486de6e91b085808d45220314 # iwantu-app.com
      - 2041a3b41fe621f4fe23e4326e4cc77f # meetmesexy.com
      - 79d74e2d3cb1b3d20b709d9d7938393d # delightsexy.com
      - d89230600a57b43ab8df4107c0f2f6ca # laymatures.com
      - 04fcc5596e2a76f9a12cf925e51ff841 # medpaadenvaerste.com
      - 887881cec8f115511b97722848641426 # expresflirt.com
      - a10cbdb10b44bf87dd25da325270ea7a # asiansgetnaughty.com
      - 001b635667df27730e05ddb45f8ac280 # nezavazne.com
      - e31b3fb4cd66927e23d5bdcf8878a127 # niegrzecznezabawy.com
      - e3122656d9ca33f2a8c8a2105334c961 # istemninga.com
      - f22df34462ba1cd1d6e9f99a4b24ceb9 # klarpaasex.com
  v: UFI

- k:
    siteId:
      - 6b88389da243a3c3af7b63dafb8adef0 # cams.wantubad.com
      - c7f974bbbe41efaf74a85f662702049d # dirtypunishtube.com
      - d4a2eb75245c4ac597d5e72d0f769ca7 # livecammates.com
      - 3ed0b7127dfb11e3ac4dd4bed9a9456d # livex.com
      - 0b180078d994cb2b5ed89d7ce8e7eea2 # mynaughtyfeed.com
      - d4f0fdca87b163be4d32680e8ab25384 # datesrepublic.com
  v: AG

- k:
    siteId:
      - 16fd14055afa4f3b9910a5933fe18878 # flirtforit.com
      - a5515f9cd8853d3e9c35a856f60dc0d6 # foreigngirlfriend.com
      - 88a5b7d765d9e9e522d0f9b8173f0007
      - d26064c845bd4b6da9b6e873c5a625f9 # hotandflirty.com
      - 33da7a4a5fa9405d9eda7f61ecec5515 # internationaldate.com
      - 7744c2c8769e4b45a325190205b66420 # lovefordate.com
      - bee17101d1d33b76837dc884488b39a5 # justgetnaughty.com
      - bbef7641c4b64797baa2f53fe2a21051 # bemynaughty.com
      - 46bff78f663558963b4db4a488bf0855 # makeitnaughty.com
      - aca20b0bd14d4c5ab97a7d678a9ffe35 # burningtalks.com
      - 60763a0fa205f7fabce30307ca3629ce # naughtywebgirl.com
      - 9200a17fde147fe93ce778fd3004a44d # spankchats.com
      - abd9740de0a24e9e898f80e03e95b64e # spicyasiangirl.com
      - 4eaf81b887fd5d187563dfe984a87947 # spicychats.com
      - e9ea1a97ec9746cd92d3f1ffd06dbd9e
      - f612f6434e55463985f103aea4032a0c
      - 2bc10653baac408aa98bfde0cdf0d509 #abroadaffair
      - e84b1134f92700368c4870a3f0c82008 # wildbuddies.com
      - 6747c8fb5b73abce100992b51af15eef # naughtyappetite.com
      - 83b87cc09ab3d521bdc33ec8369c535e # wildspank.com
  v: TN Labs

- k:
    siteId:
      # amour.com (for the future)
      # aondenamoro.com (for the future)
      # blackmatch.com (for the future)
      # canoodle.com (for the future)
      # cum.fr (for the future)
      # cupid.com (for the future)
      # cupidaffinity.com (for the future)
      - 5794d02ea693e77587947a1b711f3c9e # datefinder.com
      # datetheuk.com (for the future)
      - 00a8b9cfabc14fe78d2af8a46faec58e # datingforparents.com
      - 06301e0c7e7d99f7c2afa930d119d68f # elove.com
      # friendsreuniteddating.com (for the future)
      - 884a04bffabe4ecc974a786da9bda7bd # girlsdateforfree.com
      # granamor.com (for the future)
      # indiandating.com (for the future)
      - ebcb27dbad2ed243442e707067e7ccaf # localsgodating.com
      # loopylove.com (for the future)
      # loveagain.com (for the future)
      # lovebeginsat.com (for the future)
      - 324b723f0b4202d9d864f34609bcfbe9 # lovebook.com
      # lovechemistry.com (for the future)
      # m.socialove.com (for the future)
      - 4a2934dee17fa9a91e3bd74787f0bc8a # matureaffection.com
      - 188682cd740e88df6c35d518bc51fd1f # maturedating.com
      # maturedatinguk.com (for the future)
      - c616748ad6c542c39533e71da8a42034 # planetsappho.com
      # serencontrer.com (for the future)
      # silvercupid.com (for the future)
      # socialove.com (for the future)
      # swoon.co.uk (for the future)
      - c30563e1a668452f843bf9c3589e302f # uniformdating.com
      # yolo.com (for the future)
      - 1e303e06704773990f3f73df96bffaae # datingcrush.com
  v: TN Trad

- k:
    siteId:
      - ecf5d83a45603a08b86ac0165d316d46 # douwant.me
      - de53ccaadf56a0758d2310f6ee73db5f # douwantme-app.com
      - 80e1c22d171c0a8bb378a3f3c44b0407 # nastymams.com
      - 2ea9365cbbe27a38798c924fef9e9760 # naughtyfever-app.com
      - 38ef4f1b447f81f99dcecba07bef6c12 # naughtyfever.com
      - 4e41edba20141fc9fd886822d3f8c807 # naughtyunited-app.com
      - d630c9728282f74ac6b8a9328cc453be # naughtyunited.com
      - ee6a358c3a347fac277189e04a169ed3 # nfmature.com
  v: MI-TN

- k:
    siteId:
      - 71d42d6e5d4627a5711688203e74e142 # bangexperts.com
      - e62cbd9620e896d6f8d30871c96f248d # fievrecoquine.com
      - 2cf11d6680ebf8bc18b87df3a88b0917 # iwanthotties.com
      - 811333999cbb8b1707a47ec0d02db00c # sexintouch.com
      - c6d80bfaf5c66630f994abad93aba92f # sexintouchbbw.com
      - 304dc6c48195ac3365f518607c5109c0 # wilddate4sex.com
  v: MI-UFI
";
    }

    public function testSettingsConfig()
    {
        //Get eva sites
        $evaSites = [];
        foreach ($this->params['sites'] as $key => $val) {
            $evaSites[] = $key;
        }

        //Get phoenix sites
        $phoenixSites = [];
        $parsedYaml = \Symfony\Component\Yaml\Yaml::parse($this->yaml);
        foreach($parsedYaml as $val) {
            if (is_array($val['k']['siteId'])) {
                foreach ($val['k']['siteId'] as $v) {
                    $phoenixSites[] = $v;
                }
            } else {
                $phoenixSites[] = $val['k']['siteId'];
            }
        }

        //Find absent sites
        $absentSites = [];
        foreach ($phoenixSites as $phoenixSite) {
            if (!in_array($phoenixSite, $evaSites)) {
                $absentSites[] = $phoenixSite;
            }
        }

        $this->assertEquals(
            0,
            count($absentSites),
            "Need to add to common/config/settings.php next sites:" . PHP_EOL . var_export($absentSites, true)
        );
    }

    public function testProjectBySiteIdConfig()
    {
        //Get eva sites
        $evaTNSites     = [];
        $evaUFISites    = [];
        $evaAGSites     = [];

        foreach ($this->params['projectBySiteId']['TN'] as $key => $val) {
            $evaTNSites[] = $key;
        }

        foreach ($this->params['projectBySiteId']['UFI'] as $key => $val) {
            $evaUFISites[] = $key;
        }

        foreach ($this->params['projectBySiteId']['AG'] as $key => $val) {
            $evaAGSites[] = $key;
        }

        //Get phoenix sites
        $phoenixTNSites     = [];
        $phoenixUFISites    = [];
        $phoenixAGSites     = [];

        $parsedYaml = \Symfony\Component\Yaml\Yaml::parse($this->yaml);

        foreach($parsedYaml as $val) {
            if ($val['v'] == 'TN' || $val['v'] == 'MI-TN') {
                //TN sites
                if (is_array($val['k']['siteId'])) {
                    foreach ($val['k']['siteId'] as $v) {
                        $phoenixTNSites[] = $v;
                    }
                } else {
                    $phoenixTNSites[] = $val['k']['siteId'];
                }
            } else if ($val['v'] == 'UFI' || $val['v'] == 'MI-UFI') {
                //UFI sites
                if (is_array($val['k']['siteId'])) {
                    foreach ($val['k']['siteId'] as $v) {
                        $phoenixUFISites[] = $v;
                    }
                } else {
                    $phoenixUFISites[] = $val['k']['siteId'];
                }
            } else if ($val['v'] == 'AG') {
                //AG sites
                if (is_array($val['k']['siteId'])) {
                    foreach ($val['k']['siteId'] as $v) {
                        $phoenixAGSites[] = $v;
                    }
                } else {
                    $phoenixAGSites[] = $val['k']['siteId'];
                }
            }
        }

        //Find absent sites
        $absentTNSites = [];
        foreach ($phoenixTNSites as $phoenixTNSite) {
            if (!in_array($phoenixTNSite, $evaTNSites)) {
                $absentTNSites[] = $phoenixTNSite;
            }
        }

        $absentUFISites = [];
        foreach ($phoenixUFISites as $phoenixUFISite) {
            if (!in_array($phoenixUFISite, $evaUFISites)) {
                $absentUFISites[] = $phoenixUFISite;
            }
        }

        $absentAGSites = [];
        foreach ($phoenixAGSites as $phoenixAGSite) {
            if (!in_array($phoenixAGSite, $evaAGSites)) {
                $absentAGSites[] = $phoenixAGSite;
            }
        }

        $this->assertEquals(
            0,
            count($absentTNSites) + count($absentUFISites) + count($absentAGSites),
            "Need to add to common/config/projectDySiteId.php next TN sites:" . PHP_EOL . var_export($absentTNSites, true) . PHP_EOL . PHP_EOL .
            "Need to add to common/config/projectDySiteId.php next UFI sites:" . PHP_EOL . var_export($absentUFISites, true) . PHP_EOL . PHP_EOL .
            "Need to add to common/config/projectDySiteId.php next AG sites:" . PHP_EOL . var_export($absentAGSites, true)
        );
    }
}
