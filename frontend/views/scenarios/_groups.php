<?php
use common\widgets\BindableFormItems\BindableFormItems;

$groupItems = array();

foreach ($model->groups as $group) {
    $item['text'] = $group->getName();
    $item['hiddens'] = [
        ['name' => 'groupId', 'value' => $group->getId()],
    ];
    $groupItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => '',
    'items' => $groupItems,
]);
