<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_traffic_types_table`.
 */
class m160622_142310_create_settings_traffic_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_traffic_types_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'traffic_type_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_traffic_types_table');
    }
}
