<?php
declare(strict_types=1);

namespace frontend\models\usersId;

use yii\data\ActiveDataProvider;
use frontend\models\DataProvider;
use frontend\models\placeholders\Placeholder;
use yii\db\Expression;

class SearchCommunicationLog extends CommunicationLog
{
    public $sender;
    public $receiver;
    public $scenario;
    public $placeholder;
    public $keywords;
    public $modelKeywords;

    public $availableScenarios = array();
    public $availablePlaceholders = array();

    public function __construct()
    {
        $scenarios = DataProvider::getInstance()->getScenarios();
        // for 'all scenarios' we will use -1 key
        $this->availableScenarios[-1] = '...';
        foreach ($scenarios as $scenario) {
            $this->availableScenarios[$scenario->getId()] = $scenario->getName();
        }
        // for 'all placeholders' we will use -1 key
        $placeholders = Placeholder::find()->orderBy('id')->all();
        $this->availablePlaceholders[-1] = '...';
        foreach ($placeholders as $placeholder) {
            $this->availablePlaceholders[$placeholder->id] = $placeholder->name;
        }
    }

    public function rules()
    {
        // only this fields will be accessible in search
        return [
            [['sender', 'receiver'], 'string', 'max' => 32],
            [['scenario', 'placeholder'], 'integer'],
            [['keywords', 'modelKeywords'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = CommunicationLog::find()
            ->select('*')
            ->andFilterWhere(['isUser' => true])
            ->groupBy(['userId', 'modelId']);

        $subQuery = CommunicationLog::find()
            ->select('userId as uId, modelId as mId, COUNT(*) AS messageCount, MAX(placeholderId) AS maxPlaceholderId')
            ->andFilterWhere(['isUser' => false])
            ->groupBy(['uId', 'mId']);

        $query->innerJoin(['sq' => $subQuery], 'sq.uId = userId AND sq.mId = modelId');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'createdAt',
                'modelId',
                'userId',
                'country',
                'language',
                'trafficSource',
                'profileType',
                'platform',
                'scenarioId',
                'messageCount',
                'maxPlaceholderId',
            ]
        ]);

        // load data from form and validate
        if (!($this->load($params) && $this->validate())) {
            $threeHoursAgo = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' -3 hours'));
            $params = ['>=', 'createdAt', new Expression('\'' . $threeHoursAgo . '\'')];
            $query->andFilterWhere($params);
            $subQuery->andFilterWhere($params);
            return $dataProvider;
        }

        // add filtration
        $params = [];
        if ($this->sender == '' && $this->receiver != '') {
            $params = [
                'or',
                ['userId'   => $this->receiver],
                ['modelId'  => $this->receiver]
            ];
        } elseif ($this->sender != '' && $this->receiver == '') {
            $params = [
                'or',
                ['userId'   => $this->sender],
                ['modelId'  => $this->sender]
            ];
        } elseif ($this->sender != '' && $this->receiver != '') {
            $params = [
                'or',
                ['userId'   => $this->sender,   'modelId'  => $this->receiver],
                ['userId'   => $this->receiver, 'modelId'  => $this->sender]
            ];
        } else {
            // if sender and receiver are empty the query will be very slow
            $threeHoursAgo = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' -3 hours'));
            $params = ['>=', 'createdAt', new Expression('\'' . $threeHoursAgo . '\'')];
        }
        $query->andFilterWhere($params);
        $subQuery->andFilterWhere($params);

        if ($this->scenario != -1) {
            $query->andFilterWhere(['scenarioId' => (int)$this->scenario]);
            $subQuery->andFilterWhere(['scenarioId' => (int)$this->scenario]);
        }

        if ($this->placeholder != -1) {
            // Placeholders situated only in messages from bot
            $subQuery->andHaving(['maxPlaceholderId' => (int)$this->placeholder]);
        }

        if ($this->keywords != '') {
            $keywords = explode(',', $this->keywords);
            // Delete empty values. For example, if $this->keywords string is 'a,, b, c,,,'
            $keywords = array_filter($keywords);

            $keywordsParams = ['OR'];
            foreach ($keywords as $keyword) {
                // Delete spaces
                $keyword = trim($keyword);
                $keywordsParams[] = ['RLIKE', 'message', '[[:<:]]' . $keyword . '[[:>:]]'];
            }

            // Filter messages from user
            $query->andFilterWhere($keywordsParams);
        }

        if ($this->modelKeywords != '') {
            $modelKeywords = explode(',', $this->modelKeywords);
            // Delete empty values. For example, if $this->modelKeywords string is 'a,, b, c,,,'
            $modelKeywords = array_filter($modelKeywords);

            $modelKeywordsParams = ['OR'];
            foreach ($modelKeywords as $modelKeyword) {
                // Delete spaces
                $modelKeyword = trim($modelKeyword);
                $modelKeywordsParams[] = ['RLIKE', 'message', '[[:<:]]' . $modelKeyword . '[[:>:]]'];
            }

            // Filter messages from model
            $subQuery->andFilterWhere($modelKeywordsParams);
        }

        return $dataProvider;
    }

    public function getMessages($params)
    {
        $query = CommunicationLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        // load data from form and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // add filtration
        $query->andFilterWhere(['or',
            ['userId'   => $this->sender,   'modelId'  => $this->receiver],
            ['userId'   => $this->receiver, 'modelId'  => $this->sender]
        ]);

        return $dataProvider;
    }
}
