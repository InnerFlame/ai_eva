<?php

use yii\db\Migration;

class m180402_084807_martchenko_dialogues_by_settings_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('dialogue', [
            'id'  => $this->primaryKey(),
            'idFrom' => $this->string(32),
            'idTo' => $this->string(32),
            'settingScenarioId' => $this->integer(11),
            'createdAt' => "timestamp DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->createIndex('idFromTo', 'dialogue', ['idFrom', 'idTo']);
        $this->createIndex('createdAt', 'dialogue', 'createdAt');
    }

    public function safeDown()
    {
        $this->dropTable('dialogue');
    }
}
