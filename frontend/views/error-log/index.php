<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $model->provider,
    'columns' => [
        ['attribute' => 'id',],
        ['attribute' => 'level',],
        ['attribute' => 'category',],
        [
            'attribute' => 'log_time',
            'format' => ['datetime', 'y-MM-dd HH:mm:ss'],
            'label' => 'Time',
            'value' => function ($data) {
                return (int)$data->log_time;
            },
        ],
        ['attribute' => 'prefix',],
        ['attribute' => 'message',],
    ],
]);
?>

<form action="" method="post" name="clearLogForm">
    <input
        type="hidden"
        name="<?= Yii::$app->request->csrfParam; ?>"
        value="<?= Yii::$app->request->csrfToken; ?>"
    />
    <input type="hidden" name="clear" value="true" />
    <button type="submit" class="btn btn-danger pull-right">
        <span class="glyphicon glyphicon-trash"></span> Clear log
    </button>
</form>
