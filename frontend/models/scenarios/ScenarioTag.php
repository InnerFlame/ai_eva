<?php
namespace frontend\models\scenarios;

use yii\db\ActiveRecord;

class ScenarioTag extends ActiveRecord
{
    public static function tableName()
    {
        return 'scenarios_tags';
    }
}
