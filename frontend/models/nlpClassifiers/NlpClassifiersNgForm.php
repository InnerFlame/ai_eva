<?php
namespace frontend\models\nlpClassifiers;

use frontend\models\BaseNgForm;
use yii\base\InvalidParamException;

/**
 * Class NlpClassifiersNgForm
 * @package frontend\models\nlpClassifiers
 */
class NlpClassifiersNgForm extends BaseNgForm
{
    const CLASSIFIERS_TYPES = [
        self::TYPE_ML,
        self::TYPE_ML_ROOT
    ];

    const TYPE_ML      = 'ml_classifier';
    const TYPE_ML_ROOT = 'root_ml_classifier';

    /**
     * @param string $type
     * @return array
     */
    public function getClassifiers(string $type): array
    {
        if (!in_array($type, self::CLASSIFIERS_TYPES)) {
            return [];
        }

        $data = $this->getBrungildaApi()->getClassifiers();
        $filteredClassifiers = $this->filterClassifiersByClass($data, $type);

        return $filteredClassifiers;
    }

    /**
     * @param $requestData
     * @return array
     */
    public function createClassifier(array $requestData): array
    {
        $createParams = [
            'text' => $requestData['text'],
            'type' => $requestData['type']
        ];

        return $this->getBrungildaApi()->createClassifiers([$createParams]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updateClassifier(array $requestData): array
    {
        $updateParams = [
            'classifier_id' => (int) $requestData['classifier_id'],
            'text'          => $requestData['text'],
            'model'         => $requestData['model'],
        ];

        return $this->getBrungildaApi()->updateClassifiers([$updateParams]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteClassifier(array $requestData): array
    {
        $deleteParams['classifier_id'] = (int) $requestData['classifier_id'];

        return $this->getBrungildaApi()->deleteClassifiers([$deleteParams]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function trainClassifier(array $requestData): array
    {
        $params['classifier_id'] = (int) $requestData['classifier_id'];

        return $this->getBrungildaApi()->trainClassifiers([$params]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createClassifierClass(array $requestData): array
    {
        $createData = [
            'classifier_id'     => (int) $requestData['classifier_id'],
            'name'              => $requestData['name'],
            'trigger_threshold' => $requestData['trigger_threshold']
        ];

        return $this->getBrungildaApi()->createNlpClassifierClasses([$createData]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updateClassifierClass(array $requestData): array
    {
        $updateData = [
            'classifier_class_id'   => (int) $requestData['classifier_class_id'],
            'name'                  => $requestData['name'],
            'trigger_threshold'     => $requestData['trigger_threshold']
        ];

        return $this->getBrungildaApi()->updateNlpClassifierClasses([$updateData]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteClassifierClass(array $requestData): array
    {
        $deleteData['classifier_class_id'] = (int) $requestData['classifier_class_id'];

        return $this->getBrungildaApi()->deleteNlpClassifierClasses([$deleteData]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createClassifierClassPhrase(array $requestData): array
    {
        $text    = $requestData['text'] ?? '';
        $classId = (int) $requestData['classifier_class_id'] ?? 0;

        if (!$text || !$classId) {
            return [];
        }

        $multiplePhrases = explode(PHP_EOL, $text);

        if (count($multiplePhrases) > 1)
        {
            $res = [];
            foreach ($multiplePhrases as $phrase) {
                $res[] = $this->getBrungildaApi()->createNlpClassPhrases([[
                    'classifier_class_id'   => $classId,
                    'text'                  => $phrase,
                ]]);
            }

            return $res;
        }

        return $this->getBrungildaApi()->createNlpClassPhrases([[
            'classifier_class_id'   => $classId,
            'text'                  => $text,
        ]]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updateClassifierClassPhrase(array $requestData): array
    {
        $updateData = [
            'learning_phrase_id' => $requestData['learning_phrase_id'],
            'text'               => $requestData['text']
        ];

        return $this->getBrungildaApi()->updateNlpClassPhrases([$updateData]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteClassifierClassPhrase(array $requestData): array
    {
        $deleteData['learning_phrase_id'] = (int)$requestData['learning_phrase_id'];

        return $this->getBrungildaApi()->deleteNlpClassPhrases([$deleteData]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteMultipleClassifierClassPhrase(array $requestData): array
    {
        if (empty($requestData['phraseIds'])) {
            return [];
        }

        $result = [];
        foreach ($requestData['phraseIds'] as $phraseId) {
            $result[] = $this->deleteClassifierClassPhrase(['learning_phrase_id' => $phraseId]);
        }

        return $result;
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createPhraseEntity(array $requestData): array
    {
        $createData = [
            'entity'              => $requestData['entity'],
            'value'               => $requestData['value'],
            'start'               => $requestData['start'],
            'end'                 => $requestData['end'],
            'classifier_class_id' => $requestData['classifier_class_id'],
            'learning_phrase_id'  => $requestData['learning_phrase_id'],
        ];

        return $this->getBrungildaApi()->addPhraseEntity($createData);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updatePhraseEntity(array $requestData): array
    {
        $updateData = [
            'learning_phrase_entity_id' => $requestData['learning_phrase_entity_id'],
            'entity'                    => $requestData['entity'],
            'value'                     => $requestData['value'],
            'start'                     => $requestData['start'],
            'end'                       => $requestData['end'],
            'classifier_class_id'       => $requestData['classifier_class_id'],
            'learning_phrase_id'        => $requestData['learning_phrase_id'],
        ];

        return $this->getBrungildaApi()->updatePhraseEntity($updateData);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deletePhraseEntity(array $requestData): array
    {
        return $this->getBrungildaApi()->deletePhraseEntity((int) $requestData['learning_phrase_entity_id']);
    }

    /**
     * @param array $requestData
     * @return bool
     */
    public function createMultiPhraseEntity(array $requestData): bool
    {
        $phrases = $this->getBrungildaApi()->getNlpClassPhrases((int) $requestData['classifier_class_id']);
        $entities = $this->getBrungildaApi()->getClassifierClassPhrasesEntities((int) $requestData['classifier_class_id']);

        foreach ($phrases as $phrase) {
            if (strpos($phrase['text'], $requestData['value']) === false) {
                continue;
            }

            if ($this->isPhraseAlreadyHasEntityWithSuchValue($requestData['value'], $entities, $phrase['learning_phrase_id'])) {
                continue;
            }

            $createData = [
                'entity'              => $requestData['entity'],
                'value'               => $requestData['value'],
                'start'               => $requestData['start'],
                'end'                 => $requestData['end'],
                'classifier_class_id' => $requestData['classifier_class_id'],
                'learning_phrase_id'  => $phrase['learning_phrase_id'],
            ];

            $result = $this->getBrungildaApi()->addPhraseEntity($createData);

            if ($result[0]['err']) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $entityValue
     * @param array $entitiesData
     * @param $phraseId
     * @return bool
     */
    private function isPhraseAlreadyHasEntityWithSuchValue(string $entityValue, array $entitiesData, $phraseId): bool
    {
        foreach ($entitiesData as $entity) {
            if ($entity['learning_phrase_id'] == $phraseId) {
                if ($entity['value'] == $entityValue) {
                    return true;
                }
            }
        }

        return false;
    }
    
    /**
     * @param array $requestData
     * @return bool
     */
    public function updateMultiPhraseEntity(array $requestData): bool
    {
        $phrases  = $this->getBrungildaApi()->getNlpClassPhrases((int) $requestData['classifier_class_id']);
        $entities = $this->getBrungildaApi()->getClassifierClassPhrasesEntities((int) $requestData['classifier_class_id']);

        foreach ($phrases as $phrase)
        {
            if (strpos($phrase['text'], $requestData['value']) === false) {
                continue;
            }

            foreach ($entities as $entity)
            {
                if ($entity['learning_phrase_id'] != $phrase['learning_phrase_id']) {
                    continue;
                }

                if ($entity['value'] != $requestData['value']) {
                    continue;
                }


                $updateData = [
                    'learning_phrase_entity_id' => $entity['learning_phrase_entity_id'],
                    'learning_phrase_id'        => $phrase['learning_phrase_id'],
                    'entity'                    => $requestData['entity'],
                    'value'                     => $requestData['value'],
                    'start'                     => $requestData['start'],
                    'end'                       => $requestData['end'],
                    'classifier_class_id'       => $requestData['classifier_class_id'],
                ];

                $this->getBrungildaApi()->updatePhraseEntity($updateData);
            }

        }

        return true;
    }

    /**
     * @param array $requestData
     * @return bool
     */
    public function deleteMultiPhraseEntity(array $requestData): bool
    {
        $phrases  = $this->getBrungildaApi()->getNlpClassPhrases((int) $requestData['classifier_class_id']);
        $entities = $this->getBrungildaApi()->getClassifierClassPhrasesEntities((int) $requestData['classifier_class_id']);

        foreach ($phrases as $phrase) {
            foreach ($entities as $entity) {
                if (strpos($phrase['text'], $entity['value']) === false) {
                    continue;
                }

                if ($entity['value'] != $requestData['value']) {
                    continue;
                }

                $this->getBrungildaApi()->deletePhraseEntity((int) $entity['learning_phrase_entity_id']);
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getClassifiersAvailableModels(): array
    {
        $result = $this->getBrungildaApi()->getClassifiersStatus();

        return $result;
    }

    /**
     * @param int $classifierId
     * @return array
     */
    public function getClasses(int $classifierId): array
    {
        $classifiers = $this->getBrungildaApi()->getNlpClassifierClasses($classifierId);

        return $classifiers;
    }

    /**
     * @param int $classId
     * @return array
     */
    public function getPhrases(int $classId): array
    {
        $entitiesResult = [];
        $phrases  = $this->getBrungildaApi()->getNlpClassPhrases($classId);
        $entities = $this->getBrungildaApi()->getClassifierClassPhrasesEntities($classId);

        foreach ($phrases as $key => $phrase) {
            foreach ($entities as $entityData) {
                if ($entityData['learning_phrase_id'] == $phrase['learning_phrase_id']) {
                    $entitiesResult[$phrase['learning_phrase_id']][] = $entityData;
                }
            }
        }

        return [
            'phrasesData'  => $phrases,
            'entitiesData' => $entitiesResult
        ];
    }

    /**
     * @param $data
     * @param $type
     * @return NlpClassifier[]
     */
    private function filterClassifiersByClass($data, $type)
    {
        if (!in_array($type, self::CLASSIFIERS_TYPES)) {
            throw new InvalidParamException('Invalid type to exclude');
        }

        /** @var $data NlpClassifier [] */
        foreach ($data as $key => $classifier)
        {
            if (empty($classifier['type'])) {
                unset($data[$key]);
            }

            if($classifier['type'] == $type) {
               continue;
            }

            unset($data[$key]);
        }

        return array_values($data);
     }
}
