//Supporting variables
var currentSubstituteId         = null;
var currentSubstitutePhraseId   = null;

//Load list of substitutes
function updateSubstitutes()
{
    $.ajax({
        url: '/substitutes/substitutes',
        success: function (data) {
            //Update group phrases
            $('#substitutes').html(data);
            //Trigger click on first or current substitute item
            var substituteItem;
            if (currentSubstituteId == null) {
                substituteItem = $('#substitutes li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="substituteId"][value="' + currentSubstituteId + '"]';
                substituteItem = $('#substitutes li.list-group-item.sortable').has(str);
            }
            if (substituteItem.length == 0) {
                substituteItem = $('#substitutes li.list-group-item.sortable:first-child');
            }
            if (substituteItem.length == 0) {
                currentSubstituteId = null;
            } else {
                substituteItem.removeClass('lastClickedItem');
                substituteItem.click();
            }
        }
    });
}

//Load list of substitute phrases
function updateSubstitutePhrases()
{
    $.ajax({
        url: '/substitutes/substitute-phrases',
        type: "POST",
        data: 'substituteId=' + currentSubstituteId,
        success: function (data) {
            //Update phrases
            $('#substitutePhrases').html(data);
            //Trigger click on first substitute phrases item
            var substitutePhraseItem;
            if (currentSubstitutePhraseId == null) {
                substitutePhraseItem = $('#substitutePhrases li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="substitutePhraseId"][value="' + currentSubstitutePhraseId + '"]';
                substitutePhraseItem = $('#substitutePhrases li.list-group-item.sortable').has(str);
            }
            if (substitutePhraseItem.length == 0) {
                substitutePhraseItem = $('#substitutePhrases li.list-group-item.sortable:first-child');
            }
            if (substitutePhraseItem.length == 0) {
                currentSubstitutePhraseId = null;
            } else {
                substitutePhraseItem.removeClass('lastClickedItem');
                substitutePhraseItem.click();
            }
        }
    });
}

$(document).ready(function () {
    /**********  SUBSTITUTES FUNCTIONALITY **********/
    //Load list of substitutes
    updateSubstitutes();

    //Click on substitute item event
    $(document).on('click', '#substitutes li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        $('#substitutePhrases').html('');
        clearBindableForm('#substitutePhrasesBindableForm');
        var substituteId = $(this).children('input[name="substituteId"]').val();
        if (substituteId === undefined) {
            currentSubstituteId = null;
            return;
        }
        currentSubstituteId = substituteId;
        updateSubstitutePhrases();
    });

    //Click 'Add new substitute' button
    $(document).on('click', '#addNewSubstituteButton', function () {
        var text = $('#substituteText').val();
        text = encodeURIComponent(text);
        $.ajax({
            url: '/substitutes/create-substitute',
            type: "POST",
            data: 'text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateSubstitutes();
                } else {
                    alert('An error occurred while creating the substitute!');
                }
            }
        });
    });

    //Click 'Update substitute' button
    $(document).on('click', '#updateSubstituteButton', function () {
        var text = $('#substituteText').val();
        text = encodeURIComponent(text);
        var substituteId = $('#substitutesBindableForm input[name="substituteId"]').val();
        if (substituteId.length == 0) {
            return;
        }
        $.ajax({
            url: '/substitutes/update-substitute',
            type: "POST",
            data: 'substituteId=' + substituteId + '&text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateSubstitutes();
                } else {
                    alert('An error occurred while updating the substitute!');
                }
            }
        });
    });

    //Click substitute delete button
    $(document).on('click', '#deleteSubstituteButton', function () {
        var substituteName = $('#substituteText').val();
        var substituteId = $('#substitutesBindableForm input[name="substituteId"]').val();
        if (substituteId.length == 0) {
            return;
        }
        $('#deleteSubstituteId').val(substituteId);
        $('#deleteSubstituteName').html(substituteName);
        $('#confirmDeleteSubstituteModal').modal('show');
    });

    //Click confirm substitute delete button
    $(document).on('click', '#confirmDeleteSubstituteButton', function () {
        var substituteId = $('#deleteSubstituteId').val();
        if (substituteId.length == 0) {
            return;
        }
        $.ajax({
            url: '/substitutes/delete-substitute',
            type: "POST",
            data: 'substituteId=' + substituteId,
            success: function (data) {
                if (data == 'true') {
                    updateSubstitutes();
                } else {
                    alert('An error occurred while deleting the substitute!');
                }
                $('#confirmDeleteSubstituteModal').modal('hide');
            }
        });
    });

    /**********  SUBSTITUTE PHRASES FUNCTIONALITY **********/
    //Click on substitutes phrases item event
    $(document).on('click', '#substitutePhrases li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        var substitutePhraseId = $(this).children('input[name="substitutePhraseId"]').val();
        if (substitutePhraseId === undefined) {
            currentSubstitutePhraseId = null;
            return;
        }
        currentSubstitutePhraseId = substitutePhraseId;
    });

    //Click 'Add new substitute phrase' button
    $(document).on('click', '#addNewSubstitutePhraseButton', function () {
        var substituteId = currentSubstituteId;
        if (substituteId.length == 0) {
            return;
        }
        var text = $('#substitutePhraseText').val();
        text = encodeURIComponent(text);
        $.ajax({
            url: '/substitutes/create-substitute-phrase',
            type: "POST",
            data: 'text=' + text + '&substituteId=' + substituteId,
            success: function (data) {
                if (data == 'true') {
                    updateSubstitutePhrases();
                } else {
                    alert('An error occurred while creating the substitute phrase!');
                }
            }
        });
    });

    //Click substitute phrase delete button
    $(document).on('click', '#deleteSubstitutePhraseButton', function () {
        var substitutePhraseName = $('#substitutePhraseText').val();
        var substituteId = $('#substitutePhrasesBindableForm input[name="substituteId"]').val();
        var substitutePhraseId = $('#substitutePhrasesBindableForm input[name="substitutePhraseId"]').val();
        if (substituteId.length == 0 || substitutePhraseId == 0) {
            return;
        }
        $('#deleteSubstitutePhraseParentId').val(substituteId);
        $('#deleteSubstitutePhraseId').val(substitutePhraseId);
        $('#deleteSubstitutePhraseName').html(substitutePhraseName);
        $('#confirmDeleteSubstitutePhraseModal').modal('show');
    });

    //Click confirm substitute phrase delete button
    $(document).on('click', '#confirmDeleteSubstitutePhraseButton', function () {
        var substituteId = $('#deleteSubstitutePhraseParentId').val();
        var substitutePhraseId = $('#deleteSubstitutePhraseId').val();
        if (substituteId.length == 0 || substitutePhraseId.length == 0) {
            return;
        }
        $.ajax({
            url: '/substitutes/delete-substitute-phrase',
            type: "POST",
            data: 'substituteId=' + substituteId + '&substitutePhraseId=' + substitutePhraseId,
            success: function (data) {
                if (data == 'true') {
                    updateSubstitutePhrases();
                } else {
                    alert('An error occurred while deleting the substitute phrase!');
                }
                $('#confirmDeleteSubstitutePhraseModal').modal('hide');
            }
        });
    });

    //Click 'Update substitute phrase' button
    $(document).on('click', '#updateSubstitutePhraseButton', function () {
        var text = $('#substitutePhraseText').val();
        text = encodeURIComponent(text);
        var substituteId = $('#substitutePhrasesBindableForm input[name="substituteId"]').val();
        var substitutePhraseId = $('#substitutePhrasesBindableForm input[name="substitutePhraseId"]').val();
        if (substituteId.length == 0 || substitutePhraseId.length == 0) {
            return;
        }
        $.ajax({
            url: '/substitutes/update-substitute-phrase',
            type: "POST",
            data: 'substituteId=' + substituteId + '&substitutePhraseId=' + substitutePhraseId + '&text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateSubstitutePhrases();
                } else {
                    alert('An error occurred while updating the substitute phrase!');
                }
            }
        });
    });
});
