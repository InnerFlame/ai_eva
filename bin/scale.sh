#!/usr/bin/env bash

if [ "$DOCKER_ENV" == "prod" ]; then
    source ./bin/prodEnv.sh
    DC="docker-compose ${DOCKER_COMPOSE_FILES}"

    ${DC} up --scale app_build=0 ${RUN_ARGS} ${SCALE_SERVICES}
fi
