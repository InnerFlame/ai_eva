import { Component }           from '@angular/core';
import { Title }               from "@angular/platform-browser";
import { HttpService }         from "../shared/services/http.service";
import { OnInit }              from '@angular/core';
import { SettingsCreateModel } from "../shared/models/settings.model";
import { DragulaService }      from "ng2-dragula";
import { Router }              from "@angular/router";

@Component({
    selector    : 'settings',
    templateUrl : './settings.index.html',
})

export class SettingsComponent implements OnInit
{
    public settingsModel        : SettingsCreateModel = new SettingsCreateModel();
    public disableTargetSelect  : boolean             = true;
    public disableControlSelect : boolean             = true;
    public displayLoadingBlock  : boolean             = true;
    public displaySettingsForm  : boolean             = true;
    public errorText            : string              = '';
    public successText          : string              = '';
    public disableDragClass     : string              = '';
    public settingRecords       : any;
    public settingRelations     : any;
    public controlProfiles      : Array<Object>;
    public targetProfiles       : Array<Object>;

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param dragulaService
     * @param titleService
     * @param router
     */
    constructor(
        private httpService: HttpService,
        private dragulaService: DragulaService,
        private titleService: Title,
        private router:Router
    )
    {
        this.titleService.setTitle( 'Settings' );

        dragulaService.drop.subscribe((value) => {
            this.disableDragClass = 'disableDrag';
            const [bagName, e, el] = value;

            let newPrioritiesIds = this.getUpdatedPrioritiesData(e);

            this.updateSettingOrderRequest(newPrioritiesIds);
        });
    }

    /**
     * On init script actions and events, here we bind available models
     */
    ngOnInit(): void
    {
        this.httpService.get(SettingsCreateModel.getSettingModelsApiUrl()).then(data => {
            if (!data['models']) {
                this.errorText = 'Something goes wrong with response!';
            } else {
                this.settingRecords = data['models'];
                this.settingRelations = data['relations'];
            }
            this.displayLoadingBlock = false;
        });

        this.httpService.get(SettingsCreateModel.getCreateDropDownDataApiUrl()).then(data => {
            if (!data) {
                this.errorText = 'Something goes wrong with response!';
            } else {
                this.controlProfiles       = data['controlProfiles'];
                this.disableControlSelect  = false;
                this.targetProfiles        = data['targetProfiles'];
                this.disableTargetSelect   = false;
            }
        });
    }

    /**
     * Submit form handler
     */
    onSubmit(): void
    {
        this.displayLoadingBlock = true;

        const body = {
            name:           this.settingsModel.name,
            controlProfile: this.settingsModel.controlProfile,
            targetProfile:  this.settingsModel.targetProfile
        };

        this.httpService.post(SettingsCreateModel.getCreateSettingApiUrl(), body)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.errorText  = 'Something goes wrong with create action!';
                } else {
                    this.settingRecords = data['models'];
                    this.settingRelations = data['relations'];

                    this.successText = 'Setting was created successfully!!'
                }

                this.displayLoadingBlock = false;
            });
    }

    /**
     * Change active status for each setting
     *
     * @param settingId
     */
    toggleSettingActiveStatus(settingId)
    {
        if (!confirm('Do you really want to change setting status??')) {
            return;
        }

        this.displayLoadingBlock = true;

        const body = {settingId: settingId};

        this.httpService.post(SettingsCreateModel.getActivationToggleApiUrl(), body)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.errorText  = 'Something goes wrong with activation action!';
                } else {
                    this.settingRecords = data['models'];
                    this.settingRelations = data['relations'];

                }

                this.displayLoadingBlock = false;
            });
    }

    copySetting(settingId: number, name: string)
    {
        if (!confirm("Do you really wish to copy setting '" + name + "' ?")) {
            return;
        }

        this.displayLoadingBlock = true;

        const body = {settingId: settingId};

        this.httpService.post(SettingsCreateModel.getCopySettingApiRul(), body)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.errorText  = 'Something goes wrong with activation action!';
                } else {
                    this.router.navigateByUrl("/settings-ng/update/" + data['settingId']);
                }

                this.displayLoadingBlock = false;
            });
    }

    /**
     * Delete setting with all relations with confirmation
     *
     * @param settingId
     * @param name
     */
    deleteSetting(settingId, name): void
    {
        if (!confirm("Do you really wish to delete setting '" + name + "' ?")) {
            return;
        }
        this.displayLoadingBlock = true;

        const body = {settingId: settingId};

        this.httpService.post(SettingsCreateModel.getDeleteSettingApiUrl(), body)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.errorText  = 'Something goes wrong with create action!';
                } else {
                    this.settingRecords = data['models'];
                    this.settingRelations = data['relations'];

                }

                this.displayLoadingBlock = false;
            });
    }

    /**
     * Updates records order
     *
     * @param newPrioritiesIds
     */
    private updateSettingOrderRequest(newPrioritiesIds: Array<any>): void
    {
        let alreadySend  = false;

        if (alreadySend) {
            return;
        }

        this.httpService.post(SettingsCreateModel.getUpdateOrderApiUrl(), {newPrioritiesIds})
            .then(data => {
            if (!data) {
                this.errorText = 'Something goes wrong with response!';
            }
            alreadySend = true;
            this.disableDragClass = '';
        });
    }

    /**
     * Get elemetn index(id) for dragula onDrag event
     *
     * @param el
     */
    private getUpdatedPrioritiesData(el: any)
    {
        let newIdsPriority = [];
        let elements = [].slice.call(el.parentElement.children);

        console.log(elements);
        for (let i = 0; i < elements.length; i++) {
            console.log(elements[i]);
            newIdsPriority.push(elements[i].dataset.id);
        }

        return newIdsPriority;
    }

    /**
     * Just call {{diagnostic}} in template and watch model status in live time
     *
     * @returns {string}
     */
    get diagnostic() { return JSON.stringify(this.settingsModel); }
}
