<?php
use common\widgets\BindableFormItems\BindableFormItems;

$groupPhrasesItems = array();
foreach ($model->groupsPhrases as $groupPhrases) {
    $item['text'] = $groupPhrases->getText();
    $item['hiddens'] = [
        ['name' => 'groupPhrasesId',                    'value' => $groupPhrases->getId()],
        ['name' => 'groupGroupsPhrasesId',              'value' => $groupPhrases->getGroupGroupsPhrasesId()],
        ['name' => 'currentGroupPhrasesLanguageText',   'value' => $groupPhrases->getLanguageTag()],
        ['name' => 'currentGroupPhrasesLanguage',       'value' => $groupPhrases->getLanguageTag()],
        ['name' => 'groupPhrasesTags',                  'value' => json_encode($groupPhrases->getTags())],
    ];
    $item['rightElement'] = '<button type="button" class="btn btn-default btn-sm pull-right';
    if ($groupPhrases->getExcludePhrase() == 1) {
        $item['rightElement'] .= ' active';
    }
    $item['rightElement'] .= '" name="excludePhrasesButton"><span class="glyphicon glyphicon-ok"></span> Exc</button>';

    $item['rightElement'] .= '<button type="button" class="btn btn-default btn-sm pull-right';
    if ($groupPhrases->getHasPlaceholder() == 1) {
        $item['rightElement'] .= ' active';
    }
    $item['rightElement'] .= '" name="hasPlaceholderButton"><span class="glyphicon glyphicon-ok"></span> Link</button>';

    $item['rightElement'] .= '<button type="button" class="btn btn-default btn-sm pull-right';
    if ($groupPhrases->getHasPrePlaceholder() == 1) {
        $item['rightElement'] .= ' active';
    }
    $item['rightElement'] .= '" name="hasPrePlaceholderButton"><span class="glyphicon glyphicon-ok"></span> Pre</button>';

    $groupPhrasesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'groupPhrasesBindableForm',
    'items' => $groupPhrasesItems,
    'checked' => true,
]);
