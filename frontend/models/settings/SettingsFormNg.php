<?php

namespace frontend\models\settings;

use frontend\models\BaseNgForm;
use frontend\models\DataProvider;
use yii\helpers\ArrayHelper;

class SettingsFormNg extends BaseNgForm
{
    /**
     * @param string $name
     * @param string $controlProfile
     * @param string $targetProfile
     * @return bool
     */
    public function createSetting(
        string $name,
        string $controlProfile,
        string $targetProfile
    ): bool
    {
        $setting                  = new Setting();
        $setting->name            = $name;
        $setting->control_profile = $controlProfile;
        $setting->target_profile  = $targetProfile;
        $setting->active          = false;

        return $setting->save();
    }

    /**
     * @param $formData array
     * @return bool
     */
    public function updateSetting(array $formData): bool
    {
        $this->deleteSettingRelationsByCondition('setting_id = ' . $formData['id']);
        $setting                  = $this->getSettingWithAllRealtions($formData['id']);
        $setting->name            = $formData['name'];
        $setting->control_profile = $formData['controlProfile'];
        $setting->target_profile  = $formData['targetProfile'];
        if (!empty($formData['splitName'])) {
            $setting->splitName = $formData['splitName'];
            $setting->splitGroup = $formData['splitGroup'];
        } else {
            $setting->splitName = null;
            $setting->splitGroup = null;
        }

        $setting = $this->linkAllSettingRelations(
            $formData['projects'],
            $formData['countries'],
            $formData['locales'],
            $formData['senderLocales'],
            $formData['statuses'],
            $formData['sites'],
            $formData['traffSources'],
            $formData['actionWays'],
            $formData['traffTypes'],
            $formData['platforms'],
            $formData['fanClubs'],
            $formData['onlineStatuses'],
            $formData['scenarios'],
            $setting
        );

        $setting->active = $formData['isActive'];

        if (!$setting->save()) {
            return false;
        }
        
        $this->combineSetting((int) $formData['id']);

        return true;
    }

    public function updateSettingsPriorities(array $newPrioritiesIds): bool
    {
        $status = false;
        $newPos = 1;
        foreach ($newPrioritiesIds as $settingId) {
            $settingModel = Setting::findOne($settingId);

            if (!$settingModel) {
                continue;
            }

            $settingModel->priority = (int) $newPos;
            $status = $settingModel->save();

            $newPos++;
        }

        return (bool) $status;
    }

    /**
     * @param int $settingId
     * @return array
     */
    public function toggleSettingActiveStatus(int $settingId): array
    {
        $settingModel = Setting::findOne($settingId);

        if (empty($settingModel) || is_null($settingModel)) {
            return [];
        }

        $activeStatus = ($settingModel->active == 0) ? 1 : 0;
        $settingModel->active = $activeStatus;
        if (!$settingModel->save()) {
            return [];
        }

        ConsolidatedSettings::deleteAll(['setting_id' => $settingId]);
        $this->combineSetting($settingId);

        return ['status' => true];
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteSettingById(int $id): bool
    {
        $settingIdCondition = 'setting_id = ' . $id;
        $this->deleteSettingRelationsByCondition($settingIdCondition);
        Setting::deleteAll('id = ' . $id);
        ConsolidatedSettings::deleteAll($settingIdCondition);

        return true;
    }

    /**
     * @param Setting $settingModel
     * @return array
     */
    public function getUpdateSettingInitialData(Setting $settingModel): array
    {
        $holdersData = [
            'availableCountries'    => $this->filterDataForMultiSelects(\Yii::$app->params['countries']),
            'availableLocales'      => $this->filterDataForMultiSelects(\Yii::$app->params['availableLanguages']),
            'availableStatuses'     => $this->filterDataForMultiSelects(\Yii::$app->params['statuses']),
            'availableSites'        => $this->transformHolderKeyValue(\Yii::$app->params['sites']),
            'availableTraffSources' => $this->filterDataForMultiSelects(\Yii::$app->params['trafficSources']),
            'availableActionWays'   => $this->filterDataForMultiSelects(\Yii::$app->params['actionWays']),
            'availableTraffTypes'   => $this->filterDataForMultiSelects(\Yii::$app->params['trafficTypes']),
            'availablePlatforms'    => $this->filterDataForMultiSelects(\Yii::$app->params['platforms']),
            'availableFanClubs'     => $this->transformHolderKeyValue(\Yii::$app->params['fanClubs']),
            'availableOnlines'      => $this->transformHolderKeyValue(\Yii::$app->params['onlines']),
            'availableProjects'     => $this->filterDataForMultiSelects(\Yii::$app->params['projects']),
            'availableProfiles'     => $this->getAvailableProfilesData(),
            'availableScenarios'    => $this->getScenariosForHolder(),
        ];

        $relationData = [
            'projects'     => ArrayHelper::getColumn($settingModel->projects, 'project_name'),
            'countries'    => ArrayHelper::getColumn($settingModel->countries, 'country_name'),
            'locales'      => ArrayHelper::getColumn($settingModel->locales, 'locale_name'),
            'senderLocales' => ArrayHelper::getColumn($settingModel->senderLocales, 'locale_name'),
            'statuses'     => ArrayHelper::getColumn($settingModel->statuses, 'status_name'),
            'sites'        => ArrayHelper::getColumn($settingModel->sites, 'site_hash'),
            'traffSources' => ArrayHelper::getColumn($settingModel->trafficSources, 'traffic_source_name'),
            'actionWays'   => ArrayHelper::getColumn($settingModel->actionWays, 'action_way_name'),
            'traffTypes'   => ArrayHelper::getColumn($settingModel->trafficTypes, 'traffic_type_name'),
            'platforms'    => ArrayHelper::getColumn($settingModel->platforms, 'platform_name'),
            'fanClubs'     => ArrayHelper::getColumn($settingModel->fanClubs, 'is_fan_club_allowed'),
            'onlines'      => ArrayHelper::getColumn($settingModel->onlines, 'is_online'),
            'scenarios'    => $this->getScenariosForRelations($settingModel->scenarios, $holdersData['availableScenarios'])
        ];

        return [
            'modelData' => ['original' => $settingModel, 'relations' => $relationData],
            'holders'   => $holdersData
        ];
    }

    /**
     * @return array
     */
    public function getAvailableProfilesData(): array
    {
        $availableControlProfiles = \Yii::$app->params['controlProfiles'];
        $availableTargetProfiles  = \Yii::$app->params['targetProfiles'];

        return [
            'controlProfiles' => $availableControlProfiles,
            'targetProfiles'  => $availableTargetProfiles
        ];
    }

    /**
     * @param array $settingScenarios
     * @param array $availableScenarios
     * @return array
     */
    private function getScenariosForRelations(
        array $settingScenarios,
        array $availableScenarios
    ): array
    {
        $data  = [];
        if(is_null($availableScenarios)) {
            $availableScenarios = $this->getScenariosForHolder();
        }

        foreach ($settingScenarios as $settingScenario) {
            $nameKey = array_search($settingScenario->scenario_id, array_column($availableScenarios, 'id'));
            $data[] = [
                'id'      => $settingScenario->scenario_id,
                'isFixed' => $settingScenario->isFixed,
                'name'    => $availableScenarios[$nameKey]['name']
            ];
        }

        return $data;
    }

    /**
     * @param $settingScenarios
     * @return array
     */
    public function getScenariosForIndexRelations(array $settingScenarios): array
    {
        $data  = [];
        $availableScenarios = $this->getScenariosForHolder();

        foreach ($settingScenarios as $settingScenario) {
            $nameKey = array_search($settingScenario['scenario_id'], array_column($availableScenarios, 'id'));
            $data[] = [
                'id'      => $settingScenario['scenario_id'],
                'isFixed' => $settingScenario['isFixed'],
                'name'    => $availableScenarios[$nameKey]['name']
            ];
        }

        return $data;
    }

    /**
     * @param $settingId
     */
    private function combineSetting(int $settingId)
    {
        \Yii::$app->db->createCommand('
            DELETE FROM `consolidatedSettings`
            WHERE setting_id = :settingId
        ')
            ->bindParam(':settingId', $settingId)
            ->execute();

        \Yii::$app->db->createCommand('
            INSERT INTO `consolidatedSettings`
                (`setting_id`,`name`,`control_profile`,`target_profile`,
                `active`,`project_name`,`country_name`,`locale_name`,`sender_locale_name`,`status_name`,`site_hash`,
                `traffic_type_name`,`platform_name`,`is_fan_club_allowed`,`is_online`,`priority`,`splitName`,`splitGroup`)
            (
            SELECT
                `settings_table`.`id`,`name`,`control_profile`,`target_profile`,
                `active`, IF(`project_name` IS NULL,\'\',`project_name`),`country_name`,settings_locales.locale_name,settings_sender_locales.locale_name,`status_name`,`site_hash`,
                `traffic_type_name`,`platform_name`,`is_fan_club_allowed`,`is_online`,`priority`,`splitName`,`splitGroup`
            FROM settings_table
            JOIN settings_countries_table ON settings_table.id = settings_countries_table.setting_id
            JOIN settings_locales ON settings_table.id = settings_locales.setting_id
            JOIN settings_sender_locales ON settings_table.id = settings_sender_locales.setting_id
            JOIN settings_statuses_table ON settings_table.id = settings_statuses_table.setting_id
            JOIN settings_sites_table ON settings_table.id = settings_sites_table.setting_id
            JOIN settings_traffic_types_table ON settings_table.id = settings_traffic_types_table.setting_id
            JOIN settings_platforms_table ON settings_table.id = settings_platforms_table.setting_id
            JOIN settings_fan_club ON settings_table.id = settings_fan_club.setting_id
            JOIN settings_online ON settings_table.id = settings_online.setting_id
            LEFT JOIN settings_projects ON settings_table.id = settings_projects.setting_id
            WHERE `settings_table`.id = :settingId
            )
        ')
            ->bindParam(':settingId', $settingId)
            ->execute();
    }

    /**
     * @param $modelSources array
     * @return array
     */
    public function getExcludedTrafficSourcesForIndex(array $modelSources): array
    {
        if (empty($modelSources)) {
            return [];
        }

        $out              = [];
        $availableSources = \Yii::$app->params['trafficSources'];
        $modelSources = ArrayHelper::getColumn($modelSources, 'traffic_source_name');
        foreach ($availableSources as $source) {
            if (!in_array($source, $modelSources)) {
                $out[] = $source;
            }
        }

        return $out;
    }

    /**
     * @param $holderItems
     * @return array
     */
    public function transformHolderKeyValue(array $holderItems): array
    {
        $out = [];

        foreach ($holderItems as $id => $name) {
            $out[] = ['name' => $name, 'id' => $id];
        }

        return $out;
    }

    /**
     * @param $settingIdCondition
     */
    private function deleteSettingRelationsByCondition($settingIdCondition)
    {
        Project::deleteAll($settingIdCondition);
        Country::deleteAll($settingIdCondition);
        Locale::deleteAll($settingIdCondition);
        SenderLocale::deleteAll($settingIdCondition);
        Status::deleteAll($settingIdCondition);
        Site::deleteAll($settingIdCondition);
        TrafficSource::deleteAll($settingIdCondition);
        ActionWay::deleteAll($settingIdCondition);
        TrafficType::deleteAll($settingIdCondition);
        Platform::deleteAll($settingIdCondition);
        FanClub::deleteAll($settingIdCondition);
        Online::deleteAll($settingIdCondition);
        Scenario::deleteAll($settingIdCondition);
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    private function getSettingWithAllRealtions($id)
    {
        $setting = Setting::find()
            ->with('projects')
            ->with('countries')
            ->with('locales')
            ->with('statuses')
            ->with('sites')
            ->with('trafficSources')
            ->with('actionWays')
            ->with('trafficTypes')
            ->with('platforms')
            ->with('fanClubs')
            ->with('onlines')
            ->with('scenarios')
            ->where(['id' => $id])
            ->one();
        return $setting;
    }

    /**
     * @param $projects
     * @param $countries
     * @param $locales
     * @param $statuses
     * @param $sites
     * @param $trafficSources
     * @param $actionWays
     * @param $trafficTypes
     * @param $platforms
     * @param $fanClubs
     * @param $onlines
     * @param $scenarios
     * @param $setting
     * @return mixed
     */
    private function linkAllSettingRelations(
        $projects,
        $countries,
        $locales,
        $senderLocales,
        $statuses,
        $sites,
        $trafficSources,
        $actionWays,
        $trafficTypes,
        $platforms,
        $fanClubs,
        $onlines,
        $scenarios,
        $setting
    ) {

        foreach ($projects as $val) {
            if (!empty($val)) {
                $project = new Project();
                $project->project_name = $val;
                $project->link('setting', $setting);
            }
        }

        foreach ($countries as $val) {
            $country = new Country();
            $country->country_name = $val;
            $country->link('setting', $setting);
        }

        foreach ($locales as $val) {
            $locale = new Locale();
            $locale->locale_name = $val;
            $locale->link('setting', $setting);
        }

        foreach ($senderLocales as $val) {
            $locale = new SenderLocale();
            $locale->locale_name = $val;
            $locale->link('setting', $setting);
        }

        foreach ($statuses as $val) {
            $status = new Status();
            $status->status_name = $val;
            $status->link('setting', $setting);
        }

        array_unique($sites);
        foreach ($sites as $val) {
            $site = new Site();
            $site->site_hash = $val;
            $site->link('setting', $setting);
        }

        foreach ($trafficSources as $val) {
            $trafficSource = new TrafficSource();
            $trafficSource->traffic_source_name = $val;
            $trafficSource->link('setting', $setting);
        }

        foreach ($actionWays as $val) {
            $actionWay = new ActionWay();
            $actionWay->action_way_name = $val;
            $actionWay->link('setting', $setting);
        }

        foreach ($trafficTypes as $val) {
            $trafficType = new TrafficType();
            $trafficType->traffic_type_name = $val;
            $trafficType->link('setting', $setting);
        }

        foreach ($platforms as $val) {
            $platform = new Platform();
            $platform->platform_name = $val;
            $platform->link('setting', $setting);
        }

        foreach ($fanClubs as $val) {
            if ($val != '' || $val == 0) {
                $fanClub = new FanClub();
                $fanClub->is_fan_club_allowed = $val;
                $fanClub->link('setting', $setting);
            }
        }

        foreach ($onlines as $val) {
            if ($val != '' || $val == 0) {
                $online = new Online();
                $online->is_online = $val;
                $online->link('setting', $setting);
            }
        }

        $scenarioCounter = 1;
        $scenarios = array_filter($scenarios);
        if (is_array($scenarios)) {
            foreach ($scenarios as $val) {
                $scenario = new Scenario();
                $scenario->scenario_id = (int) $val['id'];
                $scenario->isFixed = $val['isFixed'];
                $scenario->scenario_priority = $scenarioCounter;
                $scenario->link('setting', $setting);
                $scenarioCounter++;
            }
        }

        return $setting;
    }

    public function copySetting(int $id)
    {
        $setting = Setting::findOne($id);
        unset($setting->id);
        $setting->isNewRecord = true;
        $setting->save();
        $newId = $setting->id;

        foreach (Project::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Country::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Locale::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (SenderLocale::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Status::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Site::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (TrafficSource::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (ActionWay::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (TrafficType::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Platform::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (FanClub::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Online::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Scenario::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        $this->combineSetting((int)$newId);

        return $newId;
    }
}
