<?php
declare(strict_types=1);

namespace frontend\models\nlpClassifiers;

class NlpClassifierClass
{
    public $id;
    public $classifierId;
    public $text;
    public $probability;
    public $phrases;
    public $type;

    public function __construct(int $id, int $classifierId, string $text, int $probability, array $phrases)
    {
        $this->id           = $id;
        $this->classifierId = $classifierId;
        $this->text         = $text;
        $this->probability  = $probability;
        $this->phrases      = $phrases;
    }
}
