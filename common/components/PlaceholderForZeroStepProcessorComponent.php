<?php
namespace common\components;

class PlaceholderForZeroStepProcessorComponent extends AbstractMessageProcessorComponent
{
    protected $isPredispatched = true;

    protected function getEvaAnswer($data)
    {
        return \Yii::$app->eva->getPlaceholderPhrase($data);
    }

    protected function saveIncomingMessageToLog($sender, $recipient, $text, $scenarioId)
    {
        //don`t need to save incoming message to log
    }

    protected function saveOutgoingMessageToLog($sender, $recipient, $text, $scenarioId, $firstUsedPlaceholderId, $evaResponse, $predispatched = false)
    {
        return parent::saveOutgoingMessageToLog($sender, $recipient, $text, $scenarioId, $firstUsedPlaceholderId, $evaResponse, true);
    }
}
