import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { SharedModule }                  from "../shared";
import { ErrorLogComponent }             from "./error.log.component";
import { PagerService }                  from "../shared/services";

const phrasesRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path      : 'error-log',
        component : ErrorLogComponent
    },
    {
        path      : 'error-log/:page',
        component : ErrorLogComponent
    },
]);

@NgModule({
    imports: [
        phrasesRouting,
        SharedModule
    ],
    declarations: [
        ErrorLogComponent
    ],
    providers: [
        PagerService
    ]
})

export class ErrorLogModule {}