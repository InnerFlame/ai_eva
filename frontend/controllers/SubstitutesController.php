<?php

namespace frontend\controllers;

use frontend\models\substitutes\SubstitutesForm;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class SubstitutesController
 * @package frontend\controllers
 */
class SubstitutesController extends BaseNgController 
{
    /**
     * @inheritdoc
     * SubstitutesController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new SubstitutesForm());
    }

    /**
     * @inheritdoc
     *
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (!parent::beforeAction($action)) {
           return false;
        }

        if (!\Yii::$app->user->can('substitutes')) {
            throw new ForbiddenHttpException('Access denied');
        }

        return true;
    }

    /**
     * Bootstrap page
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionGetSubstitutes(): array
    {
        $substitutes = $this->getFormModel()->getSubstitutes();

        return $substitutes;
    }

    /**
     * @param $substituteId
     * @return array
     */
    public function actionGetSubstitutePhrases($substituteId): array
    {
        $substitutePhrases = $this->getFormModel()
            ->getSubstitutePhrasesById((int) $substituteId);

        return $substitutePhrases;
    }

    /**
     * @return array
     */
    public function actionCreateSubstitute(): array
    {
        if (empty($this->requestData)) {
            return [];
        }

        $createSubstitute = $this->getFormModel()
            ->createSubstitute($this->requestData);

        return $createSubstitute;
    }

    /**
     * @return array
     */
    public function actionCreateSubstitutePhrase(): array
    {
        if (empty($this->requestData)) {
            return [];
        }

        $this->getFormModel()
            ->createSubstitutePhrase($this->requestData);

        return $this->getFormModel()
            ->getSubstitutePhrasesById((int) $this->requestData['substitute_id']);
    }

    /**
     * @return array
     */
    public function actionUpdateSubstitute(): array
    {
        if (empty($this->requestData)) {
            return [];
        }

        $substituteUpdated = $this->getFormModel()
            ->updateSubstitute($this->requestData);

        return $substituteUpdated;
    }

    /**
     * @return array
     */
    public function actionUpdateSubstitutePhrase(): array
    {
        if (empty($this->requestData)) {
            return [];
        }

        $this->getFormModel()
            ->updateSubstitutePhrase($this->requestData);

        return $this->getFormModel()
            ->getSubstitutePhrasesById((int) $this->requestData['substitute_id']);
    }

    /**
     * @return array
     */
    public function actionDeleteSubstitute(): array
    {
        if (empty($this->requestData)) {
            return [];
        }
        $substituteDeleted = $this->getFormModel()
            ->deleteSubstitute($this->requestData);

        return $substituteDeleted;
    }

    /**
     * @return array
     */
    public function actionDeleteSubstitutePhrase(): array
    {
        if (empty($this->requestData)) {
            return [];
        }

        $this->getFormModel()
            ->deleteSubstitutePhrase($this->requestData);

        return $this->getFormModel()
            ->getSubstitutePhrasesById((int) $this->requestData['substitute_id']);
    }

    /**
     * @inheritdoc
     *
     * @return SubstitutesForm
     */
    protected function getFormModel(): SubstitutesForm
    {
        return $this->formModel;
    }
}
