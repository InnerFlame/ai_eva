<?php
/**
 * @var $response array
 */

?>

    <div class="col-md-3">
        <h4>Intent</h4>
        <?php if(isset($response['intent'])): ?>
            <span>Confidence: <?= $response['intent']['confidence'] ?? 'none';?></span><br>
            <span>Name: <?= $response['intent']['name'] ?? 'none';?></span>
        <?php endif; ?>
    </div>
    <div class="col-md-3">
        <h4>Intent Ranking</h4>
        <?php if(isset($response['intent_ranking'])): ?>
            <?php foreach ($response['intent_ranking'] as $value): ?>
                <span>Confidence: <?= $value['confidence'] ?? 'none';?></span><br>
                <span>Name: <?= $value['name'] ?? 'none'; ?></span><br>
                <span>------------------------------------------------</span><br><br>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <div class="col-md-3">
        <h4>Entities</h4>
        <?php if(isset($response['entities'])): ?>
            <?php foreach ($response['entities'] as $entity): ?>
                <span>Entity: <?= $entity['entity'] ?? 'none';?></span><br>
                <span>Value: <?= $entity['value'] ?? 'none'; ?></span><br>
                <span>Start: <?= $entity['start'] ?? 'none'; ?>; End: <?= $entity['end'] ?? 'none'; ?></span><br>
                <span>Extractor: <?= $entity['extractor'] ?? 'none'; ?></span><br>
                <span>------------------------------------------------</span><br><br>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>