<?php

namespace frontend\assets;

class CommonInfoAsset extends BasicAsset
{
    public $css = [
        'css/commonInfo.css',
    ];
    public $js = [
        'js/commonInfo.js',
    ];
}
