/*
* Get binding element by initial element, bindingGlyphiconClosest, bindingGlyphiconFind
* el - initial element
* bindingGlyphiconClosest, bindingGlyphiconFind - path to binding element
* el.closest(bindingGlyphiconClosest).find(bindingGlyphiconFind)
* where closest - closest parent element,
* find - children elements
*/
function getBindingEl(el, bindingGlyphiconClosest, bindingGlyphiconFind)
{
    var bindingEl = null;
    if (bindingGlyphiconClosest != '' || bindingGlyphiconFind != '') {
        bindingEl = $(el);
        if (bindingGlyphiconClosest != '') {
            bindingEl = bindingEl.closest(bindingGlyphiconClosest);console.log(bindingEl.html());
        }
        if (bindingGlyphiconFind != '') {
            bindingEl = bindingEl.find(bindingGlyphiconFind);console.log(bindingEl.html());
        }
    }
    return bindingEl;
}

/*
* Click radiobutton function:
* el - radiobutton element (in html we send 'this')
* text - text of radiobutton when it is not active
* activeText - text of radiobutton when it is active
* glyphiconClass - class of glyphicon image (image near the text)
* bindingGlyphiconClosest, bindingGlyphiconFind - path to binding element
* which must have 'active' class when radiobutton is active. Path is like
* el.closest(bindingGlyphiconClosest).find(bindingGlyphiconFind)
* where closest - closest parent element,
* find - children elements
*/
function radioButtonClick(el, text, activeText, glyphiconClass, bindingGlyphiconClosest, bindingGlyphiconFind)
{
    var bindingEl = getBindingEl(el, bindingGlyphiconClosest, bindingGlyphiconFind);
    if ($(el).attr('aria-pressed') == 'false' || $(el).attr('aria-pressed') == undefined) {
        //Click before button become active
        $(el).html('<span class="glyphicon ' + glyphiconClass +'"></span> ' + activeText);
        if (bindingEl !== null) {
            bindingEl.children('span.' + glyphiconClass).addClass('active');
        }
    } else {
        //Click before button become not active
        $(el).html('<span class="glyphicon ' + glyphiconClass +'"></span> ' + text);
        if (bindingEl !== null) {
            bindingEl.children('span.' + glyphiconClass).removeClass('active');
        }
    }
}
