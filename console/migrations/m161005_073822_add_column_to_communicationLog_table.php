<?php

use yii\db\Migration;

class m161005_073822_add_column_to_communicationLog_table extends Migration
{
    public function up()
    {
        $this->addColumn('communicationLog', 'message', $this->text());
    }

    public function down()
    {
        $this->dropColumn('communicationLog', 'message');
    }
}
