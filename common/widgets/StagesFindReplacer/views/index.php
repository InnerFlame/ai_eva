<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\widgets\StagesFindReplacer\models\StagesFindReplaceForm;

/* @var StagesFindReplaceForm $model */

?>

<?php Pjax::begin([
    'id' => 'stage-replacer-pjax',
    'enablePushState' => false,
]); ?>

    <?php $form = ActiveForm::begin([
        'id' => 'stage-replacer-form',
        'action' => Url::to('phrases/replace-stages'),
        'method' => 'post',
        'options' => [
            'style' => 'margin-top:15px',
            'class' => 'form-horizontal bindable-form',
            'data-pjax' => true,
        ],
    ]); ?>

        <div class="form-group">
            <?= $form->field($model, 'find', ['options' => ['class' => 'col-sm-12']])
                ->textarea(['rows' => '1', 'placeholder' => $model->getAttributeLabel('find')])->label(false); ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'replace', ['options' => ['class' => 'col-sm-12']])
                ->textarea(['rows' => '1', 'placeholder' => $model->getAttributeLabel('replace')])->label(false); ?>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <?= Html::button('Replace', [
                    'id' => 'stage-replacer-button',
                    'class' => 'btn btn-primary btn-block',
                    'data-toggle' => 'modal',
                    'data-target' => '#stage-replacer-modal',
                ]); ?>
            </div>
        </div>

        <div id="stage-replacer-modal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Replace stages</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure want to replace stages?
                    </div>
                    <div class="modal-footer">
                        <button type="submit"
                                class="btn btn-secondary"
                                id="stage-replacer-button-cancel"
                                data-dismiss="modal"
                        >Close</button>
                        <button type="submit"
                                class="btn btn-primary"
                                id="stage-replacer-button-confirm"
                        >Accept</button>
                    </div>
                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>