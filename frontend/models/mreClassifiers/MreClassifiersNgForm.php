<?php

namespace frontend\models\mreClassifiers;

use frontend\models\BaseNgForm;

/**
 * Class MreClassifiersNgForm
 * @package frontend\models\mreClassifiers
 */
class MreClassifiersNgForm extends BaseNgForm
{
    const CLASSIFIER_TYPE_MRE = 'mre_classifier';

    /*
    |--------------------------------------------------------------------------
    | Classifiers
    |--------------------------------------------------------------------------
    */

    public function getClassifiers(): array
    {
        $data = $this->getBrungildaApi()->getClassifiers();
        $filteredClassifiers = $this->filterClassifiersByClass($data);

        return $filteredClassifiers;
    }

    public function createClassifier(array $requestData): array
    {
        $createParams = [
            'text' => $requestData['text'],
            'type' => self::CLASSIFIER_TYPE_MRE,
        ];

        return $this->getBrungildaApi()->createClassifiers([$createParams]);
    }

    public function updateClassifier(array $requestData): array
    {
        $updateParams = [
            'classifier_id' => (int) $requestData['classifier_id'],
            'text'          => $requestData['text'],
            'model'         => $requestData['model'],
        ];

        return $this->getBrungildaApi()->updateClassifiers([$updateParams]);
    }

    public function deleteClassifier(array $requestData): array
    {
        $deleteParams['classifier_id'] = (int) $requestData['classifier_id'];

        return $this->getBrungildaApi()->deleteClassifiers([$deleteParams]);
    }

    public function getClassifiersAvailableModels(): array
    {
        $result = $this->getBrungildaApi()->getClassifiersStatus();

        return $result;
    }

    private function filterClassifiersByClass($data): array
    {
        foreach ($data as $key => $classifier)
        {
            if($classifier['type'] != self::CLASSIFIER_TYPE_MRE) {
                unset($data[$key]);
            }
        }

        return array_values($data);
    }

    /*
    |--------------------------------------------------------------------------
    | Classes
    |--------------------------------------------------------------------------
    */

    public function getClasses(int $classifierId): array
    {
        $classifiers = $this->getBrungildaApi()->getNlpClassifierClasses($classifierId);

        return $classifiers;
    }

    public function createClass(array $requestData): array
    {
        $createData = [
            'classifier_id' => (int)$requestData['classifier_id'],
            'name'          => $requestData['name'],
        ];

        return $this->getBrungildaApi()->createNlpClassifierClasses([$createData]);
    }

    public function updateClass(array $requestData): array
    {
        $updateData = [
            'classifier_class_id' => (int)$requestData['classifier_class_id'],
            'name'                => $requestData['name'],
        ];

        return $this->getBrungildaApi()->updateNlpClassifierClasses([$updateData]);
    }

    public function deleteClass(array $requestData): array
    {
        $deleteData['classifier_class_id'] = (int) $requestData['classifier_class_id'];

        return $this->getBrungildaApi()->deleteNlpClassifierClasses([$deleteData]);
    }

    /*
    |--------------------------------------------------------------------------
    | Patterns
    |--------------------------------------------------------------------------
    */

    public function getPatterns(int $classId): array
    {
        $patternsData = $this->getBrungildaApi()->getClassPatterns($classId);

        if (empty($patternsData['patterns'])) {
            return [];
        }

        return $patternsData['patterns'];
    }

    public function createPattern(array $requestData): array
    {
        $params = [
            'classifier_id'       => (int) $requestData['classifier_id'] ?? 0,
            'classifier_class_id' => (int) $requestData['classifier_class_id'] ?? 0,
            'text'                => $requestData['text'] ?? null,
            'test'                => $requestData['test'] ?? null,
            'active'              => false,
        ];

        return $this->getBrungildaApi()->createClassifierPatterns([$params]);
    }

    public function updatePattern(array $requestData): array
    {
        $params = [
            'classifier_pattern_id' => (int) $requestData['classifier_pattern_id'] ?? 0,
            'text'                  => $requestData['text'] ?? null,
            'test'                  => $requestData['test'] ?? null,
            'active'                => (int) $requestData['active'] ?? 0,
        ];

        $params = $this->filterNullableProperty($params);

        return $this->getBrungildaApi()->updateClassifierPatterns([$params]);
    }

    public function updateMultiPatterns(array $requestData): array
    {
        $patternsIds  = $requestData['ids']   ?? [];
        $updateData   = $requestData['model'] ?? [];

        foreach ($patternsIds as $patternsId) {
            $updateParams = [
                'classifier_pattern_id' => (int) $patternsId ?? 0,
                'text'                  => $updateData['text'] ?? null,
                'test'                  => $updateData['test'] ?? null,
                'active'                => (int) $updateData['active'] ?? 0
            ];

            $updateParams = $this->filterNullableProperty($updateParams);

            $this->updatePattern($updateParams);
        }

        return ['status' => true];
    }

    public function deletePattern(array $requestData): array
    {
        $params['classifier_pattern_id'] = (int) $requestData['classifier_pattern_id'] ?? 0;

        return $this->getBrungildaApi()->deleteClassifierPatterns([$params]);

    }

    public function deleteMultiPatterns(array $requestData): array
    {
        $patternsIds = $requestData['ids'] ?? [];

        foreach ($patternsIds as $patternsId) {
            $deleteParams = [
                'classifier_pattern_id' => (int) $patternsId ?? 0
            ];
            $this->deletePattern($deleteParams);
        }

        return ['status' => true];
    }

    private function filterNullableProperty(array $params): array
    {
        foreach ($params as $key => $value) {
            if (is_null($value)) {
                unset($params[$key]);
            }
        }

        return $params;
    }
}
