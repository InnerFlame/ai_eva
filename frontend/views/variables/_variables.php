<?php
use common\widgets\BindableFormItems\BindableFormItems;

$variablesItems = array();
foreach ($model->variables as $variable) {
    $item['text'] = $variable->getName();
    $item['subtext'] = $variable->getDescription();
    $item['hiddens'] = [
        ['name' => 'variableId', 'value' => $variable->getVariableId()],
        ['name' => 'variableValue', 'value' => $variable->getValue()],
    ];
    $variablesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'variablesBindableForm',
    'items' => $variablesItems,
    'checked' => false,
]);
