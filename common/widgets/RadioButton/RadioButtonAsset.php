<?php

namespace common\widgets\RadioButton;

use yii\web\AssetBundle;

class RadioButtonAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/RadioButton/assets';

    public $css = [
        'css/radiobutton.css',
    ];

    public $js = [
        'js/radiobutton.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        //'forceCopy'=>true,
    ];
}
