import {Component} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {HttpService} from "../shared/services";
import {OnInit} from '@angular/core';
import {DragulaService} from "ng2-dragula";
import {
    TopicsModel,
    TopicRulesModel,
    RulePatternsModel,
    RuleTemplatesModel,
    RuleExcPatternsModel,
    RuleSubRulesModel,
    SubRulePatternsModel,
    SubRuleTemplatesModel,
    RulePatternsVariableModel,
    LearningPhrasesModel,
    TopicsRouting
} from "../shared";

@Component({
    selector: 'topics',
    templateUrl: './topics.html',
})

export class TopicsComponent implements OnInit {
    readonly ruleTemplatesItemTextMaxLength = 65;

    public errorText: string = '';
    public successText: string = '';
    public modalErrorText: string = '';
    public modalSuccessText: string = '';
    public displayLoadingBlock: boolean = true;
    public disableListGroups: boolean = false;
    public displayModalLoader: boolean = false;

    public copiedRule: any = null;
    public copiedPatterns: any = null;

    public topicModel: TopicsModel = new TopicsModel();
    public topicRuleModel: TopicRulesModel = new TopicRulesModel();
    public newTopicName: string = '';
    public newTopicRuleName: string = '';

    public topicsColumnCollapse: boolean = false;
    public topicsRulesColumnCollapse: boolean = false;

    public availableRules: any = [];
    public availableTopics: any = [];
    public availableScenarios: any = [];
    public availableVariables: any = [];
    public availableTimeSlots: any = [];
    public availableTags: any = [];
    public availableLanguages: any = [];
    public availableRulePatternVariables = [];
    public availablePatternsRegexpParts = [];
    public availablePatternsVaribales = [];

    public checkedPatterns = [];
    public checkedTemplates = [];
    public checkedExcPatterns = [];
    public checkedSubRulePatterns = [];
    public checkedSubRuleTemplates = [];
    public checkedClassPhrases = [];

    public rulePatternsModel = new RulePatternsModel();
    public ruleTemplatesModel = new RuleTemplatesModel();
    public ruleExcPatternsModel = new RuleExcPatternsModel();
    public ruleSubRulesModel = new RuleSubRulesModel();
    public subRulePatternsModel = new SubRulePatternsModel();
    public subRuleTemplatesModel = new SubRuleTemplatesModel();
    public rulePatternVariableModel = new RulePatternsVariableModel();
    public learningPhrasesModel = new LearningPhrasesModel();

    public rulePatterns = [];
    public ruleTemplates = [];
    public ruleExcPatterns = [];
    public ruleSubRules = [];
    public ruleSubRulePatterns = [];
    public ruleSubRuleTemplates = [];
    public learningPhrases = [];

    public activeTopicId: number = 0;
    public activeRuleId: number = 0;
    public activePatternId: number = 0;
    public activeTemplateId: number = 0;
    public activeExcPatternId: number = 0;
    public activeLearningPhrasesId: number = 0;

    public currentTopicData: any = null;

    constructor(private httpService: HttpService, private titleService: Title, private dragulaService: DragulaService) {
        this.titleService.setTitle('Topics');

        dragulaService.drop.subscribe((value) => {
            const [bagName, e, el] = value;
            let newPrioritiesIds = this.getUpdatedPrioritiesData(e);

            if (bagName == 'topics-bag') {
                this.updateTopicsPriority(newPrioritiesIds);
            }

            if (bagName == 'rules-bag') {
                this.updateRulesPriority(newPrioritiesIds);
            }
        });
    }

    ngOnInit(): void {
        let url_string = window.location.href;
        let url = new URL(url_string);
        this.activeTopicId = parseInt(url.searchParams.get('topicId'));
        this.activeRuleId = parseInt(url.searchParams.get('ruleId'));
        this.activePatternId = parseInt(url.searchParams.get('patternId'));
        this.activeTemplateId = parseInt(url.searchParams.get('templateId'));
        this.activeExcPatternId = parseInt(url.searchParams.get('excPatternId'));
        this.activeLearningPhrasesId = parseInt(url.searchParams.get('learningPhrasesId'));

        this.getSelectsData();
        this.getTopics();
    }

    public getTopicsColumnClass(): string {
        if (this.topicsColumnCollapse) {
            return 'col-md-2'
        }

        return 'col-md-3';
    }

    public getTopicsPhrasesColumnClass(): string {
        if (this.topicsRulesColumnCollapse) {
            return 'col-md-2'
        }

        return 'col-md-3';
    }

    public getFormsColumnClass(): string {
        if (this.topicsRulesColumnCollapse && this.topicsColumnCollapse) {
            return 'col-md-8';
        }

        if (this.topicsRulesColumnCollapse || this.topicsColumnCollapse) {
            return 'col-md-7';
        }

        return 'col-md-6';
    }

    public changeTopic(topicData: any) {
        this.topicModel.load(topicData);
        this.topicRuleModel.reset();
        this.rulePatternsModel.reset();
        this.ruleTemplatesModel.reset();
        this.ruleExcPatternsModel.reset();
        this.ruleSubRulesModel.reset();
        this.subRulePatternsModel.reset();
        this.subRuleTemplatesModel.reset();
        this.learningPhrasesModel.reset();
        this.availableRules = [];
        this.rulePatterns = [];
        this.ruleTemplates = [];
        this.ruleExcPatterns = [];
        this.ruleSubRules = [];
        this.ruleSubRuleTemplates = [];
        this.ruleSubRulePatterns = [];
        this.learningPhrases = [];
        this.getTopicRulesById(topicData.topic_id);
    }

    public changeRule(ruleData: any) {
        this.topicRuleModel.load(ruleData);
        this.rulePatternsModel.reset();
        this.ruleTemplatesModel.reset();
        this.ruleExcPatternsModel.reset();
        this.ruleSubRulesModel.reset();
        this.subRulePatternsModel.reset();
        this.subRuleTemplatesModel.reset();
        this.learningPhrasesModel.reset();
        this.rulePatterns = [];
        this.ruleTemplates = [];
        this.ruleExcPatterns = [];
        this.ruleSubRules = [];
        this.ruleSubRuleTemplates = [];
        this.ruleSubRulePatterns = [];
        this.learningPhrases = [];
        this.getRuleFormsData();
    }

    private getSelectsData(): void {
        this.httpService.get(TopicsRouting.getSelectsDataUrl()).then(data => {
            if (!data || !Object.keys(data).length) {
                this.displayLoadingBlock = false;
                this.errorText = 'Troubles with select data response!';
                return;
            }

            this.availableVariables = data['variables'];
            this.availableTimeSlots = data['timeSlots'];
            this.availableTags = data['tags'];
            this.availableLanguages = data['languages'];
            this.availableScenarios = data['scenarios'];
            this.availableRules = data['rules'];
        }, err => {
            this.errorText = 'Error on server while getting selects data!';
        });
    }

    private updateTopicsPriority(newPrioritiesIds: Array<any>): void {
        let alreadySend  = false;

        if (alreadySend) {
            return;
        }
        this.disableListGroups = true;
        this.httpService.post(TopicsRouting.getUpdateTopicsPriorityUrl(), newPrioritiesIds)
            .then(data => {
                if (!data) {
                    this.errorText = 'Something goes wrong with response!';
                    this.disableListGroups = false;
                }

                alreadySend = true;
                this.disableListGroups = false;
            });
    }

    private updateRulesPriority(newPrioritiesIds: Array<any>): void {
        let alreadySend  = false;

        if (alreadySend) {
            return;
        }
        this.disableListGroups = true;
        this.httpService.post(TopicsRouting.getUpdateRulesPriorityUrl(), newPrioritiesIds)
            .then(data => {
                if (!data) {
                    this.errorText = 'Something goes wrong with response!';
                    this.disableListGroups = false;
                }

                alreadySend = true;
                this.disableListGroups = false;
            });
    }

    public setRegextParts() {
        this.availablePatternsRegexpParts = [];
        this.rulePatternVariableModel.reset();

        let parts = (new RegExp(this.rulePatternsModel.text)).exec(this.rulePatternsModel.test);
        for(let i = 0; i < parts.length; i++) {
            this.availablePatternsRegexpParts.push(parts[i]);
        }

        if (this.availablePatternsVaribales[this.rulePatternsModel.pattern_id]) {
            this.rulePatternVariableModel.load(this.availablePatternsVaribales[this.rulePatternsModel.pattern_id]);
        }
    }

    /** ============================================= TOPICS BEGIN ============================================= */
    private getTopics(): void {
        this.httpService.get(TopicsRouting.getTopicsModelsUrl()).then(data => {
            if (!data || !Object.keys(data).length) {
                this.displayLoadingBlock = false;
                this.disableListGroups = false;
                this.errorText = 'Troubles with topics response or empty topics data!';
                return;
            }

            if (data[0] && !this.activeTopicId) {
                this.topicModel.load(data[0]);
                this.activeTopicId = this.topicModel.topic_id;
            }

            if (this.activeTopicId) {
                data.map(function (el) {
                    if (el.topic_id == this.activeTopic) {
                        this.topicsModel.load(el);
                    }

                    return el;
                }, {activeTopic: this.activeTopicId, topicsModel: this.topicModel});
                this.activeTopicId = 0;
            }

            if (this.topicModel.topic_id) {
                this.getTopicRulesById(this.topicModel.topic_id);
            }

            this.availableTopics = data;
            this.displayLoadingBlock = false;
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while getting topics!';
            this.displayLoadingBlock = false;
            this.disableListGroups = false;
        });
    }

    public createTopic(): void {
        this.displayModalLoader = true;
        this.disableListGroups = true;
        this.errorText = '';
        this.modalErrorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getTopicCreateUrl(), {"name" : this.newTopicName}).then(data => {
            if (!data || !Object.keys(data).length || !data[0] || data[0].err) {
                this.displayModalLoader = false;
                this.disableListGroups = false;
                this.errorText = 'Troubles with create topicData data response!';
                this.modalErrorText = 'Troubles with create topicData data response!';
                return;
            }

            if (data[0].item.item) {
                this.availableTopics.push(data[0].item.item);
                this.topicModel.load(data[0].item.item);
            }

            this.clearDOMFromModalElements('createTopicModal');

            this.successText = 'Topic was created successfully!';
            this.displayModalLoader = false;
            this.disableListGroups = false;
            this.newTopicName = '';
            this.availableRules = [];
        }, err => {
            this.displayModalLoader = false;
            this.disableListGroups = false;
            this.errorText = 'Error on server!';
            this.modalErrorText = 'Error on server!';
        });
    }

    public deleteTopic(): void {
        if (!confirm('Are you sure you want delete "' + this.topicModel.name + '" topicData?')) {
            return;
        }

        this.disableListGroups = true;
        this.displayModalLoader = true;
        this.modalErrorText = '';
        this.modalSuccessText = '';

        this.httpService.post(TopicsRouting.getTopicDeleteUrl(), this.topicModel).then(data => {
            if (!data || !Object.keys(data).length) {
                this.disableListGroups = false;
                this.modalErrorText = 'Troubles with delete topicData data response!';
                return;
            }

            this.availableTopics = this.availableTopics
                .filter(el => el.topic_id != this.topicModel.topic_id);
            this.topicModel.reset();
            this.displayModalLoader = false;
            this.availableRules = [];
            this.currentTopicData = null;
            this.successText = 'Topic was deleted successfully!';
            this.displayModalLoader = false;
            this.disableListGroups = false;
        }, err => {
            this.disableListGroups = false;
            this.displayModalLoader = false;
            this.errorText = 'Error on server while deleting topicData!';
        });
    }

    public updateTopic(): void {
        this.displayModalLoader = true;
        this.disableListGroups = true;
        this.modalErrorText = '';
        this.modalSuccessText = '';

        this.httpService.post(TopicsRouting.getTopicUpdateUrl(), this.topicModel).then(data => {
            if (!data || !Object.keys(data).length || !data || data['result'].err) {
                this.displayModalLoader = false;
                this.disableListGroups = false;
                this.modalErrorText = 'Troubles with update topicData response!';
                return;
            }

            this.availableTopics = this.availableTopics.map(function (el) {
                if (el.topic_id == this.updatedTopicModel.topic_id) {
                    el.name = this.updatedTopicModel.name;
                    el.switching = this.updatedTopicModel.switching;
                    el.active = this.updatedTopicModel.active;
                    el.enabled = this.updatedTopicModel.enabled;
                }
                return el;
            }, {updatedTopicModel: this.topicModel});

            this.modalSuccessText = 'Topic was updated successfully!';
            this.displayModalLoader = false;
            this.disableListGroups = false;
        }, err => {
            this.displayModalLoader = false;
            this.disableListGroups = false;
            this.modalErrorText = 'Error on server!';
        });
    }

    public openUpdateTopicModal(topicData: any) {
        this.currentTopicData = topicData;
    }

    /** ============================================= TOPICS END ============================================= */


    /** ============================================= RULES BEGIN ============================================= */
    private getTopicRulesById(topicId: number): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.httpService.get(TopicsRouting.getTopicRulesModelsUrl(), {topicId: topicId}).then(data => {
            if (!data || !Object.keys(data).length) {
                this.displayLoadingBlock = false;
                this.disableListGroups = false;
                this.errorText = 'Troubles with rules response or empty topicData rules data!';
                this.availableRules = [];
                return;
            }

            if (data[0]) {
                this.topicRuleModel.load(data[0]);
                this.activeTopicId = this.topicRuleModel.topic_id;
                this.activeRuleId = this.topicRuleModel.rule_id;
            }

            this.getRuleFormsData();
            this.availableRules = data;
            this.displayLoadingBlock = false;
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while getting rules!';
            this.displayLoadingBlock = false;
            this.disableListGroups = false;
        });
    }

    public createTopicRule(): void {
        this.displayModalLoader = true;
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.modalErrorText = '';

        this.httpService.post(TopicsRouting.getTopicRuleCreateUrl(), {"name" : this.newTopicRuleName, "topic_id": this.topicModel.topic_id}).then(data => {
            if (!data || !Object.keys(data).length || !data[0] || data[0].err) {
                this.displayModalLoader = false;
                this.disableListGroups = false;
                this.errorText = 'Troubles with create rule response!';
                this.modalErrorText = 'Troubles with create rule response!';
                return;
            }

            if (data[0].item.item) {
                this.availableRules.push(data[0].item.item);
                this.topicRuleModel.load(data[0].item.item);
            }

            this.clearDOMFromModalElements('createTopicRuleModal');

            this.successText = 'Rule was created successfully!';
            this.displayModalLoader = false;
            this.disableListGroups = false;
            this.newTopicRuleName = '';
        }, err => {
            this.displayModalLoader = false;
            this.disableListGroups = false;
            this.errorText = 'Error on server!';
            this.modalErrorText = 'Error on server!';
        });
    }

    public changeTopicRuleStatusOpt(optName: string, newValue: boolean): void {
        this.displayModalLoader = true;
        this.disableListGroups = true;
        this.modalErrorText = '';
        this.modalSuccessText = '';

        let body = {};
        body[optName] = newValue;
        body['rule_id'] = this.topicRuleModel.rule_id;

        this.httpService.post(TopicsRouting.getTopicRuleUpdateOptUrl(), body).then(data => {
            if (!data || !data['result'] || data['result'][0]['err']) {
                this.displayModalLoader = false;
                this.disableListGroups = false;
                this.modalErrorText = 'Troubles with update status response!';
                return;
            }

            this.availableRules = this.availableRules.map(function (el) {
                if (el.rule_id == this.updatedRuleModel.rule_id) {
                    el[this.optionName] = this.optionValue;
                }

                return el;
            }, {updatedRuleModel: this.topicRuleModel, optionName: optName, optionValue: newValue});

            this.modalSuccessText = 'Status was update successfully!';
            this.displayModalLoader = false;
            this.disableListGroups = false;
        }, err => {
            this.displayModalLoader = false;
            this.disableListGroups = false;
            this.modalErrorText = 'Error on server!';
        });
    }

    public deleteRule(): void {
        if (!confirm('Are you sure you want delete "' + this.topicRuleModel.name + '" rule?')) {
            return;
        }

        this.disableListGroups = true;
        this.displayModalLoader = true;
        this.modalErrorText = '';
        this.modalSuccessText = '';

        this.httpService.post(TopicsRouting.getDeleteTopicRuleUrl(), this.topicRuleModel).then(data => {
            if (!data || !Object.keys(data).length) {
                this.disableListGroups = false;
                this.modalErrorText = 'Troubles with delete rule data response!';
                return;
            }

            this.availableRules = this.availableRules
                .filter(el => el.rule_id != this.topicRuleModel.rule_id);
            this.topicModel.reset();
            this.displayModalLoader = false;
            this.clearDOMFromModalElements();
            this.successText = 'Rule was deleted successfully!';
            this.displayModalLoader = false;
            this.disableListGroups = false;
        }, err => {
            this.disableListGroups = false;
            this.displayModalLoader = false;
            this.errorText = 'Error on server while deleting rule!';
        });


    }

    public updateRule(): void {
        this.displayModalLoader = true;
        this.disableListGroups = true;
        this.modalErrorText = '';
        this.modalSuccessText = '';

        this.httpService.post(TopicsRouting.getUpdateTopicRuleUrl(), this.topicRuleModel).then(data => {
            if (!data || !Object.keys(data).length || !data || data['result'].err) {
                this.displayModalLoader = false;
                this.disableListGroups = false;
                this.modalErrorText = 'Troubles with update rule response!';
                return;
            }

            this.availableRules = this.availableRules.map(function (el) {
                if (el.rule_id == this.updatedRuleModel.rule_id) {
                    el.name = this.updatedRuleModel.name;
                }
                return el;
            }, {updatedRuleModel: this.topicRuleModel});

            this.modalSuccessText = 'Rule was updated successfully!';
            this.displayModalLoader = false;
            this.disableListGroups = false;
        }, err => {
            this.displayModalLoader = false;
            this.disableListGroups = false;
            this.modalErrorText = 'Error on server!';
        });
    }

    public changeActivePattern(rulePattern) {
        this.disableListGroups = true;
        this.rulePatternsModel.load(rulePattern);


        this.httpService.get(TopicsRouting.getPatternsVariablesUrl()).then(data => {
            if (!data || !Object.keys(data).length) {
                this.errorText = 'Troubles with patterns variables!';
                this.disableListGroups = false;
                return;
            }

            this.availablePatternsVaribales = data;
            this.setRegextParts();

            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while getting updated rule patterns variables data!';
            this.disableListGroups = false;
        });
    }

    public getRuleFormsData(): void {
        this.disableListGroups = true;
        this.activeTopicId = this.topicRuleModel.topic_id;
        this.activeRuleId = this.topicRuleModel.rule_id;
        this.httpService.get(TopicsRouting.getRuleFormsDataUrl(), {ruleId: this.topicRuleModel.rule_id}).then(data => {
            if (!data || !Object.keys(data).length) {
                this.errorText = 'Troubles with rules response or empty topicData rules data!';
                this.disableListGroups = false;
                return;
            }

            if (data.patternVariables) {
                this.availablePatternsVaribales = data.patternVariables;
            }

            if (data.rule.learning_phrases && data.rule.learning_phrases.length) {
                this.learningPhrases = data.rule.learning_phrases;
                if (this.activeLearningPhrasesId) {
                    this.learningPhrases.map(function (el) {
                        if (el.learning_phrase_id == this.activeId) {
                            this.learningPhrasesModel.load(el);
                        }

                        return el;
                    }, {activeId: this.activeLearningPhrasesId, learningPhrasesModel: this.learningPhrasesModel});
                    this.activeLearningPhrasesId = 0;
                } else {
                    this.learningPhrasesModel.load(this.learningPhrases[0]);
                }
            }
            if (data.rule.patterns && data.rule.patterns.length) {
                this.rulePatterns = data.rule.patterns;
                if (this.activePatternId) {
                    this.rulePatterns.map(function (el) {
                        if (el.pattern_id == this.activeId) {
                            this.patternModel.load(el);
                        }

                        return el;
                    }, {activeId: this.activePatternId, patternModel: this.rulePatternsModel});
                    this.activePatternId = 0;
                } else {
                    this.rulePatternsModel.load(this.rulePatterns[0]);
                }

                this.setRegextParts();
            }

            if (data.rule.templates && data.rule.templates.length) {
                this.ruleTemplates = data.rule.templates;
                if (this.activeTemplateId) {
                    this.ruleTemplates.map(function (el) {
                        if (el.template_id == this.activeId) {
                            this.templateModel.load(el);
                        }

                        return el;
                    }, {activeId: this.activeTemplateId, templateModel: this.ruleTemplatesModel});
                    this.activeTemplateId = 0;
                } else {
                    this.ruleTemplatesModel.load(this.ruleTemplates[0]);
                }
            }

            if (data.rule.exception_patterns && data.rule.exception_patterns.length) {
                this.ruleExcPatterns = data.rule.exception_patterns;
                if (this.activeExcPatternId) {
                    this.ruleExcPatterns.map(function (el) {
                        if (el.exception_pattern_id == this.activeId) {
                            this.excPatternModel.load(el);
                        }

                        return el;
                    }, {activeId: this.activeExcPatternId, excPatternModel: this.ruleExcPatternsModel});
                    this.activeExcPatternId = 0;
                } else {
                    this.ruleExcPatternsModel.load(this.ruleExcPatterns[0]);
                }
            }

            if (data.rule.rules && data.rule.rules.length) {
                this.ruleSubRules = data.rule.rules;
                this.ruleSubRulesModel.load(this.ruleSubRules[0]);
                this.getSubRulesData();
            }

            if (data.variables && data.variables.length) {
                this.availableRulePatternVariables = data.variables;
            }



            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while getting rule forms data!';
            this.disableListGroups = false;
        });
    }

    public getSubRulesData(): void {
        this.disableListGroups = true;
        this.ruleSubRuleTemplates = [];
        this.subRuleTemplatesModel.reset();
        this.subRulePatternsModel.reset();
        this.ruleSubRulePatterns = [];

        this.httpService.get(TopicsRouting.getSubRuleDataUrl(), {subRuleId: this.ruleSubRulesModel.rule_id}).then(data => {
            if (!data || !Object.keys(data).length) {
                this.errorText = 'Troubles with sub rules response or its empty!';
                this.disableListGroups = false;
                return;
            }

            if (data.rule.patterns && data.rule.patterns.length) {
                this.ruleSubRulePatterns = data.rule.patterns;
                this.subRulePatternsModel.load(this.ruleSubRulePatterns[0]);
            }

            if (data.rule.templates && data.rule.templates.length) {
                this.ruleSubRuleTemplates = data.rule.templates;
                this.subRuleTemplatesModel.load(this.ruleSubRuleTemplates[0]);
            }
            this.disableListGroups = false;
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while getting sub rules!';
        });
    }
    /** ============================================= RULES END ============================================= */


    /** ============================================= PATTERNS BEGIN ============================================= */
    testPattern(successAlert: boolean = true): boolean {
        let testStatus = this.rulePatternsModel.test.match(this.rulePatternsModel.text);

        if (testStatus) {
            if (successAlert) {
                alert('Success. The regular expression and string entered is match.');
            }

            return true;
        }

        alert('Error. The regular expression and string entered do not match.');
        return false;
    }

    public createPattern(): void {
        if (!this.testPattern(false)) {
            this.errorText = 'Invalid pattern regexp';
            return;
        }
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.rulePatternsModel.rule_id = this.topicRuleModel.rule_id;
        this.httpService.post(TopicsRouting.getCreatePatternUrl(), this.rulePatternsModel).then(data => {
            if (!data || !data[0] || data[0].err || !data[0].item) {
                this.errorText = 'Invalid response for create pattern';
                this.disableListGroups = false;
                return;
            }

            let createdItem = data[0].item.item;
            if (!createdItem) {
                this.errorText = 'New pattern not created!';
                return;
            }

            this.rulePatterns.push(createdItem);
            this.rulePatternsModel.load(createdItem);
            this.disableListGroups = false;
            this.successText = 'Pattern was created successfully!';
        }, err => {
            this.errorText = 'Error on server while creating pattern!';
            this.disableListGroups = false;
        });
    }

    public updatePattern(updateActiveStatusAction = false): void {
        if (!this.testPattern(false)) {
            this.errorText = 'Invalid pattern regexp';
            return;
        }

        if (this.checkedPatterns.length > 1) {
            this.updateMultiPatterns(updateActiveStatusAction);
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdatePatternUrl(), this.rulePatternsModel).then(data => {
            if (!data || !data.result || data.result[0].err || !data.result[0].item) {
                this.errorText = 'Invalid response for update pattern';
                this.disableListGroups = false;
                return;
            }

            let updatedItem = data.result[0].item;
            if (!updatedItem) {
                this.errorText = 'Pattern not updated!';
                return;
            }

            this.rulePatterns = this.rulePatterns.map(function (el) {
                if (el.pattern_id == this.updatedPattern.pattern_id) {
                    el.test = this.updatedPattern.test;
                    el.active = this.updatedPattern.active;
                    el.text = this.updatedPattern.text;
                }
                return el;
            }, {updatedPattern: updatedItem});

            this.rulePatternsModel.load(updatedItem);
            this.disableListGroups = false;
            this.successText = 'Pattern was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating pattern!';
            this.disableListGroups = false;
        });
    }

    private updateMultiPatterns(updateActiveStatusAction = false): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        let requestUrl: string = updateActiveStatusAction ? TopicsRouting.getUpdateMultiPatternStatusUrl() : TopicsRouting.getUpdateMultiPatternUrl();

        this.httpService.post(requestUrl, {model: this.rulePatternsModel, ids: this.checkedPatterns}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for update patterns';
                this.disableListGroups = false;
                return;
            }

            if (updateActiveStatusAction) {
                this.rulePatterns = this.rulePatterns.map(function (el) {
                    if (this.updatedIds.indexOf(el.pattern_id) !== -1) {
                        el.active = this.rulePatternsModelLink.active;
                    }

                    return el;
                }, {updatedIds: this.checkedPatterns, rulePatternsModelLink: this.rulePatternsModel});
            } else {
                this.rulePatterns = this.rulePatterns.map(function (el) {
                    if (this.updatedIds.indexOf(el.pattern_id) !== -1) {
                        el.test = this.rulePatternsModelLink.test;
                        el.active = this.rulePatternsModelLink.active;
                        el.text = this.rulePatternsModelLink.text;
                    }

                    return el;
                }, {updatedIds: this.checkedPatterns, rulePatternsModelLink: this.rulePatternsModel});
            }

            this.rulePatternsModel.reset();
            this.disableListGroups = false;
            this.checkedPatterns = [];
            this.successText = 'Patterns was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating patterns!';
            this.disableListGroups = false;
        });
    }

    public deletePattern(): void {
        if (this.checkedPatterns.length > 1) {
            this.deleteMultiPatterns();
            return;
        }

        if (!confirm('Do you sure that you want delete "' + this.rulePatternsModel.test + '" pattern?')) {
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeletePatternUrl(), {pattern_id: this.rulePatternsModel.pattern_id}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete pattern';
                this.disableListGroups = false;
                return;
            }

            this.rulePatterns = this.rulePatterns
                .filter(el => el.pattern_id != this.rulePatternsModel.pattern_id);

            this.rulePatternsModel.reset();
            this.disableListGroups = false;
            this.successText = 'Pattern was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting pattern!';
        });
    }

    private deleteMultiPatterns(): void {
        if (!confirm('Do you sure that you want delete all checked patterns?')) {
            return;
        }
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteMultiPatternUrl(), this.checkedPatterns).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete patterns';
                this.disableListGroups = false;
                return;
            }

            this.rulePatterns = this.rulePatterns
                .filter(el => this.checkedPatterns.indexOf(el.pattern_id) === -1);

            this.rulePatternsModel.reset();
            this.disableListGroups = false;
            this.checkedPatterns = [];
            this.successText = 'Patterns was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting patterns!';
        });
    }
    /** ============================================= PATTERNS END ============================================= */


    /** ============================================= TEMPLATES BEGIN ============================================= */
    public createMultiTemplate(): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.ruleTemplatesModel.rule_id = this.topicRuleModel.rule_id;

        let lines = this.ruleTemplatesModel.text.split("\n");

        for (let i = 0; i < lines.length; i++) {
            if (!lines[i]) {
                continue;
            }
            this.ruleTemplatesModel.text = lines[i];
            this.createTemplate(this.ruleTemplatesModel);
        }
    }

    private createTemplate(ruleTemplatesModel): void {
        this.httpService.post(TopicsRouting.getCreateTemplateUrl(), ruleTemplatesModel).then(data => {
            if (!data || !data[0] || data[0].err || !data[0].item) {
                this.errorText = 'Invalid response for create template';
                this.disableListGroups = false;
                return;
            }

            let createdItem = data[0].item.item;
            if (!createdItem) {
                this.errorText = 'New template not created!';
                return;
            }

            this.ruleTemplates.push(createdItem);
            this.ruleTemplatesModel.load(createdItem);
            this.disableListGroups = false;
            this.successText = 'Template was created successfully!';
        }, err => {
            this.errorText = 'Error on server while creating template!';
            this.disableListGroups = false;
        });
    }

    public updateTemplate(): void {
        if (this.checkedTemplates.length > 1) {
            this.updateMultiTemplate();
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdateTemplateUrl(), this.ruleTemplatesModel).then(data => {
            if (!data || !data.result || data.result[0].err || !data.result[0].item) {
                this.errorText = 'Invalid response for update template';
                this.disableListGroups = false;
                return;
            }

            let updatedItem = data.result[0].item;
            if (!updatedItem) {
                this.errorText = 'Template not updated!';
                return;
            }

            this.ruleTemplates = this.ruleTemplates.map(function (el) {
                if (el.template_id == this.updatedTemplate.template_id) {
                    el.text = this.updatedTemplate.text;
                    el.timeslot_id = this.updatedTemplate.timeslot_id;
                }
                return el;
            }, {updatedTemplate: updatedItem});

            this.ruleTemplatesModel.load(updatedItem);
            this.disableListGroups = false;
            this.successText = 'Template was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating template!';
            this.disableListGroups = false;
        });
    }

    private updateMultiTemplate(): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdateMultiTemplateUrl(), {model: this.ruleTemplatesModel, ids: this.checkedTemplates}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for update templates';
                this.disableListGroups = false;
                return;
            }

            this.ruleTemplates = this.ruleTemplates.map(function (el) {
                if (this.updatedIds.indexOf(el.template_id) !== -1) {
                    el.text = this.ruleTemplatesModelLink.text;
                    el.timeslot_id = this.ruleTemplatesModelLink.timeslot_id;
                }

                return el;
            }, {updatedIds: this.checkedTemplates, ruleTemplatesModelLink: this.ruleTemplatesModel});

            this.ruleTemplatesModel.reset();
            this.disableListGroups = false;
            this.checkedTemplates = [];
            this.successText = 'Templates was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating templates!';
            this.disableListGroups = false;
        });
    }

    public deleteTemplate(): void {
        if (this.checkedTemplates.length > 1) {
            this.deleteMultiTemplate();
            return;
        }

        if (!confirm('Do you sure that you want delete "' + this.ruleTemplatesModel.text + '" template?')) {
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteTemplateUrl(), {template_id: this.ruleTemplatesModel.template_id}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete template';
                this.disableListGroups = false;
                return;
            }

            this.ruleTemplates = this.ruleTemplates
                .filter(el => el.template_id != this.ruleTemplatesModel.template_id);

            this.ruleTemplatesModel.reset();
            this.disableListGroups = false;
            this.successText = 'Template was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting template!';
        });
    }

    public deleteMultiTemplate(): void {
        if (!confirm('Do you sure that you want delete all checked templates?')) {
            return;
        }
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteMultiTemplateUrl(), this.checkedTemplates).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete templates';
                this.disableListGroups = false;
                return;
            }

            this.ruleTemplates = this.ruleTemplates
                .filter(el => this.checkedTemplates.indexOf(el.template_id) === -1);

            this.ruleTemplatesModel.reset();
            this.disableListGroups = false;
            this.checkedPatterns = [];
            this.successText = 'Templates was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting templates!';
        });
    }
    /** ============================================= TEMPLATES END ============================================= */


    /** ============================================= EXCPATTERNS BEGIN ============================================= */
    testExcPattern(successAlert: boolean = true): boolean {
        let testStatus = this.ruleExcPatternsModel.test.match(this.ruleExcPatternsModel.text);

        if (testStatus) {
            if (successAlert) {
                alert('Success. The regular expression and string entered is match.');
            }

            return true;
        }

        alert('Error. The regular expression and string entered do not match.');
        return false;
    }

    public createExcPattern(): void {
        if (!this.testExcPattern(false)) {
            this.errorText = 'Invalid pattern regexp';
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.ruleExcPatternsModel.rule_id = this.topicRuleModel.rule_id;
        this.httpService.post(TopicsRouting.getCreateExcPatternUrl(), this.ruleExcPatternsModel).then(data => {
            if (!data || !data[0] || data[0].err || !data[0].item) {
                this.errorText = 'Invalid response for create exc pattern';
                this.disableListGroups = false;
                return;
            }

            let createdItem = data[0].item.item;
            if (!createdItem) {
                this.errorText = 'New exc pattern not created!';
                return;
            }

            this.ruleExcPatterns.push(createdItem);
            this.ruleExcPatternsModel.load(createdItem);
            this.disableListGroups = false;
            this.successText = 'ExcPattern was created successfully!';
        }, err => {
            this.errorText = 'Error on server while creating exc pattern!';
            this.disableListGroups = false;
        });
    }
    
    public updateExcPattern(): void {
        if (!this.testExcPattern(false)) {
            this.errorText = 'Invalid pattern regexp';
            return;
        }

        if (this.checkedExcPatterns.length > 1) {
            this.updateMultiExcPattern();
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdateExcPatternUrl(), this.ruleExcPatternsModel).then(data => {
            if (!data || !data.result || data.result[0].err || !data.result[0].item) {
                this.errorText = 'Invalid response for update exc pattern';
                this.disableListGroups = false;
                return;
            }

            let updatedItem = data.result[0].item;
            if (!updatedItem) {
                this.errorText = 'ExcPattern not updated!';
                return;
            }

            this.ruleExcPatterns = this.ruleExcPatterns.map(function (el) {
                if (el.exception_pattern_id == this.updatedPattern.exception_pattern_id) {
                    el.test = this.updatedPattern.test;
                    el.active = this.updatedPattern.active;
                    el.text = this.updatedPattern.text;
                }
                return el;
            }, {updatedPattern: updatedItem});

            this.ruleExcPatternsModel.load(updatedItem);
            this.disableListGroups = false;
            this.successText = 'ExcPattern was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating exc pattern!';
            this.disableListGroups = false;
        });
    }
    
    public updateMultiExcPattern(): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdateMultiExcPatternUrl(), {model: this.ruleExcPatternsModel, ids: this.checkedExcPatterns}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for update exc patterns';
                this.disableListGroups = false;
                return;
            }

            this.ruleExcPatterns = this.ruleExcPatterns.map(function (el) {
                if (this.updatedIds.indexOf(el.exception_pattern_id) !== -1) {
                    el.test = this.rulePatternsModelLink.test;
                    el.active = this.rulePatternsModelLink.active;
                    el.text = this.rulePatternsModelLink.text;
                }

                return el;
            }, {updatedIds: this.checkedExcPatterns, rulePatternsModelLink: this.ruleExcPatternsModel});

            this.ruleExcPatternsModel.reset();
            this.disableListGroups = false;
            this.checkedExcPatterns = [];
            this.successText = 'ExcPatterns was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating exc patterns!';
            this.disableListGroups = false;
        });
    }
    
    public deleteExcPattern(): void {
        if (this.checkedExcPatterns.length > 1) {
            this.deleteMultiExcPattern();
            return;
        }

        if (!confirm('Do you sure that you want delete "' + this.ruleExcPatternsModel.test + '" pattern?')) {
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteExcPatternUrl(), {pattern_id: this.ruleExcPatternsModel.exception_pattern_id}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete exc pattern';
                this.disableListGroups = false;
                return;
            }

            this.ruleExcPatterns = this.ruleExcPatterns
                .filter(el => el.exception_pattern_id != this.ruleExcPatternsModel.exception_pattern_id);

            this.ruleExcPatternsModel.reset();
            this.disableListGroups = false;
            this.successText = 'ExcPattern was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting exc pattern!';
        });
    }
    
    public deleteMultiExcPattern(): void {
        if (!confirm('Do you sure that you want delete all checked exc patterns?')) {
            return;
        }
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteMultiExcPatternUrl(), this.checkedExcPatterns).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete exc patterns';
                this.disableListGroups = false;
                return;
            }

            this.ruleExcPatterns = this.ruleExcPatterns
                .filter(el => this.checkedExcPatterns.indexOf(el.exception_pattern_id) === -1);

            this.ruleExcPatternsModel.reset();
            this.disableListGroups = false;
            this.checkedExcPatterns = [];
            this.successText = 'ExcPatterns was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting exc patterns!';
        });
    }
    /** ============================================= EXCPATTERNS END ============================================= */


    /** ============================================= SUBRULES BEGIN ============================================= */
    public createSubRule(): void {
        this.disableListGroups = true;
        this.successText = '';
        this.errorText = '';

        this.ruleSubRulesModel.parent_id = this.topicRuleModel.rule_id;
        this.httpService.post(TopicsRouting.getSubRuleCreateUrl(), this.ruleSubRulesModel).then(data => {
            if (!data || !Object.keys(data).length || !data[0] || data[0].err) {
                this.disableListGroups = false;
                this.errorText = 'Troubles with create sub rule response!';
                return;
            }

            if (data[0].item.item) {
                this.ruleSubRules.push(data[0].item.item);
                this.ruleSubRulesModel.load(data[0].item.item);
            }

            this.successText = 'SubRule was created successfully!';
            this.disableListGroups = false;
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server!';
        });
    }

    public deleteSubRule(): void {
        if (!confirm('Are you sure you want delete "' + this.ruleSubRulesModel.name + '" sub rule?')) {
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteTopicRuleUrl(), this.ruleSubRulesModel).then(data => {
            if (!data || !Object.keys(data).length) {
                this.disableListGroups = false;
                this.modalErrorText = 'Troubles with delete sub rule data response!';
                return;
            }

            this.ruleSubRules = this.ruleSubRules
                .filter(el => el.rule_id != this.ruleSubRulesModel.rule_id);

            this.successText = 'SubRule was deleted successfully!';
            this.disableListGroups = false;
        }, err => {
            this.disableListGroups = false;
            this.errorText = '';
        });
    }

    public updateSubRule(): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdateTopicRuleUrl(), this.ruleSubRulesModel).then(data => {
            if (!data || !Object.keys(data).length || !data || data['result'].err) {
                this.disableListGroups = false;
                this.errorText = 'Troubles with update sub rule response!';
                return;
            }

            this.ruleSubRules = this.ruleSubRules.map(function (el) {
                if (el.rule_id == this.updatedRuleModel.rule_id) {
                    el.name = this.updatedRuleModel.name;
                    el.active = this.updatedRuleModel.active;
                    el.enabled = this.updatedRuleModel.enabled;
                    el.exclude_template = this.updatedRuleModel.exclude_template;
                    el.exec_limit = this.updatedRuleModel.exec_limit;
                }
                return el;
            }, {updatedRuleModel: this.ruleSubRulesModel});

            this.successText = 'SubRule was updated successfully!';
            this.disableListGroups = false;
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server!';
        });


    }
    /** ============================================= SUBRULES END ============================================= */


    /** ============================================= SUB RULE PATTERNS BEGIN ============================================= */
    testSubPattern(successAlert: boolean = true): boolean {
        let testStatus = this.subRulePatternsModel.test.match(this.subRulePatternsModel.text);

        if (testStatus) {
            if (successAlert) {
                alert('Success. The regular expression and string entered is match.');
            }

            return true;
        }

        alert('Error. The regular expression and string entered do not match.');
        return false;
    }

    public createSubPattern(): void {
        if (!this.testSubPattern(false)) {
            this.errorText = 'Invalid pattern regexp';
            return;
        }
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.subRulePatternsModel.rule_id = this.ruleSubRulesModel.rule_id;
        this.httpService.post(TopicsRouting.getCreatePatternUrl(), this.subRulePatternsModel).then(data => {
            if (!data || !data[0] || data[0].err || !data[0].item) {
                this.errorText = 'Invalid response for create sub rule pattern';
                this.disableListGroups = false;
                return;
            }

            let createdItem = data[0].item.item;
            if (!createdItem) {
                this.errorText = 'New sub rule pattern not created!';
                return;
            }

            this.ruleSubRulePatterns.push(createdItem);
            this.subRulePatternsModel.load(createdItem);
            this.disableListGroups = false;
            this.successText = 'Sub rule pattern was created successfully!';
        }, err => {
            this.errorText = 'Error on server while creating sub rule pattern!';
            this.disableListGroups = false;
        });
    }

    public updateSubPattern(updateActiveStatusAction = false): void {
        if (!this.testPattern(false)) {
            this.errorText = 'Invalid pattern regexp';
            return;
        }

        if (this.checkedSubRulePatterns.length > 1) {
            this.updateMultiSubPatterns(updateActiveStatusAction);
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdatePatternUrl(), this.subRulePatternsModel).then(data => {
            if (!data || !data.result || data.result[0].err || !data.result[0].item) {
                this.errorText = 'Invalid response for update sub rule pattern';
                this.disableListGroups = false;
                return;
            }

            let updatedItem = data.result[0].item;
            if (!updatedItem) {
                this.errorText = 'Pattern not updated!';
                return;
            }

            this.ruleSubRulePatterns = this.ruleSubRulePatterns.map(function (el) {
                if (el.pattern_id == this.updatedPattern.pattern_id) {
                    el.test = this.updatedPattern.test;
                    el.active = this.updatedPattern.active;
                    el.text = this.updatedPattern.text;
                }
                return el;
            }, {updatedPattern: updatedItem});

            this.subRulePatternsModel.load(updatedItem);
            this.disableListGroups = false;
            this.successText = 'Sub rule pattern was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating sub rule pattern!';
            this.disableListGroups = false;
        });
    }

    private updateMultiSubPatterns(updateActiveStatusAction = false): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        let requestUrl: string = updateActiveStatusAction ? TopicsRouting.getUpdateMultiPatternStatusUrl() : TopicsRouting.getUpdateMultiPatternUrl();

        this.httpService.post(requestUrl, {model: this.subRulePatternsModel, ids: this.checkedSubRulePatterns}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for update sub rule patterns';
                this.disableListGroups = false;
                return;
            }

            if (updateActiveStatusAction) {
                this.ruleSubRulePatterns = this.ruleSubRulePatterns.map(function (el) {
                    if (this.updatedIds.indexOf(el.pattern_id) !== -1) {
                        el.active = this.rulePatternsModelLink.active;
                    }

                    return el;
                }, {updatedIds: this.checkedSubRulePatterns, rulePatternsModelLink: this.subRulePatternsModel});
            } else {
                this.ruleSubRulePatterns = this.ruleSubRulePatterns.map(function (el) {
                    if (this.updatedIds.indexOf(el.pattern_id) !== -1) {
                        el.test = this.rulePatternsModelLink.test;
                        el.active = this.rulePatternsModelLink.active;
                        el.text = this.rulePatternsModelLink.text;
                    }

                    return el;
                }, {updatedIds: this.checkedSubRulePatterns, rulePatternsModelLink: this.subRulePatternsModel});
            }


            this.subRulePatternsModel.reset();
            this.disableListGroups = false;
            this.checkedSubRulePatterns = [];
            this.successText = 'Sub rule patterns was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating sub rule patterns!';
            this.disableListGroups = false;
        });
    }

    public deleteSubPattern(): void {
        if (this.checkedSubRulePatterns.length > 1) {
            this.deleteMultiSubPatterns();
            return;
        }

        if (!confirm('Do you sure that you want delete "' + this.subRulePatternsModel.test + '" pattern?')) {
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeletePatternUrl(), {pattern_id: this.subRulePatternsModel.pattern_id}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete pattern';
                this.disableListGroups = false;
                return;
            }

            this.ruleSubRulePatterns = this.ruleSubRulePatterns
                .filter(el => el.pattern_id != this.subRulePatternsModel.pattern_id);

            this.subRulePatternsModel.reset();
            this.disableListGroups = false;
            this.successText = 'Sub rule pattern was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting sub rule pattern!';
        });
    }

    private deleteMultiSubPatterns(): void {
        if (!confirm('Do you sure that you want delete all checked sub rule patterns?')) {
            return;
        }
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteMultiPatternUrl(), this.checkedSubRulePatterns).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete patterns';
                this.disableListGroups = false;
                return;
            }

            this.ruleSubRulePatterns = this.ruleSubRulePatterns
                .filter(el => this.checkedSubRulePatterns.indexOf(el.pattern_id) === -1);

            this.subRulePatternsModel.reset();
            this.disableListGroups = false;
            this.checkedSubRulePatterns = [];
            this.successText = 'Sub rule patterns was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting sub rule patterns!';
        });
    }
    /** ============================================= SUB RULE PATTERNS END ============================================= */


    /** ============================================= SUB RULE TEMPLATES BEGIN ============================================= */
    public createSubTemplate(): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.subRuleTemplatesModel.rule_id = this.ruleSubRulesModel.rule_id;
        this.subRuleTemplatesModel.timeslot_id = null;
        this.httpService.post(TopicsRouting.getCreateTemplateUrl(), this.subRuleTemplatesModel).then(data => {
            if (!data || !data[0] || data[0].err || !data[0].item) {
                this.errorText = 'Invalid response for create template';
                this.disableListGroups = false;
                return;
            }

            let createdItem = data[0].item.item;
            if (!createdItem) {
                this.errorText = 'New template not created!';
                return;
            }

            this.ruleSubRuleTemplates.push(createdItem);
            this.subRuleTemplatesModel.load(createdItem);
            this.disableListGroups = false;
            this.successText = 'Template was created successfully!';
        }, err => {
            this.errorText = 'Error on server while creating template!';
            this.disableListGroups = false;
        });
    }

    public updateSubTemplate(): void {
        if (this.checkedSubRuleTemplates.length > 1) {
            this.updateMultiSubTemplate();
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdateTemplateUrl(), this.subRuleTemplatesModel).then(data => {
            if (!data || !data.result || data.result[0].err || !data.result[0].item) {
                this.errorText = 'Invalid response for update template';
                this.disableListGroups = false;
                return;
            }

            let updatedItem = data.result[0].item;
            if (!updatedItem) {
                this.errorText = 'Template not updated!';
                return;
            }

            this.ruleSubRuleTemplates = this.ruleSubRuleTemplates.map(function (el) {
                if (el.template_id == this.updatedTemplate.template_id) {
                    el.text = this.updatedTemplate.text;
                }
                return el;
            }, {updatedTemplate: updatedItem});

            this.subRuleTemplatesModel.load(updatedItem);
            this.disableListGroups = false;
            this.successText = 'Template was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating template!';
            this.disableListGroups = false;
        });
    }

    private updateMultiSubTemplate(): void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getUpdateMultiTemplateUrl(), {model: this.subRuleTemplatesModel, ids: this.checkedSubRuleTemplates}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for update templates';
                this.disableListGroups = false;
                return;
            }

            this.ruleSubRuleTemplates = this.ruleSubRuleTemplates.map(function (el) {
                if (this.updatedIds.indexOf(el.template_id) !== -1) {
                    el.text = this.ruleTemplatesModelLink.text;
                    el.timeslot_id = this.ruleTemplatesModelLink.timeslot_id;
                }

                return el;
            }, {updatedIds: this.checkedSubRuleTemplates, ruleTemplatesModelLink: this.subRuleTemplatesModel});

            this.subRuleTemplatesModel.reset();
            this.disableListGroups = false;
            this.checkedSubRuleTemplates = [];
            this.successText = 'Templates was updated successfully!';
        }, err => {
            this.errorText = 'Error on server while updating templates!';
            this.disableListGroups = false;
        });
    }

    public deleteSubTemplate(): void {
        if (this.checkedSubRuleTemplates.length > 1) {
            this.deleteMultiSubTemplate();
            return;
        }

        if (!confirm('Do you sure that you want delete "' + this.subRuleTemplatesModel.text + '" template?')) {
            return;
        }

        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteTemplateUrl(), {template_id: this.subRuleTemplatesModel.template_id}).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete template';
                this.disableListGroups = false;
                return;
            }

            this.ruleSubRuleTemplates = this.ruleSubRuleTemplates
                .filter(el => el.template_id != this.subRuleTemplatesModel.template_id);

            this.subRuleTemplatesModel.reset();
            this.disableListGroups = false;
            this.successText = 'Template was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting template!';
        });
    }

    public deleteMultiSubTemplate(): void {
        if (!confirm('Do you sure that you want delete all checked sub rule templates?')) {
            return;
        }
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';

        this.httpService.post(TopicsRouting.getDeleteMultiTemplateUrl(), this.checkedSubRuleTemplates).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for delete templates';
                this.disableListGroups = false;
                return;
            }

            this.ruleSubRuleTemplates = this.ruleSubRuleTemplates
                .filter(el => this.checkedSubRuleTemplates.indexOf(el.template_id) === -1);

            this.subRuleTemplatesModel.reset();
            this.disableListGroups = false;
            this.checkedSubRuleTemplates = [];
            this.successText = 'Templates was deleted successfully!';
        }, err => {
            this.disableListGroups = false;
            this.errorText = 'Error on server while deleting templates!';
        });
    }
    /** ============================================= SUB RULE TEMPLATES END ============================================= */


    /** ============================================= PATTERN VARS BEGIN ============================================= */

    public setPatternVar() {
        if (!this.rulePatternVariableModel.pattern_var_id && ((this.rulePatternVariableModel.variable_id != "null") && (this.rulePatternVariableModel.index_content != "null"))) {
            this.createPatternVar();
            return;
        }

        if ((this.rulePatternVariableModel.variable_id == "null") && (this.rulePatternVariableModel.index_content == "null")) {
            this.deletePatternVar();
            return;
        }

        this.updatePatternVar();
    }

    private createPatternVar():void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.rulePatternVariableModel.pattern_id = this.rulePatternsModel.pattern_id;
        this.httpService.post(TopicsRouting.getCreatePatternVarUrl(), this.rulePatternVariableModel).then(data => {
            if (!data) {
                this.errorText = 'Error response from create pattern var!';
            }

            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server create pattern var!';
            this.disableListGroups = false;
        });
    }

    private updatePatternVar():void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.httpService.post(TopicsRouting.getUpdatePatternVarUrl(), this.rulePatternVariableModel).then(data => {
            if (!data) {
                this.errorText = 'Error response from update pattern var!';
            }

            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server update pattern var!';
            this.disableListGroups = false;
        });
    }

    private deletePatternVar():void {
        this.disableListGroups = true;
        this.errorText = '';
        this.successText = '';
        this.httpService.post(TopicsRouting.getDeletePatternVarUrl(), this.rulePatternVariableModel).then(data => {
            if (!data) {
                this.errorText = 'Error response from delete pattern var!';
            }

            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server delete pattern var!';
            this.disableListGroups = false;
        });
    }

    /** ============================================= PATTERN VARS END ============================================= */

    public revertTopicModel(topicData: any): void
    {
        this.availableTopics.map(function (el) {
            if (el.topic_id == this.modifiedTopicModel.topic_id) {
                this.activeTopicModel.load(el);
            }
        }, {modifiedTopicModel: topicData, activeTopicModel: this.topicModel});
    }

    public revertTopicRuleModel(ruleData: any): void
    {
        this.availableRules.map(function (el) {
            if (el.rule_id == this.modifiedTopicRuleModel.rule_id) {
                this.activeTopicRuleModel.load(el);
            }
        }, {modifiedTopicRuleModel: ruleData, activeTopicRuleModel: this.topicRuleModel});
    }

    public isValidFormByAdditionalConditions():boolean {
        if (this.disableListGroups) {
            return false;
        }

        if (this.displayModalLoader) {
            return false;
        }

        if (this.displayLoadingBlock) {
            return false;
        }

        return true;
    }

    public copyRule():void {
        this.copiedRule = {
            topicId: null,
            model: JSON.parse(JSON.stringify(this.topicRuleModel)),
            patterns: JSON.parse(JSON.stringify(this.rulePatterns)),
            templates: JSON.parse(JSON.stringify(this.ruleTemplates)),
            excPatterns: JSON.parse(JSON.stringify(this.ruleExcPatterns)),
            subRules: JSON.parse(JSON.stringify(this.ruleSubRules))
        };


        alert('Rule was copied! You can paste it to another topicData');
    }

    public pasteRule(): void {
        if (!this.copiedRule) {
            alert('Choose rule to copy!');
        }

        this.copiedRule.topicId = this.topicModel.topic_id;
        this.disableListGroups = true;

        this.httpService.post(TopicsRouting.getCopyRuleUrl(), this.copiedRule).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for copy rule';
                this.disableListGroups = false;
                return;
            }

            this.availableRules.push(data.createdRule[0]);
            this.topicRuleModel.load(data.createdRule[0]);
            this.rulePatterns = this.copiedRule.patterns;
            this.ruleTemplates = this.copiedRule.templates;
            this.ruleExcPatterns = this.copiedRule.excPatterns;
            this.ruleSubRules = this.copiedRule.ruleSubRules;

            this.disableListGroups = false;
            this.copiedRule = null;
            this.successText = 'Rule was copied successfully!';
        }, err => {
            this.errorText = 'Error on server while copying rule!';
            this.disableListGroups = false;
        });
    }

    public copyPatterns(): void {
        let patterns: Array<any> = [];
        for (let i = 0; i < this.checkedPatterns.length; i++) {
            for (let j = 0; j < this.rulePatterns.length; j++) {
                if (this.rulePatterns[j].pattern_id == this.checkedPatterns[i]) {
                    patterns.push(this.rulePatterns[j]);
                }
            }
        }

        this.copiedPatterns = {
            ruleId: null,
            patterns: JSON.parse(JSON.stringify(patterns))
        };
        alert('Patterns were copied! You can paste them to sub rule patterns');
    }

    public copyPattern(): void {
        let patterns: Array<any> = [];
        for (let j = 0; j < this.rulePatterns.length; j++) {
            if (this.rulePatterns[j].pattern_id == this.rulePatternsModel.pattern_id) {
                patterns.push(this.rulePatterns[j]);
            }
        }

        this.copiedPatterns = {
            ruleId: null,
            patterns: JSON.parse(JSON.stringify(patterns))
        };
        alert('Pattern was copied! You can paste them to sub rule patterns');
    }

    public pastePatterns(): void {
        if (!this.copiedPatterns) {
            alert('Choose patterns to copy!');
        }

        this.copiedPatterns.ruleId = this.ruleSubRulesModel.rule_id;
        this.disableListGroups = true;

        this.httpService.post(TopicsRouting.getCopyPatternsUrl(), this.copiedPatterns).then(data => {
            if (!data) {
                this.errorText = 'Invalid response for copy patterns';
                this.disableListGroups = false;
                return;
            }

            this.ruleSubRulePatterns = this.ruleSubRulePatterns.concat(this.copiedPatterns.patterns);
            this.disableListGroups = false;
            this.copiedPatterns = null;
            this.successText = 'Patterns were copied successfully!';
        }, err => {
            this.errorText = 'Error on server while copying patterns!';
            this.disableListGroups = false;
        });
    }

    public checkAllItems(itemHolder: string, checkedHolder: string, column: string): void {
        for (let i = 0; i < this[itemHolder].length; i++) {
            this[checkedHolder].push(this[itemHolder][i][column]);
        }
    }

    public uncheckAllItems(checkedHolder: string) {
        this[checkedHolder] = [];
    }

    public checkItem(checkedHolder: string, value: number): void {
        let key = this[checkedHolder].indexOf(value);
        if (key !== -1) {
            this[checkedHolder].splice(key, 1);
            return;
        }

        this[checkedHolder].push(value);
    }

    private clearDOMFromModalElements(modalId: string = ''): void {
        if (modalId) {
            let link = document.getElementById(modalId);
            link.style.display = 'none';
        }

        document.getElementsByClassName('modal-backdrop')[0].parentNode.removeChild(document.getElementsByClassName('modal-backdrop')[0]);
        document.getElementsByTagName('body')[0].classList.remove("modal-open");
    }

    private getUpdatedPrioritiesData(el: any) {
        let newIdsPriority = [];
        let elements = [].slice.call(el.parentElement.children);

        for (let i = 0; i < elements.length; i++) {
            newIdsPriority.push(elements[i].dataset.id);
        }

        return newIdsPriority;
    }

    /** ============================================= LEARNING PHRASES BEGIN ============================================= */

    public createLearningPhrase():void
    {
        this.learningPhrasesModel.topic_id = this.activeTopicId;
        this.learningPhrasesModel.rule_id = this.activeRuleId;
        this.disableListGroups = true;
        this.httpService.post(TopicsRouting.getLearningPhrasesCreateUrl(), this.learningPhrasesModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data[0]).length || data[0].err) {
                this.errorText = 'Error while creating phrase!';
                this.disableListGroups = false;
                return;
            }

            this.setLearningPhrasesAfterCreate(data);

            this.disableListGroups = false;
            this.successText = 'Phrase was created successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    public updateLearningPhrase():void
    {
        this.disableListGroups = true;
        this.httpService.post(TopicsRouting.getLearningPhrasesUpdateUrl(), this.learningPhrasesModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data.result).length) {
                this.errorText = 'Error while updating phrase!';
                this.disableListGroups = false;
                return;
            }

            this.setLearningPhrasesAfterUpdate(data);
            this.disableListGroups = false;
            this.successText = 'Phrase was updated successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    public deleteLearningPhrase():void
    {
        let unifyPhrasesData = Array.from(new Set(this.checkedClassPhrases));

        if (unifyPhrasesData.length > 1) {
            this.deleteMultiplePhrases(unifyPhrasesData);
            return;
        }

        if (!confirm('Do you sure you want to delete "' + this.learningPhrasesModel.text + '" phrase?')) {
            return;
        }

        this.disableListGroups = true;
        this.httpService.post(TopicsRouting.getLearningPhrasesDeleteUrl(), this.learningPhrasesModel).then(data => {
            if (!Object.keys(data).length || !Object.keys(data.result).length || data.result[0].err) {
                this.errorText = 'Error while deleting phrase!';
                this.disableListGroups = false;
                return;
            }

            this.filterLearningPhrasesAfterDelete();
            this.disableListGroups = false;
            this.successText = 'Phrase was deleted successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });
    }

    private deleteMultiplePhrases(phraseIds: Array<any>): void
    {
        if (!confirm('Do you sure you want to delete selected phrases?')) {
            return;
        }
        this.disableListGroups = true;

        this.httpService.post(TopicsRouting.getLearningPhrasesDeleteMultipleUrl(), {phraseIds}).then(data => {
            if (!Object.keys(data).length) {
                this.errorText = 'Error while deleting phrases!';
                this.disableListGroups = false;
                return;
            }

            this.filterLearningPhrasesAfterMultiDelete(phraseIds);
            this.disableListGroups = false;
            this.successText = 'Phrases were deleted successful!';
        }, err => {
            this.errorText = 'Error on server!';
            this.disableListGroups = false;
        });

    }

    public checkAllPhrases(): void
    {
        for (let i = 0; i < this.learningPhrases.length; i++) {
            this.checkedClassPhrases.push(this.learningPhrases[i].learning_phrase_id);
        }
    }

    public unCheckAllPhrases(): void
    {
        this.checkedClassPhrases = [];
    }

    private filterLearningPhrasesAfterMultiDelete(phraseIds: any)
    {
        let filteredPhrases: any = [];

        for (let i = 0; i < this.learningPhrases.length; i++ ) {
            if (phraseIds.indexOf(this.learningPhrases[i].learning_phrase_id) === -1) {
                filteredPhrases.push(this.learningPhrases[i]);
            }
        }

        this.learningPhrases = filteredPhrases;
    }

    private filterLearningPhrasesAfterDelete(): void
    {
        this.learningPhrases = this.learningPhrases
            .filter(x => x.learning_phrase_id != this.learningPhrasesModel.learning_phrase_id);
    }

    private setLearningPhrasesAfterCreate(responseData: any): void
    {
        if (Object.keys(responseData).length > 1) {
            for (let i=0; i < Object.keys(responseData).length; i++) {
                this.learningPhrases.push(responseData[i][0].item.item);
            }
        } else {
            this.learningPhrases.push(responseData[0].item.item);
        }
    }

    private setLearningPhrasesAfterUpdate(requestData: any): void
    {
        for (let i=0; i < Object.keys(this.learningPhrases).length; i++)
        {
            let updatedPhraseId  : number = requestData.result[0].item.learning_phrase_id;
            let updatedPhraseText: string = requestData.result[0].item.text;

            if (this.learningPhrases[i].learning_phrase_id == updatedPhraseId) {
                this.learningPhrases[i].text = updatedPhraseText;
            }
        }
    }

    public changeLearningPhrase(learningPhrase: any): void
    {
        this.learningPhrasesModel = new LearningPhrasesModel(
            learningPhrase.learning_phrase_id,
            learningPhrase.topic_id,
            learningPhrase.rule_id,
            learningPhrase.text
        );

        this.modalErrorText = '';
        this.modalSuccessText = '';
    }

    public getMarkedPhraseText(phraseId: number, phraseText: string): string
    {
        if (!this.learningPhrases[phraseId]) {
            return phraseText;
        }

        let markedText = '';

        for (let i = 0; i < this.learningPhrases[phraseId].length; i++) {
            let entityData = this.learningPhrases[phraseId][i];

            if (phraseText.indexOf(entityData.value) !== -1) {
                markedText = phraseText.replace(new RegExp('<[^>]+>|('+ entityData.value +')', 'g'), function (p1, p2) {
                    return ((p2==undefined)||p2=='') ? p1 : '<span class="marked">'+p1+'</span>';
                });

                phraseText = markedText;
            }
        }

        return phraseText;
    }

    /** ============================================= LEARNING PHRASES END ============================================= */
}
