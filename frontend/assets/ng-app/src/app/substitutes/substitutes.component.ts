import { Component }                            from '@angular/core';
import { Title }                                from "@angular/platform-browser";
import { HttpService }                          from "../shared/services/http.service";
import { OnInit }                               from '@angular/core';
import {SubstituteModel, SubstitutePhraseModel} from "../shared/models";
import { Globals }                              from "../shared/globals";

@Component({
    selector    : 'substitutes',
    templateUrl : './substitutes.index.html',
})

export class SubstitutesComponent implements OnInit
{
    public substitutionModel       : SubstituteModel;
    public substitutionPhraseModel : SubstitutePhraseModel;

    public availableSubstitutes       : any;
    public availablePhraseSubstitutes : any;

    public errorText           : string  = '';
    public successText         : string  = '';
    public displayLoadingBlock : boolean = true;

    public disableSubstitutesListItemsClass: string = '';
    public disableSubstitutePhraseListItemsClass: string = '';

    /**
     * @param {HttpService} httpService
     * @param {Title} titleService
     */
    constructor(
        private httpService : HttpService,
        private titleService: Title
    ) {
        this.titleService.setTitle( 'Substitutes' );
    }

    /**
     * On init script actions and events, here we bind available models
     */
    ngOnInit(): void
    {
        this.proceedSubstitutes();
    }

    public updateSubstitutePhrases(): void
    {
        this.disableSubstitutesListItemsClass = 'disableListItems';

        this.httpService.get('/substitutes/get-substitute-phrases', {'substituteId': this.substitutionModel.id}).then(data => {
            if (!Object.keys(data).length || !data) {
                this.substitutionPhraseModel = new SubstitutePhraseModel(0 , '');
                this.availablePhraseSubstitutes = [];
                this.disableSubstitutesListItemsClass = '';
                return;
            }

            this.availablePhraseSubstitutes = data;
            this.substitutionPhraseModel = new SubstitutePhraseModel(
                data[0].substitute_phrase_id, data[0].phrase
            );

            this.disableSubstitutesListItemsClass = '';
        }, err => {
            this.errorText = 'Error on server (subPhrases)!';
            this.disableSubstitutesListItemsClass = '';
        });
    }

    public createSubstitute(): void
    {
        this.displayLoadingBlock = true;

        this.httpService.post('/substitutes/create-substitute', {'phrase' : this.substitutionModel.text})
            .then(data => {
                if (!data) {
                   this.errorText = 'Error while creating substitute!';
                   return;
                }

                this.proceedSubstitutes();
                this.successText = 'Substitute was created successfully!';
            }, err => {
                this.errorText = 'Error on server!';
                this.displayLoadingBlock = false;
            });
    }

    public updateSubstitute(): void
    {
        this.displayLoadingBlock = true;

        this.httpService.post('/substitutes/update-substitute',
            {'substitute_id' : this.substitutionModel.id, 'phrase': this.substitutionModel.text})
            .then(data => {
                if (!data) {
                    this.errorText = 'Error while updating substitute!';
                    return;
                }

                this.proceedSubstitutes();
                this.successText = 'Substitute was updated successfully!';
            }, err => {
                this.errorText = 'Error on server!';
                this.displayLoadingBlock = false;
            });

    }

    public deleteSubstitute(): void
    {
        if (!confirm('You really want to delete substitute "' + this.substitutionModel.text + '"')) {
            return;
        }

        this.displayLoadingBlock = true;

        this.httpService.post('/substitutes/delete-substitute', {'substitute_id' : this.substitutionModel.id})
            .then(data => {
                if (!data) {
                    this.errorText = 'Error while deleting substitute!';
                    return;
                }

                this.proceedSubstitutes();
                this.successText = 'Substitute was deleted successfully!';
            }, err => {
                this.errorText = 'Error on server!';
                this.displayLoadingBlock = false;
            });
    }

    public createSubstitutePhrase(): void
    {
        this.disableSubstitutePhraseListItemsClass = 'disableListItems';

        this.httpService.post('/substitutes/create-substitute-phrase',
            {'phrase' : this.substitutionPhraseModel.text, 'substitute_id': this.substitutionModel.id})
            .then(data => {
                if (!data) {
                    this.errorText = 'Error while creating substitute phrase!';
                    this.disableSubstitutePhraseListItemsClass = '';
                    this.substitutionPhraseModel = new SubstitutePhraseModel(0 , '');
                    return;
                }

                this.availablePhraseSubstitutes = data;
                this.substitutionPhraseModel = new SubstitutePhraseModel(
                    data[0].substitute_phrase_id, data[0].phrase
                );

                this.disableSubstitutePhraseListItemsClass = '';
                this.successText = 'Substitute phrase was created successfully!';
            }, err => {
                this.errorText = 'Error on server!';
                this.disableSubstitutePhraseListItemsClass = '';
            });
    }

    public updateSubstitutePhrase(): void
    {
        this.disableSubstitutePhraseListItemsClass = 'disableListItems';

        this.httpService.post('/substitutes/update-substitute-phrase',
            {'substitute_id' : this.substitutionModel.id, 'phrase': this.substitutionPhraseModel.text, 'substitute_phrase_id': this.substitutionPhraseModel.id})
            .then(data => {
                if (!data) {
                    this.errorText = 'Error while updating substitute phrase!';
                    this.disableSubstitutePhraseListItemsClass = '';
                    this.substitutionPhraseModel = new SubstitutePhraseModel(0 , '');
                    return;
                }

                this.availablePhraseSubstitutes = data;
                this.substitutionPhraseModel = new SubstitutePhraseModel(
                    data[0].substitute_phrase_id, data[0].phrase
                );

                this.successText = 'Substitute phrase was updated successfully!';
                this.disableSubstitutePhraseListItemsClass = '';
            }, err => {
                this.errorText = 'Error on server!';
                this.disableSubstitutePhraseListItemsClass = '';
            });
    }

    public deleteSubstitutePhrase(): void
    {
        if (!confirm('You really want to delete phrase "' + this.substitutionPhraseModel.text + '"')) {
            return;
        }

        this.disableSubstitutePhraseListItemsClass = 'disableListItems';

        this.httpService.post('/substitutes/delete-substitute-phrase',
            {'substitute_id' : this.substitutionModel.id, 'substitute_phrase_id': this.substitutionPhraseModel.id})
            .then(data => {
                if (!data) {
                    this.errorText = 'Error while deleting substitute phrase!';
                    this.disableSubstitutePhraseListItemsClass = '';
                    this.substitutionPhraseModel = new SubstitutePhraseModel(0 , '');
                    return;
                }

                this.availablePhraseSubstitutes = data;
                this.substitutionPhraseModel = new SubstitutePhraseModel(
                    data[0].substitute_phrase_id, data[0].phrase
                );

                this.disableSubstitutePhraseListItemsClass = '';
                this.successText = 'Substitute phrase was deleted successfully!';
            }, err => {
                this.errorText = 'Error on server!';
                this.disableSubstitutePhraseListItemsClass = '';
            });
    }

    private proceedSubstitutes(): void
    {
        this.httpService.get('/substitutes/get-substitutes').then(data => {
            if (!data) {
                this.errorText = 'No substitutions!';
                this.substitutionModel = new SubstituteModel(0 , '');
                this.displayLoadingBlock = false;
                return;
            }

            this.availableSubstitutes = data;
            this.substitutionModel = new SubstituteModel(
                data[0].substitute_id, data[0].phrase
            );

            this.getSubstitutePhrases(this.substitutionModel.id);

            this.displayLoadingBlock = false;
        }, err => {
            this.errorText = 'Error on server!';
            this.displayLoadingBlock = false;
        });
    }

    private getSubstitutePhrases(substituteId: number): void
    {
        this.httpService.get('/substitutes/get-substitute-phrases?substituteId=' + substituteId).then(data => {
            if (!Object.keys(data).length || !data) {
                this.substitutionPhraseModel = new SubstitutePhraseModel(0 , '');
                return;
            }

            this.availablePhraseSubstitutes = data;
            this.substitutionPhraseModel = new SubstitutePhraseModel(
                data[0].substitute_phrase_id, data[0].phrase
            );

        }, err => {
            this.errorText = 'Error on server (subPhrases)!';
        });
    }

    /**
     * Just call {{diagnostic}} in template and watch model status in live time
     *
     * @returns {string}
     */
    get diagnostic() { return JSON.stringify(this.substitutionModel); }
    get diagnosticPhrase() { return JSON.stringify(this.substitutionPhraseModel); }
}
