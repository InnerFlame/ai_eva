<?php

use yii\db\Migration;

class m160711_124439_insert_general_group_group_phrases extends Migration
{
    public function up()
    {
        $this->insert('group_group_phrases_table', [
            'id' => 1,
            'name' => 'General',
        ]);
    }

    public function down()
    {
        $this->delete('group_group_phrases_table', [
            'id' => 1,
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
