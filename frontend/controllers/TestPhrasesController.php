<?php
namespace frontend\controllers;

use frontend\models\testPhrases\TestPhrasesFormModel;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class TestPhrasesController
 * @package frontend\controllers
 */
class TestPhrasesController extends BaseNgController
{
    /**
     * @inheritdoc
     *
     * TestPhrasesController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new TestPhrasesFormModel());
    }

    /**
     * @inheritdoc
     *
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('testChat')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * Renders main test phrases page.
     *
     * @return string Result of rendering.
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionGetModels(): array
    {
        $apiResult = $this->getFormModel()->getClassifiersStatus();
        $result = $apiResult['available_projects'] ?? [];

        foreach ($result as $key => $value) {
            $result[$key] = $this->sortModels($value['available_models']);
        }

        return $result;
    }

    /**
     * @param array $modelsArray
     * @return array
     */
    private function sortModels(array $modelsArray): array
    {
        $sortedModels = [];
        $preparedModels = [];

        foreach ($modelsArray as $model) {
            $modelParts     = explode('_', $model);
            $modelDateParts = explode('-', $modelParts[1]);

            $modelName  = (string) $modelParts[0];
            $modelDate  = (int) $modelDateParts[0];
            $modelTime  = (int) $modelDateParts[1];

            $sortedModels[$modelName][strtotime($modelDate)][$modelTime] = $model;
            krsort($sortedModels[$modelName]);
            krsort($sortedModels[$modelName][strtotime($modelDate)]);
        }

        foreach ($sortedModels as $name => $values) {
            foreach ($values as $date => $timeValues) {
                foreach ($timeValues as $modelValue) {
                    $preparedModels[] = $modelValue;
                }
            }
        }

        return $preparedModels;
    }

    /**
     * @return array
     */
    public function actionGetResult(): array
    {
        $testResults = $this->getFormModel()->getResponseTestResults(
            $this->requestData['text'],
            $this->requestData['model'],
            $this->requestData['project']
        );

        return $testResults;
    }

    /**
     * @inheritdoc
     *
     * @return TestPhrasesFormModel
     */
    protected function getFormModel(): TestPhrasesFormModel
    {
        return $this->formModel;
    }
}
