<?php
use common\widgets\RadioButton\RadioButton;
use common\widgets\CollapsableLink\CollapsableLink;
use common\widgets\BindableForm\BindableForm;
use common\widgets\Modal\Modal;
use yii\bootstrap\Html;
use frontend\assets\TopicsAsset;

TopicsAsset::register($this);

//Modal add new topic
echo Modal::widget([
    'id' => 'addNewTopicModal',
    'title' => 'Add new topic',
    'inputId' => 'addNewTopicName',
    'inputLabelText' => 'Topic name:',
    'buttonId' => 'addNewTopicButton',
    'buttonText' => 'Add new topic',
    'hasForm' => true
]);

//Modal confirm delete topic
echo Modal::widget([
    'id' => 'confirmDeleteTopicModal',
    'title' => 'Delete topic',
    'hiddenInputIds' => ['deleteTopicId',],
    'nameText' => 'topic',
    'nameId' => 'deleteTopicName',
    'buttonId' => 'confirmDeleteTopicButton'
]);

//Modal add new rule
echo Modal::widget([
    'id' => 'addNewRuleModal',
    'title' => 'Add new rule',
    'inputId' => 'addNewRuleName',
    'inputLabelText' => 'Rule name:',
    'buttonId' => 'addNewRuleButton',
    'buttonText' => 'Add new rule',
    'hasForm' => true
]);

//Modal confirm delete rule
echo Modal::widget([
    'id' => 'confirmDeleteRuleModal',
    'title' => 'Delete rule',
    'hiddenInputIds' => ['deleteRuleTopicId', 'deleteRuleId',],
    'nameText' => 'rule',
    'nameId' => 'deleteRuleName',
    'buttonId' => 'confirmDeleteRuleButton'
]);

//Modal confirm delete subrule
echo Modal::widget([
    'id' => 'confirmDeleteSubRuleModal',
    'title' => 'Delete subrule',
    'hiddenInputIds' => ['deleteSubRuleId',],
    'nameText' => 'subrule',
    'nameId' => 'deleteSubRuleName',
    'buttonId' => 'confirmDeleteSubRuleButton'
]);

// Available variables part
$variablesItemsContent = Html::beginTag('ul', [
    'class' => 'dropdown-menu dropdown-menu-right',
    'id' => 'availableVariables',
]);
$variablesItemsContent .= Html::beginTag('li');
$variablesItemsContent .= Html::a('No variables', 'javascript:void(0);');
$variablesItemsContent .= Html::hiddenInput(null, \frontend\models\DataProvider::DEFAULT_ID);
$variablesItemsContent .= Html::endTag('li');
foreach ($model->availableVariables as $variable) {
    $variablesItemsContent .= Html::beginTag('li');
    $variablesItemsContent .= Html::a($variable->getName(), 'javascript:void(0);');
    $variablesItemsContent .= Html::hiddenInput(null, $variable->getVariableId());
    $variablesItemsContent .= Html::endTag('li');
}
$variablesItemsContent .= Html::endTag('ul');

// Available regexp part
$regexpItemsContent = Html::beginTag('ul', [
    'class' => 'dropdown-menu dropdown-menu-right',
    'id' => 'availableRegexp',
]);
$regexpItemsContent .= Html::endTag('ul');
?>

<!-- Modal progress -->
<div role="dialog" class="modal fade" id="progressModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">Progress</h4></div>
            <div class="modal-body">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100"
                     aria-valuemin="0" aria-valuemax="100" style="width:100%">Progress...</div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<input type="hidden" value="<?= $model->currentTopicId ?>" id="currentTopicId"/>
<input type="hidden" value="<?= $model->currentRuleId ?>" id="currentRuleId"/>
<input type="hidden" value="<?= $model->currentPatternId ?>" id="currentPatternId"/>
<input type="hidden" value="<?= $model->currentTemplateId ?>" id="currentTemplateId"/>
<input type="hidden" value="<?= $model->currentExcPatternId ?>" id="currentExcPatternId"/>

<input type="hidden" value="" id="currentSubRuleId"/>
<input type="hidden" value="" id="currentSubPatternId"/>
<input type="hidden" value="" id="currentSubTemplateId"/>

<!-- TOPICS CONTAINER -->
<div class="col-md-3 collapsable-container" id="topicsContainer">
    <?php
    echo CollapsableLink::widget([
        'columnId' => 'topicsContainer',
        'rubberColumnId' => 'propertiesContainer',
    ]);
    ?>
    <div id="topics"></div>
</div>
<!-- RULES CONTAINER -->
<div class="col-md-3 collapsable-container" id="rulesContainer">
    <?php
    echo CollapsableLink::widget([
        'columnId' => 'rulesContainer',
        'rubberColumnId' => 'propertiesContainer',
    ]);
    ?>
    <div id="rules"></div>
</div>
<!-- PROPERTIES CONTAINER -->
<div class="col-md-6 collapsable-container" id="propertiesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li<?= ($model->currentTab == \frontend\models\topics\TopicsForm::PATTERNS_TAB) ? ' class="active"' : ''?>><a data-toggle="tab" href="#patternsContainer">Patterns</a></li>
        <li<?= ($model->currentTab == \frontend\models\topics\TopicsForm::TEMPLATES_TAB) ? ' class="active"' : ''?>><a data-toggle="tab" href="#templatesContainer">Templates</a></li>
        <li<?= ($model->currentTab == \frontend\models\topics\TopicsForm::EXC_PATTERNS_TAB) ? ' class="active"' : ''?>><a data-toggle="tab" href="#excPatternsContainer">Exc. patterns</a></li>
        <li><a data-toggle="tab" href="#subRulesContainer">Sub rules</a></li>
    </ul>
    <div class="tab-content">
        <!-- PATTERNS -->
        <div id="patternsContainer" class="tab-pane <?= ($model->currentTab == \frontend\models\topics\TopicsForm::PATTERNS_TAB) ? 'active' : ''?>">
            <div id="regularExpressionCheckModal" class="modal" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <strong></strong>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo BindableForm::widget([
                'id' => 'patternsBindableForm',
                'hiddens' => [
                    ['name' => 'patternId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::input('text', 'testText', '', [
                                'class' => 'form-control',
                                'id' => 'regularExpressionText',
                            ])
                        ],
                    ]],
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows' => '5',
                                'id' => 'regularExpressionPattern',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => RadioButton::widget([
                            'id' => 'updatePatternActiveTestRadioButton',
                            'activeText' => 'Active',
                            'text' => 'Test',
                            'glyphiconClass' => 'glyphicon-signal',
                            'activeState' => false,
                            'name' => 'activePatternIcon',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Check', [
                            'class' => 'btn btn-default btn-block',
                            'type' => 'button',
                            'id' => 'checkRegularExpressionButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type' => 'button',
                            'id' => 'deletePatternButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Copy', [
                            'class' => 'btn btn-default btn-block',
                            'type' => 'button',
                            'id' => 'copyPatternsButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type' => 'button',
                            'id' => 'addNewPatternButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Check all', [
                            'class' => 'btn btn-default btn-block',
                            'type' => 'button',
                            'id' => 'checkAllPatternsButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type' => 'button',
                            'id' => 'updatePatternButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'hr'],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' =>
                            Html::button(
                                Html::tag('span', 'No variables', ['class' => 'pull-left', 'id' => 'currentVariableName',])
                                . Html::hiddenInput(
                                    'currentVariableId',
                                    \frontend\models\DataProvider::DEFAULT_ID,
                                    ['id' => 'currentVariableId',]
                                )
                                . Html::hiddenInput(
                                    'currentVariableRegexpId',
                                    \frontend\models\DataProvider::DEFAULT_ID,
                                    ['id' => 'currentVariableRegexpId',]
                                )
                                . Html::tag('span', '', ['class' => 'caret']),
                                [
                                    'class' => 'btn btn-default dropdown-toggle btn-block',
                                    'aria-expanded' => 'false',
                                    'aria-haspopup' => 'true',
                                    'data-toggle' => 'dropdown',
                                    'type' => 'button',
                                    'style' => 'text-align: right',
                                    'id' => 'availableVariablesButton',
                                ]
                            )
                            . $variablesItemsContent
                        ],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' =>
                            Html::button(
                                Html::tag('span', 'No regexp', ['class' => 'pull-left', 'id' => 'currentRegexpName',])
                                . Html::hiddenInput(
                                    'currentRegexpId',
                                    \frontend\models\DataProvider::DEFAULT_ID,
                                    ['id' => 'currentRegexpId',]
                                )
                                . Html::tag('span', '', ['class' => 'caret']),
                                [
                                    'class' => 'btn btn-default dropdown-toggle btn-block',
                                    'aria-expanded' => 'false',
                                    'aria-haspopup' => 'true',
                                    'data-toggle' => 'dropdown',
                                    'type' => 'button',
                                    'style' => 'text-align: right',
                                    'id' => 'availableRegexpButton',
                                ]
                            )
                            . $regexpItemsContent
                        ],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Set', [
                            'class' => 'btn btn-primary btn-block',
                            'type' => 'button',
                            'id' => 'updatePatternVariable',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="patterns"></div>
        </div>

        <?php
        $timeSlotsOptions = "";
        $timeSlotsOptions .= "<option value='-1'>Choose timeslot</option>";
        foreach ($model->availableTimeslots as $timeslot) {
            $timeSlotId   = $timeslot['timeslot_id'];
            $timeSlotName = $timeslot['name'];
            $timeSlotsOptions .= "<option value='$timeSlotId'>$timeSlotName</option>";
        }
        ?>
        <!-- TEMPLATES -->
        <div id="templatesContainer" class="tab-pane <?= ($model->currentTab == \frontend\models\topics\TopicsForm::TEMPLATES_TAB) ? 'active' : ''?>">
            <?php
            echo BindableForm::widget([
                'id' => 'templatesBindableForm',
                'hiddens' => [
                    ['name' => 'templateId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows' => '5',
                                'id' => 'templateText',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        [
                            'tag' => 'select', 'options' => ['class' => 'form-control', 'id' => 'templateTimeslot'],
                            'content' => $timeSlotsOptions
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type' => 'button',
                            'id' => 'deleteTemplateButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type' => 'button',
                            'id' => 'addNewTemplateButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Check all', [
                            'class' => 'btn btn-default btn-block',
                            'type' => 'button',
                            'id' => 'checkAllTemplatesButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type' => 'button',
                            'id' => 'updateTemplateButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="templates"></div>
        </div>
        <!-- EXC. PATTERNS -->
        <div id="excPatternsContainer" class="tab-pane <?= ($model->currentTab == \frontend\models\topics\TopicsForm::EXC_PATTERNS_TAB) ? 'active' : ''?>">
            <div id="excRegularExpressionCheckModal" class="modal" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <strong></strong>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo BindableForm::widget([
                'id' => 'excPatternsBindableForm',
                'hiddens' => [
                    ['name' => 'excPatternId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::input('text', 'testText', '', [
                                'class' => 'form-control',
                                'id' => 'excRegularExpressionText',
                            ])
                        ],
                    ]],
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows' => '5',
                                'id' => 'excRegularExpressionPattern',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => RadioButton::widget([
                            'id' => 'updateExcPatternActiveTestRadioButton',
                            'activeText' => 'Active',
                            'text' => 'Test',
                            'glyphiconClass' => 'glyphicon-signal',
                            'activeState' => false,
                            'name' => 'activeExcPatternIcon',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Check', [
                            'class' => 'btn btn-default btn-block',
                            'type' => 'button',
                            'id' => 'checkExcRegularExpressionButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-2'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type' => 'button',
                            'id' => 'deleteExcPatternButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-3'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type' => 'button',
                            'id' => 'addNewExcPatternButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-7'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type' => 'button',
                            'id' => 'updateExcPatternButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="excPatterns"></div>
        </div>
        <!-- SUB. RULES -->
        <div id="subRulesContainer" class="tab-pane">
            <?php
            echo BindableForm::widget([
                'id' => 'subRulesBindableForm',
                'hiddens' => [
                    ['name' => 'subRuleId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::input('text', 'itemText', '', [
                                'class' => 'form-control',
                                'id' => 'subRulesText',
                            ])
                        ],
                    ]],
                    ['line' => [
                        [
                            'tag' => 'label', 'options' => ['class' => 'col-sm-6 control-label', 'style' => 'text-align: left'],
                            'content' => 'Number of execution:'
                        ],
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-6'],
                            'content' => Html::input('text', 'itemExecution', '', [
                                'class' => 'form-control',
                                'id' => 'subRulesExecution',
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                            'id' => 'updateSubRulesActiveTestRadioButton',
                            'activeText' => 'Active',
                            'text' => 'Test',
                            'glyphiconClass' => 'glyphicon-signal',
                            'activeState' => false,
                            'name' => 'activeSubRulesIcon',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                            'id' => 'updateSubRulesOnOffRadioButton',
                            'activeText' => 'On',
                            'text' => 'Off',
                            'glyphiconClass' => 'glyphicon-off',
                            'activeState' => false,
                            'name' => 'onSubRulesIcon',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                            'id' => 'updateSubRulesExcIncRadioButton',
                            'activeText' => 'Exc',
                            'text' => 'Inc',
                            'glyphiconClass' => 'glyphicon-ok',
                            'activeState' => false,
                            'name' => 'excSubRulesIcon',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-2'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type' => 'button',
                            'id' => 'deleteSubRulesButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-3'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type' => 'button',
                            'id' => 'addNewSubRulesButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-7'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type' => 'button',
                            'id' => 'updateSubRulesButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="subRules"></div>
            <!-- SUB RULES PROPERTIES -->
            <ul class="nav nav-tabs" style="margin-bottom: 10px">
                <li class="active"><a data-toggle="tab" href="#subPatternsContainer">Patterns</a></li>
                <li><a data-toggle="tab" href="#subTemplatesContainer">Templates</a></li>
            </ul>
            <div class="tab-content">
                <!-- PATTERNS -->
                <div id="subPatternsContainer" class="tab-pane active">
                    <div id="subRegularExpressionCheckModal" class="modal" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <strong></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    echo BindableForm::widget([
                        'id' => 'subPatternsBindableForm',
                        'hiddens' => [
                            ['name' => 'patternId'],
                        ],
                        'lines' => [
                            ['line' => [
                                [
                                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                                    'content' => Html::input('text', 'testText', '', [
                                        'class' => 'form-control',
                                        'id' => 'subRegularExpressionText',
                                    ])
                                ],
                            ]],
                            ['line' => [
                                [
                                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                                    'content' => Html::textarea('itemText', '', [
                                        'class' => 'form-control',
                                        'rows' => '5',
                                        'id' => 'subRegularExpressionPattern',
                                        'style' => 'resize:vertical;'
                                    ])
                                ],
                            ]],
                            ['line' => [
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => RadioButton::widget([
                                    'id' => 'subUpdatePatternActiveTestRadioButton',
                                    'activeText' => 'Active',
                                    'text' => 'Test',
                                    'glyphiconClass' => 'glyphicon-signal',
                                    'activeState' => false,
                                    'name' => 'activePatternIcon',
                                ])],
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Check', [
                                    'class' => 'btn btn-default btn-block',
                                    'type' => 'button',
                                    'id' => 'subCheckRegularExpressionButton',
                                ])],
                            ]],
                            ['line' => [
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                                    'class' => 'btn btn-danger btn-block',
                                    'type' => 'button',
                                    'id' => 'subDeletePatternButton',
                                ])],
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Paste', [
                                    'class' => 'btn btn-default btn-block',
                                    'type' => 'button',
                                    'id' => 'pastePatternsButton',
                                ])],
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Add', [
                                    'class' => 'btn btn-success btn-block',
                                    'type' => 'button',
                                    'id' => 'subAddNewPatternButton',
                                ])],
                            ]],
                            ['line' => [
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Check all', [
                                    'class' => 'btn btn-default btn-block',
                                    'type' => 'button',
                                    'id' => 'checkAllSubPatternsButton',
                                ])],
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                                    'class' => 'btn btn-primary btn-block',
                                    'type' => 'button',
                                    'id' => 'subUpdatePatternButton',
                                ])],
                            ]],
                        ],
                    ]);
                    ?>
                    <div id="subPatterns"></div>
                </div>
                <!-- TEMPLATES -->
                <div id="subTemplatesContainer" class="tab-pane">
                    <?php
                    echo BindableForm::widget([
                        'id' => 'subTemplatesBindableForm',
                        'hiddens' => [
                            ['name' => 'templateId'],
                        ],
                        'lines' => [
                            ['line' => [
                                [
                                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                                    'content' => Html::textarea('itemText', '', [
                                        'class' => 'form-control',
                                        'rows' => '5',
                                        'id' => 'subTemplateText',
                                        'style' => 'resize:vertical;'
                                    ])
                                ],
                            ]],
                            ['line' => [
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                                    'class' => 'btn btn-danger btn-block',
                                    'type' => 'button',
                                    'id' => 'subDeleteTemplateButton',
                                ])],
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Add', [
                                    'class' => 'btn btn-success btn-block',
                                    'type' => 'button',
                                    'id' => 'subAddNewTemplateButton',
                                ])],
                            ]],
                            ['line' => [
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Check all', [
                                    'class' => 'btn btn-default btn-block',
                                    'type' => 'button',
                                    'id' => 'checkAllSubTemplatesButton',
                                ])],
                                ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Update', [
                                    'class' => 'btn btn-primary btn-block',
                                    'type' => 'button',
                                    'id' => 'subUpdateTemplateButton',
                                ])],
                            ]],
                        ],
                    ]);
                    ?>
                    <div id="subTemplates"></div>
                </div>
            </div>
        </div>
    </div>
</div>