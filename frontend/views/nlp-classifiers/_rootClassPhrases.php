<?php
use common\widgets\BindableFormItems\BindableFormItems;

$classPhrasesItems = [];

foreach ($model->phrases as $phrase) {
    $text = $phrase->text;
    foreach ($phrase->entities as $entity) {
        if (strpos($text, $entity['value']) !== false) {
            $text = str_replace($entity['value'], '<mark style=\'background-color: yellow\'>'.$entity['value'].'</mark>', $text);
        }
    }
    $item['text'] = $text . '<span class="glyphicon glyphicon-chevron-down pull-right openRootEntities"></span>';
    $item['hiddens'] = [
        ['name' => 'rootClassId',   'value' => $phrase->classId],
        ['name' => 'rootPhraseId',  'value' => $phrase->id],
    ];
    $classPhrasesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId'    => 'rootPhrasesBindableForm',
    'items'             => $classPhrasesItems,
    'checked'           => true,
]);
