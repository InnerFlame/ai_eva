<?php
use common\widgets\ExpandedTable\ExpandedTable;

$placeholdersItems = array();
foreach ($model->placeholders as $placeholder) {
    $item = null;
    $item[0][] = ['value' => $placeholder->id, 'hidden' => $placeholder->id];
    $item[1][] = ['value' => '<input type="checkbox" name="itemCheckbox">', 'hidden' => $placeholder->id];
    $item[2][] = ['value' => $placeholder->name, 'hidden' => $placeholder->name];
    $item[3][] = ['value' => $placeholder->type, 'hidden' => $placeholder->type];
    $item[4][] = ['value' => $placeholder->url, 'hidden' => $placeholder->url];
    $item[5][] = ['value' => $placeholder->url_wap, 'hidden' => $placeholder->url_wap];
    $item[6][] = ['value' => $placeholder->url_type, 'hidden' => $placeholder->url_type];

    $item[7][] = null;
    foreach ($placeholder->senderTypes as $senderType) {
        $item[7][] = ['value' => $senderType->sender_type_name, 'hidden' => $senderType->sender_type_name];
    }

    $item[8][] = null;
    foreach ($placeholder->receiverLocations as $receiverLocation) {
        $item[8][] = [
            'value' => $receiverLocation->receiver_location_name,
            'hidden' => $receiverLocation->receiver_location_name
        ];
    }

    $placeholderActiveText = '<span class="glyphicon glyphicon-off';
    if ($placeholder->active) {
        $placeholderActiveText .= ' active';
    }
    $placeholderActiveText .= '"></span>';
    $item[9][] = ['value' => $placeholderActiveText, 'hidden' => $placeholder->active];

    $placeholderIsLink = Yii::$app->params['placeholderIsLink'];
    $item[10][] = [
        'value' => $placeholderIsLink[$placeholder->is_link],
        'hidden' => $placeholderIsLink[$placeholder->is_link],
    ];

    $placeholdersItems[] = $item;
}

echo ExpandedTable::widget([
    'headerItems' => [
        ['label' => 'Id',],
        ['label' => '',],
        ['label' => 'Name',],
        ['label' => 'Placeholder type',],
        ['label' => 'URL',],
        ['label' => 'URL wap',],
        ['label' => 'URL type',],
        ['label' => 'Senders type',],
        ['label' => 'Receivers loc',],
        ['label' => 'Active',],
        ['label' => 'Is link',],
    ],
    'items' => $placeholdersItems,
]);
