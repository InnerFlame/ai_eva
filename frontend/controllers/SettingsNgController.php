<?php

namespace frontend\controllers;

use frontend\models\settings\Setting;
use frontend\models\settings\SettingsFormNg;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class SettingsNgController
 * @package frontend\controllers
 */
class SettingsNgController extends BaseNgController
{
    /**
     * SettingsNgController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new SettingsFormNg());
    }

    /**
     * @inheritdoc
     *
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('readSettings')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * Renders main chat page
     *
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * Creates new dummy inActive setting
     *
     * @return array
     */
    public function actionCreate(): array
    {
        if (!\Yii::$app->user->can('editSettings')) {
            return [];
        }

        $name           = $this->requestData['name'];
        $controlProfile = $this->requestData['controlProfile'];
        $targetProfile  = $this->requestData['targetProfile'];

        if (!$this->getFormModel()->createSetting($name, $controlProfile, $targetProfile)) {
            return [];
        }

        return $this->actionGetSettingModels();
    }

    /**
     * Update setting with all relations
     *
     * @return array
     */
    public function actionUpdate(): array
    {
        if (!\Yii::$app->user->can('editSettings')) {
            return [];
        }

        if (!$this->getFormModel()->updateSetting($this->requestData)) {
            return [];
        }

        return [
            'status' => true
        ];
    }

    /**
     * Copy setting by id
     *
     * @return array
     */
    public function actionCopy(): array
    {
        if (!\Yii::$app->user->can('editSettings')) {
            return [];
        }


        if (!$settingId = (int) $this->requestData['settingId']) {
            return [];
        }

        $copiedSettingId = $this->getFormModel()->copySetting($settingId);

        if (!$copiedSettingId) {
            return [];
        }

        return ['settingId' => $copiedSettingId];
    }

    /**
     * Deletes setting with all relations
     *
     * @return array
     */
    public function actionDelete(): array
    {
        if (!\Yii::$app->user->can('editSettings')) {
            return [];
        }

        $settingId = (int) $this->requestData['settingId'];

        if (!$this->getFormModel()->deleteSettingById($settingId)) {
            return [];
        }

        return $this->actionGetSettingModels();
    }

    /**
     * @param $settingId
     * @return array
     */
    public function actionGetSettingDataForUpdate($settingId)
    {
        if (!$settingModel = $this->findModel((int) $settingId)) {
            return [];
        }

        return $this->getFormModel()->getUpdateSettingInitialData($settingModel);
    }

    /**
     * Get setting models for table on index page
     *
     * @return array
     */
    public function actionGetSettingModels(): array
    {
        $models = Setting::find()
            ->orderBy('priority')
            ->with('scenarios')->with('trafficSources')->with('fanClubs')->with('onlines')
            ->asArray()
            ->all();

        $relations = [];

        if (!$models) {
            return [];
        }

        foreach ($models as $index => $model) {
            if (!empty($model['scenarios'])) {
                $relations[$index]['scenarios'] = $this->getFormModel()
                    ->getScenariosForIndexRelations($model['scenarios']);
            }
            if (!empty($model['trafficSources'])) {
                $relations[$index]['trafficSources'] = $this->getFormModel()
                    ->getExcludedTrafficSourcesForIndex($model['trafficSources']);
            }
        }

        return ['models' => $models, 'relations' => $relations];
    }


    /**
     * Toggle active sstatus for setting record
     *
     * @return array
     */
    public function actionActivationToggle()
    {
        if (!\Yii::$app->user->can('editSettings')) {
            return [];
        }

        $settingId = (int) $this->requestData['settingId'];

        if (empty($this->getFormModel()->toggleSettingActiveStatus($settingId))) {
            return [];
        }

        return $this->actionGetSettingModels();
    }

    /**
     * Updates setting priory and shift other models priority on 1 step if it needed
     *
     * @return array
     */
    public function actionUpdateOrder(): array
    {
        if (empty($this->requestData['newPrioritiesIds'])) {
            return [];
        }

        return [
            'status' => $this->getFormModel()
                ->updateSettingsPriorities($this->requestData['newPrioritiesIds'])
        ];
    }

    /**
     * Get data for create Form dropDowns on index page
     *
     * @return array
     */
    public function actionGetCreateDropDownData(): array
    {
        return $this->getFormModel()->getAvailableProfilesData();
    }

    /**
     * @param int $settingId
     * @return array|Setting
     */
    private function findModel(int $settingId)
    {
        $settingModel = Setting::findOne((int) $settingId);

        if (!$settingModel) {
            return [];
        }

        return $settingModel;
    }

    /**
     * @inheritdoc
     * 
     * @return SettingsFormNg
     */
    protected function getFormModel(): SettingsFormNg
    {
        return $this->formModel;
    }
}
