//If previous item was expanded this function collapse all items before we expand current item
function collapseAllItems(columnEl)
{
    columnEl.children('ul').children('li').children('form').collapse('hide');
    columnEl.children('ul').children('li').children('span').children('a')
        .children('span.glyphicon-chevron-down')
        .removeClass('glyphicon-chevron-down')
        .addClass('glyphicon-chevron-right');
}

$(document).ready(function () {
    //Click column item
    $(document).on('click', '.column li.list-group-item', function () {
        if ($(this).hasClass('current')) {
            return;
        }
        $(this).siblings().removeClass('current');
        $(this).addClass('current');
        if ($(this).siblings().children('form[aria-expanded="true"]').length != 0) {
            $(this).children('form').collapse('show');
            $(this).children('span').children('a')
                .children('span')
                .removeClass('glyphicon-chevron-right')
                .addClass('glyphicon-chevron-down');
            setTimeout(function(x){
                $(x).siblings().children('form').collapse('hide');
                $(x).siblings().children('span').children('a')
                    .children('span')
                    .removeClass('glyphicon-chevron-down')
                    .addClass('glyphicon-chevron-right');
            }, 100, this);
        }
    });

    //Click collapse column item link
    $(document).on('click', '.column a', function () {
        if ($(this).attr('aria-expanded') == 'true') {
            $(this).children('span').removeClass('glyphicon-chevron-right');
            $(this).children('span').addClass('glyphicon-chevron-down');
        } else {
            $(this).children('span').removeClass('glyphicon-chevron-down');
            $(this).children('span').addClass('glyphicon-chevron-right');
        }
    });
});
