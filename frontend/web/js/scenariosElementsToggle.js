/**
 * This functionality need to hide & show blocks in scenarios diagram page
 * @type {jQuery|HTMLElement}
 */
var toggleScenariosBlocksBtn    = $('.toggle-scenarioBlocksContainer');
var togglePhrasesBlocksBtn      = $('.toggle-scenarioPhrasesClassifiersContainer');
var scenariosBlocksHolder       = $('#scenarioBlocksContainer');
var phrasesBlocksHolder         = $('#scenarioPhrasesClassifiersContainer');
var scenarioDiagramHolder       = $('#scenarioDiagram');


toggleScenariosBlocksBtn.click(function () {
    scenariosBlocksHolder.toggle();
    if (scenariosBlocksHolder.is(":visible")) {
        scenarioDiagramHolder.removeClass('col-md-8');
        scenarioDiagramHolder.addClass('col-md-6');
        if (phrasesBlocksHolder.is(":visible")) {
            scenarioDiagramHolder.removeClass('col-md-10');
            scenarioDiagramHolder.addClass('col-md-8');
        }
    } else {
        scenarioDiagramHolder.removeClass('col-md-6');
        scenarioDiagramHolder.addClass('col-md-8');

        if (!phrasesBlocksHolder.is(":visible")) {
            scenarioDiagramHolder.removeClass('col-md-8');
            scenarioDiagramHolder.addClass('col-md-10');
        }
    }
});

togglePhrasesBlocksBtn.click(function () {
    phrasesBlocksHolder.toggle();
    if (phrasesBlocksHolder.is(":visible")) {
        scenarioDiagramHolder.removeClass('col-md-8');
        scenarioDiagramHolder.addClass('col-md-6');

        if (scenariosBlocksHolder.is(":visible")) {
            scenarioDiagramHolder.removeClass('col-md-10');
            scenarioDiagramHolder.addClass('col-md-8');
        }
    } else {
        scenarioDiagramHolder.removeClass('col-md-6');
        scenarioDiagramHolder.addClass('col-md-8');

        if (!scenariosBlocksHolder.is(":visible")) {
            scenarioDiagramHolder.removeClass('col-md-8');
            scenarioDiagramHolder.addClass('col-md-10');
        }
    }
});