//Expand column (increase column width)
function expandColumn(columnEl)
{
    for (var i = 1; i <= 11; i++) {
        if (columnEl.hasClass('col-md-' + i)) {
            columnEl.removeClass('collapsed-column');
            columnEl.removeClass('col-md-' + i);
            columnEl.addClass('col-md-' + (i + 1));
            break;
        }
    }
}

//Collapse column (decrease column width)
function collapseColumn(columnEl)
{
    for (var i = 2; i <= 12; i++) {
        if (columnEl.hasClass('col-md-' + i)) {
            columnEl.addClass('collapsed-column');
            columnEl.removeClass('col-md-' + i);
            columnEl.addClass('col-md-' + (i - 1));
            break;
        }
    }
}

//Click collapse column link
function collapseColumnLinkClick(el, columnId, rubberColumnId)
{
    var columnEl = $(columnId);
    var rubberColumnEl = $(rubberColumnId);
    if (columnEl.hasClass('collapsed-column')) {
        $(el).removeClass('glyphicon-circle-arrow-right');
        $(el).addClass('glyphicon-circle-arrow-left');
        expandColumn(columnEl);
        collapseColumn(rubberColumnEl);
    } else {
        $(el).removeClass('glyphicon-circle-arrow-left');
        $(el).addClass('glyphicon-circle-arrow-right');
        collapseColumn(columnEl);
        expandColumn(rubberColumnEl);
        collapseAllItems(columnEl);
    }
}
