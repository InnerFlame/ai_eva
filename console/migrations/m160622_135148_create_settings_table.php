<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_table`.
 */
class m160622_135148_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_table', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'control_profile' => $this->string()->notNull(),
            'target_profile' => $this->string()->notNull(),
            'active' => $this->boolean(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_table');
    }
}
