<?php

use yii\db\Migration;

/**
 * Handles the creation for table `jumps`.
 */
class m170125_145305_create_jumps_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('jumps', [
            'id'            => $this->primaryKey(),
            'idFrom'        => $this->string(32),
            'jumpsCount'    => $this->integer(),
            'createdAt'     => "timestamp DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->createIndex('idFrom', 'jumps', 'idFrom');
        $this->createIndex('createdAt', 'jumps', 'createdAt');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('jumps');
    }
}
