<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class TrafficType extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_traffic_types_table';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
