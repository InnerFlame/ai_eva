<?php
use common\widgets\BindableFormItems\BindableFormItems;

$patternsItems = array();

$variableNames = array();
foreach ($model->availableVariables as $variable) {
    $variableNames[$variable->getVariableId()] = $variable->getName();
}
$patternsVariables = $model->getPatternsVariables();

foreach ($model->rule->getPatterns() as $pattern) {
    $currentVariableId = $patternsVariables[$pattern->getPatternId()]['variable_id'] ?? -1;
    $currentVariableName = $variableNames[$currentVariableId] ?? 'No variables';
    $currentRegexpId = $patternsVariables[$pattern->getPatternId()]['index_content'] ?? -1;
    $currentVariableRegexpId = $patternsVariables[$pattern->getPatternId()]['pattern_var_id'] ?? -1;

    $item['images'] = [
        [
            'class' => 'glyphicon glyphicon-signal' . ($pattern->isActive() ? ' active' : ''),
            'name' => 'activePatternIcon',
        ],
    ];
    $item['text'] = $pattern->getText();
    $item['hiddens'] = [
        ['name' => 'testText', 'value' => $pattern->getTest()],
        ['name' => 'patternId', 'value' => $pattern->getPatternId()],
        ['name' => 'currentVariableId', 'value' => $currentVariableId],
        ['name' => 'currentVariableName', 'value' => $currentVariableName],
        ['name' => 'currentRegexpId', 'value' => $currentRegexpId],
        ['name' => 'currentVariableRegexpId', 'value' => $currentVariableRegexpId],
    ];
    $patternsItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'patternsBindableForm',
    'items' => $patternsItems,
    'checked' => true,
]);
