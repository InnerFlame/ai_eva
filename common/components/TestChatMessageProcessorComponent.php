<?php
namespace common\components;

class TestChatMessageProcessorComponent extends AbstractMessageProcessorComponent
{
    protected function getControlProfileType($profile)
    {
        return $profile['control_profile_type'];
    }

    protected function getPaidStatus($profile)
    {
        return $profile['paid'];
    }

    protected function getScenarioId($sender, $recipient)
    {
        $scenarioId = \Yii::$app->testChatScenarios->getScenarioId([
            'idFrom'                => $sender['id'],
            'idFromMulti'           => $sender['multi_profile_id'],
            'idTo'                  => $recipient['id'],
            'controlProfileType'    => $this->getControlProfileType($recipient),
            'country'               => $sender['country'],
            'language'              => $sender['language'],
            'paidStatus'            => $this->getPaidStatus($sender),
            'site'                  => $sender['site'],
            'platform'              => $sender['platform'],
            'trafficSource'         => $sender['traff_src'],
            'actionWay'             => $sender['action_way'],
            'isFanClubAllowed'      => $recipient['isFanClubAllowed'] ?? null,
            'isOnline'              => $recipient['isModelOnline'] ?? null,

            'setting_id'            => $recipient['setting_id'],
        ]);

        return $scenarioId;
    }

    protected function getEvaAnswer($data)
    {
        return \Yii::$app->BrungildaApiCurl->chat(
            $data['from'],
            $data['to'],
            $data['text'],
            $data['scenario_id']
        ) + ['scenario_id' => $data['scenario_id']];
    }

    protected function saveIncomingMessageToLog($sender, $recipient, $text, $scenarioId)
    {
        // Don`t need to save incoming message to log
    }

    protected function saveOutgoingMessageToLog($sender, $recipient, $text, $scenarioId, $firstUsedPlaceholderId, $evaResponse, $predispatched = false)
    {
        // Don`t need to save outgoing message to log
    }
}
