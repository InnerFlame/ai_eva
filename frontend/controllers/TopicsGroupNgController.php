<?php

namespace frontend\controllers;

use frontend\controllers\traits\ClassifiersTrait;
use frontend\models\topicsGroup\TopicsGroupNgForm;
use yii\base\InvalidParamException;
use yii\base\Module;
use yii\web\Response;

/**
 * Class TopicsGroupNgController
 * @package frontend\controllers
 */
class TopicsGroupNgController extends BaseNgController
{
    use ClassifiersTrait;

    /* @var $requestData array */
    protected $requestData;

    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new TopicsGroupNgForm());
    }

    public function beforeAction($action): bool
    {
        if ($action->id == self::BOOTSTRAP_ACTION) {
            return parent::beforeAction($action);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (\Yii::$app->request->isPost) {
            $this->setRequestData();

            if (empty($this->requestData)) {
                throw new InvalidParamException();
            }
        }

        return parent::beforeAction($action);
    }

    public function actionIndex(): string
    {
        return $this->render('index');
    }

    public function actionGetTopicsGroup(): array
    {
        return $this->getFormModel()->getTopicsGroup();
    }

    public function actionCreateTopicsGroup(): array
    {
        return $this->getFormModel()->createTopicsGroup($this->requestData);
    }

    public function actionUpdateTopicsGroup(): array
    {
        return $this->getFormModel()->updateTopicsGroup($this->requestData);
    }

    public function actionTrainTopicsGroup(): array
    {
        return $this->getFormModel()->trainTopicsGroup($this->requestData);
    }

    public function actionGetClassifiers($type = TopicsGroupNgForm::TYPE_RULE_ML_CLASSIFIER): array
    {
        return [
            'classifiers'     => $this->getFormModel()->getClassifiers($type),
            'availableModels' => $this->getClassifiersAvailableModels()
        ];
    }

    protected function getFormModel(): TopicsGroupNgForm
    {
        return $this->formModel;
    }
}
