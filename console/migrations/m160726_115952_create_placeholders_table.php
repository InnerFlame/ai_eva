<?php

use yii\db\Migration;

/**
 * Handles the creation for table `placeholders_table`.
 */
class m160726_115952_create_placeholders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('placeholders_table', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'url_type' => $this->string()->notNull(),
            'active' => $this->boolean(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('placeholders_table');
    }
}
