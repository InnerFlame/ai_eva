<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\phrases\GroupPhrases;
use frontend\models\phrases\PhrasesForm;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class PhrasesController extends BaseController
{
    protected $model;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new PhrasesForm();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('scenarios')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        $request    = \Yii::$app->request;
        $get        = $request->get();

        $this->model->currentGroupGroupPhrasesId    = $get['groupGroupPhrasesId'] ?? NULL;
        $this->model->currentGroupPhrasesId         = $get['groupPhrasesId']      ?? NULL;
        $this->model->currentPhrasesId              = $get['id']                  ?? NULL;

        return $this->render('index', ['model' => $this->model]);
    }

    public function actionGroupPhrases()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getGroupsPhrases();
                $response = \Yii::$app->response;
                $response->format = \yii\web\Response::FORMAT_JSON;
                $response->data = [
                    'groupPhrases' => $this->renderAjax('_groupPhrases', ['model' => $this->model]),
                    'groupGroupsPhrases' => $this->renderAjax('_groupGroupsPhrases', ['model' => $this->model])
                ];
                return $response;
                //return $this->renderAjax('_groupPhrases', ['model' => $this->model]);
            }
        );
    }

    public function actionReplaceStages()
    {
        $model = new PhrasesForm();
        $model->getGroupsPhrases();

        $formData = \Yii::$app->request->post('StagesFindReplaceForm');

        /** @var GroupPhrases $groupsPhrase */
        foreach ($model->groupsPhrases as $groupsPhrase) {
            if (strpos($groupsPhrase->getText(), $formData['find']) !== false) {
                $groupsPhrase->setText(str_replace($formData['find'], $formData['replace'], $groupsPhrase->getText()));
                $model->updateGroupPhrases(
                    $groupsPhrase->getId(),
                    $groupsPhrase->getText(),
                    $groupsPhrase->getLanguageTag(),
                    $groupsPhrase->getTags(),
                    [$groupsPhrase->getId()]
                );
            }
        }

        return $this->render('index', ['model' => $this->model]);
    }

    public function actionCreateGroupGroupsPhrases()
    {
        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->createGroupGroupsPhrases($post['name']);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionUpdateGroupGroupsPhrases()
    {
        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->updateGroupGroupsPhrases($post['id'], $post['name']);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionDeleteGroupGroupsPhrases()
    {
        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->deleteGroupGroupsPhrases($post['id']);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionCopyGroupGroupsPhrases()
    {
        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->copyGroupGroupsPhrases($post['id']);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionPhrases()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getPhrases((int)$post['groupPhrasesId']);
                return $this->renderAjax('_phrases', ['model' => $this->model]);
            }
        );
    }

    public function actionCreateMultiGroupPhrases()
    {
        $request = \Yii::$app->request->post();
        $stages = explode("\n", $request['text']);
        foreach ($stages as $stage) {
            $request['text'] = $stage;
            $this->createGroupPhrases($request);
        }
    }

    public function actionUpdateGroupPhrases()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $groupPhrasesIds = json_decode($post['groupPhrasesIds'], true);
                return $this->model->updateGroupPhrases((int)$post['groupPhrasesId'], $post['text'], $post['lang'],
                    json_decode($post['tags']), $groupPhrasesIds);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateGroupPhrasesExcluded()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $excluded = 0;
                if ($post['excluded'] == "true") {
                    $excluded = 1;
                }
                return $this->model->updateGroupPhrasesExcluded((int)$post['groupPhrasesId'], $excluded);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateGroupPhrasesHasPlaceholder()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $hasPlaceholder = 0;
                if ($post['hasPlaceholder'] == "true") {
                    $hasPlaceholder = 1;
                }
                return $this->model->updateGroupPhrasesHasPlaceholder((int)$post['groupPhrasesId'], $hasPlaceholder);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateGroupPhrasesHasPrePlaceholder()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $hasPrePlaceholder = 0;
                if ($post['hasPrePlaceholder'] == "true") {
                    $hasPrePlaceholder = 1;
                }
                return $this->model->updateGroupPhrasesHasPrePlaceholder((int)$post['groupPhrasesId'], $hasPrePlaceholder);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeleteGroupPhrases()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                $response = $this->model->deleteGroupPhrases($params);
                foreach ($params as $val) {
                    $query = 'DELETE FROM group_group_phrases_group_phrases_table ';
                    $query .= 'WHERE group_phrases_id=:group_phrases_id';
                    $deleteGroupPhrases = \Yii::$app->db->createCommand($query);
                    $deleteGroupPhrases->bindValue(':group_phrases_id', (int)$val['group_phrase_id'])->execute();
                }
                return $response;
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionCreatePhrase()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->createPhrase((int)$post['groupPhrasesId'], $post['text']);
            }
        );
    }

    public function actionCreatePhrases()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $phrases = json_decode($post['phrases'], true);
                return $this->model->createPhrases((int)$post['groupPhrasesId'], $phrases);
            }
        );
    }

    public function actionUpdatePhrase()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->updatePhrase((int)$post['phraseId'], $post['text']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdatePhrases()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->updatePhrases($params);
            }
        );
    }

    public function actionDeletePhrases()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->deletePhrases($params);
            }
        );
    }

    private function createGroupPhrases(array $request)
    {
        $response = $this->model->createGroupPhrases($request['text']);
        if(count($response) > 1) {
            foreach ($response as $responseItem) {
                $this->insertGroupPhrases(
                    $responseItem[0]['item']['item']['group_phrase_id'],
                    $request['groupGroupsPhrasesId']
                );
            }
            return $response[0];
        }
        $this->insertGroupPhrases(
            $response[0]['item']['item']['group_phrase_id'],
            $request['groupGroupsPhrasesId']
        );
    }

    private function insertGroupPhrases($groupPhrasesId, $groupGroupsPhrasesId)
    {
        $query = 'INSERT INTO group_group_phrases_group_phrases_table(group_group_phrases_id, group_phrases_id) ';
        $query .= 'VALUES (:group_group_phrases_id, :group_phrases_id)';
        $insertGroupPhrases = \Yii::$app->db->createCommand($query);
        $insertGroupPhrases->bindValue(':group_group_phrases_id', $groupGroupsPhrasesId)
            ->bindValue(':group_phrases_id', $groupPhrasesId)->execute();
    }
}
