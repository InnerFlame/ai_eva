//Current group of groups phrases id
var currentGroupId = -1;

//Set height of scenarios divs
function setDivsHeight()
{
    //header height + footer height = 100px
    $('#scenarioPhrasesClassifiersContainer').css({
        height: ($(window).height() - 110) + 'px'
    });
    $('#scenarioBlocksContainer').css({
        height: ($(window).height() - 100) + 'px'
    });
    $('#scenarioDiagram').css({
        height: ($(window).height() - 100) + 'px'
    });
}

//Load list of scenarios
function updateScenarios()
{
    $.ajax({
        url: '/scenarios/scenarios',
        success: function (data) {
            //Update topics
            $('#scenarios').html(data);
            //Trigger click on first or current scenario item
            if ($('#currentScenarioId').val() != '') {
                var str = 'input[name="scenarioId"][value="' + $('#currentScenarioId').val() + '"]';
                $('#scenarios li.list-group-item.sortable').has(str).click();
            } else {
                $('#scenarios ul li.list-group-item.sortable:first').click();
            }

            if ($('#scenarioType').val() == 'block') {
                $('input[name="scenarioBlockRadio"]').click();
            } else {
                changeRadioScenarioBlock('radioScenarios');
            }
        }
    });
}

//Convert checkboxes values to array
function checkboxesToArray(obj)
{
    var objects = $(obj).find('ul li a input[type="checkbox"]:checked').closest('li').find('input[type="hidden"]');
    var arr = [];
    for (var i = 0; i < objects.size(); i++) {
        arr.push($(objects[i]).val());
    }
    return arr;
}

//Load list of groups
function updateGroups()
{
    $.ajax({
        url: '/scenarios/groups',
        success: function (data) {
            //Update groups
            $('#groups').html(data);
            //Trigger click on first group item
            var firstGroupsItem = $('#groups li.list-group-item.sortable:first-child');
            if (firstGroupsItem.length != 0) {
                firstGroupsItem.click();
            }
        }
    });
}

$(window).resize(function () {
    //Set height of scenarios divs
    setDivsHeight();
});

//Filter phrases by group and live-search
function filterPhrases()
{
    var text = $('#filterPalettePhrases').val();
    filteredPhrasesArray = [];

    //Filter by text
    if (text == '') {
        filteredPhrasesArray = phrasesArray;
    } else {
        for (i = 0; i < phrasesArray.length; i++) {
            if ((phrasesArray[i].key.toLowerCase()).indexOf(text.toLowerCase()) != -1) {
                filteredPhrasesArray.push(phrasesArray[i]);
            }
        }
    }

    //Filter by group
    if (currentGroupId != -1) {
        var tempArray = filteredPhrasesArray;
        filteredPhrasesArray = [];
        for (i = 0; i < tempArray.length; i++) {
            if (tempArray[i].groupGroupsPhrasesId == currentGroupId ||
                tempArray[i].category == "start" ||
                tempArray[i].category == "stop") {
                filteredPhrasesArray.push(tempArray[i]);
            }
        }
    }

    palettePhrases.animationManager.isEnabled = false;
    palettePhrases.model = new go.GraphLinksModel(filteredPhrasesArray);
}

//Get array of checked items ids
function getCheckedItemsIds(parentId, itemIdName)
{
    var checkedItemsIds = [];
    $(parentId + ' li.list-group-item:visible input[name="itemCheckbox"]:checked').each(function () {
        var id = $(this).closest('li.list-group-item').find('input[name="' + itemIdName + '"]').val();
        checkedItemsIds.push(id);
    });
    return checkedItemsIds;
}

//Get array of checked or current items ids
function getCheckedOrCurrentItemsIds(parentId, itemIdName, bindableFormId)
{
    //Get checked items
    var checkedOrCurrentItemsIds = getCheckedItemsIds(parentId, itemIdName);
    //If there are no checked items get current item
    if (checkedOrCurrentItemsIds.length == 0) {
        var currentItemId = $(bindableFormId + ' input[name="' + itemIdName + '"]').val();
        if (currentItemId != 0) {
            checkedOrCurrentItemsIds.push(currentItemId);
        }
    }
    return checkedOrCurrentItemsIds
}

//Get string of checked or current items names (for modal window)
function getCheckedOrCurrentItemsNames(parentId, itemIdName, bindableFormId)
{
    var itemsNames = '';
    var itemsIds = getCheckedOrCurrentItemsIds(parentId, itemIdName, bindableFormId);
    $.each(itemsIds, function (index, value) {
        var itemsName = $(parentId + ' ul li:visible input[name="' + itemIdName + '"][value="' + value + '"]').siblings('span[name="itemText"]').html();
        itemsNames += (index + 1) + '. ' + itemsName + '<br/>'
    });
    return itemsNames;
}

$(document).ready(function () {
    //Load list of scenarios
    updateScenarios();

    //Load list of groups
    updateGroups();

    //Set height of scenarios divs
    setDivsHeight();

    //Click on scenarios item event
    $(document).on('click', '#scenarios li.list-group-item.sortable', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        $('#currentScenarioId').val($(this).find('input[name="scenarioId"]').val());

        // Set checked tags of current scenario
        if ($(this).find('input[name="isBlock"]').val() == 0){
            var jsonTags = $(this).find('input[name="scenarioTags"]').val();
            var tags = JSON.parse(jsonTags);
            $('div[name="scenarioTagsButton"] li').each(function(index) {
                var currentTagId = $(this).find('input[type="hidden"]').val();
                if ($.inArray(currentTagId, tags) !== -1) {
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                } else {
                    $(this).find('input[type="checkbox"]').prop('checked', false);
                }
            });
        }

        load();//This function situated in scenariosDiagram.js
    });

    //Click on groups item event
    $(document).on('click', '#groups li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        currentGroupId = $(this).children('input[name="groupId"]').val();
        filterPhrases();
    });

    //Disable 'Add' button when scenario name is empty
    $(document).on('input', '#addNewScenarioName', function () {
        if ($(this).val() === '') {
            $('#addNewScenarioButton').attr('disabled', true);
        } else {
            $('#addNewScenarioButton').attr('disabled', false);
        }
    });

    //Disable 'Add' button when block name is empty
    $(document).on('input', '#addNewBlockName', function () {
        if ($(this).val() === '') {
            $('#addNewBlockButton').attr('disabled', true);
        } else {
            $('#addNewBlockButton').attr('disabled', false);
        }
    });

    //Click 'Add new scenario' button
    $(document).on('click', '#addNewScenarioButton', function () {
        var scenarioName = $('#addNewScenarioName').val();
        scenarioName = encodeURIComponent(scenarioName);
        $.ajax({
            url: '/scenarios/create-scenario',
            type: "POST",
            data: 'name=' + scenarioName,
            success: function (data) {
                if (data == 'true') {
                    updateScenarios();
                } else {
                    alert('An error occurred while creating the scenario!');
                }
                $('#addNewScenarioModal').modal('hide');
            }
        });
    });

    //Click 'Add new block' button
    $(document).on('click', '#addNewBlockButton', function () {
        var blockName = $('#addNewBlockName').val();
        blockName = encodeURIComponent(blockName);
        $.ajax({
            url: '/scenarios/create-block',
            type: "POST",
            data: 'name=' + blockName,
            success: function (data) {
                if (data == 'true') {
                    updateScenarios();
                } else {
                    alert('An error occurred while creating the block!');
                }
                $('#addNewBlockModal').modal('hide');
            }
        });
    });

    //Click scenario delete button
    $(document).on('click', '#deleteScenarioButton', function () {
        if (getCheckedOrCurrentItemsIds('#scenarios', 'scenarioId', '#scenarioBindableForm').length == 0) {
            return;
        }
        var scenariosNames = getCheckedOrCurrentItemsNames('#scenarios', 'scenarioId', '#scenarioBindableForm');
        $('#deleteScenariosNames').html(scenariosNames);
        $('#confirmDeleteScenariosModal').modal('show');
    });

    //Click confirm scenario delete button
    $(document).on('click', '#confirmDeleteScenariosButton', function () {
        var scenariosIds = getCheckedOrCurrentItemsIds('#scenarios', 'scenarioId', '#scenarioBindableForm');
        if (scenariosIds.length == 0) {
            return;
        }
        var params = [];
        $.each(scenariosIds, function (index, value) {
            var elem = {};
            elem.scenario_id = parseInt(value);
            params.push(elem);
        });

        $.ajax({
            url: '/scenarios/delete-scenarios',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updateScenarios();
                } else {
                    alert('An error occurred while deleting the scenarios!');
                }
                $('#confirmDeleteScenariosModal').modal('hide');
            }
        });
    });

    //Click 'Update scenario' button
    $(document).on('click', '#updateScenarioButton', function () {
        var scenarioId = $('#scenarioBindableForm input[name="scenarioId"]').val();
        if (scenarioId.length == 0) {
            return;
        }
        var scenarioName = $('#scenarioText').val();
        var scenarioMaxStepCount = $('#scenarioMaxStepCount').val();
        scenarioName = encodeURIComponent(scenarioName);
        var nodes = $('#scenarioBindableForm input[name="scenarioNodes"]').val();
        var flowChart = myDiagram.model.toJson();//This variable situated in scenariosDiagram.js

        var parent = $(this).closest('#scenarioBindableForm');
        var checkedTags = checkboxesToArray(parent.find('div[name="scenarioTagsButton"]'));

        var scenarioType = $('#scenarioBindableForm #scenarioType').val();

        $.ajax({
            url: '/scenarios/update-scenario',
            type: "POST",
            data: 'scenarioId=' + scenarioId + '&name=' + scenarioName + '&nodes=' + nodes + '&flowChart=' + flowChart +
                '&maxStepCount=' + scenarioMaxStepCount + '&scenarioType=' + scenarioType +
                '&checkedTags=' + JSON.stringify(checkedTags),
            success: function (data) {
                if (data == 'true') {
                    updateScenarios();
                } else {
                    alert('An error occurred while updating the scenario!');
                }
            }
        });
    });

    //Click scenario copy button
    $(document).on('click', '#copyScenarioButton', function () {
        var scenarioId = $('#scenarioBindableForm input[name="scenarioId"]').val();
        if (scenarioId.length == 0) {
            return;
        }
        $('#confirmCopyScenarioModal').modal('show');
    });

    //Disable 'Copy' button when scenario name is empty
    $(document).on('input', '#copyScenarioName', function () {
        if ($(this).val() === '') {
            $('#confirmCopyScenarioButton').attr('disabled', true);
        } else {
            $('#confirmCopyScenarioButton').attr('disabled', false);
        }
    });

    //Click confirm scenario copy button
    $(document).on('click', '#confirmCopyScenarioButton', function () {
        var nodes = $('#scenarioBindableForm input[name="scenarioNodes"]').val();
        var flowChart = myDiagram.model.toJson();//This variable situated in scenariosDiagram.js
        var scenarioName = $('#copyScenarioName').val();
        var scenarioMaxStepCount = $('#scenarioBindableForm #scenarioMaxStepCount').val();
        scenarioName = encodeURIComponent(scenarioName);
        var scenarioType = $('#scenarioBindableForm #scenarioType').val();
        $.ajax({
            url: '/scenarios/copy-scenario',
            type: "POST",
            data: 'name=' + scenarioName + '&nodes=' + nodes + '&flowChart=' + flowChart +
                '&maxStepCount=' + scenarioMaxStepCount + '&scenarioType=' + scenarioType,
            success: function (data) {
                if (data == 'true') {
                    updateScenarios();
                } else {
                    alert('An error occurred while copying the scenario!');
                }
                $('#confirmCopyScenarioModal').modal('hide');
            }
        });
    });

    $(document).on('keyup', '#filterPalettePhrases', function () {
        filterPhrases();
    });

    //Scenarios live search functionality
    $(document).on('keyup', 'input.scenariosLivesearch', function () {
        var pattern = $(this).val().toLowerCase();

        $('#scenarios li.sortable').each(function (i) {
            if ($('input[name="scenarioBlockRadio"]:checked').val() == 'radioScenarios') {
                if($(this).find('input[name="isBlock"]').val() == 1) {
                    $(this).hide();
                    return true;
                }
            } else if ($('input[name="scenarioBlockRadio"]:checked').val() == 'radioBlocks') {
                if($(this).find('input[name="isBlock"]').val() == 0) {
                    $(this).hide();
                    return true;
                }
            }

            var text = $(this).find('span[name="itemText"]').html().toLowerCase();
            if (text.indexOf(pattern) == -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });

    //Groups live search functionality
    $(document).on('keyup', 'input.groupsLivesearch', function () {
        var pattern = $(this).val().toLowerCase();
        $('#groups li.sortable').each(function (i) {
            var text = $(this).find('span[name="itemText"]').html().toLowerCase();
            if (text.indexOf(pattern) == -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });

    $(document).on('change', 'input[name="scenarioBlockRadio"]', function () {
        changeRadioScenarioBlock($(this).val());
    });
});

function changeRadioScenarioBlock(val)
{
    if (val == 'radioScenarios') {
        //Scenarios
        $('#scenarios ul li.scenarioItem').show();
        $('#scenarios ul li.blockItem').hide();
        $('#scenarios').find('input[name="isBlock"][value="0"]').closest('li').show();
        $('#scenarios').find('input[name="isBlock"][value="1"]').closest('li').hide();
        $('#scenarioBindableForm .scenarioItem').show();
        $('#scenarioBindableForm .blockItem').hide();
        $('#scenarioBindableForm #scenarioType').val('scenario');
    } else if (val == 'radioBlocks') {
        //Blocks
        $('#scenarios ul li.scenarioItem').hide();
        $('#scenarios ul li.blockItem').show();
        $('#scenarios').find('input[name="isBlock"][value="0"]').closest('li').hide();
        $('#scenarios').find('input[name="isBlock"][value="1"]').closest('li').show();
        $('#scenarioBindableForm .scenarioItem').hide();
        $('#scenarioBindableForm .blockItem').show();
        $('#scenarioBindableForm #scenarioType').val('block');
    }
}
