<?php
declare(strict_types=1);

namespace frontend\models\topics;

class Rule
{
    private $ruleId;
    private $topicId;
    private $excludeTemplate;
    private $execLimit;
    private $priority;
    private $enabled;
    private $active;
    private $deleted;
    private $name;
    private $version;
    private $parentId;
    private $linkId;
    private $phraseId;
    private $typePhraseId;

    private $patterns;
    private $templates;
    private $exception_patterns;
    private $rules;

    /**
     * Rule constructor.
     * @param $ruleId
     * @param $topicId
     * @param $excludeTemplate
     * @param $execLimit
     * @param $priority
     * @param $enabled
     * @param $active
     * @param $deleted
     * @param $name
     * @param $version
     * @param $parentId
     * @param $linkId
     * @param $phraseId
     * @param $typePhraseId
     * @param $patterns
     * @param $templates
     * @param $exception_patterns
     * @param $rules
     */
    public function __construct(
        int $ruleId,
        $topicId,
        $excludeTemplate,
        $execLimit,
        int $priority,
        bool $enabled,
        bool $active,
        bool $deleted,
        string $name,
        int $version,
        int $parentId,
        int $linkId,
        int $phraseId,
        int $typePhraseId,
        array $patterns = array(),
        array $templates = array(),
        array $exception_patterns = array(),
        array $rules = array()
    ) {
        $this->ruleId = $ruleId;
        $this->topicId = $topicId;
        $this->excludeTemplate = $excludeTemplate;
        $this->execLimit = $execLimit;
        $this->priority = $priority;
        $this->enabled = $enabled;
        $this->active = $active;
        $this->deleted = $deleted;
        $this->name = $name;
        $this->version = $version;
        $this->parentId = $parentId;
        $this->linkId = $linkId;
        $this->phraseId = $phraseId;
        $this->typePhraseId = $typePhraseId;
        $this->patterns = $patterns;
        $this->templates = $templates;
        $this->exception_patterns = $exception_patterns;
        $this->rules = $rules;
    }

    /**
     * @return int
     */
    public function getRuleId() : int
    {
        return $this->ruleId;
    }

    /**
     * @param int $ruleId
     */
    public function setRuleId(int $ruleId)
    {
        $this->ruleId = $ruleId;
    }

    /**
     * @return int
     */
    public function getTopicId()
    {
        return $this->topicId;
    }

    /**
     * @param int $topicId
     */
    public function setTopicId($topicId)
    {
        $this->topicId = $topicId;
    }

    /**
     * @return mixed
     */
    public function getExcludeTemplate()
    {
        return $this->excludeTemplate;
    }

    /**
     * @param mixed $excludeTemplate
     */
    public function setExcludeTemplate($excludeTemplate)
    {
        $this->excludeTemplate = $excludeTemplate;
    }

    /**
     * @return mixed
     */
    public function getExecLimit()
    {
        return $this->execLimit;
    }

    /**
     * @param mixed $execLimit
     */
    public function setExecLimit($execLimit)
    {
        $this->execLimit = $execLimit;
    }

    /**
     * @return int
     */
    public function getPriority() : int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return bool
     */
    public function isEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getParentId() : int
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId(int $parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getLinkId() : int
    {
        return $this->linkId;
    }

    /**
     * @param int $linkId
     */
    public function setLinkId(int $linkId)
    {
        $this->linkId = $linkId;
    }

    /**
     * @return int
     */
    public function getPhraseId() : int
    {
        return $this->phraseId;
    }

    /**
     * @param int $phraseId
     */
    public function setPhraseId(int $phraseId)
    {
        $this->phraseId = $phraseId;
    }

    /**
     * @return int
     */
    public function getTypePhraseId() : int
    {
        return $this->typePhraseId;
    }

    /**
     * @param int $typePhraseId
     */
    public function setTypePhraseId(int $typePhraseId)
    {
        $this->typePhraseId = $typePhraseId;
    }

    /**
     * @return array
     */
    public function getPatterns() : array
    {
        return $this->patterns;
    }

    /**
     * @param array $patterns
     */
    public function setPatterns(array $patterns)
    {
        $this->patterns = $patterns;
    }

    /**
     * @return array
     */
    public function getTemplates() : array
    {
        return $this->templates;
    }

    /**
     * @param array $templates
     */
    public function setTemplates(array $templates)
    {
        $this->templates = $templates;
    }

    /**
     * @return array
     */
    public function getExceptionPatterns() : array
    {
        return $this->exception_patterns;
    }

    /**
     * @param array $exception_patterns
     */
    public function setExceptionPatterns(array $exception_patterns)
    {
        $this->exception_patterns = $exception_patterns;
    }

    /**
     * @return array
     */
    public function getRules() : array
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     */
    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }
}
