<?php

use yii\db\Migration;

class m170210_150939_create_settings_locales_table extends Migration
{
    public function up()
    {
        $this->createTable('settings_locales', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'locale_name' => $this->string(3)->defaultValue('DEF'),
        ]);
    }

    public function down()
    {
        $this->dropTable('settings_locales');
    }
}
