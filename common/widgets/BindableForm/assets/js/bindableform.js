function clearBindableForm(bindableFormId)
{
    $(bindableFormId).find('[name]').each(function () {
        $(this).val('');
    });
    $(bindableFormId).find('.radio-button').each(function () {
        if ($(this).hasClass('active')) {
            $(this).click();
        }
    });
}
