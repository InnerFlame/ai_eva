<?php
//This array is available by \Yii::$app->params. For example - \Yii::$app->params['placeholdersTypes']
return [
    'placeholdersTypes' => [
        'external',
        'internal',
    ],
    'placeholdersUrlsTypes' => [
        'static',
        'dynamic',
    ],
    'placeholdersSendersTypes' => [
        'Cams',
        'External',
        'Scam 2.0',
        'Labs',
        'Euromodels',
        '',
    ],
    'placeholderDispatchTypes' => [
        0 => 'Standart',
        1 => 'Predispatched',
    ],
    'placeholderIsLink' => [
        0 => 'no',
        1 => 'yes',
    ],
];
