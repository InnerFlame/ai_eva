<?php

namespace frontend\models\substitutes;

use frontend\models\BaseNgForm;

/**
 * Class SubstitutesForm
 * @package frontend\models\substitutes
 */
class SubstitutesForm extends BaseNgForm
{
    /**
     * @return array
     */
    public function getSubstitutes(): array
    {
        return $this->getBrungildaApi()->getSubstitutes();
    }

    /**
     * @param int $substituteId
     * @return array
     */
    public function getSubstitutePhrasesById(int $substituteId): array
    {
        return $this->getBrungildaApi()->getSubstitutePhrases($substituteId);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createSubstitute(array $requestData): array
    {
        return $this->getBrungildaApi()->createSubstitute($requestData);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createSubstitutePhrase(array $requestData): array
    {
        return $this->getBrungildaApi()->createSubstitutePhrase($requestData);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updateSubstitute(array $requestData): array
    {
        return $this->getBrungildaApi()->updateSubstitute($requestData);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updateSubstitutePhrase(array $requestData): array
    {
        return $this->getBrungildaApi()->updateSubstitutePhrase($requestData);
    }


    /**
     * @param array $requestData
     * @return array
     */
    public function deleteSubstitute(array $requestData): array
    {
        return $this->getBrungildaApi()->deleteSubstitute($requestData);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteSubstitutePhrase(array $requestData): array
    {
        return $this->getBrungildaApi()->deleteSubstitutePhrase($requestData);
    }
}
