<?php

namespace frontend\models\topics;

use frontend\models\BaseNgForm;

/**
 * Class TopicsNgForm
 * @package frontend\models\topics
 */
class TopicsNgForm extends BaseNgForm
{
    /**
     * @return array
     */
    public function getTopics(): array
    {
        $topics          = $this->getTopicsWithRestrictions();
        $topicsTags      = TopicTag::find()->asArray()->all();
        $topicExclusions = TopicExclusionInitial::find()->asArray()->all();
        
        foreach ($topics as $key => $topic) 
        {
            foreach ($topicsTags as $topicTag) {
                if ($topic['topic_id'] == $topicTag['topicId']) {
                    $topics[$key]['tags'][] = (int) $topicTag['tagId'];
                }
            }
            
            foreach ($topicExclusions as $topicExclusion) {
                if ($topic['topic_id'] == $topicExclusion['topicId']) {
                    $topics[$key]['isExclude']       = $topicExclusion['isExclude'];
                    $topics[$key]['countTriggering'] = $topicExclusion['countTriggering'];
                    break;
                }
            }
        }

        return $topics;
    }

    /**
     * @param int $topicId
     * @return array
     */
    public function getTopicRulesByTopicId(int $topicId): array
    {
        if (!$topicId) {
            return [];
        }

        $rules      = [];
        $rulesArray = $this->getBrungildaApi()->getRulesByTopicId($topicId);

        foreach ($rulesArray as $rule)
        {
            if ($rule['enabled'] == null) {
                $rule['enabled'] = false;
            }

            $rule['parent_id']      = $rule['parent_id']      == null ? 0 : $rule['parent_id'];
            $rule['link_id']        = $rule['link_id']        == null ? 0 : $rule['link_id'];
            $rule['phrase_id']      = $rule['phrase_id']      == null ? 0 : $rule['phrase_id'];
            $rule['type_phrase_id'] = $rule['type_phrase_id'] == null ? 0 : $rule['type_phrase_id'];

            $rules[] = $rule;
        }

        usort($rules, [$this, "comparePriorityForSort"]);
        return $rules;
    }

    /**
     * @return array
     */
    public function getTopicsAvailableDataForSelects(): array
    {
        $tags          = [];
        $availableTags = \Yii::$app->params['tags'];

        foreach ($availableTags as $id => $name) {
            $tags[] = [
                'id'   => $id,
                'name' => $name
            ];
        }

        return [
            'scenarios'  => $this->getScenariosForHolder(),
            'variables'  => $this->filterDataForMultiSelectsWithStrictKeys('variable_id', 'name', $this->getBrungildaApi()->getVariables()),
            'timeSlots'  => $this->getBrungildaApi()->getTimeslots(),
            'tags'       => $tags,
            'languages'  => $this->filterDataForMultiSelects(\Yii::$app->params['languageTags']),
        ];
    }

    
    public function createTopic(array $requestData): array
    {
        $params['name'] = $requestData['name'] ?? null;
        return $this->getBrungildaApi()->createTopics([$params]);
    }

    
    public function createTopicRule(array $requestData): array
    {
        $params['topic_id'] = $requestData['topic_id'] ?? 0;
        $params['name']     = $requestData['name'] ?? null;
        return $this->getBrungildaApi()->createRules([$params]);
    }

    
    public function updateTopic(array $requestData): array
    {
        $topicId = $requestData['topic_id'] ?? 0;
        $requestData['countTriggering'] = empty($requestData['countTriggering']) ? 0 : (int) $requestData['countTriggering'];

        if (empty($topicId)) {
            return [];
        }

        if (isset($requestData['tags'])) {
           $this->manageLocalTopicUpdateData($requestData, $topicId);
        }

        if (isset($requestData['restrictions']))
        {
            $allRestrictions = $this->getBrungildaApi()->getRestrictions();

            $currentTopicRestrictions = [];
            foreach ($allRestrictions as $restriction) {
                if (($restriction['tb_name'] == 'topics') && ($restriction['id'] == $topicId)) {
                    $currentTopicRestrictions[] = $restriction;
                }
            }

            $restrictionsToDelete = [];
            foreach ($currentTopicRestrictions as $currentTopicRestriction) {
                $requestScenarioIds = array_values($requestData['restrictions']);
                if(!in_array($currentTopicRestriction['scenario_id'], $requestScenarioIds)) {
                    $restrictionsToDelete[] = $currentTopicRestriction;
                }
            }
            if (!empty($restrictionsToDelete)) {
                $this->getBrungildaApi()->deleteRestrictions($restrictionsToDelete);
            }

            $restrictionsToCreate = [];
            foreach ($requestData['restrictions'] as $scenarioId) {
                $currentTopicRestrictionsScenarios = array_column($currentTopicRestrictions, 'scenario_id');
                if (!in_array($scenarioId, $currentTopicRestrictionsScenarios)) {
                    $restrictionsToCreate[] = [
                        'tb_name' => 'topics',
                        'id'=> $topicId,
                        'scenario_id' => (int)$scenarioId
                    ];
                }
            }
            if (!empty($restrictionsToCreate)) {
                $this->getBrungildaApi()->createRestrictions($restrictionsToCreate);
            }

            unset($requestData['restrictions']);
        }

        return $this->getBrungildaApi()->updateTopics([$requestData]);
    }

    /**
     * @param array $requestData
     * @param int $topicId
     */
    private function manageLocalTopicUpdateData(array &$requestData, int $topicId)
    {
        TopicTag::deleteAll('topicId = ' . $topicId);

        foreach ($requestData['tags'] as $tagId) {
            $topicTag = new TopicTag();
            $topicTag->topicId = $topicId;
            $topicTag->tagId = $tagId;
            $topicTag->save();
        }

        if ((isset($requestData['isExclude']) && $requestData['isExclude'] !== '') && $requestData['countTriggering'] !== '') {
            $topicExclusionInitial = TopicExclusionInitial::findOne(['topicId' => $topicId]);
            if (empty($topicExclusionInitial)) {
                $topicExclusionInitial          = new TopicExclusionInitial();
                $topicExclusionInitial->topicId = $topicId;
            }
            $topicExclusionInitial->isExclude       = $requestData['isExclude'] ?? '0';
            $topicExclusionInitial->countTriggering = $requestData['countTriggering'];
            $topicExclusionInitial->save();
        }

        unset($requestData['tags']);
        unset($requestData['isExclude']);
        unset($requestData['countTriggering']);
    }

    /**
     * @param int $topicId
     * @return array
     */
    public function deleteTopic(int $topicId): array
    {
        TopicTag::deleteAll('topicId = ' . $topicId);
        TopicExclusionInitial::deleteAll('topicId = ' . $topicId);

        $params['topic_id'] = $topicId;

        $restrictions = $this->getBrungildaApi()->getRestrictions();
        $restrictionsToDelete = [];
        foreach ($restrictions as $restriction) {
            if ($restriction['id'] == $topicId) {
                $restrictionsToDelete[] = $restriction;
            }
        }

        $this->getBrungildaApi()->deleteRestrictions($restrictionsToDelete);

        $response = $this->getBrungildaApi()->deleteTopics([$params]);

        return $response;
    }

    
    public function updateTopicOpt(array $requestData): array
    {
        return $this->getBrungildaApi()->updateTopics([$requestData]);
    }

    /**
     * @param $requestData
     * @return array
     */
    public function updateRule($requestData): array
    {
        return $this->getBrungildaApi()->updateRules([$requestData]);
    }

    
    public function updateRuleOpt(array $requestData): array
    {
        return $this->getBrungildaApi()->updateRules([$requestData]);
    }

    
    public function deleteRule(array $requestData): array
    {
        return $this->getBrungildaApi()->deleteRules([$requestData]);
    }

    /**
     * @param int $ruleId
     * @return array
     */
    public function getRuleData(int $ruleId): array
    {
        $data = $this->getBrungildaApi()->getRuleById($ruleId);
        $data['patternVariables'] = $this->getPatternsVariables();

        return $data;
    }

    /**
     * @param int $subRuleId
     * @return array
     */
    public function getSubRuleData(int $subRuleId)
    {
        return $this->getBrungildaApi()->getRuleById($subRuleId);
    }

    
    public function createPattern(array $requestData): array
    {
        unset($requestData['pattern_id']);
        $requestData['active'] = 0;
        return $this->getBrungildaApi()->createPatterns([$requestData]);
    }


    public function updatePattern(array $requestData): array
    {
        unset($requestData['variable_id']);
        return $this->getBrungildaApi()->updatePatterns([$requestData]);
    }


    public function updatePatterns(array $requestData): array
    {
        $updateData = [];
        $ids = array_unique($requestData['ids']);
        unset($requestData['ids']);
        unset($requestData['model']['variable_id']);

        foreach ($ids as $patternId) {
            $requestData['model']['pattern_id'] = (int) $patternId;
            $updateData[] = $requestData['model'];
        }

        return  $this->getBrungildaApi()->updatePatterns($updateData);
    }

    public function updatePatternsStatus(array $requestData): array
    {
        $updateData = [];
        $ids = array_unique($requestData['ids']);
        unset($requestData['ids']);
        unset($requestData['model']['variable_id']);

        foreach ($ids as $patternId) {
            $updateData[] = [
                'active' => (int) $requestData['model']['active'] ?? 0,
                'pattern_id' => (int) $patternId
            ];
        }

        return  $this->getBrungildaApi()->updatePatterns($updateData);
    }

    /**
     * @param int $patternId
     * @return array
     */
    public function deletePattern(int $patternId): array
    {
        return $this->getBrungildaApi()->deletePatterns([['pattern_id' => (int) $patternId]]);
    }

    /**
     * @param array $ids
     * @return array
     */
    public function deletePatterns(array $ids): array
    {
        $deleteData = [];

        foreach ($ids as $patternId) {
            $deleteData[] = ['pattern_id' => (int) $patternId];
        }

        return $this->getBrungildaApi()->deletePatterns($deleteData);
    }

    public function createTemplate(array $requestData): array
    {
        unset($requestData['template_id']);
        if (empty($requestData['timeslot_id'])) {
            unset($requestData['timeslot_id']);
        }

        $createData = [
            'rule_id' => (int) $requestData['rule_id'],
            'text' => $requestData['text'],
        ];
        return $this->getBrungildaApi()->createTemplates([$createData]);
    }

    public function updateTemplate(array $requestData): array
    {
        if ($requestData['timeslot_id'] == 'null') {
            $requestData['timeslot_id'] = null;
        }

        return $this->getBrungildaApi()->updateTemplates([$requestData]);
    }

    public function updateTemplates(array $requestData): array
    {
        $updateData = [];
        if ($requestData['model']['timeslot_id'] == 'null') {
            $requestData['model']['timeslot_id'] = null;
        }

        $ids = array_unique($requestData['ids']);

        foreach ($ids as $templateId) {

            $requestData['model']['template_id'] = (int) $templateId;
            $updateData[] = $requestData['model'];
        }

        return $this->getBrungildaApi()->updateTemplates($updateData);
    }

    /**
     * @param int $templateId
     * @return array
     */
    public function deleteTemplate(int $templateId): array
    {
        return $this->getBrungildaApi()->deleteTemplates([['template_id' => $templateId]]);
    }

    /**
     * @param array $ids
     * @return array
     */
    public function deleteTemplates(array $ids): array
    {
        $dataToDelete = [];
        foreach ($ids as $templateId) {
            $dataToDelete[] = ['template_id' => (int) $templateId];
        }

        return $this->getBrungildaApi()->deleteTemplates($dataToDelete);
    }


    public function createExcPattern(array $requestData): array
    {
        unset($requestData['exception_pattern_id']);
        $createData = [
            'rule_id' => (int) $requestData['rule_id'],
            'test'    => $requestData['test'],
            'text'    => $requestData['text']
        ];

        return $this->getBrungildaApi()->createExceptionPatterns([$createData]);
    }


    public function updateExcPattern(array $requestData): array
    {
        return $this->getBrungildaApi()->updateExceptionPatterns([$requestData]);
    }

    /**
     * @param int $patternId
     * @return array
     */
    public function deleteExcPattern(int $patternId): array
    {
        return $this->getBrungildaApi()->deleteExceptionPatterns([['exception_pattern_id' => $patternId]]);
    }


    public function updateExcPatterns(array $requestData): array
    {
        $updateData = [];
        $ids = array_unique($requestData['ids']);

        foreach ($ids as $patternId) {
            $requestData['model']['exception_pattern_id'] = (int) $patternId;
            $updateData[] = $requestData['model'];
        }

        return  $this->getBrungildaApi()->updateExceptionPatterns($updateData);
    }

    /**
     * @param array $ids
     * @return array
     */
    public function deleteExcPatterns(array $ids): array
    {
        $deleteData = [];

        foreach ($ids as $patternId) {
            $deleteData[] = ['exception_pattern_id' => (int) $patternId];
        }

        return $this->getBrungildaApi()->deleteExceptionPatterns($deleteData);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createSubRule(array $requestData): array
    {
        $requestData['name'] = str_replace(["\r\n", "\r", "\n", "\t"], '', $requestData['name']);
        $createData = [
            'parent_id' => (int) $requestData['parent_id'],
            'name' => $requestData['name'],
        ];

        return $this->getBrungildaApi()->createSubRule([$createData]);
    }

    public function updateTopicsPriority(array $newOrderData): array
    {
        $updateData = [];

        foreach ($newOrderData as $priority => $topicId) {
            $updateData[] = [
                'topic_id' => (int) $topicId,
                'priority' => (int) ($priority + 1)
            ];
        }

        if (empty($updateData)) {
            return [];
        }

        return $this->getBrungildaApi()->updateTopics($updateData);
    }

    public function updateRulesPriority(array $newOrderData): array
    {
        $updateData = [];

        foreach ($newOrderData as $priority => $ruleId) {
            $updateData[] = [
                'rule_id' => (int) $ruleId,
                'priority' => (int) ($priority + 1)
            ];
        }

        if (empty($updateData)) {
            return [];
        }

        return $this->getBrungildaApi()->updateRules($updateData);
    }

    public function copyRuleToTopic(array $copyData): array
    {
        $out = [];
        $topicId = (int) $copyData['topicId'] ?? 0;
        if (!$topicId) {
            return [];
        }

        $ruleToCopy = $copyData['model'];
        $ruleToCopy['topic_id'] = (int) $topicId;
        unset($ruleToCopy['rule_id']);

        $excPatterns = $copyData['excPatterns'];
        $patterns = $copyData['patterns'];
        $subRules = $copyData['subRules'];
        $templates = $copyData['templates'];

        $rule = $out['createdRule'][] = $this->getBrungildaApi()->createRules([$ruleToCopy])[0]['item']['item'];

        $patternsCreateData = [];
        foreach ($patterns as $pattern) {
            unset($pattern['variable_id']);
            unset($pattern['pattern_id']);

            $pattern['rule_id'] = (int) $rule['rule_id'];
            $pattern['active'] = 0;
            $patternsCreateData[] = $pattern;
        }
        if (!empty($patternsCreateData)) {
            $out['patterns'][] = $this->getBrungildaApi()->createPatterns($patternsCreateData);
        }

        $excPatternsCreateData = [];
        foreach ($excPatterns as $excPattern) {
            unset($excPatterns['exception_pattern_id']);
            $excPattern['rule_id'] = (int) $rule['rule_id'];
            $excPatternsCreateData[] = $excPattern;
        }
        if (!empty($excPatternsCreateData)) {
            $out['excPatterns'][] = $this->getBrungildaApi()->createExceptionPatterns($excPatternsCreateData);
        }

        $templatesCreateData = [];
        foreach ($templates as $template) {
            unset($template['template_id']);
            if (empty($template['timeslot_id'])) {
                unset($template['timeslot_id']);
            }
            $template['rule_id'] = (int) $rule['rule_id'];
            $templatesCreateData[] = $template;
        }
        if (!empty($templatesCreateData)) {
            $out['templates'][] = $this->getBrungildaApi()->createTemplates($templatesCreateData);
        }

        $subRulesCreateData = [];
        foreach ($subRules as $subRule) {
            unset($subRule['rule_id']);
            $subRule['parent_id'] = (int) $rule['rule_id'];
            $subRulesCreateData[] = $subRule;
        }
        if (!empty($subRulesCreateData)) {
            $out['subRules'][] = $this->getBrungildaApi()->createSubRule($subRulesCreateData);
        }

        return $out;
    }

    public function copyPatterns(array $copyData): array
    {
        $ruleId = (int) $copyData['ruleId'] ?? 0;
        if (!$ruleId) {
            return [];
        }
        $createData = [];
        $patterns = $copyData['patterns'];
        foreach ($patterns as $pattern) {
            unset($pattern['pattern_id']);
            $pattern['rule_id'] = (int) $ruleId;
            $createData[] = $pattern;
        }

        return $this->getBrungildaApi()->createPatterns($createData);
    }

    public function createPatternVar(array $requestData)
    {
        unset($requestData['pattern_var_id']);
        return $this->getBrungildaApi()->createPatternVariable($requestData);
    }

    public function updatePatternVar(array $requestData)
    {
        return $this->getBrungildaApi()->updatePatternVariable($requestData);
    }

    public function deletePatternVar(array $requestData)
    {
        return $this->getBrungildaApi()->deletePatternVariable(['pattern_var_id' => (int)$requestData['pattern_var_id']]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createLearningPhrases(array $requestData): array
    {
        $text    = $requestData['text'] ?? '';
        $ruleId = (int) $requestData['rule_id'] ?? 0;
        $topicId = (int) $requestData['topic_id'] ?? 0;

        if (!$text || !$ruleId || !$topicId) {
            return [];
        }

        $multiplePhrases = explode(PHP_EOL, $text);

        if (count($multiplePhrases) > 1)
        {
            $res = [];
            foreach ($multiplePhrases as $phrase) {
                $res[] = $this->getBrungildaApi()->createNlpClassPhrases([[
                    'rule_id'               => $ruleId,
                    'topic_id'              => $topicId,
                    'text'                  => $phrase,
                ]]);
            }

            return $res;
        }

        return $this->getBrungildaApi()->createNlpClassPhrases([[
            'rule_id'               => $ruleId,
            'topic_id'              => $topicId,
            'text'                  => $text,
        ]]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updateLearningPhrases(array $requestData): array
    {
        $updateData = [
            'learning_phrase_id' => $requestData['learning_phrase_id'],
            'text'               => $requestData['text']
        ];

        return $this->getBrungildaApi()->updateNlpClassPhrases([$updateData]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteLearningPhrases(array $requestData): array
    {
        $deleteData['learning_phrase_id'] = (int)$requestData['learning_phrase_id'];

        return $this->getBrungildaApi()->deleteNlpClassPhrases([$deleteData]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteMultipleLearningPhrases(array $requestData): array
    {
        if (empty($requestData['phraseIds'])) {
            return [];
        }

        $result = [];
        foreach ($requestData['phraseIds'] as $phraseId) {
            $result[] = $this->deleteLearningPhrases(['learning_phrase_id' => $phraseId]);
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getTopicsWithRestrictions(): array 
    {
        $topics       = [];
        $topicsArray  = $this->getBrungildaApi()->getTopics();
        $restrictions = $this->getBrungildaApi()->getRestrictions();
        
        foreach ($topicsArray as $topic) 
        {
            foreach ($restrictions as $restriction) {
                if ($restriction['id'] == $topic['topic_id']) {
                    $topic['restrictions'][] = (int) $restriction['scenario_id'];
                }
            }

            $topics[] = $topic;
        }

        usort($topics, array($this, "comparePriorityForSort"));
        
        return $topics;
    }

    /**
     * @return array
     */
    public function getPatternsVariables()
    {
        $arr = $this->getBrungildaApi()->getPatternsVariables();
        $response = [];
        foreach ($arr as $val) {
            $response[$val['pattern_id']]['pattern_var_id']   = $val['pattern_var_id'];
            $response[$val['pattern_id']]['index_content']    = $val['index_content'];
            $response[$val['pattern_id']]['variable_id']      = $val['variable_id'];
            $response[$val['pattern_id']]['pattern_id']      = $val['pattern_id'];
        }
        return $response;
    }
}

//
//active: 1
//deleted:0
//enabled:1
//exclude_template:1
//exec_limit:1
//link_id:null
//name:"do you live there"
//parent_id:578
//phrase_id:null
//priority:1
//rule_id:916
//topic_id:null
//type_phrase_id:null
//version:4
//
//
//
//active:0
//deleted:0
//enabled:null
//exclude_template:null
//exec_limit:null
//link_id:null
//name:"test"
//parent_id:578
//phrase_id:null
//priority:1
//rule_id:1080
//topic_id:null
//type_phrase_id:null
//version:0