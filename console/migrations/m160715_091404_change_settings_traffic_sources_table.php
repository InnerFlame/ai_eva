<?php

use yii\db\Migration;

class m160715_091404_change_settings_traffic_sources_table extends Migration
{
    public function up()
    {
        $this->dropTable('settings_traffic_sources_table');
        $this->createTable('settings_traffic_sources_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'traffic_source_name' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('settings_traffic_sources_table');
        $this->createTable('settings_traffic_sources_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'traffic_source_id' => $this->integer(11),
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
