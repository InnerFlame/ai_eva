<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\DataProvider;
use yii\web\Controller;

class ApiController extends Controller
{
    const SECRET_KEY = '2578bffc1e1df47ac5cef34b541aa654';

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionPlaceholder()
    {
        //TODO: temporary before rename on remote side
        return $this->actionDialogRecovery();
    }

    public function actionScam()
    {
        //TODO: temporary before rename on remote side
        return $this->actionExternalMessage();
    }

    public function actionPlaceholderForZeroStep()
    {
        $data     = $this->getDataFromPost();
        $messages = \Yii::$app->placeholderForZeroStepProcessor->getAnswerData($data);

        return $this->makeApiResponse($messages);
    }

    public function actionDialogRecovery()
    {
        $data     = $this->getDataFromPost();
        $messages = \Yii::$app->dialogRecovery->getAnswerData($data);

        return $this->makeApiResponse($messages);
    }

    public function actionExternalMessage()
    {
        $data     = $this->getDataFromPost();
        $messages = \Yii::$app->externalMessageProcessor->getAnswerData($data);

        return $this->makeApiResponse($messages);
    }

    public function actionMessage()
    {
        //Get json from request
        $json = file_get_contents('php://input');

        \Yii::$app->directMessageProcessor->addMessageToProcess($json);

        $dataRequest = json_decode($json, true);
        return \yii\helpers\Json::encode([
            'id'       => $dataRequest['id'],
            'result'   => '',
        ]);
    }

    protected function getDataFromPost()
    {
        $request = \Yii::$app->request;
        $post = $request->post();

        $senderId       = $post['from'];
        $multiProfileId = (!empty($post['multi_profile_id'])) ? $post['multi_profile_id'] : $post['from'];
        $recipientId    = $post['to'];
        $locale         = $post['locale'] ?? '';
        $senderLocale   = $post['senderLocale'] ?? '';
        $splits         = $post['splits'] ?? null;

        if (empty($post['site'])) {
            throw new \Exception('No site' . json_encode($post));
        }

        $data = [
            'from'               => $senderId,
            'multi_profile_id'   => $multiProfileId,
            'to'                 => $recipientId,
            'country'            => $post['country'],
            'site'               => $post['site'],
            'text'               => $post['text'] ?? '',
            'isScam'             => true,
            'profiles' => [
                $senderId       => [
                    'id'                => $senderId,
                    'multi_profile_id'  => $multiProfileId,
                    'site'              => $post['site'],
                    'country'           => $post['country'],
                    'language'          => \Yii::$app->params['availableLocales'][$senderLocale] ?? 'DEF',
                    'paid'              => $post['paid'],
                    'platform'          => $post['platform'],
                    //TODO: need to receive correct source
                    'traff_src'         => $post['traff_src_from'] ?? '',
                    'action_way'        => '',
                ],
                $recipientId    => [
                    'id'                => $recipientId,
                    'multi_profile_id'  => $recipientId,
                    'site'              => $post['site'],
                    'country'           => $post['sender_country'] ?? $post['country'],
                    'language'          => \Yii::$app->params['availableLocales'][$locale] ?? 'DEF',
                    'platform'          => $post['platform'],
                    'traff_src'         => $post['traff_src_to'] ?? ($post['traff_src'] ?? ''),
                    'action_way'        => $post['action_way_to'] ?? ($post['action_way'] ?? ''),
                    'city'              => $post['sender_city'] ?? '',
                    'bday'              => $post['sender_birthday'] ?? '',
                    'time_offset'       => (int)($post['time_offset'] ?? '0'),
                    'profile_short_id'  => $post['profile_short_id'] ?? '',
                    'prepaid'           => (int)($post['prepaid'] ?? 0),
                    'splits'            => $splits
                ],
            ],
        ];

        return $data;
    }

    public function actionContent()
    {
        $request = \Yii::$app->request;
        $get = $request->get();

        if (empty($get['key']) || $get['key'] != self::SECRET_KEY) {
            return '';
        }

        if (empty($get['type'])) {
            return '';
        }

        $content    = '';
        $lang       = $get['lang'] ?? null;

        switch ($get['type']) {
            case 'templates':
                $content = \yii\helpers\Json::encode($this->getTemplates($lang));
                break;
            case 'phrases':
                $content = \yii\helpers\Json::encode($this->getPhrases($lang));
                break;
            case 'placeholders':
                $content = \yii\helpers\Json::encode($this->getPlaceholders());
                break;
        }

        return $content;
    }

    protected function getTemplates($lang = null)
    {
        $content = array();

        $topics = DataProvider::getInstance()->getTopics();
        foreach ($topics as $topic) {
            if ($lang != null && $topic->getLanguageTag() != $lang) {
                continue;
            }
            $topicId = $topic->getId();
            $rules = DataProvider::getInstance()->getRules($topicId);
            foreach ($rules as $rule) {
                $ruleId = $rule->getRuleId();
                $ruleContent = DataProvider::getInstance()->getRule($ruleId);

                // Get templates from rule
                $templates = $ruleContent->getTemplates();
                foreach ($templates as $template) {
                    $content[] = [
                        'topic_id'      => $topicId,
                        'topic_name'    => $topic->getName(),
                        'rule_id'       => $ruleId,
                        'rule_name'     => $rule->getName(),
                        'template_id'   => $template->getTemplateId(),
                        'template_text' => $template->getText(),
                        'lang'          => $topic->getLanguageTag(),
                    ];
                }

                // Get templates from subrules
                $subrules = $ruleContent->getRules();
                foreach ($subrules as $subrule) {
                    $subruleId = $subrule->getRuleId();
                    $subruleContent = DataProvider::getInstance()->getRule($subruleId);
                    $templates = $subruleContent->getTemplates();
                    foreach ($templates as $template) {
                        $content[] = [
                            'rule_id'       => $ruleId,
                            'rule_name'     => $rule->getName(),
                            'subrule_id'    => $subruleId,
                            'subrule_name'  => $subrule->getName(),
                            'template_id'   => $template->getTemplateId(),
                            'template_text' => $template->getText(),
                            'lang'          => $topic->getLanguageTag(),
                        ];
                    }
                }
            }
        }

        return $content;
    }

    protected function getPhrases($lang = null)
    {
        $content = array();

        $groupsPhrases = DataProvider::getInstance()->getGroupsPhrases();

        //Get group phrases tags (we don't use PhrasesForm for getting results more quickly)
        foreach ($groupsPhrases as $key => $val) {
            $currentGroupPhrasesTagsArr = [];
            $currentGroupPhrasesTags    = \frontend\models\phrases\GroupPhrasesTag::findAll(['groupPhrasesId' => $val->getId()]);
            foreach ($currentGroupPhrasesTags as $currentGroupPhrasesTag) {
                $currentGroupPhrasesTagsArr[] = $currentGroupPhrasesTag->tagId;
            }
            $groupsPhrases[$key]->setTags($currentGroupPhrasesTagsArr);
        }

        foreach ($groupsPhrases as $groupPhrases) {
            if ($lang != null && $groupPhrases->getLanguageTag() != $lang) {
                continue;
            }
            $groupPhrasesId = $groupPhrases->getId();
            $phrases = DataProvider::getInstance()->getPhrases($groupPhrasesId);
            foreach ($phrases as $phrase) {
                $content[] = [
                    'group_phrases_id'      => $groupPhrasesId,
                    'group_phrases_name'    => $groupPhrases->getText(),
                    'phrase_id'             => $phrase->getId(),
                    'phrase_text'           => $phrase->getText(),
                    'phrase_tag'            => $groupPhrases->getTagsNames(),
                    'lang'                  => $groupPhrases->getLanguageTag(),
                ];
            }
        }

        return $content;
    }

    protected function getPlaceholders()
    {
        $content = array();

        $placeholders = \frontend\models\placeholders\Placeholder::find()->orderBy('id')->all();
        foreach ($placeholders as $placeholder) {
            $content[] = [
                'id'        => $placeholder->id,
                'name'      => $placeholder->name,
                'type'      => $placeholder->type,
                'url'       => $placeholder->url,
                'url_wap'   => $placeholder->url_wap,
                'url_type'  => $placeholder->url_type,
                'active'    => $placeholder->active,
            ];
        }

        return $content;
    }

    private function makeApiResponse($messages)
    {
        if (!empty($messages['error'])) {
            return \yii\helpers\Json::encode([
                'code'  => 400,
                'error' => $messages['error'],
            ]);
        }

        return \yii\helpers\Json::encode([
            'code'      => 200,
            'messages'  => $messages,
        ]);
    }
}
