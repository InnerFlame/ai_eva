<?php

use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use demogorgorn\ajax\AjaxSubmitButton;
use frontend\assets\TestPhrasesAsset;

/**
 * @var $this \yii\web\View
 * @var $model \frontend\models\testPhrases\TestPhrasesFormModel
 */

TestPhrasesAsset::register($this);
?>

<h1>Test classifier models page</h1>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-2">
        <?= $form->field($model, 'model')->dropDownList(
            $model->getModelsForDropDown(),
            [
                'prompt' => 'Select model'
            ]
        ); ?>
    </div>
    <div class="col-md-10">
        <?= $form->field($model, 'text') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <?php
        AjaxSubmitButton::begin([
            'label'       => 'Send',
            'id'          => 'TestChatModelForm',
            'ajaxOptions' => [
                'type'    => 'POST',
                'url'     => Url::to(['/test-phrases/send']),
                'success' => new JsExpression('sendPhrase'),
            ],
            'options' => ['class' => 'btn btn-success', 'type' => 'submit', 'style' => 'width: 100%']
        ]);
        AjaxSubmitButton::end();
        ?>
    </div>
</div>
<br>
<br>
<h2>Results</h2>
<div id="result"></div>
<?php ActiveForm::end(); ?>
<br>

