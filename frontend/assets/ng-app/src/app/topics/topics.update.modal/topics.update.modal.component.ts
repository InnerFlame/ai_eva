import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TopicsModel} from "../../shared/models";
import {IMultiSelectSettings, IMultiSelectTexts} from "angular-2-dropdown-multiselect";

declare let jQuery: any;

@Component({
    selector: 'app-topicsUpdateModal',
    templateUrl: './topics.update.modal.component.html',
    styleUrls: ['./topics.update.modal.component.css']
})
export class TopicsUpdateModalComponent implements OnInit, AfterViewInit {

    @ViewChild('modal') modal: ElementRef;

    @Input() topicData: any;
    @Input() modalErrorText: string;
    @Input() modalSuccessText: string;
    @Input() displayModalLoader: boolean;
    @Input() topicModel: TopicsModel;
    @Input() availableScenarios: any;
    @Input() availableTags: any;
    @Input() availableLanguages: any;

    @Output() onDeleteTopic: EventEmitter<any> = new EventEmitter<any>();
    @Output() onUpdateTopic: EventEmitter<any> = new EventEmitter<any>();
    @Output() onPasteRule: EventEmitter<any> = new EventEmitter<any>();
    @Output() onClose: EventEmitter<any> = new EventEmitter<any>();

    public multiSelectSettings: IMultiSelectSettings = {
        enableSearch: true,
        showCheckAll: true,
        showUncheckAll: true,
        checkedStyle: 'fontawesome',
        buttonClasses: 'btn btn-default btn-block multiSelectSettingsButton',
        dynamicTitleMaxItems: 900,
        displayAllSelectedText: false,
    };

    public multiSelectTexts: IMultiSelectTexts = {
        checkAll: 'Select all',
        uncheckAll: 'Unselect all',
        checked: 'item selected',
        checkedPlural: 'items selected',
        searchPlaceholder: 'Find',
        searchEmptyResult: 'Nothing found...',
        searchNoRenderText: 'Type in search box to see results...',
        defaultTitle: 'Select Items',
        allSelected: 'All selected'
    };

    constructor() {}

    ngAfterViewInit() {
        jQuery(this.modal.nativeElement).modal();
        jQuery(this.modal.nativeElement).on('hidden.bs.modal', () => {this.onClose.emit(true)});
    }

    ngOnInit() {
    }

    public deleteTopic(): void {
        this.onDeleteTopic.emit(true);
        jQuery(this.modal.nativeElement).modal('hide');
    }

    public updateTopic(): void {
        this.onUpdateTopic.emit(true);
    }

    public pasteRule() {
        this.onPasteRule.emit(true);
    }
}
