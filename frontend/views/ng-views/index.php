<?php

use frontend\assets\AngularAsset;

/**
 * @var $this \yii\web\View
 */

AngularAsset::register($this);
?>
<app-root>
    <!-- Just loader, all content inside @app-root@ will be replaced -->
    <div class="col-md-12 text-center">
        <img src="/build/assets/loader.gif" style="margin-top: 10%;">
    </div>
</app-root>
