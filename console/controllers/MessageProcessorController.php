<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\IterativeRunner;

class MessageProcessorController extends Controller
{
    public function actionProcessMessages()
    {
        $runner = new IterativeRunner(
            function () {
                \Yii::$app->directMessageProcessor->processMessages();
            },
            [],
            5,
            0
        );
        $runner->run();
    }

    public function actionPushReadyMessagesToQueue()
    {
        $runner = new IterativeRunner(
            function() {
                \Yii::$app->directMessageProcessor->pushReadyMessagesToQueue();
            },
            [],
            1,
            0
        );
        $runner->run();
    }

    public function actionSendMessages()
    {
        $runner = new IterativeRunner(
            function() {
                \Yii::$app->directMessageProcessor->sendMessages();
            },
            [],
            5,
            0
        );
        $runner->run();
    }
}
