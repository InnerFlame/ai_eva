<?php

namespace common\widgets\StagesFindReplacer;

use common\widgets\StagesFindReplacer\models\StagesFindReplaceForm;
use yii\bootstrap\Widget;

class StagesFindReplacer extends Widget
{
    public function init()
    {
        parent::init();
        StagesFindReplacerAsset::register($this->getView());
    }

    public function run()
    {
        return $this->render('index.php', [
            'model' => (new StagesFindReplaceForm())
        ]);
    }
}