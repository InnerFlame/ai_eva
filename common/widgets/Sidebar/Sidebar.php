<?php

namespace common\widgets\Sidebar;

use yii\bootstrap\Nav;
use yii\bootstrap\Html;
use yii\widgets\Menu;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class Sidebar extends Nav
{
    public function init()
    {
        parent::init();
        $view = $this->getView();
        SidebarAsset::register($view);
    }

    public function renderItems()
    {
        $items = [];
        foreach ($this->items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                continue;
            }
            $items[] = $this->renderIndexItem($i, $item);
        }

        return Html::tag('ul', implode("\n", $items), $this->options);
    }

    //Redefinition of isChildActive method for display it as active (white background) in sidebar
    protected function isChildActive($items, &$active)
    {
        foreach ($items as $i => $child) {
            $supplementChild = $child;
            if (isset($supplementChild['url'][0])) {
                $supplementChild['url'][0] = $supplementChild['url'][0] . '/index';
            }
            if (ArrayHelper::remove($items[$i], 'active', false) || $this->isItemActive($child) ||
                $this->isItemActive($supplementChild)) {
                Html::addCssClass($items[$i]['options'], 'active');
                if ($this->activateParents) {
                    $active = true;
                }
            }
        }
        return $items;
    }

    public function renderIndexItem($index, $item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $supplementItem = $item;
            if (isset($supplementItem['url'][0])) {
                $supplementItem['url'][0] = $supplementItem['url'][0] . '/index';
            }
            $active = $this->isItemActive($item) || $this->isItemActive($supplementItem);
        }

        if (empty($items)) {
            $items = '';
        } else {
            $linkOptions['data-toggle'] = 'collapse';
            $linkOptions['aria-expanded'] = 'false';
            $collapseItemsId = $this->options['id'] . '-' . $index . '-collapse';
            $url = '#' . $collapseItemsId;
            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }
                $items = $this->renderCollapse($items, $collapseItemsId);
            }
        }

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'active');
        }

        return Html::tag('li', Html::a($label, $url, $linkOptions) . $items, $options);
    }

    protected function renderCollapse($items, $id)
    {
        return Menu::widget([
            'items' => $items,
            'options' => [
                'class' => 'collapse nav nav-pills nav-stacked',
                'id' => $id,
            ],
        ]);
    }
}
