import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { SharedModule }                  from "../shared";
import { TopicsGroupComponent }          from "./topics.group.component";
import { DragulaModule }                 from 'ng2-dragula';
import { MultiselectDropdownModule }     from 'angular-2-dropdown-multiselect';

const routing: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'topics-group-ng',
        component: TopicsGroupComponent
    }
]);

@NgModule({
    imports: [
        routing,
        SharedModule,
        DragulaModule,
        MultiselectDropdownModule
    ],
    declarations: [
        TopicsGroupComponent
    ]
})

export class TopicsGroupModule {}