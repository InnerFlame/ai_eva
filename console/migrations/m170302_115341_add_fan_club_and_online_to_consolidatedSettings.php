<?php

use yii\db\Migration;

class m170302_115341_add_fan_club_and_online_to_consolidatedSettings extends Migration
{
    public function up()
    {
        $this->addColumn('consolidatedSettings', 'is_fan_club_allowed', $this->boolean());
        $this->addColumn('consolidatedSettings', 'is_online', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('consolidatedSettings', 'is_fan_club_allowed');
        $this->dropColumn('consolidatedSettings', 'is_online');
    }
}
