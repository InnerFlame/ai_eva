<?php
use yii\bootstrap\Html;
use frontend\assets\SearchAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

SearchAsset::register($this);
?>
<div class="col-md-12" id="searchContainer">
    <?php $form = ActiveForm::begin(['method' => 'GET', 'action' => Url::to(['search/']),]); ?>
        <?= $form->field($model, 'text')->textInput(['class' => 'form-control', 'aria-label' => '...',])->label(false); ?>
        <div class="input-group">
            <div class="input-group-btn">
                <?= Html::submitButton('Search in', ['class' => 'btn btn-primary']) ?>
                <button type="button" id="searchByButton" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $model->searchBy ?> <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0);"><?= $model::PATTERNS ?></a></li>
                    <li><a href="javascript:void(0);"><?= $model::EXC_PATTERNS ?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="javascript:void(0);"><?= $model::TEMPLATES ?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="javascript:void(0);"><?= $model::RULES ?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="javascript:void(0);"><?= $model::PHRASES ?></a></li>
                </ul>
                <?= $form->field($model, 'searchBy')->hiddenInput(['id' => 'searchBy',])->label(false) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    <?php if(!empty($model->text)): ?>
        <hr/>
        <span>Search results for <b><?= $model->text ?></b> in <b><?= $model->searchBy ?></b></span>
    <?php endif; ?>
    <hr/>
    <?php
    foreach ($model->searchResults as $key => $val) {
        echo '<div class="panel panel-default" style=""><div class="panel-body">';
        switch ($model->searchBy) {
            case \frontend\models\search\SearchForm::RULES:
                echo '<div class="col-md-1">' . ($key + 1) . '.</div><div class="col-md-11">';
                echo 'Name: ' . $val->getName() .
                    '<br/>Topic: &thinsp;' . ($val->_topicName ?? '') .
                    '<br/>Rule:&ensp;&thinsp;&thinsp;' . ($val->_ruleName ?? '');
                echo '<br/>' . Html::a('Link', [
                        '/topics',
                        'topicId' => $val->getTopicId(),
                        'ruleId' => $val->getRuleId(),
                    ], ['target'=>'_blank']);
                echo '</div>';
                break;
            case \frontend\models\search\SearchForm::TEMPLATES:
                echo '<div class="col-md-1">' . ($key + 1) . '.</div><div class="col-md-11">';
                echo 'Text:&ensp;&nbsp;' . $val->getText() .
                    '<br/>Topic: ' . ($val->_topicName ?? '') .
                    '<br/>Rule:&ensp;&thinsp;' . ($val->_ruleName ?? '');
                if (isset($val->_topicId)) {
                    echo '<br/>' . Html::a('Link', [
                        '/topics',
                        'topicId' => $val->_topicId,
                        'ruleId' => $val->getRuleId(),
                        'templateId' => $val->getTemplateId(),
                    ], ['target'=>'_blank']);
                }
                echo '</div>';
                break;
            case \frontend\models\search\SearchForm::PATTERNS:
                echo '<div class="col-md-1">' . ($key + 1) . '.</div><div class="col-md-11">';
                echo 'Text:&ensp;&nbsp;' . $val->getText() .
                    '<br/>Test:&ensp;&nbsp;' . $val->getTest() .
                    '<br/>Topic: ' . ($val->_topicName ?? '') .
                    '<br/>Rule:&ensp;&thinsp;' . ($val->_ruleName ?? '');
                if (isset($val->_topicId)) {
                    echo '<br/>' . Html::a('Link', [
                        '/topics',
                        'topicId' => $val->_topicId,
                        'ruleId' => $val->getRuleId(),
                        'patternId' => $val->getPatternId(),
                    ], ['target'=>'_blank']);
                }
                echo '</div>';
                break;
            case \frontend\models\search\SearchForm::EXC_PATTERNS:
                echo '<div class="col-md-1">' . ($key + 1) . '.</div><div class="col-md-11">';
                echo 'Text:&ensp;&nbsp;' . $val->getText() .
                    '<br/>Test:&ensp;&nbsp;' . $val->getTest() .
                    '<br/>Topic: ' . ($val->_topicName ?? '') .
                    '<br/>Rule:&ensp;&thinsp;' . ($val->_ruleName ?? '');
                if (isset($val->_topicId)) {
                    echo '<br/>' . Html::a('Link', [
                        '/topics',
                        'topicId' => $val->_topicId,
                        'ruleId' => $val->getRuleId(),
                        'excPatternId' => $val->getPatternId(),
                    ], ['target'=>'_blank']);
                }
                echo '</div>';
                break;
            case \frontend\models\search\SearchForm::PHRASES:
                echo '<div class="col-md-1">' . ($key + 1) . '.</div><div class="col-md-11">';
                echo 'Text: &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&thinsp;' . $val->getText() .
                    '<br/>Group phrase: &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;' . ($val->_groupPhraseText ?? '') .
                    '<br/>Group group phrase: ' . ($val->_groupGroupPhraseName ?? '');
                if (isset($val->_groupGroupPhraseId)) {
                    echo '<br/>' . Html::a('Link', [
                        '/phrases',
                        'groupGroupPhrasesId' => $val->_groupGroupPhraseId,
                        'groupPhrasesId' => $val->getGroupPhrasesId(),
                        'id' => $val->getId(),
                    ], ['target'=>'_blank']);
                }
                echo '</div>';
                break;
        }
        echo '</div></div>';
    }
    ?>
</div>
