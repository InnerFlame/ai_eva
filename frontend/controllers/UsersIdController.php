<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\usersId\SearchCommunicationLog;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class UsersIdController extends BaseController
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('usersId')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        $searchModel = new SearchCommunicationLog();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider'  => $dataProvider,
            'searchModel'   => $searchModel,
        ]);
    }

    public function actionMessages()
    {
        $request = \Yii::$app->request;
        $searchModel = new SearchCommunicationLog();
        $dataProvider = $searchModel->getMessages(\Yii::$app->request->get());
        // Since view _messages does not contain any script we can use there renderPartial too
        return $this->renderAjax('_messages', [
            'dataProvider'  => $dataProvider,
        ]);
    }
}
