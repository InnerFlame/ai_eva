<?php
use common\widgets\BindableFormItems\BindableFormItems;

$patternsItems = array();
foreach ($model->rule->getExceptionPatterns() as $pattern) {
    $item['images'] = [
        [
            'class' => 'glyphicon glyphicon-signal' . ($pattern->isActive() ? ' active' : ''),
            'name' => 'activeExcPatternIcon',
        ],
    ];
    $item['text'] = $pattern->getText();
    $item['hiddens'] = [
        ['name' => 'testText', 'value' => $pattern->getTest()],
        ['name' => 'excPatternId', 'value' => $pattern->getPatternId()],
    ];
    $patternsItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'excPatternsBindableForm',
    'items' => $patternsItems,
    'checked' => true,
]);
