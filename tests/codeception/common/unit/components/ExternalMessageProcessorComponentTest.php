<?php
namespace tests\codeception\common\unit\components;

use tests\codeception\common\fixtures\PostFixture;
use Yii;
use tests\codeception\common\unit\TestCase;
use common\components\ExternalMessageProcessorComponent;

/**
 * ExternalMessageProcessorComponent test
 */
class ExternalMessageProcessorComponentTest extends TestCase
{
    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    // TODO Move to parent class which will be extend TestCase
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $parameters);
    }

    /**
     * @dataProvider splitMessageProvider
     */
    public function testSplitMessage($text, $expected)
    {
        $empc = new ExternalMessageProcessorComponent();
        $messages = $this->invokeMethod($empc, 'splitMessage', array($text, $this->params));

        foreach ($expected as $key => $value) {
            $this->assertEquals($messages[$key]['message'], $value, 'There is a failure in text:' . $value . PHP_EOL);
            $this->assertGreaterThan(0, $messages[$key]['delay'], 'There is a failure in delay:' . $value . PHP_EOL . 'Array: ' . var_export($messages, true));
        }
    }

    public function splitMessageProvider()
    {
        return [
            ['hi there', array('hi there')],
            ['hi ', array('hi')],
            ['', array()],
            [' ', array('')],
            ['hi;)there', array('hi;)', 'there')],
            ['hi...there', array('hi...', 'there')],
            ['hi ... there', array('hi ...', 'there')],
            ['hi... there', array('hi...', 'there')],
            ['hi...', array('hi...')],
            [
                'hi... flirt4free and $flirt4free and how about $flirt4free',
                array('hi...', 'flirt4free and', '$flirt4free', 'and how about', '$flirt4free')],
            [
                'hi... flirt4free and $flirt4free and how about $flirt4free with you',
                array('hi...', 'flirt4free and', '$flirt4free', 'and how about', '$flirt4free', 'with you')],
            ['$flirt4free', array('$flirt4free')],
            ['tratata $flirt4free tatata', array('tratata', '$flirt4free', 'tatata')],
            ['tratata $flirtt4free tatata', array('tratata', '$flirtt4free', 'tatata')],
            ['tratata flirt4free tatata', array('tratata flirt4free tatata')],
            ['tratata$flirt4free tatata', array('tratata', '$flirt4free', 'tatata')],
            ['tratata$flirt4freetatata', array('tratata', '$flirt4freetatata')],
            [
                'my ex actually liked that i\'m a cam model. here\'s my profile, you can check out $wcchat_livecam what\'s your attitude to cam models, btw?',
                array('my ex actually liked that i\'m a cam model. here\'s my profile, you can check out', '$wcchat_livecam', 'what\'s your attitude to cam models, btw?')
            ],
            [
                'i like you sooo much! but... since i forgot my password for social websites, mb let\'s chat here? $usbuddies the site\'s very handy, you can send pics easily',
                array('i like you sooo much! but...', 'since i forgot my password for social websites, mb let\'s chat here?', '$usbuddies', 'the site\'s very handy, you can send pics easily')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'params' => [
                'class' => PostFixture::className(),
                'dataFile' => '@tests/codeception/common/unit/fixtures/data/components/post.php'
            ],
        ];
    }
}
