import { Component }              from '@angular/core';
import { Title }                  from "@angular/platform-browser";
import { HttpService }            from "../shared/services/http.service";
import { OnInit }                 from '@angular/core';
import { ActivatedRoute, Params } from "@angular/router";
import { PagerService }           from "../shared/services";
import { Router }                 from '@angular/router';

@Component({
    selector    : 'errorLog',
    templateUrl : './error.log.html'
})

export class ErrorLogComponent implements OnInit
{
    readonly pagerPageSize : number = 30;

    public displayLoadingBlock : boolean = true;
    public errorText           : string = '';
    public pager               : any    = {};
    public pagedItems          : any[];

    private allItems   : any;
    private totalCount : number;

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param titleService
     * @param pagerService
     * @param activatedRoute
     * @param router
     */
    constructor(
        private httpService    : HttpService,
        private titleService   : Title,
        private pagerService   : PagerService,
        private activatedRoute : ActivatedRoute,
        private router         : Router
    ) {  }

    /**
     * On init script actions and events, here we bind available models
     */
    ngOnInit(): void
    {
        this.titleService.setTitle( 'Error Log' );

        this.activatedRoute.params.subscribe((params: Params) => {
            let page: number = !params['page'] ? 1 : parseInt(params['page']);

            this.httpService.get('/error-log/get-data/' + page).then(data => {
                    this.allItems   = data['items'];
                    this.totalCount = data['count'];
                    this.setPage(page, false);
                    this.displayLoadingBlock = false;
                }, err =>  {
                    this.errorText = 'Error on server!';
                    this.displayLoadingBlock = false;
            });
        });
    }

    setPage(page: number, callFromTemplate = true): void
    {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        window.scrollTo(0, 0);

        if (callFromTemplate) {
            this.displayLoadingBlock = true;
            this.router.navigate(['/error-log/' + page]);
            return;
        }

        this.pager      = this.pagerService.getPager(this.totalCount, page, this.pagerPageSize);
        this.pagedItems = this.allItems;
    }

    clearLog(): void
    {
        if (!confirm('Are you sure you want to clear all log data?')) {
            return;
        }

        this.displayLoadingBlock = true;

        this.httpService.post('/error-log/clear-log', {'clearData': true}).then(data => {
            let status = data['status'];
            if (!status) {
                this.errorText = 'Failed to clear log!';
            } else {
                this.pagedItems = [];
            }
            this.displayLoadingBlock = false;
        }, err =>  {
            this.errorText = 'Error on server!';
            this.displayLoadingBlock = false;
        });
    }

    refreshPage(): void
    {
        location.reload();
    }
}
