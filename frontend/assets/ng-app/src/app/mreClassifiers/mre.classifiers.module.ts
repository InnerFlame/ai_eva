import {ModuleWithProviders, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SharedModule} from "../shared";
import {MreClassifiersComponent} from "./mre.classifiers.component";

const routing: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'mre-classifiers-ng',
        component: MreClassifiersComponent
    }
]);

@NgModule({
    imports: [
        routing,
        SharedModule
    ],
    declarations: [
        MreClassifiersComponent
    ]
})

export class MreClassifiersModule {}