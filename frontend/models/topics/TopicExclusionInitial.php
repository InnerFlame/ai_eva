<?php
namespace frontend\models\topics;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $topicId
 * @property integer $isExclude
 * @property integer $countTriggering
 * @property string $createdAt
 */
class TopicExclusionInitial extends ActiveRecord
{
    public static function tableName()
    {
        return 'topicExclusionInitial';
    }
}
