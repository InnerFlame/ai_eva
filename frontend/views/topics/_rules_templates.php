<?php
use common\widgets\BindableFormItems\BindableFormItems;

$templatesItems = array();
foreach ($model->rule->getTemplates() as $template) {
    $item['text'] = $template->getText();
    $item['hiddens'] = [
        ['name' => 'templateId', 'value' => $template->getTemplateId()],
        ['name' => 'timeslotId', 'value' => $template->getTimeslotId()],
    ];
    $templatesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'templatesBindableForm',
    'items' => $templatesItems,
    'checked' => true,
]);
