<?php

use yii\db\Migration;

/**
 * Handles the creation for table `placeholders_sender_types_table`.
 */
class m160726_120015_create_placeholders_sender_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('placeholders_sender_types_table', [
            'id' => $this->primaryKey(),
            'placeholder_id' => $this->integer(11),
            'sender_type_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('placeholders_sender_types_table');
    }
}
