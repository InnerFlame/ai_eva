//Arrays for drag&drop topics, rules, subrules...
var sortableTopicsArray     = null;
var sortableRulesArray      = null;
var sortableSubRulesArray   = null;

//Supporting variables
var currentTopicId = null;
var currentRuleId = null;
var currentSubRuleId = null;

var allPatternsItemsWasChecked      = false;
var allTemplatesItemsWasChecked     = false;
var allSubPatternsItemsWasChecked   = false;
var allSubTemplatesItemsWasChecked  = false;

var ruleIdToCopy        = false;

var ruleIdFromCopy      = false;
var patternsIdsToCopy   = [];

//Get array of checked patterns, templates, sub patterns, sub templates ids
function getCheckedObjectsIds(listDivId, listItemObjectIdName)
{
    var checkedObjectsIds = [];
    $('#' + listDivId +' li.list-group-item input[name="itemCheckbox"]:checked').each(function () {
        var id = $(this).closest('li.list-group-item').find('input[name="' + listItemObjectIdName + '"]').val();
        checkedObjectsIds.push(id);
    });
    return checkedObjectsIds;
}

//Get array of checked or current patterns, templates, sub patterns, sub templates ids
function getCheckedOrCurrentObjectsIds(bindableFormId, bindableFormObjectIdName, listDivId, listItemObjectIdName)
{
    //Get checked objects
    var checkedOrCurrentObjectsIds = getCheckedObjectsIds(listDivId, listItemObjectIdName);
    //If there are no checked objects get current objects
    if (checkedOrCurrentObjectsIds.length == 0) {
        var currentObjectId = $('#' + bindableFormId +' input[name="' + bindableFormObjectIdName + '"]').val();
        if (currentObjectId != 0) {
            checkedOrCurrentObjectsIds.push(currentObjectId);
        }
    }
    return checkedOrCurrentObjectsIds
}

//Get string of checked or current patterns names (for modal window)
/*function getCheckedOrCurrentPatternsNames()
{
    var patternsNames = '';
    var patternsIds = getCheckedOrCurrentObjectsIds('patternsBindableForm', 'patternId', 'patterns', 'patternId');
    $.each(patternsIds, function (index, value) {
        var patternName = $('#patterns ul li input[name="patternId"][value="' + value + '"]').siblings('span[name="itemText"]').html();
        patternsNames += (index + 1) + '. ' + patternName + '<br/>'
    });
    return patternsNames;
}*/

//Load list of topics and make it sortable
function updateTopics()
{
    $.ajax({
        url: '/topics/topics',
        success: function (data) {
            //Update topics
            $('#topics').html(data);
            //Make topics sortable (by drag&drop)
            $("#topicsList").sortable({
                //Get array of topics (id, priority) before sorting
                start: function () {
                    sortableTopicsArray = [];
                    $('#topicsList li.list-group-item.sortable').each(function () {
                        var elem = [];
                        elem['id'] = $(this).find('input[name="topicId"]').val();
                        elem['priority'] = $(this).find('input[name="topicPriority"]').val();
                        //if element is not a 'Add new topic' button
                        if (elem['id'] !== undefined && elem['priority'] !== undefined) {
                            sortableTopicsArray.push(elem);
                        }
                    });
                },
                //Get array of topics (id, priority) after sorting
                stop: function () {
                    var changedElements = [];
                    var i = 0;
                    $('#topicsList li.list-group-item.sortable').each(function () {
                        var elem = {};
                        elem.topic_id = $(this).find('input[name="topicId"]').val();
                        elem.priority = $(this).find('input[name="topicPriority"]').val();
                        //Push elements with changed priority
                        if (elem.priority != sortableTopicsArray[i]['priority']) {
                            elem.priority = sortableTopicsArray[i]['priority'];
                            changedElements.push(elem);
                        }
                        i++;
                    });
                    //If we have elements with changed priority make ajax request
                    if (changedElements.length > 0) {
                        $.ajax({
                            url: '/topics/update-topics',
                            type: "POST",
                            data: 'params=' + JSON.stringify(changedElements),
                            success: function (data) {
                                if (data != 'true') {
                                    alert('An error occurred while updating the topics priority!');
                                }
                            }
                        });
                    }
                }
            });
            //Trigger click on first or current topic item
            var firstTopicsItem;
            if ($('#currentTopicId').val() != '') {
                var str = 'input[name="topicId"][value="' + $('#currentTopicId').val() + '"]';
                firstTopicsItem = $('#topics li.list-group-item.sortable').has(str);
            } else {
                firstTopicsItem = $('#topics li.list-group-item.sortable:first-child');
            }

            if (firstTopicsItem.length == 0) {
                currentTopicId = null;
            } else {
                firstTopicsItem.click();
            }
        }
    });
}

//Load list of rules and make it sortable
function updateRules()
{
    $.ajax({
        url: '/topics/rules',
        type: "POST",
        data: 'topicId=' + currentTopicId,
        success: function (data) {
            //Update rules
            $('#rules').html(data);
            //Make rules sortable (by drag&drop)
            $("#rulesList").sortable({
                //Get array of rules (id, priority) before sorting
                start: function () {
                    sortableRulesArray = [];
                    $('#rulesList li.list-group-item.sortable').each(function () {
                        var elem = [];
                        elem['id'] = $(this).find('input[name="ruleId"]').val();
                        elem['priority'] = $(this).find('input[name="rulePriority"]').val();
                        //if element is not a 'Add new rule' button
                        if (elem['id'] !== undefined && elem['priority'] !== undefined) {
                            sortableRulesArray.push(elem);
                        }
                    });
                },
                //Get array of rules (id, priority) after sorting
                stop: function () {
                    var changedElements = [];
                    var i = 0;
                    $('#rulesList li.list-group-item.sortable').each(function () {
                        var elem = {};
                        elem.rule_id = $(this).find('input[name="ruleId"]').val();
                        elem.priority = $(this).find('input[name="rulePriority"]').val();
                        //Push elements with changed priority
                        if (elem.priority != sortableRulesArray[i]['priority']) {
                            elem.priority = sortableRulesArray[i]['priority'];
                            changedElements.push(elem);
                        }
                        i++;
                    });
                    //If we have elements with changed priority make ajax request
                    if (changedElements.length > 0) {
                        $.ajax({
                            url: '/topics/update-rules',
                            type: "POST",
                            data: 'params=' + JSON.stringify(changedElements),
                            success: function (data) {
                                if (data != 'true') {
                                    alert('An error occurred while updating the rules priority!');
                                }
                            }
                        });
                    }
                }
            });
            //Trigger click on first or current rule item
            var firstRulesItem;
            var temp = $('#currentRuleId').val();
            if ($('#currentRuleId').val() != '') {
                var str = 'input[name="ruleId"][value="' + $('#currentRuleId').val() + '"]';
                firstRulesItem = $('#rules li.list-group-item.sortable').has(str);
            } else {
                firstRulesItem = $('#rules li.list-group-item.sortable:first-child');
            }

            if (firstRulesItem.length == 0) {
                currentRuleId = null;
            } else {
                firstRulesItem.click();
            }
        }
    });
}

//Load list of rule properties (patterns, templates, ...)
function updateRuleProperties()
{
    $.ajax({
        url: '/topics/rule',
        type: "POST",
        data: 'ruleId=' + currentRuleId,
        success: function (data) {
            $('#patterns').html(data.rules_patterns);
            $('#templates').html(data.rules_templates);
            $('#excPatterns').html(data.rules_exc_patterns);
            $('#subRules').html(data.rules_subrules);

            //Make subrules sortable (by drag&drop)
            $("#subRules ul").sortable({
                //Get array of subrules (id, priority) before sorting
                start: function () {
                    sortableSubRulesArray = [];
                    $('#subRules ul li.list-group-item.sortable').each(function () {
                        var elem = [];
                        elem['id'] = $(this).find('input[name="subRuleId"]').val();
                        elem['priority'] = $(this).find('input[name="subRulePriority"]').val();
                        //if element is not a 'Add new rule' button
                        if (elem['id'] !== undefined && elem['priority'] !== undefined) {
                            sortableSubRulesArray.push(elem);
                        }
                    });
                },
                //Get array of subrules (id, priority) after sorting
                stop: function () {
                    var changedElements = [];
                    var i = 0;
                    $('#subRules ul li.list-group-item.sortable').each(function () {
                        var elem = {};
                        elem.rule_id = $(this).find('input[name="subRuleId"]').val();
                        elem.priority = $(this).find('input[name="subRulePriority"]').val();
                        //Push elements with changed priority
                        if (elem.priority != sortableSubRulesArray[i]['priority']) {
                            elem.priority = sortableSubRulesArray[i]['priority'];
                            changedElements.push(elem);
                        }
                        i++;
                    });
                    //If we have elements with changed priority make ajax request
                    if (changedElements.length > 0) {
                        $.ajax({
                            url: '/topics/update-rules',
                            type: "POST",
                            data: 'params=' + JSON.stringify(changedElements),
                            success: function (data) {
                                if (data != 'true') {
                                    alert('An error occurred while updating the subrules priority!');
                                }
                            }
                        });
                    }
                }
            });

            //Trigger click on first or current item
            if ($('#currentPatternId').val() != '') {
                var str = 'input[name="patternId"][value="' + $('#currentPatternId').val() + '"]';
                $('#patterns li.list-group-item.sortable').has(str).click();
            } else {
                $('#patterns li.list-group-item.sortable:first-child').click();
            }

            if ($('#currentTemplateId').val() != '') {
                var str = 'input[name="templateId"][value="' + $('#currentTemplateId').val() + '"]';
                $('#templates li.list-group-item.sortable').has(str).click();
            } else {
                $('#templates li.list-group-item.sortable:first-child').click();
            }

            if ($('#currentExcPatternId').val() != '') {
                var str = 'input[name="excPatternId"][value="' + $('#currentExcPatternId').val() + '"]';
                $('#excPatterns li.list-group-item.sortable').has(str).click();
            } else {
                $('#excPatterns li.list-group-item.sortable:first-child').click();
            }

            if ($('#currentSubRuleId').val() != '') {
                var str = 'input[name="subRuleId"][value="' + $('#currentSubRuleId').val() + '"]';
                $('#subRules li.list-group-item.sortable').has(str).click();
            } else {
                var firstSubRulesItem = $('#subRules li.list-group-item.sortable:first-child');
                if (firstSubRulesItem.length == 0) {
                    currentSubRuleId = null;
                } else {
                    firstSubRulesItem.click();
                }
            }
        }
    });
}

//Load list of subrule properties (patterns, templates, ...)
function updateSubRuleProperties()
{
    $.ajax({
        url: '/topics/sub-rule',
        type: "POST",
        data: 'ruleId=' + currentSubRuleId,
        success: function (data) {
            $('#subPatterns').html(data.subrules_patterns);
            $('#subTemplates').html(data.subrules_templates);

            //Trigger click on first or current item
            if ($('#currentSubPatternId').val() != '') {
                var str = 'input[name="patternId"][value="' + $('#currentSubPatternId').val() + '"]';
                $('#subPatterns li.list-group-item.sortable').has(str).click();
            } else {
                $('#subPatterns li.list-group-item.sortable:first-child').click();
            }

            if ($('#currentSubTemplateId').val() != '') {
                var str = 'input[name="templateId"][value="' + $('#currentSubTemplateId').val() + '"]';
                $('#subTemplates li.list-group-item.sortable').has(str).click();
            } else {
                $('#subTemplates li.list-group-item.sortable:first-child').click();
            }
        }
    });
}

//Convert checkboxes values to array
function checkboxesToArray(obj)
{
    var objects = $(obj).find('ul li a input[type="checkbox"]:checked').closest('li').find('input[type="hidden"]');
    var arr = [];
    for (var i = 0; i < objects.size(); i++) {
        arr.push($(objects[i]).val());
    };
    return arr;
};

// Clear variables and regexps on patternsBindableForm
function clearVariablesRegexp()
{
    $('#currentVariableId').val(-1);
    $('#currentVariableName').html('No variable');
    $('#currentVariableRegexpId').val(-1);
    $('#currentRegexpId').val(-1);
    $('#currentRegexpName').html('No regexp');
}

$(document).ready(function () {
    /**********  TOPICS FUNCTIONALITY **********/
    //Load list of topics
    updateTopics();

    //Click on topics item event
    $(document).on('click', '#topics li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        $('#rules').html('');
        $('#patterns').html('');
        $('#excPatterns').html('');
        $('#templates').html('');
        $('#subRules').html('');
        $('#subPatterns').html('');
        $('#subTemplates').html('');
        clearBindableForm('#patternsBindableForm');
        clearVariablesRegexp();
        clearBindableForm('#excPatternsBindableForm');
        clearBindableForm('#templatesBindableForm');
        clearBindableForm('#subRulesBindableForm');
        clearBindableForm('#subPatternsBindableForm');
        clearBindableForm('#subTemplatesBindableForm');
        var topicId = $(this).children('input[name="topicId"]').val();
        if (topicId === undefined) {
            currentTopicId = null;
            return;
        }
        currentTopicId = topicId;
        updateRules();
    });

    //Click topic update button
    $(document).on('click', 'button[name="updateTopicButton"]', function () {
        $('#progressModal').modal('show');
        var parent = $(this).closest('li.list-group-item');
        var checkedScenarios = checkboxesToArray(parent.find('div[name="topicScenarios"]'));
        var checkedTags = checkboxesToArray(parent.find('div[name="topicTags"]'));
        var topicId = parent.find('input[name="topicId"]').val();
        var topicLanguageTag = parent.find('input[name="topicLanguageTag"]').val();
        var topicIsExclude = parent.find('input[name="topicIsExclude"]').val();
        var params = {};
        params['name'] = $.trim(parent.find('input[name="name"]').val());
        params['name'] = encodeURIComponent(params['name']);
        params['checkedScenarios'] = checkedScenarios;
        params['checkedTags'] = checkedTags;
        params['lang'] = topicLanguageTag;
        params['isExclude'] = topicIsExclude;
        params['countTriggering'] = $.trim(parent.find('input[name="countTriggering"]').val());
        $.ajax({
            url: '/topics/update-topic',
            type: "POST",
            data: 'topicId=' + topicId + '&params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    parent.find('span[name="topicName"]').html(decodeURIComponent(params['name']));
                } else {
                    alert('An error occurred while updating the topic!');
                }
            }
        });
    });

    //Supported function for topics radiobuttons click methods
    function clickTopicRadioButton(topicId, glyphicon, paramName)
    {
        $('#progressModal').modal('show');
        var params = {};
        if (glyphicon.hasClass('active')) {
            params[paramName] = 0;
        } else {
            params[paramName] = 1;
        }
        $.ajax({
            url: '/topics/update-topic',
            type: "POST",
            data: 'topicId=' + topicId + '&params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    (params[paramName] == 1) ? glyphicon.addClass('active') : glyphicon.removeClass('active');
                } else {
                    alert('An error occurred while updating the topic!');
                }
            }
        });
    }

    //Click topic update auto/human radiobutton
    $(document).on('click', 'button[name="updateTopicsHumAutoRadioButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var topicId = parent.find('input[name="topicId"]').val();
        var glyphicon = parent.find('span[name="topicsHumAuto"]');
        clickTopicRadioButton(topicId, glyphicon, 'switching');
    });

    //Click topic update active/test radiobutton
    $(document).on('click', 'button[name="updateTopicsActiveTestRadioButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var topicId = parent.find('input[name="topicId"]').val();
        var glyphicon = parent.find('span[name="topicsActiveTest"]');
        clickTopicRadioButton(topicId, glyphicon, 'active');
    });

    //Click topic update on/off radiobutton
    $(document).on('click', 'button[name="updateTopicsOnOffRadioButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var topicId = parent.find('input[name="topicId"]').val();
        var glyphicon = parent.find('span[name="topicsOnOff"]');
        clickTopicRadioButton(topicId, glyphicon, 'enabled');
    });

    //Click topic delete button
    $(document).on('click', 'button[name="deleteTopicButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var topicId = parent.find('input[name="topicId"]').val();
        var topicName = parent.find('input[name="name"]').val();
        $('#deleteTopicId').val(topicId);
        $('#deleteTopicName').html(topicName);
        $('#confirmDeleteTopicModal').modal('show');
    });

    //Click confirm topic delete button
    $(document).on('click', '#confirmDeleteTopicButton', function () {
        var topicId = $('#deleteTopicId').val();
        $.ajax({
            url: '/topics/delete-topic',
            type: "POST",
            data: 'topicId=' + topicId,
            success: function (data) {
                if (data == 'true') {
                    updateTopics();
                } else {
                    alert('An error occurred while deleting the topic!');
                }
                $('#confirmDeleteTopicModal').modal('hide');
            }
        });
    });

    //Disable 'Add' button when topic name is empty
    $(document).on('input', '#addNewTopicName', function () {
        if ($(this).val() === '') {
            $('#addNewTopicButton').attr('disabled', true);
        } else {
            $('#addNewTopicButton').attr('disabled', false);
        }
    });

    //Click 'Add new topic' button
    $(document).on('click', '#addNewTopicButton', function () {
        var topicName = $.trim($('#addNewTopicName').val());
        topicName = encodeURIComponent(topicName);
        $.ajax({
            url: '/topics/create-topic',
            type: "POST",
            data: 'name=' + topicName,
            success: function (data) {
                if (data == 'true') {
                    updateTopics();
                } else {
                    alert('An error occurred while creating the topic!');
                }
                $('#addNewTopicModal').modal('hide');
            }
        });
    });

    //Click "Check all"
    $(document).on('click', 'ul li.checkAll', function () {
        var isChecked = ($(this).siblings('li').first().find('a input[type="checkbox"]:checked').length > 0);
        $(this).siblings('li').find('a input[type="checkbox"]').prop('checked', !isChecked);
    });

    //Click paste rule button
    $(document).on('click', 'button[name="pasteRuleButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var topicId = parent.find('input[name="topicId"]').val();
        if (ruleIdToCopy == false) {
            return;
        }
        $.ajax({
            url: '/topics/paste-rule',
            type: "POST",
            data: 'ruleId=' + ruleIdToCopy + '&topicId=' + topicId,
            success: function (data) {
                if (data == 'true') {
                    updateRules();
                } else {
                    alert('An error occurred while pasting the rule!');
                }
            }
        });
    });

    // Click on available languages dropdown
    $(document).on('click', '#topics div[name="topicLanguage"] li', function () {
        var val     = $(this).find('input').val();
        var name    = $(this).find('a').html();
        $(this).closest('div').find('span[name="languageTag"]').html(name);
        $(this).closest('div').closest('li').find('input[name="topicLanguageTag"]').val(val);
    });

    // Click on available IsExclude dropdown
    $(document).on('click', '#topics div[name="topicIsExclude"] li', function () {
        var val     = $(this).find('input').val();
        var name    = $(this).find('a').html();
        $(this).closest('div').find('span[name="isExclude"]').html(name);
        $(this).closest('div').closest('li').find('input[name="topicIsExclude"]').val(val);
    });

    /**********  RULES FUNCTIONALITY **********/
    //Click on rules item event
    $(document).on('click', '#rules li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        $('#patterns').html('');
        $('#excPatterns').html('');
        $('#templates').html('');
        $('#subRules').html('');
        $('#subPatterns').html('');
        $('#subTemplates').html('');
        clearBindableForm('#patternsBindableForm');
        clearVariablesRegexp();
        clearBindableForm('#excPatternsBindableForm');
        clearBindableForm('#templatesBindableForm');
        clearBindableForm('#subRulesBindableForm');
        clearBindableForm('#subPatternsBindableForm');
        clearBindableForm('#subTemplatesBindableForm');
        var ruleId = $(this).children('input[name="ruleId"]').val();
        if (ruleId === undefined) {
            currentRuleId = null;
            return;
        }
        currentRuleId = ruleId;
        updateRuleProperties();
    });

    //Click rule update button
    $(document).on('click', 'button[name="updateRuleButton"]', function () {
        $('#progressModal').modal('show');
        var parent = $(this).closest('li.list-group-item');
        var ruleId = parent.find('input[name="ruleId"]').val();
        var params = {};
        params['name'] = $.trim(parent.find('input[name="name"]').val());
        params['name'] = encodeURIComponent(params['name']);
        params['exec_limit'] = $.trim(parent.find('input[name="executionLimit"]').val());
        params['exec_limit'] = encodeURIComponent(params['exec_limit']);
        $.ajax({
            url: '/topics/update-rule',
            type: "POST",
            data: 'ruleId=' + ruleId + '&params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    parent.find('span[name="ruleName"]').html(decodeURIComponent(params['name']));
                    parent.find('span[name="executionLimit"]').html(decodeURIComponent(params['exec_limit']));
                } else {
                    alert('An error occurred while updating the rule!');
                }
            }
        });
    });

    //Disable 'Add' button when rule name is empty
    $(document).on('input', '#addNewRuleName', function () {
        if ($(this).val() === '') {
            $('#addNewRuleButton').attr('disabled', true);
        } else {
            $('#addNewRuleButton').attr('disabled', false);
        }
    });

    //Click 'Add new rule' button
    $(document).on('click', '#addNewRuleButton', function () {
        var ruleName = $.trim($('#addNewRuleName').val());
        ruleName = encodeURIComponent(ruleName);
        $.ajax({
            url: '/topics/create-rule',
            type: "POST",
            data: 'name=' + ruleName + '&topicId=' + currentTopicId,
            success: function (data) {
                if (data == 'true') {
                    updateRules();
                } else {
                    alert('An error occurred while creating the rule!');
                }
                $('#addNewRuleModal').modal('hide');
            }
        });
    });

    //Supported function for rules radiobuttons click methods
    function clickRuleRadioButton(ruleId, glyphicon, paramName)
    {
        $('#progressModal').modal('show');
        var params = {};
        if (glyphicon.hasClass('active')) {
            params[paramName] = 0;
        } else {
            params[paramName] = 1;
        }
        $.ajax({
            url: '/topics/update-rule',
            type: "POST",
            data: 'ruleId=' + ruleId + '&params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    (params[paramName] == 1) ? glyphicon.addClass('active') : glyphicon.removeClass('active');
                } else {
                    alert('An error occurred while updating the rule!');
                }
            }
        });
    }

    //Click topic rule active/test radiobutton
    $(document).on('click', 'button[name="updateRulesActiveTestRadioButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var ruleId = parent.find('input[name="ruleId"]').val();
        var glyphicon = parent.find('span[name="rulesActiveTest"]');
        clickRuleRadioButton(ruleId, glyphicon, 'active');
    });

    //Click rule update on/off radiobutton
    $(document).on('click', 'button[name="updateRulesOnOffRadioButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var ruleId = parent.find('input[name="ruleId"]').val();
        var glyphicon = parent.find('span[name="rulesOnOff"]');
        clickRuleRadioButton(ruleId, glyphicon, 'enabled');
    });

    //Click rule update exc/inc radiobutton
    $(document).on('click', 'button[name="updateRulesExcIncRadioButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var ruleId = parent.find('input[name="ruleId"]').val();
        var glyphicon = parent.find('span[name="rulesExcInc"]');
        clickRuleRadioButton(ruleId, glyphicon, 'exclude_template');
    });

    //Click rule delete button
    $(document).on('click', 'button[name="deleteRuleButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        var topicId = parent.find('input[name="topicId"]').val();
        var ruleId = parent.find('input[name="ruleId"]').val();
        var ruleName = parent.find('input[name="name"]').val();
        $('#deleteRuleTopicId').val(topicId);
        $('#deleteRuleId').val(ruleId);
        $('#deleteRuleName').html(ruleName);
        $('#confirmDeleteRuleModal').modal('show');
    });

    //Click confirm topic delete button
    $(document).on('click', '#confirmDeleteRuleButton', function () {
        var topicId = $('#deleteRuleTopicId').val();
        var ruleId = $('#deleteRuleId').val();
        $.ajax({
            url: '/topics/delete-rule',
            type: "POST",
            data: 'ruleId=' + ruleId + '&topicId=' + topicId,
            success: function (data) {
                if (data == 'true') {
                    updateRules();
                } else {
                    alert('An error occurred while deleting the rule!');
                }
                $('#confirmDeleteRuleModal').modal('hide');
            }
        });
    });

    //Click copy rule button
    $(document).on('click', 'button[name="copyRuleButton"]', function () {
        var parent = $(this).closest('li.list-group-item');
        ruleIdToCopy = parent.find('input[name="ruleId"]').val();
    });

    /**********  RULE PATTERNS FUNCTIONALITY **********/
    //Click check regular expression button
    $(document).on('click', '#checkRegularExpressionButton', function () {
        var text = $('#regularExpressionText').val();
        var pattern = $('#regularExpressionPattern').val();
        var isMatch = text.match(pattern);
        if (isMatch !== null) {
            $('#regularExpressionCheckModal .modal-body strong').html('<p class="text-success">Success. The regular expression and string entered is match.</p>');
        } else {
            $('#regularExpressionCheckModal .modal-body strong').html('<p class="text-danger">Error. The regular expression and string entered do not match.</p>');
        }
        $('#regularExpressionCheckModal').modal('show');
    });

    //Click copy patterns button
    $(document).on('click', '#copyPatternsButton', function () {
        ruleIdFromCopy      = $('#rules #rulesList li.current').find('input[name="ruleId"]').val();
        patternsIdsToCopy   = getCheckedOrCurrentObjectsIds('patternsBindableForm', 'patternId', 'patterns', 'patternId');
    });

    //Type text in pattern in regular expression pattern or text
    $(document).on('keyup', '#regularExpressionPattern, #regularExpressionText', function () {
        var text = $('#regularExpressionText').val();
        var pattern = $('#regularExpressionPattern').val();
        var isMatch = text.match(pattern);
        if (isMatch !== null) {
            $('#addNewPatternButton').removeAttr("disabled");
            $('#updatePatternButton').removeAttr("disabled");
        } else {
            $('#addNewPatternButton').attr("disabled", true);
            $('#updatePatternButton').attr("disabled", true);
        }
    });

    //Click on pattern item event
    $(document).on('click', '#patterns li.list-group-item', function () {
        $('#addNewPatternButton').removeAttr("disabled");
        $('#updatePatternButton').removeAttr("disabled");
    });

    //Click 'Add new pattern' button
    $(document).on('click', '#addNewPatternButton', function () {
        $('#progressModal').modal('show');
        var test = $('#regularExpressionText').val();
        var text = $('#regularExpressionPattern').val();
        test = encodeURIComponent(test);
        text = encodeURIComponent(text);
        $.ajax({
            url: '/topics/create-pattern',
            type: "POST",
            data: 'ruleId=' + currentRuleId + '&test=' + test + '&text=' + text,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while creating the pattern!');
                }
            }
        });
    });

    //Click 'Delete pattern' button
    $(document).on('click', '#deletePatternButton', function () {
        $('#progressModal').modal('show');
        var patternsIds = getCheckedOrCurrentObjectsIds('patternsBindableForm', 'patternId', 'patterns', 'patternId');
        if (patternsIds.length == 0) {
            $('#progressModal').modal('hide');
            return;
        }
        var params = [];
        $.each(patternsIds, function (index, value) {
            var elem = {};
            elem.pattern_id = value;
            params.push(elem);
        });
        $.ajax({
            url: '/topics/delete-patterns',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while deleting the patterns!');
                }
            }
        });
    });

    //Click 'Update pattern' button
    $(document).on('click', '#updatePatternButton', function () {
        $('#progressModal').modal('show');
        var test = $('#regularExpressionText').val();
        var text = $('#regularExpressionPattern').val();
        test = encodeURIComponent(test);
        text = encodeURIComponent(text);
        var patternId = $('#patternsBindableForm input[name="patternId"]').val();
        $.ajax({
            url: '/topics/update-pattern',
            type: "POST",
            data: 'patternId=' + patternId + '&test=' + test + '&text=' + text,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while updating the pattern!');
                }
            }
        });
    });

    //Click pattern active/test radiobutton
    $(document).on('click', '#updatePatternActiveTestRadioButton', function () {
        //Check if this is real click (not bindable click from clearBindableForm or bindableFormItemClick functions)
        if ($(this).hasClass('active') != $('#patterns').find('ul li.current span[name="activePatternIcon"]').hasClass('active')) {
            $('#progressModal').modal('show');
            var patternsIds = getCheckedOrCurrentObjectsIds('patternsBindableForm', 'patternId', 'patterns', 'patternId');
            if (patternsIds.length == 0) {
                $('#progressModal').modal('hide');
                return;
            }
            var active = 0;
            if ($(this).hasClass('active')) {
                active = 1;
            }
            var params = [];
            $.each(patternsIds, function (index, value) {
                var elem = {};
                elem.pattern_id = value;
                elem.active = active;
                params.push(elem);
            });
            $.ajax({
                url: '/topics/update-patterns',
                type: "POST",
                data: 'params=' + JSON.stringify(params),
                success: function (data) {
                    $('#progressModal').modal('hide');
                    if (data == 'true') {
                        updateRuleProperties();
                    } else {
                        alert('An error occurred while updating the patterns!');
                    }
                }
            });
        }
    });

    //Click check all patterns button
    $(document).on('click', '#checkAllPatternsButton', function () {
        if (allPatternsItemsWasChecked) {
            allPatternsItemsWasChecked = false;
            $('#patternsContainer li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allPatternsItemsWasChecked = true;
            $('#patternsContainer li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });

    // Click on available variables dropdown
    $(document).on('click', '#availableVariables li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#currentVariableId').val(val);
        $('#currentVariableName').html(name);
    });

    // Click on available regexp dropdown
    $(document).on('click', '#availableRegexp li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#currentRegexpId').val(val);
        $('#currentRegexpName').html(name);
    });

    $(document).on('click', '#patterns li', function () {
        $('#currentPatternId').val($(this).find('input[name="patternId"]').val());
        // Set corresponding variable in dropdown
        var currentVariableId = $(this).find('input[name="currentVariableId"]').val();
        var currentVariableName = $(this).find('input[name="currentVariableName"]').val();
        var currentVariableRegexpId = $(this).find('input[name="currentVariableRegexpId"]').val();
        $('#currentVariableId').val(currentVariableId);
        $('#currentVariableName').html(currentVariableName);
        $('#currentVariableRegexpId').val(currentVariableRegexpId);

        // Set available regexp in dropdown
        var testText = $(this).find('input[name="testText"]').val();
        var itemText = $(this).find('span[name="itemText"]').html();
        var arr = (new RegExp(itemText)).exec(testText);
        var currentRegexpId = $(this).find('input[name="currentRegexpId"]').val();
        var items = '<li><a href="javascript:void(0);">No regexp</a> <input type="hidden" value="-1"></li>';
        $('#currentRegexpId').val(-1);
        $('#currentRegexpName').html('No regexp');
        for(var i = 0; i < arr.length; i++) {
            items += '<li><a href="javascript:void(0);">';
            items += arr[i];
            items += '</a> <input type="hidden" value="';
            items += i;
            items += '"></li>';
            // Set corresponding regexp in dropdown
            if (i == currentRegexpId) {
                $('#currentRegexpId').val(currentRegexpId);
                $('#currentRegexpName').html(arr[i]);
            }
        }
        $('#availableRegexp').html(items);
    });

    //Click 'Update pattern variable' button
    $(document).on('click', '#updatePatternVariable', function () {
        var patternId = $('#patternsBindableForm input[name="patternId"]').val();
        var variableId = $('#currentVariableId').val();
        var regexpId = $('#currentRegexpId').val();
        var variableRegexpId = $('#currentVariableRegexpId').val();
        if (variableRegexpId == -1) {
            if (variableId != -1 && regexpId != -1) {
                $.ajax({
                    url: '/topics/create-pattern-variable',
                    type: "POST",
                    data: 'patternId=' + patternId + '&variableId=' + variableId + '&regexpId=' + regexpId,
                    success: function (data) {
                        if (data == 'true') {
                            updateRuleProperties();
                        } else {
                            alert('An error occurred while updating the pattern!');
                        }
                    }
                });
            }
        } else {
            if (variableId != -1 && regexpId != -1) {
                $.ajax({
                    url: '/topics/update-pattern-variable',
                    type: "POST",
                    data: 'patternVariableId=' + variableRegexpId + '&patternId=' + patternId + '&variableId=' + variableId + '&regexpId=' + regexpId,
                    success: function (data) {
                        if (data == 'true') {
                            updateRuleProperties();
                        } else {
                            alert('An error occurred while updating the pattern!');
                        }
                    }
                });
            } else {
                $.ajax({
                    url: '/topics/delete-pattern-variable',
                    type: "POST",
                    data: 'patternVariableId=' + variableRegexpId,
                    success: function (data) {
                        if (data == 'true') {
                            updateRuleProperties();
                        } else {
                            alert('An error occurred while updating the pattern!');
                        }
                    }
                });
            }
        }
    });

    /**********  RULE TEMPLATES FUNCTIONALITY **********/
    //Click 'Add new template' button
    $(document).on('click', '#addNewTemplateButton', function () {
        $('#progressModal').modal('show');
        var text = $('#templateText').val();
        text = text.replace(/"/g, '\\"');
        var lines = text.split("\n");
        var elements = [];
        for (var i = 0; i < lines.length; i++) {
            var elem = {};
            elem.rule_id = currentRuleId;
            elem.text = encodeURIComponent(lines[i]);
            if (elem.text != "") {
                elements.push(elem);
            }
        }
        if (elements.length < 1) {
            $('#progressModal').modal('hide');
            return;
        }

        $.ajax({
            url: '/topics/create-templates',
            type: "POST",
            data: 'params=' + JSON.stringify(elements),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while creating the template!');
                }
            }
        });
    });

    //Click 'Delete template' button
    $(document).on('click', '#deleteTemplateButton', function () {
        $('#progressModal').modal('show');
        /*var templateId = $('#templatesBindableForm input[name="templateId"]').val();
        $.ajax({
            url: '/topics/delete-template',
            type: "POST",
            data: 'templateId=' + templateId,
            success: function (data) {
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while deleting the template!');
                }
            }
        });*/
        var templatesIds = getCheckedOrCurrentObjectsIds('templatesBindableForm', 'templateId', 'templates', 'templateId');
        if (templatesIds.length == 0) {
            $('#progressModal').modal('hide');
            return;
        }
        var params = [];
        $.each(templatesIds, function (index, value) {
            var elem = {};
            elem.template_id = value;
            params.push(elem);
        });
        $.ajax({
            url: '/topics/delete-templates',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while deleting the templates!');
                }
            }
        });
        /*WebClient.getInstance().get(
            '/topics/delete-templates',
            'params=' + JSON.stringify(params),
            updateRuleProperties,
            'An error occurred while deleting the templates!'
        );*/
    });

    //Click 'Update template' button
    $(document).on('click', '#updateTemplateButton', function () {
        $('#progressModal').modal('show');
        var text = $('#templateText').val();
        text = encodeURIComponent(text);
        var templateId = $('#templatesBindableForm input[name="templateId"]').val();
        var timeslotId = $('#templateTimeslot').val();

        $.ajax({
            url: '/topics/update-template',
            type: "POST",
            data: 'templateId=' + templateId + '&text=' + text + '&timeslotId=' + timeslotId,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while updating the template!');
                }
            }
        });
    });

    //Click check all templates button
    $(document).on('click', '#checkAllTemplatesButton', function () {
        if (allTemplatesItemsWasChecked) {
            allTemplatesItemsWasChecked = false;
            $('#templatesContainer li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allTemplatesItemsWasChecked = true;
            $('#templatesContainer li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });

    $(document).on('click', '#templates li', function () {
        var templateId = $(this).find('input[name="templateId"]').val();
        var timeslotId = $(this).find('input[name="timeslotId"]').val();

        var timeSlotSelect = $('#templateTimeslot');

        $('#currentTemplateId').val(templateId);

        timeSlotSelect.val(timeslotId);


        console.log(timeslotId);
        console.log('click');
    });

    /**********  RULE EXCEPTION PATTERNS FUNCTIONALITY **********/
    //Click check regular expression button
    $(document).on('click', '#checkExcRegularExpressionButton', function () {
        var text = $('#excRegularExpressionText').val();
        var pattern = $('#excRegularExpressionPattern').val();
        var isMatch = text.match(pattern);
        if (isMatch !== null) {
            $('#excRegularExpressionCheckModal .modal-body strong').html('<p class="text-success">Success. The regular expression and string entered is match.</p>');
        } else {
            $('#excRegularExpressionCheckModal .modal-body strong').html('<p class="text-danger">Error. The regular expression and string entered do not match.</p>');
        }
        $('#excRegularExpressionCheckModal').modal('show');
    });

    //Type text in exception pattern in regular expression pattern or text
    $(document).on('keyup', '#excRegularExpressionPattern, #excRegularExpressionText', function () {
        var text = $('#excRegularExpressionText').val();
        var pattern = $('#excRegularExpressionPattern').val();
        var isMatch = text.match(pattern);
        if (isMatch !== null) {
            $('#addNewExcPatternButton').removeAttr("disabled");
        } else {
            $('#addNewExcPatternButton').attr("disabled", true);
        }
    });

    //Click on exception pattern item event
    $(document).on('click', '#excPatterns li.list-group-item', function () {
        $('#currentExcPatternId').val($(this).find('input[name="excPatternId"]').val());
        $('#addNewExcPatternButton').removeAttr("disabled");
    });

    //Click 'Add new exception pattern' button
    $(document).on('click', '#addNewExcPatternButton', function () {
        $('#progressModal').modal('show');
        var test = $('#excRegularExpressionText').val();
        var text = $('#excRegularExpressionPattern').val();
        test = encodeURIComponent(test);
        text = encodeURIComponent(text);
        $.ajax({
            url: '/topics/create-exc-pattern',
            type: "POST",
            data: 'ruleId=' + currentRuleId + '&test=' + test + '&text=' + text,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while creating the exception pattern!');
                }
            }
        });
    });

    //Click 'Delete exception pattern' button
    $(document).on('click', '#deleteExcPatternButton', function () {
        $('#progressModal').modal('show');
        var patternsIds = getCheckedOrCurrentObjectsIds('excPatternsBindableForm', 'excPatternId', 'excPatterns', 'excPatternId');
        if (patternsIds.length == 0) {
            $('#progressModal').modal('hide');
            return;
        }
        var params = [];
        $.each(patternsIds, function (index, value) {
            var elem = {};
            elem.exception_pattern_id = value;
            params.push(elem);
        });
        $.ajax({
            url: '/topics/delete-exc-patterns',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while deleting the exception patterns!');
                }
            }
        });
    });

    //Click 'Update exception pattern' button
    $(document).on('click', '#updateExcPatternButton', function () {
        $('#progressModal').modal('show');
        var test = $('#excRegularExpressionText').val();
        var text = $('#excRegularExpressionPattern').val();
        test = encodeURIComponent(test);
        text = encodeURIComponent(text);
        var patternId = $('#excPatternsBindableForm input[name="excPatternId"]').val();
        $.ajax({
            url: '/topics/update-exc-pattern',
            type: "POST",
            data: 'patternId=' + patternId + '&test=' + test + '&text=' + text,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while updating the exception pattern!');
                }
            }
        });
    });

    //Click exception pattern active/test radiobutton
    $(document).on('click', '#updateExcPatternActiveTestRadioButton', function () {
        //Check if this is real click (not bindable click from clearBindableForm or bindableFormItemClick functions)
        if ($(this).hasClass('active') != $('#excPatterns').find('ul li.current span[name="activeExcPatternIcon"]').hasClass('active')) {
            $('#progressModal').modal('show');
            var patternsIds = getCheckedOrCurrentObjectsIds('excPatternsBindableForm', 'excPatternId', 'excPatterns', 'excPatternId');
            if (patternsIds.length == 0) {
                $('#progressModal').modal('hide');
                return;
            }
            var active = 0;
            if ($(this).hasClass('active')) {
                active = 1;
            }
            var params = [];
            $.each(patternsIds, function (index, value) {
                var elem = {};
                elem.exception_pattern_id = value;
                elem.active = active;
                params.push(elem);
            });
            $.ajax({
                url: '/topics/update-exc-patterns',
                type: "POST",
                data: 'params=' + JSON.stringify(params),
                success: function (data) {
                    $('#progressModal').modal('hide');
                    if (data == 'true') {
                        updateRuleProperties();
                    } else {
                        alert('An error occurred while updating the exception patterns!');
                    }
                }
            });
        }
    });

    /**********  SUB RULES FUNCTIONALITY **********/
    //Click on subrules item event
    $(document).on('click', '#subRules li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        $('#subPatterns').html('');
        $('#subTemplates').html('');
        clearBindableForm('#subPatternsBindableForm');
        clearBindableForm('#subTemplatesBindableForm');
        var subRuleId = $(this).children('input[name="subRuleId"]').val();
        if (subRuleId === undefined) {
            currentSubRuleId = null;
            return;
        }
        currentSubRuleId = subRuleId;
        $('#currentSubRuleId').val($(this).find('input[name="subRuleId"]').val());
        updateSubRuleProperties();
    });

    //Click 'Add new subrule' button
    $(document).on('click', '#addNewSubRulesButton', function () {
        $('#progressModal').modal('show');
        var parentId = currentRuleId;
        var name = $.trim($('#subRulesText').val());
        name = encodeURIComponent(name);
        $.ajax({
            url: '/topics/create-sub-rule',
            type: "POST",
            data: 'parentId=' + parentId + '&name=' + name,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while creating the sub rule!');
                }
            }
        });
    });

    //Click 'Update subrule' button
    $(document).on('click', '#updateSubRulesButton', function () {
        $('#progressModal').modal('show');
        var parent = $(this).closest('form');
        var subRuleId = parent.find('input[name="subRuleId"]').val();
        if (subRuleId == '') {
            $('#progressModal').modal('hide');
            return;
        }
        var params = {};
        params['name'] = $.trim($('#subRulesText').val());
        params['name'] = encodeURIComponent(params['name']);
        params['exec_limit'] = $.trim($('#subRulesExecution').val());
        params['exec_limit'] = encodeURIComponent(params['exec_limit']);
        $.ajax({
            url: '/topics/update-rule',
            type: "POST",
            data: 'ruleId=' + subRuleId + '&params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while updating the subrule!');
                }
            }
        });
    });

    //Click subrules active/test radiobutton
    $(document).on('click', '#updateSubRulesActiveTestRadioButton', function () {
        //Check if this is real click (not bindable click from clearBindableForm or bindableFormItemClick functions)
        if ($(this).hasClass('active') != $('#subRules').find('ul li.current span[name="activeSubRulesIcon"]').hasClass('active')) {
            $('#progressModal').modal('show');
            var active = $(this).hasClass('active');
            var parent = $(this).closest('form');
            var subRuleId = parent.find('input[name="subRuleId"]').val();
            var params = {};
            if (active) {
                params['active'] = 1;
            } else {
                params['active'] = 0;
            }
            $.ajax({
                url: '/topics/update-rule',
                type: "POST",
                data: 'ruleId=' + subRuleId + '&params=' + JSON.stringify(params),
                success: function (data) {
                    $('#progressModal').modal('hide');
                    if (data == 'true') {
                        updateRuleProperties();
                    } else {
                        alert('An error occurred while updating the sub rule!');
                    }
                }
            });
        }
    });

    //Click subrules on/off radiobutton
    $(document).on('click', '#updateSubRulesOnOffRadioButton', function () {
        //Check if this is real click (not bindable click from clearBindableForm or bindableFormItemClick functions)
        if ($(this).hasClass('active') != $('#subRules').find('ul li.current span[name="onSubRulesIcon"]').hasClass('active')) {
            $('#progressModal').modal('show');
            var active = $(this).hasClass('active');
            var parent = $(this).closest('form');
            var subRuleId = parent.find('input[name="subRuleId"]').val();
            var params = {};
            if (active) {
                params['enabled'] = 1;
            } else {
                params['enabled'] = 0;
            }
            $.ajax({
                url: '/topics/update-rule',
                type: "POST",
                data: 'ruleId=' + subRuleId + '&params=' + JSON.stringify(params),
                success: function (data) {
                    $('#progressModal').modal('hide');
                    if (data == 'true') {
                        updateRuleProperties();
                    } else {
                        alert('An error occurred while updating the sub rule!');
                    }
                }
            });
        }
    });

    //Click subrules exc/inc radiobutton
    $(document).on('click', '#updateSubRulesExcIncRadioButton', function () {
        //Check if this is real click (not bindable click from clearBindableForm or bindableFormItemClick functions)
        if ($(this).hasClass('active') != $('#subRules').find('ul li.current span[name="excSubRulesIcon"]').hasClass('active')) {
            $('#progressModal').modal('show');
            var active = $(this).hasClass('active');
            var parent = $(this).closest('form');
            var subRuleId = parent.find('input[name="subRuleId"]').val();
            var params = {};
            if (active) {
                params['exclude_template'] = 1;
            } else {
                params['exclude_template'] = 0;
            }
            $.ajax({
                url: '/topics/update-rule',
                type: "POST",
                data: 'ruleId=' + subRuleId + '&params=' + JSON.stringify(params),
                success: function (data) {
                    $('#progressModal').modal('hide');
                    if (data == 'true') {
                        updateRuleProperties();
                    } else {
                        alert('An error occurred while updating the sub rule!');
                    }
                }
            });
        }
    });

    //Click subrule delete button
    $(document).on('click', '#deleteSubRulesButton', function () {
        var parent = $(this).closest('form');
        var subRuleId = parent.find('input[name="subRuleId"]').val();
        if (subRuleId == '') {
            return;
        }
        var subRuleName = $('#subRulesText').val();
        $('#deleteSubRuleId').val(subRuleId);
        $('#deleteSubRuleName').html(subRuleName);
        $('#confirmDeleteSubRuleModal').modal('show');
    });

    //Click confirm subrule delete button
    $(document).on('click', '#confirmDeleteSubRuleButton', function () {
        var subRuleId = $('#deleteSubRuleId').val();
        $.ajax({
            url: '/topics/delete-sub-rule',
            type: "POST",
            data: 'ruleId=' + subRuleId,
            success: function (data) {
                if (data == 'true') {
                    updateRuleProperties();
                } else {
                    alert('An error occurred while deleting the subrule!');
                }
                $('#confirmDeleteSubRuleModal').modal('hide');
            }
        });
    });

    /**********  SUBRULE PATTERNS FUNCTIONALITY **********/
    //Click check regular expression button
    $(document).on('click', '#subCheckRegularExpressionButton', function () {
        var text = $('#subRegularExpressionText').val();
        var pattern = $('#subRegularExpressionPattern').val();
        var isMatch = text.match(pattern);
        if (isMatch !== null) {
            $('#subRegularExpressionCheckModal .modal-body strong').html('<p class="text-success">Success. The regular expression and string entered is match.</p>');
        } else {
            $('#subRegularExpressionCheckModal .modal-body strong').html('<p class="text-danger">Error. The regular expression and string entered do not match.</p>');
        }
        $('#subRegularExpressionCheckModal').modal('show');
    });

    //Click paste patterns button
    $(document).on('click', '#pastePatternsButton', function () {
        var subRuleId = $('#subRulesBindableForm').find('input[name="subRuleId"]').val();
        if (ruleIdFromCopy == false || subRuleId == false || patternsIdsToCopy.length == 0) {
            return;
        }
        $.ajax({
            url: '/topics/paste-patterns',
            type: "POST",
            data: 'ruleIdFromCopy=' + ruleIdFromCopy + '&subRuleId=' + subRuleId + '&patternsIds=' + JSON.stringify(patternsIdsToCopy),
            success: function (data) {
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while pasting the patterns!');
                }
            }
        });
    });

    //Type text in sub pattern in regular expression pattern or text
    $(document).on('keyup', '#subRegularExpressionPattern, #subRegularExpressionText', function () {
        var text = $('#subRegularExpressionText').val();
        var pattern = $('#subRegularExpressionPattern').val();
        var isMatch = text.match(pattern);
        if (isMatch !== null) {
            $('#subAddNewPatternButton').removeAttr("disabled");
            $('#subUpdatePatternButton').removeAttr("disabled");
        } else {
            $('#subAddNewPatternButton').attr("disabled", true);
            $('#subUpdatePatternButton').attr("disabled", true);
        }
    });

    //Click on sub pattern item event
    $(document).on('click', '#subPatterns li.list-group-item', function () {
        $('#currentSubPatternId').val($(this).find('input[name="patternId"]').val());
        $('#subAddNewPatternButton').removeAttr("disabled");
        $('#subUpdatePatternButton').removeAttr("disabled");
    });

    //Click 'Add new pattern' button
    $(document).on('click', '#subAddNewPatternButton', function () {
        $('#progressModal').modal('show');
        var test = $('#subRegularExpressionText').val();
        var text = $('#subRegularExpressionPattern').val();
        test = encodeURIComponent(test);
        text = encodeURIComponent(text);
        $.ajax({
            url: '/topics/create-pattern',
            type: "POST",
            data: 'ruleId=' + currentSubRuleId + '&test=' + test + '&text=' + text,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while creating the pattern!');
                }
            }
        });
    });

    //Click 'Delete pattern' button
    $(document).on('click', '#subDeletePatternButton', function () {
        /*var patternId = $('#subPatternsBindableForm input[name="patternId"]').val();
        $.ajax({
            url: '/topics/delete-pattern',
            type: "POST",
            data: 'patternId=' + patternId,
            success: function (data) {
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while deleting the pattern!');
                }
            }
        });*/
        $('#progressModal').modal('show');
        var patternsIds = getCheckedOrCurrentObjectsIds('subPatternsBindableForm', 'patternId', 'subPatterns', 'patternId');
        if (patternsIds.length == 0) {
            $('#progressModal').modal('hide');
            return;
        }
        var params = [];
        $.each(patternsIds, function (index, value) {
            var elem = {};
            elem.pattern_id = value;
            params.push(elem);
        });
        $.ajax({
            url: '/topics/delete-patterns',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while deleting the patterns!');
                }
            }
        });
    });

    //Click 'Update pattern' button
    $(document).on('click', '#subUpdatePatternButton', function () {
        $('#progressModal').modal('show');
        var test = $('#subRegularExpressionText').val();
        var text = $('#subRegularExpressionPattern').val();
        test = encodeURIComponent(test);
        text = encodeURIComponent(text);
        var patternId = $('#subPatternsBindableForm input[name="patternId"]').val();
        $.ajax({
            url: '/topics/update-pattern',
            type: "POST",
            data: 'patternId=' + patternId + '&test=' + test + '&text=' + text,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while updating the pattern!');
                }
            }
        });
    });

    //Click pattern active/test radiobutton
    $(document).on('click', '#subUpdatePatternActiveTestRadioButton', function () {
        //Check if this is real click (not bindable click from clearBindableForm or bindableFormItemClick functions)
        if ($(this).hasClass('active') != $('#subPatterns').find('ul li.current span[name="activePatternIcon"]').hasClass('active')) {
            $('#progressModal').modal('show');
            /*var patternId = $('#subPatternsBindableForm').find('input[name="patternId"]').val();
            var glyphicon = $('#subPatterns').find('ul li.current span[name="activePatternIcon"]');
            var params = {};
            if (glyphicon.hasClass('active')) {
                params['active'] = 0;
            } else {
                params['active'] = 1;
            }
            $.ajax({
                url: '/topics/update-pattern-params',
                type: "POST",
                data: 'patternId=' + patternId + '&params=' + JSON.stringify(params),
                success: function (data) {
                    if (data == 'true') {
                        (params['active'] == 1) ? glyphicon.addClass('active') : glyphicon.removeClass('active');
                    } else {
                        alert('An error occurred while updating the pattern!');
                    }
                }
            });*/
            var patternsIds = getCheckedOrCurrentObjectsIds('subPatternsBindableForm', 'patternId', 'subPatterns', 'patternId');
            if (patternsIds.length == 0) {
                $('#progressModal').modal('hide');
                return;
            }
            var active = 0;
            if ($(this).hasClass('active')) {
                active = 1;
            }
            var params = [];
            $.each(patternsIds, function (index, value) {
                var elem = {};
                elem.pattern_id = value;
                elem.active = active;
                params.push(elem);
            });
            $.ajax({
                url: '/topics/update-patterns',
                type: "POST",
                data: 'params=' + JSON.stringify(params),
                success: function (data) {
                    $('#progressModal').modal('hide');
                    if (data == 'true') {
                        updateSubRuleProperties();
                    } else {
                        alert('An error occurred while updating the patterns!');
                    }
                }
            });
        }
    });

    //Click check all sub patterns button
    $(document).on('click', '#checkAllSubPatternsButton', function () {
        if (allSubPatternsItemsWasChecked) {
            allSubPatternsItemsWasChecked = false;
            $('#subPatternsContainer li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allSubPatternsItemsWasChecked = true;
            $('#subPatternsContainer li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });

    /**********  SUBRULE TEMPLATES FUNCTIONALITY **********/
    //Click 'Add new template' button
    $(document).on('click', '#subAddNewTemplateButton', function () {
        $('#progressModal').modal('show');
        var text = $('#subTemplateText').val();
        text = text.replace(/"/g, '\\"');
        var lines = text.split("\n");
        var elements = [];
        for (var i = 0; i < lines.length; i++) {
            var elem = {};
            elem.rule_id = currentSubRuleId;
            elem.text = encodeURIComponent(lines[i]);
            if (elem.text != "") {
                elements.push(elem);
            }
        }
        if (elements.length < 1) {
            $('#progressModal').modal('hide');
            return;
        }

        $.ajax({
            url: '/topics/create-templates',
            type: "POST",
            data: 'params=' + JSON.stringify(elements),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while creating the template!');
                }
            }
        });
    });

    //Click 'Delete template' button
    $(document).on('click', '#subDeleteTemplateButton', function () {
        /*var templateId = $('#subTemplatesBindableForm input[name="templateId"]').val();
        $.ajax({
            url: '/topics/delete-template',
            type: "POST",
            data: 'templateId=' + templateId,
            success: function (data) {
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while deleting the template!');
                }
            }
        });*/
        $('#progressModal').modal('show');
        var templatesIds = getCheckedOrCurrentObjectsIds('subTemplatesBindableForm', 'templateId', 'subTemplates', 'templateId');
        if (templatesIds.length == 0) {
            $('#progressModal').modal('hide');
            return;
        }
        var params = [];
        $.each(templatesIds, function (index, value) {
            var elem = {};
            elem.template_id = value;
            params.push(elem);
        });
        $.ajax({
            url: '/topics/delete-templates',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while deleting the templates!');
                }
            }
        });
    });

    //Click 'Update template' button
    $(document).on('click', '#subUpdateTemplateButton', function () {
        $('#progressModal').modal('show');
        var text = $('#subTemplateText').val();
        text = encodeURIComponent(text);
        var templateId = $('#subTemplatesBindableForm input[name="templateId"]').val();
        $.ajax({
            url: '/topics/update-template',
            type: "POST",
            data: 'templateId=' + templateId + '&text=' + text,
            success: function (data) {
                $('#progressModal').modal('hide');
                if (data == 'true') {
                    updateSubRuleProperties();
                } else {
                    alert('An error occurred while updating the template!');
                }
            }
        });
    });

    //Click check all sub templates button
    $(document).on('click', '#checkAllSubTemplatesButton', function () {
        if (allSubTemplatesItemsWasChecked) {
            allSubTemplatesItemsWasChecked = false;
            $('#subTemplatesContainer li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allSubTemplatesItemsWasChecked = true;
            $('#subTemplatesContainer li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });

    $(document).on('click', '#subTemplates li', function () {
        $('#currentSubTemplateId').val($(this).find('input[name="templateId"]').val());
    });

    // Prevent closing dropdown of scenarios when click on it's items near checkboxes
    $(document).on('click', '.current .dropdown-menu', function (e) {
        if (!$(this).hasClass('doNotStopPropagation')) {
            e.stopPropagation();
        }
    });
});
