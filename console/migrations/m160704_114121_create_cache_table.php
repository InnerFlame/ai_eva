<?php

use yii\db\Migration;

/**
 * Handles the creation for table `cache`.
 */
class m160704_114121_create_cache_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cache', [
            'id' => 'char(128) NOT NULL PRIMARY KEY',
            'expire' => 'int(11)',
            'data' => 'BLOB'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cache');
    }
}
