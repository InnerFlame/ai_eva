<?php

namespace frontend\assets;

class AccessAsset extends BasicAsset
{
    public $js = [
        'js/access.js',
    ];
}
