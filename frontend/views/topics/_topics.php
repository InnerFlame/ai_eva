<?php
use common\widgets\Column\Column;
use common\widgets\RadioButton\RadioButton;
use yii\bootstrap\Html;
use yii\widgets\Menu;

$topicsItems = array();
foreach ($model->topics as $topic) {
    $scenariosOfCurrentTopic = '<ul class="dropdown-menu">' .
        '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';

    $reversedScenarios = array_reverse($model->availableScenarios, true);
    foreach ($reversedScenarios as $scenario) {
        $scenariosOfCurrentTopic .= '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="..." ';
        if (in_array($scenario->getId(), $topic->restrictions)) {
            $scenariosOfCurrentTopic .= 'checked';
        }
        $scenariosOfCurrentTopic .= '>';
        $scenariosOfCurrentTopic .= $scenario->getName();
        $scenariosOfCurrentTopic .= '</a><input type="hidden" value="';
        $scenariosOfCurrentTopic .= $scenario->getId();
        $scenariosOfCurrentTopic .= '"></li>';
    }
    $scenariosOfCurrentTopic .= '</ul>';

    $tagsOfCurrentTopic = '<ul class="dropdown-menu">';
    foreach ($model->availableTags as $key => $value) {
        $tagsOfCurrentTopic .= '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="..." ';
        if (in_array($key, $topic->tags)) {
            $tagsOfCurrentTopic .= 'checked';
        }
        $tagsOfCurrentTopic .= '>';
        $tagsOfCurrentTopic .= $value;
        $tagsOfCurrentTopic .= '</a><input type="hidden" value="';
        $tagsOfCurrentTopic .= $key;
        $tagsOfCurrentTopic .= '"></li>';
    }
    $tagsOfCurrentTopic .= '</ul>';

    $availableLanguageTags = '<ul class="dropdown-menu dropdown-menu-right doNotStopPropagation">';
    foreach (\Yii::$app->params['languageTags'] as $value) {
        $availableLanguageTags .= '<li><a href="javascript:void(0);">';
        $availableLanguageTags .= $value;
        $availableLanguageTags .= '</a><input type="hidden" value="';
        $availableLanguageTags .= $value;
        $availableLanguageTags .= '"></li>';
    }
    $availableLanguageTags .= '</ul>';

    $availableIsExclude = '<ul class="dropdown-menu dropdown-menu-right doNotStopPropagation">';
    foreach ($topic->getExcludeParams() as $key => $value) {
        $availableIsExclude .= '<li><a href="javascript:void(0);">';
        $availableIsExclude .= $value;
        $availableIsExclude .= '</a><input type="hidden" value="';
        $availableIsExclude .= $key;
        $availableIsExclude .= '"></li>';
    }
    $availableIsExclude .= '</ul>';

    $item['label'] = $topic->getName();
    $item['labelOptions'] = ['name' => 'topicName'];
    $item['hiddens'] = [
        ['name' => 'topicId', 'value' => $topic->getId()],
        ['name' => 'topicPriority', 'value' => $topic->getPriority()],
        ['name' => 'topicLanguageTag', 'value' => $topic->getLanguageTag()],
        ['name' => 'topicIsExclude', 'value' => $topic->isExclude],
    ];
    $item['header'] = [
        //['label' => 'cam', 'class' => 'label label-primary',],
        ['class' => 'glyphicon glyphicon-user' . ($topic->isSwitching() ? ' active' : ''), 'name' => 'topicsHumAuto',],
        [
            'class' => 'glyphicon glyphicon-signal' . ($topic->isActive() ? ' active' : ''),
            'name' => 'topicsActiveTest',
        ],
        ['class' => 'glyphicon glyphicon-off' . ($topic->isEnabled() ? ' active' : ''), 'name' => 'topicsOnOff',],
    ];
    $item['lines'] = [
        ['elements' => [
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                'activeText' => 'Hum',
                'text' => 'Auto',
                'glyphiconClass' => 'glyphicon-user',
                'activeState' => $topic->isSwitching(),
                'name' => 'updateTopicsHumAutoRadioButton',
            ])],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                'activeText' => 'Active',
                'text' => 'Test',
                'glyphiconClass' => 'glyphicon-signal',
                'activeState' => $topic->isActive(),
                'name' => 'updateTopicsActiveTestRadioButton',
            ])],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                'activeText' => 'On',
                'text' => 'Off',
                'glyphiconClass' => 'glyphicon-off',
                'activeState' => $topic->isEnabled(),
                'name' => 'updateTopicsOnOffRadioButton',
            ])],
        ]],
        ['elements' => [
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Paste', [
                'class' => 'btn btn-default btn-block',
                'name' => 'pasteRuleButton',
                'type' => 'button',
            ])],
        ]],
        ['elements' => [['tag' => 'hr', 'options' => ['style' => 'border: 1px solid #fff']],]],
        ['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Name:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::input(
                'text',
                'name',
                $topic->getName(),
                ['class' => 'form-control']
            )],
        ]],
        /*['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Tag:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' =>
                Html::button(
                    Html::tag('span', 'No', ['class' => 'pull-left'])
                    . Html::tag('span', '', ['class' => 'caret']),
                    [
                        'class' => 'btn btn-default dropdown-toggle btn-block',
                        'aria-expanded' => 'false',
                        'aria-haspopup' => 'true',
                        'data-toggle' => 'dropdown',
                        'type' => 'button',
                    ]
                )
                . Menu::widget([
                    'items' => [
                        ['label' => 'No', 'url' => 'javascript:void(0);'],
                        ['label' => 'Cam', 'url' => 'javascript:void(0);'],
                        ['label' => 'Scam', 'url' => 'javascript:void(0);'],
                    ],
                    'options' => [
                        'class' => 'dropdown-menu dropdown-menu-right',
                    ],
                ])
            ],
        ]],*/
        ['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Scenarios:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8', 'name' => 'topicScenarios'], 'content' =>
                Html::button(
                    Html::tag('span', 'Scenarios', ['class' => 'pull-left'])
                    . Html::tag('span', '', ['class' => 'caret']),
                    [
                        'class' => 'btn btn-default dropdown-toggle btn-block',
                        'aria-expanded' => 'false',
                        'aria-haspopup' => 'true',
                        'data-toggle' => 'dropdown',
                        'type' => 'button',
                    ]
                )
                . $scenariosOfCurrentTopic
            ],
        ]],
        ['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Tags:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8', 'name' => 'topicTags'], 'content' =>
                Html::button(
                    Html::tag('span', 'Tags', ['class' => 'pull-left'])
                    . Html::tag('span', '', ['class' => 'caret']),
                    [
                        'class' => 'btn btn-default dropdown-toggle btn-block',
                        'aria-expanded' => 'false',
                        'aria-haspopup' => 'true',
                        'data-toggle' => 'dropdown',
                        'type' => 'button',
                    ]
                )
                . $tagsOfCurrentTopic
            ],
        ]],
        ['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Language:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8', 'name' => 'topicLanguage'], 'content' =>
                Html::button(
                    Html::tag('span', $topic->getLanguageTag(), ['class' => 'pull-left', 'name' => 'languageTag'])
                    . Html::tag('span', '', ['class' => 'caret']),
                    [
                        'class' => 'btn btn-default dropdown-toggle btn-block',
                        'aria-expanded' => 'false',
                        'aria-haspopup' => 'true',
                        'data-toggle' => 'dropdown',
                        'type' => 'button',
                    ]
                )
                . $availableLanguageTags
            ],
        ]],
        ['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Is Exclude For Initial:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8', 'name' => 'topicIsExclude'], 'content' =>
                Html::button(
                    Html::tag('span', $topic->getIsExcludeForView(), ['class' => 'pull-left', 'name' => 'isExclude'])
                    . Html::tag('span', '', ['class' => 'caret']),
                    [
                        'class' => 'btn btn-default dropdown-toggle btn-block',
                        'aria-expanded' => 'false',
                        'aria-haspopup' => 'true',
                        'data-toggle' => 'dropdown',
                        'type' => 'button',
                    ]
                )
                . $availableIsExclude
            ],
        ]],
        ['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Count Triggering:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::input(
                'text',
                'countTriggering',
                $topic->countTriggering,
                ['class' => 'form-control']
            )],
        ]],
        ['elements' => [
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                'class' => 'btn btn-danger btn-block',
                'name' => 'deleteTopicButton',
                'type' => 'button',
            ])],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                'class' => 'btn btn-primary btn-block',
                'name' => 'updateTopicButton',
                'type' => 'button',
            ])],
        ]],
    ];
    $topicsItems[] = $item;
}

echo Column::widget([
    'listId' => 'topicsList',
    'items' => $topicsItems,
    'collapsed' => true,
    'lastItemText' => 'Add new topic...',
    'lastItemOptions' => [
        'name' => 'addNewTopic',
        'data-toggle' => 'modal',
        'data-target' => '#addNewTopicModal',
    ],
]);
