export class SettingsCreateModel
{
    constructor(
        public name           : string = '',
        public controlProfile : string = '',
        public targetProfile  : string = ''
    ) {  }

    public static getSettingModelsApiUrl(): string
    {
        return "/settings-ng/get-setting-models";
    }

    public static getCreateSettingApiUrl(): string
    {
        return "/settings-ng/create";
    }

    public static getCreateDropDownDataApiUrl(): string
    {
        return "/settings-ng/get-create-drop-down-data";
    }

    public static getUpdateOrderApiUrl(): string
    {
        return "/settings-ng/update-order";
    }

    public static getDeleteSettingApiUrl(): string
    {
        return "/settings-ng/delete";
    }

    public static getActivationToggleApiUrl(): string
    {
        return "/settings-ng/activation-toggle";
    }

    public static getCopySettingApiRul(): string
    {
        return "/settings-ng/copy";
    }
}

export class SettingsUpdateModel
{
    constructor(
       public id                : number,
       public name              : string,
       public controlProfile    : string,
       public targetProfile     : string,
       public isActive          : number,
       public priority          : number,
       public countries         : Array<Object>,
       public locales           : Array<Object>,
       public senderLocales     : Array<Object>,
       public statuses          : Array<Object>,
       public sites             : Array<Object>,
       public traffSources      : Array<Object>,
       public actionWays        : Array<Object>,
       public traffTypes        : Array<Object>,
       public platforms         : Array<Object>,
       public fanClubs          : Array<Object>,
       public onlineStatuses    : Array<Object>,
       public scenarios         : Array<Object>,
       public projects          : Array<Object>,
       public splitName         : string,
       public splitGroup        : number
    ) {  }

    public static getSettingUpdateDataApiUrl(settingId: number): string
    {
        return "/settings-ng/get-setting-data-for-update?settingId=" + settingId;
    }

    public static getSettingAdditionalUpdateDataApiUrl(): string
    {
        return "/settings-ng/get-additional-setting-data-for-update";
    }

    public static getUpdateSettingApiUrl(): string
    {
        return "/settings-ng/update";
    }
}