<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_statuses_table`.
 */
class m160622_141544_create_settings_statuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_statuses_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'status_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_statuses_table');
    }
}
