import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { SharedModule }                  from "../shared";
import { NlpClassifiersComponent }       from "./nlp.classifiers.component";

const routing: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'nlp-classifiers-ng',
        component: NlpClassifiersComponent
    }
]);

@NgModule({
    imports: [
        routing,
        SharedModule
    ],
    declarations: [
        NlpClassifiersComponent
    ]
})

export class NlpClassifiersModule {}