<?php

namespace frontend\assets;

class SettingsAsset extends BasicAsset
{
    public $css = [
        'css/settings.css',
    ];
    public $js = [
        'js/settings.js',
    ];
}
