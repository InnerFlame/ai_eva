<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\commonInfo\CommonInfoForm;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class CommonInfoController extends BaseController
{
    public function beforeAction($action)
    {
        if ($action->id == 'export') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('commonInfo')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        $searchModel = new CommonInfoForm();
        $dataProvider = $searchModel->search(\Yii::$app->request->post());
        $paceholderDataProvider = $searchModel->placeholdersInfo(\Yii::$app->request->post());

        return $this->render('index', [
            'dataProvider'  => $dataProvider,
            'paceholderDataProvider' => $paceholderDataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionExport()
    {
        $request = \Yii::$app->request;
        $post = $request->post();
        $data = json_decode($post['params']);

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Common Info');

        for ($row = 1; $row <= count($data); $row++) {
            for ($column = 0; $column < count($data[$row - 1]); $column++) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $data[$row - 1][$column]);
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        $filename = "CommonInfo_" . date("d-m-Y_H:i:s") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        return $objWriter->save('php://output');
    }

    public function actionExplain()
    {
        $searchModel = new CommonInfoForm();
        $dataProvider = $searchModel->search(\Yii::$app->request->post(), true);
        $paceholderDataProvider = $searchModel->placeholdersInfo(\Yii::$app->request->post(), true);

        return $this->render('index', [
            'dataProvider'  => $dataProvider,
            'paceholderDataProvider' => $paceholderDataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}
