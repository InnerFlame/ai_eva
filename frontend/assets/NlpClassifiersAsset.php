<?php
namespace frontend\assets;

class NlpClassifiersAsset extends BasicAsset
{
    public $css = [
        'css/nlpClassifiers.css',
    ];

    public $js = [
        'js/nlpClassifiers.js',
    ];
}
