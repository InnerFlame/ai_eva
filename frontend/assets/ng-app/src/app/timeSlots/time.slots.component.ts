import { Component }      from '@angular/core';
import { Title }          from "@angular/platform-browser";
import { HttpService }    from "../shared/services/http.service";
import { OnInit }         from '@angular/core';
import { TimeSlotsModel } from "../shared";
import { IMyDpOptions }   from 'mydatepicker';

@Component({
    selector    : 'timeSlots',
    templateUrl : './time.slots.index.html',
})

export class TimeSlotsComponent implements OnInit
{
    public timeSlotsModel    : TimeSlotsModel = new TimeSlotsModel();
    public hideLoadingBlock  : boolean = true;
    public enableModelsSelect: boolean = false;
    public errorText         : string  = '';
    public successText       : string  = '';
    public timeSlotRecords   : any;

    public myDatePickerDateOptions: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
    };

    public timeMask = [/\d/, /\d/, ':', /\d/, /\d/, ':00'];

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param titleService
     */
    constructor(private httpService: HttpService, private titleService: Title)
    {
        this.titleService.setTitle( 'Test Chat' );
    }

    /**
     * On init script actions and events, here we bind available models
     */
    ngOnInit(): void
    {
        this.titleService.setTitle( 'Time Slots' );

        this.httpService.get(TimeSlotsModel.getModelsApiUrl()).then(data => {
            if (!data) {
                this.errorText = 'Something goes wrong wit response!';
            } else {
                this.timeSlotRecords = data;
                this.hideLoadingBlock = false;
            }
        });
    }

    /**
     * Submit form handler
     */
    public onSubmit(): void
    {
        this.errorText        = '';
        this.successText      = '';
        this.hideLoadingBlock = true;

        if (!this.isValidDateAndTimeInputs()) {
            this.hideLoadingBlock = false;
            return;
        }

        const body = { formData: JSON.stringify(this.timeSlotsModel) };

        this.httpService.post(TimeSlotsModel.getTiemslotsCreateApiUrl(), body)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.errorText = 'Something goes wrong wit test response or its empty...';
                } else {
                    this.successText = 'Timeslot was created successfully!';
                    this.timeSlotRecords = data;
                }

                this.hideLoadingBlock = false;
            });
    }

    public deleteTimeslot(timeslot_id: number, name: string): void
    {
        if (!confirm("Do you really wish to delete timeslot '" + name + "' ?")) {
            return;
        }

        this.hideLoadingBlock = true;

        const body = { timeslot_id: timeslot_id };

        this.httpService.post(TimeSlotsModel.getTimeslotDeleteApiUrl(), body)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.errorText  = 'Something goes wrong with create action!';
                } else {
                    this.timeSlotRecords = data;
                }

                this.hideLoadingBlock = false;
            });
    }

    public isValidDateAndTimeInputs(): boolean
    {
        if (this.timeSlotsModel.date1 != '' && this.timeSlotsModel.date2 == '') {
            this.errorText = 'Date 1 is invalid or you don\'t specify Date 2';
            return false;
        }

        if (this.timeSlotsModel.date2 != '' && this.timeSlotsModel.date1 == '') {
            this.errorText = 'Date 1 is invalid or you don\'t specify Date 2';
            return false;
        }


        let filteredTime1 = this.timeSlotsModel.time1.replace('_', '');
        let filteredTime2 = this.timeSlotsModel.time2.replace('_', '');

        if ((filteredTime1.length < 8) || (filteredTime1 == '00:00:00')) {
            this.errorText = 'Time 1 is incorrect';
            return false;
        }

        if ((filteredTime2.length < 8) || (filteredTime2 == '00:00:00')) {
            this.errorText = 'Time 2 is incorrect';
            return false;
        }

        return true;
    }

    public transformDate(date: string): string
    {
        return date.substring(0, 10);
    }

    /**
     * Just call {{diagnostic}} in template and watch model status in live time
     *
     * @returns {string}
     */
    get diagnostic() { return JSON.stringify(this.timeSlotsModel); }
}
