<?php
namespace common\components;

class TestChatScenariosComponent extends ScenariosComponent
{
    protected function getScenarios($params) {
        //Get scenarios
        $scenarios = \frontend\models\settings\Scenario::find()
            ->where(['setting_id' => $params['setting_id']])
            ->orderBy('scenario_priority')
            ->asArray()
            ->all();

        return $scenarios;
    }
}
