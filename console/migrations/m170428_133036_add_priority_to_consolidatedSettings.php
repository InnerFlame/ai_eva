<?php

use yii\db\Migration;

class m170428_133036_add_priority_to_consolidatedSettings extends Migration
{
    public function up()
    {
        $this->addColumn('consolidatedSettings', 'priority', $this->smallInteger()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('consolidatedSettings', 'priority');
    }
}
