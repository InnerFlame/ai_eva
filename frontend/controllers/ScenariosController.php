<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\scenarios\ScenariosForm;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class ScenariosController extends BaseController
{
    protected $model;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new ScenariosForm();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('scenarios')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        return $this->render('index', ['model' => $this->model]);
    }

    public function actionScenarios()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getScenarios();
                return $this->renderAjax('_scenarios', ['model' => $this->model]);
            }
        );
    }

    public function actionGroups()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getGroups();
                return $this->renderAjax('_groups', ['model' => $this->model]);
            }
        );
    }

    public function actionGroupPhrases()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getGroupsPhrases();
                $groupsPhrases = array();
                foreach ($this->model->groupsPhrases as $groupPhrases) {
                    $item['id'] = $groupPhrases->getId();
                    $item['text'] = $groupPhrases->getText();
                    $item['groupGroupsPhrasesId'] = $groupPhrases->getGroupGroupsPhrasesId();
                    $groupsPhrases[] = $item;
                }
                return json_encode($groupsPhrases);
            }
        );
    }

    public function actionClassifiers()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getClassifiers();
                $classifiersNames = array();
                foreach ($this->model->classifiers as $classifier) {
                    $classifiersNames[] = $classifier->getText();
                }
                return json_encode($classifiersNames);
            }
        );
    }

    public function actionNlpClassifiers()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getNlpClassifiers();
                $nlpClassifiers = array();
                foreach ($this->model->nlpClassifiers as $nlpClassifier) {
                    $object['name']     = $nlpClassifier->text;
                    $object['type']     = $nlpClassifier->type;
                    $object['items']    = [];
                    foreach ($nlpClassifier->classes as $class) {
                        $object['items'][] = $class->text;
                    }
                    $nlpClassifiers[]   = $object;
                }
                return json_encode($nlpClassifiers);
            }
        );
    }

    public function actionCreateScenario()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                //$post['name'] = str_replace('\\', '\\\\', $post['name']);
                return $this->model->createScenario($post['name']);
            },
            self::RETURN_RESPONSE_WITH_ITEM
        );
    }

    public function actionCreateBlock()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->createBlock($post['name']);
            },
            self::RETURN_RESPONSE_WITH_ITEM
        );
    }

    public function actionDeleteScenario()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteScenario((int)$post['scenarioId']);
            }
        );
    }

    public function actionDeleteScenarios()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->deleteScenarios($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateScenario()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                //$post['name'] = str_replace('\\', '\\\\', $post['name']);
                return $this->model->updateScenario(
                    (int)$post['scenarioId'],
                    $post['name'],
                    $post['nodes'],
                    $this->clearRootTag($post['flowChart']),
                    (int)$post['maxStepCount'],
                    $post['checkedTags'],
                    $post['scenarioType']
                );
            }
        );
    }

    private function clearRootTag($flowChartJson)
    {
        $flowChartArray = json_decode($flowChartJson);
        $items = [];
        foreach ($flowChartArray->nodeDataArray as $item) {
                $key = str_replace('(ROOT)', '', $item->key);
                $item->key = $key;
                $items[] = $item;
        }

        $flowChartArray->nodeDataArray = $items;

        return json_encode($flowChartArray);
    }

    public function actionCopyScenario()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                //$post['name'] = str_replace('\\', '\\\\', $post['name']);
                return $this->model->copyScenario($post['name'], $post['nodes'], $post['flowChart'], (int)$post['maxStepCount'], $post['scenarioType']);
            },
            self::RETURN_RESPONSE_WITH_ITEM
        );
    }

    public function actionBlocks()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getScenarios();
                $blocks = array();
                foreach ($this->model->scenarios as $scenario) {
                    if ($scenario->getIsBlock() == 1) {
                        $item['id'] = $scenario->getId();
                        $item['name'] = $scenario->getName();
                        $blocks[] = $item;
                    }
                }
                return json_encode($blocks);
            }
        );
    }
}
