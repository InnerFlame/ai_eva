<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_traffic_sources_table`.
 */
class m160622_141654_create_settings_traffic_sources_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_traffic_sources_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'traffic_source_id' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_traffic_sources_table');
    }
}
