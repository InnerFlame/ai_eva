import { Component }        from '@angular/core';
import { Title }            from "@angular/platform-browser";
import { HttpService }      from "../shared/services/http.service";
import { OnInit }           from '@angular/core';
import { TestChatModel }    from "../shared";

@Component({
    selector    : 'testChat',
    templateUrl : './test.chat.index.html',
})

export class TestChatComponent implements OnInit
{
    readonly selectTypeSetting: string = "setting";
    readonly selectTypeScenario: string = "scenario";

    private groupPhrases;
    public testChatModel:TestChatModel   = new TestChatModel();
    public hideDialogueBlock: boolean    = true;
    public hideLoadingBlock : boolean    = true;
    public hideHasErrorAlert: boolean    = true;
    public enableScenarioSelect: boolean = false;
    public enableSettingSelect: boolean  = false;
    public disableSubmitButton: boolean  = false;
    public errorText: string             = '';
    public chatScenarios;
    public chatSettings;

    /**
     * Data holders for template
     */
    public dialogueData;
    public properties;

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param titleService
     */
    constructor(private httpService: HttpService, private titleService: Title)
    {
        this.titleService.setTitle( 'Test Chat' );
    }

    /**
     * On init script actions and events, here we bind available models
     */
    ngOnInit(): void
    {


        this.httpService.get(TestChatModel.getScenariosApiUrl()).then(data => {
            if (!data) {
                this.hideHasErrorAlert = false;
                this.errorText         = 'Something goes wrong with scenarios response!';
            } else {
                this.enableScenarioSelect = true;
                this.chatScenarios        = this.mappingDataForSelect2(data);
            }
        });

        this.httpService.get(TestChatModel.getSettingsApiUrl()).then(data => {
            if (!data) {
                this.hideHasErrorAlert = false;
                this.errorText         = 'Something goes wrong with settings response!';
            } else {
                this.enableSettingSelect = true;
                this.chatSettings        = this.mappingDataForSelect2(data);
            }
        });

        this.hideLoadingBlock = false;
        this.httpService.get(TestChatModel.getHistoryDataApiUrl()).then(data => {
            if (data && (Object.keys(data).length > 0)) {
                this.dialogueData       = [data['res']];
                if (data['inputs']) {
                    this.testChatModel.scenario   = data['inputs'].scenarioId;
                    this.testChatModel.setting    = data['inputs'].setting;
                    this.testChatModel.fromUserId = data['inputs'].fromUserId;
                    this.testChatModel.toUserId   = data['inputs'].toUserId;
                }
                this.hideDialogueBlock  = false;
            }

            this.hideLoadingBlock = true;
        });

        this.httpService.get(TestChatModel.getGroupPhrasesApiUrl()).then(data => {
            if (data) {
                this.groupPhrases = data;
            }
        });
    }

    /**
     * Submit form handler
     */
    onSubmit(): void
    {
        if ((this.testChatModel.setting == '') && (this.testChatModel.scenario == '')) {
            alert("You must specify either setting or scenario!");
            return;
        }

        this.hideHasErrorAlert  = true;
        this.hideDialogueBlock  = true;
        this.hideLoadingBlock   = false;

        const body    = {
            scenario   : this.testChatModel.scenario,
            setting    : this.testChatModel.setting,
            fromUserId : this.testChatModel.fromUserId,
            toUserId   : this.testChatModel.toUserId,
            text       : this.testChatModel.text
        };

        this.httpService.post(TestChatModel.getResultsApiUrl(), body)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.hideHasErrorAlert = false;
                    this.errorText         = 'Something goes wrong wit test response or its empty... Try another phrase or model';
                } else {
                    this.hideDialogueBlock  = false;
                    this.dialogueData       = [data];
                    this.testChatModel.text = '';
                }

                this.hideLoadingBlock = true;
            });
    }

    /**
     * Get scenario name by id
     *
     * @param scenario_id
     * @returns {string}
     */
    getSendScenario(scenario_id)
    {
        if (!scenario_id || scenario_id == "") {
            return 'Undefined';
        }

        for(var key in this.chatScenarios) {
            if(this.chatScenarios.hasOwnProperty(key)) {
                if (!this.chatScenarios[key]) {
                    continue;
                }
                if (this.chatScenarios[key].id == scenario_id) {
                    return this.chatScenarios[key].name;
                }
            }
        }

        return 'Undefined';
    }

    /**
     * Get group phrase name by id
     *
     * @param group_phrase_id
     * @returns {string}
     */
    getGroupPhraseName(group_phrase_id): string
    {
        if (!group_phrase_id || group_phrase_id == "") {
            return 'Undefined';
        }

        for(let key in this.groupPhrases) {
            if(this.groupPhrases.hasOwnProperty(key)) {
                if (!this.groupPhrases[key]) {
                    continue;
                }

                if (this.groupPhrases[key].group_phrase_id == group_phrase_id) {
                    return this.groupPhrases[key].text;
                }
            }
        }

        return 'Undefined';
    }

    /**
     * Clear manager dialogue test history and all dialogue data from DB
     */
    clearHistory()
    {
        this.hideDialogueBlock  = true;
        this.hideLoadingBlock   = false;

        this.httpService.get(TestChatModel.getClearHistoryApiUrl()).then(data => {
            if (!data || Object.keys(data).length <= 0) {
                this.hideHasErrorAlert = false;
                this.errorText         = 'Nothing to clear!';
            } else {
                this.dialogueData = [];
            }

            this.hideLoadingBlock = true;
        });
    }

    /**
     * Simulate either validation for scenario and setting field
     *
     */
    updateSelects(type: string): void
    {
        if (type == this.selectTypeSetting) {
            if (this.testChatModel.scenario) {
                this.testChatModel.scenario = '';
            }
        } else {
            if (this.testChatModel.setting) {
                this.testChatModel.setting = '';
            }
        }
    }

    mappingDataForSelect2(data: object): object
    {
        for(var i in data) {
            data[i].text = data[i].name;
        }

        return data;
    }

    /**
     * Just call {{diagnostic}} in template and watch model status in live time
     *
     * @returns {string}
     */
    get diagnostic() { return JSON.stringify(this.testChatModel); }
}
