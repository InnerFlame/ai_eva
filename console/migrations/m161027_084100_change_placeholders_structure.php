<?php

use yii\db\Migration;

class m161027_084100_change_placeholders_structure extends Migration
{
    public function up()
    {
        $this->addColumn('placeholders_table', 'url_wap', $this->string()->notNull());
        $this->addColumn('placeholders_table', 'url_text_wap', $this->string());

        //Get ids of wap placeholders
        $wapIds = Yii::$app->db
            ->createCommand('SELECT placeholder_id FROM placeholders_receiver_platforms_table WHERE receiver_platform_name="mobSite";')
            ->queryAll();

        foreach ($wapIds as $wapId) {
            //Get wap placeholder
            $wapPlaceholder = Yii::$app->db
                ->createCommand('SELECT * FROM placeholders_table WHERE id=:id;')
                ->bindValue(':id', $wapId['placeholder_id'])
                ->queryOne();

            //Generate corresponding web placeholder name
            $webPlaceholderName = str_replace('_wap', '_web', $wapPlaceholder['name']);
            if ($webPlaceholderName == $wapPlaceholder['name']) {
                //go to the next placeholder if there is no corresponding name
                continue;
            }

            //Get web placeholder id
            $webId = Yii::$app->db
                ->createCommand('SELECT id FROM placeholders_table WHERE name=:name;')
                ->bindValue(':name', $webPlaceholderName)
                ->queryOne();

            //Update url_wap, url_text_wap and name fields in placeholders_table
            $placeholderName = $webPlaceholderName;
            $placeholderName = str_replace('_web', '', $placeholderName);
            $placeholderName = str_replace('_wap', '', $placeholderName);
            Yii::$app->db->createCommand('UPDATE placeholders_table SET url_wap=:url_wap, url_text_wap=:url_text_wap, name=:name WHERE id=:id;')
                ->bindValue(':url_wap', $wapPlaceholder['url'])
                ->bindValue(':url_text_wap', $wapPlaceholder['url_text'])
                ->bindValue(':id', $webId['id'])
                ->bindValue(':name', $placeholderName)
                ->execute();

            //Update communicationLog table
            Yii::$app->db->createCommand('UPDATE communicationLog SET placeholderId=:new_id WHERE placeholderId=:old_id;')
                ->bindValue(':new_id', $webId['id'])
                ->bindValue(':old_id', $wapPlaceholder['id'])
                ->execute();

            //Delete wap placeholder from placeholders_receiver_locations_table
            Yii::$app->db->createCommand('DELETE FROM placeholders_receiver_locations_table WHERE placeholder_id=:id;')
                ->bindValue(':id', $wapPlaceholder['id'])
                ->execute();
            //Delete wap placeholder from placeholders_receiver_platforms_table
            Yii::$app->db->createCommand('DELETE FROM placeholders_receiver_platforms_table WHERE placeholder_id=:id;')
                ->bindValue(':id', $wapPlaceholder['id'])
                ->execute();
            //Delete wap placeholder from placeholders_sender_types_table
            Yii::$app->db->createCommand('DELETE FROM placeholders_sender_types_table WHERE placeholder_id=:id;')
                ->bindValue(':id', $wapPlaceholder['id'])
                ->execute();
            //Delete wap placeholder from placeholders_table
            Yii::$app->db->createCommand('DELETE FROM placeholders_table WHERE id=:id;')
                ->bindValue(':id', $wapPlaceholder['id'])
                ->execute();
        }

        $this->dropTable('placeholders_receiver_platforms_table');
    }

    public function down()
    {
        $this->createTable('placeholders_receiver_platforms_table', [
            'id' => $this->primaryKey(),
            'placeholder_id' => $this->integer(11),
            'receiver_platform_name' => $this->string(),
        ]);

        //Get all placeholders
        $placeholders = Yii::$app->db
            ->createCommand('SELECT * FROM placeholders_table;')
            ->queryAll();

        foreach ($placeholders as $placeholder) {
            //Generate corresponding web and wap placeholders name
            $webPlaceholderName = $placeholder['name'] . '_web';
            $wapPlaceholderName = $placeholder['name'] . '_wap';

            Yii::$app->db->createCommand('UPDATE placeholders_table SET name=:name WHERE id=:id;')
                ->bindValue(':id', $placeholder['id'])
                ->bindValue(':name', $webPlaceholderName)
                ->execute();

            $this->insert('placeholders_table', [
                'name'          => $wapPlaceholderName,
                'type'          => $placeholder['type'],
                'url'           => $placeholder['url_wap'],
                'url_text'      => $placeholder['url_text_wap'],
                'url_type'      => $placeholder['url_type'],
                'active'        => $placeholder['active'],
                'url_wap'       => '',
                'url_text_wap'  => '',
            ]);

            $wapPlaceholderId = Yii::$app->db
                ->createCommand('SELECT MAX(id) FROM placeholders_table;')
                ->queryScalar();

            //Insert wap placeholder to placeholders_receiver_locations_table
            $locations = Yii::$app->db
                ->createCommand('SELECT receiver_location_name FROM placeholders_receiver_locations_table WHERE placeholder_id=:id;')
                ->bindValue(':id', $placeholder['id'])
                ->queryAll();
            foreach ($locations as $location) {
                $this->insert('placeholders_receiver_locations_table', [
                    'placeholder_id' => $wapPlaceholderId,
                    'receiver_location_name' => $location['receiver_location_name'],
                ]);
            }

            //Insert wap placeholder to placeholders_sender_types_table
            $types = Yii::$app->db
                ->createCommand('SELECT sender_type_name FROM placeholders_sender_types_table WHERE placeholder_id=:id;')
                ->bindValue(':id', $placeholder['id'])
                ->queryAll();
            foreach ($types as $type) {
                $this->insert('placeholders_sender_types_table', [
                    'placeholder_id' => $wapPlaceholderId,
                    'sender_type_name' => $type['sender_type_name'],
                ]);
            }

            //Insert web placeholder to placeholders_receiver_platforms_table
            $this->insert('placeholders_receiver_platforms_table', [
                'placeholder_id' => $placeholder['id'],
                'receiver_platform_name' => 'webSite',
            ]);

            //Insert wap placeholder to placeholders_receiver_platforms_table
            $this->insert('placeholders_receiver_platforms_table', [
                'placeholder_id' => $wapPlaceholderId,
                'receiver_platform_name' => 'mobSite',
            ]);
            //Cannot update communication log table
        }

        $this->dropColumn('placeholders_table', 'url_wap');
        $this->dropColumn('placeholders_table', 'url_text_wap');
    }
}
