#!/usr/bin/env bash

DOCKER_COMPOSE_FILES="-f docker-compose.yml -f docker-compose.prod.yml"
RUN_ARGS="-d"

SCALE_SERVICES=" --scale ng_app_build=0"
SCALE_SERVICES+=" --scale worker_send-messages=2"

export DOCKER_COMPOSE_FILES
export RUN_ARGS
export SCALE_SERVICES
