<?php

use yii\db\Migration;

class m170302_120458_add_fan_club_and_online_values_to_settings extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand('INSERT INTO settings_fan_club(setting_id, is_fan_club_allowed) SELECT id, 0 FROM settings_table;')
            ->execute();
        Yii::$app->db->createCommand('INSERT INTO settings_fan_club(setting_id, is_fan_club_allowed) SELECT id, 1 FROM settings_table;')
            ->execute();
        Yii::$app->db->createCommand('INSERT INTO settings_online(setting_id, is_online) SELECT id, 0 FROM settings_table;')
            ->execute();
        Yii::$app->db->createCommand('INSERT INTO settings_online(setting_id, is_online) SELECT id, 1 FROM settings_table;')
            ->execute();
    }

    public function down()
    {
        Yii::$app->db->createCommand('DELETE FROM settings_fan_club;')
            ->execute();
        Yii::$app->db->createCommand('DELETE FROM settings_online;')
            ->execute();
    }
}
