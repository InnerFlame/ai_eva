export class MreClassifierModel {
    constructor(
        public classifier_id: number = 0,
        public model: string = '',
        public text: string = ''
    ) {}

    public load(data)
    {
        this.classifier_id = data.classifier_id;
        this.model = data.model;
        this.text = data.text;
    }
}

export class MreClassModel {
    constructor(
        public classifier_class_id: number = 0,
        public classifier_id: number = 0,
        public name: string = '',
    ) {}

    public load(data)
    {
        this.classifier_class_id = data.classifier_class_id;
        this.classifier_id = data.classifier_id;
        this.name = data.name;
    }
}

export class MrePatternModel {
    constructor(
        public classifier_id: number = 0,
        public classifier_pattern_id: number = 0,
        public classifier_class_id: number = 0,
        public text: string = '',
        public test: string = '',
        public deleted: number = 0,
        public version: number = 0,
        public active: number = 0,
    ) {}

    public load(data)
    {
        this.classifier_id = data.classifier_id;
        this.classifier_pattern_id = data.classifier_pattern_id;
        this.classifier_class_id = data.classifier_class_id;
        this.text = data.text;
        this.test = data.test;
        this.deleted = data.deleted;
        this.version = data.version;
        this.active = data.active;
    }
}

export class MreClassifierRouting
{
    private static baseUrl = '/mre-classifiers-ng/';

    /*
    |--------------------------------------------------------------------------
    | Classifiers
    |--------------------------------------------------------------------------
    */

    public static actionGetClassifiers(): string
    {
        return this.baseUrl + 'get-classifiers';
    }

    public static actionCreateClassifier(): string
    {
        return this.baseUrl + 'create-classifier';
    }

    public static actionUpdateClassifier(): string
    {
        return this.baseUrl + 'update-classifier';
    }

    public static actionDeleteClassifier(): string
    {
        return this.baseUrl + 'delete-classifier';
    }

    /*
    |--------------------------------------------------------------------------
    | Classes
    |--------------------------------------------------------------------------
    */

    public static actionGetClasses(): string
    {
        return this.baseUrl + 'get-classes';
    }

    public static actionCreateClass(): string
    {
        return this.baseUrl + 'create-class';
    }

    public static actionUpdateClass(): string
    {
        return this.baseUrl + 'update-class';
    }

    public static actionDeleteClass(): string
    {
        return this.baseUrl + 'delete-class';
    }

    /*
    |--------------------------------------------------------------------------
    | Patterns
    |--------------------------------------------------------------------------
    */

    public static actionGetPatterns(): string
    {
        return this.baseUrl + 'get-patterns';
    }

    public static actionCreatePattern(): string
    {
        return this.baseUrl + 'create-pattern';
    }

    public static actionUpdatePattern(): string
    {
        return this.baseUrl + 'update-pattern';
    }

    public static actionDeletePattern(): string
    {
        return this.baseUrl + 'delete-pattern';
    }

    public static actionUpdateMultiPatterns(): string
    {
        return this.baseUrl + 'update-multi-patterns';
    }

    public static actionDeleteMultiPatterns(): string
    {
        return this.baseUrl + 'delete-multi-patterns';
    }
}
