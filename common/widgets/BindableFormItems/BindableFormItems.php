<?php

namespace common\widgets\BindableFormItems;

use yii\bootstrap\Html;
use \yii\bootstrap\Widget;

class BindableFormItems extends Widget
{
    public $bindableFormId;
    public $items;
    public $checked;
    public $firstItemText;
    public $firstItemOptions;
    public $secondItemText;
    public $secondItemOptions;
    public $lastItemText;
    public $lastItemOptions;

    public function init()
    {
        parent::init();
        $view = $this->getView();
        BindableFormItemsAsset::register($view);
    }

    public function run()
    {
        $content = '';

        $content .= Html::beginTag('ul', ['class' => 'list-group bindable-form-items']);
        if (isset($this->firstItemText)) {
            if (isset($this->firstItemOptions['class'])) {
                $this->firstItemOptions['class'] .= ' list-group-item first-item';
            } else {
                $this->firstItemOptions['class'] = 'list-group-item first-item';
            }
            $content .= Html::beginTag('li', $this->firstItemOptions);
            $content .= $this->firstItemText;
            $content .= Html::endTag('li');
        }
        if (isset($this->secondItemText)) {
            if (isset($this->secondItemOptions['class'])) {
                $this->secondItemOptions['class'] .= ' list-group-item first-item';
            } else {
                $this->secondItemOptions['class'] = 'list-group-item first-item';
            }
            $content .= Html::beginTag('li', $this->secondItemOptions);
            $content .= $this->secondItemText;
            $content .= Html::endTag('li');
        }
        foreach ($this->items as $item) {
            $content .= Html::beginTag('li', [
                'class' => 'list-group-item sortable',
                'onclick' => 'bindableFormItemClick(this,\'#' . $this->bindableFormId . '\');'
            ]);

            //Hidden fields
            if (isset($item['hiddens'])) {
                foreach ($item['hiddens'] as $hidden) {
                    $content .= Html::hiddenInput($hidden['name'], $hidden['value']);
                }
            }

            if (isset($item['rightElement'])) {
                $content .= $item['rightElement'];
            }
            $content .= Html::beginTag('span', ['class' => 'pull-left',]);
            if (isset($this->checked) && $this->checked == true) {
                $content .= Html::tag('input', '', ['type' => 'checkbox', 'name' => 'itemCheckbox']);
            }
            if (isset($item['images'])) {
                foreach ($item['images'] as $itemImage) {
                    $content .= Html::tag('span', $itemImage['label'] ?? '', [
                        'class' => $itemImage['class'] ?? '',
                        'name' => $itemImage['name'] ?? ''
                    ]);
                    $content .= '&nbsp';
                }
            }
            $content .= Html::endTag('span');
            $content .= '&nbsp';
            if (isset($item['subtext'])) {
                $content .= '<b>';
                $content .= Html::tag('span', $item['text'], ['name' => 'itemText', 'style' => 'font-size: 16px;',]);
                $content .= '</b> &nbsp';
                $content .= Html::tag('span', $item['subtext'], ['name' => 'itemSubtext',]);
            } else {
                $content .= Html::tag('span', $item['text'], ['name' => 'itemText']);
            }

            $content .= Html::endTag('li');
        }
        if (isset($this->lastItemText)) {
            if (isset($this->lastItemOptions['class'])) {
                $this->lastItemOptions['class'] .= ' list-group-item last-item';
            } else {
                $this->lastItemOptions['class'] = 'list-group-item last-item';
            }
            $content .= Html::beginTag('li', $this->lastItemOptions);
            $content .= $this->lastItemText;
            $content .= Html::endTag('li');
        }
        $content .= Html::endTag('ul');

        return $content;
    }
}
