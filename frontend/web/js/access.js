$(document).ready(function () {
    $(document).on('click', '.modal-href', function () {
        var userId      = $(this).data('id');
        var userName    = $(this).data('name');
        $('#editUserModal #userId').val(userId);
        $('#editUserModal #userName').html(userName);
    });

    $(document).on('click', '#editUserModal div[id="changeRole"] ul li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#editUserModal div[id="changeRole"] button input').val(val);
        $('#editUserModal div[id="changeRole"] button span:eq(0)').html(name);
    });

    $(document).on('click', '#updateRole', function () {
        var role = $('#editUserModal div[id="changeRole"] button input').val();
        var userId = $('#editUserModal #userId').val();
        $.ajax({
            url: '/access/update',
            type: "POST",
            data: 'userId=' + userId + '&role=' + role,
            success: function (data) {
                $('#editUserModal').modal('hide');
                if (data == 'true') {
                    window.location.reload();
                } else {
                    alert('An error occurred while updating user role!');
                }
            }
        });
    });
});
