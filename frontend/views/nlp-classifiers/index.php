<?php
use yii\bootstrap\Html;
use common\widgets\BindableForm\BindableForm;
use frontend\assets\NlpClassifiersAsset;
use common\widgets\Modal\Modal;

NlpClassifiersAsset::register($this);

//Modal confirm delete classifiers
echo Modal::widget([
    'id'                => 'confirmDeleteClassifiersModal',
    'title'             => 'Delete classifiers',
    'hiddenInputIds'    => ['deleteClassifierId',],
    'nameText'          => 'classifiers',
    'nameId'            => 'deleteClassifiersName',
    'buttonId'          => 'confirmDeleteClassifiersButton'
]);

//Modal confirm delete classifier classes
echo Modal::widget([
    'id'                => 'confirmDeleteClassModal',
    'title'             => 'Delete classes',
    'hiddenInputIds'    => ['deleteClassId',],
    'nameText'          => 'classes',
    'nameId'            => 'deleteClassesName',
    'buttonId'          => 'confirmDeleteClassesButton'
]);

//Modal confirm delete class phrases
echo Modal::widget([
    'id'                => 'confirmDeleteClassPhrasesModal',
    'title'             => 'Delete class phrases',
    'nameText'          => 'class phrases',
    'nameId'            => 'deleteClassPhrasesNames',
    'buttonId'          => 'confirmDeleteClassPhrasesButton'
]);

//Modal confirm delete root classifiers
echo Modal::widget([
    'id'                => 'confirmDeleteRootClassifiersModal',
    'title'             => 'Delete classifiers',
    'hiddenInputIds'    => ['deleteRootClassifierId',],
    'nameText'          => 'classifiers',
    'nameId'            => 'deleteRootClassifiersName',
    'buttonId'          => 'confirmDeleteRootClassifiersButton'
]);

//Modal confirm delete root classifier classes
echo Modal::widget([
    'id'                => 'confirmDeleteRootClassModal',
    'title'             => 'Delete classes',
    'hiddenInputIds'    => ['deleteRootClassId',],
    'nameText'          => 'classes',
    'nameId'            => 'deleteRootClassesName',
    'buttonId'          => 'confirmDeleteRootClassesButton'
]);

//Modal confirm delete class root phrases
echo Modal::widget([
    'id'                => 'confirmDeleteRootClassPhrasesModal',
    'title'             => 'Delete class phrases',
    'nameText'          => 'class phrases',
    'nameId'            => 'deleteRootClassPhrasesNames',
    'buttonId'          => 'confirmDeleteRootClassPhrasesButton'
]);
?>

<!-- Modal train classifier -->
<div id="trainClassifierModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Train classifier...</h4>
            </div>
            <div class="modal-body">
                <br/>
                <div class="progress progress-striped active">
                    <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0"
                         aria-valuemax="100" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal train classifier -->
<div id="trainRootClassifierModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Train root classifier...</h4>
            </div>
            <div class="modal-body">
                <br/>
                <div class="progress progress-striped active">
                    <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0"
                         aria-valuemax="100" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CLASSIFIERS CONTAINER -->
<div class="col-md-4" id="classifiersContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a data-toggle="tab" href="#classifiersTab">Classifiers</a></li>
        <li><a data-toggle="tab" href="#rootClassifiersTab">Root Classifiers</a></li>
    </ul>
    <div class="tab-content">
        <div id="classifiersTab" class="tab-pane fade in active">
            <?php
            echo BindableForm::widget([
                'id' => 'classifiersBindableForm',
                'hiddens' => [
                    ['name' => 'classifierId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows'  => '5',
                                'id'    => 'classifiersText',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-2'], 'content' =>
                            Html::a('<span class="glyphicon glyphicon-filter"></span>', null, [
                                'class' => 'pull-left btn btn-default',
                                'id' => 'filterClassifiersModels',
                                'title' => 'Display all/only for current classifier models'
                            ])
                        ],
                        ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Model:'],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6', 'name' => 'classifierModel'], 'content' =>

                            Html::button(
                                Html::tag('span', '', [
                                    'class' => 'pull-left',
                                    'id' => 'classifierModelText',
                                    'name' => 'classifierModelText',
                                ])
                                . Html::hiddenInput(
                                    'classifierModel',
                                    '',
                                    ['id' => 'classifierModel',]
                                )
                                . Html::tag('span', '', ['class' => 'caret']),
                                [
                                    'class' => 'btn btn-default dropdown-toggle btn-block',
                                    'aria-expanded' => 'false',
                                    'aria-haspopup' => 'true',
                                    'data-toggle' => 'dropdown',
                                    'type' => 'button',
                                    'style' => 'text-align: right',
                                ]
                            )
                            . Html::tag('ul', '', ['id' => 'availableModelsList', 'class' => 'dropdown-menu dropdown-menu-right doNotStopPropagation'])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type'  => 'button',
                            'id'    => 'updateClassifierButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type'  => 'button',
                            'id'    => 'deleteClassifierButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type'  => 'button',
                            'id'    => 'addClassifierButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Train', [
                            'class' => 'btn btn-info btn-block',
                            'type'  => 'button',
                            'id'    => 'trainClassifierButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="classifiers"></div>
        </div>
        <div id="rootClassifiersTab" class="tab-pane fade in">
            <?php
            echo BindableForm::widget([
                'id' => 'rootClassifiersBindableForm',
                'hiddens' => [
                    ['name' => 'rootClassifierId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows'  => '5',
                                'id'    => 'rootClassifiersText',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-2'], 'content' =>
                            Html::a('<span class="glyphicon glyphicon-filter"></span>', null, [
                                'class' => 'pull-left btn btn-default',
                                'id' => 'filterRootClassifiersModels',
                                'title' => 'Display all/only for current classifier models'
                            ])
                        ],
                        ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Model:'],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6', 'name' => 'rootClassifierModel'], 'content' =>
                            Html::button(
                                Html::tag('span', '', [
                                    'class' => 'pull-left',
                                    'id' => 'rootClassifierModelText',
                                    'name' => 'rootClassifierModelText',
                                ])
                                . Html::hiddenInput(
                                    'rootClassifierModel',
                                    '',
                                    ['id' => 'rootClassifierModel',]
                                )
                                . Html::tag('span', '', ['class' => 'caret']),
                                [
                                    'class' => 'btn btn-default dropdown-toggle btn-block',
                                    'aria-expanded' => 'false',
                                    'aria-haspopup' => 'true',
                                    'data-toggle' => 'dropdown',
                                    'type' => 'button',
                                    'style' => 'text-align: right',
                                ]
                            )
                            . Html::tag('ul', '', ['id' => 'availableRootModelsList', 'class' => 'dropdown-menu dropdown-menu-right doNotStopPropagation'])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type'  => 'button',
                            'id'    => 'updateRootClassifierButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type'  => 'button',
                            'id'    => 'deleteRootClassifierButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type'  => 'button',
                            'id'    => 'addRootClassifierButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Train', [
                            'class' => 'btn btn-info btn-block',
                            'type'  => 'button',
                            'id'    => 'trainRootClassifierButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="rootClassifiers"></div>
        </div>
    </div>
</div>

<!-- CLASSES CONTAINER -->
<div class="col-md-4" id="classesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active disabled"><a data-toggle="tab" href="#classesTab">Classes</a></li>
        <li class="disabled"><a data-toggle="tab" href="#rootClassesTab">Root Classes</a></li>
    </ul>
    <div class="tab-content">
        <div id="classesTab" class="tab-pane fade in active">
            <?php
            echo BindableForm::widget([
                'id' => 'classesBindableForm',
                'hiddens' => [
                    ['name' => 'classifierId'],
                    ['name' => 'classId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows'  => '5',
                                'id'    => 'classText',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-6'],
                            'content' => Html::label( 'Probability:', 'classProbability', [
                                'class' => 'labelForInput',
                            ])
                        ],
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-6'],
                            'content' => Html::input('number', 'itemProbability', '', [
                                'class' => 'form-control',
                                'id'    => 'classProbability',
                                'min'   => '0',
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-12'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type'  => 'button',
                            'id'    => 'updateClassButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type'  => 'button',
                            'id'    => 'addClassButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type'  => 'button',
                            'id'    => 'deleteClassButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="classes"></div>
        </div>
        <div id="rootClassesTab" class="tab-pane fade in">
            <?php
            echo BindableForm::widget([
                'id' => 'rootClassesBindableForm',
                'hiddens' => [
                    ['name' => 'rootClassifierId'],
                    ['name' => 'rootClassId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows'  => '5',
                                'id'    => 'rootClassText',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-6'],
                            'content' => Html::label( 'Probability:', 'rootClassProbability', [
                                'class' => 'labelForInput',
                            ])
                        ],
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-6'],
                            'content' => Html::input('number', 'rootItemProbability', '', [
                                'class' => 'form-control',
                                'id'    => 'rootClassProbability',
                                'min'   => '0',
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-12'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type'  => 'button',
                            'id'    => 'updateRootClassButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type'  => 'button',
                            'id'    => 'addRootClassButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type'  => 'button',
                            'id'    => 'deleteRootClassButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="rootClasses"></div>
        </div>
</div>
</div>

<!-- PHRASES CONTAINER -->
<div class="col-md-4" id="phrasesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active disabled"><a data-toggle="tab" href="#phrasesTab">Phrases</a></li>
        <li class="disabled"><a data-toggle="tab" href="#rootPhrasesTab">Root Phrases</a></li>
    </ul>
    <div class="tab-content">
        <div id="phrasesTab" class="tab-pane fade in active">
            <?php
            echo BindableForm::widget([
                'id' => 'phrasesBindableForm',
                'hiddens' => [
                    ['name' => 'classId'],
                    ['name' => 'phraseId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows'  => '5',
                                'id'    => 'phraseText',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type'  => 'button',
                            'id'    => 'updatePhraseButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type'  => 'button',
                            'id'    => 'deletePhraseButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Check all', [
                            'class' => 'btn btn-default btn-block',
                            'type'  => 'button',
                            'id'    => 'checkAllPhrasesButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type'  => 'button',
                            'id'    => 'addPhraseButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="phrases"></div>
        </div>
        <div id="rootPhrasesTab" class="tab-pane fade in">
            <?php
            echo BindableForm::widget([
                'id' => 'rootPhrasesBindableForm',
                'hiddens' => [
                    ['name' => 'rootClassId'],
                    ['name' => 'rootPhraseId'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::textarea('itemText', '', [
                                'class' => 'form-control',
                                'rows'  => '5',
                                'id'    => 'rootPhraseText',
                                'style' => 'resize:vertical;'
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                            'class' => 'btn btn-primary btn-block',
                            'type'  => 'button',
                            'id'    => 'updateRootPhraseButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                            'class' => 'btn btn-danger btn-block',
                            'type'  => 'button',
                            'id'    => 'deleteRootPhraseButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Check all', [
                            'class' => 'btn btn-default btn-block',
                            'type'  => 'button',
                            'id'    => 'checkAllRootPhrasesButton',
                        ])],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Add', [
                            'class' => 'btn btn-success btn-block',
                            'type'  => 'button',
                            'id'    => 'addRootPhraseButton',
                        ])],
                    ]],
                ],
            ]);
            ?>
            <div id="rootPhrases"></div>
        </div>
    </div>
</div>
