<?php

namespace console\controllers;

use yii\console\Controller;


class AmqpTestController extends Controller
{

    public function actionPublish()
    {
        $data = [
            'rnd' => mt_rand(),
        ];

        var_dump($data);

        \Yii::$app->amqp->publishEvent('test_queue', $data);
    }

    public function actionConsume()
    {
        \Yii::$app->amqp->consumeEvents('test_queue', [$this, 'messageHandler']);
    }

    public function messageHandler($message)
    {
        if (empty($message) || empty($message->body)) {
            echo 'empty message' . PHP_EOL;
            return false;
        }

        $correlationId = null;
        if ($message->has('correlation_id')) {
            $correlationId = $message->get('correlation_id');
        }

        $incomingData = json_decode($message->body, true);

        if (empty($incomingData)) {
            echo 'empty incoming data' . PHP_EOL;
            return false;
        }

        var_dump($correlationId, $incomingData);

        \Yii::$app->amqp->ackMessage($message);

        return false;
    }
}