<?php

namespace common\widgets\CollapsableLink;

use yii\web\AssetBundle;

class CollapsableLinkAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/CollapsableLink/assets';

    public $css = [
        'css/collapsablelink.css',
    ];

    public $js = [
        'js/collapsablelink.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        //'forceCopy'=>true,
    ];
}
