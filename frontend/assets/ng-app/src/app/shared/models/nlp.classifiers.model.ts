export class NlpClassifiersModel {
    constructor(
        public type: string,
        public classifier_id: number = 0,
        public model: string = '',
        public text: string = ''
    ) {}
}

export class NlpClassifiersClassesModel {
    constructor(
        public classifier_class_id: number = 0,
        public classifier_id: number = 0,
        public name: string = '',
        public trigger_threshold: number = 0
    ) {}
}

export class NlpClassifiersClassPhrasesModel {
    constructor(
        public learning_phrase_id: number = 0,
        public classifier_class_id: number = 0,
        public text: string = ''
    ) {}
}

export class PhraseEntitiesModel {
    constructor(
        public learning_phrase_id: number = 0,
        public learning_phrase_entity_id: number = 0,
        public classifier_class_id: number = 0,
        public start: number = 0,
        public end: number = 0,
        public entity: string = '',
        public value: string = ''
    ) {}
}

export class NlpClassifiersRouting
{
    private static baseUrl = '/nlp-classifiers-ng/';

    public static getClassifierCreateUrl(): string
    {
        return this.baseUrl + 'create-classifier';
    }

    public static getClassifierUpdateUrl(): string
    {
        return this.baseUrl + 'update-classifier';
    }

    public static getClassifierDeleteUrl(): string
    {
        return this.baseUrl + 'delete-classifier';
    }

    public static getClassifierTrainUrl(): string
    {
        return this.baseUrl + 'train-classifier';
    }

    public static getClassifiersUrl(): string
    {
        return this.baseUrl + 'get-classifiers';
    }

    public static getClassifierClassesUrl(): string
    {
        return this.baseUrl + 'get-classifier-classes';
    }

    public static getClassifierClassPhrasesUrl(): string
    {
        return this.baseUrl + 'get-classifier-phrases';
    }

    public static getClassifierClassCreateUrl(): string
    {
        return this.baseUrl + 'create-classifier-class';
    }

    public static getClassifierClassUpdateUrl(): string
    {
        return this.baseUrl + 'update-classifier-class';
    }

    public static getClassifierClassDeleteUrl(): string
    {
        return this.baseUrl + 'delete-classifier-class';
    }

    public static getClassifierClassPhraseCreateUrl(): string
    {
        return this.baseUrl + 'create-classifier-class-phrase';
    }

    public static getClassifierClassPhraseUpdateUrl(): string
    {
        return this.baseUrl + 'update-classifier-class-phrase';
    }

    public static getClassifierClassPhraseDeleteUrl(): string
    {
        return this.baseUrl + 'delete-classifier-class-phrase';
    }

    public static getClassifierClassPhraseDeleteMultipleUrl(): string
    {
        return this.baseUrl + 'delete-multiple-classifier-class-phrase';
    }

    public static getPhraseEntityCreateUrl(): string
    {
        return this.baseUrl + 'create-phrase-entity';
    }

    public static getPhraseEntityUpdateUrl(): string
    {
        return this.baseUrl + 'update-phrase-entity';
    }

    public static getPhraseEntityDeleteUrl(): string
    {
        return this.baseUrl + 'delete-phrase-entity';
    }

    public static getPhraseEntityCreateMultiUrl(): string
    {
        return this.baseUrl + 'create-multi-phrase-entity';
    }

    public static getPhraseEntityUpdateMultiUrl(): string
    {
        return this.baseUrl + 'update-multi-phrase-entity';
    }

    public static getPhraseEntityDeleteMultiUrl(): string
    {
        return this.baseUrl + 'delete-multi-phrase-entity';
    }

    public static getClassifierAvailableModelsUrl(): string
    {
        return this.baseUrl + 'get-available-classifier-models';
    }
}


