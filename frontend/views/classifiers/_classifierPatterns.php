<?php
use common\widgets\BindableFormItems\BindableFormItems;

$classifierPatternsItems = [];

foreach ($model->classifierPatterns as $classifierPattern) {
    $item['images'] = [
        [
            'class' => 'glyphicon glyphicon-signal' . ($classifierPattern->isActive() ? ' active' : ''),
            'name' => 'activeClassifierPatternIcon',
        ],
    ];
    $item['text'] = $classifierPattern->getText();
    $item['hiddens'] = [
        ['name' => 'testText',            'value' => $classifierPattern->getTest()],
        ['name' => 'classifierPatternId', 'value' => $classifierPattern->getId()],
    ];
    $classifierPatternsItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId'    => 'classifierPatternsBindableForm',
    'items'             => $classifierPatternsItems,
    'checked'           => true,
]);
