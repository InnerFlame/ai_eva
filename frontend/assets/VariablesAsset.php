<?php

namespace frontend\assets;

class VariablesAsset extends BasicAsset
{
    public $js = [
        'js/variables.js',
    ];
}
