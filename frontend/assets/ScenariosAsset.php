<?php

namespace frontend\assets;

class ScenariosAsset extends BasicAsset
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/scenarios.css',
    ];
    public $js = [
        'js/scenariosDiagram.js',
        'js/scenarios.js',
        'js/scenariosElementsToggle.js'
    ];

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->depends[] = 'frontend\assets\GojsAsset';
    }
}
