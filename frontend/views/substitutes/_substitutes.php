<?php
use common\widgets\BindableFormItems\BindableFormItems;

$substitutesItems = array();
foreach ($model->substitutes as $substitute) {
    $item['text'] = $substitute->getPhrase();
    $item['hiddens'] = [
        ['name' => 'substituteId', 'value' => $substitute->getId()],
    ];
    $substitutesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'substitutesBindableForm',
    'items' => $substitutesItems,
    'checked' => false,
]);
