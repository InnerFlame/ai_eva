<?php
declare(strict_types=1);

namespace frontend\models\commonInfo;

use frontend\models\DataProvider;
use frontend\models\placeholders\Placeholder;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;

class CommonInfoForm extends CommunicationLog
{
    // In future maybe this constant will be insert in database and some fields (like country) max length is 3,
    // that is why this constant is short
    public static $IS_NULL_OR_EMPTY_LABEL = 'NaN';
    public static $DEFAULT_VALUE = 'NaN';

    public $uniqueUserCount;
    public $uniqueSessionCount;
    public $placeholderCount;
    public $switchingCount;
    public $rulesCount;

    public $profileTypes = [];
    public $projects = [];
    public $sites = [];
    public $countries = [];
    public $languages = [];
    public $statuses = [];
    public $platforms = [];
    public $trafficSources = [];
    public $scenarios = [];
    public $placeholders = [];
    public $splits = [];
    public $placeholderDispatchTypes = [];

    public $dateFrom;
    public $dateTo;

    public $availableProfileTypes = [];
    public $availableProjects = [];
    public $availableSites = [];
    public $availableCountries = [];
    public $availableLanguages = [];
    public $availableStatuses = [];
    public $availablePlatforms = [];
    public $availableTrafficSources = [];
    public $availableScenarios = [];
    public $availablePlaceholders = [];
    public $availableSplits = [];
    public $availablePlaceholderDispatchTypes = [];

    public $explainSearchQueries = array();
    public $explainPlaceholdersInfoQuery = array();

    protected $queryMessageCount;
    protected $queryUniqueUserSessionCount;
    protected $queryPlaceholderCount;
    protected $querySwitchingCount;
    protected $queryRuleCount;

    public function __construct()
    {
        //First row
        foreach (\Yii::$app->params['placeholdersSendersTypes'] as $value) {
            $this->availableProfileTypes[$value] = $value;
        }
        foreach ($availableProjects = \Yii::$app->params['availableProjects'] as $value) {
            $this->availableProjects[$value] = $value;
        }
        foreach (\Yii::$app->params['sites'] as $key => $value) {
            $this->availableSites[$key] = $value;
        }
        //Second row
        foreach (\Yii::$app->params['countries'] as $value) {
            $this->availableCountries[$value] = $value;
        }
        foreach (\Yii::$app->params['availableLanguages'] as $value) {
            $this->availableLanguages[$value] = $value;
        }
        $this->availableLanguages[self::$IS_NULL_OR_EMPTY_LABEL] = self::$IS_NULL_OR_EMPTY_LABEL;
        foreach (\Yii::$app->params['statuses'] as $value) {
            $this->availableStatuses[$value] = $value;
        }
        $this->availableStatuses[self::$IS_NULL_OR_EMPTY_LABEL] = self::$IS_NULL_OR_EMPTY_LABEL;
        //Third row
        foreach (\Yii::$app->params['platforms'] as $value) {
            $this->availablePlatforms[$value] = $value;
        }
        foreach (\Yii::$app->params['trafficSources'] as $value) {
            $this->availableTrafficSources[$value] = $value;
        }
        $this->availableTrafficSources[self::$IS_NULL_OR_EMPTY_LABEL] = self::$IS_NULL_OR_EMPTY_LABEL;
        //Fourth row
        foreach (DataProvider::getInstance()->getScenarios() as $scenario) {
            $this->availableScenarios[$scenario->getId()] = $scenario->getName();
        }
        foreach (Placeholder::find()->orderBy('id')->all() as $placeholder) {
            $this->availablePlaceholders[$placeholder->id] = $placeholder->name;
        }
        $this->availableSplits = array();
        //Fifth row
        foreach (\Yii::$app->params['placeholderDispatchTypes'] as $key => $value) {
            $this->availablePlaceholderDispatchTypes[$key] = $value;
        }
    }

    public function rules()
    {
        // only this fields will be accessible in search
        return [
            [
                [
                    'profileTypes',
                    'projects',
                    'sites',
                    'countries',
                    'languages',
                    'statuses',
                    'platforms',
                    'trafficSources',
                    'splits',
                    'scenarios',
                    'placeholders',
                ], 'each', 'rule' => ['string']
            ],
            [['dateFrom', 'dateTo',], 'string'],
            [
                [
                    'placeholderDispatchTypes',
                ],  'each', 'rule' => ['integer']
            ],
        ];
    }

    public function search($params, $explain = false)
    {
        $this->queryMessageCount = CommunicationLog::find()
            ->select(['COUNT(userId) AS messageCount, DATE(createdAt) AS createdDate'])
            ->from(['communicationLog FORCE INDEX (createdAt, `country-siteHash`, idFrom, idTo) FORCE INDEX FOR GROUP BY(createdAt)'])
            ->where(['isUser' => true,])
            ->groupBy('createdDate');

        $this->queryUniqueUserSessionCount = CommunicationLog::find()
            ->select(['COUNT(DISTINCT(userId)) AS uniqueUserCount, COUNT(DISTINCT userId, modelId) AS uniqueSessionCount, DATE(createdAt) AS createdDate'])
            ->from(['communicationLog FORCE INDEX (createdAt, `country-siteHash`, idFrom, idTo) FORCE INDEX FOR GROUP BY(createdAt)'])
            ->where(['isUser' => true,])
            ->groupBy('createdDate');

        $this->queryPlaceholderCount = CommunicationLog::find()
            ->select(['COUNT(userId) AS placeholderCount, DATE(createdAt) AS createdDate'])
            ->from(['communicationLog FORCE INDEX (createdAt, `country-siteHash`, idFrom, idTo) FORCE INDEX FOR GROUP BY(createdAt)'])
            ->where(['isUser' => false,])//For speed-up query as placeholders are sending by not users
            ->groupBy('createdDate');

        $this->querySwitchingCount = CommunicationLog::find()
            ->select(['COUNT(userId) AS switchingCount, DATE(createdAt) AS createdDate'])
            ->from(['communicationLog FORCE INDEX (createdAt, `country-siteHash`, idFrom, idTo) FORCE INDEX FOR GROUP BY(createdAt)'])
            ->where(['hasSwitching' => true, 'isUser' => false,])
            ->groupBy('createdDate');

        $this->queryRuleCount = CommunicationLog::find()
            ->select(['COUNT(userId) AS ruleCount, DATE(createdAt) AS createdDate'])
            ->from(['communicationLog FORCE INDEX (createdAt, `country-siteHash`, idFrom, idTo) FORCE INDEX FOR GROUP BY(createdAt)'])
            ->where(['hasRule' => true, 'isUser' => false,])
            ->groupBy('createdDate');

        // load data from form and validate
        if (!($this->load($params) && $this->validate())) {
            return new ArrayDataProvider([
                'allModels' => [],
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
        }

        $this->queryMessageCount->andFilterWhere($this->getWhereConditions());
        $this->queryUniqueUserSessionCount->andFilterWhere($this->getWhereConditions());
        $this->queryPlaceholderCount->andFilterWhere($this->getWhereConditions(false));
        $this->querySwitchingCount->andFilterWhere($this->getWhereConditions());
        $this->queryRuleCount->andFilterWhere($this->getWhereConditions());

        if ($explain) {
            $this->explainSearchQueries[] = \Yii::$app->db->createCommand('EXPLAIN ' . $this->queryMessageCount->createCommand()->getRawSql())
                ->queryAll();
            $this->explainSearchQueries[] = \Yii::$app->db->createCommand('EXPLAIN ' . $this->queryUniqueUserSessionCount->createCommand()->getRawSql())
                ->queryAll();
            $this->explainSearchQueries[] = \Yii::$app->db->createCommand('EXPLAIN ' . $this->queryPlaceholderCount->createCommand()->getRawSql())
                ->queryAll();
            $this->explainSearchQueries[] = \Yii::$app->db->createCommand('EXPLAIN ' . $this->querySwitchingCount->createCommand()->getRawSql())
                ->queryAll();
            $this->explainSearchQueries[] = \Yii::$app->db->createCommand('EXPLAIN ' . $this->queryRuleCount->createCommand()->getRawSql())
                ->queryAll();
            return new ArrayDataProvider([
                'allModels' => [],
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
        }

        $arrMessageCount            = $this->queryMessageCount->asArray()->all();
        $arrUniqueUserSessionCount  = $this->queryUniqueUserSessionCount->asArray()->all();
        $arrPlaceholderCount        = $this->queryPlaceholderCount->asArray()->all();
        $arrSwitchingCount          = $this->querySwitchingCount->asArray()->all();
        $arrRuleCount               = $this->queryRuleCount->asArray()->all();

        $messageCount       = array_column($arrMessageCount, 'messageCount', 'createdDate');
        $userCount          = array_column($arrUniqueUserSessionCount, 'uniqueUserCount', 'createdDate');
        $sessionCount       = array_column($arrUniqueUserSessionCount, 'uniqueSessionCount', 'createdDate');
        $placeholderCount   = array_column($arrPlaceholderCount, 'placeholderCount', 'createdDate');
        $switchingCount     = array_column($arrSwitchingCount, 'switchingCount', 'createdDate');
        $ruleCount          = array_column($arrRuleCount, 'ruleCount', 'createdDate');

        $data = [];
        foreach ($messageCount as $key => $val) {
            $data[] = [
                'createdDate'           => $key,
                'messageCount'          => $val,
                'uniqueUserCount'       => $userCount[$key] ?? '',
                'uniqueSessionCount'    => $sessionCount[$key] ?? '',
                'placeholderCount'      => $placeholderCount[$key] ?? '',
                'switchingCount'        => $switchingCount[$key] ?? '',
                'ruleCount'             => $ruleCount[$key] ?? '',
            ];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $dataProvider;
    }

    private function getWhereConditions($hasNullPlaceholder = true)
    {
        $query = [];
        $query[] = 'and';
        $query[] = ['profileType' => $this->profileTypes];
        $query[] = ['siteHash' => $this->sites];
        $query[] = ['country' => $this->countries];
        if ($this->languages != null && in_array(self::$IS_NULL_OR_EMPTY_LABEL, $this->languages)) {
            $query[] = [
                'or',
                ['language' => $this->languages],
                'language IS NULL OR language = \'\''
            ];
        } else {
            $query[] = ['language' => $this->languages];
        }
        if ($this->statuses != null && in_array(self::$IS_NULL_OR_EMPTY_LABEL, $this->statuses)) {
            $query[] = [
                'or',
                ['status' => $this->statuses],
                'status IS NULL OR status = \'\''
            ];
        } else {
            $query[] = ['status' => $this->statuses];
        }
        $query[] = ['platform' => $this->platforms];
        if ($this->trafficSources != null && in_array(self::$IS_NULL_OR_EMPTY_LABEL, $this->trafficSources)) {
            $query[] = [
                'or',
                ['trafficSource' => $this->trafficSources],
                'trafficSource IS NULL OR trafficSource = \'\''
            ];
        } else {
            $query[] = ['trafficSource' => $this->trafficSources];
        }
        $query[] = ['scenarioId' => $this->scenarios];
        if (!$hasNullPlaceholder) {
            $query[] = ['placeholderId' => $this->placeholders];
        }

        $query[] = [
            'between',
            'createdAt', $this->dateFrom . ' 00:00:00',
            ((isset($this->dateTo) && $this->dateTo != '') ? $this->dateTo : $this->dateFrom) . ' 23:59:59'
        ];

        $query[] = ['predispatched' => $this->placeholderDispatchTypes];

        return $query;
    }

    public function placeholdersInfo($params, $explain = false)
    {
        //SELECT COUNT(*), DATE(createdAt) AS createdDate, placeholderId, country FROM communicationLog WHERE isUser = false AND placeholderId > 0 GROUP BY createdDate, placeholderId, country
        $query = CommunicationLog::find()
            ->select(['COUNT(*) AS placeholdersCount, DATE(createdAt) AS createdDate, placeholderId, country'])
            ->from(['communicationLog FORCE INDEX (createdAt)'])
            ->where(['isUser' => false])
            ->andWhere('placeholderId > 0')
            ->groupBy(['createdDate', 'placeholderId', 'country']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'createdDate',
                'placeholderId',
                'country',
                'placeholdersCount',
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            $query->andFilterWhere(['createdAt' => '0']);
            return $dataProvider;
        }

        $query->andFilterWhere($this->getWhereConditions(false));

        if ($explain) {
            $this->explainPlaceholdersInfoQuery = \Yii::$app->db->createCommand('EXPLAIN ' . $query->createCommand()->getRawSql())
                ->queryAll();
            $query->andFilterWhere(['createdAt' => '0']);
        }

        return $dataProvider;
    }
}
