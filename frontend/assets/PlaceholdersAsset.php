<?php

namespace frontend\assets;

class PlaceholdersAsset extends BasicAsset
{
    public $css = [
        'css/placeholders.css',
    ];
    public $js = [
        'js/placeholders.js',
    ];
}
