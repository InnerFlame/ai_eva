<?php
use common\widgets\BindableFormItems\BindableFormItems;

$rootClassifiersItems = [];
foreach ($model->classifiers as $rootClassifier) {
    $item['text']    = $rootClassifier->text;
    $item['hiddens'] = [
        [
            'name'  => 'rootClassifierId',
            'value' => $rootClassifier->id,
        ],
        [
            'name'  => 'rootClassifierModelText',
            'value' => $rootClassifier->model,
        ],
        [
            'name'  => 'rootClassifierModel',
            'value' => $rootClassifier->model,
        ],
    ];
    $rootClassifiersItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'rootClassifiersBindableForm',
    'items'          => $rootClassifiersItems,
]);
