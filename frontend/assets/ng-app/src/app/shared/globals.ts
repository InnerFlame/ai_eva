import { HttpHeaders }         from '@angular/common/http';

/**
 * This file contains of global variables and functions
 */
export class Globals
{
    static getCSRFToken() {
        return document.head.querySelector("meta[name=csrf-token]").getAttribute("content");
    }

    static getRequestOptions()
    {
        let options = {
            headers: new HttpHeaders()
                        .set('X-CSRF-Token', Globals.getCSRFToken())
                        .set('Content-Type', 'application/json; charset=utf-8'),
        };

        return options;
    }
}
