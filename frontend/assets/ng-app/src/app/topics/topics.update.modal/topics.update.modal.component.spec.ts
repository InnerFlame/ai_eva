import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Topics.Update.ModalComponent } from './topics.update.modal.component';

describe('Topics.Update.ModalComponent', () => {
  let component: Topics.Update.ModalComponent;
  let fixture: ComponentFixture<Topics.Update.ModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Topics.Update.ModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Topics.Update.ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
