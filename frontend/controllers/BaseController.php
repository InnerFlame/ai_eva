<?php
declare(strict_types=1);

namespace frontend\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

class BaseController extends Controller
{
    const RETURN_RAW_RESPONSE = 0;
    const RETURN_RESPONSE_WITH_ERR_ITEM_CONDITION = 1;
    const RETURN_RESPONSE_WITH_AFFECTED_ROWS_CONDITION = 2;
    const RETURN_RESPONSE_WITH_ITEM = 3;

    public function __construct($id, $module, $config = [])
    {
        $isLocked = \Yii::$app->cache->get('adminLock');
        if ($isLocked) {
            die('Admin panel is on maintenance. It will be released in hour. Try to ask releaser for details');
        }

        parent::__construct($id, $module, $config);
    }

    private function ajaxGetTemplate($getDataFromPost)
    {
        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            return $getDataFromPost($post);
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    private function getResponseCondition($returnResponseType)
    {
        $successResponseCondition = null;
        switch ($returnResponseType) {
            case self::RETURN_RAW_RESPONSE:
                $successResponseCondition = function ($response) {
                    return $response;
                };
                break;
            case self::RETURN_RESPONSE_WITH_ERR_ITEM_CONDITION:
                $successResponseCondition = function ($response) {
                    return $response[0]['err'] == null && isset($response[0]['item']);
                };
                break;
            case self::RETURN_RESPONSE_WITH_AFFECTED_ROWS_CONDITION:
                $successResponseCondition = function ($response) {
                    return isset($response['affectedRows']) && $response['affectedRows'] != 0;
                };
                break;
            case self::RETURN_RESPONSE_WITH_ITEM:
                $successResponseCondition = function ($response) {
                    return (
                        isset($response['item']) &&
                        isset($response['item']['scenario_id']) &&
                        $response['item']['scenario_id'] != 0
                    );
                };
                break;
            default:
                throw new NotFoundHttpException('Page not found.');
        }
        return $successResponseCondition;
    }

    private function ajaxSetTemplate($getDataFromPost, $returnResponseType)
    {
        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $getDataFromPost($post);
            $successResponseCondition = $this->getResponseCondition($returnResponseType);
            if ($successResponseCondition($response)) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    protected function ajaxGetDataTemplate($getDataFromPost)
    {
        return $this->ajaxGetTemplate($getDataFromPost);
    }

    protected function ajaxCreateDataTemplate(
        $getDataFromPost,
        $returnResponseType = self::RETURN_RESPONSE_WITH_ERR_ITEM_CONDITION
    ) {
        return $this->ajaxSetTemplate($getDataFromPost, $returnResponseType);
    }

    protected function ajaxUpdateDataTemplate(
        $getDataFromPost,
        $returnResponseType = self::RETURN_RESPONSE_WITH_AFFECTED_ROWS_CONDITION
    ) {
        return $this->ajaxSetTemplate($getDataFromPost, $returnResponseType);
    }

    protected function ajaxDeleteDataTemplate(
        $getDataFromPost,
        $returnResponseType = self::RETURN_RESPONSE_WITH_AFFECTED_ROWS_CONDITION
    ) {
        return $this->ajaxSetTemplate($getDataFromPost, $returnResponseType);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }
}
