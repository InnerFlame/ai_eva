<?php
namespace common\components;

use yii\base\Component;
use frontend\models\placeholders\PlaceholdersForm;

class PlaceholdersComponent extends Component
{
    public function replacePlaceholders($text, $params)
    {
        $placeholdersModel = new PlaceholdersForm();

        $placeholders = $placeholdersModel->getPlaceholdersByParams($params['controlProfileType'], $params['country'], $params['platform']);

        $lastPlaceholderId = null;
        foreach ($placeholders as $placeholder) {
            if (strpos($text, $placeholder['name']) === false) {
                continue;
            }

            if ($placeholder['url_type'] == 'dynamic') {
                $placeholder['url'] = str_replace('{sender_id}', $params['idTo'], $placeholder['url']);
                $placeholder['url'] = str_replace('{recipient_id}', $params['idFrom'], $placeholder['url']);
                $placeholder['url'] = str_replace('{country}', $params['country'], $placeholder['url']);
                $placeholder['url'] = str_replace('{status}', $params['status'], $placeholder['url']);
                $placeholder['url'] = str_replace('{site}', $params['site'], $placeholder['url']);
                $placeholder['url'] = str_replace('{traffic_source}', $params['trafficSource'], $placeholder['url']);
                $placeholder['url'] = str_replace('{action_way}', $params['actionWay'], $placeholder['url']);
                $placeholder['url'] = str_replace('{platform}', $params['platform'], $placeholder['url']);
                $placeholder['url'] = str_replace('{screenname}', $params['screenname'], $placeholder['url']);
                $sitename = \Yii::$app->params['sites'][$params['site']] ?? '';
                $placeholder['url'] = str_replace('{sitename}', $sitename, $placeholder['url']);
                $placeholder['url'] = str_replace('{shortId}', $params['shortId'], $placeholder['url']);
                $placeholder['url'] = str_replace('{providerName}', $params['providerName'], $placeholder['url']);
            }

            $lastPlaceholderId = $placeholder['id'];

            $text = str_replace(
                $placeholder['name'],
                $placeholder['url'],
                $text
            );
        }

        $response = [
            'text'          => $text,
            'placeholderId' => $lastPlaceholderId,
        ];

        return $response;
    }
}
