<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class Site extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_sites_table';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
