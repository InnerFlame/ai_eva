<?php
declare(strict_types=1);

namespace frontend\models\nlpClassifiers;

use frontend\models\BaseForm;
use frontend\models\DataProvider;
use yii\base\InvalidParamException;

class NlpClassifiersForm extends BaseForm
{
    const CLASSIFIERS_TYPES = [
        'ml_classifier',
        'root_ml_classifier'
    ];

    public $classifiers = [];
    public $classes     = [];
    public $phrases     = [];

    public function getClassifiers()
    {
        $data = DataProvider::getInstance()->getNlpClassifiers();
        $this->classifiers = $this->unifyClassifiers($data, 'root_ml_classifier');
    }

    public function getRootClassifiers()
    {
        $data = DataProvider::getInstance()->getNlpClassifiers();
        $this->classifiers = $this->unifyClassifiers($data, 'ml_classifier');
    }

    public function getClasses(int $classifierId)
    {
        $this->classes = DataProvider::getInstance()->getNlpClassifierClasses($classifierId);
    }

    public function getPhrases(int $classId)
    {
        $this->phrases = DataProvider::getInstance()->getNlpClassPhrases($classId);
    }

    public function getPhrasesIds(int $classId)
    {
        return DataProvider::getInstance()->getNlpClassPhrasesIds($classId);
    }

    private function unifyClassifiers($data, $typeToExclude)
    {
        if (!in_array($typeToExclude, self::CLASSIFIERS_TYPES)) {
            throw new InvalidParamException('Invalid type to exclude');
        }

        /** @var $data NlpClassifier [] */
        foreach ($data as $key => $classifier) {
            if($classifier->type == $typeToExclude) {
                unset($data[$key]);
            }
        }

        return $data;
     }

    public function getEntityForPhrase($phraseId)
    {
        return DataProvider::getInstance()->getPhraseEntities($phraseId);
    }

    public function deleteEntityForPhrase($entityId)
    {
        return DataProvider::getInstance()->deletePhraseEntity($entityId);
    }

    public function createMultiEntityForPhrase(array $data)
    {
        $res = false;

        foreach ($data as $datum) {
            $res = DataProvider::getInstance()->createPhraseEntity($datum);
        }

        return $res;
    }

    public function createEntityForPhrase(array $data)
    {
        return DataProvider::getInstance()->createPhraseEntity($data);
    }

    public function updateEntityForPhrase(array $data)
    {
        return DataProvider::getInstance()->updatePhraseEntity($data);
    }

    public function updateMultiEntityForPhrase(array $data)
    {
        $res = false;

        foreach ($data as $datum) {
            $res = DataProvider::getInstance()->updatePhraseEntity($datum);
        }

        return $res;
    }
}
