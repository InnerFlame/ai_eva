var phrasesArray = new Array();
var blocksArray = new Array();

function init()
{
    //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
        $(
            go.Diagram,
            "scenarioDiagram",  // must name or refer to the DIV HTML element
            {
                initialContentAlignment: go.Spot.Center,
                allowDrop: true,  // must be true to accept drops from the Palette
                //"LinkDrawn": showLinkLabel,  // this DiagramEvent listener is defined below
                //"LinkRelinked": showLinkLabel,
                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                "undoManager.isEnabled": true  // enable undo & redo
            }
        );

    // helper definitions for node templates
    function nodeStyle()
    {
        return [
            // The Node.location comes from the "loc" property of the node data,
            // converted by the Point.parse static method.
            // If the Node.location is changed, it updates the "loc" property of the node data,
            // converting back using the Point.stringify static method.
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            {
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center,
                //isShadowed: true,
                //shadowColor: "#888",
                // handle mouse enter/leave events to show/hide the ports
                mouseEnter: function (e, obj) { showPorts(obj.part, true); },
                mouseLeave: function (e, obj) { showPorts(obj.part, false); }
            }
        ];
    }

    // Define a function for creating a "port" that is normally transparent.
    // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
    // and where the port is positioned on the node, and the boolean "output" and "input" arguments
    // control whether the user can draw links from or to the port.
    function makePort(name, spot, output, input, fromMaxLinks)
    {
        // the port is basically just a small circle that has a white stroke when it is made visible
        return $(
            go.Shape,
            "Circle",
            {
                fill: "transparent",
                stroke: null,  // this is changed to "white" in the showPorts function
                desiredSize: new go.Size(8, 8),
                alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
                portId: name,  // declare this object to be a "port"
                fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                cursor: "pointer",  // show a different cursor to indicate potential link point
                fromMaxLinks: fromMaxLinks ? fromMaxLinks : 1
            }
        );
    }

    var lightText = 'whitesmoke';

    myDiagram.nodeTemplateMap.add(
        "start",
        $(
            go.Node,
            "Auto",
            nodeStyle(),
            $(go.Shape, "RoundedRectangle", {fill: "#5cb85c", width: 130, height: 40, stroke: null}),
            $(
                go.TextBlock,
                {
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    stroke: lightText,
                    margin: 5,
                    textAlign: "center",
                    wrap: go.TextBlock.WrapFit
                },
                new go.Binding("text", "key")
            ),
            makePort("output", go.Spot.Bottom, true, false, 2)
        )
    );

    myDiagram.nodeTemplateMap.add(
        "stop",
        $(
            go.Node,
            "Auto",
            nodeStyle(),
            $(go.Shape, "RoundedRectangle", {fill: "#d9534f", width: 130, height: 40, stroke: null}),
            $(go.TextBlock,
                {
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    stroke: lightText,
                    margin: 5,
                    textAlign: "center",
                    wrap: go.TextBlock.WrapFit
                },
                new go.Binding("text", "key")
            ),
            makePort("input", go.Spot.Top, false, true)
        )
    );

    myDiagram.nodeTemplateMap.add(
        "phrases",
        $(
            go.Node,
            "Auto",
            nodeStyle(),
            $(go.Shape, "RoundedRectangle", {fill: "#428bca", width: 130, height: 40, stroke: null}),
            $(go.TextBlock,
                {
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    stroke: lightText,
                    margin: 5,
                    textAlign: "center",
                    wrap: go.TextBlock.WrapFit
                },
                new go.Binding("text", "key")
            ),
            makePort("input", go.Spot.Top, false, true),
            makePort("output", go.Spot.Bottom, true, false, 2)
        )
    );

    myDiagram.nodeTemplateMap.add(
        "scenario",
        $(
            go.Node,
            "Auto",
            nodeStyle(),
            $(go.Shape, "RoundedRectangle", {fill: "#f0ad4e", width: 130, height: 40, stroke: null}),
            $(go.TextBlock,
                {
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    stroke: lightText,
                    margin: 5,
                    textAlign: "center",
                    wrap: go.TextBlock.WrapFit
                },
                new go.Binding("text", "key")
            ),
            makePort("input", go.Spot.Top, false, true),
            makePort("output", go.Spot.Bottom, true, false)
        )
    );

    myDiagram.nodeTemplateMap.add(
        "classifier",
        $(
            go.Node,
            "Auto",
            nodeStyle(),
            $(go.Shape, "RoundedRectangle", { fill: "#5bc0de", width: 130, stroke: null}),
            $(
                go.Panel,
                "Vertical",
                $(
                    go.TextBlock,
                    {margin: 5, textAlign: "center", font: "bold 11pt Helvetica, Arial, sans-serif"},
                    new go.Binding("text", "key")
                ),
                $(
                    go.Panel,
                    "Table",
                    $(go.RowColumnDefinition, { column: 0, alignment: go.Spot.Left }),
                    $(go.RowColumnDefinition, { column: 2, alignment: go.Spot.Right }),
                    $(
                        go.Panel,
                        "Horizontal",
                        { column: 0, row: 1 },
                        $(
                            go.Shape,
                            "Circle",
                            {
                                fill: "lightblack",
                                stroke: null,
                                desiredSize: new go.Size(8, 8),
                                alignment: go.Spot.Left,
                                alignmentFocus: go.Spot.Left,
                                portId: "yes",
                                fromSpot: go.Spot.Left,
                                toSpot: go.Spot.Left,
                                fromLinkable: true,
                                toLinkable: false,
                                cursor: "pointer",
                                fromMaxLinks: 1
                            }
                        ),
                        $(
                            go.TextBlock,
                            { margin: 3, textAlign: "left", text: "yes", font: "11pt Helvetica, Arial, sans-serif"}
                        )
                    ),
                    $(
                        go.Panel,
                        "Horizontal",
                        { column: 2, row: 1 },
                        $(
                            go.TextBlock,
                            { margin: 3, textAlign: "right", text: "no", font: "11pt Helvetica, Arial, sans-serif"}
                        ),
                        $(
                            go.Shape,
                            "Circle",
                            {
                                fill: "lightblack",
                                stroke: null,
                                desiredSize: new go.Size(8, 8),
                                alignment: go.Spot.Right,
                                alignmentFocus: go.Spot.Right,
                                portId: "no",
                                fromSpot: go.Spot.Right,
                                toSpot: go.Spot.Right,
                                fromLinkable: true,
                                toLinkable: false,
                                cursor: "pointer",
                                fromMaxLinks: 1
                            }
                        )
                    )
                )
            ),
            makePort("input", go.Spot.Top, false, true)
        )
    );

    var itemTpl = $(
        go.Panel,
        "Horizontal",
        $(
            go.Shape,
            "Circle",
            {
                fill: "lightblack",
                stroke: null,
                desiredSize: new go.Size(8, 8),
                alignment: go.Spot.Left,
                alignmentFocus: go.Spot.Left,
                portId: "L",
                fromSpot: go.Spot.Left,
                toSpot: go.Spot.Left,
                fromLinkable: true,
                toLinkable: false,
                cursor: "pointer"
            },
            new go.Binding("portId", "key")
        ),
        $(go.TextBlock, {margin: 5, textAlign: "left"}, new go.Binding("text", "key"))
    );

    myDiagram.nodeTemplateMap.add(
        "ml_classifier",
        $(
            go.Node,
            "Auto",
            nodeStyle(),
            $(go.Shape, "RoundedRectangle", { fill: "lightblue", width: 130}),
            $(go.Panel, "Table", { margin: 8, stretch: go.GraphObject.Fill},
                $(go.RowColumnDefinition, { row: 0, sizing: go.RowColumnDefinition.None }),
                $(go.TextBlock, {margin: 5, textAlign: "left"}, new go.Binding("text", "key")),
                $(
                    "PanelExpanderButton",
                    "LIST",
                    {
                        row: 0,
                        alignment: go.Spot.TopRight
                    }
                ),
                $(
                    go.Panel,
                    "Vertical",
                    {
                        name: "LIST",
                        row: 1,
                        padding: 3,
                        alignment: go.Spot.TopLeft,
                        defaultAlignment: go.Spot.Left,
                        stretch: go.GraphObject.Horizontal,
                        itemTemplate: itemTpl
                    },
                    new go.Binding("itemArray", "items")
                )
            ),
            makePort("input", go.Spot.Top, false, true)
        )
    );

    myDiagram.nodeTemplateMap.add(
        "mre_classifier",
        $(
            go.Node,
            "Auto",
            nodeStyle(),
            $(go.Shape, "RoundedRectangle", { fill: "#F2F5A9", width: 130}),
            $(go.Panel, "Table", { margin: 8, stretch: go.GraphObject.Fill},
                $(go.RowColumnDefinition, { row: 0, sizing: go.RowColumnDefinition.None }),
                $(go.TextBlock, {margin: 5, textAlign: "left"}, new go.Binding("text", "key")),
                $(
                    "PanelExpanderButton",
                    "LIST",
                    {
                        row: 0,
                        alignment: go.Spot.TopRight
                    }
                ),
                $(
                    go.Panel,
                    "Vertical",
                    {
                        name: "LIST",
                        row: 1,
                        padding: 3,
                        alignment: go.Spot.TopLeft,
                        defaultAlignment: go.Spot.Left,
                        stretch: go.GraphObject.Horizontal,
                        itemTemplate: itemTpl
                    },
                    new go.Binding("itemArray", "items")
                )
            ),
            makePort("input", go.Spot.Top, false, true)
        )
    );


    myDiagram.nodeTemplateMap.add(
        "root_ml_classifier",
        $(
            go.Node,
            "Auto",
            nodeStyle(),
            $(go.Shape, "RoundedRectangle", { fill: "#EEEEEE", width: 130}),
            $(go.Panel, "Table", { margin: 8, stretch: go.GraphObject.Fill},
                $(go.RowColumnDefinition, { row: 0, sizing: go.RowColumnDefinition.None }),
                $(go.TextBlock, {margin: 5, textAlign: "left"}, new go.Binding("text", "key")),
                $(
                    "PanelExpanderButton",
                    "LIST",
                    {
                        row: 0,
                        alignment: go.Spot.TopRight
                    }
                ),
                $(
                    go.Panel,
                    "Vertical",
                    {
                        name: "LIST",
                        row: 1,
                        padding: 3,
                        alignment: go.Spot.TopLeft,
                        defaultAlignment: go.Spot.Left,
                        stretch: go.GraphObject.Horizontal,
                        itemTemplate: itemTpl
                    },
                    new go.Binding("itemArray", "items")
                )
            )
        )
    );

    myDiagram.linkTemplate =
        $(
            go.Link,
            go.Link.Orthogonal,
            {
                routing: go.Link.AvoidsNodes,
                curve: go.Link.JumpOver,
                corner: 10, toShortLength: 4,
                relinkableFrom: true,
                relinkableTo: true,
                reshapable: true,
                resegmentable: true
            },
            $(go.Shape, { strokeWidth: 1 }),
            $(go.Shape, { toArrow: "Standard" })
        );

    // Make link labels visible if coming out of a "conditional" node.
    // This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
    //function showLinkLabel(e) {
    //    var label = e.subject.findObject("LABEL");
    //    if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
    //}

    // temporary links used by LinkingTool and RelinkingTool are also orthogonal:
    myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
    myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;

    //load();  // load an initial diagram from some JSON text

    //Load phrases
    phrasesArray.push({ category: "start", key: "Start" });
    jQuery.ajax({
        url: '/scenarios/group-phrases',
        async: false,
        success: function (data) {
            phrases = JSON.parse(data);
            for (i = 0; i < phrases.length; i++) {
                phrasesArray.push({ category: "phrases", key: phrases[i].text, groupGroupsPhrasesId: phrases[i].groupGroupsPhrasesId });
            }
        }
    });
    phrasesArray.push({ category: "stop", key: "Stop" });

    //Load blocks
    jQuery.ajax({
        url: '/scenarios/blocks',
        async: false,
        success: function (data) {
            blocks = JSON.parse(data);
            for (i = 0; i < blocks.length; i++) {
                blocksArray.push({ category: "scenario", key: blocks[i].name });
            }
        }
    });

    //Load classifiers
    var classifiersArray = [];
    jQuery.ajax({
        url: '/scenarios/classifiers',
        async: false,
        success: function (data) {
            var classifiersNames = JSON.parse(data);
            for (var i = 0; i < classifiersNames.length; i++) {
                classifiersArray.push({ category: "classifier", key: classifiersNames[i] });
            }
        }
    });

    //Load NLP classifiers
    var nlpClassifiersArray = [];
    jQuery.ajax({
        url: '/scenarios/nlp-classifiers',
        async: false,
        success: function (data) {
            var nlpClassifiers = JSON.parse(data);
            for (var i = 0; i < nlpClassifiers.length; i++) {
                var items = [];
                for (var j = 0; j < nlpClassifiers[i].items.length; j++) {
                    items.push({key: nlpClassifiers[i].items[j]});
                }

                if (nlpClassifiers[i].type == "ml_classifier") {
                    items.push({key: "not classified"});
                }

                if (["root_ml_classifier", "ml_classifier", "mre_classifier"].includes(nlpClassifiers[i].type)) {
                    nlpClassifiersArray.push({
                        category: nlpClassifiers[i].type,
                        key: nlpClassifiers[i].name,
                        items: items
                    });
                }
            }
        }
    });

    // initialize the Palette that is on the left side of the page
    palettePhrases =
        $(
            go.Palette,
            "palettePhrases",  // must name or refer to the DIV HTML element
            {
                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                model: new go.GraphLinksModel(phrasesArray)
            }
        );

    paletteClassifiers =
        $(
            go.Palette,
            "paletteClassifiers",  // must name or refer to the DIV HTML element
            {
                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                model: new go.GraphLinksModel(classifiersArray)
            }
        );

    paletteNlpClassifiers =
        $(
            go.Palette,
            "paletteNlpClassifiers",  // must name or refer to the DIV HTML element
            {
                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                model: new go.GraphLinksModel(nlpClassifiersArray)
            }
        );

    paletteBlocks =
        $(
            go.Palette,
            "paletteBlocks",  // must name or refer to the DIV HTML element
            {
                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                model: new go.GraphLinksModel(blocksArray)
            }
        );
}

// Make all ports on a node visible when the mouse is over the node
function showPorts(node, show)
{
    var diagram = node.diagram;
    if (!diagram || diagram.isReadOnly || !diagram.allowLink) {
        return;
    }
    node.ports.each(function (port) {
        port.stroke = (show ? "white" : null);
    });
}

// Show the diagram's model in JSON format that the user may edit
function save()
{
    document.getElementById("mySavedModel").value = myDiagram.model.toJson();
    myDiagram.isModified = false;
}

function load()
{
    if (jQuery('#scenarios ul li.current input[name="scenarioFlowChart"]').val() == '{}') {
        jQuery('#scenarios ul li.current input[name="scenarioFlowChart"]').val('{ "class": "go.GraphLinksModel", "linkFromPortIdProperty": "fromPort", "linkToPortIdProperty": "toPort", "nodeDataArray": [], "linkDataArray": []}');
    }
    myDiagram.model = go.Model.fromJson(jQuery('#scenarios ul li.current input[name="scenarioFlowChart"]').val());
}

// add an SVG rendering of the diagram at the end of this page
//function makeSVG() {
//    var svg = myDiagram.makeSvg({
//        scale: 0.5
//    });
//    svg.style.border = "1px solid black";
//    obj = document.getElementById("SVGArea");
//    obj.appendChild(svg);
//    if (obj.children.length > 0) {
//        obj.replaceChild(svg, obj.children[0]);
//    }
//}

$(document).ready(function () {
    init();
});
