<?php
use common\widgets\BindableFormItems\BindableFormItems;

$patternsItems = array();
foreach ($model->subRule->getPatterns() as $pattern) {
    $item['images'] = [
        [
            'class' => 'glyphicon glyphicon-signal' . ($pattern->isActive() ? ' active' : ''),
            'name' => 'activePatternIcon',
        ],
    ];
    $item['text'] = $pattern->getText();
    $item['hiddens'] = [
        ['name' => 'testText', 'value' => $pattern->getTest()],
        ['name' => 'patternId', 'value' => $pattern->getPatternId()],
    ];
    $patternsItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'subPatternsBindableForm',
    'items' => $patternsItems,
    'checked' => true,
]);
