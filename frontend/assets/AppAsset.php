<?php

namespace frontend\assets;

class AppAsset extends BasicAsset
{
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/site.js',
    ];
}
