<?php

use yii\db\Migration;

/**
 * Handles the creation of table `scenarios_tags`.
 */
class m170517_083320_create_tags_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('scenarios_tags', [
            'id'            => $this->primaryKey(),
            'scenarioId'    => $this->integer(11),
            'tagId'         => $this->integer(),
            'createdAt'     => "timestamp DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->createIndex('scenarioId-tagId', 'scenarios_tags', ['scenarioId', 'tagId']);

        $this->createTable('topics_tags', [
            'id'        => $this->primaryKey(),
            'topicId'   => $this->integer(11),
            'tagId'     => $this->integer(),
            'createdAt' => "timestamp DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->createIndex('topicId-tagId', 'topics_tags', ['topicId', 'tagId']);

        $this->createTable('users_tags', [
            'id'        => $this->primaryKey(),
            'userId'    => $this->string(32),
            'tagId'     => $this->integer(),
            'createdAt' => "timestamp DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->createIndex('userId-tagId', 'users_tags', ['userId', 'tagId']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('scenarios_tags');
        $this->dropTable('topics_tags');
        $this->dropTable('users_tags');
    }
}
