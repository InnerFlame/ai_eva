<?php

use yii\db\Migration;

class m170210_150504_add_locale_to_consolidatedSettings extends Migration
{
    public function up()
    {
        $this->addColumn('consolidatedSettings', 'locale_name', $this->string(3)->defaultValue('DEF'));
    }

    public function down()
    {
        $this->dropColumn('consolidatedSettings', 'locale_name');
    }
}
