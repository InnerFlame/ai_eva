<?php

namespace console\controllers;

use common\models\User;
use yii\console\Controller;
use yii\db\Exception;
use yii\helpers\Console;

class MaintenanceController extends Controller
{
    public function actionWaitForDb()
    {
        while (true) {
            sleep(5);
            try {
                \Yii::$app->db->open();
                Console::output('Database connection ready');
                break;
            } catch (Exception $e) {
                Console::error($e->getMessage());
                Console::output('Wait for database connection');
            }
        }
    }

    public function actionCreateUserIfNotExists()
    {
        if (User::find()->count() == 0) {
            Console::output('Create user "test"');
            $user = new User();
            $user->username = 'test';
            $user->email = 'test@example.com';
            $user->setPassword('testpasswd123');
            $user->generateAuthKey();
            $user->save();
        }
    }
}
