<?php
namespace console\controllers;

use frontend\models\Dialogues;
use yii\console\Controller;

// This function can be call from terminal - 'php yii clear-jumps-and-dialogues'
// It can be call in any time
// This function deletes rows earlier than two weeks
class ClearJumpsAndDialoguesController extends Controller
{
    public function actionIndex()
    {
        echo 'Clear Dialogues tables start...' . PHP_EOL;

        \Yii::$app->db
            ->createCommand('DELETE FROM ' . Dialogues::tableName() . ' WHERE createdAt <= (NOW() - INTERVAL 2 MONTH)')
            ->execute();

        echo 'Clear  Dialogues tables done.' . PHP_EOL;
    }
}
