<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class GojsAsset extends AssetBundle
{
    public $sourcePath = '@npm/gojs/release';
    public $js = [
        'go.js',
    ];
}
