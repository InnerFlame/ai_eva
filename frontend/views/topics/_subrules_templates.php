<?php
use common\widgets\BindableFormItems\BindableFormItems;

$templatesItems = array();
foreach ($model->subRule->getTemplates() as $template) {
    $item['text'] = $template->getText();
    $item['hiddens'] = [
        ['name' => 'templateId', 'value' => $template->getTemplateId()],
    ];
    $templatesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'subTemplatesBindableForm',
    'items' => $templatesItems,
    'checked' => true,
]);
