<?php
use common\widgets\BindableFormItems\BindableFormItems;

$classifiersItems = [];
foreach ($model->classifiers as $classifier) {
    $item['text']    = $classifier->text;
    $item['hiddens'] = [
        [
            'name'  => 'classifierId',
            'value' => $classifier->id,
        ],
        [
            'name'  => 'classifierModelText',
            'value' => $classifier->model,
        ],
        [
            'name'  => 'classifierModel',
            'value' => $classifier->model,
        ],
    ];
    $classifiersItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'classifiersBindableForm',
    'items'          => $classifiersItems,
]);
