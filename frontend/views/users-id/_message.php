<div class="row alert alert-info message">
    <div class="col-md-2">
        <?= $model->createdAt ?>
    </div>
    <div class="col-md-2 overflow-to-points">
        <?= $model->isUser ? $model->userId : $model->modelId ?>
    </div>
    <div class="col-md-8">
        <?= $model->isUser ? '<span class="glyphicon glyphicon-user"></span>' : '' ?>
        <?= '<b>' . $model->screenname . '</b> :' ?>
        <?= $model->message ?>
    </div>
</div>
