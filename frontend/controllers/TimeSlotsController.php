<?php
namespace frontend\controllers;

use frontend\models\timeSlots\TimeSlotsModel;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class TimeSlotsController
 * @package frontend\controllers
 */
class TimeSlotsController extends BaseNgController
{
    /**
     * @inheritdoc
     * 
     * TimeSlotsController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new TimeSlotsModel());
    }

    /**
     * @inheritdoc
     *
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('topics')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        }

        return false;
    }

    /**
     * Renders main test phrases page.
     *
     * @return string Result of rendering.
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionGetModels(): array
    {
        $timeSlots = $this->getFormModel()->getTimeSlots();

        return $timeSlots;
    }

    /**
     * @return array
     */
    public function actionCreate(): array
    {
        $formData = $this->requestData['formData'] ?? [];

        if (empty($formData)) {
            return [];
        }

        $createdTimeslot = $this->getFormModel()->createTimeSlot($formData);

        if (!empty($createdTimeslot['err'])) {
            return [];
        }

        return $this->actionGetModels();
    }

    /**
     * @return array
     */
    public function actionUpdate(): array
    {
        $formData = $this->requestData['formData'] ?? [];

        if (empty($formData)) {
            return [];
        }

        $updatedTimeslot = $this->getFormModel()->updateTimeSlot($formData);

        if (!empty($updatedTimeslot['err'])) {
            return [];
        }

        return $updatedTimeslot;
    }

    /**
     * @param $timeSlotId
     * @return array
     */
    public function actionGetTimeslotData($timeSlotId): array
    {
        $timeSlots = $this->actionGetModels();

        foreach ($timeSlots as $timeSlot) {
            if ($timeSlot['timeslot_id'] == $timeSlotId) {
                return $timeSlot;
            }
        }

        return [];
    }

    /**
     * @return array
     */
    public function actionDelete(): array
    {
        $timeSlotId = $this->requestData['timeslot_id'] ?? null;

        if(is_null($timeSlotId)) {
            return [];
        }

        $deleteTimeslot = $this->getFormModel()->deleteTimeSlot((int) $timeSlotId);

        if (!empty($deleteTimeslot['err'])) {
            return [];
        }

        return $this->actionGetModels();
    }

    /**
     * @return TimeSlotsModel
     */
    protected function getFormModel(): TimeSlotsModel
    {
        return $this->formModel;
    }
}
