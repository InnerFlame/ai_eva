<?php

namespace common\widgets\BindableFormItems;

use yii\web\AssetBundle;

class BindableFormItemsAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/BindableFormItems/assets';

    public $css = [
        'css/bindableformitems.css',
    ];

    public $js = [
        'js/bindableformitems.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        //'forceCopy'=>true,
    ];
}
