<?php
declare(strict_types=1);

namespace frontend\models\topics;

class Topic
{
    private $id;
    private $name;
    private $priority;
    private $enabled;
    private $active;
    private $switching;
    private $deleted;
    private $version;
    private $statusOut;
    private $languageTag;
    public  $restrictions = [];
    public  $tags = [];
    public  $isExclude;
    public  $countTriggering;

    /**
     * Topic constructor.
     * @param $id
     * @param $name
     * @param $priority
     * @param $enabled
     * @param $active
     * @param $switching
     * @param $deleted
     * @param $version
     * @param $statusOut
     */
    public function __construct(
        int     $id,
        string  $name,
        int     $priority,
        bool    $enabled,
        bool    $active,
        bool    $switching,
        bool    $deleted,
        int     $version,
        bool    $statusOut,
        string  $languageTag = ''
    ) {
        $this->id           = $id;
        $this->name         = $name;
        $this->priority     = $priority;
        $this->enabled      = $enabled == null ? false : $enabled;
        $this->active       = $active;
        $this->switching    = $switching;
        $this->deleted      = $deleted;
        $this->version      = $version;
        $this->statusOut    = $statusOut;
        $this->languageTag  = $languageTag;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPriority() : int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return bool
     */
    public function isEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isSwitching() : bool
    {
        return $this->switching;
    }

    /**
     * @param bool $switching
     */
    public function setSwitching(bool $switching)
    {
        $this->switching = $switching;
    }

    /**
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * @return bool
     */
    public function isStatusOut() : bool
    {
        return $this->statusOut;
    }

    /**
     * @param bool $statusOut
     */
    public function setStatusOut(bool $statusOut)
    {
        $this->statusOut = $statusOut;
    }

    public function getLanguageTag() : string
    {
        return $this->languageTag;
    }

    public function setLanguageTag(string $languageTag)
    {
        $this->languageTag = $languageTag;
    }

    public function getExcludeParams()
    {
        return ['No', 'Yes'];
    }

    public function getIsExcludeForView()
    {
        if ($this->isExclude !== NULL) {
            return $this->getExcludeParams()[$this->isExclude];
        }

        return '';
    }
}
