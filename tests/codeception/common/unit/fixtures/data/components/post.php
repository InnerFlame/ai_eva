<?php
$idFromMulti        = $idFrom           = '00000000000000000000000000000001';
$idToMulti          = $idTo             = '00000000000000000000000000000002';
$countryFrom        = $countryTo        = 'USA';
$siteFrom           = $siteTo           = '3c09509e33441cf21f9b1cc3a49413f2';
$languageFrom       = $languageTo       = 'DEF';
$paid               = 'Paid';
$platformFrom       = $platformTo       = 'webSite';
$trafficSourceFrom  = $trafficSourceTo  = 'direct';
$actionWayFrom      = $actionWayTo      = 'default';
$text               = 'Some text';

return [
    [
        'from'              => $idFrom,
        'multi_profile_id'  => $idFrom,
        'to'                => $idTo,
        'country'           => $countryFrom,
        'site'              => $siteFrom,
        'text'              => $text,
        'profiles' => [
            $idFrom         => [
                'id'                => $idFrom,
                'multi_profile_id'  => $idFromMulti,
                'site'              => $siteFrom,
                'country'           => $countryFrom,
                'language'          => $languageFrom,
                'paid'              => $paid,
                'platform'          => $platformFrom,
                'traff_src'         => $trafficSourceFrom,
                'action_way'        => $actionWayFrom,
            ],
            $idTo           => [
                'id'                => $idTo,
                'multi_profile_id'  => $idToMulti,
                'site'              => $siteTo,
                'country'           => $countryTo,
                'platform'          => $platformTo,
                'traff_src'         => $trafficSourceTo,
                'action_way'        => $actionWayTo,
            ],
        ],
    ],
];
