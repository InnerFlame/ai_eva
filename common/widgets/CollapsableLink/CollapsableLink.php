<?php

namespace common\widgets\CollapsableLink;

use yii\bootstrap\Html;
use \yii\bootstrap\Widget;

class CollapsableLink extends Widget
{
    public $columnId;
    public $rubberColumnId;

    public function init()
    {
        parent::init();
        $view = $this->getView();
        CollapsableLinkAsset::register($view);
    }

    public function run()
    {
        $content = '';
        $content .= Html::tag('span', '', [
            'class' => 'glyphicon glyphicon-circle-arrow-left pull-right collapse-column-link',
            'onclick' => 'collapseColumnLinkClick(this, \'#' .
                $this->columnId .
                '\', \'#' . $this->rubberColumnId . '\');',
        ]);
        return $content;
    }
}
