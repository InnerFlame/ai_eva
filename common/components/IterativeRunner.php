<?php
namespace common\components;

class IterativeRunner
{
    /** @var int
     *  in seconds
     */
    protected $sleep;

    /** @var int
     * count of run
     */
    protected $iterationCount;

   /** @var  callable */
    protected $callback;

    /** @var  array */
    protected $funcParams;

    /**
     * InteractiveRunner constructor.
     *
     * @param callable  $callback function to run
     * @param array     $params callback params
     */
    public function __construct(callable $callback, array $params = [], $sleep = 0, $iterationCount = 1000)
    {
        $this->setCallback($callback);
        $this->setFuncParams($params);
        $this->setSleep($sleep);
        $this->setIterationCount($iterationCount);
    }

    public function run()
    {
        $iterationCount = $this->getIterationCount();
        $foreverLoop = $iterationCount == 0 ? true : false;

        for($i = 0; ($i < $iterationCount || $foreverLoop); ++$i) {
            call_user_func($this->getCallback(), $this->getFuncParams());

            if ($this->getSleep() > 0) {
                sleep($this->getSleep());
            }
        }
    }

    /**
     * @return int
     */
    protected function getSleep()
    {
        return $this->sleep;
    }

    /**
     * Set sleep in micro seconds
     *
     * @param int $sleep
     */
    protected function setSleep(int $sleep)
    {
        $this->sleep = $sleep;
        return $this;
    }

    /**
     * @return int
     */
    protected function getIterationCount()
    {
        return $this->iterationCount;
    }

    /**
     * Set count of iterations;
     *
     * @param int $iterationCount
     */
    protected function setIterationCount(int $iterationCount)
    {
        $this->iterationCount = $iterationCount;
        return $this;
    }

    /**
     * @return callable
     */
    protected function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param callable $callback
     */
    protected function setCallback(callable $callback)
    {
        $this->callback = $callback;
        return $this;
    }

    /**
     * @return array
     */
    protected function getFuncParams()
    {
        return $this->funcParams;
    }

    /**
     * @param array $funcParams
     */
    protected function setFuncParams(array $funcParams)
    {
        $this->funcParams = $funcParams;
        return $this;
    }
}