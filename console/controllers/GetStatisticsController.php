<?php
namespace console\controllers;

use yii\console\Controller;
use frontend\models\usersId\CommunicationLog;
use yii\db\Expression;

class GetStatisticsController extends Controller
{
    public function actionIndex()
    {
        $query = CommunicationLog::find()
            ->select(['userId, modelId, MIN(createdAt) AS min, MAX(createdAt) AS max, TIMESTAMPDIFF(SECOND, MIN(createdAt), MAX(createdAt)) AS dif'])
            ->from(['communicationLog FORCE INDEX (createdAt, idFrom, idTo)'])
            ->andFilterWhere(['isUser' => true])
            ->andFilterWhere(['>=', 'createdAt', new Expression('(NOW() - INTERVAL 48 HOUR)')])
            ->groupBy(['userId', 'modelId'])
            ->having('dif > 86400')//60 seconds * 60 minutes * 24 hours * 1 day = 86400 seconds
            ->asArray()
            ->count();//If you need details check '->all()' instead of '->count()' and var_export($query, true)

        echo 'Count of dialogues which lasts more than 24 hours: ' . $query . PHP_EOL;
    }
}
