<?php
declare(strict_types=1);

namespace frontend\models\topics;

use frontend\models\BaseForm;
use frontend\models\DataProvider;

class TopicsForm extends BaseForm
{
    public $topics  = array();
    public $rules   = array();
    public $rule;
    public $subRule = null;
    public $availableScenarios  = array();
    public $availableVariables  = array();
    public $availableTags       = array();

    public $availableTimeslots;

    public $currentTopicId;
    public $currentRuleId;
    public $currentPatternId;
    public $currentTemplateId;
    public $currentExcPatternId;

    const PATTERNS_TAB      = 'Patterns';
    const TEMPLATES_TAB     = 'Templates';
    const EXC_PATTERNS_TAB  = 'ExcPatterns';

    public $currentTab = self::PATTERNS_TAB;

    public function __construct()
    {
        $this->availableScenarios   = DataProvider::getInstance()->getScenarios();
        $this->availableVariables   = DataProvider::getInstance()->getVariables();
        $this->availableTags        = \Yii::$app->params['tags'];
        $this->availableTimeslots   = \Yii::$app->BrungildaApiCurl->getTimeslots();

    }

    public function getTopics()
    {
        $this->topics    = DataProvider::getInstance()->getTopics();
        $topicsTags      = TopicTag::find()->asArray()->all();
        $topicExclusions = TopicExclusionInitial::find()->asArray()->all();
        foreach ($this->topics as $key => $topic) {
            foreach ($topicsTags as $topicTag) {
                if ($topic->getId() == $topicTag['topicId']) {
                    $this->topics[$key]->tags[] = $topicTag['tagId'];
                }
            }
            foreach ($topicExclusions as $topicExclusion) {
                if ($topic->getId() == $topicExclusion['topicId']) {
                    $this->topics[$key]->isExclude = $topicExclusion['isExclude'];
                    $this->topics[$key]->countTriggering = $topicExclusion['countTriggering'];
                    break;
                }
            }
        }
    }

    public function updateTopic(int $topicId, array $params)
    {
        if (isset($params['checkedTags'])) {
            TopicTag::deleteAll('topicId = ' . $topicId);

            foreach ($params['checkedTags'] as $tagId) {
                $topicTag = new TopicTag();
                $topicTag->topicId = $topicId;
                $topicTag->tagId = $tagId;
                $topicTag->save();
            }

            if ($params['isExclude'] !== '' && $params['countTriggering'] !== '') {
                $topicExclusionInitial = TopicExclusionInitial::findOne(['topicId' => $topicId]);
                if (empty($topicExclusionInitial)) {
                    $topicExclusionInitial          = new TopicExclusionInitial();
                    $topicExclusionInitial->topicId = $topicId;
                }
                $topicExclusionInitial->isExclude       = $params['isExclude'];
                $topicExclusionInitial->countTriggering = $params['countTriggering'];
                $topicExclusionInitial->save();
            }

            unset($params['checkedTags']);
            unset($params['isExclude']);
            unset($params['countTriggering']);
        }
        return DataProvider::getInstance()->updateTopic($topicId, $params);
    }

    public function deleteTopic(int $topicId)
    {
        TopicTag::deleteAll('topicId = ' . $topicId);
        TopicExclusionInitial::deleteAll('topicId = ' . $topicId);
        return DataProvider::getInstance()->deleteTopic($topicId);
    }

    public function getRules(int $topicId)
    {
        $this->rules = DataProvider::getInstance()->getRules($topicId);
    }

    public function getRule(int $ruleId)
    {
        $this->rule = DataProvider::getInstance()->getRule($ruleId);
    }

    public function getRawRule(int $ruleId)
    {
        return DataProvider::getInstance()->getRawRule($ruleId);
    }

    public function getSubRule(int $ruleId)
    {
        $this->subRule = DataProvider::getInstance()->getRule($ruleId);
    }

    public function getPatternsVariables()
    {
        $arr = DataProvider::getInstance()->getPatternsVariables();
        $response = array();
        foreach ($arr as $val) {
            $response[$val['pattern_id']]['pattern_var_id']   = $val['pattern_var_id'];
            $response[$val['pattern_id']]['index_content']    = $val['index_content'];
            $response[$val['pattern_id']]['variable_id']      = $val['variable_id'];
        }
        return $response;
    }
}
