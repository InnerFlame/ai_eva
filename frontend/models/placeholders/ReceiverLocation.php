<?php

namespace frontend\models\placeholders;

use yii\db\ActiveRecord;

class ReceiverLocation extends ActiveRecord
{
    public static function tableName()
    {
        return 'placeholders_receiver_locations_table';
    }

    public function getPlaceholder()
    {
        return $this->hasOne(Placeholder::className(), ['id' => 'placeholder_id']);
    }
}
