<?php

namespace common\widgets\BindableForm;

use yii\bootstrap\Html;
use \yii\bootstrap\Widget;

class BindableForm extends Widget
{
    public $lines;
    public $id;
    public $hiddens;

    public function init()
    {
        parent::init();
        $view = $this->getView();
        BindableFormAsset::register($view);
    }

    public function run()
    {
        $content = '';

        $content .= Html::beginTag('form', [
            'class' => 'form-horizontal bindable-form',
            'role' => 'form',
            'id' => $this->id,
        ]);

        if (isset($this->hiddens)) {
            foreach ($this->hiddens as $hidden) {
                $params = ['type' => 'hidden'];
                if (isset($hidden['name'])) {
                    $params['name'] = $hidden['name'];
                }
                if (isset($hidden['id'])) {
                    $params['id'] = $hidden['id'];
                }
                if (isset($hidden['value'])) {
                    $params['value'] = $hidden['value'];
                }
                $content .= Html::tag('input', '', $params);
            }
        }

        foreach ($this->lines as $line) {
            $content .= Html::beginTag('div', ['class' => 'form-group',]);
            foreach ($line['line'] as $element) {
                $content .= Html::beginTag($element['tag'], $element['options'] ?? []);
                $content .= $element['content'] ?? '';
                $content .= Html::endTag($element['tag']);
            }
            $content .= Html::endTag('div');
        }
        $content .= Html::endTag('form');

        return $content;
    }
}
