<?php
namespace frontend\models;

use yii\db\ActiveRecord;

class UserTag extends ActiveRecord
{
    public static function tableName()
    {
        return 'users_tags';
    }
}
