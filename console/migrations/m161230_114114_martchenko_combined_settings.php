<?php

use yii\db\Migration;

/**
 * Handles adding is_fixed to table `settings_scenarios`.
 */
class m161230_114114_martchenko_combined_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        \Yii::$app->db->createCommand('
            CREATE TABLE `consolidatedSettings` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `setting_id` int(11) NOT NULL,
              `name` varchar(255) NOT NULL,
              `control_profile` varchar(32) NOT NULL,
              `target_profile` varchar(32) NOT NULL,
              `active` tinyint(1) DEFAULT NULL,
              `country_name` varchar(3) NOT NULL,
              `status_name` varchar(10) DEFAULT NULL,
              `site_hash` varchar(32) NOT NULL,
              `traffic_type_name` varchar(10) DEFAULT NULL,
              `platform_name` varchar(16) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `setting_id` (`setting_id`),
              KEY `site_country_type` (`site_hash`, `country_name`, `control_profile`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ')
        ->execute();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        \Yii::$app->db->createCommand('DROP TABLE `consolidatedSettings`')->execute();
    }
}
