$(document).ready(function () {
    //Get count of table columns (if it is one per page)
    var columnsCount = $('#expanded-table thead tr th').length;
    //Click on table item
    $(document).on('click', '#expanded-table tbody tr:not(.current, .expanded-table-header, .expanded-table-footer)', function () {
        $('.expanded-table-header, .expanded-table-footer').remove();
        $(this).siblings().removeClass('current');
        $(this).addClass('current');
        var currentHeader = '<tr class="expanded-table-header">';
        currentHeader += $('#expanded-table-header').html();
        currentHeader += '</tr>';
        $(currentHeader).show('fast').insertBefore($(this));
        var currentFooter = '<tr class="expanded-table-footer"><th colspan="' + columnsCount + '">';
        currentFooter += $('#expanded-table-footer').html();
        currentFooter += '</th></tr>';
        $(currentFooter).show('fast').insertAfter($(this));
        clickExpandedTableItemCallback(this);
    });
});

function collapseExpandedItem()
{
    $('.expanded-table-header, .expanded-table-footer').remove();
    $('#expanded-table tbody tr:not(.expanded-table-header, .expanded-table-footer)').siblings().removeClass('current');
}
