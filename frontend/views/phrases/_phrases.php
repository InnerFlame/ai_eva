<?php
use common\widgets\BindableFormItems\BindableFormItems;

$phrasesItems = array();

foreach ($model->phrases as $phrase) {
    $item['text'] = $phrase->getText();
    $item['hiddens'] = [
        ['name' => 'phraseId', 'value' => $phrase->getId()],
        //phraseDelayId and phraseTimeslotId not binding to the form by BindableFormItems widget
        //It is binding by javascript file, because corresponding field are dropdown
        //and we have only id of delays and phrases inside phrase item
        ['name' => 'phraseDelayId', 'value' => $phrase->getDelayId()],
        ['name' => 'phraseTimeslotId', 'value' => $phrase->getTimeslotId()],
    ];
    $phrasesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'phrasesBindableForm',
    'items' => $phrasesItems,
    'checked' => true,
]);
