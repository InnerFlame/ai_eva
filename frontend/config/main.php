<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/settings.php'),
    require(__DIR__ . '/../../common/config/countries.php'),
    require(__DIR__ . '/../../common/config/placeholders.php'),
    require(__DIR__ . '/../../common/config/common-info.php'),
    require(__DIR__ . '/../../common/config/projectBySiteId.php'),
    require(__DIR__ . '/../../common/config/tags.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'homeUrl' => '/site/login',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                    'except' => [
                        'yii\web\HttpException:403',
                    ],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                //TODO temporary routes, before we got SPA
                'settings-ng/update/<id:\d+>' => 'settings-ng/index',
                'time-slots/update/<id:\d+>'  => 'time-slots/index',

                'error-log/<page:\d+>'          => 'error-log/index',
                'error-log/index/<page:\d+>'    => 'error-log/index',
                'error-log/get-data/<page:\d+>' => 'error-log/get-data',

                'access/<page:\d+>'                => 'access/index',
                'access/index/<page:\d+>'          => 'access/index',
                'access/get-users-data/<page:\d+>' => 'access/get-users-data',
            ],
        ],
        'request' => [
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_SECRET', true)
        ],
    ],
    'params' => $params,
];
