<?php
declare(strict_types=1);

namespace frontend\models\classifiers;

use frontend\models\BaseForm;
use frontend\models\DataProvider;

class ClassifiersForm extends BaseForm
{
    public $classifiers        = [];
    public $classifierPatterns = [];

    public function getClassifiers()
    {
        $this->classifiers = DataProvider::getInstance()->getClassifiers();
    }

    public function getClassifierPatterns(int $classifierId)
    {
        $this->classifierPatterns = DataProvider::getInstance()->getClassifierPatterns($classifierId);
    }
}
