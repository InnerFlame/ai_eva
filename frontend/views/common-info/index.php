<?php
use frontend\assets\CommonInfoAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;

CommonInfoAsset::register($this);

Pjax::begin(['id' => 'search', 'timeout' => false, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]);

echo $this->render('_search', ['model' => $searchModel]);

echo '<hr />';

echo '<table class="table table-bordered"><thead>';
foreach($searchModel->explainSearchQueries[0] ?? array() as $row) {
    echo '<tr>';
    foreach ($row as $key => $val) {
        echo '<th>' . $key . '</th>';
    }
    echo '<tr/>';
    break;
}
echo '<thead/><tbody>';
foreach($searchModel->explainSearchQueries as $explainSearchQuery) {
    foreach($explainSearchQuery as $row) {
        echo '<tr>';
        foreach ($row as $key => $val) {
            echo '<td>' . $val . '</td>';
        }
        echo '<tr/>';
    }
}
echo '</tbody></table>';

echo '<div id="searchResults">';
echo GridView::widget([
    'id' => 'searchResultsGrid',
    'dataProvider' => $dataProvider,
    'columns' => [
        ['attribute' => 'messageCount',],
        ['attribute' => 'uniqueUserCount',],
        ['attribute' => 'uniqueSessionCount',],
        ['attribute' => 'placeholderCount',],
        ['attribute' => 'switchingCount',],
        ['attribute' => 'ruleCount',],
        ['attribute' => 'createdDate',],
    ],
]);
echo '</div>';

echo '<hr />';
echo '<table class="table table-bordered"><thead>';
foreach($searchModel->explainPlaceholdersInfoQuery as $row) {
    echo '<tr>';
    foreach ($row as $key => $val) {
        echo '<th>' . $key . '</th>';
    }
    echo '<tr/>';
    break;
}
echo '<thead/><tbody>';
foreach($searchModel->explainPlaceholdersInfoQuery as $row) {
    echo '<tr>';
    foreach ($row as $key => $val) {
        echo '<td>' . $val . '</td>';
    }
    echo '<tr/>';
}
echo '</tbody></table>';

echo '<div id="placeholdersSearchResults">';
echo GridView::widget([
    'id' => 'placeholdersSearchResultsGrid',
    'dataProvider' => $paceholderDataProvider,
    'columns' => [
        ['attribute' => 'createdDate',],
        [
            'attribute' => 'placeholderId',
            'label'     => 'Placeholder',
            'value'     => function ($data) use ($searchModel) {
                if ($data->placeholderId != 0) {
                    return $searchModel->availablePlaceholders[$data->placeholderId] ?? '';
                } else {
                    return '';
                }
            },
        ],
        ['attribute' => 'country',],
        ['attribute' => 'placeholdersCount',],
    ],
]);
echo '</div>';

Pjax::end();
