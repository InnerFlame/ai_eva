<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_action_ways_table`.
 */
class m160622_142207_create_settings_action_ways_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_action_ways_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'action_way_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_action_ways_table');
    }
}
