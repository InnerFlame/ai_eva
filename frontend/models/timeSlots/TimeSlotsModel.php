<?php

namespace frontend\models\timeSlots;

use frontend\models\BaseNgForm;

/**
 * Class TimeSlotsModel
 * @package frontend\models\timeSlots
 */
class TimeSlotsModel extends BaseNgForm
{
    /**
     * @return mixed
     */
    public function getTimeSlots(): array
    {
        return $this->getBrungildaApi()->getTimeslots();
    }

    /**
     * @param string $formData
     * @return array
     */
    public function createTimeSlot(string $formData): array
    {
        $formData = json_decode($formData, true);

        $createData = [
            'name'  => $formData['name'],
            'time1' => $formData['time1'],
            'time2' => $formData['time2']
        ];

        if (!empty($formData['date1']) && !empty($formData['date2'])) {
            $createData['date1'] = $formData['date1']['formatted'];
            $createData['date2'] = $formData['date2']['formatted'];
        }

        return $this->getBrungildaApi()->createTimeslot($createData);
    }

    /**
     * @param string $formData
     * @return array
     */
    public function updateTimeSlot(string $formData): array
    {
        $formData = json_decode($formData, true);

        $createData = [
            'timeslot_id' => $formData['timeslot_id'],
            'name'        => $formData['name'],
            'time1'       => $formData['time1'],
            'time2'       => $formData['time2']
        ];

        if (!empty($formData['date1']) && !empty($formData['date2'])) {
            $createData['date1'] = $formData['date1']['formatted'];
            $createData['date2'] = $formData['date2']['formatted'];
        } else {
            $createData['date1'] = null;
            $createData['date2'] = null;
        }

        return $this->getBrungildaApi()->updateTimeslot($createData);
    }

    /**
     * @param int $timeSlotId
     * @return array
     */
    public function deleteTimeSlot(int $timeSlotId): array
    {
        return $this->getBrungildaApi()->deleteTimeslot($timeSlotId);
    }
}
