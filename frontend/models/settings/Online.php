<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class Online extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_online';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
