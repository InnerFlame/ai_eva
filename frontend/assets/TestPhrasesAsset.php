<?php

namespace frontend\assets;

class TestPhrasesAsset extends BasicAsset
{
    public $js = [
        'js/testPhrases.js',
    ];
}
