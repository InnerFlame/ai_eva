<?php

namespace common\widgets\Sidebar;

use yii\web\AssetBundle;

class SidebarAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/Sidebar/assets';

    public $css = [
        'css/sidebar.css',
    ];

    public $js = [
        'js/sidebar.js',
    ];

    public $depends = [];

    public $publishOptions = [
        //'forceCopy'=>true,
    ];
}
