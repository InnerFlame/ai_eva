<?php

use yii\db\Migration;
use frontend\models\topics\TopicExclusionInitial;
use frontend\models\topics\UserTopic;

class m170804_141111_tutko_create_table_user_topic extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE  IF NOT EXISTS `' . TopicExclusionInitial::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `topicId` INT(11) NOT NULL,
                `isExclude` TINYINT(1) NOT NULL DEFAULT 0,
                `countTriggering` INT(11) NOT NULL DEFAULT 0,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `topicId` (`topicId`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            CREATE TABLE IF NOT EXISTS `' . UserTopic::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `topicId` INT(11) NOT NULL,
                `userId` VARCHAR(32) NOT NULL,
                `countTriggering` INT(11) NOT NULL,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `usserTopicId` (`userId`,`topicId`),
                KEY `topicId` (`topicId`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    public function safeDown()
    {
        $this->execute('
            DROP TABLE IF EXISTS `' . TopicExclusionInitial::tableName() . '`;
            DROP TABLE IF EXISTS `' . UserTopic::tableName() . '`;
        ');
    }
}
