<?php

namespace common\widgets\ExpandedTable;

use yii\web\AssetBundle;

class ExpandedTableAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/ExpandedTable/assets';

    public $css = [
        'css/expandedtable.css',
    ];

    public $js = [
        'js/expandedtable.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        //'forceCopy'=>true,
    ];
}
