<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group_phrases_tags`.
 */
class m170620_160829_create_group_phrases_tags_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group_phrases_tags', [
            'id'                => $this->primaryKey(),
            'groupPhrasesId'    => $this->integer(11),
            'tagId'             => $this->integer(),
            'createdAt'         => "timestamp DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->createIndex('groupPhrasesId', 'group_phrases_tags', ['groupPhrasesId']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('group_phrases_tags');
    }
}
