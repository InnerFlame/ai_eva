<?php
namespace frontend\models\topics;

use yii\db\ActiveRecord;

class TopicTag extends ActiveRecord
{
    public const TAG_ID_DATING = 1;
    public const TAG_ID_CAMS = 2;

    public static function tableName()
    {
        return 'topics_tags';
    }
}
