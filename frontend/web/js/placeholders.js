function updatePlaceholders()
{
    $.ajax({
        url: '/placeholders/placeholders',
        success: function (data) {
            //Update placeholders
            $('#placeholdersAjax').html(data);
        }
    });
};

function modalCheckboxesToArray(divId)
{
    var objects = $('#addNewPlaceholderModal #' + divId + ' ul li a input[type="checkbox"]:checked').closest('li').find('input[type="hidden"]');
    var arr = [];
    for (var i = 0; i < objects.size(); i++) {
        arr.push($(objects[i]).val());
    };
    return arr;
};

function checkboxesToArray(divName)
{
    var objects = $('#expanded-table div[name="' + divName + '"] ul li a input[type="checkbox"]:checked').closest('li').find('input[type="hidden"]');
    var arr = [];
    for (var i = 0; i < objects.size(); i++) {
        arr.push($(objects[i]).val());
    };
    return arr;
};

$(document).ready(function () {
    $(document).on('click', '.dropdown-toggler ul li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        var toggler = $(this).closest('.dropdown-toggler');
        toggler.find('button input').val(val);
        toggler.find('button span:eq(0)').html(name);
    });
    $(document).on('focus', 'input[name="placeholderUrl"], #addNewPlaceholderUrl, input[name="placeholderUrlWap"], #addNewPlaceholderUrlWap', function () {
        $(this).popover({
            placement: 'left',
            html: true,
            title: 'Available shortcodes',
            content:
                "<b>{sender_id}</b> - id of sender<br/>" +
                "<b>{recipient_id}</b> - id of recepient<br/>" +
                "<b>{country}</b> - country<br/>" +
                "<b>{status}</b> - status<br/>" +
                "<b>{site}</b> - site<br/>" +
                "<b>{traffic_source}</b> - traffic source<br/>" +
                "<b>{action_way}</b> - action way<br/>" +
                "<b>{platform}</b> - platform<br/>" +
                "<b>{screenname}</b> - screenname<br/>" +
                "<b>{sitename}</b> - sitename<br/>" +
                "<b>{shortId}</b> - recipient short id<br/>" +
                "<b>{providerName}</b> - cams provider name<br/>"
        });
    });

    $(document).on('focusout', 'input[name="placeholderUrl"], #addNewPlaceholderUrl, input[name="placeholderUrlWap"], #addNewPlaceholderUrlWap', function () {
        $(this).popover('hide');
    });

    updatePlaceholders();

    $(document).on('click', '#expanded-table div[name="placeholderType"] ul li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#expanded-table div[name="placeholderType"] button input').val(val);
        $('#expanded-table div[name="placeholderType"] button span:eq(0)').html(name);
    });
    $(document).on('click', '#expanded-table div[name="placeholderUrlType"] ul li', function () {
        var val = $(this).find('input').val();
        var name = $(this).find('a').html();
        $('#expanded-table div[name="placeholderUrlType"] button input').val(val);
        $('#expanded-table div[name="placeholderUrlType"] button span:eq(0)').html(name);
    });
    //Disable 'Add' button when placeholder name or url is empty
    if ($('#addNewPlaceholderName').val() === '' || $('#addNewPlaceholderUrl').val() === '' || $('#addNewPlaceholderUrlWap').val() === '') {
        $('#addNewPlaceholderButton').attr('disabled', true);
    } else {
        $('#addNewPlaceholderButton').attr('disabled', false);
    }
    $(document).on('input', '#addNewPlaceholderName, #addNewPlaceholderUrl, #addNewPlaceholderUrlWap', function () {
        if ($('#addNewPlaceholderName').val() === '' || $('#addNewPlaceholderUrl').val() === '' || $('#addNewPlaceholderUrlWap').val() === '') {
            $('#addNewPlaceholderButton').attr('disabled', true);
        } else {
            $('#addNewPlaceholderButton').attr('disabled', false);
        }
    });

    //Click 'Add new placeholder' button
    $(document).on('click', '#addNewPlaceholderButton', function () {
        var placeholderName = $('#addNewPlaceholderName').val();
        var placeholderType = $('#addNewPlaceholderType button input[type="hidden"]').val();
        var placeholderUrl = $('#addNewPlaceholderUrl').val();
        var placeholderUrlWap = $('#addNewPlaceholderUrlWap').val();
        var placeholderUrlType = $('#addNewPlaceholderUrlType button input[type="hidden"]').val();
        var placeholderSenderTypes = modalCheckboxesToArray('addNewPlaceholderSenderTypes');
        var placeholderReceiverLocations = modalCheckboxesToArray('addNewPlaceholderReceiverLocations');
        var placeholderIsLink = $('#addNewPlaceholderIsLink button input[type="hidden"]').val();
        $.ajax({
            url: '/placeholders/create-placeholder',
            type: "POST",
            data:   'name=' + placeholderName +
                    '&type=' + placeholderType +
                    '&url=' + encodeURIComponent(placeholderUrl) +
                    '&urlWap=' + encodeURIComponent(placeholderUrlWap) +
                    '&urlType=' + placeholderUrlType +
                    '&senderTypes=' + placeholderSenderTypes +
                    '&receiverLocations=' + placeholderReceiverLocations +
                    '&isLink=' + placeholderIsLink,
            success: function (data) {
                if (data == 'true') {
                    updatePlaceholders();
                } else {
                    alert('An error occurred while creating the placeholder!');
                }
                $('#addNewPlaceholderModal').modal('hide');
            }
        });
    });

    //Click delete placeholders button
    $(document).on('click', '#deletePlaceholders', function () {
        var itemsNames      = '';
        $('#expanded-table td input[name="itemCheckbox"]:checked').each(function (index) {
            var itemsName = $(this).closest('td').next().find('input[type="hidden"]').val();
            itemsNames += (index + 1) + '. ' + itemsName + '<br/>';
        });
        if (itemsNames.length == 0) {
            return;
        }

        $('#deletePlaceholdersNames').html(itemsNames);
        $('#confirmDeletePlaceholdersModal').modal('show');
    });

    //Click confirm delete placeholders button
    $(document).on('click', '#confirmDeletePlaceholdersButton', function () {
        var checkedItemsIds = [];
        $('#expanded-table td input[name="itemCheckbox"]:checked').each(function () {
            var id = $(this).closest('span').parent().find('input[type="hidden"]').val();
            checkedItemsIds.push(id);
        });
        if (checkedItemsIds.length == 0) {
            return;
        }

        var params = [];
        $.each(checkedItemsIds, function (index, value) {
            var elem = {};
            elem.placeholder_id = value;
            params.push(elem);
        });

        $.ajax({
            url: '/placeholders/delete-placeholders',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updatePlaceholders();
                } else {
                    alert('An error occurred while deleting the placeholders!');
                }
                $('#confirmDeletePlaceholdersModal').modal('hide');
            }
        });
    });

    //Click 'Update placeholder' button
    $(document).on('click', 'button[name="updatePlaceholderButton"]', function () {
        var placeholderId = $('#expanded-table input[name="placeholderId"]').val();
        var placeholderName = $('#expanded-table input[name="placeholderName"]').val();
        var placeholderType = $('#expanded-table div[name="placeholderType"] button input[type="hidden"]').val();
        var placeholderUrl = $('#expanded-table input[name="placeholderUrl"]').val();
        var placeholderUrlWap = $('#expanded-table input[name="placeholderUrlWap"]').val();
        var placeholderUrlType = $('#expanded-table div[name="placeholderUrlType"] button input[type="hidden"]').val();
        var placeholderSenderTypes = checkboxesToArray('placeholderSenderTypes');
        var placeholderReceiverLocations = checkboxesToArray('placeholderReceiverLocations');
        var active = $('#expanded-table button[name="placeholderActiveRadioButton"]').hasClass('active');
        var placeholderIsLink = $('#expanded-table div[name="placeholderIsLink"] button input[type="hidden"]').val();

        $.ajax({
            url: '/placeholders/update-placeholder',
            type: "POST",
            data:
            'id=' + placeholderId +
            '&name=' + placeholderName +
            '&type=' + placeholderType +
            '&url=' + encodeURIComponent(placeholderUrl) +
            '&urlWap=' + encodeURIComponent(placeholderUrlWap) +
            '&urlType=' + placeholderUrlType +
            '&senderTypes=' + placeholderSenderTypes +
            '&receiverLocations=' + placeholderReceiverLocations +
            '&active=' + active +
            '&isLink=' + placeholderIsLink,
            success: function (data) {
                if (data == 'true') {
                    updatePlaceholders();
                } else {
                    alert('An error occurred while updating the placeholder!');
                }
            }
        });
    });

    //Click 'Cancel placeholder' button
    $(document).on('click', 'button[name="cancelPlaceholderButton"]', function () {
        collapseExpandedItem();
    });

    //Click placeholder delete button
    $(document).on('click', 'button[name="deletePlaceholderButton"]', function () {
        var placeholderId = $('#expanded-table input[name="placeholderId"]').val();
        var placeholderName = $('#expanded-table input[name="placeholderName"]').val();
        $('#deletePlaceholderId').val(placeholderId);
        $('#deletePlaceholderName').html(placeholderName);
        $('#confirmDeletePlaceholderModal').modal('show');
    });

    //Click confirm placeholder delete button
    $(document).on('click', '#confirmDeletePlaceholderButton', function () {
        var placeholderId = $('#deletePlaceholderId').val();
        $.ajax({
            url: '/placeholders/delete-placeholder',
            type: "POST",
            data: 'id=' + placeholderId,
            success: function (data) {
                if (data == 'true') {
                    updatePlaceholders();
                } else {
                    alert('An error occurred while deleting the placeholder!');
                }
                $('#confirmDeletePlaceholderModal').modal('hide');
            }
        });
    });

    //Click "Check all"
    $(document).on('click', '#expanded-table ul li.checkAll, #addNewPlaceholderModal ul li.checkAll', function () {
        var isChecked = ($(this).siblings('li').first().find('a input[type="checkbox"]:checked').length > 0);
        $(this).siblings('li:visible').find('a input[type="checkbox"]').prop('checked', !isChecked);
    });

    //Live search functionality
    $(document).on('keyup', 'input.livesearch', function () {
        var pattern = $(this).val().toLowerCase();
        $(this).siblings('li:not(.checkAll)').each(function (i) {
            var text = $(this).contents().eq(0).text().toLowerCase();
            if (text.indexOf(pattern) == -1 && !$(this).first().find('a input[type="checkbox"]:checked').length > 0) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });
});

function clickExpandedTableItemCallback(item)
{
    //Set id
    var placeholderId = $(item).find('th span span').html();
    $('#expanded-table input[name="placeholderId"]').val(placeholderId);
    //Set name
    var placeholderName = $(item).find('td:eq(1) span span').html();
    $('#expanded-table input[name="placeholderName"]').val(placeholderName);
    //Set placeholder type
    var placeholderType = $(item).find('td:eq(2) span input').val();
    var placeholderType = $('#expanded-table div[name="placeholderType"] ul li input[value="' + placeholderType + '"]').siblings('a').html();
    $('#expanded-table div[name="placeholderType"] button input').val(placeholderType);
    $('#expanded-table div[name="placeholderType"] button span:eq(0)').html(placeholderType);
    //Set url
    var placeholderUrl = $(item).find('td:eq(3) span input').val();
    $('#expanded-table input[name="placeholderUrl"]').val(placeholderUrl);
    //Set url wap
    var placeholderUrlWap = $(item).find('td:eq(4) span input').val();
    $('#expanded-table input[name="placeholderUrlWap"]').val(placeholderUrlWap);
    //Set url type
    var placeholderUrlType = $(item).find('td:eq(5) span input').val();
    var placeholderUrlType = $('#expanded-table div[name="placeholderUrlType"] ul li input[value="' + placeholderUrlType + '"]').siblings('a').html();
    $('#expanded-table div[name="placeholderUrlType"] button input').val(placeholderUrlType);
    $('#expanded-table div[name="placeholderUrlType"] button span:eq(0)').html(placeholderUrlType);
    //Set sender types
    $(item).find('td:eq(6) span input').each(function () {
        $('#expanded-table div[name="placeholderSenderTypes"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set receiver locations
    $(item).find('td:eq(7) span input').each(function () {
        $('#expanded-table div[name="placeholderReceiverLocations"] ul li input[value="' + $(this).val() + '"]').siblings('a').find('input[type="checkbox"]').attr('checked',true);
    });
    //Set active
    if ($(item).find('td:eq(8) span input').val() == "1") {
        $('#expanded-table button[name="placeholderActiveRadioButton"]').click();
    }
    //Set is_link
    var placeholderIsLinkLable = $(item).find('td:eq(9) span input').val();
    var placeholderIsLinkValue = placeholderIsLinkLable == 'no' ? 0 : 1;
    $('#expanded-table div[name="placeholderIsLink"] button input').val(placeholderIsLinkValue);
    $('#expanded-table div[name="placeholderIsLink"] button span:eq(0)').html(placeholderIsLinkLable);
}
