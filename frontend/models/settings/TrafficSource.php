<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class TrafficSource extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_traffic_sources_table';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
