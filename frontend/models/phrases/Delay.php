<?php
declare(strict_types=1);

namespace frontend\models\phrases;

class Delay
{
    private $id;
    private $t1;
    private $t2;
    private $name;

    /**
     * Delay constructor.
     * @param $id
     * @param $t1
     * @param $t2
     * @param $name
     */
    public function __construct(int $id, int $t1, int $t2, string $name)
    {
        $this->id = $id;
        $this->t1 = $t1;
        $this->t2 = $t2;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getT1() : int
    {
        return $this->t1;
    }

    /**
     * @param int $t1
     */
    public function setT1(int $t1)
    {
        $this->t1 = $t1;
    }

    /**
     * @return int
     */
    public function getT2() : int
    {
        return $this->t2;
    }

    /**
     * @param int $t2
     */
    public function setT2(int $t2)
    {
        $this->t2 = $t2;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
}
