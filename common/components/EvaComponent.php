<?php
namespace common\components;

use yii\base\Component;

class EvaComponent extends Component
{
    public function getEvaAnswer($data)
    {
        $response = \Yii::$app->webClient->post(
            'http://localhost:8088/input',
            json_encode($data),
            'application/json'
        );

        if (!empty($response)) {
            return json_decode($response, true);
        }

        return false;
    }

    public function getPlaceholderPhrase($data)
    {
        $response = \Yii::$app->webClient->post(
            'http://localhost:8088/get_placeholder_phrase',
            json_encode($data),
            'application/json'
        );

        if (!empty($response)) {
            return json_decode($response, true);
        }

        return false;
    }

    public function getSessions($data)
    {
        $response = \Yii::$app->webClient->post(
            'http://localhost:8088/sessions',
            json_encode($data),
            'application/json'
        );

        if (!empty($response)) {
            return json_decode($response, true);
        }
    }
}