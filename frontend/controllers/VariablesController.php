<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\variables\VariablesForm;
use yii\web\ForbiddenHttpException;

class VariablesController extends BaseController
{
    protected $model;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new VariablesForm();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('variables')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        return $this->render('index', ['model' => $this->model]);
    }

    public function actionVariables()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getVariables();
                return $this->renderAjax('_variables', ['model' => $this->model]);
            }
        );
    }

    public function actionCreateVariable()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                //$post['name'] = str_replace('\\', '\\\\', $post['name']);
                //$post['value'] = str_replace('\\', '\\\\', $post['value']);
                //$post['description'] = str_replace('\\', '\\\\', $post['description']);
                return $this->model->createVariable($post['name'], $post['value'], $post['description']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeleteVariable()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteVariable((int)$post['variableId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateVariable()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                //$post['name'] = str_replace('\\', '\\\\', $post['name']);
                //$post['value'] = str_replace('\\', '\\\\', $post['value']);
                //$post['description'] = str_replace('\\', '\\\\', $post['description']);
                return $this->model->updateVariable(
                    (int)$post['variableId'],
                    $post['name'],
                    $post['value'],
                    $post['description']
                );
            },
            self::RETURN_RAW_RESPONSE
        );
    }
}
