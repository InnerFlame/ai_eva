<div class="dialoguePart">
    <div class="lines" style="width: 100%;">
        <div class="line user">
            <div class="col userinfo">
                <b><?= $fromUserId ?> : </b>
            </div>
            <div class="col text">
                <?= $sendedText ?>
            </div>
        </div>
        <div class="line eva">
            <div class="col userinfo">
                <b><?= $toUserId ?> : </b>
            </div>
            <div class="col text">
                <?= $text ?>
            </div>
        </div>
    </div>
    <div class="details" style="width: 100%;">
        <?php
        foreach ($availableScenarios as $availableScenario) {
            if ($availableScenario->getId() == $scenario_id) {
                echo '<strong>Send scenario : </strong>' . $availableScenario->getName() . '<br/>';
            }
        }
        if (isset($params[0]['scenarioName'])) {
            echo '<strong>Scenario: </strong>' . $params[0]['scenarioName'] . '<br/>';
        }
        ?>
        <strong>Properties:</strong>
        <br>
        <ul>
        <?php foreach (($params ?? array()) as $key => $param) : ?>
            <?= isset($param['topic']['name'])   ? "<li>Topic: {$param['topic']['name']}</li>"      : '' ?>
            <?= isset($param['rule']['name'])    ? "<li>Rule: {$param['rule']['name']}</li>"        : '' ?>
            <?= isset($param['pattern']['text']) ? "<li>Pattern: {$param['pattern']['text']}</li>"  : '' ?>
            <?php
            if (isset($param['phrase']['group_phrase_id'])) {
                foreach ($availableGroupPhrases as $groupPhrases) {
                    if ($groupPhrases->getId() == $param['phrase']['group_phrase_id']) {
                        echo "<li>Group phrase: {$groupPhrases->getText()}</li>";
                        break;
                    }
                }
            }
            echo "<li>Correct text: " .  $correct_input_text[$key] . "</li>";
            echo "<br/>";
            ?>
        <?php endforeach; ?>
        </ul>
    </div>
</div>
