<?php

use yii\db\Migration;

class m170511_082830_add_project_name_to_consolidated_settings extends Migration
{
    public function up()
    {
        $this->addColumn('consolidatedSettings', 'project_name', $this->string()->defaultValue(''));
    }

    public function down()
    {
        $this->dropColumn('consolidatedSettings', 'project_name');
    }
}
