<?php
declare(strict_types=1);

namespace frontend\models\scenarios;

use frontend\models\BaseForm;
use frontend\models\DataProvider;

class ScenariosForm extends BaseForm
{
    public $groupsPhrases   = array();
    public $classifiers     = array();
    public $nlpClassifiers  = array();
    public $scenarios       = array();
    public $groups          = array();
    public $availableTags   = array();

    public function __construct()
    {
        $this->availableTags = \Yii::$app->params['tags'];
    }

    public function getScenarios()
    {
        $this->scenarios = DataProvider::getInstance()->getScenarios();
        $scenariosTags = ScenarioTag::find()->asArray()->all();
        foreach ($this->scenarios as $key => $scenario) {
            foreach ($scenariosTags as $scenarioTag) {
                if ($scenario->getId() == $scenarioTag['scenarioId']) {
                    $this->scenarios[$key]->tags[] = $scenarioTag['tagId'];
                }
            }
        }
    }

    public function updateScenario(int $scenarioId, string $name, string $nodes, string $flowChart, int $maxStepCount, $checkedTags, $scenarioType)
    {
        $checkedTagsArr = json_decode($checkedTags);

        ScenarioTag::deleteAll('scenarioId = ' . $scenarioId);

        foreach ($checkedTagsArr as $tagId) {
            $scenarioTag = new ScenarioTag();
            $scenarioTag->scenarioId = $scenarioId;
            $scenarioTag->tagId = $tagId;
            $scenarioTag->save();
        }
        return DataProvider::getInstance()->updateScenario($scenarioId, $name, $nodes, $flowChart, $maxStepCount, $scenarioType);
    }

    public function deleteScenario(int $scenarioId)
    {
        ScenarioTag::deleteAll('scenarioId = ' . $scenarioId);
        return DataProvider::getInstance()->deleteScenario($scenarioId);
    }

    public function getGroups()
    {
        $phrasesForm = new \frontend\models\phrases\PhrasesForm();
        $phrasesForm->getGroupsPhrases();
        $this->groups[] = new \frontend\models\phrases\GroupGroupsPhrases(-1, 'All');
        foreach ($phrasesForm->groupsGroupsPhrases as $value) {
            $this->groups[] = $value;
        }
    }

    public function getGroupsPhrases()
    {
        $phrasesForm = new \frontend\models\phrases\PhrasesForm();
        $phrasesForm->getGroupsPhrases();
        foreach ($phrasesForm->groupsPhrases as $value) {
            $this->groupsPhrases[] = $value;
        }
    }

    public function getClassifiers()
    {
        $this->classifiers = DataProvider::getInstance()->getClassifiers();
    }

    public function getNlpClassifiers()
    {
        $this->nlpClassifiers = DataProvider::getInstance()->getNlpClassifiers();
        foreach ($this->nlpClassifiers as $key => $val) {
            $this->nlpClassifiers[$key]->classes = DataProvider::getInstance()->getNlpClassifierClasses($val->id);
        }
    }
}
