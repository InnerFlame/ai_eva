<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings_fan_club`.
 */
class m170302_100337_create_settings_fan_club_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_fan_club', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'is_fan_club_allowed' => $this->boolean(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_fan_club');
    }
}
