<?php
use common\widgets\RadioButton\RadioButton;
use frontend\assets\SettingsAsset;
use common\widgets\Modal\Modal;

SettingsAsset::register($this);

$availableControlProfiles = \Yii::$app->params['controlProfiles'];
$availableTargetProfiles = \Yii::$app->params['targetProfiles'];
$availableCountries = \Yii::$app->params['countries'];
$availableLocales = \Yii::$app->params['availableLanguages'];
$availableStatuses = \Yii::$app->params['statuses'];
$availableSites = \Yii::$app->params['sites'];
$availableTrafficSources = \Yii::$app->params['trafficSources'];
$availableActionWays = \Yii::$app->params['actionWays'];
$availableTrafficTypes = \Yii::$app->params['trafficTypes'];
$availablePlatforms = \Yii::$app->params['platforms'];
$availableFanClubs = \Yii::$app->params['fanClubs'];
$availableOnlines = \Yii::$app->params['onlines'];
$availableScenarios = $model->availableScenarios;
$availableProjects = \Yii::$app->params['projects'];
?>
<!-- Modal add new setting -->
<div id="addNewSettingModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title">Add new setting</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="control-label" for="addNewSettingName">Setting name:</label>
                        </div>
                        <div class="col-sm-9">
                            <input id="addNewSettingName" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="control-label" for="addNewSettingControlProfile">Control profile:</label>
                        </div>
                        <div class="col-sm-9" id="addNewSettingControlProfile">
                            <button
                                type="button"
                                class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                                >
                                <span name="name"><?php echo $availableControlProfiles[0];?></span>
                                <input type="hidden" value="<?php echo $availableControlProfiles[0];?>">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php
                                for ($i = 0; $i < count($availableControlProfiles); $i++) {
                                    echo '<li><a href="javascript:void(0);">' .
                                        $availableControlProfiles[$i] .
                                        '</a><input type="hidden" value="' .
                                        $availableControlProfiles[$i] .
                                        '"></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="control-label" for="addNewSettingTargetProfile">Target profile:</label>
                        </div>
                        <div class="col-sm-9" id="addNewSettingTargetProfile">
                            <button
                                type="button"
                                class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                                >
                                <span name="name"><?php echo $availableTargetProfiles[0];?></span>
                                <input type="hidden" value="<?php echo $availableTargetProfiles[0];?>">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php
                                for ($i = 0; $i < count($availableTargetProfiles); $i++) {
                                    echo '<li><a href="javascript:void(0);">' .
                                        $availableTargetProfiles[$i] .
                                        '</a><input type="hidden" value="' .
                                        $availableTargetProfiles[$i] .
                                        '"></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                <button id="addNewSettingButton" class="btn btn-success" disabled="true" type="button">Add new setting
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal confirm delete setting -->
<?php
echo Modal::widget([
    'id' => 'confirmDeleteSettingModal',
    'title' => 'Delete setting',
    'hiddenInputIds' => ['deleteSettingId',],
    'nameText' => 'setting',
    'nameId' => 'deleteSettingName',
    'buttonId' => 'confirmDeleteSettingButton'
]);
?>

<!-- Modal combine settings -->
<div id="combineSettingsModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Combine settings...</h4>
            </div>
            <div class="modal-body">
                <br/>
                <div class="progress progress-striped active">
                    <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0"
                         aria-valuemax="100" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        <button
            id="createNewSetting"
            class="btn btn-success btn-block"
            type="button"
            data-target="#addNewSettingModal"
            data-toggle="modal"
            ><span class="glyphicon glyphicon-plus"></span> Add setting</button>
    </div>
    <div class="col-sm-offset-8 col-sm-2">
        <button
            id="combineSettings"
            class="btn btn-primary btn-block"
            type="button"
            data-target="#combineSettingsModal"
            data-toggle="modal"
            ><span class="glyphicon glyphicon-refresh"></span> Combine settings. Don`t touch!!!</button>
    </div>
</div>
<table>
    <tr id="expanded-table-header">
        <th scope="row"><input type="hidden" class="form-control" value="" name="settingsId"></th>
        <td><input type="text" class="form-control" value="" name="settingsName"></td>
        <td>
            <div class="input-group-btn" name="settingsProjects">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Project</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <input type="text" class="form-control livesearch" placeholder="Search">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availableProjects); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availableProjects[$i] .
                            '</a><input type="hidden" value="' .
                            $availableProjects[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsControlProfile">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span></span>
                    <input type="hidden" value="">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    for ($i = 0; $i < count($availableControlProfiles); $i++) {
                        echo '<li><a href="javascript:void(0);">' .
                            $availableControlProfiles[$i] .
                            '</a><input type="hidden" value="' .
                            $availableControlProfiles[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsTargetProfile">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span></span>
                    <input type="hidden" value="">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    for ($i = 0; $i < count($availableTargetProfiles); $i++) {
                        echo '<li><a href="javascript:void(0);">' .
                            $availableTargetProfiles[$i] .
                            '</a><input type="hidden" value="' .
                            $availableTargetProfiles[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsCountries">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Country</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <input type="text" class="form-control livesearch" placeholder="Search">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availableCountries); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availableCountries[$i] .
                            '</a><input type="hidden" value="' .
                            $availableCountries[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsLocales">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Locale</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <input type="text" class="form-control livesearch" placeholder="Search">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availableLocales); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availableLocales[$i] .
                            '</a><input type="hidden" value="' .
                            $availableLocales[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsStatus">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Status</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availableStatuses); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availableStatuses[$i] .
                            '</a><input type="hidden" value="' .
                            $availableStatuses[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsSite">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Site</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <input type="text" class="form-control extendedLivesearch" placeholder="Search">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    foreach ($availableSites as $key => $value) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $value .
                            '</a><input type="hidden" value="' .
                            $key .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsTrafficSource">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Traffic source</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <input type="text" class="form-control extendedLivesearch" placeholder="Search">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availableTrafficSources); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availableTrafficSources[$i] .
                            '</a><input type="hidden" value="' .
                            $availableTrafficSources[$i] .
                            '"></li>';
                    }
                    /*foreach ($availableTrafficSources as $key => $value) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $value .
                            '</a><input type="hidden" value="' .
                            $key .
                            '"></li>';
                    }*/
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsActionWay">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Action way</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availableActionWays); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availableActionWays[$i] .
                            '</a><input type="hidden" value="' .
                            $availableActionWays[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsTrafficType">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Traffic type</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availableTrafficTypes); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availableTrafficTypes[$i] .
                            '</a><input type="hidden" value="' .
                            $availableTrafficTypes[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsPlatform">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Platform</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availablePlatforms); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availablePlatforms[$i] .
                            '</a><input type="hidden" value="' .
                            $availablePlatforms[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsFanClub">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Fan Club</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    foreach ($availableFanClubs as $key => $value) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $value .
                            '</a><input type="hidden" value="' .
                            $key .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsOnline">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Online</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    foreach ($availableOnlines as $key => $value) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $value .
                            '</a><input type="hidden" value="' .
                            $key .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="settingsScenario">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Scenario</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu list-group">
                    <li class="checkAllScenarios"><a href="javascript:void(0);">Check all</a></li>
                    <li class="divider" role="separator"></li>
                    <input type="text" class="form-control extendedButtonsLivesearch" placeholder="Search">
                    <?php
                    foreach ($availableScenarios as $scenario) {
                        echo '<li class="livesearchButton"><a href="javascript:void(0);">' .
                            '<input type="button" name="addScenarioToPanel" value="' .
                            $scenario->getName() .
                            '"></a><input type="hidden" value="' .
                            $scenario->getId() .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <?php
            echo RadioButton::widget([
                'activeText' => 'On',
                'text' => 'Off',
                'glyphiconClass' => 'glyphicon-off',
                'activeState' => false,
                'name' => 'settingsActiveRadioButton',
            ]);
            ?>
        </td>
    </tr>
</table>
<div id="expanded-table-footer">
    <div class="col-md-1">
        <button class="btn btn-danger btn-block" type="button" name="deleteSettingButton">
            <span class="glyphicon glyphicon-trash"></span> Delete
        </button>
    </div>
    <div class="col-md-1">
        <button class="btn btn-default btn-block" type="button" name="copySettingButton">
            <span class="glyphicon glyphicon-copy"></span> Copy
        </button>
    </div>
    <div class="col-md-1 col-md-offset-7">
        <button class="btn btn-default btn-block" type="button" name="cancelSettingButton">Cancel</button>
    </div>
    <div class="col-md-2">
        <button class="btn btn-primary btn-block" type="button" name="updateSettingButton">
            <span class="glyphicon glyphicon-refresh"></span> Update
        </button>
    </div>
</div>
<div id="settingsAjax"></div>