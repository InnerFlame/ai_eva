import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';
import { HttpClient } from '@angular/common/http';
import { Globals }    from "../globals";
import { Injectable } from "@angular/core";

/**
 * Http module service
 */
@Injectable()
export class HttpService
{
    /**
     * Http service constructor
     * @param {HttpClient} http
     */
    constructor(
        private http: HttpClient
    ) {  }

    /** GET request */
    public get (requestUrl: string, params: Object = {}): Promise<any>
    {
        if (params && Object.keys(params).length) {
            let urlWithParams : string = requestUrl + '?';
            let paramsString  : string = Object.keys(params).map((key)=>{ return key + '=' + params[key]}).join('&');

            requestUrl = urlWithParams + paramsString;
        }

        return this.http
            .get(requestUrl)
            .toPromise();
    }

    /** POST request */
    public post(requestUrl: string, requestBody: Object, timeout: number = 0): Promise<any>
    {
        let postRequest = this.http
            .post(requestUrl, requestBody, Globals.getRequestOptions());

        if (timeout) {
            postRequest.timeout(timeout);
        }

        return postRequest.toPromise();
    }
}
