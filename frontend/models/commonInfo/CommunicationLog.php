<?php
namespace frontend\models\commonInfo;

use yii\db\ActiveRecord;

//TODO: wtf duplicate is this?
class CommunicationLog extends ActiveRecord
{
    public $messageCount;
    public $uniqueUserCount;
    public $uniqueSessionCount;
    public $placeholderCount;
    public $switchingCount;
    public $ruleCount;
    public $createdDate;
    public $placeholdersCount;

    public static function tableName()
    {
        return 'communicationLog';
    }
}
