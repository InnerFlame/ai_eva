<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\search\SearchForm;
use yii\web\ForbiddenHttpException;

/**
 * Class SearchController
 * @package frontend\controllers
 */
class SearchController extends BaseController
{
    protected $model;

    /**
     * @inheritdoc
     * SearchController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new SearchForm();
    }

    /**
     * @inheritdoc
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('search')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        }

        return false;
    }

    /**
     * Main search page
     * @return string
     */
    public function actionIndex()
    {
        if ($this->model->load(\Yii::$app->request->get()) && $this->model->validate()) {
            $this->model->search();
        } else {
            $this->model->searchBy  = SearchForm::PATTERNS;
            $this->model->text      = '';
        }

        return $this->render('index', [
            'model' => $this->model
        ]);
    }
}
