<?php
declare(strict_types=1);

namespace frontend\models\topics;

class RulesTemplate
{
    private $templateId;
    private $ruleId;
    private $text;
    private $deleted;
    private $delayId;
    private $timeslotId;
    private $version;

    /**
     * RulesTemplate constructor.
     * @param $templateId
     * @param $ruleId
     * @param $text
     * @param $deleted
     * @param $delayId
     * @param $timeslotId
     * @param $version
     */
    public function __construct(
        int $templateId,
        int $ruleId,
        string $text,
        bool $deleted,
        int $delayId,
        int $timeslotId,
        int $version
    ) {
        $this->templateId = $templateId;
        $this->ruleId = $ruleId;
        $this->text = $text;
        $this->deleted = $deleted;
        $this->delayId = $delayId;
        $this->timeslotId = $timeslotId;
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getTemplateId() : int
    {
        return $this->templateId;
    }

    /**
     * @param int $templateId
     */
    public function setTemplateId(int $templateId)
    {
        $this->templateId = $templateId;
    }

    /**
     * @return int
     */
    public function getRuleId() : int
    {
        return $this->ruleId;
    }

    /**
     * @param int $ruleId
     */
    public function setRuleId(int $ruleId)
    {
        $this->ruleId = $ruleId;
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getDelayId() : int
    {
        return $this->delayId;
    }

    /**
     * @param int $delayId
     */
    public function setDelayId(int $delayId)
    {
        $this->delayId = $delayId;
    }

    /**
     * @return int
     */
    public function getTimeslotId() : int
    {
        return $this->timeslotId;
    }

    /**
     * @param int $timeslotId
     */
    public function setTimeslotId(int $timeslotId)
    {
        $this->timeslotId = $timeslotId;
    }

    /**
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }
}
