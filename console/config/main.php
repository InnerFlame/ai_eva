<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/settings.php'),
    require(__DIR__ . '/../../common/config/countries.php'),
    require(__DIR__ . '/../../common/config/placeholders.php'),
    require(__DIR__ . '/../../common/config/common-info.php'),
    require(__DIR__ . '/../../common/config/projectBySiteId.php'),
    require(__DIR__ . '/../../common/config/tags.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
