<?php

namespace frontend\assets;

class UsersIdAsset extends BasicAsset
{
    public $css = [
        'css/usersId.css',
    ];

    public $js = [
        'js/usersId.js',
    ];
}
