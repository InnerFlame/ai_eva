import { Component }              from '@angular/core';
import { Title }                  from "@angular/platform-browser";
import { HttpService }            from "../shared/services/http.service";
import { OnInit }                 from '@angular/core';
import { PagerService }           from "../shared/services";
import { Router }                 from '@angular/router';
import { AccessModel }            from "../shared/models/access.model";
import { ActivatedRoute, Params } from "@angular/router";

@Component({
    selector    : 'access',
    templateUrl : './access.html'
})

export class AccessComponent implements OnInit
{
    readonly pagerPageSize : number = 10;

    public displayLoadingBlock : boolean     = true;
    public enableModelsSelect  : boolean     = false;
    public errorText           : string      = '';
    public successText         : string      = '';
    public selectedRoleForUpdate : string    = '';
    public pager               : any         = {};
    public accessModel         : AccessModel =  new AccessModel();
    public pagedItems          : any[];
    public availableRoles      : any;

    private allItems   : any;
    private totalCount : number;

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param titleService
     * @param pagerService
     * @param activatedRoute
     * @param router
     */
    constructor(
        private httpService    : HttpService,
        private titleService   : Title,
        private pagerService   : PagerService,
        private activatedRoute : ActivatedRoute,
        private router         : Router
    ) {  }

    /**
     * On init script actions and events, here we bind available models
     */
    ngOnInit(): void
    {
        this.titleService.setTitle( 'Access' );

        this.activatedRoute.params.subscribe((params: Params) => {
            let page: number = !params['page'] ? 1 : parseInt(params['page']);

            this.httpService.get('/access/get-users-data/' + page).then(data => {
                this.allItems   = data['items'];
                this.totalCount = data['count'];
                this.setPage(page, false);
                this.displayLoadingBlock = false;
            }, err =>  {
                this.errorText = 'Error on server!';
                this.displayLoadingBlock = false;
            });
        });

        this.httpService.get('/access/get-user-roles').then(data => {
            this.availableRoles = data;
            this.enableModelsSelect = true;
        }, err =>  {
            this.errorText = 'Error response for roles dropdown!';
        });
    }

    createUser(): void
    {
        this.displayLoadingBlock = true;

        this.httpService.post('/access/create', this.accessModel)
            .then(data => {
                if (!data) {
                    this.errorText  = 'Something goes wrong with create action!';
                    this.displayLoadingBlock = false;
                    return;
                }

                this.successText= 'User was created successfully!';
                this.allItems   = data['items'];
                this.totalCount = data['count'];
                this.setPage(1, false);
                this.displayLoadingBlock = false;
            }, err =>  {
                this.errorText = 'Failed to create user';
                this.displayLoadingBlock = false;
            });
    }

    updateUser(userId: number): void
    {
        this.displayLoadingBlock = true;

        this.httpService.post('/access/update', {'userId': userId, 'newRole' : this.selectedRoleForUpdate})
            .then(data => {
                if (!data) {
                    this.errorText  = 'Something goes wrong with update action!';
                    return;
                }

                this.successText= 'User was updated successfully!';
                this.allItems   = data['items'];
                this.totalCount = data['count'];
                this.setPage(1, false);
                this.displayLoadingBlock = false;
            }, err =>  {
                this.errorText = 'Failed to update user';
                this.displayLoadingBlock = false;
            });
    }

    deleteUser(userId: number, userName: string):void
    {
        if (!confirm('Are you sure you want delete user: ' + userName)) {
            return;
        }

        this.displayLoadingBlock = true;

        this.httpService.post('/access/delete', {'userId': userId})
            .then(data => {
                if (!data) {
                    this.errorText  = 'Something goes wrong with create action!';
                    return;
                }

                this.successText= 'User was updated successfully!';
                this.allItems   = data['items'];
                this.totalCount = data['count'];
                this.setPage(1, false);
                this.displayLoadingBlock = false;
            }, err =>  {
                this.errorText = 'Failed to create user';
                this.displayLoadingBlock = false;
            });
    }

    setPage(page: number, callFromTemplate: boolean = true): void
    {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        if (callFromTemplate) {
            this.displayLoadingBlock = true;
            this.router.navigate(['/access/' + page]);
            return;
        }

        this.pager      = this.pagerService.getPager(this.totalCount, page, this.pagerPageSize);
        this.pagedItems = this.allItems;
    }

    formatTimestamp(timestamp: number): string
    {
        let dateCreated = new Date(timestamp * 1000);

        return dateCreated.toDateString();
    }
}
