<?php

use yii\db\Migration;

/**
 * Handles adding is_fixed to table `settings_scenarios`.
 */
class m161219_161304_add_is_fixed_column_to_settings_scenarios_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('settings_scenarios_table', 'isFixed', $this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('settings_scenarios_table', 'isFixed');
    }
}
