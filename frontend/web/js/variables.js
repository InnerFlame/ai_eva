//Supporting variables
var currentVariableId = null;

//Load list of variables
function updateVariables()
{
    $.ajax({
        url: '/variables/variables',
        success: function (data) {
            $('#variables').html(data);
            //Trigger click on first or current variable item
            var variableItem;
            if (currentVariableId == null) {
                variableItem = $('#variables li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="variableId"][value="' + currentVariableId + '"]';
                variableItem = $('#variables li.list-group-item.sortable').has(str);
            }
            if (variableItem.length == 0) {
                variableItem = $('#variables li.list-group-item.sortable:first-child');
            }
            if (variableItem.length == 0) {
                currentVariableId = null;
            } else {
                variableItem.removeClass('lastClickedItem');
                variableItem.click();
            }
        }
    });
}

$(document).ready(function () {
    //Load list of variables
    updateVariables();

    //Click on variables item event
    $(document).on('click', '#variables li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        var variableId = $(this).children('input[name="variableId"]').val();
        if (variableId === undefined) {
            currentVariableId = null;
            return;
        }
        currentVariableId = variableId;
    });

    //Click 'Add new variable' button
    $(document).on('click', '#addNewVariableButton', function () {
        var variableName = $('#variableName').val();
        var variableValue = $('#variableValue').val();
        var variableDescription = $('#variableDescription').val();
        variableName = encodeURIComponent(variableName);
        variableValue = encodeURIComponent(variableValue);
        variableDescription = encodeURIComponent(variableDescription);
        $.ajax({
            url: '/variables/create-variable',
            type: "POST",
            data: 'name=' + variableName + '&value=' + variableValue + '&description=' + variableDescription,
            success: function (data) {
                if (data == 'true') {
                    updateVariables();
                } else {
                    alert('An error occurred while creating the variable!');
                }
            }
        });
    });

    //Click variable delete button
    $(document).on('click', '#deleteVariableButton', function () {
        var variableName = $('#variableName').val();
        var variableId = $('#variablesBindableForm input[name="variableId"]').val();
        if (variableId.length == 0) {
            return;
        }
        $('#deleteVariableId').val(variableId);
        $('#deleteVariableName').html(variableName);
        $('#confirmDeleteVariableModal').modal('show');
    });

    // Click confirm variable delete button
    $(document).on('click', '#confirmDeleteVariableButton', function () {
        var variableId = $('#deleteVariableId').val();
        $.ajax({
            url: '/variables/delete-variable',
            type: "POST",
            data: 'variableId=' + variableId,
            success: function (data) {
                if (data == 'true') {
                    updateVariables();
                } else {
                    alert('An error occurred while deleting the variable!');
                }
                $('#confirmDeleteVariableModal').modal('hide');
            }
        });
    });

    //Click 'Update variable' button
    $(document).on('click', '#updateVariableButton', function () {
        var variableName = $('#variableName').val();
        var variableValue = $('#variableValue').val();
        var variableDescription = $('#variableDescription').val();
        variableName = encodeURIComponent(variableName);
        variableValue = encodeURIComponent(variableValue);
        variableDescription = encodeURIComponent(variableDescription);
        var variableId = $('#variablesBindableForm input[name="variableId"]').val();
        if (variableId.length == 0) {
            return;
        }
        $.ajax({
            url: '/variables/update-variable',
            type: "POST",
            data: 'variableId=' + variableId + '&name=' + variableName + '&value=' + variableValue + '&description=' + variableDescription,
            success: function (data) {
                if (data == 'true') {
                    updateVariables();
                } else {
                    alert('An error occurred while updating the variable!');
                }
            }
        });
    });
});
