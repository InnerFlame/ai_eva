<?php
use common\widgets\BindableForm\BindableForm;
use common\widgets\Modal\Modal;
use yii\bootstrap\Html;
use frontend\assets\VariablesAsset;

VariablesAsset::register($this);

//Modal confirm delete variable
echo Modal::widget([
    'id' => 'confirmDeleteVariableModal',
    'title' => 'Delete variable',
    'hiddenInputIds' => ['deleteVariableId',],
    'nameText' => 'variable',
    'nameId' => 'deleteVariableName',
    'buttonId' => 'confirmDeleteVariableButton'
]);
?>
<div class="col-md-12" id="variablesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a href="javascript:void(0);">Variables</a></li>
    </ul>
    <?php
    echo BindableForm::widget([
        'id' => 'variablesBindableForm',
        'hiddens' => [
            ['name' => 'variableId'],
        ],
        'lines' => [
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-2'],
                    'content' => Html::tag('label', 'Variable name:', [
                        'for' => 'variableName',
                        'class' => 'control-label',
                    ])
                ],
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-10'],
                    'content' => Html::input('text', 'itemText', '', [
                        'class' => 'form-control',
                        'id' => 'variableName',
                    ])
                ],
            ]],
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-2'],
                    'content' => Html::tag('label', 'Variable value:', [
                        'for' => 'variableValue',
                        'class' => 'control-label',
                    ])
                ],
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-10'],
                    'content' => Html::input('text', 'variableValue', '', [
                        'class' => 'form-control',
                        'id' => 'variableValue',
                    ])
                ],
            ]],
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-2'],
                    'content' => Html::tag('label', 'Variable description:', [
                        'for' => 'variableDescription',
                        'class' => 'control-label',
                    ])
                ],
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-10'],
                    'content' => Html::input('text', 'itemSubtext', '', [
                        'class' => 'form-control',
                        'id' => 'variableDescription',
                    ])
                ],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-2'], 'content' => Html::button('Delete', [
                    'class' => 'btn btn-danger btn-block',
                    'type' => 'button',
                    'id' => 'deleteVariableButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-3'], 'content' => Html::button('Add', [
                    'class' => 'btn btn-success btn-block',
                    'type' => 'button',
                    'id' => 'addNewVariableButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-7'], 'content' => Html::button('Update', [
                    'class' => 'btn btn-primary btn-block',
                    'type' => 'button',
                    'id' => 'updateVariableButton',
                ])],
            ]],
        ],
    ]);
    ?>
    <div id="variables"></div>
</div>
