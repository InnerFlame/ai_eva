<?php

namespace console\controllers;

use yii\console\Controller;


class ReleaseController extends Controller
{

    public function actionLock()
    {
        \Yii::$app->cache->set('adminLock', true, 3600);
        echo 'Locked' . PHP_EOL;
    }

    public function actionUnlock()
    {
        \Yii::$app->cache->delete('adminLock');
        echo 'Released' . PHP_EOL;
    }
}