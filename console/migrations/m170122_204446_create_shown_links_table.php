<?php

use yii\db\Migration;

/**
 * Handles the creation for table `shown_links`.
 */
class m170122_204446_create_shown_links_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shown_links', [
            'id'            => $this->primaryKey(),
            'userId'        => $this->string(32),
            'scenarioId'    => $this->integer(11),
            'placeholderId' => $this->integer(11),
        ]);
        $this->createIndex('userIdScenarioId', 'shown_links', ['userId', 'scenarioId']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shown_links');
    }
}
