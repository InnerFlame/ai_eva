<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\AccessAsset;

AccessAsset::register($this);

$availableRoles;
$availableRolesForSelect;
foreach (\Yii::$app->authManager->getRoles() as $key => $value) {
    $availableRoles[]               = $key;
    $availableRolesForSelect[$key]  = $key;
}
?>

<div id="editUserModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title">Edit user role</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <input id="userId" type="hidden" value="">
                        <div class="col-sm-6">
                            <label class="control-label" for="changeRole">Role for <b><span id="userName"></span></b>:</label>
                        </div>
                        <div class="col-sm-6" id="changeRole">
                            <button
                                type="button"
                                class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                                >
                                <span name="name"><?php echo $availableRoles[0];?></span>
                                <input type="hidden" value="<?php echo $availableRoles[0];?>">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php
                                for ($i = 0; $i < count($availableRoles); $i++) {
                                    echo '<li><a href="javascript:void(0);">' .
                                        $availableRoles[$i] .
                                        '</a><input type="hidden" value="' .
                                        $availableRoles[$i] .
                                        '"></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                <button id="updateRole" class="btn btn-success" type="button">Update role</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin([
            'id' => 'form-create',
            'method' => 'post',
            'action' => ['access/create'],
        ]); ?>
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'role')->dropDownList($availableRolesForSelect); ?>
        <div class="form-group">
            <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<hr/>

<?php
echo GridView::widget([
    'dataProvider' => $model->provider,
    'columns' => [
        ['attribute' => 'id',],
        ['attribute' => 'username',],
        ['attribute' => 'email',],
        [
            'attribute' => 'created_at',
            'label'     => 'Created',
            'format'    => ['datetime', 'y-MM-dd HH:mm:ss'],
            'value'     => function ($data) {
                return (int)$data->created_at;
            },
        ],
        [
            'attribute' => 'roles',
            'value'     => function ($data) {
                $rolesStr = '';
                $roles = \Yii::$app->authManager->getRolesByUser($data->id);
                foreach ($roles as $key => $val) {
                    $rolesStr .= $key . ' ';
                }
                return $rolesStr;
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Actions',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return
                        '<a data-toggle="modal" data-name="' . $model->username . '" data-id="' . $key . '" class="modal-href" href="#editUserModal">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>';
                },
            ],
        ],
    ],
]);
?>
