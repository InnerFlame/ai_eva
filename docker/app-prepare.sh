#!/bin/bash

echo "Build env ${YII_INIT_ENV}"
if [ "$YII_INIT_ENV" == "Production" ]; then
    COMPOSER_NODEV=1
fi
XDEBUG_CONFIG=remote_enable=off composer install ${COMPOSER_NODEV+"--no-dev"}

php init --env=${YII_INIT_ENV} --overwrite=${YII_INIT_OVERWRITE}

php yii migrate --interactive=0

php yii maintenance/create-user-if-not-exists

php yii rbac/init
