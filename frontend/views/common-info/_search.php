<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'action'        => ['index'],
    'method'        => 'post',
    'id'            => 'commonInfoSearch',
    'options'       => [
        'class'         => 'form-horizontal',
        'data-pjax'     => true,
    ],
    'fieldConfig'   => [
        'template' => "
            <label class='col-sm-6 control-label'>{label}:</label>{hint}{error}
            <div class='col-sm-6'>
                <button
                class='btn btn-default dropdown-toggle'
                data-toggle='dropdown'
                type='button'>
                    <span>{labelTitle}</span>
                    <span class='caret'></span>
                </button>
                <ul class='dropdown-menu'>
                    <input type='text' class='form-control livesearch' placeholder='Search'>
                    <li class='checkAll'><a href='javascript:void(0);'>Check all</a></li>
                    {input}
                </ul>
                <span class='short-summary'></span>
            </div>

        ",
        'options' => ['class' => 'col-sm-4',],
        'inputOptions'  => ['class' => 'form-control',],
    ],
]);

$renderCheckboxListItem = [
    'item' => function ($index, $label, $name, $checked, $value) {
        return '<li>' . Html::checkbox($name, $checked, [
            'value' => $value,
            'label' => Html::encode($label),
        ]) . '</li>';
    }
];
?>

<div class="form-group">
    <?= $form->field($model, 'profileTypes')
        ->checkboxList($model->availableProfileTypes, $renderCheckboxListItem)
        ->label('Profile type'); ?>
    <?= $form->field($model, 'sites')
        ->checkboxList($model->availableSites, $renderCheckboxListItem)
        ->label('Site'); ?>
    <?= $form->field($model, 'countries')
        ->checkboxList($model->availableCountries, $renderCheckboxListItem)
        ->label('Country'); ?>
</div>
<br/>
<div class="form-group">
    <?= $form->field($model, 'languages')
        ->checkboxList($model->availableLanguages, $renderCheckboxListItem)
        ->label('Language'); ?>
    <?= $form->field($model, 'statuses')
        ->checkboxList($model->availableStatuses, $renderCheckboxListItem)
        ->label('Status'); ?>
    <?= $form->field($model, 'platforms')
        ->checkboxList($model->availablePlatforms, $renderCheckboxListItem)
        ->label('Platform'); ?>
</div>
<br/>
<div class="form-group">
    <?= $form->field($model, 'trafficSources')
        ->checkboxList($model->availableTrafficSources, $renderCheckboxListItem)
        ->label('Traffic source'); ?>
    <?= $form->field($model, 'splits')
        ->checkboxList($model->availableSplits, $renderCheckboxListItem)
        ->label('Split'); ?>
    <?= $form->field($model, 'scenarios')
        ->checkboxList($model->availableScenarios, $renderCheckboxListItem)
        ->label('Scenario'); ?>
</div>
<br/>
<div class="form-group">
    <?= $form->field($model, 'placeholders')
        ->checkboxList($model->availablePlaceholders, $renderCheckboxListItem)
        ->label('Placeholder'); ?>
    <?= $form->field($model, 'dateFrom', [
        'template' =>
            "<label class='col-sm-6 control-label'>{label}:</label><div class='col-sm-6'>{input}{hint}{error}</div>",
        'inputOptions' => ['id' => 'datepickerFrom',]
    ])->label('Date from'); ?>
    <?= $form->field($model, 'dateTo', [
        'template' =>
            "<label class='col-sm-6 control-label'>{label}:</label><div class='col-sm-6'>{input}{hint}{error}</div>",
        'inputOptions' => ['id' => 'datepickerTo',]
    ])->label('Date to'); ?>
</div>
<br/>
<div class="form-group">
    <?= $form->field($model, 'placeholderDispatchTypes')
        ->checkboxList($model->availablePlaceholderDispatchTypes, $renderCheckboxListItem)
        ->label('Placeholder types'); ?>
</div>
<br/>
<div class="form-group">
    <div class="col-sm-6">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary',]) ?>
        <?= Html::button('Explain', ['class' => 'btn btn-default', 'id' => 'explainButton']) ?>
    </div>
    <div class="col-sm-6">
        <button type="button" class="btn btn-success pull-right" id="export">Export to excel</button>
    </div>
</div>

<?php ActiveForm::end();?>
