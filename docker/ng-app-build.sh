#!/bin/bash

set -e
npm install
WATCH_FLAG=""
if [ "$NG_FILE_WATCHER" == "true" ]; then
    WATCH_FLAG="-w"
fi

./node_modules/.bin/ng build ${WATCH_FLAG} &
pid="$!"
echo "PID $pid"
trap "echo 'Stopping PID $pid'; kill -SIGTERM $pid" SIGINT SIGTERM
while kill -0 $pid > /dev/null 2>&1; do wait; done
