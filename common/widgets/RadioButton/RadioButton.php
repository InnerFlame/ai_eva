<?php

namespace common\widgets\RadioButton;

use yii\bootstrap\Html;
use \yii\bootstrap\Widget;

class RadioButton extends Widget
{
    public $text;
    public $activeText;
    public $glyphiconClass;
    public $bindingGlyphicon;
    public $activeState;
    public $name;
    public $id;

    public function init()
    {
        parent::init();
        if ($this->activeState === null) {
            $this->activeState = false;
        }
        if (!isset($this->bindingGlyphicon['closest'])) {
            $this->bindingGlyphicon['closest'] = '';
        }
        if (!isset($this->bindingGlyphicon['find'])) {
            $this->bindingGlyphicon['find'] = '';
        }
        $this->text = ' ' . $this->text;
        $this->activeText = ' ' . $this->activeText;
        $view = $this->getView();
        RadioButtonAsset::register($view);
    }

    public function run()
    {
        $params = [
            'type' => 'button',
            'class' => 'btn btn-default btn-block radio-button' . ($this->activeState ? ' active' : ''),
            'data-toggle' => 'button',
            'aria-pressed' => ($this->activeState ? 'true' : 'false'),
            'autocomplete' => 'off',
            'onclick' => 'radioButtonClick(this, \''
                . $this->text . '\', \''
                . $this->activeText . '\', \''
                . $this->glyphiconClass . '\', \''
                . $this->bindingGlyphicon['closest'] . '\', \''
                . $this->bindingGlyphicon['find'] . '\');',
        ];
        if (isset($this->name)) {
            $params['name'] = $this->name;
        }
        if (isset($this->id)) {
            $params['id'] = $this->id;
        }
        $content = Html::button(
            Html::tag(
                'span',
                '',
                ['class' => 'glyphicon ' . $this->glyphiconClass]
            ) . ($this->activeState ? $this->activeText : $this->text),
            $params
        );
        return $content;
    }
}
