<?php

namespace frontend\controllers;

use frontend\models\errorLog\Log;
use yii\web\ForbiddenHttpException;

/**
 * Class ErrorLogController
 * @package frontend\controllers
 */
class ErrorLogController extends BaseNgController
{
    /* @var $paginationPageSize int */
    protected $paginationPageSize = 30;

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * @param int $page
     * @return array
     */
    public function actionGetData($page = 1): array
    {
        $page = (int) $page;

        $query = Log::find()
            ->asArray()
            ->limit($this->paginationPageSize)
            ->offset($this->getPagerOffsetFromPage($page))
            ->orderBy(['id' => SORT_DESC])
        ;

        return [
            'items' => $this->prepareDataForRender($query->all()),
            'count' => $query->count()
        ];
    }

    /**
     * @return array
     * @throws ForbiddenHttpException
     */
    public function actionClearLog(): array
    {
        if (!\Yii::$app->user->can('editSettings')) {
            throw new ForbiddenHttpException('Access denied');
        }

        return [
            'status' => (bool) Log::deleteAll()
        ];
    }

    /**
     * @param array $logData
     * @return array
     */
    private function prepareDataForRender(array $logData): array
    {
        $preparedData = [];
        foreach ($logData as $datum) {
            $datum['log_time'] = date('Y-m-d H:i:s', (int) $datum['log_time']);
            $preparedData[] = $datum;
        }

        return $preparedData;
    }

    /**
     * @inheritdoc
     *
     * @return null
     */
    protected function getFormModel()
    {
        return null;
    }
}
