export class TimeSlotsModel
{
    constructor(
        public name  : string = '',
        public time1 : string = '',
        public time2 : string = '',
        public date1 : any    = '',
        public date2 : any    = ''
    ) {  }

    public static getModelsApiUrl(): string
    {
        return "/time-slots/get-models";
    }

    public static getTiemslotsCreateApiUrl(): string
    {
        return "/time-slots/create";
    }



    public static getTimeslotDeleteApiUrl(): string
    {
        return "/time-slots/delete";
    }
}

export class TimeSlotsUpdateModel
{
    constructor(
        public timeslot_id : number,
        public name        : string,
        public time1       : string,
        public time2       : string,
        public date1       : any,
        public date2       : any
    ) {  }


    public static getTiemslotsUpdateApiUrl(): string
    {
        return "/time-slots/update";
    }

    public static getTimeslotDataApiUrl(timeslotId): string
    {
        return "/time-slots/get-timeslot-data?timeSlotId=" + timeslotId;
    }
}