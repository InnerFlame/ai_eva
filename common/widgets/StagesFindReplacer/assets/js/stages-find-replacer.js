function animatePreLoader () {
    $('#groupGroupsPhrasesContainer, #groupPhrasesContainer, #phrasesContainer, #pre-loader').toggle();
}

$('#stage-replacer-pjax')
    .on('click', '#stage-replacer-button-confirm', function() {
        $('#stage-replacer-modal').modal('hide');
    })
    .on('pjax:start', function() {
        animatePreLoader();
    })
    .on('pjax:end', function() {
        updateGroupsPhrases();
        animatePreLoader();
    });