import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { TimeSlotsComponent }            from './time.slots.component';
import { TimeSlotsUpdateComponent }      from "./time.slots.update.component";
import { SharedModule }                  from "../shared";
import { MyDatePickerModule }            from 'mydatepicker';
import { TextMaskModule } from 'angular2-text-mask';


const routing: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'time-slots',
        component: TimeSlotsComponent
    },
    {
        path: 'time-slots/update/:id',
        component: TimeSlotsUpdateComponent
    }
]);

@NgModule({
    imports: [
        routing,
        SharedModule,
        MyDatePickerModule,
        TextMaskModule
    ],
    declarations: [
        TimeSlotsComponent,
        TimeSlotsUpdateComponent
    ]
})

export class TimeSlotsModule {}