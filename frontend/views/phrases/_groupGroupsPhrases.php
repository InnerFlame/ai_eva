<?php
use common\widgets\BindableFormItems\BindableFormItems;

$groupGroupsPhrasesItems = array();
foreach ($model->groupsGroupsPhrases as $groupGroupsPhrases) {
    $item['text'] = $groupGroupsPhrases->getName();
    $item['hiddens'] = [
        ['name' => 'groupGroupsPhrasesId', 'value' => $groupGroupsPhrases->getId()],
    ];
    $groupGroupsPhrasesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'groupGroupsPhrasesBindableForm',
    'items' => $groupGroupsPhrasesItems,
]);
