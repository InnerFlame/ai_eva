export class SubstituteModel
{
    constructor(
        public id  : number,
        public text: string
    ) {  }
}

export class SubstitutePhraseModel
{
    constructor(
        public id  : number,
        public text: string
    ) {  }
}
