<?php
declare(strict_types=1);

namespace frontend\models\nlpClassifiers;

class NlpClassifier
{
    public $id;
    public $text;
    public $classes;
    public $model;
    public $type;

    public function __construct(int $id, string $text, array $classes, string $model, string $type)
    {
        $this->id       = $id;
        $this->text     = $text;
        $this->classes  = $classes;
        $this->model    = $model;
        $this->type     = $type;
    }
}
