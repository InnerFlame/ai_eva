<?php

namespace frontend\controllers;

use frontend\models\classifiers\ClassifiersNgForm;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class ClassifiersNgController
 * @package frontend\controllers
 */
class ClassifiersNgController extends BaseNgController
{
    /**
     * @inheritdoc
     *
     * ClassifiersNgController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new ClassifiersNgForm());
    }

    /**
     * @inheritdoc
     *
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('scenarios')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        }

        return false;
    }

    /**
     * Renders main Classifiers page.
     *
     * @return string Result of rendering.
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionGetClassifiers(): array
    {
        return $this->getFormModel()->getClassifiers();
    }

    /**
     * @return array
     */
    public function actionCreateClassifier(): array
    {
        return $this->getFormModel()->createClassifier($this->requestData);
    }

    /**
     * @return array
     */
    public function actionUpdateClassifier(): array
    {
        return $this->getFormModel()->updateClassifier($this->requestData);
    }

    /**
     * @return array
     */
    public function actionDeleteClassifier(): array
    {
        return $this->getFormModel()->deleteClassifier($this->requestData);
    }

    /**
     * @param $classifierId
     * @return array
     */
    public function actionGetPatterns($classifierId): array
    {
        return $this->getFormModel()->getPatternsByClassifierId((int) $classifierId);
    }

    /**
     * @return array
     */
    public function actionCreatePattern(): array
    {
        return $this->getFormModel()->createPattern($this->requestData);
    }

    /**
     * @return array
     */
    public function actionUpdatePattern(): array
    {
        return $this->getFormModel()->updatePattern($this->requestData);
    }

    /**
     * @return array
     */
    public function actionUpdateMultiPatterns(): array
    {
        return $this->getFormModel()->updateMultiPatterns($this->requestData);
    }

    /**
     * @return array
     */
    public function actionDeletePattern(): array
    {
        return $this->getFormModel()->deletePattern($this->requestData);
    }

    /**
     * @return array
     */
    public function actionDeleteMultiPatterns(): array
    {
        return $this->getFormModel()->deleteMultiPatterns($this->requestData);
    }

    /**
     * @inheritdoc
     *
     * @return ClassifiersNgForm
     */
    protected function getFormModel(): ClassifiersNgForm
    {
        return $this->formModel;
    }
}
