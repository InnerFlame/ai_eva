<?php
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use demogorgorn\ajax\AjaxSubmitButton;
use frontend\assets\TestChatAsset;

TestChatAsset::register($this);
?>

<h1>Test chat page</h1>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-2">
        <?php $availableScenarios = array();
        foreach ($model->availableScenarios as $availableScenario) {
            $availableScenarios[$availableScenario->getId()] = $availableScenario->getName();
        }

        echo $form->field($model, 'scenario')->dropDownList(
            $availableScenarios,
            [
                'prompt' => 'Select scenario'
            ]
        ); ?>
    </div>
    <div class="col-md-5">
        <?= $form->field($model, 'fromUserId') ?>
    </div>
    <div class="col-md-5">
        <?= $form->field($model, 'toUserId') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <?php $availableSettings = array();
        foreach ($model->availableSettings as $availableSetting) {
            $availableSettings[$availableSetting->id] = $availableSetting->name;
        }

        echo $form->field($model, 'setting')->dropDownList(
            $availableSettings,
            [
                'prompt' => 'Select setting'
            ]
        ); ?>
    </div>
    <div class="col-md-8">
        <?= $form->field($model, 'text') ?>
    </div>
    <div class="col-md-2">
        <label class="control-label" style="visibility: hidden">Text</label>
        <?php AjaxSubmitButton::begin([
            'label'       => 'Clear history',
            'id'          => 'TestChatClearHistory',
            'ajaxOptions' => [
                'type'    => 'POST',
                'url'     => Url::to(['/test-chat/clear']),
                'success' => new JsExpression('clearChat'),
            ],
            'options' => ['class' => 'btn btn-default', 'type' => 'button', 'style' => 'width: 100%',],
        ]);
        AjaxSubmitButton::end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <label class="control-label" style="visibility: hidden">Text</label>
        <?php AjaxSubmitButton::begin([
            'label'       => 'Send',
            'id'          => 'TestChatModelForm',
            'ajaxOptions' => [
                'type'    => 'POST',
                'url'     => Url::to(['/test-chat/send']),
                'success' => new JsExpression('sendChat'),
            ],
            'options' => ['class' => 'btn btn-success', 'type' => 'submit', 'style' => 'width: 100%',],
        ]);
        AjaxSubmitButton::end(); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<br>
<h4>Dialogue:</h4>
<div id="dialogue">
    <?php
    $key = \Yii::$app->user->getId() . '_chat';
    if (\Yii::$app->cache->get($key)) {
        echo \Yii::$app->cache->get($key);
    }
    ?>
</div>
