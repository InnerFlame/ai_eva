<?php

namespace frontend\controllers;

use frontend\controllers\traits\ClassifiersTrait;
use frontend\models\nlpClassifiers\NlpClassifiersNgForm;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class NlpClassifiersNgController
 * @package frontend\controllers
 */
class NlpClassifiersNgController extends BaseNgController
{
    use ClassifiersTrait;

    /**
     * @inheritdoc
     *
     * NlpClassifiersNgController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new NlpClassifiersNgForm());
    }

    /**
     * @inheritdoc
     *
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('scenarios')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * Bootstrap app page
     *
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * @param string $type
     * @return array
     */
    public function actionGetClassifiers($type = NlpClassifiersNgForm::TYPE_ML): array
    {
        return [
            'classifiers'     => $this->getFormModel()->getClassifiers($type),
            'availableModels' => $this->getClassifiersAvailableModels()
        ];
    }

    /**
     * @param $classifierId
     * @return array
     */
    public function actionGetClassifierClasses($classifierId): array
    {
        $classifierId = (int) $classifierId;
        $classesData  = $this->getFormModel()->getClasses($classifierId);

        return $classesData;
    }

    /**
     * @param $classId
     * @return array
     */
    public function actionGetClassifierPhrases($classId): array
    {
        $classId     = (int) $classId;
        $phrasesData = $this->getFormModel()->getPhrases($classId);

        return $phrasesData;
    }

    /**
     * @return array
     */
    public function actionCreateClassifier(): array
    {
        $createdData = $this->getFormModel()->createClassifier($this->requestData);

        return $createdData;
    }

    /**
     * @return array
     */
    public function actionUpdateClassifier(): array
    {
        $updateData = $this->getFormModel()->updateClassifier($this->requestData);

        return $updateData;
    }

    /**
     * @return array
     */
    public function actionDeleteClassifier(): array
    {
        $deleteData = $this->getFormModel()->deleteClassifier($this->requestData);

        return $deleteData;
    }

    /**
     * @return array
     */
    public function actionTrainClassifier(): array
    {
        $models = $this->getClassifiersAvailableModels();

        if ($models['trainings_processes'] == 1) {
            return [];
        }

        $trainData = $this->getFormModel()->trainClassifier($this->requestData);

        return $trainData;
    }

    public function actionGetAvailableClassifierModels()
    {
        return $this->getClassifiersAvailableModels();
    }

    /**
     * @return array
     */
    public function actionCreateClassifierClass(): array
    {
        $createData = $this->getFormModel()->createClassifierClass($this->requestData);

        return $createData;
    }

    /**
     * @return array
     */
    public function actionUpdateClassifierClass(): array
    {
        $updateData = $this->getFormModel()->updateClassifierClass($this->requestData);

        return $updateData;
    }

    /**
     * @return array
     */
    public function actionDeleteClassifierClass(): array
    {
        $deleteData = $this->getFormModel()->deleteClassifierClass($this->requestData);

        return $deleteData;
    }

    /**
     * @return array
     */
    public function actionCreateClassifierClassPhrase(): array
    {
        $createData = $this->getFormModel()->createClassifierClassPhrase($this->requestData);

        return $createData;
    }

    /**
     * @return array
     */
    public function actionUpdateClassifierClassPhrase(): array
    {
        $updateData = $this->getFormModel()->updateClassifierClassPhrase($this->requestData);

        return $updateData;
    }

    /**
     * @return array
     */
    public function actionDeleteClassifierClassPhrase(): array
    {
        $deleteData = $this->getFormModel()->deleteClassifierClassPhrase($this->requestData);

        return $deleteData;
    }

    /**
     * @return array
     */
    public function actionDeleteMultipleClassifierClassPhrase(): array
    {
        $deleteData = $this->getFormModel()->deleteMultipleClassifierClassPhrase($this->requestData);

        return $deleteData;
    }

    /**
     * @return array
     */
    public function actionCreatePhraseEntity(): array
    {
        $createData = $this->getFormModel()->createPhraseEntity($this->requestData);

        return $createData;
    }

    /**
     * @return array
     */
    public function actionUpdatePhraseEntity(): array
    {
        $updateData = $this->getFormModel()->updatePhraseEntity($this->requestData);

        return $updateData;
    }

    /**
     * @return array
     */
    public function actionDeletePhraseEntity(): array
    {
        $deleteData = $this->getFormModel()->deletePhraseEntity($this->requestData);

        return $deleteData;
    }

    /**
     * @return array
     */
    public function actionCreateMultiPhraseEntity(): array
    {
        $createData = $this->getFormModel()->createMultiPhraseEntity($this->requestData);

        return ['status' => $createData];
    }

    /**
     * @return array
     */
    public function actionUpdateMultiPhraseEntity(): array
    {
        $updateData = $this->getFormModel()->updateMultiPhraseEntity($this->requestData);

        return ['status' => $updateData];
    }

    /**
     * @return array
     */
    public function actionDeleteMultiPhraseEntity(): array
    {
        $deleteData = $this->getFormModel()->deleteMultiPhraseEntity($this->requestData);

        return ['status' => $deleteData];
    }

    /**
     * @return NlpClassifiersNgForm
     */
    protected function getFormModel(): NlpClassifiersNgForm
    {
        return $this->formModel;
    }
}
