<?php
namespace frontend\models;

use yii\db\ActiveRecord;

class ShownLinks extends ActiveRecord
{
    public static function tableName()
    {
        return 'shown_links';
    }
}
