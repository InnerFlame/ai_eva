<?php
namespace common\components;

use frontend\models\Dialogues;
use frontend\models\settings\ActionWay;
use frontend\models\settings\ConsolidatedSettings;
use frontend\models\settings\Scenario;
use frontend\models\settings\TrafficSource;
use frontend\models\ShownLinks;
use frontend\models\topics\TopicTag;
use yii\base\Component;
use yii\db\Query;
use frontend\models\scenarios\ScenarioTag;
use frontend\models\UserTag;
use yii\helpers\ArrayHelper;

class ScenariosComponent extends Component
{

    public function getScenarioId(array $params): ?int
    {
        $idFromMulti = $params['idFromMulti'];
        $idTo = $params['idTo'];

        //try to get not broken scenario for current pair
        $currentScenarioId = $this->getCurrentScenarioForPair($idFromMulti, $idTo);

        if (!empty($currentScenarioId)) {
            return $currentScenarioId;
        }

        $settingId = $this->getSettingId($params);

        if (empty($settingId)) {
            return null;
        }

        $possibleScenarios = $this->getFlexibleScenariosByPriority($settingId, $idFromMulti);

        if (empty($possibleScenarios)) {
            $possibleScenarios = $this->getFixedScenariosByPriority($settingId, $idFromMulti);
        }

        if (empty($possibleScenarios)) {
            return null;
        }

        $topScenario = array_shift($possibleScenarios);

        $this->saveNewDialogue($idFromMulti, $idTo, $topScenario['id']);

        return $topScenario['scenario_id'];
    }

    public function getFlexibleScenariosByPriority(int $settingId, string $idFromMulti): ?array
    {
        //get all available scenarios
        $possibleScenarios = $this->getScenarios($settingId);
        if (empty($possibleScenarios)) {
            return null;
        }

        $scenariosFilterMethods = [
            'filterUsedScenarios',
            'filterScenariosWithShownLinks',
            'filterCamsScenarios',
            'filterDatingScenarios',
        ];

        $possibleScenarios = $this->applyFilteringMethods($idFromMulti, $possibleScenarios, $scenariosFilterMethods);

        ArrayHelper::multisort($possibleScenarios, 'scenario_priority');

        return $possibleScenarios;
    }

    public function getFixedScenariosByPriority(int $settingId, string $idFromMulti): ?array
    {
        //get all available scenarios
        $possibleScenarios = $this->getFixedScenarios($settingId, $idFromMulti);
        if (empty($possibleScenarios)) {
            return null;
        }

        $scenariosFilterMethods = [
            'filterCamsScenarios',
            'filterDatingScenarios',
        ];

        $possibleScenarios = $this->applyFilteringMethods($idFromMulti, $possibleScenarios, $scenariosFilterMethods);

        ArrayHelper::multisort($possibleScenarios, ['dialoguesCount', 'scenario_priority']);

        return $possibleScenarios;
    }

    protected function applyFilteringMethods(string $idFromMulti, array $possibleScenarios, array $scenariosFilterMethods): array
    {
        foreach ($scenariosFilterMethods as $method) {
            $possibleScenarios = $this->$method($idFromMulti, $possibleScenarios);
            if (empty($possibleScenarios)) {
                return [];
            }
        }

        return $possibleScenarios;
    }

    protected function getCurrentScenarioForPair(string $idFromMulti, string $idTo): ?int
    {
        $scenarioId = (new Query())
            ->select('s.scenario_id')
            ->from(Scenario::tableName() . ' as s')
            ->innerJoin(Dialogues::tableName() . ' as d', 's.id = d.settingScenarioId')
            ->where([
                'idFrom' => $idFromMulti,
                'idTo' => $idTo,
            ])
            ->limit(1)
            ->scalar();

        return $scenarioId;
    }

    protected function filterUsedScenarios(string $idFromMulti, array $possibleScenarios): array
    {
        $excludedSettingScenariosIds = (new Query())
            ->select('settingScenarioId')
            ->from(Dialogues::tableName())
            ->where(['idFrom' => $idFromMulti])
            ->column();

        $filteredScenarios = [];
        foreach ($possibleScenarios as $scenario) {
            if (!in_array($scenario['id'], $excludedSettingScenariosIds)) {
                $filteredScenarios[] = $scenario;
            }
        }

        return $filteredScenarios;
    }

    protected function filterCamsScenarios(string $idFromMulti, array $possibleScenarios)
    {
        $tagId = TopicTag::TAG_ID_CAMS;
        return $this->filterScenariosByTag($tagId, $idFromMulti, $possibleScenarios);
    }

    protected function filterDatingScenarios(string $idFromMulti, array $possibleScenarios): array
    {
        $tagId = TopicTag::TAG_ID_DATING;
        return $this->filterScenariosByTag($tagId, $idFromMulti, $possibleScenarios);
    }

    protected function filterScenariosByTag(int $tagId, string $idFromMulti, array $possibleScenarios): array
    {
        $userCamsTag = UserTag::findOne([
            'userId'    => $idFromMulti,
            'tagId'     => $tagId,
        ]);

        if (empty($userCamsTag)) {
            //Nothing to filter
            return $possibleScenarios;
        }

        $excludedScenariosIds = (new Query())
            ->select('scenarioId')
            ->from(ScenarioTag::tableName())
            ->where([
                'scenarioId'    => array_keys($possibleScenarios),
                'tagId'         => $tagId,
            ])
            ->column();

        $filteredScenarios = [];
        foreach ($possibleScenarios as $scenario) {
            if (!in_array($scenario['scenario_id'], $excludedScenariosIds)) {
                $filteredScenarios[] = $scenario;
            }
        }

        return $filteredScenarios;
    }

    protected function filterScenariosWithShownLinks(string $idFromMulti, array $possibleScenarios): array
    {
        $excludedScenariosIds = (new Query())
            ->select('scenarioId')
            ->from(ShownLinks::tableName())
            ->where([
                'userId' => $idFromMulti,
                'isPredispatched' => false
            ])
            ->column();

        $filteredScenarios = [];
        foreach ($possibleScenarios as $scenario) {
            if (
                !empty($scenario['isFixed'])
                || !in_array($scenario['scenario_id'], $excludedScenariosIds)
            ) {
                $filteredScenarios[] = $scenario;
            }
        }

        return $filteredScenarios;
    }

    protected function getProjectBySiteHash($hash)
    {
        $project = '';
        $projects = \Yii::$app->params['projectBySiteId'];
        foreach ($projects as $currentProject => $sites) {
            foreach ($sites as $siteHash => $siteName) {
                if ($hash == $siteHash) {
                    $project = $currentProject;
                    break 2;
                }
            }
        }
        return $project;
    }

    protected function saveNewDialogue(string $idFromMulti, string $idTo, int $settingScenarioId)
    {
        $dialogue = new Dialogues([
            'idFrom' => $idFromMulti,
            'idTo' => $idTo,
            'settingScenarioId' => $settingScenarioId,
        ]);
        $dialogue->save();
    }
    
    protected function getScenarios(int $settingId): ?array
    {
        //Get scenarios
        $scenarios = Scenario::find()
            ->where(['setting_id' => $settingId])
            ->asArray()
            ->all();

        return $scenarios;
    }

    protected function getFixedScenarios(int $settingId, string $idFromMulti): ?array
    {
        $scenarios = (new Query())
            ->select('s.id, s.scenario_id, s.scenario_priority, COUNT(d.id) as dialoguesCount')
            ->from(Scenario::tableName() . ' as s')
            ->leftJoin(Dialogues::tableName() . ' as d', 's.id = d.settingScenarioId and d.idFrom = :idFrom')
            ->where([
                'setting_id' => $settingId,
                'isFixed' => 1,
            ])
            ->addParams([':idFrom' => $idFromMulti])
            ->groupBy('s.id')
            ->all();

        return $scenarios;
    }

    protected function getSettingId($params)
    {
        $splitConditions = ['or'];
        $project         = $this->getProjectBySiteHash($params['site']);

        $query = (new Query())
            ->select('s.locale_name, s.setting_id')
            ->from(ConsolidatedSettings::tableName() . ' as s')
            ->where([
                'control_profile'   => $params['controlProfileType'],
                'country_name'      => $params['country'],
                'status_name'       => $params['paidStatus'],
                'site_hash'         => $params['site'],
                'platform_name'     => $params['platform'],
                'project_name'      => $project,
                'active'            => 1,
            ])
            ->orderBy('priority');

        if (in_array($params['country'], ['FIN', 'USA', 'CAN', 'AUS'])) {
            if ($params['prepaid'] == 1) {
                $query->andWhere(['target_profile' => 'PrepaidUsers']);
            } else {
                $query->andWhere(['target_profile' => 'Phoenix']);
            }
        }

        if (!empty($params['splits'])) {
            foreach ($params['splits'] as $splitName => $group) {
                $splitConditions[] = ['and', ['s.splitName' => $splitName], ['s.splitGroup' => $group]];
            }
        }
        $splitConditions[] = ['and', ['s.splitName' => NULL], ['s.splitGroup' => NULL]];
        $splitConditions[] = ['and', ['s.splitName' => ''], ['s.splitGroup' => '']];

        $query->andWhere($splitConditions);


        if (isset($params['language'])) {
            $query->andWhere([
                'or',
                ['locale_name' => $params['language']],
                ['locale_name' => 'DEF']
            ]);
        }

        if (isset($params['senderLanguage'])) {
            $query->andWhere([
                'or',
                ['sender_locale_name' => $params['senderLanguage']],
                ['sender_locale_name' => 'DEF']
            ]);
        }

        if (isset($params['isFanClubAllowed'])) {
            $query->andWhere([
                'is_fan_club_allowed' => $params['isFanClubAllowed'],
            ]);
        }

        if (isset($params['isOnline'])) {
            $query->andWhere([
                'is_online' => $params['isOnline'],
            ]);
        }

        if (!empty($params['trafficSource'])) {
            $query->innerJoin(TrafficSource::tableName() . ' as ts', 's.setting_id = ts.setting_id')
                ->andWhere(['ts.traffic_source_name' => $params['trafficSource']]);
        }
        if (!empty($params['actionWay'])) {
            $query->innerJoin(ActionWay::tableName() . ' as aw', 's.setting_id = aw.setting_id')
                ->andWhere(['aw.action_way_name' => $params['actionWay']]);
        }

        $settings = $query->all();

        if (empty($settings)) {
            return null;
        }

        $settingsByLang = [];
        foreach ($settings as $setting) {
            $settingsByLang[$setting['locale_name']][] = $setting['setting_id'];
        }

        if (isset($params['language']) && !empty($settingsByLang[$params['language']])) {
            $settingId = array_shift($settingsByLang[$params['language']]);
        } elseif (!empty($settingsByLang['DEF'])) {
            $settingId = array_shift($settingsByLang['DEF']);
        } else {
            return null;
        }

        return $settingId;
    }
}
