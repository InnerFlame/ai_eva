<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings_online`.
 */
class m170302_100406_create_settings_online_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_online', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'is_online' => $this->boolean(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_online');
    }
}
