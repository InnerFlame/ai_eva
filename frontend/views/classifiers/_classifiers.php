<?php
use common\widgets\BindableFormItems\BindableFormItems;

$classifiersItems = [];
foreach ($model->classifiers as $classifiers) {
    $item['text']    = $classifiers->getText();
    $item['hiddens'] = [
        [
            'name'  => 'classifierId',
            'value' => $classifiers->getId(),
        ],
    ];
    $classifiersItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'classifiersBindableForm',
    'items'          => $classifiersItems,
]);
