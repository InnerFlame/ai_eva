<?php

namespace common\widgets\Modal;

use yii\bootstrap\Html;
use \yii\bootstrap\Widget;

class Modal extends Widget
{
    public $id;
    public $title;
    public $inputId;
    public $inputLabelText;
    public $hiddenInputIds;
    public $nameId;
    public $nameText;
    public $buttonId;
    public $buttonText;
    public $hasForm;
    public $isMultilineQuestion;

    public function init()
    {
        parent::init();
        if (!isset($this->hasForm)) {
            $this->hasForm = false;
        }
        $view = $this->getView();
        ModalAsset::register($view);
    }

    public function run()
    {
        $content = Html::beginTag('div', ['class' => 'modal fade', 'role' => 'dialog', 'id' => $this->id,]);
        $content .= Html::beginTag('div', ['class' => 'modal-dialog',]);
        $content .= Html::beginTag('div', ['class' => 'modal-content',]);

        $content .= Html::beginTag('div', ['class' => 'modal-header',]);
        $content .= Html::tag('button', '&times;', [
            'type' => 'button',
            'class' => 'close',
            'data-dismiss' => 'modal',
        ]);
        $content .= Html::tag('h4', $this->title, ['class' => 'modal-title',]);
        $content .= Html::endTag('div');

        $content .= Html::beginTag('div', ['class' => 'modal-body',]);
        if (isset($this->hiddenInputIds)) {
            foreach ($this->hiddenInputIds as $hiddenInputId) {
                $content .= Html::tag('input', null, ['type' => 'hidden', 'id' => $hiddenInputId,]);
            }
        }
        if ($this->hasForm) {
            $content .= Html::beginTag('form', ['class' => 'form-horizontal',]);
            $content .= Html::beginTag('div', ['class' => 'form-group',]);
            $content .= Html::beginTag('div', ['class' => 'col-sm-3',]);
            $content .= Html::tag('label', $this->inputLabelText, [
                'for' => $this->inputId,
                'class' => 'control-label',
            ]);
            $content .= Html::endTag('div');
            $content .= Html::beginTag('div', ['class' => 'col-sm-9',]);
            $content .= Html::tag('input', null, [
                'type' => 'text',
                'class' => 'form-control',
                'id' => $this->inputId,
            ]);
            $content .= Html::endTag('div');
            $content .= Html::endTag('div');
            $content .= Html::endTag('form');
        } else {
            $content .= '<p>Are you sure want to delete ' . $this->nameText;
            if (isset($this->isMultilineQuestion) && $this->isMultilineQuestion == true) {
                $content .= '<br/>';
            }
            $content .= ' <b id=\'' . $this->nameId . '\'></b>?</p>';
        }
        $content .= Html::endTag('div');

        $content .= Html::beginTag('div', ['class' => 'modal-footer',]);
        $content .= Html::tag('button', 'Cancel', [
            'type' => 'button',
            'class' => 'btn btn-default',
            'data-dismiss' => 'modal',
        ]);
        if ($this->hasForm) {
            $content .= Html::tag('button', $this->buttonText, [
                'type' => 'button',
                'class' => 'btn btn-success',
                'id' => $this->buttonId,
                'disabled' => 'true',
            ]);
        } else {
            $content .= Html::tag('button', 'Delete', [
                'type' => 'button',
                'class' => 'btn btn-danger',
                'id' => $this->buttonId,
            ]);
        }
        $content .= Html::endTag('div');

        $content .= Html::endTag('div');
        $content .= Html::endTag('div');
        $content .= Html::endTag('div');

        return $content;
    }
}
