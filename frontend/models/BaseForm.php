<?php
declare(strict_types=1);

namespace frontend\models;

class BaseForm
{
    public function __call($name, $arguments)
    {
        $dataProviderInstanceLink = DataProvider::getInstance();
        return call_user_func_array(array($dataProviderInstanceLink, $name), $arguments);
    }
}
