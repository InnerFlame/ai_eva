<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_sites_table`.
 */
class m160622_141617_create_settings_sites_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_sites_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'site_hash' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_sites_table');
    }
}
