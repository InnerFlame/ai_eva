<?php

namespace frontend\models\errorLog;

use yii\db\ActiveRecord;

class Log extends ActiveRecord
{
    public static function tableName()
    {
        return 'log';
    }
}
