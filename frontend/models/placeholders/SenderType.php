<?php

namespace frontend\models\placeholders;

use yii\db\ActiveRecord;

class SenderType extends ActiveRecord
{
    public static function tableName()
    {
        return 'placeholders_sender_types_table';
    }

    public function getPlaceholder()
    {
        return $this->hasOne(Placeholder::className(), ['id' => 'placeholder_id']);
    }
}
