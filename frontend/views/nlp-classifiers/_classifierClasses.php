<?php
use common\widgets\BindableFormItems\BindableFormItems;

$classifierClassesItems = [];

foreach ($model->classes as $class) {
    $item['text'] = $class->text;
    $item['hiddens'] = [
        ['name' => 'classifierId',      'value' => $class->classifierId],
        ['name' => 'classId',           'value' => $class->id],
        ['name' => 'itemProbability',   'value' => $class->probability],
    ];
    $classifierClassesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId'    => 'classesBindableForm',
    'items'             => $classifierClassesItems,
]);
