<?php

use yii\db\Migration;

/**
 * Handles the creation for table `messages`.
 */
class m160927_143108_create_communicationLog_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('communicationLog', [
            'id'                => $this->primaryKey(),
            'profileType'       => $this->string(16),
            'country'           => $this->string(3),
            'platform'          => $this->string(32),
            'scenarioId'        => $this->integer(11),
            'project'           => $this->string(16),
            'language'          => $this->string(3),
            'trafficSource'     => $this->string(32),
            'placeholderId'     => $this->integer(11)->defaultValue(0),
            'siteHash'          => $this->string(32),
            'status'            => $this->string(4),
            'createdAt'         => "timestamp DEFAULT CURRENT_TIMESTAMP",
            'splitId'           => $this->integer(11),
            'idFrom'            => $this->string(32),
            'idTo'              => $this->string(32),
            'isUser'            => $this->boolean(),
        ]);
        $this->createIndex('country-siteHash', 'communicationLog', ['country', 'siteHash']);
        $this->createIndex('createdAt', 'communicationLog', 'createdAt');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('communicationLog');
    }
}
