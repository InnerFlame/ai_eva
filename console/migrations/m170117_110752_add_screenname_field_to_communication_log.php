<?php

use yii\db\Migration;

class m170117_110752_add_screenname_field_to_communication_log extends Migration
{
    public function up()
    {
        $this->addColumn('communicationLog', 'screenname', $this->string(64)->defaultValue(''));
    }

    public function down()
    {
        $this->dropColumn('communicationLog', 'screenname');
    }
}
