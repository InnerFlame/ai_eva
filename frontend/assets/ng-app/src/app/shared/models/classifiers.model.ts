export class ClassifiersModel {
    constructor(
        public classifier_id: number = 0,
        public text: string = '',
        public deleted: number = 0,
        public version: number = 0,
    ) {}

    public load(classifier_id: number, text: string, deleted: number, version: number)
    {
        this.classifier_id = classifier_id;
        this.text = text;
        this.deleted = deleted;
        this.version = version;
    }

    public reset()
    {
        this.classifier_id = 0;
        this.text = '';
        this.deleted = 0;
        this.version = 0;
    }
}

export class PatternsModel {
    constructor(
        public classifier_id: number = 0,
        public classifier_pattern_id: number = 0,
        public text: string = '',
        public test: string = '',
        public deleted: number = 0,
        public version: number = 0,
        public active: number = 0,
    ) {}

    public load(classifier_pattern_id: number, classifier_id: number, text: string, test: string, deleted: number, version: number, active: number)
    {
        this.classifier_id = classifier_id;
        this.classifier_pattern_id = classifier_pattern_id;
        this.text = text;
        this.test = test;
        this.deleted = deleted;
        this.version = version;
        this.active = active;
    }

    public reset()
    {
        this.classifier_id = 0;
        this.classifier_pattern_id = 0;
        this.text = '';
        this.test = '';
        this.deleted = 0;
        this.version = 0;
        this.active = 0;
    }
}

export class ClassifiersRouting
{
    private static baseUrl = '/classifiers-ng/';

    public static getClassifierCreateUrl(): string
    {
        return this.baseUrl + 'create-classifier';
    }

    public static getClassifierUpdateUrl(): string
    {
        return this.baseUrl + 'update-classifier';
    }

    public static getClassifierDeleteUrl(): string
    {
        return this.baseUrl + 'delete-classifier';
    }

    public static getClassifiersUrl(): string
    {
        return this.baseUrl + 'get-classifiers';
    }

    public static getClassifierPatternCreateUrl(): string
    {
        return this.baseUrl + 'create-pattern';
    }

    public static getClassifierPatternUpdateUrl(): string
    {
        return this.baseUrl + 'update-pattern';
    }

    public static getClassifierPatternDeleteUrl(): string
    {
        return this.baseUrl + 'delete-pattern';
    }

    public static getClassifierPatternMultiUpdateUrl(): string
    {
        return this.baseUrl + 'update-multi-patterns';
    }

    public static getClassifierPatternMultiDeleteUrl(): string
    {
        return this.baseUrl + 'delete-multi-patterns';
    }

    public static getClassifierPatternsUrl(): string
    {
        return this.baseUrl + 'get-patterns';
    }
}


