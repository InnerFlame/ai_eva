import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { TestChatComponent }             from './test.chat.component';
import { SharedModule }                  from "../shared";
import { Select2Component }              from 'angular-select2-component';

const phrasesRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'test-chat',
        component: TestChatComponent
    }
]);

@NgModule({
    imports: [
        phrasesRouting,
        SharedModule
    ],
    declarations: [
        TestChatComponent,
        Select2Component
    ]
})

export class TestChatModule {}