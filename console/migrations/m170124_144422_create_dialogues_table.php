<?php

use yii\db\Migration;

/**
 * Handles the creation for table `dialogues`.
 */
class m170124_144422_create_dialogues_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('dialogues', [
            'id'            => $this->primaryKey(),
            'idFrom'        => $this->string(32),
            'idTo'          => $this->string(32),
            'scenarioId'    => $this->integer(11),
            'createdAt'     => "timestamp DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->createIndex('idFromTo', 'dialogues', ['idFrom', 'idTo']);
        $this->createIndex('createdAt', 'dialogues', 'createdAt');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('dialogues');
    }
}
