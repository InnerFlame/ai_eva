<?php

namespace console\controllers;

use yii\console\Controller;


class CombineScenarioSettingsController extends Controller
{

    public function actionIndex()
    {
        \Yii::$app->db->createCommand('TRUNCATE TABLE `consolidatedSettings`')->execute();

        \Yii::$app->db->createCommand('
            INSERT INTO `consolidatedSettings`
                (`setting_id`,`name`,`control_profile`,`target_profile`,
                `active`,`country_name`,`status_name`,`site_hash`,
                `traffic_type_name`,`platform_name`)
            SELECT
                settings_table.`id`,`name`,`control_profile`,`target_profile`,
                `active`,`country_name`,`status_name`,`site_hash`,
                `traffic_type_name`,`platform_name`
            FROM settings_table
            JOIN settings_countries_table ON settings_table.id = settings_countries_table.setting_id
            JOIN settings_statuses_table ON settings_table.id = settings_statuses_table.setting_id
            JOIN settings_sites_table ON settings_table.id = settings_sites_table.setting_id
            JOIN settings_traffic_types_table ON settings_table.id = settings_traffic_types_table.setting_id
            JOIN settings_platforms_table ON settings_table.id = settings_platforms_table.setting_id
        ')->execute();
    }
}