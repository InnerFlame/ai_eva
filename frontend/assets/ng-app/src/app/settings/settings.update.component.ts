import { Component}                                from '@angular/core';
import { Title }                                   from "@angular/platform-browser";
import { HttpService }                             from "../shared/services/http.service";
import { OnInit }                                  from '@angular/core';
import { SettingsUpdateModel }                     from "../shared/models/settings.model";
import { ActivatedRoute, Params }                  from "@angular/router";
import { IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { DragulaService }                          from "ng2-dragula";

@Component({
    selector    : 'settingsUpdate',
    templateUrl : './settings.update.html',
})

export class SettingsUpdateComponent implements OnInit
{
    public errorText            : string  = '';
    public successText          : string  = '';
    public displayLoadingBlock  : boolean = true;
    public scenarioFilter       : string  = '';
    public multipleSitesAdd     : any     = '';
    public settingsModel        : SettingsUpdateModel;
    public dataHolders          : any;
    private originalScenarios   : any;

    public multiSelectSettings: IMultiSelectSettings = {
        enableSearch           : true,
        showCheckAll           : true,
        showUncheckAll         : true,
        checkedStyle           : 'fontawesome',
        buttonClasses          : 'btn btn-default btn-block multiSelectSettingsButton',
        dynamicTitleMaxItems   : 900,
        displayAllSelectedText : false,
    };

    public multiSelectTexts: IMultiSelectTexts = {
        checkAll           : 'Select all',
        uncheckAll         : 'Unselect all',
        checked            : 'item selected',
        checkedPlural      : 'items selected',
        searchPlaceholder  : 'Find',
        searchEmptyResult  : 'Nothing found...',
        searchNoRenderText : 'Type in search box to see results...',
        defaultTitle       : 'Select Items',
        allSelected        : 'All selected'
    };

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param activatedRoute
     * @param titleService
     * @param dragulaService
     */
    constructor(
        private httpService: HttpService,
        private activatedRoute: ActivatedRoute,
        private titleService: Title,
        private dragulaService: DragulaService
    ) {
        this.titleService.setTitle( 'Update Setting' );
    }


    /**
     * On init script actions and events, here we bind data for update
     */
    ngOnInit(): void
    {
        this.activatedRoute.params.subscribe((params: Params) => {
            let settingId = params['id'];

            this.httpService.get(SettingsUpdateModel.getSettingUpdateDataApiUrl(settingId)).then(data => {
                if (!data) {
                    this.errorText = 'Something goes wrong with response!';
                } else {
                    this.dataHolders = data['holders'];
                    this.originalScenarios = data['holders'].availableScenarios;
                    this.bindInitialData(data['modelData']);
                    this.displayLoadingBlock = false;
                }
            });
        });
    }

    /**
     * Bind model
     *
     * @param modelData
     */
    private bindInitialData(modelData: any) {
        this.settingsModel = new SettingsUpdateModel(
            modelData.original.id,
            modelData.original.name,
            modelData.original.control_profile,
            modelData.original.target_profile,
            modelData.original.active,
            modelData.original.priority,
            modelData.relations.countries,
            modelData.relations.locales,
            modelData.relations.senderLocales,
            modelData.relations.statuses,
            modelData.relations.sites,
            modelData.relations.traffSources,
            modelData.relations.actionWays,
            modelData.relations.traffTypes,
            modelData.relations.platforms,
            modelData.relations.fanClubs,
            modelData.relations.onlines,
            modelData.relations.scenarios,
            modelData.relations.projects,
            modelData.original.splitName,
            modelData.original.splitGroup
        );
    }

    public filterScenarios(): void
    {
        let originalScenarios = this.originalScenarios;
        let filteredScenarios = [];

        if (!this.scenarioFilter) {
            this.dataHolders.availableScenarios = originalScenarios;
            return;
        }

        for (let key in originalScenarios) {
            if (!originalScenarios.hasOwnProperty(key) || !originalScenarios[key]) {
               continue;
            }

            if (originalScenarios[key].name.indexOf(this.scenarioFilter) !== -1) {
                filteredScenarios.push(originalScenarios[key]);
            }
        }

        this.dataHolders.availableScenarios = filteredScenarios;
    }

    public toggleIsFixed(key: string): void
    {
        let isNowFixed = this.settingsModel.scenarios[key].isFixed;

        if (isNowFixed) {
            this.settingsModel.scenarios[key].isFixed = 0;
            return;
        }

        this.settingsModel.scenarios[key].isFixed = 1;
    }

    public unsetModelScenario(i)
    {
        delete this.settingsModel.scenarios[i];
    }

    public parseMultiSitesString(): void
    {
        if (!this.multipleSitesAdd) {
            alert('Nothing to parse');
            return;
        }

        let sitesToAdd: Array<any>       = [];
        let parsedSites: Array<any>      = this.multipleSitesAdd.split(' ');
        let availableSitesLength: number = this.dataHolders.availableSites.length;
        let parsedSitesLength: number    = parsedSites.length;

        for (let i = 0; i < availableSitesLength; i++)
        {
            for (let j = 0; j < parsedSitesLength; j++)
            {
                if (this.dataHolders.availableSites[i].name == parsedSites[j])
                {
                    sitesToAdd.push(this.dataHolders.availableSites[i].id);
                }
            }
        }

        let mergedData = this.settingsModel.sites.concat(sitesToAdd);

        this.settingsModel.sites = mergedData.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });
    }


    public isValidSplitData(): boolean
    {
        if (!this.settingsModel.splitName && (this.settingsModel.splitGroup === null)) {
            return true;
        }

        if (this.settingsModel.splitName && ((this.settingsModel.splitGroup !== null) && (this.settingsModel.splitGroup >= 0))) {
            return true;
        }

        return false;
    }

    /**
     * Submit form handler
     */
    onSubmit(): void
    {
        this.displayLoadingBlock = true;
        this.successText         = '';
        this.errorText           = '';

        this.httpService.post(SettingsUpdateModel.getUpdateSettingApiUrl(), this.settingsModel)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.errorText  = 'Something goes wrong with create action!';
                } else {
                    this.successText = 'Setting was updated successfully!!';
                }

                this.displayLoadingBlock = false;
            }, err =>  {
                this.errorText = 'Failed to update setting';
                this.displayLoadingBlock = false;
            });
    }
}
