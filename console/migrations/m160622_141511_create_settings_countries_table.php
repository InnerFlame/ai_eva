<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_countries_table`.
 */
class m160622_141511_create_settings_countries_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_countries_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'country_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_countries_table');
    }
}
