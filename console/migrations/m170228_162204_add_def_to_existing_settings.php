<?php

use yii\db\Migration;

class m170228_162204_add_def_to_existing_settings extends Migration
{
    public function up()
    {
        $this->update('settings_locales', ['locale_name' => 'DEF'], ['locale_name' => '']);
        $this->update('consolidatedSettings', ['locale_name' => 'DEF'], ['locale_name' => '']);
        Yii::$app->db->createCommand('INSERT INTO settings_locales(setting_id, locale_name) SELECT id, \'DEF\' FROM settings_table;')
            ->execute();
    }

    public function down()
    {
        $this->update('settings_locales', ['locale_name' => ''], ['locale_name' => 'DEF']);
        $this->update('consolidatedSettings', ['locale_name' => ''], ['locale_name' => 'DEF']);
        Yii::$app->db->createCommand('DELETE FROM settings_locales;')
            ->execute();
    }
}
