<?php

declare(strict_types=1);

namespace common\components\BrungildaApi;

use yii\base\Object;
use linslin\yii2\curl;

class BrungildaApiCurl extends Object implements BrungildaApi
{
    public $config;
    protected $curl;

    /**
     * Method for cURL initialization
     *
     * @return void
     */
    protected function curlInit()
    {
        $this->curl = new curl\Curl();
        foreach ($this->config['curlOptions'] as $key => $value) {
            $this->curl->setOption(constant($key), $value);
        }
    }

    /**
     * Returns data by method.
     *
     * @param string $method Method name
     * @param array  $params Query params
     *
     * @return array
     */
    protected function getData(string $method, array $params = []) : array
    {
        $this->curlInit();
        $url = $this->config['curlDomain'];

        switch ($method) {
        case 'get_classifier_patterns':
        case 'get_classifier_class_patterns':
        case 'get_gambits':
        case 'get_phrases':
        case 'get_rule_by_id':
        case 'get_rule_by_name':
        case 'get_rules':
        case 'search/exception_patterns':
        case 'search/gambits':
        case 'search/rule':
        case 'search/patterns':
        case 'search/templates':
        case 'search/phrases':
        case 'get_substitute_phrases':
        case 'get_classifier_classes':
        case 'get_learning_phrases':
            $url .= "{$method}/{$params[0]}";
            break;
        case 'get_gambit/rules':
        case 'get_rule/rules':
            $url .= "{$method}/{$params[0]}/{$params[1]}";
            if (isset($params[2])) {
                $url .= "/{$params[2]}";
            }
            break;
        default:
            $url .= $method;
            break;
        }

        return $this->processResult(
            $this->curl->get($url)
        );
    }

    /**
     * Posts and returns data by method.
     *
     * @param string $method   Method name
     * @param array  $entities Entities which we are modifying.
     *
     * @return array
     */
    protected function postData(string $method, array $entities): array
    {
        $this->curlInit();
        $url = $this->config['curlDomain'] . $method;

        $this->curl->setOption(CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $this->curl->setOption(CURLOPT_POSTFIELDS, json_encode($entities));

        return $this->processResult(
            $this->curl->post($url)
        );
    }

    /**
     * Method for processing result got via cURL.
     *
     * @param mixed $result Result which we need to process.
     *
     * @return array
     */
    protected function processResult($result)
    {
        $decoded = json_decode($result, true);
        $error   = json_last_error();

        if (!empty($error)) {
            throw new \Exception(
                'Error while decoding json. Code: ' . json_last_error() . ', message: "' . json_last_error_msg() . '".'
            );
        }

        return $decoded;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $fromUserId    Sender user id.
     * @param string $toUserId      Recipient user id.
     * @param string $text          Message text.
     * @param string $scenarioName  Name of scenario.
     *
     * @return array Result of trying of sending chat message.
     */
    public function chat(string $fromUserId, string $toUserId, string $text, int $scenarioId): array
    {
        return $this->postData(
            'chat',
            [
                'from' => $fromUserId,
                'to'   => $toUserId,
                'text' => $text,
                'scenario_id' => $scenarioId,
            ]
        );
    }

    public function testClassifierModel(string $model, string $text, string $project)
    {
        return $this->postData('check_model', ['model' => $model, 'text' => $text, 'project' => $project]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getTopics() : array
    {
        return $this->getData('get_topics');
    }

    /**
     * @return array
     */
    public function getTopicsGroup() : array
    {
        return $this->getData('get_group_topics_by_scenario');
    }

    public function getRestrictions() : array
    {
        return $this->getData('get_restrictions');
    }

    public function createRestrictions(array $restrictions): array
    {
        return $this->postData('restrictions/create', $restrictions);
    }

    public function deleteRestrictions(array $restrictions): array
    {
        return $this->postData('restrictions/delete', $restrictions);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $topics Array of arrays with topics data for creating.
     *
     * @return array Array of arrays with created topics data.
     */
    public function createTopics(array $topics): array
    {
        return $this->postData('topics/create', $topics);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $topics Array of arrays with topics data for deleting.
     *
     * @return array Array [result => array of deleted topics, affectedRows => count of deleted topics]
     */
    public function deleteTopics(array $topics): array
    {
        return $this->postData('topics/delete', $topics);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $topics Array of arrays with topics data for updating.
     *
     * @return array Array [result => array of updated topics, affectedRows => count of updated topics]
     */
    public function updateTopics(array $topics): array
    {
        return $this->postData('topics/update', $topics);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getSubstitutes(): array
    {
        return $this->getData('get_substitutes');
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getSubstitutePhrases(int $substituteId): array
    {
        return $this->getData('get_substitute_phrases', [$substituteId]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getGroupPhrases(): array
    {
        return $this->getData('get_group_phrases');
    }

    /**
     * {@inheritdoc}
     *
     * @param int $groupPhraseId Group phrase id
     *
     * @return array
     */
    public function getPhrases(int $groupPhraseId): array
    {
        return $this->getData('get_phrases', [$groupPhraseId]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getClassifiers(): array
    {
        return $this->getData('get_classifiers');
    }

    /**
     * {@inheritdoc}
     *
     * @param int $classifierId Classifier id
     *
     * @return array
     */
    public function getClassifierPatterns(int $classifierId): array
    {
        return $this->getData('get_classifier_patterns', [$classifierId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $classId
     *
     * @return array
     */
    public function getClassPatterns(int $classId): array
    {
        return $this->getData('get_classifier_class_patterns', [$classId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $classifiers Array of arrays with classifiers data for creating.
     *
     * @return array Array of arrays with created classifiers data.
     */
    public function createClassifiers(array $classifiers): array
    {
        return $this->postData('classifiers/create', $classifiers);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $classifiers Array of arrays with classifiers data for updating.
     *
     * @return array Array [result => array of updated classifiers, affectedRows => count of updated classifiers]
     */
    public function updateClassifiers(array $classifiers): array
    {
        return $this->postData('classifiers/update', $classifiers);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $classifiers Array of arrays with classifiers data for deleting.
     *
     * @return array Array [result => array of deleted classifiers, affectedRows => count of deleted classifiers]
     */
    public function deleteClassifiers(array $classifiers): array
    {
        return $this->postData('classifiers/delete', $classifiers);
    }

    /**
     * Creates new classifier patterns and returns their data.
     *
     * @param array $classifierPatterns Array of arrays with classifier patterns data for creating.
     *
     * @return array Array of arrays with created classifier patterns data.
     */
    public function createClassifierPatterns(array $classifierPatterns): array
    {
        return $this->postData('classifier_patterns/create', $classifierPatterns);
    }

    /**
     * Updates classifier patterns and returns their data.
     *
     * @param array $classifierPatterns Array of arrays with classifier patterns data for updating.
     *
     * @return array Array [result => array of updated classifier patterns, affectedRows => count of updated classifier patterns]
     */
    public function updateClassifierPatterns(array $classifierPatterns): array
    {
        return $this->postData('classifier_patterns/update', $classifierPatterns);
    }

    /**
     * Deletes classifier patterns and returns their data.
     *
     * @param array $classifierPatterns Array of arrays with classifier patterns data for deleting.
     *
     * @return array Array [result => array of deleted classifier patterns, affectedRows => count of deleted classifier patterns]
     */
    public function deleteClassifierPatterns(array $classifierPatterns): array
    {
        return $this->postData('classifier_patterns/delete', $classifierPatterns);
    }


    /**
     * NLP Classifiers
     */

    public function getClassifiersStatus(): array
    {
        return $this->getData('get_classifiers_status');
    }

    public function trainClassifiers(array $classifiers): array
    {
        return $this->postData('classifier/train', $classifiers[0]);
    }

    public function getNlpClassifierClasses(int $classifierId): array
    {
        return $this->getData('get_classifier_classes', [$classifierId]);
    }

    public function createNlpClassifierClasses(array $classifierClasses): array
    {
        return $this->postData('classifier_classes/create', $classifierClasses);
    }

    public function updateNlpClassifierClasses(array $classifierClasses): array
    {
        return $this->postData('classifier_classes/update', $classifierClasses);
    }

    public function deleteNlpClassifierClasses(array $classifierClasses): array
    {
        return $this->postData('classifier_classes/delete', $classifierClasses);
    }


    public function getNlpClassPhrases(int $classId): array
    {
        return $this->getData('get_learning_phrases', [$classId]);
    }

    public function createNlpClassPhrases(array $classPhrases): array
    {
        return $this->postData('learning_phrases/create', $classPhrases);
    }

    public function updateNlpClassPhrases(array $classPhrases): array
    {
        return $this->postData('learning_phrases/update', $classPhrases);
    }

    public function deleteNlpClassPhrases(array $classPhrases): array
    {
        return $this->postData('learning_phrases/delete', $classPhrases);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getScenarios(): array
    {
        return $this->getData('get_scenarios');
    }

    /**
     * {@inheritdoc}
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInRules(string $needle): array
    {
        return $this->getData('search/rule', [$needle]);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInTemplates(string $needle): array
    {
        return $this->getData('search/templates', [$needle]);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInPatterns(string $needle): array
    {
        return $this->getData('search/patterns', [$needle]);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInExceptionPatterns(string $needle): array
    {
        return $this->getData('search/exception_patterns', [$needle]);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInGambits(string $needle): array
    {
        return $this->getData('search/gambits', [$needle]);
    }

    /**
     * Searches for $needle in phrases.
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInPhrases(string $needle): array
    {
        return $this->getData('search/phrases', [$needle]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getDelays(): array
    {
        return $this->getData('get_delays');
    }

    /**
     * {@inheritdoc}
     *
     * @param string $ruleName Rule name
     *
     * @return array
     */
    public function getRuleByName(string $ruleName): array
    {
        return $this->getData('get_rule_by_name', [$ruleName]);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $ruleId Rule id
     *
     * @return array
     */
    public function getRuleById(int $ruleId): array
    {
        return $this->getData('get_rule_by_id', [$ruleId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $topicId Topic id
     *
     * @return array
     */
    public function getRulesByTopicId(int $topicId): array
    {
        return $this->getData('get_rules', [$topicId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $rules Array of arrays with rules data for creating.
     *
     * @return array Array of arrays with created rules data.
     */
    public function createRules(array $rules): array
    {
        return $this->postData('rules/create', $rules);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $rules Array of arrays with rules data for updating.
     *
     * @return array Array of arrays with updated rules data.
     */
    public function updateRules(array $rules): array
    {
        return $this->postData('rules/update', $rules);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $rules Array of arrays with rules data for deleting.
     *
     * @return array Array of arrays with deleted rules data.
     */
    public function deleteRules(array $rules): array
    {
        return $this->postData('rules/delete', $rules);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $topicId Topic id
     *
     * @return array
     */
    public function getGambits(int $topicId): array
    {
        return $this->getData('get_gambits', [$topicId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $topicId   Topic id
     * @param int $gambitId  Gambit id
     * @param int $subruleId Subrule id
     *
     * @return array
     */
    public function getGambitRule(int $topicId, int $gambitId, int $subruleId): array
    {
        return $this->getData('get_gambit/rules', [$topicId, $gambitId, $subruleId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $topicId  Topic id
     * @param int $gambitId Gambit id
     *
     * @return array
     */
    public function getGambitRules(int $topicId, int $gambitId): array
    {
        return $this->getData('get_gambit/rules', [$topicId, $gambitId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $gambits Array of arrays with gambits data for creating.
     *
     * @return array Array of arrays with created gambits data.
     */
    public function createGambits(array $gambits): array
    {
        return $this->postData('gambits/create', $gambits);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $gambits Array of arrays with gambits data for updating.
     *
     * @return array Array [result => array of updated gambits, affectedRows => count of updated gambits]
     */
    public function updateGambits(array $gambits): array
    {
        return $this->postData('gambits/update', $gambits);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $gambits Array of arrays with gambits data for deleting.
     *
     * @return array Array [result => array of deleted gambits, affectedRows => count of deleted gambits]
     */
    public function deleteGambits(array $gambits): array
    {
        return $this->postData('gambits/delete', $gambits);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $topicId Topic id
     * @param int $ruleId  Gambit id
     *
     * @return array
     */
    public function getRuleSubrules(int $topicId, int $ruleId): array
    {
        return $this->getData('get_rule/rules', [$topicId, $ruleId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param int $topicId   Topic id
     * @param int $ruleId    Gambit id
     * @param int $subRuleId SubRule id
     *
     * @return array
     */
    public function getRuleSubrule(int $topicId, int $ruleId, int $subRuleId): array
    {
        return $this->getData('get_rule/rules', [$topicId, $ruleId, $subRuleId]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $patterns Array of arrays with patterns data for creating.
     *
     * @return array Array of arrays with created patterns data.
     */
    public function createPatterns(array $patterns): array
    {
        return $this->postData('patterns/create', $patterns);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $patterns Array of arrays with patterns data for updating.
     *
     * @return array Array [result => array of updated patterns, affectedRows => count of updated patterns]
     */
    public function updatePatterns(array $patterns): array
    {
        return $this->postData('patterns/update', $patterns);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $patterns Array of arrays with patterns data for deleting.
     *
     * @return array Array [result => array of deleted patterns, affectedRows => count of deleted patterns]
     */
    public function deletePatterns(array $patterns): array
    {
        return $this->postData('patterns/delete', $patterns);
    }

    public function getPatternsVariables(): array
    {
        return $this->getData('get_pattern_vars');
    }

    public function createPatternVariable($params): array
    {
        return $this->postData('pattern_var/create', $params);
    }

    public function updatePatternVariable($params): array
    {
        return $this->postData('pattern_var/update', $params);
    }

    public function deletePatternVariable($params): array
    {
        return $this->postData('pattern_var/delete', $params);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $templates Array of arrays with templates data for creating.
     *
     * @return array Array of arrays with created templates data.
     */
    public function createTemplates(array $templates): array
    {
        return $this->postData('templates/create', $templates);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $templates Array of arrays with templates data for updating.
     *
     * @return array Array [result => array of updated templates, affectedRows => count of updated templates]
     */
    public function updateTemplates(array $templates): array
    {
        return $this->postData('templates/update', $templates);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $templates Array of arrays with templates data for deleting.
     *
     * @return array Array [result => array of deleted templates, affectedRows => count of deleted templates]
     */
    public function deleteTemplates(array $templates): array
    {
        return $this->postData('templates/delete', $templates);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $exceptionPatterns Array of arrays with exceptionPatterns data for creating.
     *
     * @return array Array of arrays with created exceptionPatterns data.
     */
    public function createExceptionPatterns(array $exceptionPatterns): array
    {
        return $this->postData('exception_patterns/create', $exceptionPatterns);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $exceptionPatterns Array of arrays with exceptionPatterns data for updating.
     *
     * @return array Array [result => array of updated exceptionPatterns, affectedRows => count of updated exceptionPatterns]
     */
    public function updateExceptionPatterns(array $exceptionPatterns): array
    {
        return $this->postData('exception_patterns/update', $exceptionPatterns);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $exceptionPatterns Array of arrays with exceptionPatterns data for deleting.
     *
     * @return array Array [result => array of deleted exceptionPatterns, affectedRows => count of deleted exceptionPatterns]
     */
    public function deleteExceptionPatterns(array $exceptionPatterns): array
    {
        return $this->postData('exception_patterns/delete', $exceptionPatterns);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $phrases Array of arrays with phrases data for creating.
     *
     * @return array Array of arrays with created phrases data.
     */
    public function createPhrases(array $phrases): array
    {
        return $this->postData('phrases/create', $phrases);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $phrases Array of arrays with phrases data for updating.
     *
     * @return array Array [result => array of updated phrases, affectedRows => count of updated phrases]
     */
    public function updatePhrases(array $phrases): array
    {
        return $this->postData('phrases/update', $phrases);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $phrases Array of arrays with phrases data for deleting.
     *
     * @return array Array [result => array of deleted phrases, affectedRows => count of deleted phrases]
     */
    public function deletePhrases(array $phrases): array
    {
        return $this->postData('phrases/delete', $phrases);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $groupPhrases Array of arrays with groupPhrases data for creating.
     *
     * @return array Array of arrays with created groupPhrases data.
     */
    public function createGroupPhrases(array $groupPhrases): array
    {
        return $this->postData('group_phrases/create', $groupPhrases);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $groupPhrases Array of arrays with groupPhrases data for updating.
     *
     * @return array Array [result => array of updated groupPhrases, affectedRows => count of updated groupPhrases]
     */
    public function updateGroupPhrases(array $groupPhrases): array
    {
        return $this->postData('group_phrases/update', $groupPhrases);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $groupPhrases Array of arrays with groupPhrases data for deleting.
     *
     * @return array Array [result => array of deleted groupPhrases, affectedRows => count of deleted groupPhrases]
     */
    public function deleteGroupPhrases(array $groupPhrases): array
    {
        return $this->postData('group_phrases/delete', $groupPhrases);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $scenario Array of scenario parameters for creating.
     *
     * @return array Array of created scenario data.
     */
    public function createScenario(array $scenario): array
    {
        return $this->postData('scenarios/create', $scenario);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $scenario Array of scenario parameters for deleting.
     *
     * @return array Array of deleted scenario data.
     */
    public function deleteScenario(array $scenario): array
    {
        return $this->postData('scenarios/delete', $scenario);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $scenario Array of scenario data for updating.
     *
     * @return array Array of updated scenario data.
     */
    public function updateScenario(array $scenario): array
    {
        return $this->postData('scenarios/update', $scenario);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $substitute Array of substitute parameters for creating.
     *
     * @return array Array of created substitute data.
     */
    public function createSubstitute(array $substitute): array
    {
        return $this->postData('substitutes/create', [$substitute]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $substitute Array of substitute parameters for deleting.
     *
     * @return array Array of deleted substitute data.
     */
    public function deleteSubstitute(array $substitute): array
    {
        return $this->postData('substitutes/delete', [$substitute]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $substitute Array of substitute data for updating.
     *
     * @return array Array of updated substitute data.
     */
    public function updateSubstitute(array $substitute): array
    {
        return $this->postData('substitutes/update', [$substitute]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $substitutePhrase Array of substitute phrase parameters for creating.
     *
     * @return array Array of created substitute phrase data.
     */
    public function createSubstitutePhrase(array $substitutePhrase): array
    {
        return $this->postData('substitute_phrases/create', [$substitutePhrase]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $substitutePhrase Array of substitute phrase parameters for deleting.
     *
     * @return array Array of deleted substitute phrase data.
     */
    public function deleteSubstitutePhrase(array $substitutePhrase): array
    {
        return $this->postData('substitute_phrases/delete', [$substitutePhrase]);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $substitutePhrase Array of substitute phrase data for updating.
     *
     * @return array Array of updated substitute phrase data.
     */
    public function updateSubstitutePhrase(array $substitutePhrase): array
    {
        return $this->postData('substitute_phrases/update', [$substitutePhrase]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getVariables(): array
    {
        return $this->getData('get_variables');
    }

    /**
     * {@inheritdoc}
     *
     * @param array $variable Array of variable parameters for creating.
     *
     * @return array Array of created variable data.
     */
    public function createVariable(array $variable): array
    {
        return $this->postData('variables/create', $variable);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $variable Array of variable parameters for deleting.
     *
     * @return array Array of deleted variable data.
     */
    public function deleteVariable(array $variable): array
    {
        return $this->postData('variables/delete', $variable);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $variable Array of variable data for updating.
     *
     * @return array Array of updated variable data.
     */
    public function updateVariable(array $variable): array
    {
        return $this->postData('variables/update', $variable);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $rules Array of arrays with sub rules data for creating.
     *
     * @return array Array of arrays with created sub rules data.
     */
    public function createSubRule(array $subRules): array
    {
        return $this->postData('rules/create', $subRules);
    }

    /**
     * {@inheritdoc}
     *
     * @param array $chat Array of chat data for deleting.
     *
     * @return array Array of deleted chat data.
     */
    public function deleteChat(array $chat): array
    {
        return $this->postData('clear_history', $chat);
    }


    /** ================================= Entities ================================= */

    /**
     * @param int $phraseId
     * @return array
     */
    public function getPhrasesEntities(int $phraseId)
    {
        return $this->getData('get_learning_phrase_entitys/' . $phraseId);
    }

    /**
     * @param int $classId
     * @return array
     */
    public function getClassifierClassPhrasesEntities(int $classId)
    {
        return $this->getData('get_class_learning_phrases_entitys/' . $classId);
    }

    /**
     * @param array $data
     * @return array
     */
    public function addPhraseEntity(array $data)
    {
        return $this->postData('learning_phrase_entitys/create', [$data]);
    }

    /**
     * @param array $data
     * @return array
     */
    public function updatePhraseEntity(array $data)
    {
        return $this->postData('learning_phrase_entitys/update', [$data]);
    }

    /**
     * @param int $entityId
     * @return array
     */
    public function deletePhraseEntity(int $entityId)
    {
        return $this->postData('learning_phrase_entitys/delete', [['learning_phrase_entity_id' => $entityId]]);
    }


    /** ================================= TimeSlots ================================= */

    /**
     * @return array
     */
    public function getTimeslots(): array
    {
        return $this->getData('get_timeslots');
    }

    /**
     * @param array $data
     * @return array
     */
    public function createTimeslot(array $data): array
    {
        return $this->postData('timeslot/create', $data);
    }

    /**
     * @param array $data
     * @return array
     */
    public function updateTimeslot(array $data): array
    {
        return $this->postData('timeslot/update', $data);
    }

    /**
     * @param int $timeSlotId
     * @return array
     */
    public function deleteTimeslot(int $timeSlotId): array
    {
        return $this->postData('timeslot/delete', ['timeslot_id' => $timeSlotId]);
    }
}
