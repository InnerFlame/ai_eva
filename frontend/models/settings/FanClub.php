<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class FanClub extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_fan_club';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
