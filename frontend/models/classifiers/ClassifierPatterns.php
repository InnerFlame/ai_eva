<?php
declare(strict_types=1);

namespace frontend\models\classifiers;

class ClassifierPatterns
{
    public $id;
    public $classifierId;
    public $text;
    public $test;
    public $version;
    public $active;
    public $deleted;

    /**
     * Classifiers constructor
     *
     * @param int    $id           Id of classifier pattern.
     * @param int    $classifierId Id of classifier.
     * @param string $text         Its text.
     * @param string $test         Its test string.
     * @param int    $version      Its version.
     * @param bool   $deleted      Is deleted.
     * @param bool   $active       Is active.
     *
     * @return this
     */
    public function __construct(
        int $id,
        int $classifierId,
        string $text,
        string $test,
        int $version,
        bool $deleted,
        bool $active
    ) {
        $this->id           = $id;
        $this->classifierId = $classifierId;
        $this->text         = $text;
        $this->test         = $test;
        $this->version      = $version;
        $this->deleted      = $deleted;
        $this->active       = $active;
    }

    /**
     * Getter for "id" param.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Setter for "id" param.
     *
     * @param int $id Classifier pattern id
     *
     * @return void
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Getter for "classifierId" param.
     *
     * @return int
     */
    public function getClassifierId() : int
    {
        return $this->classifierId;
    }

    /**
     * Setter for "classifierId" param.
     *
     * @param int $classifierId Classifier id
     *
     * @return void
     */
    public function setClassifierId(int $classifierId)
    {
        $this->classifierId = $classifierId;
    }

    /**
     * Getter for "text" param.
     *
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * Setter for "text" param.
     *
     * @param string $text Classifier text
     *
     * @return void
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * Getter for "test" param.
     *
     * @return string
     */
    public function getTest() : string
    {
        return $this->test;
    }

    /**
     * Setter for "test" param.
     *
     * @param string $test Classifier test string
     *
     * @return void
     */
    public function setTest(string $test)
    {
        $this->test = $test;
    }

    /**
     * Getter for "version" param.
     *
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * Setter for "version" param.
     *
     * @param int $version Classifier version
     *
     * @return void
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * Getter for "deleted" param.
     *
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * Setter for "deleted" param.
     *
     * @param bool $deleted Is deleted
     *
     * @return void
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Getter for "active" param.
     *
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->active;
    }

    /**
     * Setter for "active" param.
     *
     * @param bool $active Is active
     *
     * @return void
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }
}
