import { Component }        from '@angular/core';
import { Title }            from "@angular/platform-browser";
import { OnInit }           from '@angular/core';
import { TestPhrasesModel } from "../shared";
import { HttpService }      from "../shared/services";

@Component({
    selector    : 'testPhrases',
    templateUrl : './test.phrases.index.html',
})

export class TestPhrasesComponent implements OnInit
{
    public testPhrasesModel: TestPhrasesModel = new TestPhrasesModel();
    public hideResultsBlock: boolean          = true;
    public hideLoadingBlock: boolean          = true;
    public hideHasErrorAlert: boolean         = true;
    public enableModelsSelect: boolean        = false;
    public errorText: string                  = '';

    /**
     * Data holders for template
     */
    public entities         = [];
    public intent           = [];
    public intentRanking    = [];
    public responseData     = {};
    public projects;
    public availableProjects;

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param titleService
     */
    constructor(
        private httpService: HttpService,
        private titleService: Title
    ) {
        this.titleService.setTitle( 'Test Phrases' );
    }

    /**
     * On init script actions and events, here we bind available models
     */
    ngOnInit(): void
    {
        this.httpService.get(TestPhrasesModel.getModelsApiUrl()).then(data => {
            if (!data) {
                this.hideHasErrorAlert = false;
                this.errorText         = 'Something goes wrong wit response!';
            } else {
                this.enableModelsSelect = true;
                this.availableProjects = Object.keys(data);
                this.projects = data;
            }
        });
    }

    /**
     * Submit form handler
     */
    onSubmit(): void
    {
        this.hideHasErrorAlert = true;
        this.hideResultsBlock  = true;
        this.hideLoadingBlock  = false;

        for (let project in this.projects) {
           if (this.projects[project].indexOf(this.testPhrasesModel.model) != -1) {
               this.testPhrasesModel.project = project;
           }
        }

        this.httpService.post(TestPhrasesModel.getResultsApiUrl(), this.testPhrasesModel)
            .then(data => {
                if (!data || !Object.keys(data).length) {
                    this.hideHasErrorAlert = false;
                    this.errorText         = 'Something goes wrong wit test response or its empty... Try another phrase or model';
                } else {
                    this.hideResultsBlock = false;
                    this.bindDataHolders(data);
                }

                this.hideLoadingBlock = true;
            });
    }

    /**
     * Bind data holder properties
     *
     * @param {Object} data
     */
    bindDataHolders(data: object)
    {
        this.responseData  = data;
        this.entities      = !this.responseData['entities']       ? [] : this.responseData['entities'];
        this.intent        = !this.responseData['intent']         ? [] : [this.responseData['intent']];
        this.intentRanking = !this.responseData['intent_ranking'] ? [] : this.responseData['intent_ranking'];
    }

    /**
     * Just call {{diagnostic}} in template and watch model status in live time
     *
     * @returns {string}
     */
    get diagnostic() { return JSON.stringify(this.testPhrasesModel); }
}
