<?php

namespace frontend\models\topicsGroup;

use frontend\models\BaseNgForm;

class TopicsGroupNgForm extends BaseNgForm
{
    const TYPE_RULE_ML_CLASSIFIER = 'rule_ml_classifier';

    public function getTopicsGroup(): array
    {
        return $this->getBrungildaApi()->getTopicsGroup();
    }

    public function createTopicsGroup(array $requestData): array
    {
        return $this->getBrungildaApi()->createClassifiers([$requestData]);
    }

    public function updateTopicsGroup(array $requestData): array
    {
        return $this->getBrungildaApi()->updateClassifiers([$requestData]);
    }

    public function trainTopicsGroup(array $requestData): array
    {
        return $this->getBrungildaApi()->trainClassifiers([$requestData]);
    }

    public function getClassifiers(string $type): array
    {
        $data = $this->getBrungildaApi()->getClassifiers();
        $filteredClassifiers = $this->filterClassifiers($data, $type);

        return $filteredClassifiers;
    }

    public function getClassifiersAvailableModels(): array
    {
        $result = $this->getBrungildaApi()->getClassifiersStatus();

        return $result;
    }

    private function filterClassifiers($data, $type)
    {
        foreach ($data as $key => $classifier) {
            if (empty($classifier['type'])) {
                unset($data[$key]);
            }

            if($classifier['type'] == $type) {
                continue;
            }

            unset($data[$key]);
        }

        return array_values($data);
    }
}
