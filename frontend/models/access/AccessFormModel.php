<?php

namespace frontend\models\access;

use common\models\User;
use frontend\models\BaseNgForm;

/**
 * Class AccessFormModel
 * @package frontend\models\access
 */
class AccessFormModel extends BaseNgForm
{
    /* @var \yii\rbac\ManagerInterface */
    private $authManager;

    /**
     * Form properties
     */
    public $username, $email, $password, $role;

    /**
     * @inheritdoc
     *
     * AccessFormModel constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->authManager = \Yii::$app->authManager;
    }

    /**
     * @param $data
     * @return bool
     */
    public function loadFormData($data): bool
    {
        $preparedDataForLoad = [];

        foreach ($data as $attribute => $datum) {
            $this->{$attribute} = $datum;
            $preparedDataForLoad[$this->{$attribute}] = $datum;
        }


        return $this->load($preparedDataForLoad, '');
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['role', 'required'],
            ['role', 'string', 'max' => 255],
        ];
    }

    /**
     * @return User
     */
    public function createUser(): User
    {
        $userModel = new User([
            'username' => $this->username,
            'email'    => $this->email
        ]);
        $userModel->setPassword($this->password);
        $userModel->generateAuthKey();
        $userModel->save();

        return $userModel;
    }

    /**
     * @param int $userId
     * @param string $role
     * @return bool
     */
    public function updateUser(int $userId, string $role): bool
    {
        $this->authManager->revokeAll($userId);
        $roleObject = $this->authManager->getRole($role);

        return (bool) $this->authManager->assign($roleObject, $userId);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteUser(int $id): bool
    {
        UserItem::deleteAll(['id' => $id]);
        AuthAssignment::deleteAll(['user_id' => $id]);

        return true;
    }

    /**
     * @param string $role
     * @param int $userId
     * @return bool
     */
    public function linkAuthAssignment(string $role, int $userId): bool
    {
        $auth = \Yii::$app->authManager;
        $authorRole = $this->authManager->getRole($role);

        return (bool) $auth->assign($authorRole, $userId);
    }
}
