<?php

use yii\db\Migration;

class m161114_145842_delete_urls_fields extends Migration
{
    public function up()
    {
        $this->dropColumn('placeholders_table', 'url_text');
        $this->dropColumn('placeholders_table', 'url_text_wap');
    }

    public function down()
    {
        $this->addColumn('placeholders_table', 'url_text', $this->string());
        $this->addColumn('placeholders_table', 'url_text_wap', $this->string());
        $this->update('placeholders_table', ['url_text' => new yii\db\Expression('url')]);
        $this->update('placeholders_table', ['url_text_wap' => new yii\db\Expression('url_wap')]);
    }
}
