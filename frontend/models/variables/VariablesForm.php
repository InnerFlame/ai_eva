<?php
declare(strict_types=1);

namespace frontend\models\variables;

use frontend\models\BaseForm;
use frontend\models\DataProvider;

class VariablesForm extends BaseForm
{
    public $variables = array();

    public function __construct()
    {
    }

    public function getVariables()
    {
        $this->variables = DataProvider::getInstance()->getVariables();
    }
}
