<?php
namespace frontend\models\topics;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $topicId
 * @property integer $countTriggering
 * @property string $userId
 * @property string $createdAt
 */
class UserTopic extends ActiveRecord
{
    public static function tableName()
    {
        return 'userTopic';
    }
}
