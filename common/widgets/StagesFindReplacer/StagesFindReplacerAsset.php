<?php

namespace common\widgets\StagesFindReplacer;

use yii\web\AssetBundle;

class StagesFindReplacerAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/StagesFindReplacer/assets';

    public $js = [
        'js/stages-find-replacer.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
