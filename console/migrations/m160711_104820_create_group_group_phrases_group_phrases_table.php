<?php

use yii\db\Migration;

/**
 * Handles the creation for table `group_group_phrases_group_phrases_table`.
 */
class m160711_104820_create_group_group_phrases_group_phrases_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group_group_phrases_group_phrases_table', [
            'id' => $this->primaryKey(),
            'group_group_phrases_id' => $this->integer(11)->defaultValue(1),
            'group_phrases_id' => $this->integer(11)->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('group_group_phrases_group_phrases_table');
    }
}
