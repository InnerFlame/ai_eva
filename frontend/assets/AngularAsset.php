<?php

namespace frontend\assets;

class AngularAsset extends BasicAsset
{
    public $css = [
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        'build/assets/css/app.component.css',
        'build/assets/css/dragula.css',
    ];

    public $js = [
        'build/inline.bundle.js',
        'build/polyfills.bundle.js',
        'build/vendor.bundle.js',
        'build/main.bundle.js'
    ];
}
