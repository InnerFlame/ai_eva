<?php
declare(strict_types=1);

namespace frontend\models\usersId;

use yii\db\ActiveRecord;

//TODO: wtf duplicate is this?
class CommunicationLog extends ActiveRecord
{
    public $maxPlaceholderId;
    public $messageCount;

    public static function tableName()
    {
        return 'communicationLog';
    }
}
