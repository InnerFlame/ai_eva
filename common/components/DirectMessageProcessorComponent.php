<?php
namespace common\components;

use Predis;

class DirectMessageProcessorComponent extends AbstractMessageProcessorComponent
{
    public function addMessageToProcess($dataJson)
    {
        \Yii::$app->amqp->publishEvent('messages_to_process', $dataJson);
    }

    public function processMessages()
    {
        \Yii::$app->amqp->consumeEvents('messages_to_process', [$this, 'processMessagesHandler']);
    }

    public function processMessagesHandler($amqpMessage)
    {
        \Yii::$app->amqp->ackMessage($amqpMessage);
        $message = \Yii::$app->amqp->getMessageData($amqpMessage);

        if (empty($message)) {
            return false;
        }

        $dataRequest = json_decode($message, true);

        $data = $dataRequest['params'][0];

        #Before AK fix
        if (empty($data['profiles'][$data['from']]['multi_profile_id'])) {
            return false;
        }

        $recipientId    = $data['to'];
        $senderMultiId  = $data['multi_profile_id'];

        //Get processed eva ansver
        $evaResponse = [];
        $messages = $this->getAnswerData($data, $evaResponse);

        if (!empty($messages['error'])) {
            return false;
        }

        $this->pushMessagesToSendingQueue($senderMultiId, $recipientId, $dataRequest, $messages, $evaResponse);

        return true;
    }

    private function pushMessagesToSendingQueue($senderId, $recipientId, $dataRequest, $messages, $evaResponse)
    {
        $lastDialogueTimeKey    = 'time_' . $senderId . '-' . $recipientId;
        $lastDialogueTime       = \Yii::$app->cache->get($lastDialogueTimeKey);
        if (!empty($lastDialogueTime) && ($lastDialogueTime > time())) {
            $startTimeOffset = $lastDialogueTime;
        } else {
            $startTimeOffset = time();
        }

        end($messages);
        $lastMessageKey = key($messages);

        //Write data and corresponding time to send to database
        $dataRequest['method'] = 'onPerform';
        foreach ($messages as $key => $message) {
            $dataRequest['params'][0]['text'] = $message['message'];
            //Send exit status only in last short message
            if (isset($evaResponse['status']) && (($key == $lastMessageKey) || ($evaResponse['status'] != 1))) {
                $dataRequest['params'][0]['status'] = $evaResponse['status'];
            }
            //Send exit true only in last short message
            if (isset($evaResponse['exit']) && (($key == $lastMessageKey) || ($evaResponse['exit'] != true))) {
                $dataRequest['params'][0]['exit'] = $evaResponse['exit'];
            }

            $delay = $message['delay'];
            $sendingTime = $startTimeOffset + $delay;
            \Yii::$app->redis->zadd('messages',  $sendingTime, json_encode($dataRequest));
            \Yii::$app->cache->set($lastDialogueTimeKey, $sendingTime, $delay + ($startTimeOffset - time()) + 300);
        }
    }

    public function actionPushReadyMessagesToQueue()
    {
        $runner = new IterativeRunner([$this, 'pushReadyMessagesToQueue'], [], 1, 0);
        $runner->run();
    }

    public function pushReadyMessagesToQueue()
    {
        $currentTime = time();
        $messages = \Yii::$app->redis->zrangebyscore('messages', 0, $currentTime);

        foreach ($messages as $message) {
            \Yii::$app->redis->zrem('messages', $message);
            \Yii::$app->amqp->publishEvent('messages_to_send', $message);
        }
    }

    public function sendMessages()
    {
        \Yii::$app->amqp->consumeEvents('messages_to_send', [$this, 'sendMessagesHandler']);
    }

    public function sendMessagesHandler($amqpMessage)
    {
        \Yii::$app->amqp->ackMessage($amqpMessage);
        $message = \Yii::$app->amqp->getMessageData($amqpMessage);

        if (empty($message)) {
            return false;
        }

        $messageData = json_decode($message, true);

        $url = 'http://b2b.bratuha.com/cbrpc.php';
        if (!empty($messageData['params'][0]['dev']) && $messageData['params'][0]['dev'] == 'rel') {
            $url = 'http://rel.bsys.redut.net/cbrpc.php';
        }

        \Yii::$app->webClient->post(
            $url,
            $message,
            'application/json'
        );

        return true;
    }
}