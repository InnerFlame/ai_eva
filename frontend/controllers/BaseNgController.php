<?php

namespace frontend\controllers;

use frontend\models\BaseNgForm;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\Module;
use yii\base\ViewContextInterface;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class BaseNgController
 * @package frontend\controllers
 */
abstract class BaseNgController extends Controller implements ViewContextInterface
{
    const BOOTSTRAP_ACTION = 'index';

    /* @var  $paginationPageSize int */
    protected $paginationPageSize = 25;

    /* @var $requestData array */
    protected $requestData;

    /* @var $formModel (instance of BaseNgForm) */
    protected $formModel = null;

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     *
     * BaseNgController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     * @return void
     */
    public function __construct($id, Module $module, array $config = [])
    {
        $isLocked = \Yii::$app->cache->get('adminLock');
        if ($isLocked) {
            die('Admin panel is on maintenance. It will be released in hour. Try to ask releaser for details');
        }

        parent::__construct($id, $module, $config);
    }

    /**
     * Realize this method for specify additional configs and type of returnable model
     * set to NULL if you don't need model provider in controller
     *
     * @return mixed
     */
    abstract protected function getFormModel();

    /**
     * @inheritdoc
     *
     * @return bool|string
     */
    public function getViewPath(): string
    {
        return \Yii::getAlias('@frontend/views/ng-views');
    }

    /**
     * @inheritdoc
     *
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action): bool
    {
        if ($action->id == self::BOOTSTRAP_ACTION) {
            return parent::beforeAction($action);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (\Yii::$app->request->isPost) {
            $this->setRequestData();

            if (empty($this->requestData)) {
                throw new InvalidParamException();
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * @param string $var
     * @return array|int|string|boolean|mixed
     * @throws \Exception
     */
    protected function getRequestData(string $var = '')
    {
        if (!empty($var)) {
            if (!isset($this->requestData[$var])){
                throw new \Exception('Request data does not have ' . $var . ' param');
            }

            return $this->requestData[$var];
        }

        return $this->requestData;
    }

    /**
     * @return void
     */
    protected function setRequestData()
    {
        $requestBody       = file_get_contents('php://input');
        $this->requestData = json_decode($requestBody, true);
    }

    /**
     * Calculates and returns offset for pagination
     *
     * @param int $page
     * @return int
     */
    protected function getPagerOffsetFromPage(int $page): int
    {
        return ($page !== 1) ? ($this->paginationPageSize * ($page - 1)) : 0;
    }

    /**
     * Get form model for all data transfer from api
     * @param $model (must be instance of BaseNgFom)
     * @throws InvalidConfigException
     * @return void
     */
    protected function setFormModel($model)
    {
        if (!$model instanceof BaseNgForm) {
            throw new InvalidConfigException('Form model must extends BaseNgForm class');
        }

        $this->formModel = $model;
    }
}
