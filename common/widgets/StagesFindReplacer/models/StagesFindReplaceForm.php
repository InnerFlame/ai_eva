<?php

namespace common\widgets\StagesFindReplacer\models;

use Yii;
use yii\base\Model;

/**
 * StagesFindReplaceForm form.
 */
class StagesFindReplaceForm extends Model
{
    public $find;
    public $replace;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['find'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'find' => 'Find text',
            'replace' => 'Replace text',
        ];
    }
}
