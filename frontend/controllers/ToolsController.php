<?php
declare(strict_types=1);

namespace frontend\controllers;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class ToolsController extends BaseController
{
    public function actionCleanAssets()
    {
        $dir         = \Yii::$app->getBasePath() . '/web/assets';
        $leave_files = ['.gitignore'];

        foreach (glob("$dir/*") as $file)
        {
            if(!in_array(basename($file), $leave_files) ) {
                $this->removeDirRecurcive($file);
            }
        }

        return $this->goBack();
    }

    private function removeDirRecurcive($dir){
        $it    = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($files as $file)
        {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);
    }
}
