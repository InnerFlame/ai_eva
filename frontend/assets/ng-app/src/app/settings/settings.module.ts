import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { SharedModule }                  from "../shared";
import { SettingsComponent }             from "./settings.component";
import { SettingsUpdateComponent }       from "./settings.update.component";
import { DragulaModule }                 from 'ng2-dragula';
import { MultiselectDropdownModule }     from 'angular-2-dropdown-multiselect';

const phrasesRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'settings-ng',
        component: SettingsComponent
    },
    {
        path: 'settings-ng/update/:id',
        component: SettingsUpdateComponent
    }
]);

@NgModule({
    imports: [
        phrasesRouting,
        SharedModule,
        DragulaModule,
        MultiselectDropdownModule
    ],
    declarations: [
        SettingsComponent,
        SettingsUpdateComponent
    ]
})

export class SettingsModule {}