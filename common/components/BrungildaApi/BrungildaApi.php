<?php

declare(strict_types=1);

namespace common\components\BrungildaApi;

interface BrungildaApi
{
    /**
     * Tests sending $text from $fromUserId to $toUserId and returns result.
     *
     * @param string $fromUserId Sender user id.
     * @param string $toUserId   Recipient user id.
     * @param string $text       Message text.
     *
     * @return array Result of trying of sending chat message.
     */
    public function chat(string $fromUserId, string $toUserId, string $text, int $scenarioId): array;

    /**
     * Returns list of topics with their content.
     *
     * @return array
     */
    public function getTopics(): array;

    public function getRestrictions() : array;

    public function createRestrictions(array $restrictions): array;

    public function deleteRestrictions(array $restrictions): array;

    /**
     * Creates new topics and returns their data.
     *
     * @param array $topics Array of arrays with topics data for creating.
     *
     * @return array Array of arrays with created topics data.
     */
    public function createTopics(array $topics): array;

    /**
     * Deletes topics and returns their data.
     *
     * @param array $topics Array of arrays with topics data for deleting.
     *
     * @return array Array [result => array of deleted topics, affectedRows => count of deleted topics]
     */
    public function deleteTopics(array $topics): array;

    /**
     * Updates topics and returns their data.
     *
     * @param array $topics Array of arrays with topics data for updating.
     *
     * @return array Array [result => array of updated topics, affectedRows => count of updated topics]
     */
    public function updateTopics(array $topics): array;

    /**
     * Returns list of substitutes with their content.
     *
     * @return array
     */
    public function getSubstitutes(): array;

    /**
     * Returns list of substitute phrases with their content.
     *
     * @return array
     */
    public function getSubstitutePhrases(int $substituteId): array;

    /**
     * Returns list of group phrases with their content.
     *
     * @return array
     */
    public function getGroupPhrases(): array;

    /**
     * Returns list of group phrases with their content.
     *
     * @param int $groupPhraseId Group phrase id
     *
     * @return array
     */
    public function getPhrases(int $groupPhraseId): array;

    /**
     * Returns list of classifiers with their content.
     *
     * @return array
     */
    public function getClassifiers(): array;

    /**
     * Returns list of classifier patterns with their content.
     *
     * @param int $classifierId Classifier id
     *
     * @return array
     */
    public function getClassifierPatterns(int $classifierId): array;

    /**
     * Creates new classifiers and returns their data.
     *
     * @param array $classifiers Array of arrays with classifiers data for creating.
     *
     * @return array Array of arrays with created classifiers data.
     */
    public function createClassifiers(array $classifiers): array;

    /**
     * Updates classifiers and returns their data.
     *
     * @param array $classifiers Array of arrays with classifiers data for updating.
     *
     * @return array Array [result => array of updated classifiers, affectedRows => count of updated classifiers]
     */
    public function updateClassifiers(array $classifiers): array;

    /**
     * Deletes classifiers and returns their data.
     *
     * @param array $classifiers Array of arrays with classifiers data for deleting.
     *
     * @return array Array [result => array of deleted classifiers, affectedRows => count of deleted classifiers]
     */
    public function deleteClassifiers(array $classifiers): array;

    /**
     * Creates new classifier patterns and returns their data.
     *
     * @param array $classifierPatterns Array of arrays with classifier patterns data for creating.
     *
     * @return array Array of arrays with created classifier patterns data.
     */
    public function createClassifierPatterns(array $classifierPatterns): array;

    /**
     * Updates classifier patterns and returns their data.
     *
     * @param array $classifierPatterns Array of arrays with classifier patterns data for updating.
     *
     * @return array Array [result => array of updated classifier patterns, affectedRows => count of updated classifier patterns]
     */
    public function updateClassifierPatterns(array $classifierPatterns): array;

    /**
     * Deletes classifier patterns and returns their data.
     *
     * @param array $classifierPatterns Array of arrays with classifier patterns data for deleting.
     *
     * @return array Array [result => array of deleted classifier patterns, affectedRows => count of deleted classifier patterns]
     */
    public function deleteClassifierPatterns(array $classifierPatterns): array;

    /**
     * NLP Classifiers
     */

    public function getClassifiersStatus(): array;
    public function trainClassifiers(array $classifiers): array;

    public function getNlpClassifierClasses(int $classifierId): array;
    public function createNlpClassifierClasses(array $classifierClasses): array;
    public function updateNlpClassifierClasses(array $classifierClasses): array;
    public function deleteNlpClassifierClasses(array $classifierClasses): array;

    public function getNlpClassPhrases(int $classId): array;
    public function createNlpClassPhrases(array $classPhrases): array;
    public function updateNlpClassPhrases(array $classPhrases): array;
    public function deleteNlpClassPhrases(array $classPhrases): array;

    /**
     * Returns list of scenarios with their content.
     *
     * @return array
     */
    public function getScenarios(): array;

    /**
     * Searches for $needle in rules.
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInRules(string $needle): array;

    /**
     * Searches for $needle in templates.
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInTemplates(string $needle): array;

    /**
     * Searches for $needle in patterns.
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInPatterns(string $needle): array;

    /**
     * Searches for $needle in exception patterns.
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInExceptionPatterns(string $needle): array;

    /**
     * Searches for $needle in gambits.
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInGambits(string $needle): array;

    /**
     * Searches for $needle in phrases.
     *
     * @param string $needle Text that we search
     *
     * @return array
     */
    public function searchInPhrases(string $needle): array;

    /**
     * Returns list of delays with their content.
     *
     * @return array
     */
    public function getDelays(): array;

    /**
     * Returns list of timeslots with their content.
     *
     * @return array
     */
    public function getTimeslots(): array;

    /**
     * Returns rule by its name.
     *
     * @param string $ruleName Rule name
     *
     * @return array
     */
    public function getRuleByName(string $ruleName): array;

    /**
     * Returns rule by its id.
     *
     * @param int $ruleId Rule id
     *
     * @return array
     */
    public function getRuleById(int $ruleId): array;

    /**
     * Returns rules by topic id.
     *
     * @param int $topicId Topic id
     *
     * @return array
     */
    public function getRulesByTopicId(int $topicId): array;

    /**
     * Creates new rules and returns their data.
     *
     * @param array $rules Array of arrays with rules data for creating.
     *
     * @return array Array of arrays with created rules data.
     */
    public function createRules(array $rules): array;

    /**
     * Updates new rules and returns their data.
     *
     * @param array $rules Array of arrays with rules data for updating.
     *
     * @return array Array of arrays with updated rules data.
     */
    public function updateRules(array $rules): array;

    /**
     * Deletes rules and returns their data.
     *
     * @param array $rules Array of arrays with rules data for deleting.
     *
     * @return array Array of arrays with deleted rules data.
     */
    public function deleteRules(array $rules): array;

    /**
     * Returns gambits data by topic id.
     *
     * @param int $topicId Topic id
     *
     * @return array
     */
    public function getGambits(int $topicId): array;

    /**
     * Returns gambit rule.
     *
     * @param int $topicId   Topic id
     * @param int $gambitId  Gambit id
     * @param int $subruleId Subrule id
     *
     * @return array
     */
    public function getGambitRule(int $topicId, int $gambitId, int $subruleId): array;

    /**
     * Returns gambit rules.
     *
     * @param int $topicId  Topic id
     * @param int $gambitId Gambit id
     *
     * @return array
     */
    public function getGambitRules(int $topicId, int $gambitId): array;

    /**
     * Creates new gambits and returns their data.
     *
     * @param array $gambits Array of arrays with gambits data for creating.
     *
     * @return array Array of arrays with created gambits data.
     */
    public function createGambits(array $gambits): array;

    /**
     * Updates gambits and returns their data.
     *
     * @param array $gambits Array of arrays with gambits data for updating.
     *
     * @return array Array [result => array of updated gambits, affectedRows => count of updated gambits]
     */
    public function updateGambits(array $gambits): array;

    /**
     * Deletes gambits and returns their data.
     *
     * @param array $gambits Array of arrays with gambits data for deleting.
     *
     * @return array Array [result => array of deleted gambits, affectedRows => count of deleted gambits]
     */
    public function deleteGambits(array $gambits): array;

    /**
     * Returns subrules of rule.
     *
     * @param int $topicId Topic id
     * @param int $ruleId  Gambit id
     *
     * @return array
     */
    public function getRuleSubrules(int $topicId, int $ruleId): array;

    /**
     * Returns subrule of rule.
     *
     * @param int $topicId   Topic id
     * @param int $ruleId    Gambit id
     * @param int $subRuleId SubRule id
     *
     * @return array
     */
    public function getRuleSubrule(int $topicId, int $ruleId, int $subRuleId): array;

    /**
     * Creates new patterns and returns their data.
     *
     * @param array $patterns Array of arrays with patterns data for creating.
     *
     * @return array Array of arrays with created patterns data.
     */
    public function createPatterns(array $patterns): array;

    /**
     * Updates patterns and returns their data.
     *
     * @param array $patterns Array of arrays with patterns data for updating.
     *
     * @return array Array [result => array of updated patterns, affectedRows => count of updated patterns]
     */
    public function updatePatterns(array $patterns): array;

    /**
     * Deletes patterns and returns their data.
     *
     * @param array $patterns Array of arrays with patterns data for deleting.
     *
     * @return array Array [result => array of deleted patterns, affectedRows => count of deleted patterns]
     */
    public function deletePatterns(array $patterns): array;

    public function createPatternVariable($params): array;

    public function updatePatternVariable($params): array;

    public function deletePatternVariable($params): array;

    public function getPatternsVariables(): array;

    /**
     * Creates new templates and returns their data.
     *
     * @param array $templates Array of arrays with templates data for creating.
     *
     * @return array Array of arrays with created templates data.
     */
    public function createTemplates(array $templates): array;

    /**
     * Updates templates and returns their data.
     *
     * @param array $templates Array of arrays with templates data for updating.
     *
     * @return array Array [result => array of updated templates, affectedRows => count of updated templates]
     */
    public function updateTemplates(array $templates): array;

    /**
     * Deletes templates and returns their data.
     *
     * @param array $templates Array of arrays with templates data for deleting.
     *
     * @return array Array [result => array of deleted templates, affectedRows => count of deleted templates]
     */
    public function deleteTemplates(array $templates): array;

    /**
     * Creates new exceptionPatterns and returns their data.
     *
     * @param array $exceptionPatterns Array of arrays with exceptionPatterns data for creating.
     *
     * @return array Array of arrays with created exceptionPatterns data.
     */
    public function createExceptionPatterns(array $exceptionPatterns): array;

    /**
     * Updates exceptionPatterns and returns their data.
     *
     * @param array $exceptionPatterns Array of arrays with exceptionPatterns data for updating.
     *
     * @return array Array [result => array of updated exceptionPatterns, affectedRows => count of updated exceptionPatterns]
     */
    public function updateExceptionPatterns(array $exceptionPatterns): array;

    /**
     * Deletes exceptionPatterns and returns their data.
     *
     * @param array $exceptionPatterns Array of arrays with exceptionPatterns data for deleting.
     *
     * @return array Array [result => array of deleted exceptionPatterns, affectedRows => count of deleted exceptionPatterns]
     */
    public function deleteExceptionPatterns(array $exceptionPatterns): array;

    /**
     * Creates new phrases and returns their data.
     *
     * @param array $phrases Array of arrays with phrases data for creating.
     *
     * @return array Array of arrays with created phrases data.
     */
    public function createPhrases(array $phrases): array;

    /**
     * Updates phrases and returns their data.
     *
     * @param array $phrases Array of arrays with phrases data for updating.
     *
     * @return array Array [result => array of updated phrases, affectedRows => count of updated phrases]
     */
    public function updatePhrases(array $phrases): array;

    /**
     * Deletes phrases and returns their data.
     *
     * @param array $phrases Array of arrays with phrases data for deleting.
     *
     * @return array Array [result => array of deleted phrases, affectedRows => count of deleted phrases]
     */
    public function deletePhrases(array $phrases): array;

    /**
     * Creates new groupPhrases and returns their data.
     *
     * @param array $groupPhrases Array of arrays with groupPhrases data for creating.
     *
     * @return array Array of arrays with created groupPhrases data.
     */
    public function createGroupPhrases(array $groupPhrases): array;

    /**
     * Updates groupPhrases and returns their data.
     *
     * @param array $groupPhrases Array of arrays with groupPhrases data for updating.
     *
     * @return array Array [result => array of updated groupPhrases, affectedRows => count of updated groupPhrases]
     */
    public function updateGroupPhrases(array $groupPhrases): array;

    /**
     * Deletes groupPhrases and returns their data.
     *
     * @param array $groupPhrases Array of arrays with groupPhrases data for deleting.
     *
     * @return array Array [result => array of deleted groupPhrases, affectedRows => count of deleted groupPhrases]
     */
    public function deleteGroupPhrases(array $groupPhrases): array;

    /**
     * Creates new scenario and returns their data.
     *
     * @param array $scenario Array of scenario data for creating.
     *
     * @return array Array of created scenario data.
     */
    public function createScenario(array $scenario): array;

    /**
     * Deletes scenario and returns their data.
     *
     * @param array $scenario Array of scenario data for deleting.
     *
     * @return array Array of deleted scenario data.
     */
    public function deleteScenario(array $scenario): array;

    /**
     * Updates scenario and returns their data.
     *
     * @param array $scenario Array of scenario data for updating.
     *
     * @return array Array of updated scenario data.
     */
    public function updateScenario(array $scenario): array;

    /**
     * {@inheritdoc}
     *
     * @param array $substitute Array of substitute parameters for creating.
     *
     * @return array Array of created substitute data.
     */
    public function createSubstitute(array $substitute): array;

    /**
     * {@inheritdoc}
     *
     * @param array $substitute Array of substitute parameters for deleting.
     *
     * @return array Array of deleted substitute data.
     */
    public function deleteSubstitute(array $substitute): array;

    /**
     * {@inheritdoc}
     *
     * @param array $substitute Array of substitute data for updating.
     *
     * @return array Array of updated substitute data.
     */
    public function updateSubstitute(array $substitute): array;

    /**
     * {@inheritdoc}
     *
     * @param array $substitutePhrase Array of substitute phrase parameters for creating.
     *
     * @return array Array of created substitute phrase data.
     */
    public function createSubstitutePhrase(array $substitutePhrase): array;

    /**
     * {@inheritdoc}
     *
     * @param array $substitutePhrase Array of substitute phrase parameters for deleting.
     *
     * @return array Array of deleted substitute phrase data.
     */
    public function deleteSubstitutePhrase(array $substitutePhrase): array;

    /**
     * {@inheritdoc}
     *
     * @param array $substitutePhrase Array of substitute phrase data for updating.
     *
     * @return array Array of updated substitute phrase data.
     */
    public function updateSubstitutePhrase(array $substitutePhrase): array;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getVariables(): array;

    /**
     * {@inheritdoc}
     *
     * @param array $variable Array of variable parameters for creating.
     *
     * @return array Array of created variable data.
     */
    public function createVariable(array $variable): array;

    /**
     * {@inheritdoc}
     *
     * @param array $variable Array of variable parameters for deleting.
     *
     * @return array Array of deleted variable data.
     */
    public function deleteVariable(array $variable): array;

    /**
     * {@inheritdoc}
     *
     * @param array $variable Array of variable data for updating.
     *
     * @return array Array of updated variable data.
     */
    public function updateVariable(array $variable): array;

    /**
     * {@inheritdoc}
     *
     * @param array $rules Array of arrays with sub rules data for creating.
     *
     * @return array Array of arrays with created sub rules data.
     */
    public function createSubRule(array $subRules): array;

    /**
     * {@inheritdoc}
     *
     * @param array $chat Array of chat data for deleting.
     *
     * @return array Array of deleted chat data.
     */
    public function deleteChat(array $chat): array;
}
