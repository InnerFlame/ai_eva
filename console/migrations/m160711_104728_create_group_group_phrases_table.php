<?php

use yii\db\Migration;

/**
 * Handles the creation for table `group_group_phrases_table`.
 */
class m160711_104728_create_group_group_phrases_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group_group_phrases_table', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('group_group_phrases_table');
    }
}
