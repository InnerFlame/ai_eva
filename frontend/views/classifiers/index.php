<?php
use yii\bootstrap\Html;
use common\widgets\BindableForm\BindableForm;
use common\widgets\RadioButton\RadioButton;
use common\widgets\Modal\Modal;
use frontend\assets\ClassifiersAsset;

ClassifiersAsset::register($this);

//Modal confirm delete classifiers
echo Modal::widget([
    'id' => 'confirmDeleteClassifiersModal',
    'title' => 'Delete classifiers',
    'hiddenInputIds' => ['deleteClassifierId',],
    'nameText' => 'classifiers',
    'nameId' => 'deleteClassifiersName',
    'buttonId' => 'confirmDeleteClassifiersButton'
]);

//Modal confirm delete classifier patterns
echo Modal::widget([
    'id' => 'confirmDeleteClassifierPatternsModal',
    'title' => 'Delete classifier patterns',
    'nameText' => 'classifier patterns',
    'nameId' => 'deleteClassifierPatternsNames',
    'buttonId' => 'confirmDeleteClassifierPatternsButton'
]);
?>
<!-- CLASSIFIERS CONTAINER -->
<div class="col-md-6" id="classifiersContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a href="javascript:void(0);">Classifiers</a></li>
    </ul>
    <?php
    echo BindableForm::widget([
        'id' => 'classifiersBindableForm',
        'hiddens' => [
            ['name' => 'classifierId'],
        ],
        'lines' => [
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                    'content' => Html::textarea('itemText', '', [
                        'class' => 'form-control',
                        'rows'  => '5',
                        'id'    => 'classifiersText',
                        'style' => 'resize:vertical;'
                    ])
                ],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-2'], 'content' => Html::button('Delete', [
                    'class' => 'btn btn-danger btn-block',
                    'type'  => 'button',
                    'id'    => 'deleteClassifiersButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-3'], 'content' => Html::button('Add', [
                    'class' => 'btn btn-success btn-block',
                    'type'  => 'button',
                    'id'    => 'addNewClassifiersButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-7'], 'content' => Html::button('Update', [
                    'class' => 'btn btn-primary btn-block',
                    'type'  => 'button',
                    'id'    => 'updateClassifiersButton',
                ])],
            ]],
        ],
    ]);
    ?>
    <div id="classifiers"></div>
</div>

<!-- CLASSIFIER PATTERNS CONTAINER -->
<div id="regularExpressionCheckModal" class="modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <strong></strong>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6" id="classifierPatternsContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a href="javascript:void(0);">Classifier patterns</a></li>
    </ul>
    <?php
    echo BindableForm::widget([
        'id' => 'classifierPatternsBindableForm',
        'hiddens' => [
            ['name' => 'classifierPatternId'],
        ],
        'lines' => [
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                    'content' => Html::textarea('testText', '', [
                        'class' => 'form-control',
                        'rows'  => '3',
                        'id'    => 'classifierPatternTest',
                        'style' => 'resize:vertical;'
                    ])
                ],
            ]],
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                    'content' => Html::textarea('itemText', '', [
                        'class' => 'form-control',
                        'rows'  => '3',
                        'id'    => 'classifierPatternText',
                        'style' => 'resize:vertical;'
                    ])
                ],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                    'id'             => 'updateClassifierPatternActiveTestRadioButton',
                    'activeText'     => 'Active',
                    'text'           => 'Test',
                    'glyphiconClass' => 'glyphicon-signal',
                    'activeState'    => false,
                    'name'           => 'activeClassifierPatternIcon',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Test pattern', [
                    'class' => 'btn btn-info btn-block',
                    'type'  => 'button',
                    'id'    => 'checkRegularExpressionButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Check all', [
                    'class' => 'btn btn-default btn-block',
                    'type' => 'button',
                    'id' => 'checkAllClassifierPatternsButton',
                ])],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Add', [
                    'class' => 'btn btn-success btn-block',
                    'type'  => 'button',
                    'id'    => 'addNewClassifierPatternButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Update', [
                    'class' => 'btn btn-primary btn-block',
                    'type'  => 'button',
                    'id'    => 'updateClassifierPatternButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                    'class' => 'btn btn-danger btn-block',
                    'type'  => 'button',
                    'id'    => 'deleteClassifierPatternButton',
                ])],
            ]],
        ],
    ]);
    ?>
    <div id="classifierPatterns"></div>
</div>
