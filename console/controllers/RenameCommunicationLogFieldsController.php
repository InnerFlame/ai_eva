<?php
namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;

// This class can create new table, copy to new table rows earlier than two weeks, delete old table and rename new table
class RenameCommunicationLogFieldsController extends Controller
{
    // This function call from terminal 'php yii rename-communication-log-fields/copy'
    public function actionCopy()
    {
        echo 'Create communicationLogTemp table...' . PHP_EOL;
        \Yii::$app->db
            ->createCommand(
                'CREATE TABLE `communicationLogTemp` (' .
                '`id` int(11) NOT NULL AUTO_INCREMENT,' .
                '`profileType` varchar(16) DEFAULT NULL,' .
                '`country` varchar(3) DEFAULT NULL,' .
                '`platform` varchar(32) DEFAULT NULL,' .
                '`scenarioId` int(11) DEFAULT NULL,' .
                '`project` varchar(16) DEFAULT NULL,' .
                '`language` varchar(3) DEFAULT NULL,' .
                '`trafficSource` varchar(32) DEFAULT NULL,' .
                '`placeholderId` int(11) DEFAULT \'0\',' .
                '`siteHash` varchar(32) DEFAULT NULL,' .
                '`status` varchar(4) DEFAULT NULL,' .
                '`createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,' .
                '`splitId` int(11) DEFAULT NULL,' .
                '`userId` varchar(32) DEFAULT NULL,' .
                '`modelId` varchar(32) DEFAULT NULL,' .
                '`isUser` tinyint(1) DEFAULT NULL,' .
                '`message` text,' .
                '`hasRule` tinyint(1) DEFAULT \'0\',' .
                '`hasSwitching` tinyint(1) DEFAULT \'0\',' .
                '`screenname` varchar(64) DEFAULT \'\',' .
                '`predispatched` tinyint(1) DEFAULT \'0\',' .
                'PRIMARY KEY (`id`),' .
                'KEY `country-siteHash` (`country`,`siteHash`),' .
                'KEY `createdAt` (`createdAt`),' .
                'KEY `idFrom` (`userId`),' .
                'KEY `idTo` (`modelId`),' .
                'KEY `idFromidTo` (`userId`,`modelId`)' .
                ') ENGINE=InnoDB DEFAULT CHARSET=utf8'
            )->execute();

        $now            = strtotime('now');
        $currentTime    = strtotime('now' . ' -10 day');
        $step           = 3600;

        echo 'Today date:            ' . date('Y-m-d H:i:s', $now) . PHP_EOL;
        echo 'Two weeks before date: ' . date('Y-m-d H:i:s', $currentTime) . PHP_EOL;

        $progressSize   = $now - $currentTime;
        $progressStart  = $currentTime;
        Console::startProgress(0, $progressSize, 'Copy rows: ');
        for (;$currentTime <= $now; $currentTime += $step) {
            \Yii::$app->db
                ->createCommand(
                    'INSERT INTO `communicationLogTemp` (' .
                    '`profileType`,`country`,`platform`,`scenarioId`,`project`,`language`,`trafficSource`,' .
                    '`placeholderId`,`siteHash`,`status`,`createdAt`,`splitId`,`userId`,`modelId`,`isUser`,' .
                    '`message`,`hasRule`,`hasSwitching`,`screenname`,`predispatched`) ' .
                    'SELECT ' .
                    '`profileType`,`country`,`platform`,`scenarioId`,`project`,`language`,`trafficSource`,' .
                    '`placeholderId`,`siteHash`,`status`,`createdAt`,`splitId`,`idFrom`,`idTo`,`isUser`,' .
                    '`message`,`hasRule`,`hasSwitching`,`screenname`,`predispatched` ' .
                    'FROM `communicationLog` WHERE ' .
                    '`communicationLog`.`createdAt` >= \'' . date('Y-m-d H:i:s', $currentTime) . '\' AND ' .
                    '`communicationLog`.`createdAt` < \'' . date('Y-m-d H:i:s', $currentTime + $step) . '\''
                )->execute();
            Console::updateProgress($currentTime - $progressStart, $progressSize);
            sleep(2);
        }
        Console::endProgress();
        echo 'Copy done.' . PHP_EOL;
    }

    // This function call (after copy function) from terminal 'php yii rename-communication-log-fields/switch'
    public function actionSwitch()
    {
        echo 'Switch to new table...' . PHP_EOL;

        \Yii::$app->db->transaction(function($db) {
            echo 'Rename communicationLog table to communicationLogReadyForDelete table...' . PHP_EOL;
            $db->createCommand('RENAME TABLE `communicationLog` TO `communicationLogReadyForDelete`')->execute();
            echo 'Rename communicationLogTemp table to communicationLog table...' . PHP_EOL;
            $db->createCommand('RENAME TABLE `communicationLogTemp` TO `communicationLog`')->execute();
            echo 'Delete communicationLogReadyForDelete table...' . PHP_EOL;
            $db->createCommand('DROP TABLE `communicationLogReadyForDelete`')->execute();
        }, \yii\db\Transaction::SERIALIZABLE);

        echo 'Done.' . PHP_EOL;
    }
}
