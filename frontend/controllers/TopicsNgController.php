<?php

namespace frontend\controllers;

use frontend\models\topics\TopicsNgForm;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class TopicsNgController
 * @package frontend\controllers
 */
class TopicsNgController extends BaseNgController
{
    /**
     * @inheritdoc
     *
     * TopicsNgController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new TopicsNgForm());
    }

    /**
     * @inheritdoc
     *
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('topics')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * Main bootstrap page
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionGetTopics(): array
    {
        return $this->getFormModel()->getTopics();
    }

    /**
     * @param $topicId
     * @return array
     */
    public function actionGetTopicRules($topicId)
    {
        return $this->getFormModel()->getTopicRulesByTopicId((int) $topicId);
    }

    /**
     * @return array
     */
    public function actionGetSelectsData()
    {
        return $this->getFormModel()->getTopicsAvailableDataForSelects();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreateTopic(): array
    {
        return $this->getFormModel()->createTopic($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdateTopic(): array
    {
        return $this->getFormModel()->updateTopic($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeleteTopic(): array
    {
        return $this->getFormModel()->deleteTopic((int) $this->getRequestData('topic_id'));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionToggleTopicsOpt()
    {
        return $this->getFormModel()->updateTopicOpt($this->getRequestData());
    }


    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreateRule(): array
    {
        return $this->getFormModel()->createTopicRule($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdateRule(): array
    {
        return $this->getFormModel()->updateRule($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeleteRule(): array
    {
        return $this->getFormModel()->deleteRule($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionToggleTopicRuleOpt(): array
    {
        return $this->getFormModel()->updateRuleOpt($this->getRequestData());
    }

    /**
     * @param $ruleId
     * @return array
     */
    public function actionGetRuleFormsData($ruleId): array
    {
        return $this->getFormModel()->getRuleData((int) $ruleId);
    }

    /**
     * @param $subRuleId
     * @return array
     */
    public function actionGetSubRuleData($subRuleId): array
    {
        return $this->getFormModel()->getSubRuleData((int) $subRuleId);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreatePattern(): array
    {
        return $this->getFormModel()->createPattern($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdatePattern(): array
    {
        return $this->getFormModel()->updatePattern($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdatePatterns(): array
    {
        return $this->getFormModel()->updatePatterns($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdatePatternsStatus(): array
    {
        return $this->getFormModel()->updatePatternsStatus($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeletePattern(): array
    {
        return $this->getFormModel()->deletePattern((int) $this->getRequestData('pattern_id'));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeletePatterns(): array
    {
        return $this->getFormModel()->deletePatterns($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreateTemplate(): array
    {
        return $this->getFormModel()->createTemplate($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdateTemplate(): array
    {
        return $this->getFormModel()->updateTemplate($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeleteTemplate(): array
    {
        return $this->getFormModel()->deleteTemplate((int) $this->getRequestData('template_id'));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdateTemplates(): array
    {
        return $this->getFormModel()->updateTemplates($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeleteTemplates(): array
    {
        return $this->getFormModel()->deleteTemplates($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreateExcPattern(): array
    {
        return $this->getFormModel()->createExcPattern($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdateExcPattern(): array
    {
        return $this->getFormModel()->updateExcPattern($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeleteExcPattern(): array
    {
        return $this->getFormModel()->deleteExcPattern((int) $this->getRequestData('pattern_id'));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionUpdateExcPatterns(): array
    {
        return $this->getFormModel()->updateExcPatterns($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeleteExcPatterns(): array
    {
        return $this->getFormModel()->deleteExcPatterns($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreateSubRule(): array
    {
        return $this->getFormModel()->createSubRule($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function  actionUpdateTopicPriority(): array
    {
        return $this->getFormModel()->updateTopicsPriority($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function  actionUpdateRulePriority(): array
    {
        return $this->getFormModel()->updateRulesPriority($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCopyRule()
    {
        return $this->getFormModel()->copyRuleToTopic($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCopyPatterns()
    {
        return $this->getFormModel()->copyPatterns($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreatePatternVar()
    {
        return $this->getFormModel()->createPatternVar($this->getRequestData());
    }

    public function actionUpdatePatternVar()
    {
        return $this->getFormModel()->updatePatternVar($this->getRequestData());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionDeletePatternVar()
    {
        return $this->getFormModel()->deletePatternVar($this->getRequestData());
    }

    public function actionCreateLearningPhrases(): array
    {
        return $this->getFormModel()->createLearningPhrases($this->requestData);
    }

    public function actionUpdateLearningPhrases(): array
    {
        return $this->getFormModel()->updateLearningPhrases($this->requestData);
    }

    public function actionDeleteLearningPhrases(): array
    {
        return $this->getFormModel()->deleteLearningPhrases($this->requestData);
    }

    public function actionDeleteMultipleLearningPhrases(): array
    {
        return $this->getFormModel()->deleteMultipleLearningPhrases($this->requestData);
    }

    /**
     * @return array
     */
    public function actionGetPatternsVariables()
    {
        return $this->getFormModel()->getPatternsVariables();
    }

    /**
     * @inheritdoc
     *
     * @return TopicsNgForm
     */
    protected function getFormModel(): TopicsNgForm
    {
        return $this->formModel;
    }
}
