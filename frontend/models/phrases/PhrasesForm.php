<?php
declare(strict_types=1);

namespace frontend\models\phrases;

use frontend\models\BaseForm;
use frontend\models\DataProvider;

class PhrasesForm extends BaseForm
{
    public $groupsGroupsPhrases = array();
    public $groupsPhrases = array();
    public $phrases = array();
    public $delays = array();
    public $timeslots = array();

    public $currentGroupGroupPhrasesId = null;
    public $currentGroupPhrasesId = null;
    public $currentPhrasesId = null;

    public $availableTags   = array();

    public function __construct()
    {
        $this->delays = DataProvider::getInstance()->getDelays();
        $this->timeslots = DataProvider::getInstance()->getTimeslots();
        $this->availableTags = \Yii::$app->params['groupPhrasesTags'];
    }

    public function getGroupsPhrases()
    {
        //Get groups groups phrases
        $groupsGroupsPhrases = \Yii::$app->db->createCommand('SELECT * FROM group_group_phrases_table')->queryAll();
        foreach ($groupsGroupsPhrases as $groupGroupsPhrases) {
            $this->groupsGroupsPhrases[] = new GroupGroupsPhrases((int)$groupGroupsPhrases['id'], $groupGroupsPhrases['name']);
        }

        //Initialize groups phrases
        $this->groupsPhrases = DataProvider::getInstance()->getGroupsPhrases();

        //Get group phrases tags
        foreach ($this->groupsPhrases as $key => $val) {
            $currentGroupPhrasesTagsArr = [];
            $currentGroupPhrasesTags    = GroupPhrasesTag::findAll(['groupPhrasesId' => $val->getId()]);
            foreach ($currentGroupPhrasesTags as $currentGroupPhrasesTag) {
                $currentGroupPhrasesTagsArr[] = $currentGroupPhrasesTag->tagId;
            }
            $this->groupsPhrases[$key]->setTags($currentGroupPhrasesTagsArr);
        }

        $groupPhrasesIds = array();
        foreach ($this->groupsPhrases as $groupPhrases) {
            $groupPhrasesIds[] =  array($groupPhrases->getId());
        }
        $query = \Yii::$app->db->queryBuilder->batchInsert(
            'group_group_phrases_group_phrases_table',
            ['group_phrases_id'],
            $groupPhrasesIds
        );
        $query = str_replace('INSERT INTO ', 'INSERT IGNORE INTO ', $query);
        \Yii::$app->db->createCommand($query)->execute();

        //Set group groups phrases ids
        $query = 'SELECT group_group_phrases_id FROM group_group_phrases_group_phrases_table WHERE group_phrases_id = :id';
        $selectGroupGroupsPhrasesIds = \Yii::$app->db->createCommand($query);
        foreach ($this->groupsPhrases as $key => $value) {
            $groupGroupsPhrasesId = $selectGroupGroupsPhrasesIds->bindValue(':id', $value->getId())->queryOne();
            $this->groupsPhrases[$key]->setGroupGroupsPhrasesId((int)$groupGroupsPhrasesId['group_group_phrases_id']);
        }
    }

    public function createGroupGroupsPhrases($name)
    {
        $query = 'INSERT INTO group_group_phrases_table(name) VALUES (:name)';
        $insertGroupGroupsPhrases = \Yii::$app->db->createCommand($query);
        $insertGroupGroupsPhrases->bindValue(':name', $name)->execute();
        return true;
    }

    public function updateGroupGroupsPhrases($id, $name)
    {
        $query = 'UPDATE group_group_phrases_table SET name = :name WHERE id=:id';
        $updateGroupGroupsPhrases = \Yii::$app->db->createCommand($query);
        $updateGroupGroupsPhrases->bindValue(':id', $id)->bindValue(':name', $name)->execute();
        return true;
    }

    public function deleteGroupGroupsPhrases($id)
    {
        $query = 'SELECT COUNT(group_group_phrases_id) FROM group_group_phrases_group_phrases_table WHERE group_group_phrases_id = :id';
        $selectGroupGroupsPhrasesIdsCount = \Yii::$app->db->createCommand($query);
        $count = $selectGroupGroupsPhrasesIdsCount->bindValue(':id', $id)->queryScalar();
        $count = (int)$count;
        if ($count != 0) {
            return false;
        }

        $query = 'DELETE FROM group_group_phrases_table WHERE id=:id';
        $deleteGroupGroupsPhrases = \Yii::$app->db->createCommand($query);
        $deleteGroupGroupsPhrases->bindValue(':id', $id)->execute();
        return true;
    }

    public function copyGroupGroupsPhrases($id)
    {
        // Get group groups phrases name
        $query = 'SELECT name FROM group_group_phrases_table WHERE id = :id';
        $selectGroupGroupsPhrasesName = \Yii::$app->db->createCommand($query);
        $groupGroupsPhrasesName = 'Copy ' . $selectGroupGroupsPhrasesName->bindValue(':id', $id)->queryScalar();

        // Create new group groups phrases
        $groupGroupsPhrasesId = null;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $query = 'INSERT INTO group_group_phrases_table(name) VALUES (:name)';
            $insertGroupGroupsPhrases = \Yii::$app->db->createCommand($query);
            $insertGroupGroupsPhrases->bindValue(':name', $groupGroupsPhrasesName)->execute();
            $groupGroupsPhrasesId = \Yii::$app->db->getLastInsertID();
            $transaction->commit();
        } catch(Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        // Get all groups phrases
        $allGroupsPhrases = \Yii::$app->BrungildaApiCurl->getGroupPhrases();

        // Get groups phrases of copying group groups phrases
        $query = 'SELECT group_phrases_id FROM group_group_phrases_group_phrases_table WHERE group_group_phrases_id = :id';
        $selectGroupPhrasesIds = \Yii::$app->db->createCommand($query);
        $groupPhrasesIdsEntities = $selectGroupPhrasesIds->bindValue(':id', $id)->queryAll();
        $groupPhrasesIds = array();
        foreach ($groupPhrasesIdsEntities as $groupPhrasesIdsEntity) {
            $groupPhrasesIds[] = $groupPhrasesIdsEntity['group_phrases_id'];
        }

        // Compare all groups phrases ids with ids of copying groups phrases
        foreach ($allGroupsPhrases as $groupPhrases) {
            if (in_array($groupPhrases['group_phrase_id'], $groupPhrasesIds)) {
                $originalGroupPhrasesId = $groupPhrases['group_phrase_id'];
                unset($groupPhrases['group_phrase_id']);

                // Create new group phrases
                $response = \Yii::$app->BrungildaApiCurl->createGroupPhrases(array($groupPhrases));
                $groupPhrasesId = $response[0]['item']['item']['group_phrase_id'];

                // Insert link of created group phrases to new group groups phrases
                $query = 'INSERT INTO group_group_phrases_group_phrases_table(group_group_phrases_id, group_phrases_id) ';
                $query .= 'VALUES (:group_group_phrases_id, :group_phrases_id)';
                $insertGroupPhrases = \Yii::$app->db->createCommand($query);
                $insertGroupPhrases->bindValue(':group_group_phrases_id', $groupGroupsPhrasesId)
                    ->bindValue(':group_phrases_id', $groupPhrasesId)->execute();

                // Create phrases of created group phrases
                $response = \Yii::$app->BrungildaApiCurl->getPhrases($originalGroupPhrasesId);
                foreach ($response['phrases'] as $key => $val) {
                    unset($response['phrases'][$key]['phrase_id']);
                    $response['phrases'][$key]['group_phrase_id'] = $groupPhrasesId;
                }
                \Yii::$app->BrungildaApiCurl->createPhrases($response['phrases']);
            }
        }

        return true;
    }

    public function getPhrases(int $groupPhrasesId)
    {
        $this->phrases = DataProvider::getInstance()->getPhrases($groupPhrasesId);
    }

    public function updateGroupPhrases(int $groupPhrasesId, string $text, string $lang, array $tags, $groupPhrasesIds)
    {
        GroupPhrasesTag::deleteAll('groupPhrasesId = ' . $groupPhrasesId);
        foreach ($tags as $tag) {
            $groupPhrasesTag                    = new GroupPhrasesTag();
            $groupPhrasesTag->groupPhrasesId    = $groupPhrasesId;
            $groupPhrasesTag->tagId             = $tag;
            $groupPhrasesTag->save();
        }
        return DataProvider::getInstance()->updateGroupPhrases($groupPhrasesId, $text, $lang, $groupPhrasesIds);
    }

    public function deleteGroupPhrases(array $params)
    {
        foreach ($params as $param) {
            GroupPhrasesTag::deleteAll('groupPhrasesId = ' . $param['group_phrase_id']);
        }

        return DataProvider::getInstance()->deleteGroupPhrases($params);
    }
}
