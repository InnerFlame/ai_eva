<?php
use common\widgets\BindableForm\BindableForm;
use common\widgets\Modal\Modal;
use yii\bootstrap\Html;
use frontend\assets\SubstitutesAsset;

SubstitutesAsset::register($this);

//Modal confirm delete substitute
echo Modal::widget([
    'id' => 'confirmDeleteSubstituteModal',
    'title' => 'Delete substitute',
    'hiddenInputIds' => ['deleteSubstituteId',],
    'nameText' => 'substitute',
    'nameId' => 'deleteSubstituteName',
    'buttonId' => 'confirmDeleteSubstituteButton'
]);

//Modal confirm delete substitute phrase
echo Modal::widget([
    'id' => 'confirmDeleteSubstitutePhraseModal',
    'title' => 'Delete substitute phrase',
    'hiddenInputIds' => ['deleteSubstitutePhraseParentId', 'deleteSubstitutePhraseId',],
    'nameText' => 'substitute phrase',
    'nameId' => 'deleteSubstitutePhraseName',
    'buttonId' => 'confirmDeleteSubstitutePhraseButton'
]);
?>
<!-- SUBSTITUTES CONTAINER -->
<div class="col-md-6" id="substitutesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a href="javascript:void(0);">Substitute</a></li>
    </ul>
    <?php
    echo BindableForm::widget([
        'id' => 'substitutesBindableForm',
        'hiddens' => [
            ['name' => 'substituteId'],
        ],
        'lines' => [
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                    'content' => Html::textarea('itemText', '', [
                        'class' => 'form-control',
                        'rows' => '5',
                        'id' => 'substituteText',
                        'style' => 'resize:vertical;'
                    ])
                ],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-2'], 'content' => Html::button('Delete', [
                    'class' => 'btn btn-danger btn-block',
                    'type' => 'button',
                    'id' => 'deleteSubstituteButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-3'], 'content' => Html::button('Add', [
                    'class' => 'btn btn-success btn-block',
                    'type' => 'button',
                    'id' => 'addNewSubstituteButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-7'], 'content' => Html::button('Update', [
                    'class' => 'btn btn-primary btn-block',
                    'type' => 'button',
                    'id' => 'updateSubstituteButton',
                ])],
            ]],
        ],
    ]);
    ?>
    <div id="substitutes"></div>
</div>
<!-- SUBSTITUTES CONTAINER -->
<!-- SUBSTITUTE PHRASES CONTAINER -->
<div class="col-md-6" id="substitutePhrasesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a href="javascript:void(0);">Substitute phrase</a></li>
    </ul>
    <?php
    echo BindableForm::widget([
        'id' => 'substitutePhrasesBindableForm',
        'hiddens' => [
            ['name' => 'substituteId'],
            ['name' => 'substitutePhraseId'],
        ],
        'lines' => [
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                    'content' => Html::textarea('itemText', '', [
                        'class' => 'form-control',
                        'rows' => '5',
                        'id' => 'substitutePhraseText',
                        'style' => 'resize:vertical;'
                    ])
                ],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-2'], 'content' => Html::button('Delete', [
                    'class' => 'btn btn-danger btn-block',
                    'type' => 'button',
                    'id' => 'deleteSubstitutePhraseButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-3'], 'content' => Html::button('Add', [
                    'class' => 'btn btn-success btn-block',
                    'type' => 'button',
                    'id' => 'addNewSubstitutePhraseButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-7'], 'content' => Html::button('Update', [
                    'class' => 'btn btn-primary btn-block',
                    'type' => 'button',
                    'id' => 'updateSubstitutePhraseButton',
                ])],
            ]],
        ],
    ]);
    ?>
    <div id="substitutePhrases"></div>
</div>
<!-- SUBSTITUTE PHRASES CONTAINER -->