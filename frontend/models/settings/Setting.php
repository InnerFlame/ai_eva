<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class Setting extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_table';
    }

    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['setting_id' => 'id']);
    }

    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['setting_id' => 'id']);
    }

    public function getLocales()
    {
        return $this->hasMany(Locale::className(), ['setting_id' => 'id']);
    }

    public function getSenderLocales()
    {
        return $this->hasMany(SenderLocale::className(), ['setting_id' => 'id']);
    }

    public function getStatuses()
    {
        return $this->hasMany(Status::className(), ['setting_id' => 'id']);
    }

    public function getSites()
    {
        return $this->hasMany(Site::className(), ['setting_id' => 'id']);
    }

    public function getTrafficSources()
    {
        return $this->hasMany(TrafficSource::className(), ['setting_id' => 'id']);
    }

    public function getActionWays()
    {
        return $this->hasMany(ActionWay::className(), ['setting_id' => 'id']);
    }

    public function getTrafficTypes()
    {
        return $this->hasMany(TrafficType::className(), ['setting_id' => 'id']);
    }

    public function getPlatforms()
    {
        return $this->hasMany(Platform::className(), ['setting_id' => 'id']);
    }

    public function getScenarios()
    {
        return $this->hasMany(Scenario::className(), ['setting_id' => 'id']);
    }

    public function getFanClubs()
    {
        return $this->hasMany(FanClub::className(), ['setting_id' => 'id']);
    }

    public function getOnlines()
    {
        return $this->hasMany(Online::className(), ['setting_id' => 'id']);
    }
}
