export class TestChatModel
{
    constructor(
        public scenario: string = '',
        public setting: string = '',
        public fromUserId: string = '',
        public toUserId: string = '',
        public text: string = ""
    ) {  }

    public static getScenariosApiUrl(): string
    {
        return "/test-chat/get-scenarios";
    }

    public static getSettingsApiUrl(): string
    {
        return "/test-chat/get-settings";
    }

    public static getGroupPhrasesApiUrl(): string
    {
        return "/test-chat/get-group-phrases";
    }

    public static getResultsApiUrl(): string
    {
        return "/test-chat/get-test-result";
    }

    public static getHistoryDataApiUrl(): string
    {
        return "/test-chat/get-history-data";
    }

    public static getClearHistoryApiUrl(): string
    {
        return "/test-chat/clear-history-data";
    }
}
