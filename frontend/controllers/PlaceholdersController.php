<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\placeholders\PlaceholdersForm;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class PlaceholdersController extends BaseController
{
    protected $model;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new PlaceholdersForm();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('readPlaceholders')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        return $this->render('index', ['model' => $this->model]);
    }

    public function actionPlaceholders()
    {
        $this->model->getPlaceholders();
        return $this->renderAjax('_placeholders', ['model' => $this->model]);
    }

    public function actionCreatePlaceholder()
    {
        if (!\Yii::$app->user->can('editPlaceholders')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->createPlaceholder(
                $post['name'],
                $post['type'],
                urldecode($post['url']),
                urldecode($post['urlWap']),
                $post['urlType'],
                explode(",", $post['senderTypes']),
                explode(",", $post['receiverLocations']),
                $post['isLink']
            );
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionUpdatePlaceholder()
    {
        if (!\Yii::$app->user->can('editPlaceholders')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->updatePlaceholder(
                $post['id'],
                $post['name'],
                $post['type'],
                urldecode($post['url']),
                urldecode($post['urlWap']),
                $post['urlType'],
                explode(",", $post['senderTypes']),
                explode(",", $post['receiverLocations']),
                $post['active'],
                $post['isLink']
            );
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionDeletePlaceholder()
    {
        if (!\Yii::$app->user->can('editPlaceholders')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->deletePlaceholder($post['id']);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionDeletePlaceholders()
    {
        if (!\Yii::$app->user->can('editPlaceholders')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $params = json_decode($post['params'], true);
            $response = $this->model->deletePlaceholders($params);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }
}
