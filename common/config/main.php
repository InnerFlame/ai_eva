<?php
$evaCurlOptions = [];
if ($evaAuth = getenv('EVA_AUTH', true)) {
    $evaCurlOptions = [
        'CURLOPT_SSL_VERIFYPEER' => false,
        'CURLOPT_SSL_VERIFYHOST' => false,
        'CURLOPT_USERPWD' => $evaAuth,
        'CURLOPT_HTTPAUTH' => CURLAUTH_BASIC,
    ];
}

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['log'],
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [],
            'migrationPath' => [
                '@app/migrations',
                '@yii/log/migrations',
                '@yii/rbac/migrations',
            ],
        ],
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.getenv('MYSQL_HOST', true).';dbname=' . getenv('MYSQL_DATABASE', true),
            'username' => getenv('MYSQL_USER', true),
            'password' => getenv('MYSQL_PASSWORD', true),
            'charset' => 'utf8',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'BrungildaApiCurl' => [
            'class'  => 'common\components\BrungildaApi\BrungildaApiCurl',
            'config' => [
                'curlDomain' => getenv('EVA_URL', true),
                'curlOptions' => array_merge(
                    [
                        'CURLOPT_RETURNTRANSFER' => true,
                    ],
                    $evaCurlOptions
                ),
            ],
        ],
        'webClient' => [
            'class'  => 'common\components\WebClientComponent',
        ],
        'placeholders' => [
            'class'  => 'common\components\PlaceholdersComponent',
        ],
        'scenarios' => [
            'class'  => 'common\components\ScenariosComponent',
        ],
        'communicationLog' => [
            'class'  => 'common\components\CommunicationLogComponent',
        ],
        'eva' => [
            'class'  => 'common\components\EvaComponent',
        ],
        'externalMessageProcessor' => [
            'class'  => 'common\components\ExternalMessageProcessorComponent',
        ],
        'dialogRecovery' => [
            'class'  => 'common\components\DialogRecoveryComponent',
        ],
        'directMessageProcessor' => [
            'class'  => 'common\components\DirectMessageProcessorComponent',
        ],
        'testChatMessageProcessor' => [
            'class'  => 'common\components\TestChatMessageProcessorComponent',
        ],
        'testChatScenarios' => [
            'class'  => 'common\components\TestChatScenariosComponent',
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                    'except' => [
                        'yii\web\HttpException:403',
                    ],
                ],
            ],
        ],
        'cache' => [
            'class'        => 'yii\caching\MemCache',
            'useMemcached' => true,
            'servers'      => [
                [
                    'host' => getenv('MEMCACHED_HOST', true),
                    'port' => 11211,
                ],
            ]
        ],
        'redis' => [
            'class'    => 'yii\redis\Connection',
            'hostname' => getenv('REDIS_HOST', true),
            'port'     => 6379,
            'database' => 0,
        ],
        'amqp' => [
            'class'                   => 'common\components\AmqpComponent',
            'host'                    => getenv('RABBITMQ_HOST', true),
            'port'                    => 5672,
            'user'                    => getenv('RABBITMQ_DEFAULT_USER', true),
            'pass'                    => getenv('RABBITMQ_DEFAULT_PASS', true),
            'vhost'                   => getenv('RABBITMQ_DEFAULT_VHOST', true),
            'prefix'                  => getenv('RABBITMQ_PREFIX', true),
        ],
        'placeholderForZeroStepProcessor' => [
            'class' => 'common\components\PlaceholderForZeroStepProcessorComponent',
        ],
    ],
];
