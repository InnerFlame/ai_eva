<?php

use yii\db\Migration;

class m161018_085252_rename_settings_table_values extends Migration
{
    public function up()
    {
        $this->update('settings_table', ['control_profile' => 'Cams'], ['control_profile' => 'Cams models']);
        $this->update('settings_table', ['control_profile' => 'External'], ['control_profile' => 'Direct external']);
        $this->update('settings_table', ['control_profile' => 'Labs'], ['control_profile' => 'Promoted foreign girl']);
    }

    public function down()
    {
        $this->update('settings_table', ['control_profile' => 'Cams models'], ['control_profile' => 'Cams']);
        $this->update('settings_table', ['control_profile' => 'Direct external'], ['control_profile' => 'External']);
        $this->update('settings_table', ['control_profile' => 'Promoted foreign girl'], ['control_profile' => 'Labs']);
    }
}
