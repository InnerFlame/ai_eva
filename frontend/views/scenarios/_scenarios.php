<?php
use common\widgets\BindableFormItems\BindableFormItems;

$scenarioItems = array();

foreach ($model->scenarios as $scenario) {
    $item['text'] = $scenario->getName();
    $item['hiddens'] = [
        ['name' => 'scenarioId',        'value' => $scenario->getId()],
        ['name' => 'scenarioNodes',     'value' => $scenario->getNodes()],
        ['name' => 'scenarioFlowChart', 'value' => $scenario->getFlowChart()],
        ['name' => 'scenarioTs',        'value' => $scenario->getTs()],
        ['name' => 'itemMaxStepCount',  'value' => $scenario->getMaxStepCount()],
        ['name' => 'isBlock',           'value' => $scenario->getIsBlock()],
        ['name' => 'scenarioTags',      'value' => json_encode($scenario->tags)],
    ];
    $item['rightElement'] = '<span class="badge pull-right">' . $scenario->getMaxStepCount() . '</span>';
    $scenarioItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId'    => 'scenarioBindableForm',
    'items'             => $scenarioItems,
    'firstItemText'     => 'Add new scenario...',
    'firstItemOptions'  => [
        'name'              => 'addNewScenario',
        'data-toggle'       => 'modal',
        'data-target'       => '#addNewScenarioModal',
        'class'             => 'scenarioItem',
    ],
    'secondItemText'    => 'Add new block...',
    'secondItemOptions' => [
        'name'              => 'addNewBlock',
        'data-toggle'       => 'modal',
        'data-target'       => '#addNewBlockModal',
        'class'             => 'blockItem',
    ],
    'checked'           => true,
]);
