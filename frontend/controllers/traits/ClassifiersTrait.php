<?php
declare(strict_types=1);

namespace frontend\controllers\traits;

trait ClassifiersTrait
{
    /**
     * @return array
     */
    private function getClassifiersAvailableModels(): array
    {
        $result = $this->getFormModel()->getClassifiersAvailableModels();

        if (empty($result['models'])) {
            return [];
        }

        $result['models'] = $this->sortModels($result['models']);

        return $result;
    }

    /**
     * @param array $modelsArray
     * @return array
     */
    private function sortModels(array $modelsArray): array
    {
        $sortedModels = [];
        $preparedModels = [];

        foreach ($modelsArray as $model) {
            $modelParts     = explode('_', $model);
            $modelDateParts = explode('-', $modelParts[1]);

            $modelName  = (string) $modelParts[0];
            $modelDate  = (string) $modelDateParts[0];
            $modelTime  = (int) $modelDateParts[1];

            $sortedModels[$modelName][strtotime($modelDate)][$modelTime] = $model;
            krsort($sortedModels[$modelName]);
            krsort($sortedModels[$modelName][strtotime($modelDate)]);
        }

        foreach ($sortedModels as $name => $values) {
            foreach ($values as $date => $timeValues) {
                foreach ($timeValues as $modelValue) {
                    $preparedModels[] = $modelValue;
                }
            }
        }

        return $preparedModels;
    }
}
