<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class Locale extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_locales';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
