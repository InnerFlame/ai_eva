<?php

namespace frontend\assets;

class ClassifiersAsset extends BasicAsset
{
    public $js = [
        'js/classifiers.js',
    ];
}
