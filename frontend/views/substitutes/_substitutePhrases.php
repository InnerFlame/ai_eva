<?php
use common\widgets\BindableFormItems\BindableFormItems;

$substitutePhrasesItems = array();
foreach ($model->substitutePhrases as $substitutePhrase) {
    $item['text'] = $substitutePhrase->getPhrase();
    $item['hiddens'] = [
        ['name' => 'substituteId', 'value' => $substitutePhrase->getSubstituteId()],
        ['name' => 'substitutePhraseId', 'value' => $substitutePhrase->getId()],
    ];
    $substitutePhrasesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId' => 'substitutePhrasesBindableForm',
    'items' => $substitutePhrasesItems,
    'checked' => false,
]);
