<?php

use yii\db\Migration;

/**
 * Class m171013_092350_add_column_splitName_to_settings_table
 */
class m171013_092350_add_column_splitName_to_settings_table extends Migration
{
    public function safeUp()
    {
        $this
            ->addColumn('settings_table', 'splitName', $this->string());
        $this
            ->addColumn('settings_table', 'splitGroup', $this->integer());

        $this
            ->addColumn('consolidatedSettings', 'splitName', $this->string());
        $this
            ->addColumn('consolidatedSettings', 'splitGroup', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('consolidatedSettings', 'splitGroup');
        $this->dropColumn('consolidatedSettings', 'splitName');

        $this->dropColumn('settings_table', 'splitGroup');
        $this->dropColumn('settings_table', 'splitName');
    }
}
