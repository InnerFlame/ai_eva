var datepickerFrom, datepickerTo;

$(document).ready(function () {
    //Check all checkboxes when page is ready
    $('form input[type="checkbox"]').prop('checked', true);

    //Initialize datepickers
    datepickerTo = $("#datepickerTo").datepicker({dateFormat: "yy-mm-dd"})
        .datepicker( "option", "minDate", -1)
        .datepicker( "option", "maxDate", +6);
    datepickerFrom = $("#datepickerFrom").datepicker({dateFormat: "yy-mm-dd"}).datepicker('setDate', -1).on( "change", function() {
        var minDate = $(this).datepicker('getDate');
        var maxDate = $(this).datepicker('getDate');
        maxDate.setDate(minDate.getDate() + 7);
        datepickerTo
            .datepicker( "option", "minDate", minDate)
            .datepicker( "option", "maxDate", maxDate);
    });


    //Click on Export button
    $(document).on('click', '#export', function () {
        var i = 0;
        var exportArray = [];
        exportArray[i] = [];
        $('#searchResults table thead th').each(function(){
            exportArray[i].push($(this).text());
        });

        $('#searchResults table tbody tr').each(function(){
            i = i + 1;
            exportArray[i] = [];
            $(this).find('td').each(function(){
                exportArray[i].push($(this).text());
            })
        });

        var form = $(document.createElement('form'));
        form.attr("action", "/common-info/export");
        form.attr("method", "POST");
        var input = $("<input>").attr("type", "hidden").attr("name", "params").val(JSON.stringify(exportArray));
        form.append(input);
        form.appendTo(document.body);
        form.submit();
        form.remove();
    });

    //Click "Check all"
    $(document).on('click', 'ul li.checkAll', function () {
        var isChecked = ($(this).siblings('div').first().find('input[type="checkbox"]:checked').length > 0);
        $(this).siblings('div').find('input[type="checkbox"]').prop('checked', !isChecked);
        displayShortSummary($(this).closest('ul.dropdown-menu'));

    });

    var progressBarHtml = '<div class="progress-bar progress-bar-striped active" role="progressbar"'
        + 'aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">'
        + 'Search...'
        + '</div>';

    $(document).on('beforeSubmit', '#commonInfoSearch', function () {
        $('#searchResults').html(progressBarHtml);
        $('#placeholdersSearchResults').html('');
        return true;
    });

    $(document).on('click', '#placeholdersSearchResults a', function(event) {
        event.preventDefault();
        var href = $(this).attr('href');
        $.pjax({
            type: 'POST',
            url: href,
            container: '#search',
            data: $('#commonInfoSearch').serialize() + '&_pjax=#search',
            push: false,
            scrollTo: '#placeholdersSearchResults'
        })
    })

    // Prevent closing dropdown when click on it or 'Check all'
    $(document).on('click', '#commonInfoSearch .dropdown-menu', function (e) {
        e.stopPropagation();
    });

    // Live search functionality
    $(document).on('keyup', 'input.livesearch', function () {
        var pattern = $(this).val().toLowerCase();
        $(this).siblings('div').find('li:not(.checkAll)').each(function (i) {
            var text = $(this).contents().eq(0).text().toLowerCase();
            if (text.indexOf(pattern) == -1 && !$(this).first().find('label input[type="checkbox"]:checked').length > 0) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });

    $(document).on('change', 'input[type="checkbox"]', function() {
        displayShortSummary($(this).closest('ul.dropdown-menu'));
    });

    $('ul.dropdown-menu').each(function () {
        displayShortSummary($(this));
    });

    //Click on Explain button (attribute formaction on button doesn't work)
    $(document).on('click', '#explainButton', function () {
        var form = $("#commonInfoSearch").clone();
        form.attr("action", "/common-info/explain");
        form.attr("method", "POST");
        form.appendTo(document.body);
        form.submit();
        form.remove();
    });
});

// Displaying short summary
function displayShortSummary(ulElement)
{
    var summary = '';
    var checkedArray = $(ulElement).find('label input[type="checkbox"]:checked').slice(0,3);
    if (checkedArray.length > 0) {
        for (var i = 0; i < checkedArray.length; i++) {
            summary += $(checkedArray[i]).parent().text() + ', '
        }
        summary += '...'
    }
    $(ulElement).siblings('span.short-summary').html(summary);
}

$(document).on('pjax:success', function(){
    //Initialize datepickers, but not set date
    datepickerTo = $("#datepickerTo").datepicker({dateFormat: "yy-mm-dd"});
    datepickerFrom = $("#datepickerFrom").datepicker({dateFormat: "yy-mm-dd"}).on( "change", function() {
        var minDate = $(this).datepicker('getDate');
        var maxDate = $(this).datepicker('getDate');
        maxDate.setDate(minDate.getDate() + 7);
        datepickerTo
            .datepicker( "option", "minDate", minDate)
            .datepicker( "option", "maxDate", maxDate);
    });

    $( "#datepickerFrom" ).trigger( "change" );

    $('ul.dropdown-menu').each(function () {
        displayShortSummary($(this));
    });
});
