//Core imports
import { BrowserModule }     from '@angular/platform-browser';
import { NgModule }          from '@angular/core';
import { HttpClientModule }  from '@angular/common/http';

//Custom imports
import { AppComponent }         from './app.component';
import { SharedModule }         from './shared';
import { TestPhrasesModule }    from "./testPhrases/test.phrases.module";
import { TestChatModule }       from "./testChat/test.chat.module";
import { SettingsModule }       from "./settings/settings.module";
import { TimeSlotsModule }      from "./timeSlots/time.slots.module";
import { ErrorLogModule }       from "./errorLog/error.log.module";
import { AccessModule }         from "./access/access.module";
import { SubstitutesModule }    from "./substitutes/substitutes.module";
import { NlpClassifiersModule } from "./nlpClassifiers/nlp.classifiers.module";
import { ClassifiersModule }    from "./classifiers/classifiers.module";
import { TopicsModule }         from "./topics/topics.module";
import { TopicsGroupModule }    from "./topicsGroup/topics.group.module";
import { MreClassifiersModule } from "./mreClassifiers/mre.classifiers.module";

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        SharedModule,
        TestPhrasesModule,
        TestChatModule,
        SettingsModule,
        TimeSlotsModule,
        ErrorLogModule,
        NlpClassifiersModule,
        MreClassifiersModule,
        AccessModule,
        SubstitutesModule,
        ClassifiersModule,
        TopicsModule,
        TopicsGroupModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [],
    bootstrap: [AppComponent]
})


export class AppModule { }
