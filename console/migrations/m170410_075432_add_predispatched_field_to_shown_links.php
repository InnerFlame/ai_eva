<?php

use yii\db\Migration;

class m170410_075432_add_predispatched_field_to_shown_links extends Migration
{
    public function up()
    {
        $this->addColumn('shown_links', 'isPredispatched', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('shown_links', 'isPredispatched');
    }
}
