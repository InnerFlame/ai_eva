<?php

namespace console\controllers;

use frontend\models\commonInfo\CommunicationLog;
use yii\console\Controller;


class CleanUpCommunicationLogController extends Controller
{

    public function actionIndex()
    {
        echo 'Start' . PHP_EOL;

        $startTime = strtotime('2017-01-18');

        $endTime = strtotime('2017-02-04');

        while ($startTime < $endTime) {
            echo date('Y-m-d H:i:s', $startTime) . PHP_EOL;
            \Yii::$app->db->createCommand()->delete(
                CommunicationLog::tableName(),
                'createdAt >= :from AND createdAt <= :to',
                [
                    ':from' => date('Y-m-d H:i:s', $startTime),
                    ':to'   => date('Y-m-d H:i:s', $startTime + 3600),
                ]
            )->execute();

            $startTime += 3600;
            sleep(2);
        }

        echo 'Done' . PHP_EOL;
    }
}