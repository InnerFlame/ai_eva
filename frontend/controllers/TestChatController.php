<?php
namespace frontend\controllers;

use frontend\models\settings\Setting;
use frontend\models\testChat\TestChatFormModel;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

/**
 * Class TestChatController
 * @package frontend\controllers
 */
class TestChatController extends BaseNgController
{
    /**
     * @inheritdoc
     *
     * TestChatController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new TestChatFormModel());
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('testChat')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * Renders main test chat page.
     *
     * @return string Result of rendering.
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionGetScenarios(): array
    {
        return $this->getFormModel()->getScenarios();
    }

    /**
     * @return array
     */
    public function actionGetSettings(): array
    {
        $data = Setting::find()->orderBy('priority')->asArray()->all();

        return $data;
    }

    /**
     * @return array
     */
    public function actionGetGroupPhrases(): array
    {
        return $this->getFormModel()->getGroupPhrases();
    }

    /**
     * Get test results and write its to cache, for manager history
     *
     * @return array
     */
    public function actionGetTestResult(): array
    {
        $fromUserId = $this->requestData['fromUserId'];
        $toUserId   = $this->requestData['toUserId'];
        $setting    = $this->requestData['setting'];
        $scenario   = $this->requestData['scenario'];
        $text       = $this->requestData['text'];

        $response = $this->getFormModel()->getResponseData(
                (int) $setting,
                (string) $fromUserId,
                (string) $toUserId,
                (string) $text,
                (int) $scenario
            ) + [
                'fromUserId'  => $fromUserId,
                'toUserId'    => $toUserId,
                'sendedText'  => $text,
                'scenario_id' => $scenario
            ];

        $dialogueKey = \Yii::$app->user->getId() . '_chat';
        $inputKey    = \Yii::$app->user->getId() . '_chat-input';

        if ($cachedData = \Yii::$app->cache->get($dialogueKey)) {
            array_unshift($cachedData, $response);
            \Yii::$app->cache->set($dialogueKey, $cachedData, 43200);//ttl - 12 hours
        } else {
            $cachedData = [$response];
            \Yii::$app->cache->set($dialogueKey, $cachedData, 43200);//ttl - 12 hours
        }

        \Yii::$app->cache->set($inputKey, [
            'fromUserId' => $fromUserId,
            'toUserId'   => $toUserId,
            'scenarioId' => $scenario,
            'setting'    => $setting
        ], 43200);

        return $cachedData;
    }

    /**
     * @return array
     */
    public function actionGetHistoryData(): array
    {
        $inputKey     = \Yii::$app->user->getId() . '_chat-input';
        $dialogueKey  = \Yii::$app->user->getId() . '_chat';
        $dialogueData = ['res' => \Yii::$app->cache->get($dialogueKey)];

        if (!$dialogueData['res']) {
            return [];
        }

        if ($inputData = \Yii::$app->cache->get($inputKey)) {
            $dialogueData['inputs'] = $inputData;
            return $dialogueData;
        }

        return [];
    }

    /**
     * @return array
     */
    public function actionClearHistoryData(): array
    {
        $inputKey = \Yii::$app->user->getId() . '_chat-input';
        $key      = \Yii::$app->user->getId() . '_chat';

        \Yii::$app->cache->delete($inputKey);

        $cachedData = \Yii::$app->cache->get($key);

        if (!$cachedData) {
            return [];
        }

        foreach ($cachedData as $datum) {
            $userId = $datum['fromUserId'];
            if  ($userId) {
                $this->getFormModel()->deleteChat($userId);
            }
        }

        if (\Yii::$app->cache->delete($key)) {
            return ['deleted' => true];
        }

        return [];
    }

    /**
     * @inheritdoc
     *
     * @return TestChatFormModel
     */
    protected function getFormModel(): TestChatFormModel
    {
        return $this->formModel;
    }
}
