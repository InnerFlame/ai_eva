<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\classifiers\ClassifiersForm;
use yii\web\ForbiddenHttpException;

class ClassifiersController extends BaseController
{
    protected $model;

    /**
     * {@inheritdoc}
     *
     * @param string $id     the ID of this controller.
     * @param Module $module the module that this controller belongs to.
     * @param array  $config name-value pairs that will be used to initialize the object properties.
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new ClassifiersForm();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('scenarios')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Renders main Classifiers page.
     *
     * @return string Result of rendering.
     */
    public function actionIndex()
    {
        return $this->render('index', ['model' => $this->model]);
    }

    /**
     * Gets classifiers and renders data.
     *
     * @return string Result of rendering.
     */
    public function actionClassifiers()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getClassifiers();
                return $this->renderAjax('_classifiers', ['model' => $this->model]);
            }
        );
    }

    /**
     * Creates classifiers.
     *
     * @return boolean Result
     */
    public function actionCreateClassifiers()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->createClassifiers($post['text']);
            }
        );
    }

    /**
     * Updates classifiers.
     *
     * @return boolean Result
     */
    public function actionUpdateClassifiers()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->updateClassifiers((int) $post['classifierId'], $post['text']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    /**
     * Deletes classifiers.
     *
     * @return boolean Result
     */
    public function actionDeleteClassifiers()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteClassifiers((int) $post['classifierId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    /**
     * Gets classifiers patterns and renders data.
     *
     * @return string Result of rendering.
     */
    public function actionClassifierPatterns()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getClassifierPatterns((int) $post['classifierId']);
                return $this->renderAjax('_classifierPatterns', ['model' => $this->model]);
            }
        );
    }

    /**
     * Creates classifiers patterns.
     *
     * @return boolean Result
     */
    public function actionCreateClassifierPatterns()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                //$post['test'] = str_replace('\\', '\\\\', $post['test']);
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->createClassifierPatterns(
                    $post['сlassifierId'],
                    $post['text'],
                    $post['test']
                );
            }
        );
    }

    /**
     * Updates classifiers pattern.
     *
     * @return boolean Result
     */
    public function actionUpdateClassifierPattern()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                return $this->model->updateClassifierPattern(
                    (int) $post['classifierPatternId'],
                    $post['text'],
                    $post['test']
                );
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    /**
     * Updates classifiers patterns.
     *
     * @return boolean Result
     */
    public function actionUpdateClassifierPatterns()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->updateClassifierPatterns($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    /**
     * Deletes classifiers patterns.
     *
     * @return boolean Result
     */
    public function actionDeleteClassifierPatterns()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->deleteClassifierPattern($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }
}
