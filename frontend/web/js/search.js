$(document).ready(function () {
    $(document).on('click', '#searchContainer ul.dropdown-menu a', function () {
        var searchBy = $(this).html();
        $('#searchBy').val(searchBy);
        $('#searchByButton').html(searchBy + ' <span class="caret"></span>');
    });
});
