<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\topics\TopicsForm;
use yii\web\ForbiddenHttpException;

class TopicsController extends BaseController
{
    protected $model;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new TopicsForm();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('topics')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        $request    = \Yii::$app->request;
        $get        = $request->get();

        $this->model->currentTopicId        = $get['topicId'] ?? '';
        $this->model->currentRuleId         = $get['ruleId'] ?? '';
        $this->model->currentPatternId      = $get['patternId'] ?? '';
        $this->model->currentTemplateId     = $get['templateId'] ?? '';
        $this->model->currentExcPatternId   = $get['excPatternId'] ?? '';

        if (isset($get['templateId'])) {
            $this->model->currentTab = \frontend\models\topics\TopicsForm::TEMPLATES_TAB;
        } else if (isset($get['excPatternId'])) {
            $this->model->currentTab = \frontend\models\topics\TopicsForm::EXC_PATTERNS_TAB;
        }

        return $this->render('index', ['model' => $this->model]);
    }

    public function actionTopics()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getTopics();
                return $this->renderAjax('_topics', ['model' => $this->model]);
            }
        );
    }

    public function actionRules()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                $this->model->getRules((int)$post['topicId']);
                return $this->renderAjax('_rules', ['model' => $this->model]);
            }
        );
    }

    public function actionRule()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                if ($post['ruleId'] == 'null') {
                    return 'false';
                }
                $this->model->getRule((int)$post['ruleId']);
                $response = \Yii::$app->response;
                $response->format = \yii\web\Response::FORMAT_JSON;
                $response->data = [
                    'rules_patterns' => $this->renderAjax('_rules_patterns', ['model' => $this->model]),
                    'rules_templates' => $this->renderAjax('_rules_templates', ['model' => $this->model]),
                    'rules_subrules' => $this->renderAjax('_rules_subrules', ['model' => $this->model]),
                    'rules_exc_patterns' => $this->renderAjax('_rules_exc_patterns', ['model' => $this->model]),
                ];
                return $response;
            }
        );
    }

    public function actionUpdateTopic()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $post['params'] = str_replace(array("\r\n", "\r", "\n", "\t"), '', $post['params']);
                $params = json_decode($post['params'], true);
                return $this->model->updateTopic((int)$post['topicId'], $params);
            }
        );
    }

    public function actionUpdateTopics()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $post['params'] = str_replace(array("\r\n", "\r", "\n", "\t"), '', $post['params']);
                $params = json_decode($post['params'], true);
                return $this->model->updateTopics($params);
            }
        );
    }

    public function actionCreateTopic()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $post['name'] = str_replace(array("\r\n", "\r", "\n", "\t"), '', $post['name']);
                return $this->model->createTopic($post['name']);
            }
        );
    }

    public function actionDeleteTopic()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteTopic((int)$post['topicId']);
            }
        );
    }

    public function actionPasteRule()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $rule       = $this->model->getRawRule((int)$post['ruleId']);
                $topicId    = (int)$post['topicId'];

                function pasteRule($model, $rule, $topicId, $parentId = null)
                {
                    $subrules           = $rule['rule']['rules'];
                    $patterns           = $rule['rule']['patterns'];
                    $exceptionPatterns  = $rule['rule']['exception_patterns'];
                    $templates          = $rule['rule']['templates'];

                    $patternVars    = $rule['pattern_vars'] ?? array();
                    $variables      = $rule['variables'] ?? array();

                    // Create rule
                    $rule['rule']['topic_id']   = $topicId;
                    $rule['rule']['parent_id']  = $parentId;

                    unset($rule['rule']['rule_id']);
                    unset($rule['rule']['rules']);
                    unset($rule['rule']['patterns']);
                    unset($rule['rule']['exception_patterns']);
                    unset($rule['rule']['templates']);

                    $createRuleResponse = $model->createRawRule($rule['rule']);
                    $createdRuleId      = $createRuleResponse[0]['item']['item']['rule_id'];

                    // Create patterns
                    foreach ($patterns as $key => $val) {
                        $originalPatternId = $patterns[$key]['pattern_id'];
                        unset($patterns[$key]['pattern_id']);
                        $patterns[$key]['rule_id'] = $createdRuleId;

                        // If pattern have variable (one pattern can have only one variable)
                        // we create pattern, then it's variable then delete this pattern from array
                        foreach ($patternVars as $patternVar) {
                            if ($patternVar['pattern_id'] == $originalPatternId) {
                                // Create pattern
                                $createPatternResponse = $model->createPatterns(array($patterns[$key]));
                                $createdPatternId = $createPatternResponse[0]['item']['item']['pattern_id'];

                                // Mark pattern for deleting from array
                                unset($patterns[$key]);//['delete'] = true;

                                // Create pattern variable
                                unset($patternVar['pattern_var_id']);
                                $patternVar['pattern_id'] = $createdPatternId;
                                $model->createRawPatternVariable($patternVar);
                                break;
                            }
                        }
                    }
                    // Create patterns which don't have variables
                    if (!empty($patterns)) {
                        $patterns = array_values($patterns);
                        $model->createPatterns($patterns);
                    }

                    // Create templates
                    foreach ($templates as $key => $val) {
                        unset($templates[$key]['template_id']);
                        $templates[$key]['rule_id'] = $createdRuleId;
                    }
                    if (!empty($templates)) {
                        $model->createTemplates($templates);
                    }

                    // Create exception patterns
                    foreach ($exceptionPatterns as $key => $val) {
                        unset($exceptionPatterns[$key]['exception_pattern_id']);
                        $exceptionPatterns[$key]['rule_id'] = $createdRuleId;
                    }
                    if (!empty($exceptionPatterns)) {
                        $model->createExcPatterns($exceptionPatterns);
                    }

                    // Create subrules
                    foreach ($subrules as $val) {
                        // Make subrule format like rule format
                        $subrule['rule'] = $val;
                        // Recursive call
                        pasteRule($model, $subrule, null, $createdRuleId);
                    }

                    return true;
                }

                return pasteRule($this->model, $rule, $topicId);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateRule()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $post['params'] = str_replace(array("\r\n", "\r", "\n", "\t"), '', $post['params']);
                $params = json_decode($post['params'], true);
                return $this->model->updateRule((int)$post['ruleId'], $params);
            }
        );
    }

    public function actionUpdateRules()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $post['params'] = str_replace(array("\r\n", "\r", "\n", "\t"), '', $post['params']);
                $params = json_decode($post['params'], true);
                return $this->model->updateRules($params);
            }
        );
    }

    public function actionCreateRule()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $post['name'] = str_replace(array("\r\n", "\r", "\n", "\t"), '', $post['name']);
                return $this->model->createRule((int)$post['topicId'], $post['name']);
            }
        );
    }

    public function actionDeleteRule()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteRule((int)$post['topicId'], (int)$post['ruleId']);
            }
        );
    }

    public function actionDeleteSubRule()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteSubRule((int)$post['ruleId']);
            }
        );
    }

    public function actionCreatePattern()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                //$post['test'] = str_replace('\\', '\\\\', $post['test']);
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->createPattern((int)$post['ruleId'], $post['test'], $post['text']);
            }
        );
    }

    public function actionDeletePattern()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deletePattern((int)$post['patternId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeletePatterns()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->deletePatterns($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdatePattern()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                //$post['test'] = str_replace('\\', '\\\\', $post['test']);
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->updatePattern((int)$post['patternId'], $post['test'], $post['text']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdatePatternParams()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->updatePatternParams((int)$post['patternId'], $params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdatePatterns()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->updatePatterns($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionPastePatterns()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $ruleIdFromCopy = $post['ruleIdFromCopy'];
                $subRuleId      = $post['subRuleId'];
                $patternsIds    = json_decode($post['patternsIds'], true);

                $rule           = $this->model->getRawRule((int)$ruleIdFromCopy);
                $patterns       = $rule['rule']['patterns'];

                // Delete patterns which not in array and set new rule id
                foreach ($patterns as $key => $val) {
                    if (!in_array($patterns[$key]['pattern_id'], $patternsIds)) {
                        unset($patterns[$key]);
                    } else {
                        unset($patterns[$key]['pattern_id']);
                        $patterns[$key]['rule_id'] = $subRuleId;
                    }
                }

                // Create patterns
                if (!empty($patterns)) {
                    $patterns = array_values($patterns);
                    $this->model->createPatterns($patterns);
                }

                return true;
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionCreatePatternVariable()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->createPatternVariable((int)$post['patternId'], (int)$post['variableId'], (int)$post['regexpId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdatePatternVariable()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->updatePatternVariable((int)$post['patternVariableId'], (int)$post['patternId'], (int)$post['variableId'], (int)$post['regexpId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeletePatternVariable()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->deletePatternVariable((int)$post['patternVariableId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    //Exception patterns begin
    public function actionCreateExcPattern()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                return $this->model->createExcPattern((int)$post['ruleId'], $post['test'], $post['text']);
            }
        );
    }

    public function actionDeleteExcPattern()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteExcPattern((int)$post['patternId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeleteExcPatterns()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->deleteExcPatterns($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateExcPattern()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                return $this->model->updateExcPattern((int)$post['patternId'], $post['test'], $post['text']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateExcPatternParams()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->updateExcPatternParams((int)$post['patternId'], $params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateExcPatterns()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->updateExcPatterns($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }
    //Exception patterns end

    public function actionCreateTemplate()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->createTemplate((int)$post['ruleId'], $post['text']);
            }
        );
    }

    public function actionCreateTemplates()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->createTemplates($params);
            }
        );
    }

    public function actionDeleteTemplate()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                return $this->model->deleteTemplate((int)$post['templateId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionDeleteTemplates()
    {
        return $this->ajaxDeleteDataTemplate(
            function ($post) {
                $params = json_decode($post['params'], true);
                return $this->model->deleteTemplates($params);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionUpdateTemplate()
    {
        return $this->ajaxUpdateDataTemplate(
            function ($post) {
                //$post['text'] = str_replace('\\', '\\\\', $post['text']);
                return $this->model->updateTemplate((int)$post['templateId'], $post['text'], $post['timeslotId']);
            },
            self::RETURN_RAW_RESPONSE
        );
    }

    public function actionSubRule()
    {
        return $this->ajaxGetDataTemplate(
            function ($post) {
                if ($post['ruleId'] == 'null') {
                    return 'false';
                }
                $this->model->getSubRule((int)$post['ruleId']);
                $response = \Yii::$app->response;
                $response->format = \yii\web\Response::FORMAT_JSON;
                $response->data = [
                    'subrules_patterns' => $this->renderAjax('_subrules_patterns', ['model' => $this->model]),
                    'subrules_templates' => $this->renderAjax('_subrules_templates', ['model' => $this->model])
                ];
                return $response;
            }
        );
    }

    public function actionCreateSubRule()
    {
        return $this->ajaxCreateDataTemplate(
            function ($post) {
                $post['name'] = str_replace(array("\r\n", "\r", "\n", "\t"), '', $post['name']);
                return $this->model->createSubRule((int)$post['parentId'], $post['name']);
            }
        );
    }
}
