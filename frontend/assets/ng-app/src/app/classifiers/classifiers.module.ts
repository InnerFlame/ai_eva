import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { SharedModule }                  from "../shared";
import { ClassifiersComponent }          from "./classifiers.component";

const routing: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'classifiers-ng',
        component: ClassifiersComponent
    }
]);

@NgModule({
    imports: [
        routing,
        SharedModule
    ],
    declarations: [
        ClassifiersComponent
    ]
})

export class ClassifiersModule {}