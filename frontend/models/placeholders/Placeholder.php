<?php

namespace frontend\models\placeholders;

use yii\db\ActiveRecord;

class Placeholder extends ActiveRecord
{
    public static function tableName()
    {
        return 'placeholders_table';
    }

    public function getSenderTypes()
    {
        return $this->hasMany(SenderType::className(), ['placeholder_id' => 'id']);
    }

    public function getReceiverLocations()
    {
        return $this->hasMany(ReceiverLocation::className(), ['placeholder_id' => 'id']);
    }
}
