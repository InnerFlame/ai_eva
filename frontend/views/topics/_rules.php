<?php
use common\widgets\Column\Column;
use common\widgets\RadioButton\RadioButton;
use yii\bootstrap\Html;

$rulesItems = array();
foreach ($model->rules as $rule) {
    $item['label'] = $rule->getName();
    $item['labelOptions'] = ['name' => 'ruleName'];
    $item['hiddens'] = [
        ['name' => 'topicId', 'value' => $rule->getTopicId()],
        ['name' => 'ruleId', 'value' => $rule->getRuleId()],
        ['name' => 'rulePriority', 'value' => $rule->getPriority()],
    ];
    $item['header'] = [
        ['class' => 'glyphicon glyphicon-signal' . ($rule->isActive() ? ' active' : ''), 'name' => 'rulesActiveTest',],
        ['class' => 'glyphicon glyphicon-off' . ($rule->isEnabled() ? ' active' : ''), 'name' => 'rulesOnOff',],
        ['class' => 'glyphicon glyphicon-ok' . (($rule->getExcludeTemplate() == 1) ? ' active' : ''), 'name' => 'rulesExcInc',],
    ];
    $item['lines'] = [
        ['elements' => [
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                'activeText' => 'Active',
                'text' => 'Test',
                'glyphiconClass' => 'glyphicon-signal',
                'activeState' => $rule->isActive(),
                'name' => 'updateRulesActiveTestRadioButton',
            ])],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                'activeText' => 'On',
                'text' => 'Off',
                'glyphiconClass' => 'glyphicon-off',
                'activeState' => $rule->isEnabled(),
                'name' => 'updateRulesOnOffRadioButton',
            ])],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => RadioButton::widget([
                'activeText' => 'Exc',
                'text' => 'Inc',
                'glyphiconClass' => 'glyphicon-ok',
                'activeState' => ($rule->getExcludeTemplate() == 1),
                'name' => 'updateRulesExcIncRadioButton',
            ])],
        ]],
        ['elements' => [
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Copy', [
                'class' => 'btn btn-default btn-block',
                'name' => 'copyRuleButton',
                'type' => 'button',
            ])],
        ]],
        ['elements' => [['tag' => 'hr', 'options' => ['style' => 'border: 1px solid #fff']],]],
        ['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label'], 'content' => 'Name:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::input(
                'text',
                'name',
                $rule->getName(),
                ['class' => 'form-control']
            )],
        ]],
        ['elements' => [
            ['tag' => 'label', 'options' => ['class' => 'col-sm-8 control-label'], 'content' => 'Number of execution:'],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::input(
                'text',
                'executionLimit',
                $rule->getExecLimit() == null ? 0 : $rule->getExecLimit(),
                ['class' => 'form-control']
            )],
        ]],
        ['elements' => [
            ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                'class' => 'btn btn-danger btn-block',
                'name' => 'deleteRuleButton',
                'type' => 'button',
            ])],
            ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                'class' => 'btn btn-primary btn-block',
                'name' => 'updateRuleButton',
                'type' => 'button',
            ])],
        ]],
    ];
    $rulesItems[] = $item;
}

echo Column::widget([
    'listId' => 'rulesList',
    'items' => $rulesItems,
    'id' => 'w42-rules',
    'collapsed' => true,
    'lastItemText' => 'Add new rule...',
    'lastItemOptions' => [
        'name' => 'addNewRule',
        'data-toggle' => 'modal',
        'data-target' => '#addNewRuleModal',
    ],
]);
