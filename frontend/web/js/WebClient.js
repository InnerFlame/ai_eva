var WebClient = (function () {
    var instance;

    function init()
    {
        //Private methods and variables
        function executeParameter(parameter)
        {
            if (parameter === undefined) {
                return;
            }
            if (typeof parameter === "string") {
                alert(parameter);
            } else {
                parameter();
            }
        }

        return {
            //Public methods and variables, separated by ','
            get: function (url, data, successCallback, errorCallback, callback) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: data,
                    success: function (data) {
                        if (data == 'true') {
                            executeParameter(successCallback);
                        } else {
                            executeParameter(errorCallback);
                        }
                        executeParameter(callback);
                    }
                });
            }
        };
    };

    return {
        getInstance: function () {
            if ( !instance ) {
                instance = init();
            }
            return instance;
        }
    };
})();
