import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {HttpService} from "../shared/services/http.service";
import {
    ClassifierModel,
    TopicModel,
    ScenarioModel,
    TopicsGroupRouting
} from "../shared/models/topics.group.model";

@Component({
    selector: 'topics-group',
    templateUrl: './topics.group.html',
})

export class TopicsGroupComponent implements OnInit {

    private messageWrong: string = 'Wrong response from server!';
    private messageError: string = 'Error on server!';
    private messageDone: string = 'Action done!';

    public error: string = '';
    public success: string = '';

    public loading: boolean = true;

    public data: any = [];
    public models: any = [];
    public availableModels: any;
    public groupSelected: any = [];

    public classifiers: any = [];
    public classifier: ClassifierModel = new ClassifierModel();

    public topics: any = [];
    public topic: TopicModel = new TopicModel();

    public scenarios: any = [];
    public scenario: ScenarioModel = new ScenarioModel();

    constructor(
        private httpService: HttpService,
        private titleService: Title
    ) {
        this.titleService.setTitle('Topics Group');
    }

    ngOnInit(): void {
        this.getClassifiers();
        this.getTopicsGroup();
    }

    private getClassifiers(): void {
        this.httpService.get(TopicsGroupRouting.actionGetClassifiers()).then(data => {
            this.loading = true;
            if (!Object.keys(data).length) {
                this.error = this.messageWrong;
                this.loading = false;
                return;
            }

            this.classifiers = data['classifiers'];
            this.availableModels = data['availableModels'];

            this.models = this.availableModels['available_projects'][this.classifier.text] ?
                this.availableModels['available_projects'][this.classifier.text]['available_models'] : [];
        }, err => {
            this.error = this.messageError;
            this.loading = false;
        });
    }

    private getTopicsGroup(): void {
        this.httpService.get(TopicsGroupRouting.actionGetTopicsGroup()).then(data => {
            let dataWrap = [];
            this.loading = true;
            if (!Object.keys(data).length) {
                this.error = this.messageWrong;
                this.loading = false;
                return;
            }
            if (data) {
                let classifiers = this.classifiers;
                dataWrap = Object.keys(data).map(function (group_id) {
                    data[group_id]['group_topics_id'] = group_id;
                    Object.keys(classifiers).map(function (classifier) {
                        if (classifiers[classifier].group_topics_id && classifiers[classifier].group_topics_id == group_id) {
                            data[group_id]['trigger_threshold'] = classifiers[classifier]['trigger_threshold'];
                            data[group_id]['classifier_id'] = classifiers[classifier]['classifier_id'];
                            data[group_id]['model'] = classifiers[classifier]['model'];
                            data[group_id]['text'] = classifiers[classifier]['text'];
                        }
                    });
                    return data[group_id];
                });

                if (dataWrap[0]) {
                    this.classifier.load(dataWrap[0]);
                    this.topics = dataWrap[0]['topics'];
                    this.topic.load(this.topics[0]);
                    this.scenarios = dataWrap[0]['scenarios'];
                    this.scenario.load(this.scenarios[0]);
                    this.groupSelected = dataWrap[0];
                }
            }
            this.data = dataWrap;
            this.loading = false;
        }, err => {
            this.error = this.messageError;
            this.loading = false;
        });
    }

    public changeTopicGroup(data: any) {
        this.classifier.load(data);
        this.topics = data.topics;
        this.scenarios = data.scenarios;
        this.groupSelected = data;

        this.models = this.availableModels['available_projects'][this.classifier.text] ?
            this.availableModels['available_projects'][this.classifier.text]['available_models'] : [];
    }

    public createTopicGroup() {
        this.loading = true;
        this.httpService.post(TopicsGroupRouting.actionCreateTopicsGroup(), this.classifier).then(data => {
            if (!Object.keys(data).length || data[0].err) {
                this.error = this.messageWrong;
                this.loading = false;
                return;
            }
            this.classifier.classifier_id = data[0].item.item.classifier_id;
            this.classifier.trigger_threshold = data[0].item.item.trigger_threshold;

            this.success = this.messageDone;
            this.loading = false;
        }, err => {
            this.error = this.messageError;
            this.loading = false;
        });
    }

    public updateTopicGroup() {
        this.loading = true;
        this.httpService.post(TopicsGroupRouting.actionUpdateTopicsGroup(), this.classifier).then(data => {
            if (!Object.keys(data).length || !data.result || data.result[0].err) {
                this.error = this.messageWrong;
                this.loading = false;
                return;
            }
            location.reload();
            this.success = this.messageDone;
            this.loading = false;
        }, err => {
            this.error = this.messageError;
            this.loading = false;
        });
    }

    public trainTopicGroup() {
        this.loading = true;
        this.httpService.post(TopicsGroupRouting.actionTrainTopicsGroup(), this.classifier).then(data => {
            if (!Object.keys(data).length) {
                this.error = this.messageWrong;
                this.loading = false;
                return;
            }
            this.getAvailableModels(data);
            this.success = this.messageDone;
            this.loading = false;
        }, err => {
            this.error = this.messageError;
            this.loading = false;
        });
    }

    private getAvailableModels(data) {
        let models = data.result.models;
        for (let index in models) {
            if (models[index].indexOf(this.classifier.group_topics_id) != -1) {
                this.models.push({'name': models[index]});
            }
        }
    }
}
