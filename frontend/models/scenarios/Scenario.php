<?php
declare(strict_types=1);

namespace frontend\models\scenarios;

class Scenario
{
    private $id;
    private $nodes;
    private $flowChart;
    private $ts;
    private $name;
    private $maxStepCount;
    private $isBlock;
    public  $tags = [];

    public function __construct(int $id, $nodes, string $flowChart, string $ts, string $name, int $maxStepCount, int $isBlock)
    {
        $this->id = $id;
        $this->nodes = $nodes;
        $this->flowChart = $flowChart;
        $this->ts = $ts;
        $this->name = $name;
        $this->maxStepCount = $maxStepCount;
        $this->isBlock = $isBlock;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNodes()
    {
        return json_encode($this->nodes);
    }

    /**
     * @param string $nodes
     */
    public function setNodes($nodes)
    {
        $this->nodes = $nodes;
    }

    /**
     * @return string
     */
    public function getFlowChart() : string
    {
        return $this->flowChart;
    }

    /**
     * @param string $flowChart
     */
    public function setFlowChart(string $flowChart)
    {
        $this->flowChart = $flowChart;
    }

    /**
     * @return string
     */
    public function getTs() : string
    {
        return $this->ts;
    }

    /**
     * @param string $ts
     */
    public function setTs(string $ts)
    {
        $this->ts = $ts;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getMaxStepCount() : int
    {
        return $this->maxStepCount;
    }

    /**
     * @param int $id
     */
    public function setMaxStepCount(int $maxStepCount)
    {
        $this->maxStepCount = $maxStepCount;
    }

    public function getIsBlock() : int
    {
        return $this->isBlock;
    }

    public function setIsBlock(int $isBlock)
    {
        $this->isBlock = $isBlock;
    }
}
