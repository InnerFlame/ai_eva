<?php

namespace common\widgets\Column;

use yii\web\AssetBundle;

class ColumnAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/Column/assets';

    public $css = [
        'css/column.css',
    ];

    public $js = [
        'js/column.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        //'forceCopy'=>true,
    ];
}
