<?php
namespace common\components;

use frontend\models\usersId\CommunicationLog;
use yii\base\Component;

class CommunicationLogComponent extends Component
{
    public function saveToLog($data)
    {
        try {
            $communicationLog = new CommunicationLog();
            $communicationLog->profileType = $data['profileType'];
            $communicationLog->project = $data['project'] ?? '';
            $communicationLog->siteHash = $data['site'];
            $communicationLog->country = $data['country'];
            $communicationLog->language = $data['language'] ?? 'NaN';
            $communicationLog->status = $data['status'];
            $communicationLog->platform = $data['platform'];
            $communicationLog->trafficSource = $data['trafficSource'];
            $communicationLog->scenarioId = $data['scenario'];
            $communicationLog->placeholderId = $data['placeholder'] ?? 0;
            $communicationLog->splitId = $data['split'] ?? 0;
            $communicationLog->userId = $data['userId'];
            $communicationLog->modelId = $data['modelId'];
            $communicationLog->isUser = !empty($data['isUser']);
            $communicationLog->message = $data['message'];
            $communicationLog->hasSwitching = !empty($data['hasSwitching']);
            $communicationLog->hasRule = !empty($data['hasRule']);
            $communicationLog->screenname = $data['screenname'] ?? '';
            $communicationLog->predispatched = $data['predispatched'] ?? false;
            $communicationLog->save();
        } catch (\Exception $e) {
            \Yii::warning(var_export($data, true));
            throw $e;
        }
    }
}
