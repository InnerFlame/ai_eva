<?php
declare(strict_types=1);

namespace frontend\models\substitutes;

class Substitute
{
    private $id;
    private $phrase;
    private $deleted;
    private $version;

    public function __construct(int $id, string $phrase, bool $deleted, int $version)
    {
        $this->id = $id;
        $this->phrase = $phrase;
        $this->deleted = $deleted;
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPhrase() : string
    {
        return $this->phrase;
    }

    /**
     * @param string $phrase
     */
    public function setPhrase(string $phrase)
    {
        $this->phrase = $phrase;
    }

    /**
     * @return boolean
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }
}
