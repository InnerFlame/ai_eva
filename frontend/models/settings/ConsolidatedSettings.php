<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class ConsolidatedSettings extends ActiveRecord
{
    public static function tableName()
    {
        return 'consolidatedSettings';
    }
}
