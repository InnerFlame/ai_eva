<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_platforms_table`.
 */
class m160622_142325_create_settings_platforms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_platforms_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'platform_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_platforms_table');
    }
}
