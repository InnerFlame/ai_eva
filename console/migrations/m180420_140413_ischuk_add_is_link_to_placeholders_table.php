<?php

use yii\db\Migration;

class m180420_140413_ischuk_add_is_link_to_placeholders_table extends Migration
{
    public function up()
    {
        $this->addColumn('placeholders_table', 'is_link', 'tinyint(1)');
    }

    public function down()
    {
        $this->dropColumn('placeholders_table', 'is_link');
    }
}
