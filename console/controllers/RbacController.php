<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = \Yii::$app->authManager;
        if (!empty($authManager->getRoles())) {
            return;
        }

        //Add permissions
        $topics = $authManager->createPermission('topics');
        $topics->description = 'Work with topics page';
        $authManager->add($topics);

        $scenarios = $authManager->createPermission('scenarios');
        $scenarios->description = 'Work with scenarios page';
        $authManager->add($scenarios);

        $substitutes = $authManager->createPermission('substitutes');
        $substitutes->description = 'Work with substitutes page';
        $authManager->add($substitutes);

        $variables = $authManager->createPermission('variables');
        $variables->description = 'Work with variables page';
        $authManager->add($variables);

        $readSettings = $authManager->createPermission('readSettings');
        $readSettings->description = 'Read settings';
        $authManager->add($readSettings);

        $editSettings = $authManager->createPermission('editSettings');
        $editSettings->description = 'Edit settings';
        $authManager->add($editSettings);

        $readPlaceholders = $authManager->createPermission('readPlaceholders');
        $readPlaceholders->description = 'Read placeholders';
        $authManager->add($readPlaceholders);

        $editPlaceholders = $authManager->createPermission('editPlaceholders');
        $editPlaceholders->description = 'Edit placeholders';
        $authManager->add($editPlaceholders);

        $testChat = $authManager->createPermission('testChat');
        $testChat->description = 'Work with test chat page';
        $authManager->add($testChat);

        $commonInfo = $authManager->createPermission('commonInfo');
        $commonInfo->description = 'Work with common info page';
        $authManager->add($commonInfo);

        $usersId = $authManager->createPermission('usersId');
        $usersId->description = 'Work with users id page';
        $authManager->add($usersId);

        $search = $authManager->createPermission('search');
        $search->description = 'Work with search page';
        $authManager->add($search);

        $access = $authManager->createPermission('access');
        $access->description = 'Work with access page';
        $authManager->add($access);

        //Add role admin
        $admin = $authManager->createRole('admin');
        $authManager->add($admin);
        $authManager->addChild($admin, $topics);
        $authManager->addChild($admin, $scenarios);
        $authManager->addChild($admin, $substitutes);
        $authManager->addChild($admin, $variables);
        $authManager->addChild($admin, $readSettings);
        $authManager->addChild($admin, $editSettings);
        $authManager->addChild($admin, $readPlaceholders);
        $authManager->addChild($admin, $editPlaceholders);
        $authManager->addChild($admin, $testChat);
        $authManager->addChild($admin, $commonInfo);
        $authManager->addChild($admin, $usersId);
        $authManager->addChild($admin, $search);
        $authManager->addChild($admin, $access);

        //Add role content manager
        $contentManager = $authManager->createRole('contentManager');
        $authManager->add($contentManager);
        $authManager->addChild($contentManager, $topics);
        $authManager->addChild($contentManager, $scenarios);
        $authManager->addChild($contentManager, $substitutes);
        $authManager->addChild($contentManager, $testChat);
        $authManager->addChild($contentManager, $usersId);
        $authManager->addChild($contentManager, $search);

        //Add role QA manager
        $qaManager = $authManager->createRole('qaManager');
        $authManager->add($qaManager);
        $authManager->addChild($qaManager, $readSettings);
        $authManager->addChild($qaManager, $readPlaceholders);
        $authManager->addChild($qaManager, $testChat);
        $authManager->addChild($qaManager, $usersId);

        //Add role analyst
        $analyst = $authManager->createRole('analyst');
        $authManager->add($analyst);
        $authManager->addChild($analyst, $commonInfo);
        $authManager->addChild($analyst, $usersId);

        //Assign roles to userIds
        $authManager->assign($admin, 1);
    }
}
