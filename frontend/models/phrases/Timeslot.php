<?php
declare(strict_types=1);

namespace frontend\models\phrases;

class Timeslot
{
    private $id;
    private $date1;
    private $time1;
    private $name;
    private $date2;
    private $time2;

    /**
     * Timeslot constructor.
     * @param $id
     * @param $date1
     * @param $time1
     * @param $name
     * @param $date2
     * @param $time2
     */
    public function __construct(int $id, string $date1, string $time1, string $name, string $date2, string $time2)
    {
        $this->id = $id;
        $this->date1 = $date1;
        $this->time1 = $time1;
        $this->name = $name;
        $this->date2 = $date2;
        $this->time2 = $time2;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDate1() : string
    {
        return $this->date1;
    }

    /**
     * @param string $date1
     */
    public function setDate1(string $date1)
    {
        $this->date1 = $date1;
    }

    /**
     * @return string
     */
    public function getTime1() : string
    {
        return $this->time1;
    }

    /**
     * @param string $time1
     */
    public function setTime1(string $time1)
    {
        $this->time1 = $time1;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDate2() : string
    {
        return $this->date2;
    }

    /**
     * @param string $date2
     */
    public function setDate2(string $date2)
    {
        $this->date2 = $date2;
    }

    /**
     * @return string
     */
    public function getTime2() : string
    {
        return $this->time2;
    }

    /**
     * @param string $time2
     */
    public function setTime2(string $time2)
    {
        $this->time2 = $time2;
    }
}
