<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'action'        => ['index'],
    'method'        => 'get',
    'id'            => 'usersIdSearch',
    'options'       => [
        'class'         => 'form-horizontal',
        'data-pjax'     => true,
    ],
    'fieldConfig'   => [
        'template'      => "{label}<div class='col-sm-5'>{input}</div>",
        'labelOptions'  => ['class' => 'col-sm-1 control-label',],
        'inputOptions'  => ['class' => 'form-control',],
    ],
]); ?>

<?= $form->field($model, 'sender'); ?>
<?= $form->field($model, 'receiver'); ?>
<?= $form->field($model, 'scenario')->dropDownList($model->availableScenarios); ?>
<?= $form->field($model, 'placeholder')->dropDownList($model->availablePlaceholders); ?>
<?= $form->field($model, 'keywords'); ?>
<?= $form->field($model, 'modelKeywords'); ?>

<div class="form-group">
    <div class="col-sm-offset-1 col-sm-5">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end();?>
