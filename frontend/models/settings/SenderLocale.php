<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class SenderLocale extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_sender_locales';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
