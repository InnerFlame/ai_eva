<?php

namespace common\components;

use yii\base\Component;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class AmqpComponent extends Component
{
    /** @var AMQPStreamConnection */
    private $connection = null;

    /** @var AMQPChannel[] */
    private $channels = [];

    public $host;
    public $port;
    public $user;
    public $pass;
    public $vhost;
    public $prefix = '';

    public function init()
    {
        $this->connection = new AMQPStreamConnection(
            $this->host,
            $this->port,
            $this->user,
            $this->pass,
            $this->vhost
        );
    }

    /**
     * Publish events to queue
     *
     * @param string $queue - queue name
     * @param string $data - array of data
     * @param boolean $autoDelete
     * @param string $replyTo
     * @return integer - correlation id
     */
    public function publishEvent($queue, $data, $autoDelete = false, $replyTo = '')
    {
        try {
            $exchange = $this->prefix . $queue . '.exchange';
            //TODO: need to expire channels in 5 min
            if ( empty($this->channels[$queue])) {
                $channelId = mt_rand(1, 65535);
                $channel = $this->connection->channel($channelId);
                $channel->queue_declare($this->prefix . $queue, false, true, false, $autoDelete);
                $channel->exchange_declare($exchange, 'direct', false, true, $autoDelete);
                $channel->queue_bind($this->prefix . $queue, $exchange);

                $this->channels[$queue] = $channel;
            }

            //Publish
            $correlationId = md5(microtime());
            $json = json_encode($data);

            $message = new AMQPMessage(
                $json,
                [
                    'content_type'   => 'text/plain',
                    'delivery_mode'  => AMQPMessage::DELIVERY_MODE_PERSISTENT,
                    'reply_to'       => $replyTo,
                    'correlation_id' => $correlationId,
                ]
            );
            $this->channels[$queue]->basic_publish($message, $exchange);

            return $correlationId;
        } catch (\PhpAmqpLib\Exception\AMQPProtocolChannelException $e) {
            throw $e;
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage());
        }

        return false;
    }

    public function markChannelAsInactive($queue)
    {
        if (!empty($this->channels[$queue])) {
            $this->channels[$queue]->close();
        }
        unset($this->channels[$queue]);

        $cacheKey = 'channel_id_' . $queue;
        \Yii::$app->cache->delete($cacheKey);
    }

    public function getMessageData($amqpMessage)
    {
        if (empty($amqpMessage) || empty($amqpMessage->body)) {
            \Yii::warning('Empty message');
            return false;
        }

        $message = json_decode($amqpMessage->body, true);

        if (empty($message)) {
            \Yii::warning('Empty incoming data');
            return false;
        }

        return $message;
    }

    public function ackMessage($amqpMessage)
    {
        $amqpMessage->delivery_info['channel']->basic_ack($amqpMessage->delivery_info['delivery_tag']);
    }

    /**
     * Listen to the queue and process messages in callback
     *
     * @param null $queue
     * @param callable $consumerCallback - callback to process events
     * @param boolean $autoDelete
     * @return null
     */
    public function consumeEvents($queue, $consumerCallback, $autoDelete = false)
    {
        $this->basicConsume(
            $queue,
            $consumerCallback,
            null,
            null,
            $autoDelete
        );
    }

    /**
     * Consume events from needle queue
     *
     * @param string    $queue
     * @param string    $exchange
     * @param string    $exchangeType
     * @param callable  $consumerCallback
     * @param boolean   $autoDelete
     * @return null
     */
    private function basicConsume($queue, $consumerCallback, $exchange = null, $exchangeType = null, $autoDelete = false)
    {
        $queue = $this->prefix . $queue;
        //Consume
        $channel = $this->connection->channel(mt_rand(1, 65535));
        $channel->queue_declare($queue, false, true, false, $autoDelete);
        if (!empty($exchange)) {
            $channel->exchange_declare($exchange, $exchangeType, false, true, $autoDelete);
            $channel->queue_bind($queue, $exchange);
        }
        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queue, '', false, false, false, false, $consumerCallback);

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
    }

    /**
     * close all channel and rabbitMQ connection
     */
    public function closeConnection()
    {
        foreach ($this->channels as $channel) {
            $channel->close();
        }

        $this->connection->close();

    }
}