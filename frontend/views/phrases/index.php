<?php
use common\widgets\BindableForm\BindableForm;
use common\widgets\StagesFindReplacer\StagesFindReplacer;
use common\widgets\Modal\Modal;
use yii\bootstrap\Html;
use frontend\assets\PhrasesAsset;

PhrasesAsset::register($this);

//Modal confirm delete group phrases
echo Modal::widget([
    'id' => 'confirmDeleteGroupPhrasesModal',
    'title' => 'Delete stages',
    'nameText' => 'stages',
    'isMultilineQuestion' => true,
    'nameId' => 'deleteGroupPhrasesNames',
    'buttonId' => 'confirmDeleteGroupPhrasesButton'
]);

//Modal confirm delete phrases
echo Modal::widget([
    'id' => 'confirmDeletePhrasesModal',
    'title' => 'Delete phrases',
    'nameText' => 'phrases',
    'isMultilineQuestion' => true,
    'nameId' => 'deletePhrasesNames',
    'buttonId' => 'confirmDeletePhrasesButton'
]);

// Available language tags
$availableLanguageTags = '<ul class="dropdown-menu dropdown-menu-right doNotStopPropagation">';
foreach (\Yii::$app->params['languageTags'] as $value) {
    $availableLanguageTags .= '<li><a href="javascript:void(0);">';
    $availableLanguageTags .= $value;
    $availableLanguageTags .= '</a><input type="hidden" value="';
    $availableLanguageTags .= $value;
    $availableLanguageTags .= '"></li>';
}
$availableLanguageTags .= '</ul>';

// Available tags
$availableTags = '<ul class="dropdown-menu">';
foreach ($model->availableTags as $key => $value) {
    $availableTags .= '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="..." ';
    $availableTags .= '>';
    $availableTags .= $value;
    $availableTags .= '</a><input type="hidden" value="';
    $availableTags .= $key;
    $availableTags .= '"></li>';
}
$availableTags .= '</ul>';
?>

<input type="hidden" value="<?= $model->currentGroupGroupPhrasesId ?>" id="currentGroupGroupPhrasesId"/>
<input type="hidden" value="<?= $model->currentGroupPhrasesId ?>" id="currentGroupPhrasesId"/>
<input type="hidden" value="<?= $model->currentPhrasesId ?>" id="currentPhrasesId"/>

<!-- GROUP GROUPS PHRASES CONTAINER -->
<div class="col-md-3" id="groupGroupsPhrasesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a href="javascript:void(0);">Group phrases</a></li>
    </ul>
    <?php
    echo BindableForm::widget([
        'id' => 'groupGroupsPhrasesBindableForm',
        'hiddens' => [
            ['name' => 'groupGroupsPhrasesId'],
        ],
        'lines' => [
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                    'content' => Html::input('text', 'itemText', '', [
                        'class' => 'form-control',
                        'id' => 'groupGroupsPhrasesText',
                    ])
                ],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                    'class' => 'btn btn-danger btn-block',
                    'type' => 'button',
                    'id' => 'deleteGroupGroupsPhrasesButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Add', [
                    'class' => 'btn btn-success btn-block',
                    'type' => 'button',
                    'id' => 'addNewGroupGroupsPhrasesButton',
                ])],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Update', [
                    'class' => 'btn btn-primary btn-block',
                    'type' => 'button',
                    'id' => 'updateGroupGroupsPhrasesButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Copy', [
                    'class' => 'btn btn-default btn-block',
                    'type' => 'button',
                    'id' => 'copyGroupGroupsPhrasesButton',
                ])],
            ]],
        ],
    ]);
    ?>
    <div id="groupGroupsPhrases"></div>
</div>
<!-- GROUP PHRASES CONTAINER -->
<div class="col-md-3" id="groupPhrasesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a href="javascript:void(0);">Stages</a></li>
    </ul>
    <?php
    echo BindableForm::widget([
        'id' => 'groupPhrasesBindableForm',
        'hiddens' => [
            ['name' => 'groupPhrasesId'],
            ['name' => 'groupGroupsPhrasesId'],
        ],
        'lines' => [
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                    'content' => Html::textarea('itemText', '', [
                        'class' => 'form-control',
                        'rows' => '5',
                        'id' => 'groupPhrasesText',
                        'style' => 'resize:vertical;'
                    ])
                ],
            ]],
            ['line' => [
                ['tag' => 'label', 'options' => ['class' => 'col-sm-6 control-label'], 'content' => 'Language:'],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-6', 'name' => 'topicLanguage'], 'content' =>
                    Html::button(
                        Html::tag('span', '', [
                            'class' => 'pull-left',
                            'id' => 'currentGroupPhrasesLanguageText',
                            'name' => 'currentGroupPhrasesLanguageText',
                        ])
                        . Html::hiddenInput(
                            'currentGroupPhrasesLanguage',
                            '',
                            ['id' => 'currentGroupPhrasesLanguage',]
                        )
                        . Html::tag('span', '', ['class' => 'caret']),
                        [
                            'class' => 'btn btn-default dropdown-toggle btn-block',
                            'aria-expanded' => 'false',
                            'aria-haspopup' => 'true',
                            'data-toggle' => 'dropdown',
                            'type' => 'button',
                            'style' => 'text-align: right',
                        ]
                    )
                    . $availableLanguageTags
                ],
            ]],
            ['line' => [
                ['tag' => 'label', 'options' => ['class' => 'col-sm-6 control-label'], 'content' => 'Tags:'],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-6', 'name' => 'groupPhrasesTagsButton'], 'content' =>
                    Html::button(
                        Html::tag('span', 'Tags', ['class' => 'pull-left'])
                        . Html::tag('span', '', ['class' => 'caret']),
                        [
                            'class' => 'btn btn-default dropdown-toggle btn-block',
                            'aria-expanded' => 'false',
                            'aria-haspopup' => 'true',
                            'data-toggle' => 'dropdown',
                            'type' => 'button',
                        ]
                    )
                    . $availableTags
                ],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Delete', [
                    'class' => 'btn btn-danger btn-block',
                    'type' => 'button',
                    'id' => 'deleteGroupPhrasesButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-8'], 'content' => Html::button('Add', [
                    'class' => 'btn btn-success btn-block',
                    'type' => 'button',
                    'id' => 'addNewGroupPhrasesButton',
                ])],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Check all', [
                    'class' => 'btn btn-default btn-block',
                    'type' => 'button',
                    'id' => 'checkAllGroupPhrasesButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Update', [
                    'class' => 'btn btn-primary btn-block',
                    'type' => 'button',
                    'id' => 'updateGroupPhrasesButton',
                ])],
            ]],
        ],
    ]);
    ?>
    <?= StagesFindReplacer::widget(); ?>
<div id="groupPhrases"></div>
</div>
<!-- PHRASES CONTAINER -->
<div class="col-md-6" id="phrasesContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a href="javascript:void(0);">Phrases</a></li>
    </ul>
    <?php
    //If hidden input fields have the same name and is in form
    //Its value became empty in page
    //That is why its name is null in availableDelays and availableTimeslots
    //Get available delays
    $delaysItemsContent = Html::beginTag('ul', [
        'class' => 'dropdown-menu dropdown-menu-right',
        'id' => 'availableDelays',
    ]);
    $delaysItemsContent .= Html::beginTag('li');
    $delaysItemsContent .= Html::a('No delays', 'javascript:void(0);');
    $delaysItemsContent .= Html::hiddenInput(null, \frontend\models\DataProvider::DEFAULT_ID);
    $delaysItemsContent .= Html::endTag('li');
    foreach ($model->delays as $delay) {
        $delaysItemsContent .= Html::beginTag('li');
        $delaysItemsContent .= Html::a($delay->getName(), 'javascript:void(0);');
        $delaysItemsContent .= Html::hiddenInput(null, $delay->getId());
        $delaysItemsContent .= Html::endTag('li');
    }
    $delaysItemsContent .= Html::endTag('ul');
    //Get available timeslots
    $timeslotsItemsContent = Html::beginTag('ul', [
        'class' => 'dropdown-menu dropdown-menu-right',
        'id' => 'availableTimeslots',
    ]);
    $timeslotsItemsContent .= Html::beginTag('li');
    $timeslotsItemsContent .= Html::a('No timeslots', 'javascript:void(0);');
    $timeslotsItemsContent .= Html::hiddenInput(null, \frontend\models\DataProvider::DEFAULT_ID);
    $timeslotsItemsContent .= Html::endTag('li');
    foreach ($model->timeslots as $timeslot) {
        $timeslotsItemsContent .= Html::beginTag('li');
        $timeslotsItemsContent .= Html::a($timeslot->getName(), 'javascript:void(0);');
        $timeslotsItemsContent .= Html::hiddenInput(null, $timeslot->getId());
        $timeslotsItemsContent .= Html::endTag('li');
    }
    $timeslotsItemsContent .= Html::endTag('ul');

    echo BindableForm::widget([
        'id' => 'phrasesBindableForm',
        'hiddens' => [
            ['name' => 'phraseId'],
        ],
        'lines' => [
            ['line' => [
                [
                    'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                    'content' => Html::textarea('itemText', '', [
                        'class' => 'form-control',
                        'rows' => '5',
                        'id' => 'phraseText',
                        'style' => 'resize:vertical;'
                    ])
                ],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' =>
                    Html::button(
                        Html::tag('span', '', ['class' => 'pull-left', 'id' => 'currentDelayText',])
                        . Html::hiddenInput(
                            'delayId',
                            \frontend\models\DataProvider::DEFAULT_ID,
                            ['id' => 'currentDelayId',]
                        )
                        . Html::tag('span', '', ['class' => 'caret']),
                        [
                            'class' => 'btn btn-default dropdown-toggle btn-block',
                            'aria-expanded' => 'false',
                            'aria-haspopup' => 'true',
                            'data-toggle' => 'dropdown',
                            'type' => 'button',
                            'style' => 'text-align: right',
                        ]
                    )
                    . $delaysItemsContent
                ],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' =>
                    Html::button(
                        Html::tag('span', '', ['class' => 'pull-left', 'id' => 'currentTimeslotText',])
                        . Html::hiddenInput(
                            'timeslotId',
                            \frontend\models\DataProvider::DEFAULT_ID,
                            ['id' => 'currentTimeslotId',]
                        )
                        . Html::tag('span', '', ['class' => 'caret']),
                        [
                            'class' => 'btn btn-default dropdown-toggle btn-block',
                            'aria-expanded' => 'false',
                            'aria-haspopup' => 'true',
                            'data-toggle' => 'dropdown',
                            'type' => 'button',
                            'style' => 'text-align: right',
                        ]
                    )
                    . $timeslotsItemsContent
                ],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-4'], 'content' => Html::button('Check all', [
                    'class' => 'btn btn-default btn-block',
                    'type' => 'button',
                    'id' => 'checkAllPhrasesButton',
                ])],
            ]],
            ['line' => [
                ['tag' => 'div', 'options' => ['class' => 'col-sm-3'], 'content' => Html::button('Delete', [
                    'class' => 'btn btn-danger btn-block',
                    'type' => 'button',
                    'id' => 'deletePhraseButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-3'], 'content' => Html::button('Add', [
                    'class' => 'btn btn-success btn-block',
                    'type' => 'button',
                    'id' => 'addNewPhraseButton',
                ])],
                ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Update', [
                    'class' => 'btn btn-primary btn-block',
                    'type' => 'button',
                    'id' => 'updatePhraseButton',
                ])],
            ]],
        ],
    ]);
    ?>
    <div id="phrases"></div>
</div>

<div id="pre-loader" class="col-md-12 text-center" hidden>
    <img src="/build/assets/pre-loader.gif">
</div>