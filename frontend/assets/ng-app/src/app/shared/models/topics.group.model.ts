export class ClassifierModel {
    constructor(
        public classifier_id: number = 0,
        public text: string = '',
        public deleted: number = 0,
        public version: number = 0,
        public type: string = 'rule_ml_classifier',
        public model: string = '',
        public active: number = 0,
        public group_topics_id: string = '',
        public trigger_threshold: number = 0,
    ) {
    }

    public load(data: any): void {
        this.classifier_id = data.classifier_id;
        this.text = data.text;
        this.deleted = data.deleted;
        this.version = data.version;
        this.model = data.model;
        this.active = data.active;
        this.group_topics_id = data.group_topics_id;
        this.trigger_threshold = data.trigger_threshold;
    }
}

export class TopicModel {
    constructor(
        public topic_id = 0,
        public name = '',
    ) {
    }

    public reset(): void {
        this.topic_id = 0;
        this.name = '';
    }

    public load(topic: any): void {
        this.topic_id = topic.topic_id;
        this.name = topic.name;
    }
}

export class ScenarioModel {
    constructor(
        public scenario_id = 0,
        public name = '',
    ) {
    }

    public reset(): void {
        this.scenario_id = 0;
        this.name = '';
    }

    public load(scenario: any): void {
        this.scenario_id = scenario.scenario_id;
        this.name = scenario.name;
    }
}

export class TopicsGroupRouting {
    private static baseUrl = '/topics-group-ng/';

    public static actionGetTopicsGroup(): string {
        return this.baseUrl + 'get-topics-group';
    }

    public static actionCreateTopicsGroup(): string {
        return this.baseUrl + 'create-topics-group';
    }

    public static actionUpdateTopicsGroup(): string {
        return this.baseUrl + 'update-topics-group';
    }

    public static actionTrainTopicsGroup(): string {
        return this.baseUrl + 'train-topics-group';
    }

    public static actionGetClassifiers(): string {
        return this.baseUrl + 'get-classifiers';
    }
}


