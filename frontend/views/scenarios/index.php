<?php
use common\widgets\BindableForm\BindableForm;
use common\widgets\Modal\Modal;
use yii\bootstrap\Html;
use frontend\assets\ScenariosAsset;

ScenariosAsset::register($this);

//Modal add new scenario
echo Modal::widget([
    'id' => 'addNewScenarioModal',
    'title' => 'Add new scenario',
    'inputId' => 'addNewScenarioName',
    'inputLabelText' => 'Scenario name:',
    'buttonId' => 'addNewScenarioButton',
    'buttonText' => 'Add new scenario',
    'hasForm' => true
]);

//Modal add new block
echo Modal::widget([
    'id'                => 'addNewBlockModal',
    'title'             => 'Add new block',
    'inputId'           => 'addNewBlockName',
    'inputLabelText'    => 'Block name:',
    'buttonId'          => 'addNewBlockButton',
    'buttonText'        => 'Add new block',
    'hasForm'           => true
]);

//Modal confirm delete scenario
echo Modal::widget([
    'id'                    => 'confirmDeleteScenariosModal',
    'title'                 => 'Delete scenarios',
    'nameText'              => 'scenarios',
    'isMultilineQuestion'   => true,
    'nameId'                => 'deleteScenariosNames',
    'buttonId'              => 'confirmDeleteScenariosButton'
]);

//Modal copy scenario
echo Modal::widget([
    'id' => 'confirmCopyScenarioModal',
    'title' => 'Copy scenario',
    'hiddenInputIds' => ['copyScenarioNodes', 'copyScenarioFlowChart',],
    'inputId' => 'copyScenarioName',
    'inputLabelText' => 'Scenario name:',
    'buttonId' => 'confirmCopyScenarioButton',
    'buttonText' => 'Copy scenario',
    'hasForm' => true
]);

$tagsOfCurrentScenario = '<ul class="dropdown-menu">';
foreach ($model->availableTags as $key => $value) {
    $tagsOfCurrentScenario .= '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="..." ';
    $tagsOfCurrentScenario .= '>';
    $tagsOfCurrentScenario .= $value;
    $tagsOfCurrentScenario .= '</a><input type="hidden" value="';
    $tagsOfCurrentScenario .= $key;
    $tagsOfCurrentScenario .= '"></li>';
}
$tagsOfCurrentScenario .= '</ul>';
?>
<!-- SCENARIOS AND GROUPS CONTAINER -->
<input type="hidden" value="" id="currentScenarioId"/>
<div class="col-md-2 collapsable-container" id="scenariosGroupsContainer">
    <ul class="nav nav-tabs" style="margin-bottom: 10px">
        <li class="active"><a data-toggle="tab" href="#scenariosContainer">Scenarios</a></li>
        <li><a data-toggle="tab" href="#groupsContainer">Groups</a></li>
    </ul>
    <div class="tab-content">
        <!-- SCENARIOS -->
        <div id="scenariosContainer" class="tab-pane active">
            <?php
            echo BindableForm::widget([
                'id' => 'scenarioBindableForm',
                'hiddens' => [
                    ['name' => 'scenarioId'],
                    ['name' => 'scenarioNodes'],
                    ['id'   => 'scenarioType', 'value' => 'scenario'],
                ],
                'lines' => [
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-12'],
                            'content' => Html::input('text', 'itemText', '', [
                                'class' => 'form-control',
                                'id' => 'scenarioText',
                            ])
                        ],
                    ]],
                    ['line' => [
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-6'],
                            'content' => Html::label( 'Max steps:', 'scenarioText', [
                                'class' => 'labelForInput',
                            ])
                        ],
                        [
                            'tag' => 'div', 'options' => ['class' => 'col-sm-6'],
                            'content' => Html::input('text', 'itemMaxStepCount', '', [
                                'class' => 'form-control',
                                'id' => 'scenarioMaxStepCount',
                            ])
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'label', 'options' => ['class' => 'col-sm-4 control-label scenarioItem'], 'content' => 'Tags:'],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-8 scenarioItem', 'name' => 'scenarioTagsButton'], 'content' =>
                            Html::button(
                                Html::tag('span', 'Tags', ['class' => 'pull-left'])
                                . Html::tag('span', '', ['class' => 'caret']),
                                [
                                    'class' => 'btn btn-default dropdown-toggle btn-block',
                                    'aria-expanded' => 'false',
                                    'aria-haspopup' => 'true',
                                    'data-toggle' => 'dropdown',
                                    'type' => 'button',
                                ]
                            )
                            . $tagsOfCurrentScenario
                        ],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button(
                            '<span class="glyphicon glyphicon-trash"></span> Delete',
                            [
                                'class' => 'btn btn-danger btn-block',
                                'type' => 'button',
                                'id' => 'deleteScenarioButton',
                            ]
                        )],
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-6'], 'content' => Html::button('Copy', [
                            'class' => 'btn btn-default btn-block',
                            'type' => 'button',
                            'id' => 'copyScenarioButton',
                        ])],
                    ]],
                    ['line' => [
                        ['tag' => 'div', 'options' => ['class' => 'col-sm-12'], 'content' => Html::button(
                            '<span class="glyphicon glyphicon-refresh"></span> Update',
                            [
                                'class' => 'btn btn-primary btn-block',
                                'type' => 'button',
                                'id' => 'updateScenarioButton',
                            ]
                        )],
                    ]],
                ],
            ]);
            ?>
            <br/>

            <input class="form-control scenariosLivesearch" type="text" placeholder="Search"/>
            <br/>

            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default active">
                    <input type="radio" name="scenarioBlockRadio" value="radioScenarios">Scenarios
                </label>
                <label class="btn btn-default">
                    <input type="radio" name="scenarioBlockRadio" value="radioBlocks">Blocks
                </label>
            </div>

            <div id="scenarios"></div>
        </div>
        <!-- GROUPS -->
        <div id="groupsContainer" class="tab-pane">
            <input class="form-control groupsLivesearch" type="text" placeholder="Search"/>
            <div id="groups"></div>
        </div>
    </div>
</div>
<!-- SCENARIOS CONTAINER -->
<div id="buttons" style="float: right;">
    <button type="button" class="btn btn-default toggle-scenarioPhrasesClassifiersContainer">Hide/Show phrases blocks <span class="glyphicon glyphicon-eye-open"></span></button>
    <button type="button" class="btn btn-default toggle-scenarioBlocksContainer">Hide/Show scenario blocks <span class="glyphicon glyphicon-eye-open"></span></button>
</div>
<div class="col-md-2" id="scenarioPhrasesClassifiersContainer">
    <div style="height: 50%;">
        <form role="form" style="height: 15%;">
            <div class="form-group">
                <input type="text" class="form-control" id="filterPalettePhrases" placeholder="Filter phrases">
            </div>
        </form>
        <div id="palettePhrases" style="height: 85%;"></div>
    </div>
    <div style="height: 10px"></div>
    <div id="paletteClassifiers" style="height: 50%;"></div>
</div>
<!-- SCENARIOS CONTAINER -->
<div class="col-md-6" id="scenarioDiagram" style="margin-top: 3%;">
</div>
<!-- SCENARIOS CONTAINER -->
<div class="col-md-2" id="scenarioBlocksContainer" style="margin-top: 2.6%;">
    <div id="paletteBlocks" style="height: 50%;"></div>
    <div style="height: 10px"></div>
    <div id="paletteNlpClassifiers" style="height: 50%;"></div>
</div>

