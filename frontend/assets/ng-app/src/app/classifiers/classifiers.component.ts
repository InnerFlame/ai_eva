import { Component }   from '@angular/core';
import { Title }       from "@angular/platform-browser";
import { HttpService } from "../shared/services/http.service";
import { OnInit }      from '@angular/core';
import {
    ClassifiersModel,
    PatternsModel,
    ClassifiersRouting
} from "../shared/models/classifiers.model";

@Component({
    selector    : 'classifiers',
    templateUrl : './classifiers.html',
})

export class ClassifiersComponent implements OnInit
{
    public errorText            : string  = '';
    public successText          : string  = '';
    public displayLoadingBlock  : boolean = true;
    public displayTestBlock     : boolean = false;
    public disableListGroups    : boolean = false;
    public displayModalLoader   : boolean = false;
    public classifierModel      : ClassifiersModel = new ClassifiersModel();
    public patternModel         : PatternsModel = new PatternsModel();
    public checkedPatterns      : any = [];
    public availableClassifiers : any;
    public availablePatterns    : any;
    public activeClassifierId   : number;

    /**
     * Constructor for class TestPhrasesComponent
     *
     * @param {HttpService} httpService
     * @param titleService
     */
    constructor(private httpService: HttpService, private titleService: Title)
    {
        this.titleService.setTitle( 'Classifiers' );
    }

    /**
     * Init actions
     */
    ngOnInit(): void
    {
        this.httpService.get(ClassifiersRouting.getClassifiersUrl()).then(data => {
            if (!data || !Object.keys(data).length) {
                this.displayLoadingBlock = false;
                this.errorText = 'Troubles with classifiers response or empty classifiers data!';
                return;
            }

            if (data[0]) {
                this.classifierModel.load(
                    data[0].classifier_id,
                    data[0].text,
                    data[0].deleted,
                    data[0].version
                );
                this.getClassifierPatternsData(data[0].classifier_id);
            }

            this.availableClassifiers = data;
            this.displayLoadingBlock = false;
        }, err => {
            this.errorText = 'Error on server while getting classifiers!';
            this.displayLoadingBlock = false;
        });
    }

    /**
     * Classifier list item click event handler
     * @param classifierData
     */
    public changeClassifier(classifierData: any): void
    {
        this.disableListGroups = true;

        this.classifierModel.load(
            classifierData.classifier_id,
            classifierData.text,
            classifierData.deleted,
            classifierData.version
        );
        this.getClassifierPatternsData(classifierData.classifier_id);
    }

    /**
     * Pattern list item click event handler
     * @param patternData
     */
    public changePattern(patternData: any): void
    {
        this.patternModel.load(
            patternData.classifier_pattern_id,
            patternData.classifier_id,
            patternData.text,
            patternData.test,
            patternData.deleted,
            patternData.version,
            patternData.active
        );
    }

    /**
     * Get and set available patterns for classifier
     * @param {number} classifierId
     */
    private getClassifierPatternsData(classifierId: number)
    {
        this.activeClassifierId = classifierId;
        this.httpService.get(ClassifiersRouting.getClassifierPatternsUrl(), {classifierId: classifierId}).then(data => {
            if (!data || !Object.keys(data).length) {
                this.displayLoadingBlock = false;
                this.disableListGroups = false;
                this.availablePatterns = [];
                this.patternModel.reset();
                return;
            }

            if (data[0]) {
                this.patternModel.load(
                    data[0].classifier_pattern_id,
                    data[0].classifier_id,
                    data[0].text,
                    data[0].test,
                    data[0].deleted,
                    data[0].version,
                    data[0].active
                );
            }

            this.availablePatterns = data;
            this.displayLoadingBlock = false;
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while getting patterns!';
            this.displayLoadingBlock = false;
            this.disableListGroups = false;
        });
    }

    /**
     * Create new classifier
     */
    createClassifier(): void
    {
        this.disableListGroups = true;

        this.httpService.post(ClassifiersRouting.getClassifierCreateUrl(), this.classifierModel).then(data => {
            if (!data || !Object.keys(data). length || !data[0] || data[0].err) {
                this.disableListGroups = false;
                this.errorText = 'Troubles with create classifier response!';
                return;
            }

            let createdItem = data[0].item.item;
            this.availableClassifiers.push(createdItem);
            this.classifierModel.classifier_id = createdItem.classifier_id;
            this.getClassifierPatternsData(createdItem.classifier_id);
            this.successText = 'Classifier was created successfully!';
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while creating classifier!';
            this.disableListGroups = false;
        });
    }

    /**
     * Update classifier
     */
    updateClassifier(): void
    {
        this.disableListGroups = true;

        this.httpService.post(ClassifiersRouting.getClassifierUpdateUrl(), this.classifierModel).then(data => {
            if (!data || !Object.keys(data). length || !data.result || !data.result.length || data.result[0].err) {
                this.disableListGroups = false;
                this.errorText = 'Troubles with update classifier response!';
                return;
            }

            this.availableClassifiers = this.availableClassifiers.map(function (el) {
                if (el.classifier_id == this.classifierId) {
                    el.text = this.newText;
                }

                return el;
            }, {classifierId: this.classifierModel.classifier_id, newText: this.classifierModel.text});

            this.successText = 'Classifier was updated successfully!';
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while updating classifier!';
            this.disableListGroups = false;
        });
    }

    /**
     * Delete classifier
     */
    deleteClassifier(): void
    {
        if (!confirm("Do you really want to delete '" + this.classifierModel.text + "' classifier?")) {
            return;
        }

        this.disableListGroups = true;

        this.httpService.post(ClassifiersRouting.getClassifierDeleteUrl(), this.classifierModel).then(data => {
            if (!data || !Object.keys(data). length || !data.result || !data.result.length || data.result[0].err) {
                this.disableListGroups = false;
                this.errorText = 'Troubles with delete classifier response!';
                return;
            }

            this.availableClassifiers = this.availableClassifiers
                .filter(el => el.classifier_id != this.classifierModel.classifier_id);

            this.classifierModel.reset();
            this.successText = 'Classifier was deleted successfully!';
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while deleting classifier!';
            this.disableListGroups = false;
        });
    }

    /**
     * Create new pattern for classifier
     */
    createPattern():void
    {
        if (!this.testPattern(false)) {
            return;
        }

        this.disableListGroups = true;
        this.patternModel.classifier_id = this.activeClassifierId;
        this.httpService.post(ClassifiersRouting.getClassifierPatternCreateUrl(), this.patternModel).then(data => {
            if (!data || !Object.keys(data). length || !data[0] || data[0].err) {
                this.disableListGroups = false;
                this.errorText = 'Troubles with create pattern response!';
                return;
            }

            let createdItem = data[0].item.item;
            this.availablePatterns.push(createdItem);
            this.patternModel.classifier_pattern_id = createdItem.classifier_pattern_id;
            this.successText = 'Pattern was created successfully!';
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while creating pattern!';
            this.disableListGroups = false;
        });
    }

    /**
     * Update pattern data
     * @param {boolean} toggleActive
     */
    updatePattern(toggleActive = false):void
    {
        if (!this.testPattern(false)) {
            return;
        }

        if (this.checkedPatterns.length) {
            this.updateMultiPatterns(toggleActive);
            return;
        }

        this.disableListGroups = true;

        this.httpService.post(ClassifiersRouting.getClassifierPatternUpdateUrl(), this.patternModel).then(data => {
            if (!data || !Object.keys(data). length || !data.result || !data.result.length || data.result[0].err) {
                this.disableListGroups = false;
                this.errorText = 'Troubles with update pattern response!';
                return;
            }

            this.availablePatterns = this.availablePatterns.map(function (el) {
                if (el.classifier_pattern_id == this.patternId) {
                    el.text = this.newText;
                    el.active = this.newActive
                }

                return el;
            }, {patternId: this.patternModel.classifier_pattern_id, newText: this.patternModel.text, newActive: this.patternModel.active});

            this.successText = 'Pattern was updated successfully!';
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while updating pattern!';
            this.disableListGroups = false;
        });
    }

    /**
     * Delete pattern
     */
    deletePattern():void
    {
        if (this.checkedPatterns.length) {
            if (!confirm("Do you really want to delete all checked patterns?")) {
                return;
            }
            this.deleteMultiPatterns();
            return;
        }

        if (!confirm("Do you really want to delete '" + this.patternModel.text + "' pattern?")) {
            return;
        }

        this.disableListGroups = true;
        this.httpService.post(ClassifiersRouting.getClassifierPatternDeleteUrl(), this.patternModel).then(data => {
            if (!data || !Object.keys(data). length || !data.result || !data.result.length || data.result[0].err) {
                this.disableListGroups = false;
                this.errorText = 'Troubles with delete classifier response!';
                return;
            }

            this.availablePatterns = this.availablePatterns
                .filter(el => el.classifier_pattern_id != this.patternModel.classifier_pattern_id);

            this.patternModel.reset();
            this.successText = 'Pattern was deleted successfully!';
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while deleting pattern!';
            this.disableListGroups = false;
        });
    }

    /**
     * Delete multi patterns with this.checkedPatterns ids data
     */
    private deleteMultiPatterns(): void
    {
        this.disableListGroups = true;
        this.httpService.post(ClassifiersRouting.getClassifierPatternMultiDeleteUrl(), {ids: this.checkedPatterns}).then(data => {

            this.availablePatterns = this.availablePatterns
                .filter(el => this.checkedPatterns.indexOf(el.classifier_pattern_id) === -1);

            this.unCheckAllPatterns();
            this.patternModel.reset();
            this.successText = 'Checked patterns was deleted successfully!';
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while deleting multi patterns!';
            this.disableListGroups = false;
        });
    }

    /**
     * Update multi patterns with this.checkedPatterns ids data
     * @param toggleActive
     */
    private updateMultiPatterns(toggleActive): void
    {
        if (toggleActive) {
            this.patternModel.test = null;
            this.patternModel.text = null;
        }
        this.disableListGroups = true;
        this.httpService.post(ClassifiersRouting.getClassifierPatternMultiUpdateUrl(), {model: this.patternModel, ids: this.checkedPatterns}).then(data => {
            this.availablePatterns = this.availablePatterns.map(function (el) {
                if (this.checkedPatternsArray.indexOf(el.classifier_pattern_id) !== -1) {
                    if (!toggleActive) {
                        el.text = this.model.text;
                        el.test = this.model.test;
                    }
                    el.active = this.model.active;
                }

                return el;
            }, {model: this.patternModel, checkedPatternsArray: this.checkedPatterns});

            this.unCheckAllPatterns();
            this.successText = 'Checked patterns was updated successfully!';
            this.disableListGroups = false;
        }, err => {
            this.errorText = 'Error on server while updating multi patterns!';
            this.disableListGroups = false;
        });
    }

    /**
     * Set pattern active status to false
     */
    deactivatePattern(): void
    {
        this.patternModel.active = 0;
        this.updatePattern(true);
    }

    /**
     * Set pattern active status to true
     */
    activatePattern(): void
    {
        this.patternModel.active = 1;
        this.updatePattern(true);
    }

    /**
     * Test pattern for regexp
     */
    testPattern(successAlert: boolean = true): boolean
    {
        let testStatus = this.patternModel.test.match(this.patternModel.text);

        if (testStatus) {
            if (successAlert) {
                alert('Success. The regular expression and string entered is match.');
            }

            return true;
        }

        alert('Error. The regular expression and string entered do not match.');
        return false;
    }

    /**
     * Check all patterns (push all ids to this.checkedPatterns)
     */
    checkAllPatterns():void
    {
        for (let i = 0; i < this.availablePatterns.length; i++) {
            this.checkedPatterns.push(this.availablePatterns[i].classifier_pattern_id);
        }
    }

    /**
     * Uncheck all patterns (reset this.checkedPatterns)
     */
    unCheckAllPatterns():void
    {
        this.checkedPatterns = [];
    }
}
