<?php
use common\widgets\ExpandedTable\ExpandedTable;

$availableSites = \Yii::$app->params['sites'];
$availableLocales = \Yii::$app->params['availableLanguages'];
$availableTrafficSources = \Yii::$app->params['trafficSources'];
$availableFanClubs = \Yii::$app->params['fanClubs'];
$availableOnlines = \Yii::$app->params['onlines'];
$availableScenarios = $model->availableScenarios;

function sortScenarios($s1, $s2)
{
    if ($s1->scenario_priority < $s2->scenario_priority) {
        return -1;
    } elseif ($s1->scenario_priority > $s2->scenario_priority) {
        return 1;
    } else {
        return 0;
    }
}

$settingsItems = array();
foreach ($model->settings as $setting) {
    $item = null;
    $item[0][] = ['value' => $setting->id, 'hidden' => $setting->id];

    $item[1][] = ['value' => $setting->name,];

    $item[2][] = null;
    foreach ($setting->projects as $project) {
        $item[2][] = ['value' => $project->project_name, 'hidden' => $project->project_name];
    }

    $item[3][] = ['value' => $setting->control_profile, 'hidden' => $setting->control_profile];

    $item[4][] = ['value' => $setting->target_profile, 'hidden' => $setting->target_profile];

    $item[5][] = null;
    foreach ($setting->countries as $country) {
        $item[5][] = ['value' => $country->country_name, 'hidden' => $country->country_name];
    }

    $item[6][] = null;
    foreach ($setting->locales as $locale) {
        $item[6][] = ['value' => $locale->locale_name, 'hidden' => $locale->locale_name];
    }

    $item[7][] = null;
    foreach ($setting->statuses as $status) {
        $item[7][] = ['value' => $status->status_name, 'hidden' => $status->status_name];
    }

    $item[8][] = null;
    foreach ($setting->sites as $site) {
        try {
            $siteHash = $site->site_hash;
            $siteName = $availableSites[$siteHash];
            $item[8][] = ['value' => $siteName, 'hidden' => $siteHash];
        } catch(Exception $e) {
        }
    }

    $item[9][] = null;
    $includedTrafficSources = [];
    foreach ($setting->trafficSources as $trafficSource) {
        $includedTrafficSources[] = $trafficSource->traffic_source_name;
    }
    $excludedTrafficSources = array_diff($availableTrafficSources, $includedTrafficSources);
    foreach ($excludedTrafficSources as $excludedTrafficSource) {
        $item[9][] = ['value' => $excludedTrafficSource];
    }
    foreach ($includedTrafficSources as $includedTrafficSource) {
        $item[9][] = ['hidden' => $includedTrafficSource];
    }

    $item[10][] = null;
    foreach ($setting->actionWays as $actionWay) {
        $item[10][] = ['value' => $actionWay->action_way_name, 'hidden' => $actionWay->action_way_name];
    }

    $item[11][] = null;
    foreach ($setting->trafficTypes as $trafficType) {
        $item[11][] = ['value' => $trafficType->traffic_type_name, 'hidden' => $trafficType->traffic_type_name];
    }

    $item[12][] = null;
    foreach ($setting->platforms as $platform) {
        $item[12][] = ['value' => $platform->platform_name, 'hidden' => $platform->platform_name];
    }

    $item[13][] = null;
    foreach ($setting->fanClubs as $fanClub) {
        try {
            $fanClubId = $fanClub->is_fan_club_allowed;
            $fanClubText = $availableFanClubs[$fanClubId];
            $item[13][] = ['value' => $fanClubText, 'hidden' => $fanClubId];
        } catch(Exception $e) {
            \Yii::error('Error in fan clubs ' . $e);
        }
    }

    $item[14][] = null;
    foreach ($setting->onlines as $online) {
        try {
            $onlineId = $online->is_online;
            $onlineText = $availableOnlines[$onlineId];
            $item[14][] = ['value' => $onlineText, 'hidden' => $onlineId];
        } catch(Exception $e) {
            \Yii::error('Error in onlines ' . $e);
        }
    }

    $item[15][] = null;
    $settingScenarios = $setting->scenarios;
    uasort($settingScenarios, "sortScenarios");
    foreach ($settingScenarios as $scenario) {
        $scenarioId = $scenario->scenario_id;
        $scenarioPriority = $scenario->scenario_priority;
        $scenarioIsFixed = $scenario->isFixed;
        $scenarioName = null;
        foreach ($availableScenarios as $availableScenario) {
            if ($availableScenario->getId() == $scenarioId) {
                $scenarioName = $availableScenario->getName();
                break;
            }
        }
        $item[15][] = ['value' => $scenarioName, 'hidden_1' => $scenarioId, 'hidden_2' => $scenarioIsFixed];
    }

    $settingActiveText = '<span class="glyphicon glyphicon-off';
    if ($setting->active) {
        $settingActiveText .= ' active';
    }
    $settingActiveText .= '"></span>';

    $item[16][] = ['value' => $settingActiveText, 'hidden' => $setting->active];

    $settingsItems[] = $item;
}

echo ExpandedTable::widget([
    'headerItems' => [
        ['label' => 'Id',],
        ['label' => 'Name',],
        ['label' => 'Project',],
        ['label' => 'Control profile',],
        ['label' => 'Target profile',],
        ['label' => 'Country',],
        ['label' => 'Locale',],
        ['label' => 'Status',],
        ['label' => 'Site',],
        ['label' => 'Exc traffic source',],
        ['label' => 'Action way',],
        ['label' => 'Traffic type',],
        ['label' => 'Platform',],
        ['label' => 'Fan club',],
        ['label' => 'Online',],
        ['label' => 'Scenario',],
        ['label' => 'Active',],
    ],
    'items' => $settingsItems,
]);
?>
