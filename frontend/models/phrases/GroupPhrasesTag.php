<?php
namespace frontend\models\phrases;

use yii\db\ActiveRecord;

class GroupPhrasesTag extends ActiveRecord
{
    public static function tableName()
    {
        return 'group_phrases_tags';
    }
}
