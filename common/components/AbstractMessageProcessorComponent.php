<?php
namespace common\components;

use frontend\models\placeholders\Placeholder;
use frontend\models\ShownLinks;
use frontend\models\topics\TopicExclusionInitial;
use frontend\models\topics\TopicTag;
use frontend\models\topics\UserTopic;
use frontend\models\UserTag;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\ArrayHelper;

abstract class AbstractMessageProcessorComponent extends Component
{
    protected $isPredispatched = false;

    public function getAnswerData($data, &$response = null)
    {
        $sender         = $data['profiles'][$data['from']];
        $recipient      = $data['profiles'][$data['to']];
        $isExclude      = false;

        if (static::class === DialogRecoveryComponent::class) {
            $excludedTopicsCount = (int)(new Query())
                ->from(UserTopic::tableName() . ' as ut')
                ->innerJoin(TopicExclusionInitial::tableName() . ' as tei', 'ut.topicId=tei.topicId')
                ->andWhere([
                    'ut.userId' => $sender['multi_profile_id'],
                    'tei.isExclude' => 1
                ])
                ->count();
            if ($excludedTopicsCount > 0) {
                return [];
            }
        }

        if (in_array($sender['traff_src'], ['ppc', 'ppcc'])) {
            return ['error' => 'Disallowed traffic source'];
        }

        $scenarioId = $this->getScenarioId($sender, $recipient);

        //Save incoming message data to log
        $this->saveIncomingMessageToLog($sender, $recipient, $data['text'], $scenarioId);

        if ($scenarioId == false) {
            return ['error' => 'Scenario id not found'];
        }

        //Get eva answer
        $data['scenario_id'] = $scenarioId;
        $evaResponse = $this->getEvaAnswer($data);
        if ($response !== null) {
            $response = $evaResponse;
        }

        if (empty($evaResponse['text']) && !isset($evaResponse['status'])) {
            return ['error' => 'Empty eva response'];
        }

        //If it was answer from topic (not from scenario) and it is 'cams' or 'dating' topic - save it
        if (!empty($evaResponse['answer_params']['topic']['topic_id'])) {
            $topicId = $evaResponse['answer_params']['topic']['topic_id'];
            //Check if topic has 'cams' or 'dating' flag
            $topicCamsTag   = TopicTag::findOne([
                'topicId'   => $topicId,
                'tagId'     => TopicTag::TAG_ID_CAMS,
            ]);
            $topicDatingTag = TopicTag::findOne([
                'topicId'   => $topicId,
                'tagId'     => TopicTag::TAG_ID_DATING,
            ]);

            //Save 'cams' and 'dating' tags if exists
            if ($topicCamsTag != null) {
                $userCamsTag = UserTag::findOne([
                    'userId'    => $sender['multi_profile_id'],
                    'tagId'     => TopicTag::TAG_ID_CAMS,
                ]);
                if ($userCamsTag == null) {
                    $userTag            = new UserTag();
                    $userTag->userId    = $sender['multi_profile_id'];
                    $userTag->tagId     = TopicTag::TAG_ID_CAMS;
                    $userTag->save();
                }
            }
            if ($topicDatingTag != null) {
                $userDatingTag = UserTag::findOne([
                    'userId'    => $sender['multi_profile_id'],
                    'tagId'     => TopicTag::TAG_ID_DATING,
                ]);
                if ($userDatingTag == null) {
                    $userTag            = new UserTag();
                    $userTag->userId    = $sender['multi_profile_id'];
                    $userTag->tagId     = TopicTag::TAG_ID_DATING;
                    $userTag->save();
                }
            }

            $topicExclusionInitial = TopicExclusionInitial::findOne([
                'topicId'   => $topicId,
                'isExclude' => 1
            ]);
            if (!empty($topicExclusionInitial)) {
                $userTopic = UserTopic::findOne([
                    'topicId' => $topicId,
                    'userId'  => $sender['multi_profile_id'],
                ]);
                if (empty($userTopic)) {
                    $userTopic                  = new UserTopic();
                    $userTopic->topicId         = $topicId;
                    $userTopic->userId          = $sender['multi_profile_id'];
                    $userTopic->countTriggering = 1;
                } else {
                    $userTopic->countTriggering += 1;
                }
                $userTopic->save();

                if ($userTopic->countTriggering >= $topicExclusionInitial->countTriggering) {
                    $isExclude = true;
                }
            }
        }

        //Split eva message into parts and replace placeholders if exists
        $messages = $this->splitMessage(
            $evaResponse['text'],
            [
                'idTo'                  => $recipient['id'],
                'idFrom'                => $sender['id'],
                'controlProfileType'    => $this->getControlProfileType($recipient),
                'country'               => $sender['country'],
                'status'                => $this->getPaidStatus($sender),
                'site'                  => $sender['site'],
                'trafficSource'         => $sender['traff_src'],
                'actionWay'             => $sender['action_way'],
                'platform'              => $sender['platform'],
                'screenname'            => $recipient['name'] ?? '',
                'isExclude'             => $isExclude,
                'shortId'               => $recipient['profile_short_id'] ?? '',
                'providerName'          => $data['providerName'] ?? '',
            ]
        );

        //Prepare and save outgoing message data to log
        $fullResponseText = '';
        $firstUsedPlaceholderId = 0;
        if (!empty($messages)) {
            $fullResponseText = implode(' ', ArrayHelper::getColumn($messages, 'message'));
            $usedPlaceholderIds = ArrayHelper::getColumn($messages, 'placeholderId');
            $usedPlaceholderIds = array_filter($usedPlaceholderIds);
            if (!empty($usedPlaceholderIds)) {
                $firstUsedPlaceholderId = array_shift($usedPlaceholderIds);
            }
        }
        
        $this->saveOutgoingMessageToLog(
            $sender,
            $recipient,
            $fullResponseText,
            $scenarioId,
            $firstUsedPlaceholderId,
            $evaResponse
        );

        if (!empty($firstUsedPlaceholderId)) {
            /** @var Placeholder $placeholder */
            $placeholder = Placeholder::findOne(['id' => $firstUsedPlaceholderId]);

            if ($placeholder && $placeholder->is_link) {
                $shownLinks = new ShownLinks([
                    'userId'          => $sender['multi_profile_id'],
                    'scenarioId'      => $scenarioId,
                    'placeholderId'   => $firstUsedPlaceholderId,
                    'isPredispatched' => $this->isPredispatched,
                ]);
                $shownLinks->save();
            }
        }

        return $messages;
    }

    protected function getControlProfileType($profile)
    {
        switch ($profile['traff_src']) {
            case 'Cams models':
                $controlProfileType = 'Cams';
                break;
            case 'direct':
                $controlProfileType = 'External';
                break;
            case 'Aff Internal':
                $controlProfileType = 'Scam 2.0';
                break;
            case 'b2b':
                $controlProfileType = 'Labs';
                break;
            case 'euromodels':
                $controlProfileType = 'Euromodels';
                break;
            default:
                $controlProfileType = '';
        }
        
        return $controlProfileType;
    }

    protected function getPaidStatus($profile)
    {
        return $profile['paid'] ? 'Paid' : 'Free';
    }

    protected function splitMessage($messageText, $params)
    {
        $result = [];

        if (!empty($messageText)) {
            //Divide message
            $messages = array();
            preg_match_all("/(.+?)(\.\.\.|;\)|$){1,}/", $messageText, $parts);

            $notSplitPlaceholderControlProfiles = \Yii::$app->params['notSplitPlaceholderControlProfiles'];
            if (in_array($params['controlProfileType'], $notSplitPlaceholderControlProfiles)) {
                foreach ($parts[0] as $part){
                    $messages[] = trim($part);
                }
            } else {
                foreach ($parts[0] as $part){
                    preg_match_all("/[\$][A-Za-z0-9_]+|.+?(?=[\$][A-Za-z0-9_]+|$)/", $part, $elements);
                    foreach ($elements[0] as $element) {
                        $messages[] = trim($element);
                    }
                }
            }

            $previousDelay = 0;

            foreach ($messages as $message) {
                //Substitution of placeholders
                $replacementData = \Yii::$app->placeholders->replacePlaceholders($message, $params);
                $message = $replacementData['text'];

                //Get delay for message
                $textLength = mb_strlen($message);
                $delay = rand($textLength * 1000 / 8, $textLength * 1000 / 4 + 1);
                $delay = (int)($delay / 1000);
                if ($delay < 5) {
                    $delay = 5;
                }
                $delay += $previousDelay;
                $previousDelay = $delay;

                $result[] = [
                    'message'           => $message,
                    'delay'             => $delay,
                    'hasPlaceholder'    => !empty($replacementData['placeholderId']),
                    'placeholderId'     => $replacementData['placeholderId'],
                    'isExclude'         => $params['isExclude'],
                ];
            }
        } else {
            $result[] = [
                'message'           => '',
                'delay'             => 0,
                'hasPlaceholder'    => false,
                'placeholderId'     => null,
                'isExclude'         => $params['isExclude'],
            ];
        }

        return $result;
    }

    protected function getScenarioId($sender, $recipient)
    {
        $scenarioId = \Yii::$app->scenarios->getScenarioId([
            'idFrom'                => $sender['id'],
            'idFromMulti'           => $sender['multi_profile_id'],
            'idTo'                  => $recipient['id'],
            'controlProfileType'    => $this->getControlProfileType($recipient),
            'country'               => $sender['country'],
            'language'              => $recipient['language'],
            'senderLanguage'        => $sender['language'],
            'paidStatus'            => $this->getPaidStatus($sender),
            'site'                  => $sender['site'],
            'platform'              => $sender['platform'],
            'trafficSource'         => $sender['traff_src'],
            'actionWay'             => $sender['action_way'],
            'isFanClubAllowed'      => $recipient['isFanClubAllowed'] ?? null,
            'splits'                => $recipient['splits'] ?? null,
            'prepaid'               => $recipient['prepaid'] ?? 0,
            'isOnline'              => $recipient['isModelOnline'] ?? null,
        ]);

        return $scenarioId;
    }

    protected function getEvaAnswer($data)
    {
        return \Yii::$app->eva->getEvaAnswer($data);
    }

    protected function saveIncomingMessageToLog($sender, $recipient, $text, $scenarioId)
    {
         \Yii::$app->communicationLog->saveToLog([
            'profileType'      => $this->getControlProfileType($recipient),
            'site'             => $sender['site'],
            'country'          => $sender['country'],
            'language'         => $recipient['language'] ?? '',
            'senderLanguage'   => $sender['language'] ?? '',
            'status'           => $this->getPaidStatus($sender),
            'platform'         => $sender['platform'],
            'trafficSource'    => $sender['traff_src'],
            'scenario'         => $scenarioId,
            'userId'           => $sender['id'],
            'modelId'          => $recipient['id'],
            'isUser'           => true,
            'message'          => $text,
            'screenname'       => $sender['name'] ?? '',
        ]);
    }

    protected function saveOutgoingMessageToLog($sender, $recipient, $text, $scenarioId, $firstUsedPlaceholderId, $evaResponse, $predispatched = false)
    {
        \Yii::$app->communicationLog->saveToLog([
            'profileType'      => $this->getControlProfileType($recipient),
            'site'             => $sender['site'],
            'country'          => $sender['country'],
            'language'         => $sender['language'] ?? '',
            'status'           => $this->getPaidStatus($sender),
            'platform'         => $sender['platform'],
            'trafficSource'    => $sender['traff_src'],
            'scenario'         => $scenarioId,
            'placeholder'      => $firstUsedPlaceholderId,
            'userId'           => $sender['id'],
            'modelId'          => $recipient['id'],
            'isUser'           => false,
            'message'          => $text,
            'hasSwitching'     => (isset($evaResponse['status']) && $evaResponse['status'] == 2),
            'hasRule'          => (isset($evaResponse['rule']) && $evaResponse['rule'] == 'true'),
            'screenname'       => $recipient['name'] ?? '',
            'predispatched'    => $predispatched,
        ]);
    }
}