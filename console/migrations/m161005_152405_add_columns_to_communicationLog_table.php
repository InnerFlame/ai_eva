<?php

use yii\db\Migration;

class m161005_152405_add_columns_to_communicationLog_table extends Migration
{
    public function up()
    {
        $this->addColumn('communicationLog', 'hasRule', $this->boolean()->defaultValue(false));
        $this->addColumn('communicationLog', 'hasSwitching', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('communicationLog', 'hasRule');
        $this->dropColumn('communicationLog', 'hasSwitching');
    }
}
