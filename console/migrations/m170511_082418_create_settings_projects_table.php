<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings_projects`.
 */
class m170511_082418_create_settings_projects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_projects', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'project_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_projects');
    }
}
