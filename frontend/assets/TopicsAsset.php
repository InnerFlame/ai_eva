<?php

namespace frontend\assets;

class TopicsAsset extends BasicAsset
{
    public $js = [
        'js/topics.js',
        'js/WebClient.js',
    ];
}
