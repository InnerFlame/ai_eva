//Supporting variables
var currentGroupGroupsPhrasesId = null;
var currentGroupPhrasesId       = null;
var currentPhrasesId            = null;
var allPhrasesItemsWasChecked       = false;
var allGroupPhrasesItemsWasChecked  = false;

//Load list of groups phrases
function updateGroupsPhrases()
{
    $.ajax({
        url: '/phrases/group-phrases',
        success: function (data) {
            //Update group phrases
            $('#groupPhrases').html(data.groupPhrases);
            $('#groupGroupsPhrases').html(data.groupGroupsPhrases);

            var groupGroupsPhrasesItem;

            if (currentGroupGroupsPhrasesId === null) {
                groupGroupsPhrasesItem = $('#groupGroupsPhrases li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="groupGroupsPhrasesId"][value="' + currentGroupGroupsPhrasesId + '"]';
                groupGroupsPhrasesItem = $('#groupGroupsPhrases li.list-group-item.sortable').has(str);
            }

            if (groupGroupsPhrasesItem.length === 0) {
                currentGroupGroupsPhrasesId = null;
            } else {
                groupGroupsPhrasesItem.click();
            }
        }
    });
}

//Load list of phrases
function updatePhrases()
{
    $.ajax({
        url: '/phrases/phrases',
        type: "POST",
        data: 'groupPhrasesId=' + currentGroupPhrasesId,
        success: function (data) {
            //Update phrases
            $('#phrases').html(data);
            //Trigger click on first or current phrases item
            var phrasesItem;
            var currentPhrasesId = $('#currentPhrasesId').val();
            if (currentPhrasesId === "") {
                phrasesItem = $('#phrases li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="phraseId"][value="' + currentPhrasesId + '"]';
                phrasesItem = $('#phrases li.list-group-item.sortable').has(str);
            }
            if (phrasesItem.length == 0) {
                phrasesItem = $('#phrases li.list-group-item.sortable:first-child');
            }
            if (phrasesItem.length == 0) {
                currentPhrasesId = null;
            } else {
                phrasesItem.removeClass('lastClickedItem');
                phrasesItem.click();
            }
        }
    });
}

//Get array of checked items ids
function getCheckedItemsIds(parentId, itemIdName)
{
    var checkedItemsIds = [];
    $(parentId + ' li.list-group-item:visible input[name="itemCheckbox"]:checked').each(function () {
        var id = $(this).closest('li.list-group-item').find('input[name="' + itemIdName + '"]').val();
        checkedItemsIds.push(id);
    });
    return checkedItemsIds;
}

//Get array of checked or current items ids
function getCheckedOrCurrentItemsIds(parentId, itemIdName, bindableFormId)
{
    //Get checked items
    var checkedOrCurrentItemsIds = getCheckedItemsIds(parentId, itemIdName);
    //If there are no checked items get current item
    if (checkedOrCurrentItemsIds.length == 0) {
        var currentItemId = $(bindableFormId + ' input[name="' + itemIdName + '"]').val();
        if (currentItemId != 0) {
            checkedOrCurrentItemsIds.push(currentItemId);
        }
    }
    return checkedOrCurrentItemsIds
}

//Get string of checked or current items names (for modal window)
function getCheckedOrCurrentItemsNames(parentId, itemIdName, bindableFormId)
{
    var itemsNames = '';
    var itemsIds = getCheckedOrCurrentItemsIds(parentId, itemIdName, bindableFormId);
    $.each(itemsIds, function (index, value) {
        var itemsName = $(parentId + ' ul li:visible input[name="' + itemIdName + '"][value="' + value + '"]').siblings('span[name="itemText"]').html();
        itemsNames += (index + 1) + '. ' + itemsName + '<br/>'
    });
    return itemsNames;
}

//Convert checkboxes values to array
function checkboxesToArray(obj)
{
    var objects = $(obj).find('ul li a input[type="checkbox"]:checked').closest('li').find('input[type="hidden"]');
    var arr = [];
    for (var i = 0; i < objects.size(); i++) {
        arr.push($(objects[i]).val());
    }
    return arr;
}

$(document).ready(function () {
    /**********  GROUPS GROUPS PHRASES FUNCTIONALITY **********/
    //Load list of groups phrases
    updateGroupsPhrases();

    //Click on group phrases item event
    $(document).on('click', '#groupGroupsPhrases li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        $('#phrases').html('');
        clearBindableForm('#groupPhrasesBindableForm');
        clearBindableForm('#phrasesBindableForm');
        var groupGroupsPhrasesId = $(this).children('input[name="groupGroupsPhrasesId"]').val();
        if (groupGroupsPhrasesId === undefined) {
            currentGroupGroupsPhrasesId = null;
            return;
        }
        currentGroupGroupsPhrasesId = groupGroupsPhrasesId;

        //Filter groups phrases
        $('#groupPhrases li.list-group-item.sortable').show();
        var otherItems = $('#groupPhrases li.list-group-item.sortable input[name="groupGroupsPhrasesId"][value!="' +
        currentGroupGroupsPhrasesId + '"]').closest('li').hide();

        //Click on first or current group phrases item
        var groupPhrasesItem;
        var currentGroupPhrasesId = $('#currentGroupPhrasesId').val();
        if (currentGroupPhrasesId === "") {
            groupPhrasesItem = $('#groupPhrases li.list-group-item.sortable:visible:first');
        } else {
            var str = 'input[name="groupPhrasesId"][value="' + currentGroupPhrasesId + '"]';
            groupPhrasesItem = $('#groupPhrases li.list-group-item.sortable:visible').has(str);
        }
        if (groupPhrasesItem.length == 0) {
            groupPhrasesItem = $('#groupPhrases li.list-group-item.sortable:visible:first');
        }
        if (groupPhrasesItem.length == 0) {
            currentGroupPhrasesId = null;
        } else {
            groupPhrasesItem.removeClass('lastClickedItem');
            groupPhrasesItem.click();
        }
    });

    //Click 'Add new group groups phrases' button
    $(document).on('click', '#addNewGroupGroupsPhrasesButton', function () {
        var name = $('#groupGroupsPhrasesText').val();
        name = encodeURIComponent(name);
        $.ajax({
            url: '/phrases/create-group-groups-phrases',
            type: "POST",
            data: 'name=' + name,
            success: function (data) {
                if (data == 'true') {
                    updateGroupsPhrases();
                } else {
                    alert('An error occurred while creating the group groups phrases!');
                }
            }
        });
    });

    //Click 'Update group groups phrases' button
    $(document).on('click', '#updateGroupGroupsPhrasesButton', function () {
        var name = $('#groupGroupsPhrasesText').val();
        name = encodeURIComponent(name);
        var id = $('#groupGroupsPhrasesBindableForm input[name="groupGroupsPhrasesId"]').val();
        if (id.length == 0) {
            return;
        }
        $.ajax({
            url: '/phrases/update-group-groups-phrases',
            type: "POST",
            data: 'id=' + id + '&name=' + name,
            success: function (data) {
                if (data == 'true') {
                    updateGroupsPhrases();
                } else {
                    alert('An error occurred while updating the group groups phrases!');
                }
            }
        });
    });

    //Click 'Delete group groups phrases' button
    $(document).on('click', '#deleteGroupGroupsPhrasesButton', function () {
        var id = $('#groupGroupsPhrasesBindableForm input[name="groupGroupsPhrasesId"]').val();
        if (id.length == 0) {
            return;
        }
        $.ajax({
            url: '/phrases/delete-group-groups-phrases',
            type: "POST",
            data: 'id=' + id,
            success: function (data) {
                if (data == 'true') {
                    updateGroupsPhrases();
                } else {
                    alert('Group groups phrases must be empty for deleting!');
                }
            }
        });
    });

    //Click 'Copy group groups phrases' button
    $(document).on('click', '#copyGroupGroupsPhrasesButton', function () {
        var id = $('#groupGroupsPhrasesBindableForm input[name="groupGroupsPhrasesId"]').val();
        if (id.length == 0) {
            return;
        }
        $.ajax({
            url: '/phrases/copy-group-groups-phrases',
            type: "POST",
            data: 'id=' + id,
            success: function (data) {
                if (data == 'true') {
                    updateGroupsPhrases();
                } else {
                    alert('An error occurred while copying the group groups phrases!');
                }
            }
        });
    });

    /**********  GROUPS PHRASES FUNCTIONALITY **********/
    // Click on available languages dropdown
    $(document).on('click', '#groupPhrasesBindableForm div[name="topicLanguage"] li', function () {
        var val     = $(this).find('input').val();
        var name    = $(this).find('a').html();
        $(this).closest('div').find('span[name="currentGroupPhrasesLanguageText"]').html(name);
        $(this).closest('div').find('input[name="currentGroupPhrasesLanguage"]').val(val);
    });

    //Click on group phrases item event
    $(document).on('click', '#groupPhrases li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        $('#phrases').html('');
        clearBindableForm('#phrasesBindableForm');
        var groupPhrasesId = $(this).children('input[name="groupPhrasesId"]').val();
        if (groupPhrasesId === undefined) {
            currentGroupPhrasesId = null;
            return;
        }
        currentGroupPhrasesId = groupPhrasesId;

        // Set checked tags of current group phrases
        var jsonTags = $(this).find('input[name="groupPhrasesTags"]').val();
        var tags = JSON.parse(jsonTags);
        $('div[name="groupPhrasesTagsButton"] li').each(function(index) {
            var currentTagId = $(this).find('input[type="hidden"]').val();
            if ($.inArray(parseInt(currentTagId), tags) !== -1) {
                $(this).find('input[type="checkbox"]').prop('checked', true);
            } else {
                $(this).find('input[type="checkbox"]').prop('checked', false);
            }
        });

        updatePhrases();
    });

    //Click 'Add new group phrases' button
    $(document).on('click', '#addNewGroupPhrasesButton', function () {
        var text = $('#groupPhrasesText').val();
        text = encodeURIComponent(text);
        $.ajax({
            url: '/phrases/create-multi-group-phrases',
            type: "POST",
            data: 'text=' + text + '&groupGroupsPhrasesId=' + currentGroupGroupsPhrasesId,
            success: function (data) {
                updateGroupsPhrases();
            }
        });
    });

    //Click 'Update group phrases' button
    $(document).on('click', '#updateGroupPhrasesButton', function () {
        var text            = $('#groupPhrasesText').val();
        text                = encodeURIComponent(text);
        var groupPhrasesId  = $('#groupPhrasesBindableForm input[name="groupPhrasesId"]').val();
        var groupPhrasesIds = getCheckedOrCurrentItemsIds('#groupPhrases', 'groupPhrasesId', '#groupPhrasesBindableForm');
        var parent          = $('#groupPhrasesBindableForm');
        var lang            = parent.find('input[name="currentGroupPhrasesLanguage"]').val();
        var tags            = checkboxesToArray(parent.find('div[name="groupPhrasesTagsButton"]'));

        if (groupPhrasesId.length == 0) {
            return;
        }
        $.ajax({
            url: '/phrases/update-group-phrases',
            type: "POST",
            data: 'groupPhrasesId=' + groupPhrasesId + '&text=' + text + '&lang=' + lang +
                '&tags=' + JSON.stringify(tags) + '&groupPhrasesIds=' + JSON.stringify(groupPhrasesIds),
            success: function (data) {
                if (data == 'true') {
                    updateGroupsPhrases();
                } else {
                    alert('An error occurred while updating the group phrases!');
                }
            }
        });
    });

    //Click exclude phrases button
    $(document).on('click', 'button[name="excludePhrasesButton"]', function () {
        var isActive = $(this).hasClass('active');
        var groupPhrasesId = $(this).siblings('input[name="groupPhrasesId"]').val();
        var button = $(this);
        if (groupPhrasesId.length == 0) {
            return;
        }
        $.ajax({
            url: '/phrases/update-group-phrases-excluded',
            type: "POST",
            data: 'groupPhrasesId=' + groupPhrasesId + '&excluded=' + (!isActive).toString(),
            success: function (data) {
                if (data == 'true') {
                    if (isActive) {
                        button.removeClass('active');
                    } else {
                        button.addClass('active');
                    }
                } else {
                    alert('An error occurred while updating the group phrases!');
                }
            }
        });
    });

    //Click has placeholder button
    $(document).on('click', 'button[name="hasPlaceholderButton"]', function () {
        var isActive = $(this).hasClass('active');
        var groupPhrasesId = $(this).siblings('input[name="groupPhrasesId"]').val();
        var button = $(this);
        if (groupPhrasesId.length == 0) {
            return;
        }
        $.ajax({
            url: '/phrases/update-group-phrases-has-placeholder',
            type: "POST",
            data: 'groupPhrasesId=' + groupPhrasesId + '&hasPlaceholder=' + (!isActive).toString(),
            success: function (data) {
                if (data == 'true') {
                    if (isActive) {
                        button.removeClass('active');
                    } else {
                        button.addClass('active');
                    }
                } else {
                    alert('An error occurred while updating the group phrases!');
                }
            }
        });
    });

    //Click has pre placeholder button
    $(document).on('click', 'button[name="hasPrePlaceholderButton"]', function () {
        var isActive = $(this).hasClass('active');
        var groupPhrasesId = $(this).siblings('input[name="groupPhrasesId"]').val();
        var button = $(this);
        if (groupPhrasesId.length == 0) {
            return;
        }
        $.ajax({
            url: '/phrases/update-group-phrases-has-pre-placeholder',
            type: "POST",
            data: 'groupPhrasesId=' + groupPhrasesId + '&hasPrePlaceholder=' + (!isActive).toString(),
            success: function (data) {
                if (data == 'true') {
                    if (isActive) {
                        button.removeClass('active');
                    } else {
                        button.addClass('active');
                    }
                } else {
                    alert('An error occurred while updating the group phrases!');
                }
            }
        });
    });

    //Click check all group phrases button
    $(document).on('click', '#checkAllGroupPhrasesButton', function () {
        if (allGroupPhrasesItemsWasChecked) {
            allGroupPhrasesItemsWasChecked = false;
            $('#groupPhrases li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allGroupPhrasesItemsWasChecked = true;
            $('#groupPhrases li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });

    //Click group phrases delete button
    $(document).on('click', '#deleteGroupPhrasesButton', function () {
        if (getCheckedOrCurrentItemsIds('#groupPhrases', 'groupPhrasesId', '#groupPhrasesBindableForm').length == 0) {
            return;
        }
        var groupPhrasesNames = getCheckedOrCurrentItemsNames('#groupPhrases', 'groupPhrasesId', '#groupPhrasesBindableForm');
        $('#deleteGroupPhrasesNames').html(groupPhrasesNames);
        $('#confirmDeleteGroupPhrasesModal').modal('show');
    });

    //Click confirm group phrases delete button
    $(document).on('click', '#confirmDeleteGroupPhrasesButton', function () {
        var groupPhrasesIds = getCheckedOrCurrentItemsIds('#groupPhrases', 'groupPhrasesId', '#groupPhrasesBindableForm');
        if (groupPhrasesIds.length == 0) {
            return;
        }
        var params = [];
        $.each(groupPhrasesIds, function (index, value) {
            var elem = {};
            elem.group_phrase_id = value;
            params.push(elem);
        });
        $.ajax({
            url: '/phrases/delete-group-phrases',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updateGroupsPhrases();
                } else {
                    alert('An error occurred while deleting the group phrases!');
                }
                $('#confirmDeleteGroupPhrasesModal').modal('hide');
            }
        });
    });

    /**********  PHRASES FUNCTIONALITY **********/
    //Click on phrases item event
    $(document).on('click', '#phrases li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');
        //Set current delay and timeslot id and text
        var delayId = $(this).children('input[name="phraseDelayId"]').val();
        var timeslotId = $(this).children('input[name="phraseTimeslotId"]').val();
        $('#phrasesBindableForm #currentDelayId').val(delayId);
        $('#phrasesBindableForm #currentTimeslotId').val(timeslotId);
        var delayText = $('#phrasesBindableForm #availableDelays').find('input[value=' + delayId + ']').siblings('a').html();
        var timeslotText = $('#phrasesBindableForm #availableTimeslots').find('input[value=' + timeslotId + ']').siblings('a').html();
        $('#phrasesBindableForm #currentDelayText').html(delayText);
        $('#phrasesBindableForm #currentTimeslotText').html(timeslotText);
        var phrasesId = $(this).children('input[name="phraseId"]').val();
        if (phrasesId === undefined) {
            phrasesId = null;
        }
        currentPhrasesId = phrasesId;
    });

    //Click check all phrases button
    $(document).on('click', '#checkAllPhrasesButton', function () {
        if (allPhrasesItemsWasChecked) {
            allPhrasesItemsWasChecked = false;
            $('#phrases li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allPhrasesItemsWasChecked = true;
            $('#phrases li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });

    //Click 'Add new phrase' button
    $(document).on('click', '#addNewPhraseButton', function () {
        var phrases = [];
        var text = $('#phraseText').val();
        text = text.replace(/"/g, '\\"');
        var lines = text.split('\n');
        for(var i = 0;i < lines.length;i++){
            if (lines[i] != '') {
                phrases.push(encodeURIComponent(lines[i]));
            }
        }
        var phrasesJson = JSON.stringify(phrases);
        $.ajax({
            url: '/phrases/create-phrases',
            type: "POST",
            data: 'groupPhrasesId=' + currentGroupPhrasesId + '&phrases=' + phrasesJson,
            success: function (data) {
                if (data == 'true') {
                    updatePhrases();
                } else {
                    alert('An error occurred while creating the phrases!');
                }
            }
        });
    });

    //Click 'Update phrase' button
    $(document).on('click', '#updatePhraseButton', function () {
        var text = $('#phraseText').val();
        text = encodeURIComponent(text);
        var phraseId = $('#phrasesBindableForm input[name="phraseId"]').val();
        if (phraseId.length == 0) {
            return;
        }
        $.ajax({
            url: '/phrases/update-phrase',
            type: "POST",
            data: 'phraseId=' + phraseId + '&text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updatePhrases();
                } else {
                    alert('An error occurred while updating the phrase!');
                }
            }
        });
    });

    //Click delays dropdpwn item
    $(document).on('click', '#availableDelays li', function () {
        var phrasesIds = getCheckedOrCurrentItemsIds('#phrases', 'phraseId', '#phrasesBindableForm');
        if (phrasesIds.length == 0) {
            return;
        }
        var delayId = $(this).find('input[type="hidden"]').val();
        delayId = (delayId == -1) ? null : delayId;
        var params = [];
        $.each(phrasesIds, function (index, value) {
            var elem = {};
            elem.phrase_id = value;
            elem.delay_id = delayId;
            params.push(elem);
        });
        $.ajax({
            url: '/phrases/update-phrases',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updatePhrases();
                } else {
                    alert('An error occurred while updating the phrases!');
                }
            }
        });
    });

    //Click timeslots dropdpwn item
    $(document).on('click', '#availableTimeslots li', function () {
        var phrasesIds = getCheckedOrCurrentItemsIds('#phrases', 'phraseId', '#phrasesBindableForm');
        if (phrasesIds.length == 0) {
            return;
        }
        var timeslotId = $(this).find('input[type="hidden"]').val();
        timeslotId = (timeslotId == -1) ? null : timeslotId;
        var params = [];
        $.each(phrasesIds, function (index, value) {
            var elem = {};
            elem.phrase_id = value;
            elem.timeslot_id = timeslotId;
            params.push(elem);
        });
        $.ajax({
            url: '/phrases/update-phrases',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updatePhrases();
                } else {
                    alert('An error occurred while updating the phrases!');
                }
            }
        });
    });

    //Click phrases delete button
    $(document).on('click', '#deletePhraseButton', function () {
        if (getCheckedOrCurrentItemsIds('#phrases', 'phraseId', '#phrasesBindableForm').length == 0) {
            return;
        }
        var phrasesNames = getCheckedOrCurrentItemsNames('#phrases', 'phraseId', '#phrasesBindableForm');
        $('#deletePhrasesNames').html(phrasesNames);
        $('#confirmDeletePhrasesModal').modal('show');
    });

    //Click confirm phrases delete button
    $(document).on('click', '#confirmDeletePhrasesButton', function () {
        var phrasesIds = getCheckedOrCurrentItemsIds('#phrases', 'phraseId', '#phrasesBindableForm');;
        if (phrasesIds.length == 0) {//Its not necessary because its checked in deletePhraseButton click
            return;
        }
        var params = [];
        $.each(phrasesIds, function (index, value) {
            var elem = {};
            elem.phrase_id = value;
            params.push(elem);
        });
        $.ajax({
            url: '/phrases/delete-phrases',
            type: "POST",
            data: 'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updatePhrases();
                } else {
                    alert('An error occurred while deleting the phrases!');
                }
                $('#confirmDeletePhrasesModal').modal('hide');
            }
        });
    });
});