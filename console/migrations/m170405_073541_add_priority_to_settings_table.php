<?php

use yii\db\Migration;

class m170405_073541_add_priority_to_settings_table extends Migration
{
    public function up()
    {
        $this->addColumn('settings_table', 'priority', $this->smallInteger()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('settings_table', 'priority');
    }
}
