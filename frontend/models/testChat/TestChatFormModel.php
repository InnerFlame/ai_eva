<?php

namespace frontend\models\testChat;

use common\components\TestChatMessageProcessorComponent;
use frontend\models\BaseNgForm;
use frontend\models\Dialogues;
use frontend\models\settings\Setting;
use frontend\models\ShownLinks;

class TestChatFormModel extends BaseNgForm
{
    /* @var $testChatMessageProcessor TestChatMessageProcessorComponent */
    private $testChatMessageProcessor;

    /**
     * @inheritdoc
     *
     * TestChatFormModel constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        /* @var $testChatMessageProcessor TestChatMessageProcessorComponent */
        $testChatMessageProcessor = \Yii::$app->testChatMessageProcessor;
        $this->setTestChatMessageProcessor($testChatMessageProcessor);
    }

    /**
     * @return array
     */
    public function getScenarios()
    {
        $scenarios          = [];
        $availableScenarios = \frontend\models\DataProvider::getInstance()->getScenarios();

        foreach ($availableScenarios as $availableScenario) {
            $scenarios[] = [
                'id'   => $availableScenario->getId(),
                'name' => $availableScenario->getName()
            ];
        }

        return $scenarios;
    }

    public function getGroupPhrases(): array
    {
        return $this->getBrungildaApi()->getGroupPhrases();
    }

    /**
     * @param $settingId
     * @param $fromUserId
     * @param $toUserId
     * @param $text
     * @param $scenarioId
     * @return array
     */
    public function getResponseData(
        int $settingId,
        string $fromUserId,
        string $toUserId,
        string $text,
        int $scenarioId
    )
    {
        $fromUserId = self::formatUserId($fromUserId);
        $toUserId   = self::formatUserId($toUserId);

        if (preg_match('/[^0-9a-f]/', $fromUserId)) {
            return [];
        }

        if (preg_match('/[^0-9a-f]/', $toUserId)) {
            return [];
        }

        if (empty($settingId)) {
            return  $this->getBrungildaApi()->chat(
                $fromUserId,
                $toUserId,
                $text,
                (int) $scenarioId
            );
        }

        $response = [];
        $data     = self::createPostData($settingId, $fromUserId, $toUserId, $text);

        $this->getTestChatMessageProcessor()->getAnswerData($data, $response);

        return $response;
    }

    private function formatUserId(string $userIdString): string
    {
        return str_pad($userIdString, 32, '0', STR_PAD_LEFT);
    }

    /**
     * @param $settingId
     * @param $fromUserId
     * @param $toUserId
     * @param $text
     * @return array
     */
    protected function createPostData(int $settingId, string $fromUserId, string $toUserId, string $text)
    {
        /* @var $setting Setting */
        $setting = Setting::findOne((int) $settingId);

        $data = [
            'from'               => $fromUserId,
            'multi_profile_id'   => $fromUserId,
            'to'                 => $toUserId,
            'country'            => $setting->countries[0]->country_name,
            'site'               => $setting->sites[0]->site_hash,
            'text'               => $text ?? '',
            'profiles' => [
                $fromUserId       => [
                    'id'                => $fromUserId,
                    'multi_profile_id'  => $fromUserId,
                    'site'              => $setting->sites[0]->site_hash,
                    'country'           => $setting->countries[0]->country_name,
                    'language'          => $setting->locales[0]->locale_name,
                    'paid'              => $setting->statuses[0]->status_name,
                    'platform'          => $setting->platforms[0]->platform_name,
                    'traff_src'         => $setting->trafficSources[0]->traffic_source_name,
                    'action_way'        => $setting->actionWays[0]->action_way_name,
                ],
                $toUserId    => [
                    'id'                => $toUserId,
                    'multi_profile_id'  => $toUserId,
                    'site'              => $setting->sites[0]->site_hash,
                    'country'           => $setting->countries[0]->country_name,
                    'platform'          => $setting->platforms[0]->platform_name,
                    'traff_src'         => $setting->trafficSources[0]->traffic_source_name,
                    'action_way'        => $setting->actionWays[0]->action_way_name,
                    'city'              => '',
                    'bday'              => '',
                    'time_offset'       => '0',

                    'control_profile_type'  => $setting->control_profile,
                    'setting_id'            => $setting->id,
                ],
            ],
        ];

        return $data;
    }

    public function deleteChat($fromId)
    {
        $fromUserId = self::formatUserId($fromId);

        Dialogues::deleteAll(['idFrom' => $fromUserId]);
        ShownLinks::deleteAll(['userId' => $fromUserId]);

        $params['from'] = $fromUserId;
        $response = $this->getBrungildaApi()->deleteChat($params);

        return $response;
    }

    /**
     * @return mixed
     */
    public function getTestChatMessageProcessor(): TestChatMessageProcessorComponent
    {
        return $this->testChatMessageProcessor;
    }

    /**
     * @param mixed $testChatMessageProcessor
     */
    public function setTestChatMessageProcessor($testChatMessageProcessor)
    {
        $this->testChatMessageProcessor = $testChatMessageProcessor;
    }
}
