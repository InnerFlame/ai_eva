$(document).ready(function () {
    var progressBarHtml = '<div class="progress-bar progress-bar-striped active" role="progressbar"'
        + 'aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">'
        + 'Search...'
        + '</div>';

    $(document).on('beforeSubmit', '#usersIdSearch', function () {
        $('#searchResults').html(progressBarHtml);
        $('#messages').html('');
        return true;
    });

    $(document).on('click', '.view-messages', function (e) {
        e.preventDefault();
        $('#messages').html(progressBarHtml);
        $('html, body').animate({scrollTop: $("#messages").offset().top}, 500);
        $.pjax.reload({
            container: '#messages',
            url: $(this).attr('href'),
            replace: false,
            timeout: 30000
        });

        $(this).closest('table').find('tr').removeClass('highlight');
        $(this).closest('tr').addClass('highlight');

        return false;
    });
});
