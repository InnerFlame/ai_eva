<?php
declare(strict_types=1);

namespace frontend\models\phrases;

class Phrase
{
    private $id;
    private $groupPhrasesId;
    private $text;
    private $delayId;
    private $timeslotId;
    private $version;
    private $deleted;

    /**
     * Phrase constructor.
     * @param $id
     * @param $groupPhrasesId
     * @param $text
     * @param $delayId
     * @param $timeslotId
     * @param $version
     * @param $deleted
     */
    public function __construct(
        int $id,
        int $groupPhrasesId,
        string $text,
        int $delayId,
        int $timeslotId,
        int $version,
        bool $deleted
    ) {
        $this->id = $id;
        $this->groupPhrasesId = $groupPhrasesId;
        $this->text = $text;
        $this->delayId = $delayId;
        $this->timeslotId = $timeslotId;
        $this->version = $version;
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getGroupPhrasesId() : int
    {
        return $this->groupPhrasesId;
    }

    /**
     * @param int $groupPhrasesId
     */
    public function setGroupPhrasesId(int $groupPhrasesId)
    {
        $this->groupPhrasesId = $groupPhrasesId;
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getDelayId() : int
    {
        return $this->delayId;
    }

    /**
     * @param int $delayId
     */
    public function setDelayId(int $delayId)
    {
        $this->delayId = $delayId;
    }

    /**
     * @return int
     */
    public function getTimeslotId() : int
    {
        return $this->timeslotId;
    }

    /**
     * @param int $timeslotId
     */
    public function setTimeslotId(int $timeslotId)
    {
        $this->timeslotId = $timeslotId;
    }

    /**
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * @return bool
     */
    public function getDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }
}
