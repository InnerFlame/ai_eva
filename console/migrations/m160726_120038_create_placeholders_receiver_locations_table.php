<?php

use yii\db\Migration;

/**
 * Handles the creation for table `placeholders_receiver_locations_table`.
 */
class m160726_120038_create_placeholders_receiver_locations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('placeholders_receiver_locations_table', [
            'id' => $this->primaryKey(),
            'placeholder_id' => $this->integer(11),
            'receiver_location_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('placeholders_receiver_locations_table');
    }
}
