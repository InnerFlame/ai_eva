function getCookie(name)
{
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options)
{
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function deleteCookie(name)
{
    setCookie(name, "", {
        expires: -1
    })
}

/**
 * Sends test chat message and renders it with the answer.
 *
 * @param string result Rendered html.
 *
 * @return void
 */
function sendChat(result)
{
    if (typeof result != "undefined") {
        $("#dialogue").prepend(result);
        $("#testchatformmodel-text").val("");
    }
}

function clearChat(result)
{
    if (result) {
        $("#dialogue").html('');
        $("#testchatformmodel-text").val("");
    }
}

$(document).on('focusout', '#testchatformmodel-fromuserid, #testchatformmodel-touserid', function (e) {
    var val = $(this).val();
    var length = val.length;
    if (length <= 32) {
        var before = Array(32 - length + 1).join("0");
        $(this).val(before + val);
    } else {
        $(this).val(val.substr(length-32,32));
    }
    setCookie('testChatFromId', $('#testchatformmodel-fromuserid').val());
    setCookie('testChatToId', $('#testchatformmodel-touserid').val());
});

$(document).on('change', '#testchatformmodel-scenario', function (e) {
    setCookie('testChatScenarioValue', $('#testchatformmodel-scenario').val());
});

$(document).ready(function () {
    if (getCookie('testChatFromId') !== undefined) {
        $('#testchatformmodel-fromuserid').val(getCookie('testChatFromId'));
    }
    if (getCookie('testChatToId') !== undefined) {
        $('#testchatformmodel-touserid').val(getCookie('testChatToId'));
    }
    if (getCookie('testChatScenarioValue') !== undefined) {
        $('#testchatformmodel-scenario').val(getCookie('testChatScenarioValue'))
    }
});
