<?php
declare(strict_types=1);

namespace frontend\controllers;

use frontend\models\settings\SettingsForm;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class SettingsController extends BaseController
{
    protected $model;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->model = new SettingsForm();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('readSettings')) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        return $this->render('index', ['model' => $this->model]);
    }

    public function actionSettings()
    {
        $this->model->getSettings();
        return $this->renderAjax('_settings', ['model' => $this->model]);
    }

    public function actionCreateSetting()
    {
        if (!\Yii::$app->user->can('editSettings')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->createSetting($post['name'], $post['controlProfile'], $post['targetProfile']);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionUpdateSetting()
    {
        if (!\Yii::$app->user->can('editSettings')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->updateSetting(
                $post['id'],
                $post['name'],
                $post['controlProfile'],
                $post['targetProfile'],
                explode(",", $post['projects']),
                explode(",", $post['countries']),
                explode(",", $post['locales']),
                explode(",", $post['statuses']),
                explode(",", $post['sites']),
                explode(",", $post['trafficSources']),
                explode(",", $post['actionWays']),
                explode(",", $post['trafficTypes']),
                explode(",", $post['platforms']),
                explode(",", $post['fanClubs']),
                explode(",", $post['onlines']),
                json_decode($post['scenarios'], true),
                $post['active']
            );
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionDeleteSetting()
    {
        if (!\Yii::$app->user->can('editSettings')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->deleteSetting($post['id']);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionCombineSettings()
    {
        if (!\Yii::$app->user->can('editSettings')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $response = $this->model->combineSettings();
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionUpdateSettingsPriority()
    {
        if (!\Yii::$app->user->can('editSettings')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $params = json_decode($post['params'], true);
            $response = $this->model->updatePriority($params);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    public function actionCopySetting()
    {
        if (!\Yii::$app->user->can('editSettings')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $request = \Yii::$app->request;
        if ($request->isAjax) {
            $post = $request->post();
            $response = $this->model->copySetting($post['id']);
            if ($response) {
                return 'true';
            }
            return 'false';
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }
}
