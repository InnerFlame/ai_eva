<?php

namespace common\widgets\Column;

use yii\bootstrap\Html;
use \yii\bootstrap\Widget;

class Column extends Widget
{
    public $listId;
    public $items;
    public $collapsed;
    public $lastItemText;
    public $lastItemOptions;

    public function init()
    {
        parent::init();
        if ($this->items === null) {
            $this->items = array();
        }
        if ($this->collapsed === null) {
            $this->collapsed = false;
        }
        $view = $this->getView();
        ColumnAsset::register($view);
    }

    private function renderItems()
    {
        $listOptions = ['class' => 'list-group column', 'style' => $this->collapsed ? 'margin-top: 53px' : ''];
        if (isset($this->listId)) {
            $listOptions['id'] = $this->listId;
        }
        $content = Html::beginTag('ul', $listOptions);
        foreach ($this->items as $i => $item) {
            $content .= $this->renderIndexItem($i, $item);
        }
        if (isset($this->lastItemText)) {
            if (isset($this->lastItemOptions['class'])) {
                $this->lastItemOptions['class'] .= ' list-group-item last-item';
            } else {
                $this->lastItemOptions['class'] = 'list-group-item last-item';
            }
            $content .= Html::beginTag('li', $this->lastItemOptions);
            $content .= $this->lastItemText;
            $content .= Html::endTag('li');
        }
        $content .= Html::endTag('ul');
        return $content;
    }

    private function renderIndexItem($index, $item)
    {
        $collapseItemsId = $this->options['id'] . '-' . $index . '-collapse';
        $content = Html::beginTag('li', ['class' => 'list-group-item sortable',]);

        //Hidden fields
        if (isset($item['hiddens'])) {
            foreach ($item['hiddens'] as $hidden) {
                $content .= Html::hiddenInput($hidden['name'], $hidden['value']);
            }
        }

        //Items header
        $content .= Html::tag('span', $item['label'] ?? '', $item['labelOptions'] ?? []);
        $content .= Html::beginTag('span', ['class' => 'pull-right',]);
        foreach ($item['header'] as $headerItem) {
            $content .= Html::tag('span', $headerItem['label'] ?? '', [
                'class' => $headerItem['class'] ?? '',
                'name' => $headerItem['name'] ?? ''
            ]);
        }
        $content .= Html::beginTag('a', [
            'aria-expanded' => 'false',
            'data-toggle' => 'collapse',
            'href' => '#' . $collapseItemsId,
        ]);
        $content .= Html::tag('span', '', ['class' => 'glyphicon glyphicon-chevron-right']);
        $content .= Html::endTag('a');
        $content .= Html::endTag('span');

        //Items collapsed content
        $content .= Html::beginTag('form', ['class' => 'form-horizontal collapse', 'id' => $collapseItemsId]);
        $content .= Html::tag('br');
        foreach ($item['lines'] as $line) {
            $content .= Html::beginTag('div', ['class' => 'form-group',]);
            foreach ($line['elements'] as $element) {
                $content .= Html::tag($element['tag'], $element['content'] ?? '', $element['options'] ?? []);
            }
            $content .= Html::endTag('div');
        }
        $content .= Html::endTag('form');
        $content .= Html::endTag('li');
        return $content;
    }

    public function run()
    {
        $content = $this->renderItems();
        return $content;
    }
}
