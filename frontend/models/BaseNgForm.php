<?php

namespace frontend\models;

use common\components\BrungildaApi\BrungildaApiCurl;
use yii\base\Model;
use yii\caching\Cache;

/**
 * Class BaseNgForm
 * @package frontend\models\nlpClassifiers
 */
class BaseNgForm extends Model
{
    const SCENARIOS_CACHE_KEY = 'scenarios_cache_key';

    /* @var $brungildaApiCurl BrungildaApiCurl */
    private $brungildaApiCurl;

    /* @var $cache Cache */
    protected $cache;

    /**
     * @inheritdoc
     *
     * BaseNgForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->brungildaApiCurl = \Yii::$app->BrungildaApiCurl;
        $this->cache            = \Yii::$app->cache;
    }

    /**
     * @return BrungildaApiCurl
     */
    protected function getBrungildaApi(): BrungildaApiCurl
    {
        return $this->brungildaApiCurl;
    }

    /**
     * @param $a
     * @param $b
     * @return int
     */
    protected function comparePriorityForSort($a, $b)
    {
        if ($a['priority'] == $b['priority']) {
            return 0;
        }

        return ($a['priority'] < $b['priority']) ? -1 : 1;
    }

    /**
     * @param $inputData array
     * @return array
     */
    protected function filterDataForMultiSelects(array $inputData): array
    {
        $out = [];
        foreach ($inputData as $datum) {
            $out[] = [
                'id'   => $datum,
                'name' => $datum
            ];
        }

        return $out;
    }

    /**
     * Get available scenarios for form dropDown
     *
     * @return array
     */
    protected function getScenariosForHolder(): array
    {
        $scenarios          = [];
        $availableScenarios = $this->getScenarios();

        /* @var $availableScenarios \frontend\models\scenarios\Scenario[] */
        foreach ($availableScenarios as $availableScenario) {
            $scenarios[] = [
                'id'   => $availableScenario['scenario_id'],
                'name' => $availableScenario['name']
            ];
        }

        return $scenarios;
    }

    /**
     * @param string $idKey
     * @param string $nameKey
     * @param array $inputData
     * @return array
     */
    protected function filterDataForMultiSelectsWithStrictKeys(string $idKey, string $nameKey, array $inputData): array
    {
        $filteredData = [];

        foreach ($inputData as $datum) {
            $filteredData[] = [
                'id'   => $datum[$idKey],
                'name' => $datum[$nameKey]
            ];
        }

        return $filteredData;
    }

    /**
     * Get scenarios and store it to cache for next get request
     *
     * @return array|mixed
     */
    private function getScenarios()
    {
        $scenarios = $this->cache->get('scenarios_cache_key');

        if ($scenarios === false) {
            $scenarios = $this->getBrungildaApi()->getScenarios();
            $this->cache->set(self::SCENARIOS_CACHE_KEY, $scenarios, 900);
        }

        return $scenarios;
    }
}
