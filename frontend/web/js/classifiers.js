// Supporting variables
var currentClassifierId         = null;
var currentClassifierPatternId  = null;
var allClassifierPatternItemsWasChecked = false;

// Load list of classifiers
function updateClassifiers()
{
    $.ajax({
        url: '/classifiers/classifiers',
        success: function (data) {
            // Update classifiers
            $('#classifiers').html(data);
            // Trigger click on first or current classifier item
            var classifierItem;
            if (currentClassifierId == null) {
                classifierItem = $('#classifiers li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="classifierId"][value="' + currentClassifierId + '"]';
                classifierItem = $('#classifiers li.list-group-item.sortable').has(str);
            }
            if (classifierItem.length == 0) {
                classifierItem = $('#classifiers li.list-group-item.sortable:first-child');
            }
            if (classifierItem.length == 0) {
                currentClassifierId = null;
            } else {
                classifierItem.removeClass('lastClickedItem');
                classifierItem.click();
            }
        }
    });
}

// Load list of classifier patterns
function updateClassifierPatterns()
{
    $.ajax({
        url:     '/classifiers/classifier-patterns',
        type:    "POST",
        data:    'classifierId=' + currentClassifierId,
        success: function (data) {
            // Update classifierPatterns
            $('#classifierPatterns').html(data);
            // Trigger click on first or current  classifier pattern item
            var classifierPatternItem;
            if (currentClassifierPatternId == null) {
                classifierPatternItem = $('#classifierPatterns li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="classifierPatternId"][value="' + currentClassifierPatternId + '"]';
                classifierPatternItem = $('#classifierPatterns li.list-group-item.sortable').has(str);
            }
            if (classifierPatternItem.length == 0) {
                classifierPatternItem = $('#classifierPatterns li.list-group-item.sortable:first-child');
            }
            if (classifierPatternItem.length == 0) {
                currentClassifierPatternId = null;
            } else {
                classifierPatternItem.removeClass('lastClickedItem');
                classifierPatternItem.click();
            }
        }
    });
}

//Get array of checked object ids
function getCheckedObjectsIds(listDivId, listItemObjectIdName)
{
    var checkedObjectsIds = [];
    $('#' + listDivId +' li.list-group-item input[name="itemCheckbox"]:checked').each(function () {
        var id = $(this).closest('li.list-group-item').find('input[name="' + listItemObjectIdName + '"]').val();
        checkedObjectsIds.push(id);
    });
    return checkedObjectsIds;
}

//Get array of checked or current object ids
function getCheckedOrCurrentObjectsIds(bindableFormId, bindableFormObjectIdName, listDivId, listItemObjectIdName)
{
    //Get checked objects
    var checkedOrCurrentObjectsIds = getCheckedObjectsIds(listDivId, listItemObjectIdName);
    //If there are no checked objects get current objects
    if (checkedOrCurrentObjectsIds.length == 0) {
        var currentObjectId = $('#' + bindableFormId +' input[name="' + bindableFormObjectIdName + '"]').val();
        if (currentObjectId != 0) {
            checkedOrCurrentObjectsIds.push(currentObjectId);
        }
    }
    return checkedOrCurrentObjectsIds
}

//Get string of checked or current items names (for modal window)
function getCheckedOrCurrentItemsNames(bindableFormId, bindableFormObjectIdName, listDivId, listItemObjectIdName)
{
    var itemsNames = '';
    var itemsIds = getCheckedOrCurrentObjectsIds(bindableFormId, bindableFormObjectIdName, listDivId, listItemObjectIdName);
    $.each(itemsIds, function (index, value) {
        var itemsName = $('#' + listDivId + ' ul li:visible input[name="' + listItemObjectIdName + '"][value="' + value + '"]').siblings('span[name="itemText"]').html();
        itemsNames += (index + 1) + '. ' + itemsName + '<br/>'
    });
    return itemsNames;
}

$(document).ready(function () {
    /********** CLASSIFIERS FUNCTIONALITY **********/
    // Load list of classifiers
    updateClassifiers();

    // Click on classifiers item event
    $(document).on('click', '#classifiers li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        var classifierId = $(this).children('input[name="classifierId"]').val();
        if (classifierId === undefined) {
            currentClassifierId = null;
            return;
        }
        currentClassifierId = classifierId;
        updateClassifierPatterns();
    });

    // Click 'Add new classifiers' button
    $(document).on('click', '#addNewClassifiersButton', function () {
        var text = $('#classifiersText').val();
        text = encodeURIComponent(text);
        $.ajax({
            url:     '/classifiers/create-classifiers',
            type:    "POST",
            data:    'text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateClassifiers();
                } else {
                    alert('An error occurred while creating the classifiers!');
                }
            }
        });
    });

    // Click 'Update classifiers' button
    $(document).on('click', '#updateClassifiersButton', function () {
        var text         = $('#classifiersText').val();
        text = encodeURIComponent(text);
        var classifierId = $('#classifiersBindableForm input[name="classifierId"]').val();
        if (classifierId.length == 0) {
            return;
        }
        $.ajax({
            url:     '/classifiers/update-classifiers',
            type:    "POST",
            data:    'classifierId=' + classifierId + '&text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateClassifiers();
                } else {
                    alert('An error occurred while updating the classifiers!');
                }
            }
        });
    });

    // Click classifiers delete button
    $(document).on('click', '#deleteClassifiersButton', function () {
        var classifiersName = $('#classifiersText').val();
        var classifierId    = $('#classifiersBindableForm input[name="classifierId"]').val();
        if (classifierId.length == 0) {
            return;
        }
        $('#deleteClassifierId').val(classifierId);
        $('#deleteClassifiersName').html(classifiersName);
        $('#confirmDeleteClassifiersModal').modal('show');
    });

    // Click confirm classifiers delete button
    $(document).on('click', '#confirmDeleteClassifiersButton', function () {
        var classifierId = $('#deleteClassifierId').val();
        if (classifierId.length == 0) {
            return;
        }
        $.ajax({
            url:     '/classifiers/delete-classifiers',
            type:    "POST",
            data:    'classifierId=' + classifierId,
            success: function (data) {
                if (data == 'true') {
                    updateClassifiers();
                } else {
                    alert('An error occurred while deleting the classifiers!');
                }
                $('#confirmDeleteClassifiersModal').modal('hide');
            }
        });
    });

    /**********  CLASSIFIER PATTERNS FUNCTIONALITY **********/
    // Click on classifier patterns item event
    $(document).on('click', '#classifierPatterns li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        var classifierPatternId = $(this).children('input[name="classifierPatternId"]').val();
        if (classifierPatternId === undefined) {
            currentClassifierPatternId = null;
            return;
        }
        currentClassifierPatternId = classifierPatternId;
    });

    // Click check regular expression button
    $(document).on('click', '#checkRegularExpressionButton', function () {
        var test = $('#classifierPatternTest').val();
        var text = $('#classifierPatternText').val();
        checkRegularExpression(test, text);
    });

    function checkRegularExpression(test, text, hide) {
        var success = test.match(text) !== null;

        hide = hide || false;
        if (!hide) {
            if (success) {
                $('#regularExpressionCheckModal .modal-body strong').html('<p class="text-success">Success. The regular expression and string entered is match.</p>');
            } else {
                $('#regularExpressionCheckModal .modal-body strong').html('<p class="text-danger">Error. The regular expression and string entered do not match.</p>');
            }
            $('#regularExpressionCheckModal').modal('show');
        }

        return success;
    }

    // Click 'Add new classifier pattern' button
    $(document).on('click', '#addNewClassifierPatternButton', function () {
        var testRaw = $('#classifierPatternTest').val();
        var textRaw = $('#classifierPatternText').val();
        test = encodeURIComponent(testRaw);
        text = encodeURIComponent(textRaw);

        if (checkRegularExpression(testRaw, textRaw, true)) {
            $.ajax({
                url:    '/classifiers/create-classifier-patterns',
                type:   "POST",
                data:   'сlassifierId=' + currentClassifierId + '&text=' + text + '&test=' + test,
                success: function (data) {
                    if (data == 'true') {
                        updateClassifierPatterns();
                    } else {
                        alert('An error occurred while creating the classifier pattern!');
                    }
                }
            });
        }
    });

    // Click 'Update classifier pattern' button
    $(document).on('click', '#updateClassifierPatternButton', function () {
        var testRaw             = $('#classifierPatternTest').val();
        var textRaw             = $('#classifierPatternText').val();
        var classifierPatternId = $('#classifierPatternsBindableForm input[name="classifierPatternId"]').val();
        if (classifierPatternId.length == 0) {
            return;
        }

        var text = encodeURIComponent(textRaw);
        var test = encodeURIComponent(testRaw);

        if (checkRegularExpression(testRaw, textRaw, true)) {
            $.ajax({
                url:     '/classifiers/update-classifier-pattern',
                type:    "POST",
                data:    'classifierPatternId=' + classifierPatternId + '&text=' + text + '&test=' + test,
                success: function (data) {
                    if (data == 'true') {
                        updateClassifierPatterns();
                    } else {
                        alert('An error occurred while updating the classifier pattern!');
                    }
                }
            });
        }
    });

    // Click classifier patterns delete button
    $(document).on('click', '#deleteClassifierPatternButton', function () {
        var classifierPatternNames = getCheckedOrCurrentItemsNames('classifierPatternsBindableForm', 'classifierPatternId', 'classifierPatterns', 'classifierPatternId');
        $('#deleteClassifierPatternsNames').html(classifierPatternNames);
        $('#confirmDeleteClassifierPatternsModal').modal('show');
    });

    // Click confirm classifier patterns delete button
    $(document).on('click', '#confirmDeleteClassifierPatternsButton', function () {
        var classifierPatternIds = getCheckedOrCurrentObjectsIds('classifierPatternsBindableForm', 'classifierPatternId', 'classifierPatterns', 'classifierPatternId');
        if (classifierPatternIds.length == 0) {
            return;
        }
        var params = [];
        $.each(classifierPatternIds, function (index, value) {
            var elem = {};
            elem.classifier_pattern_id = value;
            params.push(elem);
        });

        $.ajax({
            url:     '/classifiers/delete-classifier-patterns',
            type:    "POST",
            data:    'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updateClassifierPatterns();
                } else {
                    alert('An error occurred while deleting the classifier patterns!');
                }
                $('#confirmDeleteClassifierPatternsModal').modal('hide');
            }
        });
    });

    // Click classifier pattern active/test radioButton
    $(document).on('click', '#updateClassifierPatternActiveTestRadioButton', function () {
        // Check if this is real click (not bindable click from clearBindableForm or bindableFormItemClick functions)
        if ($(this).hasClass('active') != $('#classifierPatterns').find('ul li.current span[name="activeClassifierPatternIcon"]').hasClass('active')) {
            var classifierPatternIds = getCheckedOrCurrentObjectsIds('classifierPatternsBindableForm', 'classifierPatternId', 'classifierPatterns', 'classifierPatternId');
            if (classifierPatternIds.length == 0) {
                return;
            }
            var active = 0;
            if ($(this).hasClass('active')) {
                active = 1;
            }
            var params = [];
            $.each(classifierPatternIds, function (index, value) {
                var elem = {};
                elem.classifier_pattern_id = value;
                elem.active = active;
                params.push(elem);
            });

            $.ajax({
                url:    '/classifiers/update-classifier-patterns',
                type:   "POST",
                data:   'params=' + JSON.stringify(params),
                success: function (data) {
                    if (data == 'true') {
                        updateClassifierPatterns();
                    } else {
                        alert('An error occurred while updating the classifier pattern!');
                    }
                }
            });
        }
    });

    //Click check all patterns button
    $(document).on('click', '#checkAllClassifierPatternsButton', function () {
        if (allClassifierPatternItemsWasChecked) {
            allClassifierPatternItemsWasChecked = false;
            $('#classifierPatterns li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allClassifierPatternItemsWasChecked = true;
            $('#classifierPatterns li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });
});
