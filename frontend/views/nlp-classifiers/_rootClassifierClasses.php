<?php
use common\widgets\BindableFormItems\BindableFormItems;

$classifierClassesItems = [];

foreach ($model->classes as $class) {
    $item['text'] = $class->text;
    $item['hiddens'] = [
        ['name' => 'rootClassifierId',      'value' => $class->classifierId],
        ['name' => 'rootClassId',           'value' => $class->id],
        ['name' => 'rootItemProbability',   'value' => $class->probability],
    ];
    $classifierClassesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId'    => 'rootClassesBindableForm',
    'items'             => $classifierClassesItems,
]);
