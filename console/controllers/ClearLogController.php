<?php
namespace console\controllers;

use frontend\models\commonInfo\CommunicationLog;
use yii\console\Controller;

class ClearLogController extends Controller
{
    /**
     * php yii clear-log/clear-partitions
     */
    public function actionClearPartitions()
    {
        $presentPartitions = $this->getPartitions();

        $expectedPartitions = [];
        for ($i = -7; $i < 0; $i++) {
            $expectedPartitions[] = 'p' . (new \DateTime("$i day"))->format('Ymd');
        }
        for ($i = 0; $i < 7; $i++) {
            $expectedPartitions[] = 'p' . (new \DateTime("+ $i day"))->format('Ymd');
        }

        $partitionsToRemove = array_diff($presentPartitions, $expectedPartitions);

        if (!empty($partitionsToRemove)) {
            $dropPartitionsSql = sprintf('ALTER TABLE %s DROP PARTITION %s', CommunicationLog::tableName(), implode(',', $partitionsToRemove));
            \Yii::$app->db->createCommand($dropPartitionsSql)->execute();
        }
    }

    /**
     * php yii clear-log/check-partitions
     */
    public function actionCheckPartitions()
    {
        $presentPartitions = $this->getPartitions();

        $expectedPartitions = [];
        for ($i = 0; $i < 7; $i++) {
            $expectedPartitions[] = 'p' . (new \DateTime("+ $i day"))->format('Ymd');
        }
        $missedPartitions = array_diff($expectedPartitions, $presentPartitions);
        if (!empty($missedPartitions)) {
            $partitions = [];
            foreach ($missedPartitions as $partitionName) {
                $partitionTime = \DateTime::createFromFormat('pYmd H:i:s', $partitionName . ' 23:59:59')->getTimestamp();
                $partitions[] = sprintf('PARTITION %s VALUES LESS THAN (%s) ENGINE = InnoDB', $partitionName, $partitionTime);
            }
            $addPartitionsSql = sprintf('ALTER TABLE %s ADD PARTITION(%s)', CommunicationLog::tableName(), implode(',', $partitions));
            \Yii::$app->db->createCommand($addPartitionsSql)->execute();
        }
    }

    private function getPartitions()
    {
        $partitionsData = \Yii::$app->db
            ->createCommand('EXPLAIN PARTITIONS SELECT * FROM `' . CommunicationLog::tableName() . '`')
            ->queryOne();

        if(empty($partitionsData)) {
            return false;
        }

        $partitions = explode(',', $partitionsData['partitions']);
        sort($partitions);
        return $partitions;
    }
}
