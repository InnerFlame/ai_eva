#!/usr/bin/env bash

# cd proect root dir
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)/../"

RUN_ARGS=""
SCALE_SERVICES=""
DOCKER_COMPOSE_FILES=""
if [ "$DOCKER_ENV" == "prod" ]; then
    source ./bin/prodEnv.sh
fi

DC="docker-compose ${DOCKER_COMPOSE_FILES}"

${DC} pull
${DC} build --pull --force-rm
${DC} run app_build
${DC} rm -f app_build
${DC} down
${DC} up --scale app_build=0 --timeout=120 ${RUN_ARGS} ${SCALE_SERVICES}

if [ "$DOCKER_ENV" == "prod" ]; then
    ${DC} run ng_app_build
    ${DC} rm -f ng_app_build
else
    ${DC} down;
fi
