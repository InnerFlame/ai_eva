<?php

use yii\db\Migration;

class m160831_122121_add_placeholder_url_text_column extends Migration
{
    public function up()
    {
        $this->addColumn('placeholders_table', 'url_text', $this->string()->after('url'));
    }

    public function down()
    {
        $this->dropColumn('placeholders_table', 'url_text');
    }
}
