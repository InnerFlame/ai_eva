<?php
declare(strict_types=1);

namespace frontend\models\placeholders;

use frontend\models\BaseForm;
use frontend\models\DataProvider;

class PlaceholdersForm extends BaseForm
{
    public $placeholders = array();

    public function __construct()
    {
    }

    public function getPlaceholders()
    {
        $this->placeholders = Placeholder::find()->orderBy('id')->all();
    }

    public function getPlaceholdersByParams($controlProfileType, $receiverLocation, $receiverPlatform)
    {
        $query = 'SELECT DISTINCT p.name, p.url_type, p.id, ';
        if ($receiverPlatform == 'webSite') {
            $query .= 'p.url ';
        } else {
            $query .= 'p.url_wap AS url ';
        }
        $query .= 'FROM placeholders_table AS p ';
        $query .= 'JOIN placeholders_sender_types_table AS pst ON p.id = pst.placeholder_id ';
        $query .= 'JOIN placeholders_receiver_locations_table AS prl ON p.id = prl.placeholder_id ';
        $query .= 'WHERE pst.sender_type_name = :sender_type_name ';
        $query .= 'AND prl.receiver_location_name = :receiver_location_name ';
        $query .= 'AND p.active = true;';
        $placeholders = \Yii::$app->db->createCommand($query)
            ->bindValue(':sender_type_name', $controlProfileType)
            ->bindValue(':receiver_location_name', $receiverLocation)
            ->queryAll();

        return $placeholders;
    }

    public function createPlaceholder(
        $name,
        $type,
        $url,
        $urlWap,
        $urlType,
        $senderTypes,
        $receiverLocations,
        $isLink
    ) {
        $placeholder = new Placeholder();
        $placeholder->name = $name;
        $placeholder->type = $type;
        $placeholder->url = $url;
        $placeholder->url_type = $urlType;
        $placeholder->active = false;
        $placeholder->url_wap = $urlWap;
        $placeholder->is_link = $isLink;
        $placeholder->save();

        foreach ($senderTypes as $value) {
            $senderType = new SenderType();
            $senderType->placeholder_id = $placeholder->id;
            $senderType->sender_type_name = $value;
            $senderType->save();
        }

        foreach ($receiverLocations as $value) {
            $receiverLocation = new ReceiverLocation();
            $receiverLocation->placeholder_id = $placeholder->id;
            $receiverLocation->receiver_location_name = $value;
            $receiverLocation->save();
        }

        return true;
    }

    public function updatePlaceholder(
        $id,
        $name,
        $type,
        $url,
        $urlWap,
        $urlType,
        $senderTypes,
        $receiverLocations,
        $active,
        $isLink
    ) {
        $placeholderIdCondition = 'placeholder_id = ' . $id;
        SenderType::deleteAll($placeholderIdCondition);
        ReceiverLocation::deleteAll($placeholderIdCondition);
        $placeholder = Placeholder::find()
            ->with('senderTypes')
            ->with('receiverLocations')
            ->where(['id' => $id])
            ->one();
        $placeholder->name = $name;
        $placeholder->type = $type;
        $placeholder->url = $url;
        $placeholder->url_type = $urlType;
        $placeholder->url_wap = $urlWap;

        foreach ($senderTypes as $val) {
            $senderType = new SenderType();
            $senderType->sender_type_name = $val;
            $senderType->link('placeholder', $placeholder);
        }

        foreach ($receiverLocations as $val) {
            $receiverLocation = new ReceiverLocation();
            $receiverLocation->receiver_location_name = $val;
            $receiverLocation->link('placeholder', $placeholder);
        }

        $placeholder->active = ($active == "true") ? true : false;
        $placeholder->is_link = $isLink;
        $placeholder->save();
        return true;
    }

    public function deletePlaceholder($id)
    {
        $placeholderIdCondition = 'placeholder_id = ' . $id;
        SenderType::deleteAll($placeholderIdCondition);
        ReceiverLocation::deleteAll($placeholderIdCondition);
        Placeholder::deleteAll('id = ' . $id);
        return true;
    }

    public function deletePlaceholders($params)
    {
        foreach ($params as $val) {
            $this->deletePlaceholder($val['placeholder_id']);
        }
        return true;
    }
}
