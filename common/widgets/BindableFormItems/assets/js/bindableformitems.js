function bindableFormItemClick(el, bindableFormId)
{
    $(el).siblings().removeClass('current');
    $(el).addClass('current');
    $(el).find('[name]').each(function () {
        //Get name attribute
        var findNameString = '[name=\'' + $(this).attr('name') + '\']';

        //Get radiobuttons binding to glyphicons
        if (
            $(this).is('span') &&
            $(this).hasClass('glyphicon') &&
            $(bindableFormId).find(findNameString).hasClass('radio-button') &&
            ($(this).hasClass('active') != $(bindableFormId).find(findNameString).hasClass('active'))
        ) {
            $(bindableFormId).find(findNameString).click();
            return;
        }

        //Get text of other tags
        var text;
        if ($(this).is('span')) {
            text = $(this).text();
        } else {
            text = $(this).val();
        }

        if ($(bindableFormId).find(findNameString).is('span')) {
            $(bindableFormId).find(findNameString).text(text);
        } else {
            $(bindableFormId).find(findNameString).val(text);
        }
    });
}
