<?php
//This array is available by \Yii::$app->params. For example - \Yii::$app->params['tags'][1]
return [
    'tags' => [
        1 => 'dating',
        2 => 'cams',
    ],
    'languageTags' => [
        'en',
        'fr',
        'dn',
        'fi',
        'nr',
        'sw',
        'it',
        'es',
        'nz',
        'ir',
    ],
    'groupPhrasesTags' => [
        1 => 'cam models',
    ],
];
