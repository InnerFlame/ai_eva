<?php
declare(strict_types=1);

namespace frontend\models\settings;

use frontend\models\BaseForm;
use frontend\models\DataProvider;

class SettingsForm extends BaseForm
{
    public $settings = array();
    public $availableScenarios = array();

    public function __construct()
    {
        $this->availableScenarios = DataProvider::getInstance()->getScenarios();
    }

    public function getSettings()
    {
        $this->settings = Setting::find()->orderBy('priority')->all();
    }

    public function createSetting($name, $controlProfile, $targetProfile)
    {
        $setting = new Setting();
        $setting->name = $name;
        $setting->control_profile = $controlProfile;
        $setting->target_profile = $targetProfile;
        $setting->active = false;
        $setting->save();
        return true;
    }

    public function updateSetting(
        $id,
        $name,
        $controlProfile,
        $targetProfile,
        $projects,
        $countries,
        $locales,
        $statuses,
        $sites,
        $trafficSources,
        $actionWays,
        $trafficTypes,
        $platforms,
        $fanClubs,
        $onlines,
        $scenarios,
        $active
    ) {
        $settingIdCondition = 'setting_id = ' . $id;
        Project::deleteAll($settingIdCondition);
        Country::deleteAll($settingIdCondition);
        Locale::deleteAll($settingIdCondition);
        Status::deleteAll($settingIdCondition);
        Site::deleteAll($settingIdCondition);
        TrafficSource::deleteAll($settingIdCondition);
        ActionWay::deleteAll($settingIdCondition);
        TrafficType::deleteAll($settingIdCondition);
        Platform::deleteAll($settingIdCondition);
        FanClub::deleteAll($settingIdCondition);
        Online::deleteAll($settingIdCondition);
        Scenario::deleteAll($settingIdCondition);
        $setting = Setting::find()
            ->with('projects')
            ->with('countries')
            ->with('locales')
            ->with('statuses')
            ->with('sites')
            ->with('trafficSources')
            ->with('actionWays')
            ->with('trafficTypes')
            ->with('platforms')
            ->with('fanClubs')
            ->with('onlines')
            ->with('scenarios')
            ->where(['id' => $id])
            ->one();
        $setting->name = $name;
        $setting->control_profile = $controlProfile;
        $setting->target_profile = $targetProfile;

        foreach ($projects as $val) {
            if (!empty($val)) {
                $project = new Project();
                $project->project_name = $val;
                $project->link('setting', $setting);
            }
        }

        foreach ($countries as $val) {
            $country = new Country();
            $country->country_name = $val;
            $country->link('setting', $setting);
        }

        foreach ($locales as $val) {
            $locale = new Locale();
            $locale->locale_name = $val;
            $locale->link('setting', $setting);
        }

        foreach ($statuses as $val) {
            $status = new Status();
            $status->status_name = $val;
            $status->link('setting', $setting);
        }

        foreach ($sites as $val) {
            $site = new Site();
            $site->site_hash = $val;
            $site->link('setting', $setting);
        }

        foreach ($trafficSources as $val) {
            $trafficSource = new TrafficSource();
            $trafficSource->traffic_source_name = $val;
            $trafficSource->link('setting', $setting);
        }

        foreach ($actionWays as $val) {
            $actionWay = new ActionWay();
            $actionWay->action_way_name = $val;
            $actionWay->link('setting', $setting);
        }

        foreach ($trafficTypes as $val) {
            $trafficType = new TrafficType();
            $trafficType->traffic_type_name = $val;
            $trafficType->link('setting', $setting);
        }

        foreach ($platforms as $val) {
            $platform = new Platform();
            $platform->platform_name = $val;
            $platform->link('setting', $setting);
        }

        foreach ($fanClubs as $val) {
            if ($val != '') {
                $fanClub = new FanClub();
                $fanClub->is_fan_club_allowed = $val;
                $fanClub->link('setting', $setting);
            }
        }

        foreach ($onlines as $val) {
            if ($val != '') {
                $online = new Online();
                $online->is_online = $val;
                $online->link('setting', $setting);
            }
        }

        if (is_array($scenarios)) {
            $scenarioCounter = 1;
            foreach ($scenarios as $val) {
                $scenario = new Scenario();
                $scenario->scenario_id = (int)$val['id'];
                $scenario->isFixed = $val['isFixed'];
                $scenario->scenario_priority = $scenarioCounter;
                $scenario->link('setting', $setting);
                $scenarioCounter++;

                $subScenariosIds = $this->getScenarioSubScenariosIds((int) $val['id']);
                if (!empty($subScenariosIds)) {
                    foreach ($subScenariosIds as $subScenarioId) {
                        $scenario = new Scenario();
                        $scenario->scenario_id = (int) $subScenarioId;
                        $scenario->isFixed = $val['isFixed'];
                        $scenario->scenario_priority = $scenarioCounter;
                        $scenario->link('setting', $setting);
                        $scenarioCounter++;
                    }
                }
            }
        }

        $setting->active = ($active == "true") ? true : false;
        $isSaved = $setting->save();

        if ($isSaved) {
            $this->combineSetting($id);
        }

        return true;
    }

    private function getScenarioSubScenariosIds(int $scenarioId)
    {
        $subScenariosIds = [];

        foreach ($this->availableScenarios as $availableScenario)
        {
            if($availableScenario->getId() !== $scenarioId) {
                continue;
            }

            $nodes = json_decode($availableScenario->getNodes(), true);

            foreach ($nodes as $node) {
                if ($node['type'] == 'scenario') {
                    $subScenariosIds[] = $node['id'];
                }
            }
        }

        return $subScenariosIds;
    }

    public function deleteSetting($id)
    {
        $settingIdCondition = 'setting_id = ' . $id;
        Project::deleteAll($settingIdCondition);
        Country::deleteAll($settingIdCondition);
        Locale::deleteAll($settingIdCondition);
        Status::deleteAll($settingIdCondition);
        Site::deleteAll($settingIdCondition);
        TrafficSource::deleteAll($settingIdCondition);
        ActionWay::deleteAll($settingIdCondition);
        TrafficType::deleteAll($settingIdCondition);
        Platform::deleteAll($settingIdCondition);
        FanClub::deleteAll($settingIdCondition);
        Online::deleteAll($settingIdCondition);
        Scenario::deleteAll($settingIdCondition);
        Setting::deleteAll('id = ' . $id);
        return true;
    }

    private function combineSetting($settingId)
    {
        \Yii::$app->db->createCommand('
            DELETE FROM `consolidatedSettings`
            WHERE setting_id = :settingId
        ')
        ->bindParam(':settingId', $settingId)
        ->execute();

        \Yii::$app->db->createCommand('
            INSERT INTO `consolidatedSettings`
                (`setting_id`,`name`,`control_profile`,`target_profile`,
                `active`,`project_name`,`country_name`,`locale_name`,`status_name`,`site_hash`,
                `traffic_type_name`,`platform_name`,`is_fan_club_allowed`,`is_online`,`priority`)
            (
            SELECT
                `settings_table`.`id`,`name`,`control_profile`,`target_profile`,
                `active`, IF(`project_name` IS NULL,\'\',`project_name`),`country_name`,`locale_name`,`status_name`,`site_hash`,
                `traffic_type_name`,`platform_name`,`is_fan_club_allowed`,`is_online`,`priority`
            FROM settings_table
            JOIN settings_countries_table ON settings_table.id = settings_countries_table.setting_id
            JOIN settings_locales ON settings_table.id = settings_locales.setting_id
            JOIN settings_statuses_table ON settings_table.id = settings_statuses_table.setting_id
            JOIN settings_sites_table ON settings_table.id = settings_sites_table.setting_id
            JOIN settings_traffic_types_table ON settings_table.id = settings_traffic_types_table.setting_id
            JOIN settings_platforms_table ON settings_table.id = settings_platforms_table.setting_id
            JOIN settings_fan_club ON settings_table.id = settings_fan_club.setting_id
            JOIN settings_online ON settings_table.id = settings_online.setting_id
            LEFT JOIN settings_projects ON settings_table.id = settings_projects.setting_id
            WHERE `settings_table`.id = :settingId
              and active = 1
            )
        ')
        ->bindParam(':settingId', $settingId)
        ->execute();
    }

    public function combineSettings()
    {
        \Yii::$app->db
            ->createCommand("
                CREATE TABLE `consolidatedSettingsTemp` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `setting_id` int(11) NOT NULL,
                  `name` varchar(255) NOT NULL,
                  `control_profile` varchar(32) NOT NULL,
                  `target_profile` varchar(32) NOT NULL,
                  `active` tinyint(1) DEFAULT NULL,
                  `country_name` varchar(3) NOT NULL,
                  `status_name` varchar(10) DEFAULT NULL,
                  `site_hash` varchar(32) NOT NULL,
                  `traffic_type_name` varchar(10) DEFAULT NULL,
                  `platform_name` varchar(16) DEFAULT NULL,
                  `locale_name` varchar(3) DEFAULT 'DEF',
                  `sender_locale_name` varchar(3) DEFAULT 'DEF',
                  `is_fan_club_allowed` tinyint(1) DEFAULT NULL,
                  `is_online` tinyint(1) DEFAULT NULL,
                  `priority` smallint(6) DEFAULT '0',
                  `project_name` varchar(255) DEFAULT '',
                  PRIMARY KEY (`id`),
                  KEY `setting_id` (`setting_id`),
                  KEY `site_country_type` (`site_hash`,`country_name`,`control_profile`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ")->execute();

        \Yii::$app->db->createCommand('
            INSERT INTO `consolidatedSettingsTemp`
                (`setting_id`,`name`,`control_profile`,`target_profile`,
                `active`,`project_name`,`country_name`,`locale_name`,`sender_locale_name`,`status_name`,`site_hash`,
                `traffic_type_name`,`platform_name`,`is_fan_club_allowed`,`is_online`,`priority`)
            (
            SELECT
                settings_table.`id`,`name`,`control_profile`,`target_profile`,
                `active`, IF(`project_name` IS NULL,\'\',`project_name`),`country_name`,settings_locales.locale_name,settings_sender_locales.locale_name,`status_name`,`site_hash`,
                `traffic_type_name`,`platform_name`,`is_fan_club_allowed`,`is_online`,`priority`
            FROM settings_table
            JOIN settings_countries_table ON settings_table.id = settings_countries_table.setting_id
            JOIN settings_locales ON settings_table.id = settings_locales.setting_id
            JOIN settings_sender_locales ON settings_table.id = settings_sender_locales.setting_id
            JOIN settings_statuses_table ON settings_table.id = settings_statuses_table.setting_id
            JOIN settings_sites_table ON settings_table.id = settings_sites_table.setting_id
            JOIN settings_traffic_types_table ON settings_table.id = settings_traffic_types_table.setting_id
            JOIN settings_platforms_table ON settings_table.id = settings_platforms_table.setting_id
            JOIN settings_fan_club ON settings_table.id = settings_fan_club.setting_id
            JOIN settings_online ON settings_table.id = settings_online.setting_id
            LEFT JOIN settings_projects ON settings_table.id = settings_projects.setting_id
            WHERE active = 1
            )
        ')->execute();

        \Yii::$app->db->transaction(function($db) {
            $db->createCommand('RENAME TABLE `consolidatedSettings` TO `consolidatedSettingsReadyForDelete`')->execute();
            $db->createCommand('RENAME TABLE `consolidatedSettingsTemp` TO `consolidatedSettings`')->execute();
            $db->createCommand('DROP TABLE `consolidatedSettingsReadyForDelete`')->execute();
        }, \yii\db\Transaction::SERIALIZABLE);

        return true;
    }

    public function updatePriority($settings)
    {
        foreach ($settings as $val) {
            $setting = Setting::find()
                ->where(['id' => $val['id']])
                ->one();
            $setting->priority = $val['priority'];
            $setting->save();
        }
        return true;
    }

    public function copySetting($id)
    {
        $setting = Setting::findOne($id);
        unset($setting->id);
        $setting->isNewRecord = true;
        $setting->save();
        $newId = $setting->id;

        foreach (Project::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Country::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Locale::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Status::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Site::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (TrafficSource::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (ActionWay::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (TrafficType::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Platform::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (FanClub::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Online::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        foreach (Scenario::find()->where(['setting_id' => $id])->each() as $val) {
            unset($val->id);
            $val->setting_id = $newId;
            $val->isNewRecord = true;
            $val->save();
        }

        $this->combineSetting($newId);

        return true;
    }
}
