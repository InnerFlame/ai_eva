<?php

use yii\db\Migration;

/**
 * Handles the creation for table `settings_scenarios_table`.
 */
class m160622_142341_create_settings_scenarios_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings_scenarios_table', [
            'id' => $this->primaryKey(),
            'setting_id' => $this->integer(11),
            'scenario_id' => $this->integer(11),
            'scenario_priority' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings_scenarios_table');
    }
}
