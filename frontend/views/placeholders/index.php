<?php
use common\widgets\RadioButton\RadioButton;
use common\widgets\Modal\Modal;
use frontend\assets\PlaceholdersAsset;

PlaceholdersAsset::register($this);

$availablePlaceholdersTypes = \Yii::$app->params['placeholdersTypes'];
$availablePlaceholdersUrlsTypes = \Yii::$app->params['placeholdersUrlsTypes'];
$availablePlaceholdersSendersTypes = \Yii::$app->params['placeholdersSendersTypes'];
$availablePlaceholdersReceiversLocations = \Yii::$app->params['countries'];
$availablePlaceholdersIsLinkTypes = \Yii::$app->params['placeholderIsLink'];
?>
<!-- Modal add new placeholder -->
<div id="addNewPlaceholderModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title">Add new placeholder</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label class="control-label" for="addNewPlaceholderName">Placeholder:</label>
                        </div>
                        <div class="col-sm-7">
                            <input id="addNewPlaceholderName" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label class="control-label" for="addNewPlaceholderType">Placeholder type:</label>
                        </div>
                        <div class="col-sm-7 dropdown-toggler" id="addNewPlaceholderType">
                            <button
                                type="button"
                                class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                                >
                                <span name="name"><?= $availablePlaceholdersTypes[0]; ?></span>
                                <input type="hidden" value="<?= $availablePlaceholdersTypes[0]; ?>">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php
                                for ($i = 0; $i < count($availablePlaceholdersTypes); $i++) {
                                    echo '<li><a href="javascript:void(0);">' .
                                        $availablePlaceholdersTypes[$i] .
                                        '</a><input type="hidden" value="' .
                                        $availablePlaceholdersTypes[$i] .
                                        '"></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label class="control-label" for="addNewPlaceholderUrl">Placeholder url:</label>
                        </div>
                        <div class="col-sm-7">
                            <input id="addNewPlaceholderUrl" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label class="control-label" for="addNewPlaceholderUrlWap">Placeholder url wap:</label>
                        </div>
                        <div class="col-sm-7">
                            <input id="addNewPlaceholderUrlWap" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label class="control-label" for="addNewPlaceholderUrlType">Placeholder url type:</label>
                        </div>
                        <div class="col-sm-7 dropdown-toggler" id="addNewPlaceholderUrlType">
                            <button
                                type="button"
                                class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                                >
                                <span name="name"><?= $availablePlaceholdersUrlsTypes[0]; ?></span>
                                <input type="hidden" value="<?= $availablePlaceholdersUrlsTypes[0]; ?>">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php
                                for ($i = 0; $i < count($availablePlaceholdersUrlsTypes); $i++) {
                                    echo '<li><a href="javascript:void(0);">' .
                                        $availablePlaceholdersUrlsTypes[$i] .
                                        '</a><input type="hidden" value="' .
                                        $availablePlaceholdersUrlsTypes[$i] .
                                        '"></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label class="control-label" for="addNewPlaceholderSenderTypes">
                                Placeholder sender types:
                            </label>
                        </div>
                        <div class="col-sm-7" id="addNewPlaceholderSenderTypes">
                            <button
                                type="button"
                                class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                                >
                                <span>Sender types</span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php
                                echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                                for ($i = 0; $i < count($availablePlaceholdersSendersTypes); $i++) {
                                    echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                                        $availablePlaceholdersSendersTypes[$i] .
                                        '</a><input type="hidden" value="' .
                                        $availablePlaceholdersSendersTypes[$i] .
                                        '"></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label class="control-label" for="addNewPlaceholderReceiverLocations">
                                Receiver locations:
                            </label>
                        </div>
                        <div class="col-sm-7" id="addNewPlaceholderReceiverLocations">
                            <button
                                type="button"
                                class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                                >
                                <span>Receiver locations</span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <input type="text" class="form-control livesearch" placeholder="Search">
                                <?php
                                echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                                for ($i = 0; $i < count($availablePlaceholdersReceiversLocations); $i++) {
                                    echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                                        $availablePlaceholdersReceiversLocations[$i] .
                                        '</a><input type="hidden" value="' .
                                        $availablePlaceholdersReceiversLocations[$i] .
                                        '"></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label class="control-label" for="addNewPlaceholderIsLink">Placeholder is link:</label>
                        </div>
                        <div class="col-sm-7 dropdown-toggler" id="addNewPlaceholderIsLink">
                            <button
                                    type="button"
                                    class="btn btn-default dropdown-toggle"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                            >
                                <span name="name"><?= $availablePlaceholdersIsLinkTypes[0]; ?></span>
                                <input type="hidden" value="0">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php foreach($availablePlaceholdersIsLinkTypes as $value => $label): ?>
                                    <li>
                                        <a href="javascript:void(0);"><?= $label; ?></a>
                                        <input type="hidden" value="<?= $value; ?>">
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                <button id="addNewPlaceholderButton" class="btn btn-success" disabled="true" type="button">
                    Add new placeholder
                </button>
            </div>
        </div>
    </div>
</div>

<?php
//Modal confirm delete placeholder
echo Modal::widget([
    'id'                => 'confirmDeletePlaceholderModal',
    'title'             => 'Delete placeholder',
    'hiddenInputIds'    => ['deletePlaceholderId',],
    'nameText'          => 'placeholder',
    'nameId'            => 'deletePlaceholderName',
    'buttonId'          => 'confirmDeletePlaceholderButton'
]);

//Modal confirm delete placeholders
echo Modal::widget([
    'id'                    => 'confirmDeletePlaceholdersModal',
    'title'                 => 'Delete placeholders',
    'nameText'              => 'placeholders',
    'isMultilineQuestion'   => true,
    'nameId'                => 'deletePlaceholdersNames',
    'buttonId'              => 'confirmDeletePlaceholdersButton'
]);
?>

<div class="row">
    <div class="col-sm-2">
        <button
            id="createNewPlaceholder"
            class="btn btn-success btn-block"
            type="button"
            data-target="#addNewPlaceholderModal"
            data-toggle="modal"
            ><span class="glyphicon glyphicon-plus"></span> Add placeholder</button>
    </div>
    <div class="col-sm-2 col-sm-offset-8">
        <button
            id="deletePlaceholders"
            class="btn btn-danger btn-block"
            type="button"
            ><span class="glyphicon glyphicon-trash"></span> Delete placeholders</button>
    </div>
</div>

<table>
    <tr id="expanded-table-header">
        <th scope="row"><input type="hidden" class="form-control" value="" name="placeholderId"></th>
        <td></td>
        <td><input type="text" class="form-control" value="" name="placeholderName"></td>
        <td>
            <div class="input-group-btn" name="placeholderType">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span></span>
                    <input type="hidden" value="">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    for ($i = 0; $i < count($availablePlaceholdersTypes); $i++) {
                        echo '<li><a href="javascript:void(0);">' .
                            $availablePlaceholdersTypes[$i] .
                            '</a><input type="hidden" value="' .
                            $availablePlaceholdersTypes[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td><input type="text" class="form-control" value="" name="placeholderUrl"></td>
        <td><input type="text" class="form-control" value="" name="placeholderUrlWap"></td>
        <td>
            <div class="input-group-btn" name="placeholderUrlType">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span></span>
                    <input type="hidden" value="">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    for ($i = 0; $i < count($availablePlaceholdersUrlsTypes); $i++) {
                        echo '<li><a href="javascript:void(0);">' .
                            $availablePlaceholdersUrlsTypes[$i] .
                            '</a><input type="hidden" value="' .
                            $availablePlaceholdersUrlsTypes[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="placeholderSenderTypes">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Sender types</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availablePlaceholdersSendersTypes); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availablePlaceholdersSendersTypes[$i] .
                            '</a><input type="hidden" value="' .
                            $availablePlaceholdersSendersTypes[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <div class="input-group-btn" name="placeholderReceiverLocations">
                <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    >
                    <span>Receiver locations</span> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <input type="text" class="form-control livesearch" placeholder="Search">
                    <?php
                    echo '<li class="checkAll"><a href="javascript:void(0);">Check all</a></li>';
                    for ($i = 0; $i < count($availablePlaceholdersReceiversLocations); $i++) {
                        echo '<li><a href="javascript:void(0);"><input type="checkbox" aria-label="...">' .
                            $availablePlaceholdersReceiversLocations[$i] .
                            '</a><input type="hidden" value="' .
                            $availablePlaceholdersReceiversLocations[$i] .
                            '"></li>';
                    }
                    ?>
                </ul>
            </div>
        </td>
        <td>
            <?php
            echo RadioButton::widget([
                'activeText' => 'On',
                'text' => 'Off',
                'glyphiconClass' => 'glyphicon-off',
                'activeState' => false,
                'name' => 'placeholderActiveRadioButton',
            ]);
            ?>
        </td>
        <td>
            <div class="input-group-btn dropdown-toggler" name="placeholderIsLink">
                <button
                        type="button"
                        class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                >
                    <span></span>
                    <input type="hidden" value="">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php foreach($availablePlaceholdersIsLinkTypes as $value => $label): ?>
                        <li>
                            <a href="javascript:void(0);"><?= $label; ?></a>
                            <input type="hidden" value="<?= $value; ?>">
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </td>
    </tr>
</table>

<div id="expanded-table-footer">
    <div class="col-md-1">
        <button class="btn btn-danger btn-block" type="button" name="deletePlaceholderButton">
            <span class="glyphicon glyphicon-trash"></span> Delete
        </button>
    </div>
    <div class="col-md-1 col-md-offset-8">
        <button class="btn btn-default btn-block" type="button" name="cancelPlaceholderButton">Cancel</button>
    </div>
    <div class="col-md-2">
        <button class="btn btn-primary btn-block" type="button" name="updatePlaceholderButton">
            <span class="glyphicon glyphicon-refresh"></span> Update
        </button>
    </div>
</div>

<div id="placeholdersAjax">
</div>