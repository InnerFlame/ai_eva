<?php
declare(strict_types=1);

namespace frontend\models\search;

use frontend\models\DataProvider;
use frontend\models\phrases\GroupPhrases;
use frontend\models\Singleton;
use frontend\models\topics\Topic;
use yii\base\Model;

/**
 * Class SearchForm
 * @package frontend\models\search
 */
class SearchForm extends Model
{
    const RULES         = 'Rules';
    const TEMPLATES     = 'Templates';
    const PATTERNS      = 'Patterns';
    const EXC_PATTERNS  = 'Exc. Patterns';
    const PHRASES       = 'Phrases';

    public $searchBy        = 'Patterns';
    public $text            = null;
    public $searchResults   = [];

    public function rules()
    {
        return [
            [['searchBy', 'text'], 'required'],
            [['searchBy', 'text'], 'string'],
        ];
    }

    /**
     * Search data by BrungildaApi then merge it with results from local db
     */
    public function search()
    {
        $this->searchResults = $this->getDataProviderInstance()->search(rawurlencode($this->text), $this->searchBy);

        // If we search in phrases
        if ($this->searchBy == self::PHRASES) {

            $groupPhrasesArr = [];
            $groupPhrases = $this->getDataProviderInstance()->getGroupsPhrases();
            //\Yii::$app->db->createCommand('SELECT * FROM group_phrases_table')->queryAll();

            /**@var $groupPhrases GroupPhrases[] */
            foreach ($groupPhrases as $groupPhrase) {
                $id                     = $groupPhrase->getId();
                $text                   = $groupPhrase->getText();
                $groupGroupPhrasesId    = $this->getBindingGroupPhrases()[$id]                  ?? 0 ;
                $groupGroupPhrasesName  = $this->getGroupGroupPhrases()[$groupGroupPhrasesId]   ?? '';
                $groupPhrasesArr[$id]   = [
                    'text'                  => $text,
                    'groupGroupPhrasesId'   => $groupGroupPhrasesId,
                    'groupGroupPhrasesName' => $groupGroupPhrasesName,
                ];
            }

            foreach ($this->searchResults as $val) {
                if (isset($groupPhrasesArr[$val->getGroupPhrasesId()])) {
                    $val->_groupPhraseText      = $groupPhrasesArr[$val->getGroupPhrasesId()]['text'];
                    $val->_groupGroupPhraseId   = $groupPhrasesArr[$val->getGroupPhrasesId()]['groupGroupPhrasesId'];
                    $val->_groupGroupPhraseName = $groupPhrasesArr[$val->getGroupPhrasesId()]['groupGroupPhrasesName'];
                }
            }

            return;
        }

        //Get info
        $rules = [];
        foreach ($this->searchResults as $val) {
            if (!isset($rules[$val->getRuleId()])) {
                $rules[$val->getRuleId()] = $this->getDataProviderInstance()->getRuleById($val->getRuleId());
            }

            $rule = $rules[$val->getRuleId()];
            if (is_null($rule )) {
                continue;
            }

            $val->_ruleName     = $rule->getName();
            $val->_topicId      = $rule->getTopicId();
            $val->_topicName    = $this->getTopicsData()[$val->_topicId] ?? '';
        }
    }

    /**
     * @return array
     */
    private function getBindingGroupPhrases()
    {
        $bindingGroupPhrasesArr = [];
        $bindingGroupPhrases = \Yii::$app->db->createCommand('SELECT * FROM group_group_phrases_group_phrases_table')->queryAll();
        foreach ($bindingGroupPhrases as $bindingGroupPhrase) {
            $bindingGroupPhrasesArr[$bindingGroupPhrase['group_phrases_id']] = $bindingGroupPhrase['group_group_phrases_id'];
        }

        return $bindingGroupPhrasesArr;
    }

    /**
     * @return array
     */
    private function getGroupGroupPhrases()
    {
        $groupGroupPhrasesArr = [];
        $groupGroupPhrases = \Yii::$app->db->createCommand('SELECT * FROM group_group_phrases_table')->queryAll();
        foreach ($groupGroupPhrases as $groupGroupPhrase) {
            $groupGroupPhrasesArr[$groupGroupPhrase['id']] = $groupGroupPhrase['name'];
        }

        return $groupGroupPhrasesArr;
    }

    private function getTopicsData()
    {
        $topics = $this->getDataProviderInstance()->getTopics();
        $topicsArr = [];
        /**@var $topics Topic[] */
        foreach ($topics as $topic) {
            $topicsArr[$topic->getId()] = $topic->getName();
        }
        
        return $topicsArr;
    }
    
    /**
     * @return DataProvider | Singleton
     */
    private function getDataProviderInstance()
    {
        return DataProvider::getInstance();
    }
}
