<?php
namespace frontend\models\variables;

class Variable
{
    private $variable_id;
    private $name;
    private $value;
    private $description;
    private $deleted;
    private $enabled;

    public function __construct($variable_id, $name, $value, $description, $deleted, $enabled)
    {
        $this->variable_id = $variable_id;
        if ($name == null) {
            $this->name = '';
        } else {
            $this->name = $name;
        }
        if ($value == null) {
            $this->value = '';
        } else {
            $this->value = $value;
        }
        if ($description == null) {
            $this->description = '';
        } else {
            $this->description = $description;
        }
        $this->deleted = $deleted;
        $this->enabled = $enabled;
    }

    /**
     * @return int
     */
    public function getVariableId() : int
    {
        return $this->variable_id;
    }

    /**
     * @param int $variable_id
     */
    public function setVariableId(int $variable_id)
    {
        $this->variable_id = $variable_id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return bool
     */
    public function isEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }
}
