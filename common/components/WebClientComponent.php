<?php
namespace common\components;

use yii\base\Component;

class WebClientComponent extends Component
{
    public function get($url, $params, $contentType = null)
    {
        $headers = [];

        if (!empty($contentType)) {
            $headers[] = 'Content-Type: ' . $contentType;
        }

        if(!empty($params)){
            $url .= "?" . http_build_query($params);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function post($url, $params, $contentType = null)
    {
        $headers = [];

        if (!empty($contentType)) {
            $headers[] = 'Content-Type: ' . $contentType;
        }

        if (is_array($params)) {
            $params = http_build_query($params);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}
