<?php

function pr()
{
    if (isset($_REQUEST['noHtml'])) {
        $php_sapi_name = 'cli';
    } else {
        $php_sapi_name = php_sapi_name();
    }

    if ($php_sapi_name != 'cli') {
        echo "<pre>";
    }

    $trace = debug_backtrace();
    $file = strstr($trace[0]['file'], 'eva');
    $line = $trace[0]['line'];
    echo "\n{$file}:{$line}\n";
    foreach (func_get_args() as $arg) {
        if ($php_sapi_name != 'cli') {
            yii\helpers\VarDumper::dump($arg, 10, true);
        } else {
            print_r($arg);
            echo "\n";
        }
    }

    if ($php_sapi_name != 'cli') {
        echo "</pre>";
    }
}
