<?php

namespace frontend\assets;

class PhrasesAsset extends BasicAsset
{
    public $css = [
        'css/phrases.css',
    ];
    public $js = [
        'js/phrases.js',
    ];
}
