<?php

namespace frontend\models\classifiers;

use frontend\models\BaseNgForm;

/**
 * Class ClassifiersNgForm
 * @package frontend\models\classifiers
 */
class ClassifiersNgForm extends BaseNgForm
{
    /**
     * @return array
     */
    public function getClassifiers(): array
    {
        $preparedClassifiers = [];
        $rawClassifiers      = $this->getBrungildaApi()->getClassifiers();

        foreach ($rawClassifiers as $classifier) {
            if (isset($classifier['type']) && $classifier['type'] != '') {
                continue;
            }
            $preparedClassifiers[] = $classifier;
        }

        return $preparedClassifiers;
    }

    /**
     * @param int $classifierId
     * @return array
     */
    public function getPatternsByClassifierId(int $classifierId): array
    {
        $patternsData = $this->getBrungildaApi()->getClassifierPatterns($classifierId);

        if (empty($patternsData['patterns'])) {
            return [];
        }

        return $patternsData['patterns'];
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createClassifier(array $requestData): array
    {
        $params['text'] = $requestData['text'] ?? null;

        return $this->getBrungildaApi()->createClassifiers([$params]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updateClassifier(array $requestData): array
    {
        $params['classifier_id'] = (int) $requestData['classifier_id'] ?? 0;
        $params['text']          = $requestData['text'] ?? null;

        return $this->getBrungildaApi()->updateClassifiers([$params]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteClassifier(array $requestData): array
    {
        $params['classifier_id'] = (int) $requestData['classifier_id'] ?? 0;

        return $this->getBrungildaApi()->deleteClassifiers([$params]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function createPattern(array $requestData): array
    {
        $params = [
            'classifier_id' => (int) $requestData['classifier_id'] ?? 0,
            'text'          => $requestData['text'] ?? null,
            'test'          => $requestData['test'] ?? null,
            'active'        => false,
        ];

        return $this->getBrungildaApi()->createClassifierPatterns([$params]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updatePattern(array $requestData): array
    {
        $params = [
            'classifier_pattern_id' => (int) $requestData['classifier_pattern_id'] ?? 0,
            'text'                  => $requestData['text'] ?? null,
            'test'                  => $requestData['test'] ?? null,
            'active'                => (int) $requestData['active'] ?? 0
        ];

        $params = $this->filterNullableProperty($params);

        return $this->getBrungildaApi()->updateClassifierPatterns([$params]);
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function updateMultiPatterns(array $requestData): array
    {
        $patternsIds  = $requestData['ids']   ?? [];
        $updateData   = $requestData['model'] ?? [];

        foreach ($patternsIds as $patternsId) {
            $updateParams = [
                'classifier_pattern_id' => (int) $patternsId ?? 0,
                'text'                  => $updateData['text'] ?? null,
                'test'                  => $updateData['test'] ?? null,
                'active'                => (int) $updateData['active'] ?? 0
            ];

            $updateParams = $this->filterNullableProperty($updateParams);

            $this->updatePattern($updateParams);
        }

        return ['status' => true];
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deletePattern(array $requestData): array
    {
        $params['classifier_pattern_id'] = (int) $requestData['classifier_pattern_id'] ?? 0;

        return $this->getBrungildaApi()->deleteClassifierPatterns([$params]);

    }

    /**
     * @param array $requestData
     * @return array
     */
    public function deleteMultiPatterns(array $requestData): array
    {
        $patternsIds = $requestData['ids'] ?? [];

        foreach ($patternsIds as $patternsId) {
            $deleteParams = [
                'classifier_pattern_id' => (int) $patternsId ?? 0
            ];
            $this->deletePattern($deleteParams);
        }

        return ['status' => true];
    }

    private function filterNullableProperty(array $params): array
    {
        foreach ($params as $key => $value) {
            if (is_null($value)) {
                unset($params[$key]);
            }
        }

        return $params;
    }
}
