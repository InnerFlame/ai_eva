<?php
use common\widgets\BindableFormItems\BindableFormItems;

$classPhrasesItems = [];

foreach ($model->phrases as $phrase) {
    $text = $phrase->text;
    foreach ($phrase->entities as $entity) {
        if (strpos($text, $entity['value']) !== false) {
            $text = str_replace($entity['value'], '<mark style=\'background-color: yellow\'>'.$entity['value'].'</mark>', $text);
        }
    }
    $item['text'] = $text . '<span class="glyphicon glyphicon-chevron-down pull-right openEntities"></span>';
    $item['hiddens'] = [
        ['name' => 'classId',   'value' => $phrase->classId],
        ['name' => 'phraseId',  'value' => $phrase->id],
    ];
    $classPhrasesItems[] = $item;
}

echo BindableFormItems::widget([
    'bindableFormId'    => 'phrasesBindableForm',
    'items'             => $classPhrasesItems,
    'checked'           => true,
]);
