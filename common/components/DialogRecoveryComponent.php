<?php
namespace common\components;

use frontend\models\Dialogues;
use frontend\models\settings\Scenario;
use frontend\models\ShownLinks;
use yii\db\Query;

class DialogRecoveryComponent extends AbstractMessageProcessorComponent
{
    protected $isPredispatched = true;

    protected function getScenarioId($sender, $recipient)
    {
        // $dialogues - stores info about pair and corresponding scenarioId
        // Get scenarioId from DB which was/wasn't assign to pair
        $previousScenarioId = (new Query())
            ->select('s.scenario_id')
            ->from(Scenario::tableName() . ' as s')
            ->innerJoin(Dialogues::tableName() . ' as d', 's.id = d.settingScenarioId')
            ->where([
                'idFrom' => $sender['multi_profile_id'],
                'idTo' => $recipient['id'],
            ])
            ->limit(1)
            ->scalar();

        if (empty($previousScenarioId)) {
            return null;
        }

        // $shownLinks - stores info about shown links
        // Get if link was shown
        $shownLink = ShownLinks::find()
            ->where(['or',
                ['userId' => $sender['id']],
                ['userId' => $sender['multi_profile_id']]
            ])
            ->andWhere(['scenarioId' => $previousScenarioId])
            ->one();

        if (!empty($shownLink)) {
            return null;
        }

        return $previousScenarioId;
    }

    protected function getEvaAnswer($data)
    {
        return \Yii::$app->eva->getPlaceholderPhrase($data);
    }

    protected function saveIncomingMessageToLog($sender, $recipient, $text, $scenarioId)
    {
        //don`t need to save incoming message to log
    }

    protected function saveOutgoingMessageToLog($sender, $recipient, $text, $scenarioId, $firstUsedPlaceholderId, $evaResponse, $predispatched = false)
    {
        return parent::saveOutgoingMessageToLog($sender, $recipient, $text, $scenarioId, $firstUsedPlaceholderId, $evaResponse, true);
    }
}
