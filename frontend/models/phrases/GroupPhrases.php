<?php
declare(strict_types=1);

namespace frontend\models\phrases;

class GroupPhrases
{
    private $id;
    private $text;
    private $deleted;
    private $version;
    private $excludePhrase;
    private $hasPlaceholder;
    private $hasPrePlaceholder;
    private $groupGroupsPhrasesId;
    private $languageTag;
    private $tags = [];

    /**
     * GroupPhrases constructor.
     * @param $id
     * @param $text
     * @param $deleted
     * @param $version
     * @param $excludePhrase
     */
    public function __construct(int $id, string $text, bool $deleted, int $version, int $excludePhrase,
                                int $hasPlaceholder, int $hasPrePlaceholder, string $languageTag = '')
    {
        $this->id = $id;
        $this->text = $text;
        $this->deleted = $deleted;
        $this->version = $version;
        $this->excludePhrase = $excludePhrase;
        $this->hasPlaceholder = $hasPlaceholder;
        $this->hasPrePlaceholder = $hasPrePlaceholder;
        $this->languageTag = $languageTag;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getExcludePhrase() : int
    {
        return $this->excludePhrase;
    }

    /**
     * @param int $excludePhrase
     */
    public function setExcludePhrase(int $excludePhrase)
    {
        $this->excludePhrase = $excludePhrase;
    }

    /**
     * @return int
     */
    public function getHasPlaceholder() : int
    {
        return $this->hasPlaceholder;
    }

    /**
     * @param int $hasPlaceholder
     */
    public function setHasPlaceholder(int $hasPlaceholder)
    {
        $this->hasPlaceholder = $hasPlaceholder;
    }

    /**
     * @return int
     */
    public function getHasPrePlaceholder() : int
    {
        return $this->hasPrePlaceholder;
    }

    /**
     * @param int $hasPrePlaceholder
     */
    public function setHasPrePlaceholder(int $hasPrePlaceholder)
    {
        $this->hasPrePlaceholder = $hasPrePlaceholder;
    }

    /**
     * @return int
     */
    public function getGroupGroupsPhrasesId() : int
    {
        return $this->groupGroupsPhrasesId;
    }

    /**
     * @param int $groupGroupsPhrasesId
     */
    public function setGroupGroupsPhrasesId(int $groupGroupsPhrasesId)
    {
        $this->groupGroupsPhrasesId = $groupGroupsPhrasesId;
    }

    public function getLanguageTag() : string
    {
        return $this->languageTag;
    }

    public function setLanguageTag(string $languageTag)
    {
        $this->languageTag = $languageTag;
    }

    public function getTags() : array
    {
        return $this->tags;
    }

    public function getTagsNames() : string
    {
        $tagsNames = [];
        foreach ($this->tags as $tag) {
            if (isset(\Yii::$app->params['groupPhrasesTags'][$tag])) {
                $tagsNames[] = \Yii::$app->params['groupPhrasesTags'][$tag];
            }
        }
        return json_encode($tagsNames);
    }

    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }
}
