<?php
declare(strict_types=1);

namespace frontend\models\substitutes;

class SubstitutePhrase
{
    private $id;
    private $substituteId;
    private $phrase;
    private $deleted;
    private $version;

    public function __construct(int $id, int $substituteId, string $phrase, bool $deleted, int $version)
    {
        $this->id = $id;
        $this->substituteId = $substituteId;
        $this->phrase = $phrase;
        $this->deleted = $deleted;
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSubstituteId() : int
    {
        return $this->substituteId;
    }

    /**
     * @param int $substituteId
     */
    public function setSubstituteId(int $substituteId)
    {
        $this->substituteId = $substituteId;
    }

    /**
     * @return string
     */
    public function getPhrase() : string
    {
        return $this->phrase;
    }

    /**
     * @param string $phrase
     */
    public function setPhrase(string $phrase)
    {
        $this->phrase = $phrase;
    }

    /**
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }
}
