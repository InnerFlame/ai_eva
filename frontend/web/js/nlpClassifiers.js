// Supporting variables
var currentClassifierId             = null;
var currentRootClassifierId             = null;
var currentClassifierClassId        = null;
var currentRootClassifierClassId        = null;
var currentClassPhraseId            = null;
var currentRootClassPhraseId            = null;
var allClassPhrasesItemsWasChecked  = false;
var allRootClassPhrasesItemsWasChecked  = false;

// Load list of classifiers
function updateClassifiers()
{
    $.ajax({
        url: '/nlp-classifiers/classifiers',
        success: function (data) {
            // Update classifiers
            $('#classifiers').html(data);
            // Trigger click on first or current classifier item
            var classifierItem;
            if (currentClassifierId == null) {
                classifierItem = $('#classifiers li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="classifierId"][value="' + currentClassifierId + '"]';
                classifierItem = $('#classifiers li.list-group-item.sortable').has(str);
            }
            if (classifierItem.length == 0) {
                classifierItem = $('#classifiers li.list-group-item.sortable:first-child');
            }
            if (classifierItem.length == 0) {
                currentClassifierId = null;
            } else {
                classifierItem.removeClass('lastClickedItem');
                classifierItem.click();
            }
        }
    });
}

// Load list of classifiers
function updateRootClassifiers()
{
    $.ajax({
        url: '/nlp-classifiers/root-classifiers',
        success: function (data) {
            // Update classifiers
            $('#rootClassifiers').html(data);
            // Trigger click on first or current classifier item
            var classifierItem;
            if (currentRootClassifierId === null) {
                classifierItem = $('#rootClassifiers li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="rootClassifierId"][value="' + currentRootClassifierId + '"]';
                classifierItem = $('#rootClassifiers li.list-group-item.sortable').has(str);
            }
            if (classifierItem.length === 0) {
                classifierItem = $('#rootClassifiers li.list-group-item.sortable:first-child');
            }
            if (classifierItem.length === 0) {
                currentRootClassifierId = null;
            } else {
                classifierItem.removeClass('lastClickedItem');
                classifierItem.click();
            }
        }
    });
}

// Load list of classifier classes
function updateClassifierClasses()
{
    $.ajax({
        url:     '/nlp-classifiers/classifier-classes',
        type:    "POST",
        data:    'classifierId=' + currentClassifierId,
        success: function (data) {
            // Update classifierClasses
            $('#classes').html(data);
            // Trigger click on first or current  classifier classes item
            var classifierClassItem;
            if (currentClassifierClassId == null) {
                classifierClassItem = $('#classes li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="classId"][value="' + currentClassifierClassId + '"]';
                classifierClassItem = $('#classes li.list-group-item.sortable').has(str);
            }
            if (classifierClassItem.length == 0) {
                classifierClassItem = $('#classes li.list-group-item.sortable:first-child');
            }
            if (classifierClassItem.length == 0) {
                currentClassifierClassId = null;
            } else {
                classifierClassItem.removeClass('lastClickedItem');
                classifierClassItem.click();
            }
        }
    });
}

function updateRootClassifierClasses()
{
    $.ajax({
        url:     '/nlp-classifiers/root-classifier-classes',
        type:    "POST",
        data:    'classifierId=' + currentRootClassifierId,
        success: function (data) {
            // Update classifierClasses
            $('#rootClasses').html(data);
            // Trigger click on first or current  classifier classes item
            var classifierClassItem;
            if (currentRootClassifierClassId === null) {
                classifierClassItem = $('#rootClasses li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="rootClassId"][value="' + currentRootClassifierClassId + '"]';
                classifierClassItem = $('#rootClasses li.list-group-item.sortable').has(str);
            }
            if (classifierClassItem.length === 0) {
                classifierClassItem = $('#rootClasses li.list-group-item.sortable:first-child');
            }
            if (classifierClassItem.length === 0) {
                currentClassifierClassId = null;
            } else {
                classifierClassItem.removeClass('lastClickedItem');
                classifierClassItem.click();
            }
        }
    });
}

// Load list of class phrases
function updateClassPhrases()
{
    $.ajax({
        url:     '/nlp-classifiers/class-phrases',
        type:    "POST",
        data:    'classId=' + currentClassifierClassId,
        success: function (data) {
            // Update classPhrases
            $('#phrases').html(data);
            // Trigger click on first or current  classifier classes item
            var classPhraseItem;
            if (currentClassPhraseId == null) {
                classPhraseItem = $('#phrases li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="phraseId"][value="' + currentClassPhraseId + '"]';
                classPhraseItem = $('#phrases li.list-group-item.sortable').has(str);
            }
            if (classPhraseItem.length == 0) {
                classPhraseItem = $('#phrases li.list-group-item.sortable:first-child');
            }
            if (classPhraseItem.length == 0) {
                currentClassPhraseId = null;
            } else {
                classPhraseItem.removeClass('lastClickedItem');
                classPhraseItem.click();
            }
        }
    });
}

// Load list of class phrases
function updateRootClassPhrases()
{
    $.ajax({
        url:     '/nlp-classifiers/root-class-phrases',
        type:    "POST",
        data:    'classId=' + currentRootClassifierClassId,
        success: function (data) {
            // Update classPhrases
            $('#rootPhrases').html(data);
            // Trigger click on first or current  classifier classes item
            var classPhraseItem;
            if (currentRootClassPhraseId === null) {
                classPhraseItem = $('#rootPhrases li.list-group-item.sortable:first-child');
            } else {
                var str = 'input[name="rootPhraseId"][value="' + currentRootClassPhraseId + '"]';
                classPhraseItem = $('#rootPhrases li.list-group-item.sortable').has(str);
            }
            if (classPhraseItem.length === 0) {
                classPhraseItem = $('#rootPhrases li.list-group-item.sortable:first-child');
            }
            if (classPhraseItem.length === 0) {
                currentRootClassPhraseId = null;
            } else {
                classPhraseItem.removeClass('lastClickedItem');
                classPhraseItem.click();
            }
        }
    });
}

//Get array of checked object ids
function getCheckedObjectsIds(listDivId, listItemObjectIdName)
{
    var checkedObjectsIds = [];
    $('#' + listDivId +' li.list-group-item input[name="itemCheckbox"]:checked').each(function () {
        var id = $(this).closest('li.list-group-item').find('input[name="' + listItemObjectIdName + '"]').val();
        checkedObjectsIds.push(id);
    });
    return checkedObjectsIds;
}

//Get array of checked or current object ids
function getCheckedOrCurrentObjectsIds(bindableFormId, bindableFormObjectIdName, listDivId, listItemObjectIdName)
{
    //Get checked objects
    var checkedOrCurrentObjectsIds = getCheckedObjectsIds(listDivId, listItemObjectIdName);
    //If there are no checked objects get current objects
    if (checkedOrCurrentObjectsIds.length == 0) {
        var currentObjectId = $('#' + bindableFormId +' input[name="' + bindableFormObjectIdName + '"]').val();
        if (currentObjectId != 0) {
            checkedOrCurrentObjectsIds.push(currentObjectId);
        }
    }
    return checkedOrCurrentObjectsIds
}

//Get string of checked or current items names (for modal window)
function getCheckedOrCurrentItemsNames(bindableFormId, bindableFormObjectIdName, listDivId, listItemObjectIdName)
{
    var itemsNames = '';
    var itemsIds = getCheckedOrCurrentObjectsIds(bindableFormId, bindableFormObjectIdName, listDivId, listItemObjectIdName);
    $.each(itemsIds, function (index, value) {
        var itemsName = $('#' + listDivId + ' ul li:visible input[name="' + listItemObjectIdName + '"][value="' + value + '"]').siblings('span[name="itemText"]').html();
        itemsNames += (index + 1) + '. ' + itemsName + '<br/>'
    });
    return itemsNames;
}

function updateAvailableModelsList(models) {
    var modelsList = $('#availableModelsList');
    modelsList.html('');
    for (var i in models) {
        if (!models.hasOwnProperty(i)) {
            continue;
        }
        modelsList.append(
            $('<li>').append(
                $('<a>').attr('href', 'javascript:void(0);').append(
                    models[i]
                )
            )
        );
    }
}
function updateAvailableRootModelsList(models) {
    var modelsList = $('#availableRootModelsList');
    modelsList.html('');
    for (var i in models) {
        if (!models.hasOwnProperty(i)) {
            continue;
        }
        modelsList.append(
            $('<li>').append(
                $('<a>').attr('href', 'javascript:void(0);').append(
                    models[i]
                )
            )
        );
    }
}

function updateStatuses() {
    $.ajax({
        url: '/nlp-classifiers/classifiers-status',
        type: "GET",
        async: false,
        success: function (data) {
            var dataObj = JSON.parse(data);
            updateAvailableModelsList(dataObj.models);
            updateAvailableRootModelsList(dataObj.models);
        }
    });
}
$(document).ready(function () {
    /********** CLASSIFIERS FUNCTIONALITY **********/
        // Load list of classifiers
    updateClassifiers();
    updateRootClassifiers();
    updateStatuses();

    $('#filterRootClassifiersModels').click(function () {
        var classifierName = $('#rootClassifiers').find('li.current').find('span[name="itemText"]').text();
        $('#availableRootModelsList li').each(function(i) {
            if($(this).find('a').text().split('_')[0] !== classifierName) {
                $(this).remove();
            }
        });
    });

    $('#filterClassifiersModels').click(function () {
        var classifierName = $('#classifiers').find('li.current').find('span[name="itemText"]').text();
        $('#availableModelsList li').each(function(i) {
            if($(this).find('a').text().split('_')[0] !== classifierName) {
                $(this).remove();
            }
        });
    });

    // Click on classifiers item event
    $(document).on('click', '#classifiers li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        $('#classes').html('');
        $('#phrases').html('');
        clearBindableForm('#classesBindableForm');
        clearBindableForm('#phrasesBindableForm');

        var classifierId = $(this).children('input[name="classifierId"]').val();
        if (classifierId === undefined) {
            currentClassifierId = null;
            return;
        }
        currentClassifierId = classifierId;
        updateStatuses();
        updateClassifierClasses();
    });

    // Click on classifiers item event
    $(document).on('click', '#rootClassifiers li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        $('#rootClasses').html('');
        $('#rootPhrases').html('');
        clearBindableForm('#rootClassesBindableForm');
        clearBindableForm('#rootPhrasesBindableForm');

        var classifierId = $(this).children('input[name="rootClassifierId"]').val();
        if (classifierId === undefined) {
            currentRootClassifierId = null;
            return;
        }
        currentRootClassifierId = classifierId;
        updateStatuses();
        updateRootClassifierClasses();
    });

    // Click 'Add classifiers' button
    $(document).on('click', '#addClassifierButton', function () {
        var text = $('#classifiersText').val();
        text = encodeURIComponent(text);
        $.ajax({
            url:     '/nlp-classifiers/create-classifiers',
            type:    "POST",
            data:    'text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateClassifiers();
                } else {
                    alert('An error occurred while creating the classifiers!');
                }
            }
        });
    });

    // Click 'Add root classifiers' button
    $(document).on('click', '#addRootClassifierButton', function () {
        var text = $('#rootClassifiersText').val();
        text = encodeURIComponent(text);
        $.ajax({
            url:     '/nlp-classifiers/create-root-classifiers',
            type:    "POST",
            data:    'text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateRootClassifiers();
                } else {
                    alert('An error occurred while creating the classifiers!');
                }
            }
        });
    });

    // Click 'Update classifiers' button
    $(document).on('click', '#updateClassifierButton', function () {
        var text = $('#classifiersText').val();
        text = encodeURIComponent(text);
        var classifierId = $('#classifiersBindableForm input[name="classifierId"]').val();
        var classifierModel = $('#classifiersBindableForm input[name="classifierModel"]').val();
        if (classifierId.length == 0) {
            return;
        }
        $.ajax({
            url:     '/nlp-classifiers/update-classifiers',
            type:    "POST",
            data: 'classifierId=' + classifierId + '&text=' + text + '&classifierModel=' + classifierModel,
            success: function (data) {
                if (data == 'true') {
                    updateClassifiers();
                } else {
                    alert('An error occurred while updating the classifiers!');
                }
            }
        });
    });

    // Click 'Update root classifiers' button
    $(document).on('click', '#updateRootClassifierButton', function () {
        var text = $('#rootClassifiersText').val();
        text = encodeURIComponent(text);
        var classifierId = $('#rootClassifiersBindableForm input[name="rootClassifierId"]').val();
        var classifierModel = $('#rootClassifiersBindableForm input[name="rootClassifierModel"]').val();
        if (classifierId.length == 0) {
            return;
        }
        $.ajax({
            url:     '/nlp-classifiers/update-classifiers',
            type:    "POST",
            data: 'classifierId=' + classifierId + '&text=' + text + '&classifierModel=' + classifierModel,
            success: function (data) {
                if (data == 'true') {
                    updateRootClassifiers();
                } else {
                    alert('An error occurred while updating the classifiers!');
                }
            }
        });
    });

    // Click classifiers delete button
    $(document).on('click', '#deleteClassifierButton', function () {
        var classifiersName = $('#classifiersText').val();
        var classifierId    = $('#classifiersBindableForm input[name="classifierId"]').val();
        if (classifierId.length == 0) {
            return;
        }
        $('#deleteClassifierId').val(classifierId);
        $('#deleteClassifiersName').html(classifiersName);
        $('#confirmDeleteClassifiersModal').modal('show');
    });

    // Click root classifiers delete button
    $(document).on('click', '#deleteRootClassifierButton', function () {
        var classifiersName = $('#rootClassifiersText').val();
        var classifierId    = $('#rootClassifiersBindableForm input[name="rootClassifierId"]').val();
        if (classifierId.length == 0) {
            return;
        }
        $('#deleteRootClassifierId').val(classifierId);
        $('#deleteRootClassifiersName').html(classifiersName);
        $('#confirmDeleteRootClassifiersModal').modal('show');
    });

    // Click confirm classifiers delete button
    $(document).on('click', '#confirmDeleteClassifiersButton', function () {
        var classifierId = $('#deleteClassifierId').val();
        if (classifierId.length == 0) {
            return;
        }
        $.ajax({
            url:     '/nlp-classifiers/delete-classifiers',
            type:    "POST",
            data:    'classifierId=' + classifierId,
            success: function (data) {
                if (data == 'true') {
                    updateClassifiers();
                } else {
                    alert('An error occurred while deleting the classifiers!');
                }
                $('#confirmDeleteClassifiersModal').modal('hide');
            }
        });
    });

    // Click confirm root classifiers delete button
    $(document).on('click', '#confirmDeleteRootClassifiersButton', function () {
        var classifierId = $('#deleteRootClassifierId').val();
        if (classifierId.length == 0) {
            return;
        }
        $.ajax({
            url:     '/nlp-classifiers/delete-classifiers',
            type:    "POST",
            data:    'classifierId=' + classifierId,
            success: function (data) {
                if (data == 'true') {
                    updateRootClassifiers();
                } else {
                    alert('An error occurred while deleting the classifiers!');
                }
                $('#confirmDeleteRootClassifiersModal').modal('hide');
            }
        });
    });

    // Click 'Train classifier' button
    $(document).on('click', '#trainClassifierButton', function () {
        var isAlreadyRunning = false;
        $.ajax({
            url:    '/nlp-classifiers/classifiers-status',
            type: "GET",
            async:  false,
            success: function (data) {
                var dataObj = JSON.parse(data);
                updateAvailableModelsList(dataObj.models);
                if (dataObj.trainings_processes != 0) {
                    alert('Some classifier is already training now!');
                    isAlreadyRunning = true;
                }
            }
        });

        if (isAlreadyRunning) {
            return;
        }

        var text = $('#classifiersText').val();
        text = encodeURIComponent(text);
        var classifierId    = $('#classifiersBindableForm input[name="classifierId"]').val();
        if (classifierId.length == 0) {
            return;
        }

        $('#trainClassifierModal').modal('show');
        $.ajax({
            url:    '/nlp-classifiers/train-classifier',
            type:   "POST",
            async:  false,
            data:   'classifierId=' + classifierId,
            success: function (data) {}
        });

        setTimeout(function run() {
            $.ajax({
                url:    '/nlp-classifiers/classifiers-status',
                type: "GET",
                async:  false,
                success: function (data) {
                    var dataObj = JSON.parse(data);
                    if (dataObj.trainings_processes == 0) {
                        updateAvailableModelsList(dataObj.models);
                        $('#trainClassifierModal').modal('hide');
                        return;
                    } else {
                        setTimeout(run, 10000);
                    }
                }
            });
        }, 10000);
    });

    // Click 'Train root classifier' button
    $(document).on('click', '#trainRootClassifierButton', function () {
        var isAlreadyRunning = false;
        $.ajax({
            url:    '/nlp-classifiers/classifiers-status',
            type: "GET",
            async:  false,
            success: function (data) {
                var dataObj = JSON.parse(data);
                updateAvailableRootModelsList(dataObj.models);
                if (dataObj.trainings_processes != 0) {
                    alert('Some classifier is already training now!');
                    isAlreadyRunning = true;
                }
            }
        });

        if (isAlreadyRunning) {
            return;
        }

        var text = $('#rootClassifiersText').val();
        text = encodeURIComponent(text);
        var classifierId    = $('#rootClassifiersBindableForm input[name="rootClassifierId"]').val();
        if (classifierId.length == 0) {
            return;
        }

        $('#trainRootClassifierModal').modal('show');
        $.ajax({
            url:    '/nlp-classifiers/train-classifier',
            type:   "POST",
            async:  false,
            data:   'classifierId=' + classifierId,
            success: function (data) {}
        });

        setTimeout(function run() {
            $.ajax({
                url:    '/nlp-classifiers/classifiers-status',
                type: "GET",
                async:  false,
                success: function (data) {
                    var dataObj = JSON.parse(data);
                    if (dataObj.trainings_processes == 0) {
                        updateAvailableRootModelsList(dataObj.models);
                        $('#trainRootClassifierModal').modal('hide');
                        return;
                    } else {
                        setTimeout(run, 10000);
                    }
                }
            });
        }, 10000);
    });

    /**********  CLASSIFIER CLASSES FUNCTIONALITY **********/
    // Click on classes item event
    $(document).on('click', '#classes li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        $('#phrases').html('');
        clearBindableForm('#phrasesBindableForm');

        var classId = $(this).children('input[name="classId"]').val();
        if (classId === undefined) {
            currentClassifierClassId = null;
            return;
        }
        currentClassifierClassId = classId;
        updateClassPhrases();
    });
    // Click on root classes item event
    $(document).on('click', '#rootClasses li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        $('#rootPhrases').html('');
        clearBindableForm('#rootPhrasesBindableForm');

        var classId = $(this).children('input[name="rootClassId"]').val();
        if (classId === undefined) {
            currentRootClassifierClassId = null;
            return;
        }
        currentRootClassifierClassId = classId;
        updateRootClassPhrases();
    });

    // Click 'Add class' button
    $(document).on('click', '#addClassButton', function () {
        var text        = $('#classText').val();
        var probability = $('#classProbability').val();
        text            = encodeURIComponent(text);

        $.ajax({
            url:    '/nlp-classifiers/create-classifier-classes',
            type:   "POST",
            data:   'сlassifierId=' + currentClassifierId + '&text=' + text + '&probability=' + probability,
            success: function (data) {
                if (data == 'true') {
                    updateClassifierClasses();
                } else {
                    alert('An error occurred while creating the classifier class!');
                }
            }
        });
    });

    $(document).on('click', '#addRootClassButton', function () {
        var text        = $('#rootClassText').val();
        var probability = $('#rootClassProbability').val();
        text            = encodeURIComponent(text);

        $.ajax({
            url:    '/nlp-classifiers/create-classifier-classes',
            type:   "POST",
            data:   'сlassifierId=' + currentRootClassifierId + '&text=' + text + '&probability=' + probability,
            success: function (data) {
                if (data == 'true') {
                    updateRootClassifierClasses();
                } else {
                    alert('An error occurred while creating the classifier class!');
                }
            }
        });
    });

    // Click 'Update class' button
    $(document).on('click', '#updateClassButton', function () {
        var text        = $('#classText').val();
        var probability = $('#classProbability').val();
        text            = encodeURIComponent(text);

        var classifierClassId = $('#classesBindableForm input[name="classId"]').val();
        if (classifierClassId.length == 0) {
            return;
        }

        $.ajax({
            url:     '/nlp-classifiers/update-classifier-classes',
            type:    "POST",
            data:    'classifierClassId=' + classifierClassId + '&text=' + text + '&probability=' + probability,
            success: function (data) {
                if (data == 'true') {
                    updateClassifierClasses();
                } else {
                    alert('An error occurred while updating the classifier class!');
                }
            }
        });
    });

    $(document).on('click', '#updateRootClassButton', function () {
        var text        = $('#rootClassText').val();
        var probability = $('#rootClassProbability').val();
        text            = encodeURIComponent(text);

        var classifierClassId = $('#rootClassesBindableForm input[name="rootClassId"]').val();
        if (classifierClassId.length === 0) {
            return;
        }

        $.ajax({
            url:     '/nlp-classifiers/update-classifier-classes',
            type:    "POST",
            data:    'classifierClassId=' + classifierClassId + '&text=' + text + '&probability=' + probability,
            success: function (data) {
                if (data == 'true') {
                    updateRootClassifierClasses();
                } else {
                    alert('An error occurred while updating the classifier class!');
                }
            }
        });
    });

    // Click classifier class delete button
    $(document).on('click', '#deleteClassButton', function () {
        var className   = $('#classText').val();
        var classId     = $('#classesBindableForm input[name="classId"]').val();
        if (classId.length == 0) {
            return;
        }
        $('#deleteClassId').val(classId);
        $('#deleteClassesName').html(className);
        $('#confirmDeleteClassModal').modal('show');
    });

    $(document).on('click', '#deleteRootClassButton', function () {
        var className   = $('#rootClassText').val();
        var classId     = $('#rootClassesBindableForm input[name="rootClassId"]').val();
        if (classId.length == 0) {
            return;
        }
        $('#deleteRootClassId').val(classId);
        $('#deleteRootClassesName').html(className);
        $('#confirmDeleteRootClassModal').modal('show');
    });


    // Click confirm class delete button
    $(document).on('click', '#confirmDeleteClassesButton', function () {
        var classId = $('#deleteClassId').val();
        if (classId.length == 0) {
            return;
        }
        $.ajax({
            url:     '/nlp-classifiers/delete-classifier-classes',
            type:    "POST",
            data:    'classifierClassId=' + classId,
            success: function (data) {
                if (data == 'true') {
                    updateClassifierClasses();
                } else {
                    alert('An error occurred while deleting the class!');
                }
                $('#confirmDeleteClassModal').modal('hide');
            }
        });
    });

    $(document).on('click', '#confirmDeleteRootClassesButton', function () {
        var classId = $('#deleteRootClassId').val();
        if (classId.length == 0) {
            return;
        }
        $.ajax({
            url:     '/nlp-classifiers/delete-classifier-classes',
            type:    "POST",
            data:    'classifierClassId=' + classId,
            success: function (data) {
                if (data == 'true') {
                    updateRootClassifierClasses();
                } else {
                    alert('An error occurred while deleting the class!');
                }
                $('#confirmDeleteRootClassModal').modal('hide');
            }
        });
    });


    /**********  CLASS PHRASES FUNCTIONALITY **********/
    // Click on available models dropdown
    $(document).on('click', '#classifiersBindableForm div[name="classifierModel"] li', function () {
        var model = $(this).find('a').html();
        $(this).closest('div').find('span[name="classifierModelText"]').html(model);
        $(this).closest('div').find('input[name="classifierModel"]').val(model);
    });
    $(document).on('click', '#rootClassifiersBindableForm div[name="rootClassifierModel"] li', function () {
        var model = $(this).find('a').html();
        $(this).closest('div').find('span[name="rootClassifierModelText"]').html(model);
        $(this).closest('div').find('input[name="rootClassifierModel"]').val(model);
    });

    //Click check all phrases button
    $(document).on('click', '#checkAllPhrasesButton', function () {
        if (allClassPhrasesItemsWasChecked) {
            allClassPhrasesItemsWasChecked = false;
            $('#phrases li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allClassPhrasesItemsWasChecked = true;
            $('#phrases li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });

    $(document).on('click', '#checkAllRootPhrasesButton', function () {
        if (allRootClassPhrasesItemsWasChecked) {
            allRootClassPhrasesItemsWasChecked = false;
            $('#rootPhrases li.list-group-item input[name="itemCheckbox"]').prop('checked', false);
        } else {
            allRootClassPhrasesItemsWasChecked = true;
            $('#rootPhrases li.list-group-item input[name="itemCheckbox"]').prop('checked', true);
        }
    });

    // Click on phrases item event
    $(document).on('click', '#phrases li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        var phraseId = $(this).children('input[name="phraseId"]').val();
        if (phraseId === undefined) {
            currentClassPhraseId = null;
            return;
        }
        currentClassPhraseId = phraseId;
    });

    $(document).on('click', '.openEntities', function (event) {
        var phraseItem = $(document).find('#phrases li.current');
        if(phraseItem.next('#entitiesForm').length) {
            $('#entitiesForm').remove();
            event.stopPropagation();
            return;
        }
        phraseItem.click();
        getPhraseEntities(phraseItem, currentClassPhraseId, 'phrases');
    });
    $(document).on('click', '.openRootEntities', function () {
        var phraseItem = $(document).find('#rootPhrases li.current');
        if(phraseItem.next('#entitiesForm').length) {
            $('#entitiesForm').remove();
            event.stopPropagation();
            return;
        }
        phraseItem.click();
        getPhraseEntities(phraseItem, currentRootClassPhraseId, 'rootPhrases');
    });

    function getPhraseEntities(phraseItem, phraseId, phrasesHolder) {
        $.ajax({
            url:     '/nlp-classifiers/get-phrase-entities',
            type:    "POST",
            data:    'phraseId=' + phraseId,
            success: function (data) {
                if (data) {
                    $('#entitiesForm').remove();
                    $('#' + phrasesHolder +' li.current').after(getEntititesHtml(data, phraseItem, phrasesHolder));
                } else {
                    alert('An error occurred while update entities for phrase!');
                }
            }
        });
    }

    function getEntititesHtml(entities, phraseItem, phrasesHolder) {
        var out = "";
        var preparedEntities = jQuery.parseJSON(entities);

        out +=
            "<div id='entitiesForm' style='border: 2px dashed darkgray; padding: 7px; margin-bottom: 5px; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px; border-top-style: none;'>" +

                "<div style='float: right; margin-bottom: 5px;'>" +
                    "<button class='add btn btn-success' type='button' data-multi='false'>Add</button> " +
                    "<button class='add btn btn-success' type='button' data-multi='true'>Add All</button> " +
                    "<button class='update btn btn-primary' type='button' data-multi='false'>Update</button> " +
                    "<button class='update btn btn-primary' type='button' data-multi='true'>Update All</button> " +
                    "<button class='delete btn btn-danger' type='button' data-multi='false'>Delete</button>" +
                    "<button class='delete btn btn-danger' type='button' data-multi='true'>Delete All</button>" +
                "</div>"+

                "<div class=\"form-group\">" +
                    "<label for='Name'>Name</label>" +
                    "<input id='Name' class='form-control'>" +
                "</div>" +
                "<div class=\"form-group\">" +
                    "<label for='Value'>Value</label>" +
                    "<input id='Value' class='form-control'>" +
                "</div>" +


                "<div class='items'> <p>Entities: </p>";

                    var phrase = phraseItem.find('span[name="itemText"]').text();
                    var transformedPhraseText = "";

                    $.each(preparedEntities, function( key, object ) {
                        out +=
                            '<a class="btn btn-primary entityItem" style="margin: 3px;" ' +
                            'data-id="'+ object['learning_phrase_entity_id'] +'" ' +
                            'data-name="'+ object['entity'] +'" ' +
                            'data-value="'+ object['value'] +'">' +
                                object['entity'] +
                                '&nbsp;&nbsp;' +
                                '<b>' + object['value'] + '</b>' +
                            '</a>';

                        transformedPhraseText = phrase.replace(
                            object['value'],
                            "<mark style='background-color: yellow'>" + object['value'] + "</mark>"
                        );
                        phrase = transformedPhraseText;
                    });

                    if(phrasesHolder == 'rootPhrases') {
                        phrase += "<span class=\"glyphicon glyphicon-chevron-down pull-right openRootEntities\"></span>"
                    } else {
                        phrase += "<span class=\"glyphicon glyphicon-chevron-down pull-right openEntities\"></span>"
                    }
                    phraseItem.find('span[name="itemText"]').html('').html(phrase );

        out +=
                "<input type='hidden' id='EntityId' value='0'>" +
                "</div>" +
            "</div>";

        return out;
    }

    $(document).on('click', '#entitiesForm a.entityItem', function (event) {
        var btn         = $(event.target).closest('a');
        var name        = btn.attr('data-name');
        var value       = btn.attr('data-value');
        var id          = btn.attr('data-id');
        var nameInput   = $('#entitiesForm #Name');
        var valueInput  = $('#entitiesForm #Value');
        var entityInput = $('#entitiesForm #EntityId');

        nameInput.val('').val(name);
        valueInput.val('').val(value);
        entityInput.val('').val(id);
    });

    $(document).on('click', '#phrases #entitiesForm .add', function (event) {
        var phraseItem  = $('#phrases ul li.current');
        var phrase      = $('#phrases ul li.current').find('span[name="itemText"]').text();
        var nameInput   = $('#entitiesForm #Name');
        var valueInput  = $('#entitiesForm #Value');
        var start       = phrase.indexOf(valueInput.val());
        var end         = (start + valueInput.val().length);
        var multi       = $(this).attr('data-multi');

        if (!valueInput.val().length || !nameInput.val().length) {
            alert('You must fill Name and Value for entity create new');
            return;
        }

        var action = "";

        if(multi == 'true') { console.log(multi)
            action = 'multi-add-phrase-entity';
        } else {
            action = 'add-phrase-entity';
        }

        $.ajax({
            url:  '/nlp-classifiers/' + action,
            type: "POST",
            data: 'phraseId=' + currentClassPhraseId + '&classId=' + currentClassifierClassId + '&entity=' + nameInput.val() + '&value=' + valueInput.val() + '&start=' + start + '&end=' + end,
            success: function (data) {
                if (data) {
                    getPhraseEntities(phraseItem, currentClassPhraseId, 'phrases');
                } else {
                    alert('An error occurred while creating entity!');
                }
            }
        });
    });

    $(document).on('click', '#rootPhrases #entitiesForm .add', function (event) {
        var phraseItem  = $('#rootPhrases ul li.current');
        var phrase      = $('#rootPhrases ul li.current').find('span[name="itemText"]').text();
        var nameInput   = $('#entitiesForm #Name');
        var valueInput  = $('#entitiesForm #Value');
        var start       = phrase.indexOf(valueInput.val());
        var end         = (start + valueInput.val().length);
        var multi       = $(this).attr('data-multi');

        if (!valueInput.val().length || !nameInput.val().length) {
            alert('You must fill Name and Value for entity create new');
            return;
        }

        var action = "";

        if(multi == 'true') { console.log(multi)
            action = 'multi-add-phrase-entity';
        } else {
            action = 'add-phrase-entity';
        }


        $.ajax({
            url:  '/nlp-classifiers/' + action,
            type: "POST",
            data: 'phraseId=' + currentRootClassPhraseId + '&classId=' + currentRootClassifierClassId + '&entity=' + nameInput.val() + '&value=' + valueInput.val() + '&start=' + start + '&end=' + end,
            success: function (data) {
                if (data) {
                    getPhraseEntities(phraseItem, currentRootClassPhraseId, 'rootPhrases');
                } else {
                    alert('An error occurred while creating entity!');
                }
            }
        });

    });

    $(document).on('click', '#phrases #entitiesForm .update', function (event) {
        var phraseItem  = $('#phrases ul li.current');
        var phrase      = $('#phrases ul li.current').find('span[name="itemText"]').text();
        var nameInput   = $('#entitiesForm #Name');
        var valueInput  = $('#entitiesForm #Value');
        var start       = phrase.indexOf(valueInput.val());
        var end         = (start + valueInput.val().length);
        var entityId    = $('#entitiesForm #EntityId').val();
        var multi       = $(this).attr('data-multi');

        if (!entityId.length) {
            alert('You must choose entity to update');
            return;
        }

        var action = "";

        if(multi == 'true') {
            action = 'multi-update-phrase-entity';
        } else {
            action = 'update-phrase-entity';
        }

        $.ajax({
            url:  '/nlp-classifiers/' + action,
            type: "POST",
            data: 'phraseId=' + currentClassPhraseId + '&classId=' + currentClassifierClassId + '&entity=' + nameInput.val() + '&value=' + valueInput.val() + '&start=' + start + '&end=' + end +'&entityId='+entityId,
            success: function (data) {
                if (data) {
                    getPhraseEntities(phraseItem, currentClassPhraseId, 'phrases');
                } else {
                    alert('An error occurred while updating entity!');
                }
            }
        });
    });

    $(document).on('click', '#rootPhrases #entitiesForm .update', function (event) {
        var phraseItem  = $('#rootPhrases ul li.current');
        var phrase      = $('#rootPhrases ul li.current').find('span[name="itemText"]').text();
        var nameInput   = $('#entitiesForm #Name');
        var valueInput  = $('#entitiesForm #Value');
        var start       = phrase.indexOf(valueInput.val());
        var end         = (start + valueInput.val().length);
        var entityId    = $('#entitiesForm #EntityId').val();
        var multi       = $(this).attr('data-multi');

        if (!entityId.length) {
            alert('You must choose entity to update');
            return;
        }

        var action = "";

        if(multi == 'true') {
            action = 'multi-update-phrase-entity';
        } else {
            action = 'update-phrase-entity';
        }

        $.ajax({
            url:  '/nlp-classifiers/' + action,
            type: "POST",
            data: 'phraseId=' + currentRootClassPhraseId + '&classId=' + currentRootClassifierClassId + '&entity=' + nameInput.val() + '&value=' + valueInput.val() + '&start=' + start + '&end=' + end +'&entityId='+entityId,
            success: function (data) {
                if (data) {
                    getPhraseEntities(phraseItem, currentRootClassPhraseId, 'rootPhrases');
                } else {
                    alert('An error occurred while updating entity!');
                }
            }
        });
    });

    $(document).on('click', '#phrases #entitiesForm .delete', function (event) {
        var phraseItem  = $('#phrases ul li.current');
        var nameInput   = $('#entitiesForm #Name');
        var entityId    = $('#entitiesForm #EntityId').val();
        var multi       = $(this).attr('data-multi');
        var valueInput  = $('#entitiesForm #Value');

        if(!confirm('Are you sure that you want delete "' + nameInput.val() + '" entity for this phrase?')) {
            return;
        }

        if (!entityId.length) {
            alert('You must choose entity to update');
            return;
        }

        var action = "";
        console.log(multi);

        if(multi == 'true') { console.log(multi)
            action = 'multi-delete-phrase-entity';
        } else {
            action = 'delete-phrase-entity';
        }


        $.ajax({
            url:     '/nlp-classifiers/' + action,
            type:    "POST",
            data:    'entityId=' + entityId + '&value='+valueInput.val() + '&classId=' + currentClassifierClassId,
            success: function (data) {
                if (data) {
                    getPhraseEntities(phraseItem, currentClassPhraseId, 'phrases');
                } else {
                    alert('An error occurred while deletin entity!');
                }
            }
        });
    });

    $(document).on('click', '#rootPhrases #entitiesForm .delete', function (event) {
        var phraseItem  = $('#rootPhrases ul li.current');
        var nameInput   = $('#entitiesForm #Name');
        var entityId    = $('#entitiesForm #EntityId').val();
        var multi       = $(this).attr('data-multi');
        var valueInput  = $('#entitiesForm #Value');

        if(!confirm('Are you sure that you want delete "' + nameInput.val() + '" entity for this phrase?')) {
            return;
        }

        if (!entityId.length) {
            alert('You must choose entity to update');
            return;
        }

        var action = "";

        if(multi == 'true') { console.log(multi)
            action = 'multi-delete-phrase-entity';
        } else {
            action = 'delete-phrase-entity';
        }

        $.ajax({
            url:     '/nlp-classifiers/' + action,
            type:    "POST",
            data:    'entityId=' + entityId + '&value='+valueInput.val() + '&classId=' + currentRootClassifierClassId,
            success: function (data) {
                if (data) {
                    getPhraseEntities(phraseItem, currentRootClassPhraseId, 'rootPhrases');
                } else {
                    alert('An error occurred while deletin entity!');
                }
            }
        });
    });



    $(document).on('click', '#rootPhrases li.list-group-item', function () {
        if ($(this).hasClass('lastClickedItem')) {
            return;
        }
        $(this).siblings().removeClass('lastClickedItem');
        $(this).addClass('lastClickedItem');

        var phraseId = $(this).children('input[name="rootPhraseId"]').val();
        if (phraseId === undefined) {
            currentRootClassPhraseId = null;
            return;
        }
        currentRootClassPhraseId = phraseId;
    });

    // Click 'Add phrase' button
    $(document).on('click', '#addPhraseButton', function () {
        var text        = $('#phraseText').val();
        text            = encodeURIComponent(text);

        $.ajax({
            url:    '/nlp-classifiers/create-class-phrases',
            type:   "POST",
            data:   'classId=' + currentClassifierClassId + '&text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateClassPhrases();
                } else {
                    alert('An error occurred while creating the class phrase!');
                }
            }
        });
    });

    // Click 'Add phrase' button
    $(document).on('click', '#addRootPhraseButton', function () {
        var text        = $('#rootPhraseText').val();
        text            = encodeURIComponent(text);

        $.ajax({
            url:    '/nlp-classifiers/create-class-phrases',
            type:   "POST",
            data:   'classId=' + currentRootClassifierClassId + '&text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateRootClassPhrases();
                } else {
                    alert('An error occurred while creating the class phrase!');
                }
            }
        });
    });

    // Click 'Update phrase' button
    $(document).on('click', '#updatePhraseButton', function () {
        var text        = $('#phraseText').val();
        text            = encodeURIComponent(text);

        var classPhraseId = $('#phrasesBindableForm input[name="phraseId"]').val();
        if (classPhraseId.length == 0) {
            return;
        }

        $.ajax({
            url:     '/nlp-classifiers/update-class-phrases',
            type:    "POST",
            data:    'classPhraseId=' + classPhraseId + '&text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateClassPhrases();
                } else {
                    alert('An error occurred while updating the class phrase!');
                }
            }
        });
    });

    // Click 'Update phrase' button
    $(document).on('click', '#updateRootPhraseButton', function () {
        var text        = $('#rootPhraseText').val();
        text            = encodeURIComponent(text);

        var classPhraseId = $('#rootPhrasesBindableForm input[name="rootPhraseId"]').val();
        if (classPhraseId.length == 0) {
            return;
        }

        $.ajax({
            url:     '/nlp-classifiers/update-class-phrases',
            type:    "POST",
            data:    'classPhraseId=' + classPhraseId + '&text=' + text,
            success: function (data) {
                if (data == 'true') {
                    updateRootClassPhrases();
                } else {
                    alert('An error occurred while updating the class phrase!');
                }
            }
        });
    });

    // Click class phrases delete button
    $(document).on('click', '#deletePhraseButton', function () {
        var classPhrasesNames = getCheckedOrCurrentItemsNames('phrasesBindableForm', 'phraseId', 'phrases', 'phraseId');
        $('#deleteClassPhrasesNames').html(classPhrasesNames);
        $('#confirmDeleteClassPhrasesModal').modal('show');
    });

    $(document).on('click', '#deleteRootPhraseButton', function () {
        var classPhrasesNames = getCheckedOrCurrentItemsNames('rootPhrasesBindableForm', 'rootPhraseId', 'rootPhrases', 'rootPhraseId');
        $('#deleteRootClassPhrasesNames').html(classPhrasesNames);
        $('#confirmDeleteRootClassPhrasesModal').modal('show');
    });

    // Click confirm class phrases delete button
    $(document).on('click', '#confirmDeleteClassPhrasesButton', function () {
        var classPhraseIds = getCheckedOrCurrentObjectsIds('phrasesBindableForm', 'phraseId', 'phrases', 'phraseId');
        if (classPhraseIds.length == 0) {
            return;
        }
        var params = [];
        $.each(classPhraseIds, function (index, value) {
            var elem = {};
            elem.learning_phrase_id = value;
            params.push(elem);
        });

        $.ajax({
            url:     '/nlp-classifiers/delete-class-phrases',
            type:    "POST",
            data:    'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updateClassPhrases();
                } else {
                    alert('An error occurred while deleting the class phrases!');
                }
                $('#confirmDeleteClassPhrasesModal').modal('hide');
            }
        });
    });

    $(document).on('click', '#confirmDeleteRootClassPhrasesButton', function () {
        var classPhraseIds = getCheckedOrCurrentObjectsIds('rootPhrasesBindableForm', 'rootPhraseId', 'rootPhrases', 'rootPhraseId');
        if (classPhraseIds.length == 0) {
            return;
        }
        var params = [];
        $.each(classPhraseIds, function (index, value) {
            var elem = {};
            elem.learning_phrase_id = value;
            params.push(elem);
        });

        $.ajax({
            url:     '/nlp-classifiers/delete-class-phrases',
            type:    "POST",
            data:    'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updateRootClassPhrases();
                } else {
                    alert('An error occurred while deleting the class phrases!');
                }
                $('#confirmDeleteRootClassPhrasesModal').modal('hide');
            }
        });
    });

    // Click confirm class phrases delete button
    $(document).on('click', '#confirmDeleteRootClassPhrasesButton', function () {
        var classPhraseIds = getCheckedOrCurrentObjectsIds('rootPhrasesBindableForm', 'rootPhraseId', 'rootPhrases', 'rootPhraseId');
        if (classPhraseIds.length == 0) {
            return;
        }
        var params = [];
        $.each(classPhraseIds, function (index, value) {
            var elem = {};
            elem.learning_phrase_id = value;
            params.push(elem);
        });

        $.ajax({
            url:     '/nlp-classifiers/delete-class-phrases',
            type:    "POST",
            data:    'params=' + JSON.stringify(params),
            success: function (data) {
                if (data == 'true') {
                    updateRootClassPhrases();
                } else {
                    alert('An error occurred while deleting the class phrases!');
                }
                $('#confirmDeleteRootClassPhrasesModal').modal('hide');
            }
        });
    });

    //Disable and toggle tabs for containers
    $('.nav-tabs a[href="#rootClassifiersTab"]').click(function(){
        $('.nav-tabs a[href="#rootClassesTab"]').tab('show');
        $('.nav-tabs a[href="#rootPhrasesTab"]').tab('show');
    });
    $('.nav-tabs a[href="#classifiersTab"]').click(function(){
        $('.nav-tabs a[href="#classesTab"]').tab('show');
        $('.nav-tabs a[href="#phrasesTab"]').tab('show');
    });
    $('.nav-tabs a[href="#rootClassesTab"]').click(function () {
        return false;
    });
    $('.nav-tabs a[href="#rootPhrasesTab"]').click(function () {
        return false;
    });
    $('.nav-tabs a[href="#classesTab"]').click(function () {
        return false;
    });
    $('.nav-tabs a[href="#phrasesTab"]').click(function () {
        return false;
    });
});
