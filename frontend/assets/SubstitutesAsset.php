<?php

namespace frontend\assets;

class SubstitutesAsset extends BasicAsset
{
    public $js = [
        'js/substitutes.js',
    ];
}
