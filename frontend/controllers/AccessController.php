<?php

namespace frontend\controllers;

use frontend\models\access\AccessFormModel;
use frontend\models\access\UserItem;
use yii\base\Module;
use yii\web\ForbiddenHttpException;

class AccessController extends BaseNgController
{
    /* @var $paginationPageSize int */
    protected $paginationPageSize = 10;

    /**
     * AccessController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->setFormModel(new AccessFormModel());
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('access')) {
                throw new ForbiddenHttpException('Access denied');
            }

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * Creates new user with auth assignment
     *
     * @return array
     */
    public function actionCreate(): array
    {
        $accessFormModel = $this->getFormModel();

        if (!$accessFormModel->loadFormData($this->requestData)) {
            return [];
        }

        if (!$accessFormModel->validate()) {
            return [
                'status' => false,
                'errors' => $accessFormModel->getErrors()
            ];
        }

        if (!$userModel = $accessFormModel->createUser()) {
            return [
                'status' => false,
                'errors' => $userModel->getErrors()
            ];
        }

        if (!$authAssignment = $accessFormModel->linkAuthAssignment($accessFormModel->role, $userModel->id)) {
            return [];
        }

        return $this->actionGetUsersData();
    }

    /**
     * Update user role
     *
     * @return array
     */
    public function actionUpdate()
    {
        $accessFormModel = $this->getFormModel();

        $userId  = (int) $this->requestData['userId'];
        $newRole = $this->requestData['newRole'];

        if (!$accessFormModel->updateUser($userId, $newRole)) {
            return [];
        }

        return $this->actionGetUsersData();
    }

    /**
     * Delete user wit all relations
     *
     * @return array
     */
    public function actionDelete()
    {
        $accessFormModel = $this->getFormModel();

        if (!$accessFormModel->deleteUser((int) $this->requestData['userId'])) {
            return [];
        }

        return $this->actionGetUsersData();
    }

    /**
     * Get users list
     *
     * @param int $page
     * @return array
     */
    public function actionGetUsersData($page = 1): array
    {
        $page = (int) $page;

        $userModelsQuery = UserItem::find()
            ->limit($this->paginationPageSize)
            ->offset($this->getPagerOffsetFromPage($page))
            ->asArray()
            ->orderBy(['id' => SORT_ASC])
        ;

        $items = $userModelsQuery->all();

        foreach ($items as $key => $item) {
            $userRoles           = array_keys(\Yii::$app->authManager->getRolesByUser($item['id']));
            $items[$key]['role'] = $userRoles[0];
        }

        return [
            'items' => $items,
            'count' => $userModelsQuery->count()
        ];
    }

    /**
     * Get available roles for dropDowns
     * @return array
     */
    public function actionGetUserRoles(): array
    {
        $availableRoles = [];

        foreach (\Yii::$app->authManager->getRoles() as $key => $value) {
            $availableRoles[] = $key;
        }

        return $availableRoles;
    }

    /**
     * @return AccessFormModel
     */
    protected function getFormModel(): AccessFormModel
    {
        return $this->formModel;
    }
}
