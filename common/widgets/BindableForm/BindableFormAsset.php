<?php
namespace common\widgets\BindableForm;

use yii\web\AssetBundle;

class BindableFormAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/BindableForm/assets';

    public $css = [
        'css/bindableform.css',
    ];

    public $js = [
        'js/bindableform.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        //'forceCopy'=>true,
    ];
}
