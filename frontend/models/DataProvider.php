<?php
declare(strict_types=1);

namespace frontend\models;

use common\components\BrungildaApi\BrungildaApiCurl;
use frontend\models\search\SearchForm;

trait topicsTrait
{
    public function getTopics()
    {
        $topics = array();
        $arr = \Yii::$app->BrungildaApiCurl->getTopics();
        $restrictions = \Yii::$app->BrungildaApiCurl->getRestrictions();
        foreach ($arr as $val) {
            $topic = new topics\Topic(
                $val['topic_id'],
                $val['name'],
                $val['priority'],
                (bool)$val['enabled'],
                (bool)$val['active'],
                (bool)$val['switching'],
                (bool)$val['deleted'],
                $val['version'],
                (bool)$val['status_out'],
                $val['lang'] ?? ''
            );

            foreach ($restrictions as $restriction) {
                if ($restriction['id'] == $val['topic_id']) {
                    $topic->restrictions[] = $restriction['scenario_id'];
                }
            }

            $topics[] = $topic;
        }
        //Sort topics by priority
        usort($topics, array($this, "comparePriorityForSort"));
        return $topics;
    }

    public function getRules(int $topicId)
    {
        $rules = array();
        $arr = \Yii::$app->BrungildaApiCurl->getRulesByTopicId($topicId);
        foreach ($arr as $val) {
            if ($val['enabled'] == null) {
                $val['enabled'] = false;
            }
            $rule = new topics\Rule(
                $val['rule_id'],
                $val['topic_id'],
                $val['exclude_template'],
                $val['exec_limit'],
                $val['priority'],
                (bool)$val['enabled'],
                (bool)$val['active'],
                (bool)$val['deleted'],
                $val['name'],
                $val['version'],
                $val['parent_id'] == null ? self::DEFAULT_ID : $val['parent_id'],
                $val['link_id'] == null ? self::DEFAULT_ID : $val['link_id'],
                $val['phrase_id'] == null ? self::DEFAULT_ID : $val['phrase_id'],
                $val['type_phrase_id'] == null ? self::DEFAULT_ID : $val['type_phrase_id']
            );
            $rules[] = $rule;
        }
        //Sort rules by priority
        usort($rules, array($this, "comparePriorityForSort"));
        return $rules;
    }

    public function getRule(int $ruleId)
    {
        $arr = \Yii::$app->BrungildaApiCurl->getRuleById($ruleId);

        $rule = new topics\Rule(
            $arr['rule']['rule_id'],
            $arr['rule']['topic_id'],
            $arr['rule']['exclude_template'],
            $arr['rule']['exec_limit'],
            $arr['rule']['priority'],
            (bool)$arr['rule']['enabled'],
            (bool)$arr['rule']['active'],
            (bool)$arr['rule']['deleted'],
            $arr['rule']['name'],
            $arr['rule']['version'],
            $arr['rule']['parent_id'] == null ? self::DEFAULT_ID : $arr['rule']['parent_id'],
            $arr['rule']['link_id'] == null ? self::DEFAULT_ID : $arr['rule']['link_id'],
            $arr['rule']['phrase_id'] == null ? self::DEFAULT_ID : $arr['rule']['phrase_id'],
            $arr['rule']['type_phrase_id'] == null ? self::DEFAULT_ID : $arr['rule']['type_phrase_id']
        );

        $patterns = array();
        foreach ($arr['rule']['patterns'] as $val) {
            if ($val['active'] == null) {
                $val['active'] = false;
            }
            $pattern = new topics\RulesPattern(
                $val['pattern_id'],
                $val['rule_id'],
                $val['text'],
                (bool)$val['deleted'],
                $val['test'],
                $val['version'],
                (bool)$val['active']
            );
            $patterns[] = $pattern;
        }
        $rule->setPatterns($patterns);

        $templates = array();
        foreach ($arr['rule']['templates'] as $val) {
            $template = new topics\RulesTemplate(
                $val['template_id'],
                $val['rule_id'],
                $val['text'],
                (bool)$val['deleted'],
                $val['delay_id'] == null ? self::DEFAULT_ID : $val['delay_id'],
                $val['timeslot_id'] == null ? self::DEFAULT_ID : $val['timeslot_id'],
                $val['version']
            );
            $templates[] = $template;
        }
        $rule->setTemplates($templates);

        $excPatterns = array();
        foreach ($arr['rule']['exception_patterns'] as $val) {
            if ($val['active'] == null) {
                $val['active'] = false;
            }
            $pattern = new topics\RulesPattern(
                $val['exception_pattern_id'],
                $val['rule_id'],
                $val['text'],
                (bool)$val['deleted'],
                $val['test'],
                $val['version'],
                (bool)$val['active']
            );
            $excPatterns[] = $pattern;
        }
        $rule->setExceptionPatterns($excPatterns);

        $subrules = array();
        foreach ($arr['rule']['rules'] as $val) {
            $subrule = new topics\Rule(
                $val['rule_id'],
                $val['topic_id'] == null ? self::DEFAULT_ID : $val['topic_id'],
                $val['exclude_template'],
                $val['exec_limit'],
                $val['priority'],
                (bool)$val['enabled'],
                (bool)$val['active'],
                (bool)$val['deleted'],
                $val['name'],
                $val['version'],
                $val['parent_id'] == null ? self::DEFAULT_ID : $val['parent_id'],
                $val['link_id'] == null ? self::DEFAULT_ID : $val['link_id'],
                $val['phrase_id'] == null ? self::DEFAULT_ID : $val['phrase_id'],
                $val['type_phrase_id'] == null ? self::DEFAULT_ID : $val['type_phrase_id']
            );
            $subrules[] = $subrule;
        }
        //Sort subrules by priority
        usort($subrules, array($this, "comparePriorityForSort"));
        $rule->setRules($subrules);

        return $rule;
    }

    public function getRuleById(int $ruleId)
    {
        $arr = \Yii::$app->BrungildaApiCurl->getRuleById($ruleId);

        if (empty($arr)) {
            return null;
        }

        try {
            $rule = new topics\Rule(
                $arr['rule']['rule_id'],
                $arr['rule']['topic_id'],
                $arr['rule']['exclude_template'],
                $arr['rule']['exec_limit'],
                $arr['rule']['priority'],
                (bool)$arr['rule']['enabled'],
                (bool)$arr['rule']['active'],
                (bool)$arr['rule']['deleted'],
                $arr['rule']['name'],
                $arr['rule']['version'],
                $arr['rule']['parent_id']      == null ? self::DEFAULT_ID : $arr['rule']['parent_id'],
                $arr['rule']['link_id']        == null ? self::DEFAULT_ID : $arr['rule']['link_id'],
                $arr['rule']['phrase_id']      == null ? self::DEFAULT_ID : $arr['rule']['phrase_id'],
                $arr['rule']['type_phrase_id'] == null ? self::DEFAULT_ID : $arr['rule']['type_phrase_id']
            );
        } catch (\Exception $e) {
            return null;
        }

        return $rule;
    }

    public function getRawRule(int $ruleId)
    {
        return \Yii::$app->BrungildaApiCurl->getRuleById($ruleId);
    }

    public function updateTopic(int $topicId, array $params)
    {
        $params['topic_id'] = $topicId;

        if (isset($params['checkedScenarios'])) {
            $restrictions = \Yii::$app->BrungildaApiCurl->getRestrictions();
            $restrictionsToDelete = [];
            foreach ($restrictions as $restriction) {
                if ($restriction['id'] == $topicId) {
                    $restrictionsToDelete[] = $restriction;
                }
            }
            $restrictionDeleteResponse = \Yii::$app->BrungildaApiCurl->deleteRestrictions($restrictionsToDelete);

            $restrictionsToCreate = [];
            foreach ($params['checkedScenarios'] as $scenarioId) {
                $restrictionsToCreate[] = [
                    'tb_name' => 'topics',
                    'id'=> $topicId,
                    'scenario_id' => (int)$scenarioId
                ];
            }
            $restrictionCreateResponse = \Yii::$app->BrungildaApiCurl->createRestrictions($restrictionsToCreate);

            unset($params['checkedScenarios']);
        }

        $response = \Yii::$app->BrungildaApiCurl->updateTopics(array($params));
        return $response;
    }

    public function updateTopics(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->updateTopics($params);
        return $response;
    }

    public function createTopic(string $name)
    {
        $params['name'] = $name;
        $response = \Yii::$app->BrungildaApiCurl->createTopics(array($params));
        return $response;
    }

    public function deleteTopic(int $topicId)
    {
        $params['topic_id'] = $topicId;

        $restrictions = \Yii::$app->BrungildaApiCurl->getRestrictions();
        $restrictionsToDelete = [];
        foreach ($restrictions as $restriction) {
            if ($restriction['id'] == $topicId) {
                $restrictionsToDelete[] = $restriction;
            }
        }
        $restrictionDeleteResponse = \Yii::$app->BrungildaApiCurl->deleteRestrictions($restrictionsToDelete);

        $response = \Yii::$app->BrungildaApiCurl->deleteTopics(array($params));
        return $response;
    }

    public function updateRule(int $ruleId, array $params)
    {
        $params['rule_id'] = $ruleId;
        $response = \Yii::$app->BrungildaApiCurl->updateRules(array($params));
        return $response;
    }

    public function updateRules(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->updateRules($params);
        return $response;
    }

    public function createRule(int $topicId, string $name)
    {
        $params['topic_id'] = $topicId;
        $params['name'] = $name;
        $response = \Yii::$app->BrungildaApiCurl->createRules(array($params));
        return $response;
    }

    public function createRawRule($params)
    {
        $response = \Yii::$app->BrungildaApiCurl->createRules(array($params));
        return $response;
    }

    public function deleteRule(int $topicId, int $ruleId)
    {
        $params['topic_id'] = $topicId;
        $params['rule_id'] = $ruleId;
        $response = \Yii::$app->BrungildaApiCurl->deleteRules(array($params));
        return $response;
    }

    public function deleteSubRule(int $ruleId)
    {
        $params['rule_id'] = $ruleId;
        $response = \Yii::$app->BrungildaApiCurl->deleteRules(array($params));
        return $response;
    }

    public function createPattern(int $ruleId, string $test, string $text)
    {
        $params['rule_id'] = $ruleId;
        $params['test'] = $test;
        $params['text'] = $text;
        $response = \Yii::$app->BrungildaApiCurl->createPatterns(array($params));
        return $response;
    }

    public function createPatterns(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->createPatterns($params);
        return $response;
    }

    public function deletePattern(int $patternId)
    {
        $params['pattern_id'] = $patternId;
        $response = \Yii::$app->BrungildaApiCurl->deletePatterns(array($params));
        return $response;
    }

    public function deletePatterns(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->deletePatterns($params);
        return $response;
    }

    public function updatePattern(int $patternId, string $test, string $text)
    {
        $params['pattern_id'] = $patternId;
        $params['test'] = $test;
        $params['text'] = $text;
        $response = \Yii::$app->BrungildaApiCurl->updatePatterns(array($params));
        return $response;
    }

    public function updatePatternParams(int $patternId, array $params)
    {
        $params['pattern_id'] = $patternId;
        $response = \Yii::$app->BrungildaApiCurl->updatePatterns(array($params));
        return $response;
    }

    public function updatePatterns(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->updatePatterns($params);
        return $response;
    }

    public function getPatternsVariables()
    {
        $response = \Yii::$app->BrungildaApiCurl->getPatternsVariables();
        return $response;
    }

    public function createPatternVariable(int $patternId, int $variableId, int $regexpId)
    {
        $params['pattern_id'] = $patternId;
        $params['variable_id'] = $variableId;
        $params['index_content'] = $regexpId;
        $response = \Yii::$app->BrungildaApiCurl->createPatternVariable($params);
        return $response;
    }

    public function createRawPatternVariable(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->createPatternVariable($params);
        return $response;
    }

    public function updatePatternVariable(int $patternVariableId, int $patternId, int $variableId, int $regexpId)
    {
        $params['pattern_var_id'] = $patternVariableId;
        $params['pattern_id'] = $patternId;
        $params['variable_id'] = $variableId;
        $params['index_content'] = $regexpId;
        $response = \Yii::$app->BrungildaApiCurl->updatePatternVariable($params);
        return $response;
    }

    public function deletePatternVariable(int $patternVariableId)
    {
        $params['pattern_var_id'] = $patternVariableId;
        $response = \Yii::$app->BrungildaApiCurl->deletePatternVariable($params);
        return $response;
    }

    //Exception patterns begin
    public function createExcPattern(int $ruleId, string $test, string $text)
    {
        $params['rule_id'] = $ruleId;
        $params['test'] = $test;
        $params['text'] = $text;
        $response = \Yii::$app->BrungildaApiCurl->createExceptionPatterns(array($params));
        return $response;
    }

    public function createExcPatterns(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->createExceptionPatterns($params);
        return $response;
    }

    public function deleteExcPattern(int $patternId)
    {
        $params['exception_pattern_id'] = $patternId;
        $response = \Yii::$app->BrungildaApiCurl->deleteExceptionPatterns(array($params));
        return $response;
    }

    public function deleteExcPatterns(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->deleteExceptionPatterns($params);
        return $response;
    }

    public function updateExcPattern(int $patternId, string $test, string $text)
    {
        $params['exception_pattern_id'] = $patternId;
        $params['test'] = $test;
        $params['text'] = $text;
        $response = \Yii::$app->BrungildaApiCurl->updateExceptionPatterns(array($params));
        return $response;
    }

    public function updateExcPatternParams(int $patternId, array $params)
    {
        $params['exception_pattern_id'] = $patternId;
        $response = \Yii::$app->BrungildaApiCurl->updateExceptionPatterns(array($params));
        return $response;
    }

    public function updateExcPatterns(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->updateExceptionPatterns($params);
        return $response;
    }
    //Exception patterns end

    public function createTemplate(int $ruleId, string $text)
    {
        $params['rule_id'] = $ruleId;
        $params['text'] = $text;
        $response = \Yii::$app->BrungildaApiCurl->createTemplates(array($params));
        return $response;
    }

    public function createTemplates(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->createTemplates($params);
        return $response;
    }

    public function deleteTemplate(int $templateId)
    {
        $params['template_id'] = $templateId;
        $response = \Yii::$app->BrungildaApiCurl->deleteTemplates(array($params));
        return $response;
    }

    public function deleteTemplates(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->deleteTemplates($params);
        return $response;
    }

    public function updateTemplate(int $templateId, string $text, $timeslotId)
    {
        $params['template_id'] = $templateId;
        $params['text'] = $text;
        $params['timeslot_id'] = $timeslotId;
        $response = \Yii::$app->BrungildaApiCurl->updateTemplates(array($params));
        return $response;
    }

    public function createSubRule(int $parentId, string $name)
    {
        $params['parent_id'] = $parentId;
        $params['name'] = $name;
        $response = \Yii::$app->BrungildaApiCurl->createSubRule(array($params));
        return $response;
    }
}

trait phrasesTrait
{
    public function getGroupsPhrases()
    {
        $groupsPhrases = array();
        $arr = \Yii::$app->BrungildaApiCurl->getGroupPhrases();
        foreach ($arr as $val) {
            $groupPhrases = new phrases\GroupPhrases(
                $val['group_phrase_id'],
                $val['text'],
                (bool)$val['deleted'],
                $val['version'],
                $val['exclude_phrase'],
                $val['hasPlaceholder'] ?? 0,
                $val['hasPrePlaceholder'] ?? 0,
                $val['lang'] ?? ''
            );
            $groupsPhrases[] = $groupPhrases;
        }
        return $groupsPhrases;
    }

    public function getDelays()
    {
        $delays = array();
        $arr = \Yii::$app->BrungildaApiCurl->getDelays();
        foreach ($arr as $val) {
            $delay = new phrases\Delay(
                $val['delay_id'],
                $val['t1'],
                $val['t2'],
                $val['name']
            );
            $delays[] = $delay;
        }
        return $delays;
    }

    public function getTimeslots()
    {
        $timeslots = array();
        $arr = \Yii::$app->BrungildaApiCurl->getTimeslots();
        foreach ($arr as $val) {
            $timeslot = new phrases\Timeslot(
                $val['timeslot_id'],
                $val['date1'] ?? '',
                $val['time1'],
                $val['name'],
                $val['date2'] ?? '',
                $val['time2']
            );
            $timeslots[] = $timeslot;
        }
        return $timeslots;
    }

    public function getPhrases(int $groupPhrasesId)
    {
        $phrases = array();
        $arr = \Yii::$app->BrungildaApiCurl->getPhrases($groupPhrasesId);
        foreach ($arr['phrases'] as $val) {
            $phrase = new phrases\Phrase(
                $val['phrase_id'],
                $val['group_phrase_id'],
                $val['text'],
                $val['delay_id'] == null ? self::DEFAULT_ID : $val['delay_id'],
                $val['timeslot_id'] == null ? self::DEFAULT_ID : $val['timeslot_id'],
                $val['version'],
                (bool)$val['deleted']
            );
            $phrases[] = $phrase;
        }
        return $phrases;
    }

    public function createGroupPhrases(string $text)
    {
        $inputData = explode(PHP_EOL, $text);
        if(count($inputData) > 1) {
            $response = [];
            foreach ($inputData as $datum) {
                $params['text'] = $datum;
                $response[] = \Yii::$app->BrungildaApiCurl->createGroupPhrases(array($params));
            }
            return $response;
        }

        $params['text'] = $text;
        $response = \Yii::$app->BrungildaApiCurl->createGroupPhrases(array($params));
        return $response;
    }

    public function updateGroupPhrases(int $groupPhrasesId, string $text, string $lang, array $groupPhrasesIds)
    {
        $params = [];
        foreach ($groupPhrasesIds as $id) {
            $groupPhrase = [];
            $groupPhrase['group_phrase_id'] = $id;
            $groupPhrase['lang']            = $lang;
            if ($id == $groupPhrasesId) {
                $groupPhrase['text'] = $text;
            }
            $params[] = $groupPhrase;
        }
        $response = \Yii::$app->BrungildaApiCurl->updateGroupPhrases($params);
        return $response;
    }

    public function updateGroupPhrasesExcluded(int $groupPhrasesId, int $excluded)
    {
        $params['group_phrase_id'] = $groupPhrasesId;
        $params['exclude_phrase'] = $excluded;
        $response = \Yii::$app->BrungildaApiCurl->updateGroupPhrases(array($params));
        return $response;
    }

    public function updateGroupPhrasesHasPlaceholder(int $groupPhrasesId, int $hasPlaceholder)
    {
        $params['group_phrase_id'] = $groupPhrasesId;
        $params['hasPlaceholder'] = $hasPlaceholder;
        $response = \Yii::$app->BrungildaApiCurl->updateGroupPhrases(array($params));
        return $response;
    }

    public function updateGroupPhrasesHasPrePlaceholder(int $groupPhrasesId, int $hasPrePlaceholder)
    {
        $params['group_phrase_id'] = $groupPhrasesId;
        $params['hasPrePlaceholder'] = $hasPrePlaceholder;
        $response = \Yii::$app->BrungildaApiCurl->updateGroupPhrases(array($params));
        return $response;
    }

    public function deleteGroupPhrases(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->deleteGroupPhrases($params);
        return $response;
    }

    public function createPhrase(int $groupPhrasesId, string $text)
    {
        $params['group_phrase_id'] = $groupPhrasesId;
        $params['text'] = trim($text);
        $response = \Yii::$app->BrungildaApiCurl->createPhrases(array($params));
        return $response;
    }

    public function createPhrases(int $groupPhrasesId, array $phrases)
    {
        $params = array();
        foreach ($phrases as $phrase) {
            $item['group_phrase_id'] = $groupPhrasesId;
            $item['text'] = trim($phrase);
            $params[] = $item;
        }
        $response = \Yii::$app->BrungildaApiCurl->createPhrases($params);
        return $response;
    }

    public function updatePhrase(int $phraseId, string $text)
    {
        $params['phrase_id'] = $phraseId;
        $params['text'] = trim($text);
        $response = \Yii::$app->BrungildaApiCurl->updatePhrases(array($params));
        return $response;
    }

    public function updatePhrases(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->updatePhrases($params);
        return $response;
    }

    public function deletePhrases(array $params)
    {
        $response = \Yii::$app->BrungildaApiCurl->deletePhrases($params);
        return $response;
    }
}

trait classifiersTrait
{
    public function getClassifiers()
    {
        $classifiers = [];
        foreach (\Yii::$app->BrungildaApiCurl->getClassifiers() as $classifier) {
            if (isset($classifier['type']) && $classifier['type'] != "") {
                continue;
            }
            $classifier = new classifiers\Classifiers(
                $classifier['classifier_id'],
                $classifier['text'],
                (bool) $classifier['deleted'],
                $classifier['version'],
                $classifier['patterns'] ?? []
            );
            $classifiers[] = $classifier;
        }
        return $classifiers;
    }

    public function createClassifiers(string $text)
    {
        $params['text'] = $text;
        return \Yii::$app->BrungildaApiCurl->createClassifiers([$params]);
    }

    public function updateClassifiers(int $classifierId, string $text)
    {
        $params['classifier_id'] = $classifierId;
        $params['text']          = $text;
        return \Yii::$app->BrungildaApiCurl->updateClassifiers([$params]);
    }

    public function deleteClassifiers(int $classifierId)
    {
        $params['classifier_id'] = $classifierId;
        return \Yii::$app->BrungildaApiCurl->deleteClassifiers([$params]);
    }

    public function getClassifierPatterns($classifierId)
    {
        $classifiersPatterns = [];

        foreach (\Yii::$app->BrungildaApiCurl->getClassifierPatterns($classifierId)['patterns'] as $classifierPattern) {
            $classifierPattern = new classifiers\ClassifierPatterns(
                $classifierPattern['classifier_pattern_id'],
                $classifierPattern['classifier_id'],
                $classifierPattern['text'],
                $classifierPattern['test'],
                $classifierPattern['version'],
                (bool) $classifierPattern['deleted'],
                (bool) $classifierPattern['active']
            );
            $classifiersPatterns[] = $classifierPattern;
        }

        return $classifiersPatterns;
    }

    public function createClassifierPatterns(int $classifierId, string $text, string $test)
    {
        return \Yii::$app->BrungildaApiCurl->createClassifierPatterns([[
            'classifier_id' => $classifierId,
            'text'          => $text,
            'test'          => $test,
            'active'        => false,
        ]]);
    }

    public function updateClassifierPattern(int $classifierPatternId, string $text, string $test)
    {
        return \Yii::$app->BrungildaApiCurl->updateClassifierPatterns([[
            'classifier_pattern_id' => $classifierPatternId,
            'text'                  => $text,
            'test'                  => $test,
        ]]);
    }

    public function updateClassifierPatterns(array $params)
    {
        return \Yii::$app->BrungildaApiCurl->updateClassifierPatterns($params);
    }

    public function deleteClassifierPattern(array $params)
    {
        return \Yii::$app->BrungildaApiCurl->deleteClassifierPatterns($params);
    }
}

trait nlpClassifiersTrait
{
    //Classifiers
    public function getNlpClassifiers()
    {
        $classifiers = [];
        foreach (\Yii::$app->BrungildaApiCurl->getClassifiers() as $classifier) {
            if (isset($classifier['type']) && $classifier['type'] != "") {
                $classifier = new nlpClassifiers\NlpClassifier(
                    $classifier['classifier_id'],
                    $classifier['text'],
                    $classifier['classes'] ?? [],
                    (string)$classifier['model'],
                    $classifier['type']
                );
                $classifiers[] = $classifier;
            }
        }
        return $classifiers;
    }

    /**
     * @param string $text
     * @param string $type
     * @return bool
     */
    public function createNlpClassifier(string $text, string $type)
    {
        $multipleClassifiersData = explode(PHP_EOL, $text);
        $result = false;

        if(count($multipleClassifiersData) > 1) {
            foreach ($multipleClassifiersData as $datum) {
                $params = [
                    'text' => $datum,
                    'type' => $type
                ];

                $result = \Yii::$app->BrungildaApiCurl->createClassifiers([$params]);
                if(!is_null($result['err'])) {
                    return $result;
                }
            }
            return $result;
        }

        $params = [
            'text' => $text,
            'type' => $type
        ];

        return \Yii::$app->BrungildaApiCurl->createClassifiers([$params]);
    }

    public function updateNlpClassifier(int $classifierId, string $text, string $model)
    {
        $params['classifier_id']    = $classifierId;
        $params['text']             = $text;
        $params['model'] = $model;
        return \Yii::$app->BrungildaApiCurl->updateClassifiers([$params]);
    }

    public function deleteNlpClassifier(int $classifierId)
    {
        $params['classifier_id'] = $classifierId;
        return \Yii::$app->BrungildaApiCurl->deleteClassifiers([$params]);
    }

    public function getNlpClassifiersStatus()
    {
        return \Yii::$app->BrungildaApiCurl->getClassifiersStatus();
    }


    public function trainNlpClassifier(int $classifierId)
    {
        $params['classifier_id']    = $classifierId;
        return \Yii::$app->BrungildaApiCurl->trainClassifiers([$params]);
    }

    //Classes
    public function getNlpClassifierClasses($classifierId)
    {
        $classifierClasess = [];

        foreach (\Yii::$app->BrungildaApiCurl->getNlpClassifierClasses($classifierId) as $classifierClass) {
            $classifierClass = new nlpClassifiers\NlpClassifierClass(
                $classifierClass['classifier_class_id'],
                $classifierClass['classifier_id'],
                $classifierClass['name'],
                $classifierClass['trigger_threshold'],
                $classifierClass['phrases'] ?? []
            );
            $classifierClasess[] = $classifierClass;
        }

        return $classifierClasess;
    }

    public function createNlpClassifierClass(int $classifierId, string $text, int $probability)
    {
        return \Yii::$app->BrungildaApiCurl->createNlpClassifierClasses([[
            'classifier_id'     => $classifierId,
            'name'              => $text,
            'trigger_threshold' => $probability,
        ]]);
    }

    public function updateNlpClassifierClass(int $classifierClassId, string $text, int $probability)
    {
        return \Yii::$app->BrungildaApiCurl->updateNlpClassifierClasses([[
            'classifier_class_id'   => $classifierClassId,
            'name'                  => $text,
            'trigger_threshold'     => $probability,
        ]]);
    }

    public function deleteNlpClassifierClass(int $classifierClassId)
    {
        return \Yii::$app->BrungildaApiCurl->deleteNlpClassifierClasses([[
            'classifier_class_id'   => $classifierClassId,
        ]]);
    }

    //Phrases
    public function getNlpClassPhrases($classId)
    {
        $classPhrases = [];

        foreach (\Yii::$app->BrungildaApiCurl->getNlpClassPhrases($classId) as $classPhrase) {
            $classPhrase = new nlpClassifiers\NlpClassPhrase(
                $classPhrase['learning_phrase_id'],
                $classPhrase['classifier_class_id'],
                $classPhrase['text']
            );
            $classPhrases[] = $classPhrase;
        }

        return $classPhrases;
    }

    public function getNlpClassPhrasesIds($classId)
    {
        $classPhrases = [];

        foreach (\Yii::$app->BrungildaApiCurl->getNlpClassPhrases($classId) as $classPhrase) {
            $classPhrases[$classPhrase['learning_phrase_id']] = $classPhrase['text'];
        }

        return $classPhrases;
    }

    public function createNlpClassPhrase(int $classId, string $text)
    {
        $multiplePhrases = explode(PHP_EOL, $text);

        if (count($multiplePhrases) > 1) {
            $res = [];
            foreach ($multiplePhrases as $phrase) {
               $res = \Yii::$app->BrungildaApiCurl->createNlpClassPhrases([[
                    'classifier_class_id'   => $classId,
                    'text'                  => $phrase,
                ]]);
            }

            return $res;
        }

        return \Yii::$app->BrungildaApiCurl->createNlpClassPhrases([[
            'classifier_class_id'   => $classId,
            'text'                  => $text,
        ]]);
    }

    public function updateNlpClassPhrase(int $phraseId, string $text)
    {
        return \Yii::$app->BrungildaApiCurl->updateNlpClassPhrases([[
            'learning_phrase_id'    => $phraseId,
            'text'                  => $text,
        ]]);
    }

    public function deleteNlpClassPhrases(array $params)
    {
        return \Yii::$app->BrungildaApiCurl->deleteNlpClassPhrases($params);
    }


    //Entities
    public function getPhraseEntities(int $phraseId)
    {
        return \Yii::$app->BrungildaApiCurl->getPhrasesEntities($phraseId);
    }

    public function deletePhraseEntity(int $entityId)
    {
        return \Yii::$app->BrungildaApiCurl->deletePhraseEntity($entityId);
    }

    public function createPhraseEntity(array $data)
    {
        return \Yii::$app->BrungildaApiCurl->addPhraseEntity($data);
    }

    public function updatePhraseEntity(array $data)
    {
        return \Yii::$app->BrungildaApiCurl->updatePhraseEntity($data);
    }
}

trait scenariosTrait
{
    public function getScenarios()
    {
        $scenarios = array();

        $arr = \Yii::$app->cache->get('scenarios_cache_key');
        if ($arr === false) {
            $arr = \Yii::$app->BrungildaApiCurl->getScenarios();
            \Yii::$app->cache->set('scenarios_cache_key', $arr, 900);
        }

        foreach ($arr as $val) {
            $scenario = new scenarios\Scenario(
                $val['scenario_id'],
                $val['nodes'],
                $val['flowChart'],
                $val['ts'] ?? '',
                (string) $val['name'],
                (int) $val['maxStepCount'],
                $val['block']
            );
            $scenarios[] = $scenario;
        }
        return $scenarios;
    }

    public function createScenario(string $name)
    {
        $params['name'] = $name;
        $params['nodes'] = '[]';
        $params['flowChart'] = '{}';
        $params['maxStepCount'] = 0;
        $params['block'] = 0;
        $response = \Yii::$app->BrungildaApiCurl->createScenario($params);
        \Yii::$app->cache->delete('scenarios_cache_key');
        return $response;
    }

    public function createBlock(string $name)
    {
        $params['name'] = $name;
        $params['nodes'] = '[]';
        $params['flowChart'] = '{}';
        $params['maxStepCount'] = 0;
        $params['block'] = 1;
        $response = \Yii::$app->BrungildaApiCurl->createScenario($params);
        \Yii::$app->cache->delete('scenarios_cache_key');
        return $response;
    }

    public function deleteScenario(int $scenarioId)
    {
        $params['scenario_id'] = $scenarioId;
        $response = \Yii::$app->BrungildaApiCurl->deleteScenario($params);
        \Yii::$app->cache->delete('scenarios_cache_key');
        return $response;
    }

    public function deleteScenarios(array $params)
    {
        if(count($params) > 1) {
            foreach ($params as $val) {
                \Yii::$app->BrungildaApiCurl->deleteScenario($val);
            }
            \Yii::$app->cache->delete('scenarios_cache_key');
            return true;
        } else {
            $data['scenario_id'] = $params[0]['scenario_id'];
            \Yii::$app->BrungildaApiCurl->deleteScenario($data);
            \Yii::$app->cache->delete('scenarios_cache_key');
            return true;
        }
    }

    public function updateScenario(int $scenarioId, string $name, string $nodes, string $flowChart, int $maxStepCount, $scenarioType)
    {
        $params['scenario_id'] = $scenarioId;
        $params['name'] = $name;
        $params['nodes'] = $nodes;
        $params['flowChart'] = $flowChart;
        $params['maxStepCount'] = $maxStepCount;
        if ($scenarioType == 'block') {
            $params['block'] = 1;
        } else {
            $params['block'] = 0;
        }
        $response = \Yii::$app->BrungildaApiCurl->updateScenario($params);
        \Yii::$app->cache->delete('scenarios_cache_key');
        return $response;
    }

    public function copyScenario(string $name, string $nodes, string $flowChart, int $maxStepCount, $scenarioType)
    {
        $params['name'] = $name;
        $params['nodes'] = $nodes;
        $params['flowChart'] = $flowChart;
        $params['maxStepCount'] = $maxStepCount;
        if ($scenarioType == 'block') {
            $params['block'] = 1;
        } else {
            $params['block'] = 0;
        }
        $response = \Yii::$app->BrungildaApiCurl->createScenario($params);
        \Yii::$app->cache->delete('scenarios_cache_key');
        return $response;
    }
}

trait substitutesTrait
{
    public function getSubstitutes()
    {
        $substitutes = array();
        $arr = \Yii::$app->BrungildaApiCurl->getSubstitutes();
        foreach ($arr as $val) {
            $substitute = new substitutes\Substitute(
                $val['substitute_id'],
                $val['phrase'],
                (bool) $val['deleted'],
                $val['version']
            );
            $substitutes[] = $substitute;
        }
        return $substitutes;
    }

    public function getSubstitutePhrases(int $substituteId)
    {
        $arr = \Yii::$app->BrungildaApiCurl->getSubstitutePhrases($substituteId);
        $substitutePhrases = array();
        foreach ($arr as $val) {
            $substitutePhrase = new substitutes\SubstitutePhrase(
                $val['substitute_phrase_id'],
                $val['substitute_id'],
                $val['phrase'],
                (bool)$val['deleted'],
                $val['version']
            );
            $substitutePhrases[] = $substitutePhrase;
        }
        return $substitutePhrases;
    }

    public function createSubstitute(string $phrase)
    {
        $params['phrase'] = $phrase;
        $response = \Yii::$app->BrungildaApiCurl->createSubstitute($params);
        return $response;
    }

    public function deleteSubstitute(int $substituteId)
    {
        $params['substitute_id'] = $substituteId;
        $response = \Yii::$app->BrungildaApiCurl->deleteSubstitute($params);
        return $response;
    }

    public function updateSubstitute(int $substituteId, string $phrase)
    {
        $params['substitute_id'] = $substituteId;
        $params['phrase'] = $phrase;
        $response = \Yii::$app->BrungildaApiCurl->updateSubstitute($params);
        return $response;
    }

    public function createSubstitutePhrase(int $substituteId, string $phrase)
    {
        $params['substitute_id'] = $substituteId;
        $params['phrase'] = $phrase;
        $response = \Yii::$app->BrungildaApiCurl->createSubstitutePhrase($params);
        return $response;
    }

    public function deleteSubstitutePhrase(int $substituteId, int $substitutePhraseId)
    {
        $params['substitute_id'] = $substituteId;
        $params['substitute_phrase_id'] = $substitutePhraseId;
        $response = \Yii::$app->BrungildaApiCurl->deleteSubstitutePhrase($params);
        return $response;
    }

    public function updateSubstitutePhrase(int $substituteId, int $substitutePhraseId, string $phrase)
    {
        $params['substitute_id'] = $substituteId;
        $params['substitute_phrase_id'] = $substitutePhraseId;
        $params['phrase'] = $phrase;
        $response = \Yii::$app->BrungildaApiCurl->updateSubstitutePhrase($params);
        return $response;
    }
}

trait variablesTrait
{
    public function getVariables()
    {
        $variables = array();
        $arr = \Yii::$app->BrungildaApiCurl->getVariables();
        foreach ($arr as $val) {
            $variable = new variables\Variable(
                $val['variable_id'],
                $val['name'],
                $val['value'],
                $val['description'],
                (bool) $val['deleted'],
                (bool) $val['enabled']
            );
            $variables[] = $variable;
        }
        return $variables;
    }

    public function createVariable(string $name, string $value, string $description)
    {
        $params['name'] = $name;
        $params['value'] = $value;
        $params['description'] = $description;
        $response = \Yii::$app->BrungildaApiCurl->createVariable($params);
        return $response;
    }

    public function deleteVariable(int $variableId)
    {
        $params['variable_id'] = $variableId;
        $response = \Yii::$app->BrungildaApiCurl->deleteVariable($params);
        return $response;
    }

    public function updateVariable(int $variableId, string $name, string $value, string $description)
    {
        $params['variable_id'] = $variableId;
        $params['name'] = $name;
        $params['value'] = $value;
        $params['description'] = $description;
        $response = \Yii::$app->BrungildaApiCurl->updateVariable($params);
        return $response;
    }
}

trait chatTrait
{
    public function deleteChat($fromId)
    {
        $params['from'] = $fromId;
        $response = \Yii::$app->BrungildaApiCurl->deleteChat($params);
        return $response;
    }
}

trait searchTrait
{
    private $elements = [];

    /**
     * @param $text
     * @param $searchBy
     * @return array
     */
    public function search($text, $searchBy)
    {
        switch ($searchBy) {
            case SearchForm::RULES:
                $this->searchDataForRules($text);
                break;
            case SearchForm::TEMPLATES:
                $this->searchDataForTemplates($text);
                break;
            case SearchForm::PATTERNS:
                $this->searchDataForPatterns($text);
                break;
            case SearchForm::EXC_PATTERNS:
                $this->searchDataForEXCPatterns($text);
                break;
            case SearchForm::PHRASES:
                $this->searchDataForPhrases($text);
                break;
        }

        return $this->elements;
    }

    /**
     * @param $text string
     */
    private function searchDataForRules($text)
    {
        $arr = $this->getBrungildaApiCurlComponent()->searchInRules($text);
        foreach ($arr as $val) {
            $this->elements[] = new topics\Rule(
                $val['rule']['rule_id'],
                $val['rule']['topic_id'],
                $val['rule']['exclude_template'],
                $val['rule']['exec_limit'],
                $val['rule']['priority'],
                (bool)$val['rule']['enabled'],
                (bool)$val['rule']['active'],
                (bool)$val['rule']['deleted'],
                $val['rule']['name'],
                $val['rule']['version'],
                $val['rule']['parent_id']       == null ?   self::DEFAULT_ID : $val['rule']['parent_id'],
                $val['rule']['link_id']         == null ?   self::DEFAULT_ID : $val['rule']['link_id'],
                $val['rule']['phrase_id']       == null ?   self::DEFAULT_ID : $val['rule']['phrase_id'],
                $val['rule']['type_phrase_id']  == null ?   self::DEFAULT_ID : $val['rule']['type_phrase_id']
            );
        }
    }

    /**
     * @param $text string
     */
    private function searchDataForTemplates($text)
    {
        $arr = $this->getBrungildaApiCurlComponent()->searchInTemplates($text);
        foreach ($arr as $val) {
            $this->elements[] = new topics\RulesTemplate(
                $val['item']['template_id'],
                $val['item']['rule_id'],
                $val['item']['text'],
                (bool)$val['item']['deleted'],
                $val['item']['delay_id']    == null ? self::DEFAULT_ID: $val['item']['delay_id'],
                $val['item']['timeslot_id'] == null ? self::DEFAULT_ID: $val['item']['timeslot_id'],
                $val['item']['version']
            );
        }
    }

    /**
     * @param $text string
     */
    private function searchDataForPatterns($text)
    {
        $arr = $this->getBrungildaApiCurlComponent()->searchInPatterns($text);
        foreach ($arr as $val) {
            if ($val['item']['active'] == null) {
                $val['item']['active'] = false;
            }
            $this->elements[] = new topics\RulesPattern(
                $val['item']['pattern_id'],
                $val['item']['rule_id'],
                $val['item']['text'],
                (bool)$val['item']['deleted'],
                $val['item']['test'],
                $val['item']['version'],
                (bool)$val['item']['active']
            );
        }
    }

    /**
     * @param $text string
     */
    private function searchDataForEXCPatterns($text)
    {
        $arr = $this->getBrungildaApiCurlComponent()->searchInExceptionPatterns($text);
        foreach ($arr as $val) {
            if ($val['item']['active'] == null) {
                $val['item']['active'] = false;
            }

            $this->elements[] = new topics\RulesPattern(
                $val['item']['exception_pattern_id'],
                $val['item']['rule_id'],
                $val['item']['text'],
                (bool)$val['item']['deleted'],
                $val['item']['test'],
                $val['item']['version'],
                (bool)$val['item']['active']
            );
        }
    }

    /**
     * @param $text string
     */
    private function searchDataForPhrases($text)
    {
        $arr = $this->getBrungildaApiCurlComponent()->searchInPhrases($text);
        foreach ($arr as $val) {
            $this->elements[] = new phrases\Phrase(
                $val['item']['phrase_id'],
                $val['item']['group_phrase_id'],
                $val['item']['text'],
                $val['item']['delay_id']    == null ? self::DEFAULT_ID : $val['item']['delay_id']  ,
                $val['item']['timeslot_id'] == null ? self::DEFAULT_ID : $val['item']['timeslot_id'],
                $val['item']['version'],
                (bool)$val['item']['deleted']
            );
        }
    }

    /**
     * @return BrungildaApiCurl
     */
    private function getBrungildaApiCurlComponent()
    {
        return \Yii::$app->BrungildaApiCurl;
    }
}

class DataProvider
{
    use Singleton;
    const DEFAULT_ID = -1;

    private function comparePriorityForSort($a, $b)
    {
        if ($a->getPriority() == $b->getPriority()) {
            return 0;
        }
        return ($a->getPriority() < $b->getPriority()) ? -1 : 1;
    }

    use topicsTrait;
    use phrasesTrait;
    use classifiersTrait;
    use nlpClassifiersTrait;
    use scenariosTrait;
    use substitutesTrait;
    use variablesTrait;
    use chatTrait;
    use searchTrait;
}
