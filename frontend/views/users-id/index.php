<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\assets\UsersIdAsset;

UsersIdAsset::register($this);

Pjax::begin(['id' => 'search',]);
// Render search form (don't know why, but official documentation recommended to store the form in separate file -
// http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#separate-filter-form and
// https://yiiframework.com.ua/ru/doc/guide/2/output-data-widgets/#otdelnaa-forma-filtracii)
echo $this->render('_search', ['model' => $searchModel]);
echo '<hr />';

// Render search results (user-bot pairs). Gridview is wrapped by div for progressbar replacement while data is reading
echo '<div id="searchResults">';
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['attribute' => 'id',],
        [
            'class'     => 'yii\grid\ActionColumn',
            'header'    => 'View',
            'template'  => '{view}',
            'buttons'   => [
                'view'      => function ($url, $model, $key) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        [
                            'users-id/messages',
                            'SearchCommunicationLog[sender]'    => $model->userId,
                            'SearchCommunicationLog[receiver]'  => $model->modelId,
                        ],
                        ['class' => 'view-messages']
                    );
                },
            ],
        ],
        ['attribute' => 'createdAt',],
        [
            'attribute'     => 'userId',
            'label'         => '<span class="glyphicon glyphicon-user"></span> User',
            'encodeLabel'   => false,
        ],
        [
            'attribute' => 'modelId',
            'label'     => 'Bot',
        ],
        ['attribute' => 'country',],
        ['attribute' => 'language',],
        ['attribute' => 'trafficSource',],
        ['attribute' => 'profileType',],
        ['attribute' => 'platform',],
        [
            'attribute' => 'scenarioId',
            'label'     => 'Scenario',
            'value'     => function ($data) use ($searchModel) {
                if ($data->scenarioId != 0) {
                    return $searchModel->availableScenarios[$data->scenarioId] ?? '';
                } else {
                    return '';
                }
            },
        ],
        [
            'attribute' => 'messageCount',
            'label'     => 'Messages',
        ],
        [
            'attribute' => 'maxPlaceholderId',
            'label'     => 'Placeholder',
            'value'     => function ($data) use ($searchModel) {
                if ($data->maxPlaceholderId != 0) {
                    return $searchModel->availablePlaceholders[$data->maxPlaceholderId] ?? '';
                } else {
                    return '';
                }
            },
        ],
    ],
]);
echo '</div>';
echo '<hr />';
Pjax::end();

// Render dialogue between pair user-bot (it's not necessary to add 'enablePushState' => false option here,
// because this parameters will be passed in $.pjax.reload function in javascript).
// Messages handed in a separate pjax-container for not to render form and gridview again
Pjax::begin(['id' => 'messages',]);
Pjax::end();
