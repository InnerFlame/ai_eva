export class TopicsModel {
    constructor(
        public active: number = 0,
        public countTriggering: string = "",
        public deleted: number = 0,
        public enabled: number = 0,
        public isExclude: string = "0",
        public lang: string = "",
        public name: string = "",
        public priority: number = 0,
        public restrictions: any = [],
        public status_out: number = 0,
        public switching: number = 0,
        public tags: any = [],
        public topic_id: number = 0,
        public version: number = 0
    ) {}

    public reset(): void {
        this.topic_id = 0;
        this.name = "";
        this.active = 0;
        this.deleted = 0;
        this.enabled = 0;
        this.priority = 0;
        this.status_out = 0;
        this.switching = 0;
        this.isExclude = "0";
        this.countTriggering = "";
        this.lang = "";
        this.tags = [];
        this.restrictions = [];
        this.version = 0
    }

    public load(topicData: any): void {
        this.topic_id = topicData.topic_id;
        this.name = topicData.name;
        this.active = topicData.active;
        this.deleted = topicData.deleted;
        this.enabled = topicData.enabled;
        this.priority = topicData.priority;
        this.status_out = topicData.status_out;
        this.switching = topicData.switching;
        this.isExclude = topicData.isExclude;
        this.countTriggering = topicData.countTriggering;
        this.lang = topicData.lang;
        this.tags = topicData.tags ? topicData.tags : [];
        this.restrictions = topicData.restrictions ? topicData.restrictions : [];
        this.version = topicData.version;
    }
}

export class TopicRulesModel {
    constructor(
        public rule_id = 0,
        public topic_id = 0,
        public exclude_template = 0,
        public exec_limit = 0,
        public priority = 0,
        public enabled = 0,
        public active = 0,
        public deleted = 0,
        public name = "",
        public version = 0,
        public parent_id = 0,
        public link_id = 0,
        public phrase_id = 0,
        public type_phrase_id = 0
    ) {}

    public load(ruleData: any): void {
        this.rule_id = ruleData.rule_id;
        this.topic_id = ruleData.topic_id;
        this.exclude_template = ruleData.exclude_template;
        this.exec_limit = ruleData.exec_limit;
        this.priority = ruleData.priority;
        this.enabled = ruleData.enabled;
        this.active = ruleData.active;
        this.deleted = ruleData.deleted;
        this.name = ruleData.name;
        this.version = ruleData.version;
        this.parent_id = ruleData.parent_id;
        this.link_id = ruleData.link_id;
        this.phrase_id = ruleData.phrase_id;
        this.type_phrase_id = ruleData.type_phrase_id;
    }

    public reset(): void {
        this.rule_id = 0;
        this.topic_id = 0;
        this.exclude_template = 0;
        this.exec_limit = 0;
        this.priority = 0;
        this.enabled = 0;
        this.active = 0;
        this.deleted = 0;
        this.name = "";
        this.version = 0;
        this.parent_id = 0;
        this.link_id = 0;
        this.phrase_id = 0;
        this.type_phrase_id = 0;
    }
}

export class RulePatternsModel {
    constructor(
        public active: number = 0,
        public deleted: number = 0,
        public pattern_id: number = 0,
        public rule_id: number = 0,
        public test: string = "",
        public text: string = "",
        public version: number = 0,
    ) {}

    public load(patternData: any): void {
        this.active = patternData.active;
        this.deleted = patternData.deleted;
        this.pattern_id = patternData.pattern_id;
        this.rule_id = patternData.rule_id;
        this.test = patternData.test;
        this.text = patternData.text;
        this.version = patternData.version;
    }

    public reset(): void {
        this.active = 0;
        this.deleted = 0;
        this.pattern_id = 0;
        this.rule_id = 0;
        this.test = "";
        this.text = "";
        this.version = 0;
    }
}

export class RulePatternsVariableModel {
    constructor(
        public pattern_var_id: number = 0,
        public variable_id: any = null,
        public pattern_id: number = 0,
        public index_content: any = null,
    ) {}

    public load(varData: any): void {
        this.pattern_var_id = varData.pattern_var_id;
        this.pattern_id = varData.pattern_id;
        this.variable_id = varData.variable_id;
        this.index_content = varData.index_content;
    }

    public reset(): void {
        this.pattern_var_id = 0;
        this.pattern_id = 0;
        this.variable_id = null;
        this.index_content = null;
    }
}

export class SubRulePatternsModel {
    constructor(
        public active: number = 0,
        public deleted: number = 0,
        public pattern_id: number = 0,
        public rule_id: number = 0,
        public test: string = "",
        public text: string = "",
        public version: number = 0,
    ) {}

    public load(patternData: any): void {
        this.active = patternData.active;
        this.deleted = patternData.deleted;
        this.pattern_id = patternData.pattern_id;
        this.rule_id = patternData.rule_id;
        this.test = patternData.test;
        this.text = patternData.text;
        this.version = patternData.version;
    }

    public reset(): void {
        this.active = 0;
        this.deleted = 0;
        this.pattern_id = 0;
        this.rule_id = 0;
        this.test = "";
        this.text = "";
        this.version = 0;
    }
}


export class RuleTemplatesModel {
    constructor(
        public delay_id: any = null,
        public deleted: number = 0,
        public rule_id: number = 0,
        public template_id: number = 0,
        public timeslot_id: number = 0,
        public text: string = "",
        public version: number = 0
    ) {}

    public load(templateData): void {
        this.delay_id = templateData.delay_id;
        this.deleted = templateData.deleted;
        this.rule_id = templateData.rule_id;
        this.template_id = templateData.template_id;
        this.timeslot_id = templateData.timeslot_id;
        this.text = templateData.text;
        this.version = templateData.version;
    }

    public reset(): void {
        this.delay_id = 0;
        this.deleted = 0;
        this.rule_id = 0;
        this.template_id = 0;
        this.timeslot_id = 0;
        this.text = "";
        this.version = 0;
    }
}

export class SubRuleTemplatesModel {
    constructor(
        public delay_id: any = null,
        public deleted: number = 0,
        public rule_id: number = 0,
        public template_id: number = 0,
        public timeslot_id: number = 0,
        public text: string = "",
        public version: number = 0
    ) {}

    public load(templateData): void {
        this.delay_id = templateData.delay_id;
        this.deleted = templateData.deleted;
        this.rule_id = templateData.rule_id;
        this.template_id = templateData.template_id;
        this.timeslot_id = templateData.timeslot_id;
        this.text = templateData.text;
        this.version = templateData.version;
    }

    public reset(): void {
        this.delay_id = 0;
        this.deleted = 0;
        this.rule_id = 0;
        this.template_id = 0;
        this.timeslot_id = 0;
        this.text = "";
        this.version = 0;
    }
}

export class RuleExcPatternsModel {
    constructor(
        public active: number = 0,
        public deleted: number = 0,
        public exception_pattern_id: number = 0,
        public rule_id: number = 0,
        public test: string = "",
        public text: string = "",
        public version: number = 0
    ) {}

    public load(extPatternData: any): void {
        this.active = extPatternData.active;
        this.deleted = extPatternData.deleted;
        this.exception_pattern_id = extPatternData.exception_pattern_id;
        this.rule_id = extPatternData.rule_id;
        this.test = extPatternData.test;
        this.text = extPatternData.text;
        this.version = extPatternData.version;
    }

    public reset(): void {
        this.active = 0,
        this.deleted = 0,
        this.exception_pattern_id = 0,
        this.rule_id = 0,
        this.test = "",
        this.text = "",
        this.version = 0
    }
}

export class RuleSubRulesModel {
    constructor(
        public active: number = 0,
        public deleted: number = 0,
        public enabled: number = 0,
        public rule_id: number = 0,
        public exclude_template: number = 0,
        public exec_limit: number = 0,
        public link_id: any = null,
        public parent_id: number = 0,
        public phrase_id: number = 0,
        public topic_id: number = 0,
        public type_phrase_id: number = 0,
        public priority: number = 0,
        public name: string = "",
        public version: number = 0
    ) {}

    public load(subRuleData: any): void {
        this.active = subRuleData.active;
        this.deleted = subRuleData.deleted;
        this.enabled = subRuleData.enabled;
        this.rule_id = subRuleData.rule_id;
        this.exclude_template = subRuleData.exclude_template;
        this.exec_limit = subRuleData.exec_limit;
        this.link_id = subRuleData.link_id;
        this.parent_id = subRuleData.parent_id;
        this.phrase_id = subRuleData.phrase_id;
        this.rule_id = subRuleData.rule_id;
        this.topic_id = subRuleData.topic_id;
        this.type_phrase_id = subRuleData.type_phrase_id;
        this.priority = subRuleData.priority;
        this.name = subRuleData.name;
        this.version = subRuleData.version;
    }

    public reset(): void {
        this.active = 0;
        this.deleted = 0;
        this.enabled = 0;
        this.rule_id = 0;
        this.exclude_template = 0;
        this.exec_limit = 0;
        this.link_id = null;
        this.parent_id = 0;
        this.phrase_id = 0;
        this.rule_id = 0;
        this.topic_id = 0;
        this.type_phrase_id = 0;
        this.priority = 0;
        this.name = "";
        this.version = 0;
    }
}

export class LearningPhrasesModel {
    constructor(
        public learning_phrase_id: number = 0,
        public topic_id: number = 0,
        public rule_id: number = 0,
        public text: string = ''
    ) {}

    public load(data: any): void {
        this.learning_phrase_id = data.learning_phrase_id;
        this.topic_id = data.topic_id;
        this.rule_id = data.rule_id;
        this.text = data.text;
    }

    public reset(): void {
        this.learning_phrase_id = 0;
        this.topic_id = 0;
        this.rule_id = 0;
        this.text = '';
    }
}

export class TopicsRouting {
    private static baseUrl = '/topics-ng/';

    /** Topics routes */

    public static getTopicsModelsUrl(): string {
        return this.baseUrl + 'get-topics';
    }

    public static getSelectsDataUrl(): string {
        return this.baseUrl + 'get-selects-data';
    }

    public static getTopicCreateUrl(): string {
        return this.baseUrl + 'create-topic';
    }

    public static getTopicDeleteUrl(): string {
        return this.baseUrl + 'delete-topic';
    }

    public static getTopicUpdateUrl(): string {
        return this.baseUrl + 'update-topic';
    }

    public static getTopicsUpdateOptUrl(): string {
        return this.baseUrl + 'toggle-topics-opt';
    }

    /** Learning phrases routes */

    public static getLearningPhrasesCreateUrl(): string
    {
        return this.baseUrl + 'create-learning-phrases';
    }

    public static getLearningPhrasesUpdateUrl(): string
    {
        return this.baseUrl + 'update-learning-phrases';
    }

    public static getLearningPhrasesDeleteUrl(): string
    {
        return this.baseUrl + 'delete-learning-phrases';
    }

    public static getLearningPhrasesDeleteMultipleUrl(): string
    {
        return this.baseUrl + 'delete-multiple-learning-phrases';
    }

    /** Rules routes */

    public static getTopicRulesModelsUrl(): string {
        return this.baseUrl + 'get-topic-rules';
    }

    public static getTopicRuleCreateUrl(): string {
        return this.baseUrl + 'create-rule';
    }

    public static getSubRuleCreateUrl(): string {
        return this.baseUrl + 'create-sub-rule';
    }

    public static getTopicRuleUpdateOptUrl(): string {
        return this.baseUrl + 'toggle-topic-rule-opt';
    }

    public static getUpdateTopicRuleUrl(): string {
        return this.baseUrl + 'update-rule';
    }

    public static getDeleteTopicRuleUrl(): string {
        return this.baseUrl + 'delete-rule';
    }

    public static getRuleFormsDataUrl(): string {
        return this.baseUrl + 'get-rule-forms-data';
    }

    public static getSubRuleDataUrl(): string {
        return this.baseUrl + 'get-sub-rule-data'
    }

    public static getCreatePatternUrl(): string {
        return this.baseUrl + 'create-pattern';
    }

    public static getUpdatePatternUrl(): string {
        return this.baseUrl + 'update-pattern';
    }

    public static getUpdateMultiPatternUrl(): string {
        return this.baseUrl + 'update-patterns';
    }

    public static getUpdateMultiPatternStatusUrl(): string {
        return this.baseUrl + 'update-patterns-status';
    }

    public static getDeletePatternUrl(): string {
        return this.baseUrl + 'delete-pattern';
    }

    public static getDeleteMultiPatternUrl(): string {
        return this.baseUrl + 'delete-patterns';
    }

    public static getCreateTemplateUrl(): string {
        return this.baseUrl + 'create-template';
    }

    public static getUpdateTemplateUrl(): string {
        return this.baseUrl + 'update-template';
    }

    public static getDeleteTemplateUrl(): string {
        return this.baseUrl + 'delete-template';
    }

    public static getUpdateMultiTemplateUrl(): string {
        return this.baseUrl + 'update-templates';
    }

    public static getDeleteMultiTemplateUrl(): string {
        return this.baseUrl + 'delete-templates';
    }

    public static getCreateExcPatternUrl(): string {
        return this.baseUrl + 'create-exc-pattern';
    }

    public static getUpdateExcPatternUrl(): string {
        return this.baseUrl + 'update-exc-pattern';
    }

    public static getDeleteExcPatternUrl(): string {
        return this.baseUrl + 'delete-exc-pattern';
    }

    public static getUpdateMultiExcPatternUrl(): string {
        return this.baseUrl + 'update-exc-patterns';
    }

    public static getDeleteMultiExcPatternUrl(): string {
        return this.baseUrl + 'delete-exc-patterns';
    }

    public static getUpdateTopicsPriorityUrl(): string {
        return this.baseUrl + 'update-topic-priority';
    }

    public static getUpdateRulesPriorityUrl(): string {
        return this.baseUrl + 'update-rule-priority';
    }

    public static getCopyRuleUrl(): string {
        return this.baseUrl + 'copy-rule'
    }

    public static getCopyPatternsUrl(): string {
        return this.baseUrl + 'copy-patterns'
    }

    public static getCreatePatternVarUrl(): string {
        return this.baseUrl + 'create-pattern-var'
    }
    public static getUpdatePatternVarUrl(): string {
        return this.baseUrl + 'update-pattern-var'
    }
    public static getDeletePatternVarUrl(): string {
        return this.baseUrl + 'delete-pattern-var'
    }

    public static getPatternsVariablesUrl(): string {
        return this.baseUrl + 'get-patterns-variables';
    }
}


