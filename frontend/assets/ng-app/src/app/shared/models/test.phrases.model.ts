export class TestPhrasesModel
{
    constructor(
        public model: string = '',
        public text: string = '',
        public project: string = ''
    ) {  }

    public static getModelsApiUrl(): string
    {
        return "/test-phrases/get-models";
    }

    public static getResultsApiUrl(): string
    {
        return "/test-phrases/get-result";
    }
}
