<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\assets\AppAsset;
use common\widgets\Sidebar\Sidebar;
use yii\widgets\Menu;
use frontend\models\errorLog\ErrorLogForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <base href="/">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php
NavBar::begin([
    'brandLabel' => 'EVA ADMIN PANEL',
    'brandUrl' => ['/topics'],
    'options' => [
        'class' => 'navbar navbar-default navbar-fixed-top',
    ],
    'innerContainerOptions' => [
        'class' => 'container-fluid',
    ],
]);
$searchWidget = '';
$menuItemsRight = array();
if (\Yii::$app->user->isGuest) {
    $menuItemsRight[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
    //if (\Yii::$app->user->can('createUsers')) {
    //    $menuItemsRight[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    //}
    $searchWidget = Html::beginForm('', 'post', ['class' => 'navbar-form navbar-left'])
        . Html::beginTag('div', ['class' => 'input-group'])
        . Html::textInput('search', null, [
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Search in...',
            'aria-label' => '...'])
        . Html::beginTag('div', ['class' => 'input-group-btn'])
        . Html::button('<span id="headerSearchBy">Templates</span><span class="caret"></span>', [
            'class' => 'btn btn-default dropdown-toggle',
            'aria-expanded' => 'false',
            'aria-haspopup' => 'true',
            'data-toggle' => 'dropdown',
            'type' => 'button'])
        . Menu::widget([
            'items' => [
                ['label' => 'Topics properties', 'options' => ['class' => 'dropdown-header']],
                ['label' => 'Rules', 'url' => 'javascript:void(0);'],
                ['label' => 'Gambits', 'url' => 'javascript:void(0);'],
                ['label' => null, 'options' => ['class' => 'divider', 'role' => 'separator']],
                ['label' => 'Rules properties', 'options' => ['class' => 'dropdown-header']],
                ['label' => 'Patterns', 'url' => 'javascript:void(0);'],
                ['label' => 'Templates', 'url' => 'javascript:void(0);'],
                ['label' => 'Exception patterns', 'url' => 'javascript:void(0);'],
            ],
            'options' => [
                'class' => 'dropdown-menu dropdown-menu-right',
            ],
        ])
        . Html::button(
            '<span class="glyphicon glyphicon-search"></span>',
            ['class' => 'btn btn-primary', 'type' => 'button']
        )
        . Html::endTag('div')
        . Html::endTag('div')
        . Html::endForm();
    $menuItemsRight[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            '<span class="glyphicon glyphicon-log-out"></span> Logout (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link']
        )
        . Html::endForm()
        . '</li>';
}

//echo $searchWidget;
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItemsRight,
]);
NavBar::end();
?>


<div class="container-fluid">
    <?php if (!\Yii::$app->user->isGuest) {?>
    <div class="col-sm-3 col-md-2 sidebar" style="z-index: 1">
        <?php
        $errorBadge = '';
        $errorCount = ErrorLogForm::getCount();
        if ($errorCount > 0) {
            $errorBadge .= ' <span class="badge badge-danger pull-right">' . $errorCount . '</span>';
        }
        echo Sidebar::widget([
            'items' => [
                //General items
                '<li class="dropdown-header">General</li>',
                '<li class="divider" role="separator"></li>',
                [
                    'label' => 'Topics',
                    'items' => [
                        ['label' => 'Topics', 'url' => ['/topics-ng']],
                        ['label' => 'Topics Group', 'url' => ['/topics-group-ng']],
                    ],
                ],
                [
                    'label' => 'Scenarios',
                    'items' => [
                        ['label' => 'Scenarios', 'url' => ['/scenarios']],
                        ['label' => 'Phrases', 'url' => ['/phrases']],
                        ['label' => 'Classifiers', 'url' => ['/classifiers-ng']],
                        ['label' => 'NLP classifiers', 'url' => ['/nlp-classifiers-ng']],
                        ['label' => 'Multi regexp classifiers', 'url' => ['/mre-classifiers-ng']],
                    ],
                ],
                [
                    'label' => 'Substitutes',
                    'url' => ['/substitutes'],

                ],
                [
                    'label' => 'Variables',
                    'url' => ['/variables'],

                ],
                [
                    'label' => 'Intervals',
                    'items' => [
                        ['label' => 'Delays', 'url' => '#delays'],
                        ['label' => 'Timeslots', 'url' => '/time-slots'],
                    ],
                ],
                [
                    'label' => 'Settings',
                    'url' => ['/settings-ng'],

                ],
                [
                    'label' => 'Placeholders',
                    'url' => ['/placeholders'],
                ],
                //Action items
                '<li class="dropdown-header">Action</li>',
                '<li class="divider" role="separator"></li>',
                [
                    'label' => 'Test chat',
                    'url' => ['/test-chat'],
                ],
                [
                    'label' => 'Test phrases',
                    'url' => ['/test-phrases'],
                ],
                //Information items
                '<li class="dropdown-header">Information</li>',
                '<li class="divider" role="separator"></li>',
                [
                    'label' => 'Common info',
                    'url' => ['/common-info']
                ],
                [
                    'label' => 'Users id',
                    'url' => ['/users-id']
                ],
                [
                    'label' => 'Search',
                    'url' => ['/search'],
                ],
                [
                    'label' => 'History',
                    'url' => '#history',
                ],
                //Access
                '<li class="dropdown-header">Access</li>',
                '<li class="divider" role="separator"></li>',
                [
                    'label' => 'Access',
                    'url' => ['/access'],
                ],
                //Error log
                '<li class="dropdown-header">Log</li>',
                '<li class="divider" role="separator"></li>',
                [
                    'label' => 'Error log' . $errorBadge,
                    'url' => ['/error-log'],
                    'encode' => false,
                ],
                '<li class="dropdown-header">Tools</li>',
                '<li class="divider" role="separator"></li>',
                [
                    'label' => 'Clean assets',
                    'url' => ['/tools/clean-assets'],
                ],
            ],
            'options' => ['class' => 'nav nav-pills nav-stacked', 'role' => 'tablist'],
        ]);
        ?>
    </div>
    <?php }; ?>
    <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10 content">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
        <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10">
            <p class="text-muted text-center">
                <small>Copyright &copy; <?= date('Y') ?> Antiscam.</small>
            </p>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
