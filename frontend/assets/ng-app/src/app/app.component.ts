import { Component } from '@angular/core';

@Component({
  selector    : 'app-root',
  templateUrl : './app.component.html',
  styleUrls   : ['../assets/css/app.component.css'] //core styles
})

export class AppComponent {}
