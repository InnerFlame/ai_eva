<?php
namespace frontend\models\settings;

use yii\db\ActiveRecord;

class ActionWay extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_action_ways_table';
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id' => 'setting_id']);
    }
}
