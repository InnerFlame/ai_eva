<?php
declare(strict_types=1);

namespace frontend\models\classifiers;

class Classifiers
{
    public $id;
    public $text;
    public $deleted;
    public $version;
    public $patterns;

    /**
     * Classifiers constructor
     *
     * @param int    $id       Id of classifier.
     * @param string $text     Its text.
     * @param bool   $deleted  Is deleted.
     * @param int    $version  Its version.
     * @param array  $patterns Its patterns
     *
     * @return this
     */
    public function __construct(int $id, string $text, bool $deleted, int $version, array $patterns)
    {
        $this->id       = $id;
        $this->text     = $text;
        $this->deleted  = $deleted;
        $this->version  = $version;
        $this->patterns = $patterns;
    }

    /**
     * Getter for "id" param.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Setter for "id" param.
     *
     * @param int $id Classifier id
     *
     * @return void
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Getter for "text" param.
     *
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * Setter for "text" param.
     *
     * @param string $text Classifier text
     *
     * @return void
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * Getter for "deleted" param.
     *
     * @return bool
     */
    public function isDeleted() : bool
    {
        return $this->deleted;
    }

    /**
     * Setter for "deleted" param.
     *
     * @param bool $deleted Is deleted
     *
     * @return void
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Getter for "version" param.
     *
     * @return int
     */
    public function getVersion() : int
    {
        return $this->version;
    }

    /**
     * Setter for "version" param.
     *
     * @param int $version Classifier version
     *
     * @return void
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * Getter for "patterns" param.
     *
     * @return array
     */
    public function getPatterns() : array
    {
        return $this->excludePhrase;
    }

    /**
     * Setter for "patterns" param.
     *
     * @param array $patterns List of patterns
     *
     * @return void
     */
    public function setPatterns(array $patterns)
    {
        $this->patterns = $patterns;
    }
}
