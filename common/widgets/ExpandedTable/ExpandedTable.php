<?php

namespace common\widgets\ExpandedTable;

use yii\bootstrap\Html;
use \yii\bootstrap\Widget;

class ExpandedTable extends Widget
{
    public $headerItems;
    public $items;

    public function init()
    {
        parent::init();
        if ($this->headerItems === null) {
            $this->headerItems = array();
        }
        if ($this->items === null) {
            $this->items = array();
        }
        $view = $this->getView();
        ExpandedTableAsset::register($view);
    }

    private function renderItems()
    {
        $content = Html::beginTag('table', ['class' => 'table table-hover', 'id' => 'expanded-table',]);
        $content .= Html::beginTag('thead');
        $content .= Html::beginTag('tr');
        foreach ($this->headerItems as $headerItem) {
            $content .= Html::beginTag('th');
            $content .= $headerItem['label'];
            $content .= Html::endTag('th');
        }
        $content .= Html::endTag('tr');
        $content .= Html::endTag('thead');
        $content .= Html::beginTag('tbody');
        foreach ($this->items as $item) {
            $content .= Html::beginTag('tr');
            $isFirstColumn = true;
            foreach ($item as $itemEelements) {
                //Set first column elements bold (<th> tag)
                if ($isFirstColumn) {
                    $content .= Html::beginTag('th');
                } else {
                    $content .= Html::beginTag('td');
                }
                $isFirstElement = true;
                $i = 0;
                $iVisible = 0;
                foreach ($itemEelements as $itemEelement) {
                    if ($itemEelement == null) {
                        continue;
                    }
                    //Insert <br> before not first elements
                    if ($isFirstElement) {
                        $isFirstElement = false;
                    } elseif ($i < 10) {
                        if (isset($itemEelement['value'])) {
                            $content .= Html::tag('br');
                        }
                    }
                    if (isset($itemEelement['value'])) {
                        $iVisible++;
                    }
                    if ($iVisible == 10) {
                        $content .= '<span class="glyphicon glyphicon-arrow-down"></span>';
                        $iVisible++;

                    }
                    if ($i >= 9) {
                        if (isset($itemEelement['value'])) {
                            $content .= Html::beginTag('span', ['style' => 'display: none;']);
                        }
                    } else {
                        if (isset($itemEelement['value'])) {
                            $content .= Html::beginTag('span');
                        }
                    }
                        if (isset($itemEelement['value'])) {
                            $content .= Html::tag('span', $itemEelement['value']);
                        }
                        if (isset($itemEelement['hidden'])) {
                            $content .= Html::hiddenInput(null, $itemEelement['hidden']);
                        }
                        if (isset($itemEelement['hidden_1'])) {
                            $content .= Html::hiddenInput('hidden_1', $itemEelement['hidden_1']);
                        }
                        if (isset($itemEelement['hidden_2'])) {
                            $content .= Html::hiddenInput('hidden_2', $itemEelement['hidden_2']);
                        }
                    if (isset($itemEelement['value'])) {
                        $content .= Html::endTag('span');
                    }
                    $i++;
                }
                if ($isFirstColumn) {
                    $content .= Html::endTag('th');
                    $isFirstColumn = false;
                } else {
                    $content .= Html::endTag('td');
                }
            }
            $content .= Html::endTag('tr');
        }
        $content .= Html::endTag('tbody');
        $content .= Html::endTag('table');
        return $content;
    }

    public function run()
    {
        $content = $this->renderItems();
        return $content;
    }
}
